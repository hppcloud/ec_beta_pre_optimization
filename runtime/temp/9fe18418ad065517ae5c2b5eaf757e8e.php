<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:49:"./application/seller/new/service/refund_list.html";i:1548757881;s:58:"/mnt/www/test/shop/application/seller/new/public/head.html";i:1544286252;s:58:"/mnt/www/test/shop/application/seller/new/public/left.html";i:1529392734;s:58:"/mnt/www/test/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<div class="ncsc-layout wrapper">
     <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
    <div id="layoutRight" class="ncsc-layout-right">
        <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>售后服务<i class="icon-angle-right"></i>退款换货
        </div>
        <div class="main-content" id="mainContent">
            <div class="tabmenu">
                <ul class="tab pngFix">
                	<li class="active"><a href="<?php echo U('Service/refund_list'); ?>">退货退款</a></li>
                    <!--<li class=""><a href="<?php echo U('Service/return_list'); ?>">换货维修</a></li>-->
                </ul>
            </div>
            <div class="alert alert-block mt10">
			  <ul class="mt5">
			    <li>1、若退货申请相关商品未发货，卖家审核通过后，视为同意退款，平台确认后，由平台处理退款到用户账号余额或按支付原路返回</li>
			    <li>2、若退货申请相关商品已经发货，卖家同意退货时，卖家需要确认收到买家发回的货物之后，再进入平台退款流程</li>
			    <li>3、若退货申请相关商品已经发货，卖家同意退货时，如果有运费，运费问题需要卖家与用户协商</li>
			  </ul>
			</div>
            <form method="post" action="" id="search-form2">
            <table class="search-form">
                    <tr>

                        <th>申请时间</th>
                        <td class="w300">
	                        <input name="start_time" id="start_time" type="text" class="text w100" value="<?php echo $start_time; ?>" readonly="readonly">
	                        <label class="add-on"><i class="icon-calendar"></i></label> – 
	                        <input name="end_time" id="end_time" type="text" class="text w100" value="<?php echo $end_time; ?>" readonly="readonly">
	                        <label class="add-on"><i class="icon-calendar"></i></label>
                        </td>
                        <th>状态</th>
                        <td class="w100">
                            <select id="status" name="status" class="w90">
                                <option value="">所有状态</option>
                                <?php if(is_array($state) || $state instanceof \think\Collection || $state instanceof \think\Paginator): if( count($state)==0 ) : echo "" ;else: foreach($state as $kk=>$vo): ?>
									<option <?php if($kk == \think\Request::instance()->post('status') and \think\Request::instance()->post('status') != ''): ?>selected<?php endif; ?> value="<?php echo $kk; ?>"><?php echo $vo; ?></option>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </td>
                        <th class="w80">订单编号</th>
                        <td class="w100"><input style="width: 90px;" class="text" type="text" value="<?php echo $_POST['order_sn']; ?>" name="order_sn"/></td>

                    </tr>
				<tr>

					<th>店铺</th>
					<td class="w80">
						<select name="store_id" class="w300">
							<option value="">请选择...</option>
							<?php if(is_array($stores) || $stores instanceof \think\Collection || $stores instanceof \think\Paginator): if( count($stores)==0 ) : echo "" ;else: foreach($stores as $key=>$store): ?>
								<option <?php if($store['store_id'] == \think\Request::instance()->param('store_id')): ?>selected="selected"<?php endif; ?> value="<?php echo $store['store_id']; ?>" ><?php echo $store['store_name']; ?></option>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</select>

					</td>

					<th style="width: 120px">店铺名搜索</th>
					<td class="w200"><input type="text" autocomplete="off" class="text w150"  name="store_title" value="<?php echo $store_name; ?>" placeholder="输入店铺名称" /></td>
					<td class="w70 tc"><label class="submit-border"><input type="submit" class="submit" value="搜索"/></label></td>
				</tr>
            </table>
            </form>
			<div id='prompt-box'></div>
            <div id="ajax_return">
			<table class="ncsc-default-table">
			    <thead>
			    <tr>
			        <th class="w100">订单编号</th>
			        <th class="w240">商品名称*数量</th>
			        <th class="w80">退款金额</th>
					<!--<th class="w80">退还预存款</th>-->
					<th class="w80">实际退款金额</th>
			        <th class="w80">服务类型</th>
			        <th class="w120">买家会员名</th>
			        <th class="w120">申请日期</th>
			        <th class="w80">操作状态</th>
			        <!--<th class="w80">平台确认</th>-->
			        <th class="w120">操作</th>
			    </tr>
			    </thead>
			    <tbody>
			    <?php if(count($list) > 0): if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$items): $mod = ($i % 2 );++$i;?>
				        <tr class="bd-line">
				            <td><a href="<?php echo U('order/detail',array('order_id'=>$items['order_id'])); ?>"><?php echo $items['order_sn']; ?></a></td>
				            <td><?php echo $goods_list[$items['goods_id']]; ?>*<?php echo $items['goods_num']; ?></td>
				            <td><?php echo $items['refund_money']; ?></td>
				            <!--<td><?php echo $items['refund_deposit']; ?></td>-->
							<td>
								<?php if($items['real_refund_money'] != null): ?>
									<?php echo $items['real_refund_money']; else: ?>
									未区分
								<?php endif; ?>
							</td>
				            <td><?php if($items[type] == 0): ?>仅退款<?php else: ?>退货退款<?php endif; ?></td>
				            <td><?php echo $user_list[$items[user_id]]; ?></td>
				            <td class=""><?php echo date('Y-m-d H:i',$items['addtime']); ?></td>
				            <td class="">
                                <?php if(($items[status] == 3) AND ($items[is_receive] == 0)): ?>
									同意
									<?php else: ?>
									<?php echo $state[$items[status]]; endif; ?>
                            </td>
				            <!--<td class="">-->
				            	<!--<?php if($items[status] == 3): ?>待处理<?php elseif($items[status] == 4): ?>已完成<?php else: ?>无<?php endif; ?>-->
				            <!--</td>-->
							<!-- -2用户取消退货 -1经销商拒绝 0已申请退货 1经销商确认 2商品寄回 3返厂检测 4退货失败（检测未通过）5退货成功 6申诉-->
				            <td class="nscs-table-handle">
				               <span>
				                  <a href="<?php echo U('Service/refund_info',array('id'=>$items['id'])); ?>" class="btn-bluejeans">
				                  <?php if($items[status] == 0): ?>
									  <i class="icon-edit"></i><p>处理</p>
									  <?php else: ?>
									  <i class="icon-eye-open"></i><p>查看</p>
								  <?php endif; ?>
				                  </a>
				               </span>
				               <?php if($items[status] == 2): ?>
				               <span>
			               		<a onclick="confirm_recive(<?php echo $items['id']; ?>)" href="javascript:void(0)" class="btn-bluejeans"><i class="icon-check-sign"></i><p>收货</p></a>
				               </span>

				               <?php endif; if($items[status] == 3): ?>
				               <span>
			               		<a href="<?php echo U('Service/refund_info',array('id'=>$items['id'])); ?>" class="btn-bluejeans"><i class="icon-check-sign"></i><p>检测</p></a>
				               </span>

								<?php endif; if($items[status] == 4): ?>
				               <span>
			               		<a href="javascript:void(0)" onclick="confirm_refund(<?php echo $items['id']; ?>)" class="btn-bluejeans"><i class="icon-check-sign"></i><p>退款</p></a>
				               </span>

								<?php endif; ?>
				            </td>
				        </tr>
				    <?php endforeach; endif; else: echo "" ;endif; else: ?>
			        <tr>
			            <td colspan="20">无记录</td>
			        </tr>
			    <?php endif; ?>
			    </tbody>
			    <tfoot>
			    <tr>
			        <td colspan="20">
			           <?php echo $page; ?>
			        </td>
			    </tr>
			    </tfoot>
			</table>
            </div>
        </div>
    </div>
</div>
<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
<script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
<script>
    $("input[name='store_title']").promptFun({
        table:'store',
        field:'store_name,store_id',
        column:'store_name',
        w:180,
        len:10,
        call:function () {
            $('.submit').click()
        }

    })
</script>
<script>

    function confirm_recive(id) {
        if(!id){
            layer.alert("id不能为空!",{icon:2});
            return;
        }



        layer.confirm('确认收货，该操作不可逆？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            location.href = "<?php echo U('Service/refund_info'); ?>?id="+id+'&status=3&action=edit';
        }, function () {
            layer.closeAll();
        });


    }


	function confirm_refund(id) {
	    if(!id){
            layer.alert("id不能为空!",{icon:2});
            return;
		}

        layer.confirm('确认退款，该操作不可逆？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            location.href = "<?php echo U('Service/confirm_refund_money'); ?>?id="+id+'&status=1';
        }, function () {
            layer.closeAll();
        });


    }
    $(document).ready(function(){
     	$('#start_time').layDate();
     	$('#end_time').layDate();
    });
</script>
</body>
</html>
