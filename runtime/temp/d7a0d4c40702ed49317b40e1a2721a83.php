<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:45:"./application/seller/new/order/ajaxindex.html";i:1552729777;}*/ ?>
<style>
    /*.ncsc-default-table tbody th span{*/
        /*margin-left: 15px !important;*/
        /*margin-right: 0px !important;*/
    /*}*/
</style>
<table class="ncsc-default-table order multiTable">
    <thead>
    <tr>
        <th style="width:30px;text-align:ceter">
            <input id="all" type="checkbox" value=""/>
        </th>
        <!--<th class="w10"></th>-->
        <th class="w100">店铺</th>
        <th colspan="2">商品</th>
        <th class="w100">单价（元）</th>
        <th class="w40">数量</th>
        <th class="w100"></th>
        <th class="w100">买家</th>
        <th class="w100">订单金额</th>
        <!--<th class="w90">交易状态</th>-->
        <th class="w90">订单状态</th>
        <th class="w120">交易操作</th>
    </tr>
    </thead>
    <?php if(empty($orderList) == true): ?>
        <tbody>
        <tr>
            <td colspan="20" class="norecord">
                <div class="warning-option"><i class="icon-warning-sign"></i><span>暂无符合条件的数据记录</span></div>
            </td>
        </tr>
        </tbody>
        <?php else: if(is_array($orderList) || $orderList instanceof \think\Collection || $orderList instanceof \think\Paginator): $i = 0; $__LIST__ = $orderList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;$goodsList = $goodsArr[$list['order_id']]; ?>
            <tbody>
            <tr>
                <td colspan="20" class="sep-row"></td>
            </tr>
            <tr>
                <th style="width:30px !important;text-align:center" rowspan="<?php echo count($goodsList)+1 ?>">
                    <input class="js-sign" type="checkbox" value="<?php echo $list['order_id']; ?>"/>
                </th>
                <th colspan="20">


                    <span>下单时间：<em class="goods-time"><?php echo $list['create_time']; ?></em></span>
                    <span>支付：<em class="goods-time"><?php echo $pay_status[$list[pay_status]]; ?></em></span>
                    <!--<span>订单状态：<em class="goods-time"><?php echo \think\Config::get('ORDER_STATUS')[$list[order_status]]; ?></em></span>-->
                    <span>发货：<em class="goods-time"><?php if($list['shipping_status'] == 1): ?>已发货<?php else: ?>未发货<?php endif; ?></em></span>


                    <span class="ml10">类型：<em class="goods-time">

				<?php if($list['prom_type'] == 4): ?>
					预约订单
                                        
                                <?php else: if($list['coupon_type'] == '5'): ?>
                                        回收券订单
                                    <?php elseif($list['coupon_type'] == '4'): ?>
                                        注册券订单
                                    <?php else: ?>
                                        普通订单
                                    <?php endif; endif; ?>
			</em></span>
                    <span class="ml10">编号：<em><?php echo $list['order_sn']; ?></em></span>
                    <span class="fr mr10"><a
                            href="<?php echo U('Order/order_print',array('ids'=>$list['order_id'].',','template'=>'picking')); ?>"
                            target="_blank" class="ncbtn-mini" title="打印配货单"><i class="icon-print"></i>打印配货单</a></span>
                </th>
            </tr>


            <?php if(is_array($goodsList) || $goodsList instanceof \think\Collection || $goodsList instanceof \think\Paginator): $k = 0; $__LIST__ = $goodsList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$good): $mod = ($k % 2 );++$k;if($k == 1): ?>
                    <!--  第一行 -->
                    <tr>

                        <td class="w100" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">
                            <span class="ml10 "><em style="color: #0ba4da;text-underline: #0ba4da;cursor: pointer"><?php echo $list['store_name']; ?></em><em
                                    style="color: #0ba4da">[<?php echo $list['store_id']; ?>]</em></span>
                        </td>
                        <td class="w100 bdl">
                            <div class="ncsc-goods-thumb"><a
                                    href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>"
                                    target="_blank"><img src="<?php echo goods_thum_images($good['goods_id'],240,240); ?>"></a></div>
                        </td>

                        <td class="tl">
                            <dl class="goods-name">
                                <dt><a target="_blank"
                                       href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>"><?php echo $good['goods_name']; ?></a>
                                </dt>
                            </dl>
                        </td>
<!--                        <td><p><?php echo $good['final_price']; ?></p></td>-->
                        <td><p><?php echo $good['goods_price']; ?></p></td>
                        <td><?php echo $good['goods_num']; ?></td>
                        <td style="font-size: 12px;">
                            <?php echo $good['status_tip']; ?>
                        </td>

                        <!-- S 合并TD -->
                        <td class="bdl" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">
                            <div class="buyer"><?php echo $list['consignee']; ?> <p member_id="5"></p>
                                <div class="buyer-info"><em></em>
                                    <div class="con">
                                        <h3><i></i><span>联系信息 </span></h3>
                                        <dl>
                                            <dt>姓名：</dt>
                                            <dd><?php echo $list['consignee']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>电话：</dt>
                                            <dd><?php echo $list['mobile']; ?></dd>
                                        </dl>
                                        <dl>
                                            <dt>地址：</dt>
                                            <dd><?php echo $list['address']; ?></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="bdl" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">
                            <p class="ncsc-order-amount"><?php echo $list['total_amount']; ?></p>
                            <?php if($list['coupon_price'] > 0): ?><p class="">
                                <?php if($list['coupon_type'] == '5'): ?>回收券<?php elseif($list['coupon_type'] == '4'): ?>注册券<?php else: ?>优惠<?php endif; ?>


                                <em style="color: #C00;"><?php echo $list['coupon_price']; ?></em>元
                            </p><?php endif; ?>
                            <p class="goods-freight">
                                <?php if(($list['shipping_price'] < 0.01)): ?>（免运费）
                                    <?php else: ?>
                                    邮费:<?php echo $list['shipping_price']; endif; ?>
                            </p>
                            <p class="goods-pay" title="支付方式：<?php echo $list['pay_name']; ?>"><?php echo $list['pay_name']; ?></p>
                        </td>

                        <td class="bdl bdr" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>"><p><em class="goods-time"><?php echo $list['status_tip']; ?>



                        </em>
                        </p>
                            <!-- 物流跟踪 -->
                            <p></p>
                        </td>
                        <!--<td class="bdl bdr" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>"><p><em class="goods-time"><?php echo \think\Config::get('ORDER_STATUS')[$list[order_status]]; ?></em>-->
                        <!--</p>-->
                            <!--&lt;!&ndash; 物流跟踪 &ndash;&gt;-->
                            <!--<p></p>-->
                        <!--</td>-->
                        <!-- 取消订单 -->
                        <td class="nscs-table-handle" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">
                            <span><a href="<?php echo U('order/detail',array('order_id'=>$list['order_id'])); ?>"
                                     class="ncbtn-mint"><i class="icon-search"></i><p>详情</p></a></span>
                            <?php if(($list['order_status'] == 5)): ?>
                                <!--<span><a href="<?php echo U('order/delete_order',array('order_id'=>$list['order_id'])); ?>"-->
                                         <!--class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>-->
                                <?php else: ?>
                                <!--<span><a href="javascript:void(0)" onclick="layer.alert('该订单不得删除!',{icon:2});" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>-->
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php else: ?>
                    <!--  非第一行 -->
                    <tr>

                        <td class="w70">
                            <div class="ncsc-goods-thumb"><a
                                    href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>"
                                    target="_blank">
                                <?php if(isset($good['pic'])): ?>
                                    <img src="<?php echo $good['pic']; ?>">
                                    <?php else: ?>
                                        <img src="<?php echo goods_thum_images($good['goods_id'],240,240); ?>">
                                <?php endif; ?>


                            </a></div>
                        </td>
                        <td class="tl">
                            <dl class="goods-name">
                                <dt><a target="_blank"
                                       href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>"><?php echo $good['goods_name']; ?></a>
                                </dt>
                            </dl>
                        </td>
                        <td><p><?php echo $good['final_price']; ?></p></td>
                        <td><?php echo $good['goods_num']; ?></td>
                        <td style="font-size: 12px;">
                            <?php echo $good['status_tip']; ?>
                        </td>

                    </tr>
                <?php endif; ?>

                <!--  第一行 -->
                <!--<tr>-->

                <!--<td class="w100"><span class="ml10 "><em style="color: #0ba4da;text-underline: #0ba4da;cursor: pointer"><?php echo $list['store_name']; ?></em><em style="color: #0ba4da">[<?php echo $list['store_id']; ?>]</em></span></td>-->
                <!--<td class="w100 bdl">-->
                <!--<div class="ncsc-goods-thumb"><a href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>" target="_blank"><img src="<?php echo goods_thum_images($good['goods_id'],240,240); ?>"  ></a></div></td>-->

                <!--<td class="tl">-->
                <!--<dl class="goods-name">-->
                <!--<dt><a target="_blank" href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$good['goods_id'])); ?>"><?php echo $good['goods_name']; ?></a></dt>-->
                <!--</dl>-->
                <!--</td>-->
                <!--<td><p><?php echo $good['goods_price']; ?></p></td>-->
                <!--<td><?php echo $good['goods_num']; ?></td>-->

                <!--&lt;!&ndash; S 合并TD &ndash;&gt;-->
                <!--<td class="bdl" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">-->
                <!--<div class="buyer"><?php echo $list['consignee']; ?> <p member_id="5"></p>-->
                <!--<div class="buyer-info"> <em></em>-->
                <!--<div class="con">-->
                <!--<h3><i></i><span>联系信息 </span></h3>-->
                <!--<dl>-->
                <!--<dt>姓名：</dt>-->
                <!--<dd><?php echo $list['consignee']; ?></dd>-->
                <!--</dl>-->
                <!--<dl>-->
                <!--<dt>电话：</dt>-->
                <!--<dd><?php echo $list['mobile']; ?></dd>-->
                <!--</dl>-->
                <!--<dl>-->
                <!--<dt>地址：</dt>-->
                <!--<dd><?php echo $list['address']; ?></dd>-->
                <!--</dl>-->
                <!--</div>-->
                <!--</div>-->
                <!--</div>-->
                <!--</td>-->
                <!--<td class="bdl" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">-->
                <!--<p class="ncsc-order-amount"><?php echo $list['total_amount']; ?></p>-->
                <!--<p class="goods-freight"><?php if(($list['shipping_price'] < 0.01)): ?>（免运费）<?php else: ?>邮费:<?php echo $list['shipping_price']; endif; ?></p>-->
                <!--<p class="goods-pay" title="支付方式：<?php echo $list['pay_name']; ?>"><?php echo $list['pay_name']; ?></p>-->
                <!--</td>-->

                <!--<td class="bdl bdr" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>"><p><em class="goods-time"><?php echo \think\Config::get('ORDER_STATUS')[$list[order_status]]; ?></em></p>-->
                <!--&lt;!&ndash; 物流跟踪 &ndash;&gt;-->
                <!--<p></p>-->
                <!--</td>-->
                <!--&lt;!&ndash; 取消订单 &ndash;&gt;-->
                <!--<td class="nscs-table-handle" rowspan="<?php echo count($goodsArr[$list['order_id']]); ?>">-->
                <!--<span><a href="<?php echo U('order/detail',array('order_id'=>$list['order_id'])); ?>" class="ncbtn-mint"><i class="icon-search"></i><p>详情</p></a></span>-->
                <!--<?php if(($list['order_status'] == 5)): ?>-->
                <!--<span><a href="<?php echo U('order/delete_order',array('order_id'=>$list['order_id'])); ?>" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>-->
                <!--<?php else: ?>-->
                <!--&lt;!&ndash;<span><a href="javascript:void(0)" onclick="layer.alert('该订单不得删除!',{icon:2});" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>&ndash;&gt;-->
                <!--<?php endif; ?>-->
                <!--</td>-->
                <!--</tr>-->

            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        <?php endforeach; endif; else: echo "" ;endif; ?>
        <tfoot>
        <tr>
            <td colspan="20">
                <div class="tDiv">
                    <div class="tDiv2">

                        <div class="fbutton">
                            <a href="javascript:void(0)" onclick="outputFun()" data-url="<?php echo U('Seller/order/ouput'); ?>">
                                <div class="add" title="批量导出">
                                    <span><i class="fa fa-plus"></i>批量导出</span>
                                </div>
                            </a>
                        </div>
                        <!--<div class="fbutton">-->
                        <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','0',null,delAll)" data-url="<?php echo U('Seller/goods/forbid'); ?>">-->
                        <!--<div class="add" title="批量下架">-->
                        <!--<span><i class="fa fa-plus"></i>批量下架</span>-->
                        <!--</div>-->
                        <!--</a>-->
                        <!--</div>-->
                        <!--<div class="fbutton">-->
                        <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','2')" data-url="<?php echo U('Seller/goods/forbid'); ?>">-->
                        <!--<div class="add" title="违规下架">-->
                        <!--<span><i class="fa fa-plus"></i>违规下架</span>-->
                        <!--</div>-->
                        <!--</a>-->
                        <!--</div>-->
                        <!--<div class="fbutton">-->
                        <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'delete',null,null,delAll)" data-url="<?php echo U('Seller/goods/del'); ?>">-->
                        <!--<div class="add" title="批量删除经销商">-->
                        <!--<span><i class="fa fa-plus"></i>批量删除</span>-->
                        <!--</div>-->
                        <!--</a>-->
                        <!--</div>-->
                    </div>
                    <div style="clear:both"></div>
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="20"><?php echo $page; ?></td>
        </tr>
        </tfoot>
    <?php endif; ?>
</table>
<script>
    var tmp = 0;
    $(".pagination  a").click(function () {
        var page = $(this).data('p');
        ajax_get_table('search-form2', page);
    });

    $("#all").click(function () {
        if (tmp == 0) {
            $("input[type='checkbox']").attr("checked", "true");
            tmp = 1;
        } else {
            $("input[type='checkbox']").removeAttr("checked");
            tmp = 0;
        }
    })


    function publicHandleAll_order(obj, field, val, type, call) {

        var url = $(obj).attr('data-url')
        var ids = '';
        $(".multiTable tbody input[type='checkbox']:checked ").each(function (i, o) {

            ids += $(o).val() + ',';
        });
        if (ids == '') {
            layer.msg('至少选择一项', {icon: 2, time: 2000});
            return false;
        }
        publicHandle(url, ids, field, val, type, call); //调用删除函数
    }
</script>