<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:48:"./application/seller/new/coupon/storeCoupon.html";i:1548757881;s:58:"/mnt/www/test/shop/application/seller/new/public/head.html";i:1544286252;s:58:"/mnt/www/test/shop/application/seller/new/public/left.html";i:1529392734;s:58:"/mnt/www/test/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<style>
  .ncsc-goods-default-pic .goodspic-uplaod .upload-thumb{width: 180px;height:180px; display: inline-block;}
  .ncsc-goods-default-pic .goodspic-uplaod .upload-thumb img{ height: 160px; width: 160px;}
  .ncsc-goods-default-pic .goodspic-uplaod .upload-thumb{line-height: 20px;margin-right: 6px;}
  .ncsc-goods-default-pic .goodspic-uplaod .upload-thumb:nth-child(5n){margin-right: 0;}
  .ncsc-goods-default-pic{display: inherit;}
  /*.ncsc-form-goods dl dd{width: 98%;}*/
  .text-warning {color: #8a6d3b;}a{ color:#3BAEDA}
  /*.ncsc-form-goods{padding: 10px;}*/
  .table-bordered {border: 1px solid #f4f4f4;}
  .table { width: 100%;max-width: 100%;margin-bottom: 20px;}
  ul.group-list {width: 96%;min-width: 1000px; margin: auto 5px;list-style: disc outside none;}
  ul.group-list li { white-space: nowrap;float: left; width: 150px; height: 25px;padding: 3px 5px;list-style-type: none;list-style-position: outside;border: 0px;margin: 0px;}
  .row .table-bordered td .btn,.row .table-bordered td img{vertical-align: middle;}
  .row .table-bordered td{padding: 8px;line-height: 1.42857143;}
  .table-bordered{width: 100%}
  .table-bordered tr td{border: 1px solid #f4f4f4;}
  .btn-success {color: #fff;background-color: #48CFAE;border-color: #398439 solid 1px;}
  .btn {display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;
    font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap; vertical-align: middle;
    -ms-touch-action: manipulation;touch-action: manipulation;cursor: pointer;-webkit-user-select: none;-moz-user-select: none;
    -ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent; border-radius: 4px;
  }
  .col-xs-8 {width: 66.66666667%;}
  .col-xs-4 {width: 33.33333333%;}
  .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {float: left;}
  .row .tab-pane h4{padding: 10px 0;}
  .row .tab-pane h4 input{vertical-align: middle;}
  .table-striped>tbody>tr:nth-of-type(odd) {background-color: #f9f9f9;}
  .ncap-form-default .title{border-bottom: 0}
  .ncap-form-default dl.row, .ncap-form-all dd.opt/*border-color: #F0F0F0;*/border: none;
  .ncap-form-default dl.row:hover, .ncap-form-all dd.opt:hover{border: none;box-shadow: inherit;}
  a:hover {color: #3BAEDA;text-decoration: none;}
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;line-height: 1.42857143;vertical-align: top; border-top: 1px solid #ddd;}
  ul.group-list{min-width: 100%;}
  input{vertical-align:middle;}
  .ncsc-form-goods h4{margin: 10px 0 10px 10px;}
  .clabackkj{background-color: #F5F5F5;border-bottom: solid 1px #E7E7E7;overflow: hidden;}
  .clabackkj h3{border-bottom: 0;display: inline-block;}
  .clabackkj .ncbtn{float: right;margin-right: 15px;height: 12px;line-height: 12px;}
  #tab_goods_images dl dd{width: 99%;}
  .alert-block{margin-top: 0;}
  select{min-width:120px;}
  .ui-tabs-nav{height:30px;padding-top:5px;background:#f5f5f5;}
  .ui-tabs-nav>li{float:left;height:30px;padding:0 10px;}
  .ui-tabs-nav>.ui-tabs-selected{background:#ddd;}
</style>
<!--以下是在线编辑器 代码 -->


<!--以上是在线编辑器 代码  end-->
<div class="ncsc-layout wrapper">
   <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
  <div id="layoutRight" class="ncsc-layout-right">
    <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>促销管理<i class="icon-angle-right"></i>类别折扣</div>
    <div class="main-content" id="mainContent">
      <div class="tabmenu-fixed-wrap">
        <div class="tabmenu">
          <ul class="tab pngFix">
            <li class="active"><a onclick="select_nav(this);" data-id="tab_tongyong">编辑信息</a></li>

          </ul>
        </div>
      </div>

      <div class="item-publish">
        <form method="post" onsubmit="return false;" id="Form">


          <div class="ncsc-form-goods active" id="tab_tongyong">
            <h3 id="demo1">折扣信息</h3>

            <?php if($model['catid'] == null): ?>
              <dl>
                <dt>指定栏目：</dt>
                <dd>
                  <select name="catid" required id="brand_id">
                    <option value="null">选择栏目</option>
                    <?php if(is_array($cats) || $cats instanceof \think\Collection || $cats instanceof \think\Paginator): if( count($cats)==0 ) : echo "" ;else: foreach($cats as $k=>$v): ?>
                      <option  value="<?php echo $v['id']; ?>" ><?php echo $v['name']; ?></option>

                    <?php endforeach; endif; else: echo "" ;endif; ?>
                  </select>

                </dd>
              </dl>
            <?php else: ?>
              <dl>
                <dt>指定栏目：</dt>
                <dd>
                  <select name="catid" required  id="brand_id">
                    <option value="null">选择栏目</option>
                    <?php if(is_array($cats) || $cats instanceof \think\Collection || $cats instanceof \think\Paginator): if( count($cats)==0 ) : echo "" ;else: foreach($cats as $k=>$v): ?>
                      <option  value="<?php echo $v['id']; ?>" <?php if($model['catid'] == $v['id']): ?> selected="selected"<?php endif; ?> ><?php echo $v['name']; ?></option>

                    <?php endforeach; endif; else: echo "" ;endif; ?>
                  </select>

                </dd>
              </dl>

            <?php endif; ?>



            <dl class="row">
              <dt class="tit">
                <label >类别折扣</label>
              </dt>
              <dd class="opt">
                <input type="text" onblur="clearNoNum(this)"  class="text w150" name="discount" id="discount" placeholder="折扣" value="<?php echo filter_int($model['discount']); ?>"/>
                <input type="text" onblur="this.value=parseInt(this.value)" class="text w150" name="discount_limit" id="discount_limit" placeholder="购买限制数量" value="<?php echo $model['discount_limit']; ?>"/>
                <input type="text" class="text w150" autocomplete="off" name="discount_start" id="discount_start" placeholder="开始时间" value="<?php echo $model['discount_start']; ?>"/>
                <input type="text" class="text w150" autocomplete="off" name="discount_end" id="discount_end" placeholder="结束时间" value="<?php echo $model['discount_end']; ?>"/>
                <p class="notic">折扣请输入0-100的整数，例如八折输入80即可，如不设置请填写0<br>截止时间需要大于开始时间<br/>指定时间内购买超过数量的商品，将无法购买</p>
              </dd>
            </dl>
            <dl>
              <dt>店铺筛选：</dt>
              <dd>
                <input id="select_store_list" type="hidden" value="<?php echo $model['select_store_list']; ?>">
                <div id="fun_store_box">
                  <div id="box"></div>
                </div>


              </dd>
            </dl>





              <?php if($model['discount_id'] != null): ?>
              <input type="hidden" name="discount_id" value="<?php echo $model['discount_id']; ?>">

              <?php endif; ?>
          </div>
          <div class="bottom tc hr32">
            <label class="submit-border">
              <input nctype="formSubmit" class="submit" id="submit" value="保存" type="submit">
            </label>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
<script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
<script type="text/javascript" src="/public/js/seller/store.fun.js"></script>
<script>

  var config = {
      form:'#Form'
  };

  if($('#select_store_list').val()){
      config.isHas = $('#select_store_list').val();
  }

  $('#fun_store_box').storeFun(config)

    $("input[name='store_title']").promptFun({
        table:'store',
        field:'store_name,store_id',
        column:'store_name',
        w:180,
        len:10,
        call:function () {

        }

    })
</script>
<script>
    /**
     * 根据用户选择的不同规格选项
     * 返回 不同的输入框选项
     */
    // function ajaxGetStoreList() {
    //
    //
    //     var id = $("input[name='id']").val();
    //     var type = $("input[name='type']:checked").val();
    //     $.ajax({
    //         type: 'POST',
    //         data: {type:type,id:id},
    //         url: "/index.php/Seller/Coupon/editStoreCoupon?act=getStoreList",
    //
    //         success: function (data) {
    //
    //             $("#ajax_store_return").empty().html(data);
    //
    //         }
    //     });
    // }
    //
    // $(function () {
    //     ajaxGetStoreList()
    // })
</script>
<script>

    /**
     * 控制折扣只能输入0<num<100数字，并且两位小数点
     *
     */
    function clearNoNum(obj){
        console.log(obj.value)
        obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
        obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
        obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
        obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');//只能输入两个小数
        if(obj.value !=""){
            //以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额

            var val = parseInt(obj.value);
            if(val>=0&&val<=100){
                obj.value = val
            }else{
                obj.value = 0;

            }

        }
    }


    $(function () {
        $(document).on("click", '#submit', function (e) {
            $('#submit').attr('disabled',true);
            verifyForm();
        })
    })


    function verifyForm(){



        $('span.err').hide();
        $.ajax({
            type: "POST",
            url: "<?php echo U('/Seller/Coupon/editStoreCoupon'); ?>",
            data: $('#Form').serialize(),
            async:false,
            dataType: "json",
            error: function () {
                layer.alert("服务器繁忙, 请联系管理员!");
            },
            success: function (data) {
                if (data.status == 1) {
                    layer.msg(data.msg,{icon: 1,time: 2000},function(){
                        location.href = "<?php echo U('Seller/Coupon/storeList'); ?>";
                    });
                } else {
                    $('#submit').attr('disabled',false);
                    // $.each(data.result, function (index, item) {
                    //     $('span.err').show();
                    //     $('#err_'+index).text(item);
                    // });
                    layer.alert(data.msg, {icon: 2});
                }
            }
        });
    }


    /** 以下是编辑时默认选中某个商品分类*/
    $(document).ready(function(){

        // 起始位置日历控件
        laydate.skin('molv');//选择肤色
        laydate({
            elem: '#discount_start',
            format: 'YYYY-MM-DD', // 分隔符可以任意定义，该例子表示只显示年月
            festival: true, //显示节日
            istime: true,
            choose: function(datas){ //选择日期完毕的回调
                // compare_time($('#discount_start').val(),$('#discount_end').val());
            }
        });

        // 结束位置日历控件
        laydate({
            elem: '#discount_end',
            format: 'YYYY-MM-DD', // 分隔符可以任意定义，该例子表示只显示年月
            festival: true, //显示节日
            istime: true,
            choose: function(datas){ //选择日期完毕的回调
                if($('#discount_start').val()>$('#discount_end').val()){
                    layer.msg('开始时间不能大于结束时间!', {
                        //icon: 1,   // 成功图标
                        time: 2000 //2秒关闭（如果不配置，默认是3秒）
                    });
                    return false;
                }

                // compare_time($('#discount_start').val(),$('#discount_end').val());
            }
        });

    });



</script>
</body>
</html>



