<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:43:"./application/admin/view/manage/manage.html";i:1550829061;s:60:"/mnt/www/test/shop/application/admin/view/public/layout.html";i:1534500726;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
<![endif]-->
<link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    html, body { overflow: visible;}
</style>
<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
<script type="text/javascript" src="/public/static/js/admin.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<script src="/public/js/myFormValidate.js"></script>
<script src="/public/js/myAjax2.js"></script>
<script src="/public/js/global.js"></script>
<script type="text/javascript">
function delfunc(obj){
	layer.confirm('确认删除？', {
		  btn: ['确定','取消'] //按钮
		}, function(){
			$.ajax({
				type : 'post',
				url : $(obj).attr('data-url'),
				data : {act:'del',del_id:$(obj).attr('data-id')},
				dataType : 'json',
				success : function(data){
					layer.closeAll();
					if(data.status==1){
                        $(obj).parent().parent().parent().html('');
						layer.msg('操作成功', {icon: 1});
					}else{
						layer.msg('删除失败', {icon: 2,time: 2000});
					}
				}
			})
		}, function(index){
			layer.close(index);
		}
	);
}

function delAll(obj,name){
	var a = [];
	$('input[name*='+name+']').each(function(i,o){
		if($(o).is(':checked')){
			a.push($(o).val());
		}
	})
	if(a.length == 0){
		layer.alert('请选择删除项', {icon: 2});
		return;
	}
	layer.confirm('确认删除？', {btn: ['确定','取消'] }, function(){
			$.ajax({
				type : 'get',
				url : $(obj).attr('data-url'),
				data : {act:'del',del_id:a},
				dataType : 'json',
				success : function(data){
					layer.closeAll();
					if(data == 1){
						layer.msg('操作成功', {icon: 1});
						$('input[name*='+name+']').each(function(i,o){
							if($(o).is(':checked')){
								$(o).parent().parent().remove();
							}
						})
					}else{
						layer.msg(data, {icon: 2,time: 2000});
					}
				}
			})
		}, function(index){
			layer.close(index);
			return false;// 取消
		}
	);	
}

//表格列表全选反选
$(document).ready(function(){
	$('.hDivBox .sign').click(function(){
	    var sign = $('#flexigrid > table>tbody>tr');
	   if($(this).parent().hasClass('trSelected')){
	       sign.each(function(){
	           $(this).removeClass('trSelected');
	       });
	       $(this).parent().removeClass('trSelected');
	   }else{
	       sign.each(function(){
	           $(this).addClass('trSelected');
	       });
	       $(this).parent().addClass('trSelected');
	   }
	})
});

//获取选中项
function getSelected(){
	var selectobj = $('.trSelected');
	var selectval = [];
    if(selectobj.length > 0){
        selectobj.each(function(){
        	selectval.push($(this).attr('data-id'));
        });
    }
    return selectval;
}

function selectAll(name,obj){
    $('input[name*='+name+']').prop('checked', $(obj).checked);
}   

function get_help(obj){

//	window.open("http://www.baidu.com/");
//	return false;
//
//    layer.open({
//        type: 2,
//        title: '帮助手册',
//        shadeClose: true,
//        shade: 0.3,
//        area: ['70%', '80%'],
//        content: $(obj).attr('data-url'), 
//    });
}

//
///**
// * 全选
// * @param obj
// */
//function checkAllSign(obj){
//    $(obj).toggleClass('trSelected');
//    if($(obj).hasClass('trSelected')){
//        $('#flexigrid > table>tbody >tr').addClass('trSelected');
//    }else{
//        $('#flexigrid > table>tbody >tr').removeClass('trSelected');
//    }
//}
/**
 * 批量公共操作（删，改）
 * @returns {boolean}
 */
function publicHandleAll(type){
    var ids = '';
    $('#flexigrid .trSelected').each(function(i,o){
//            ids.push($(o).data('id'));
        ids += $(o).data('id')+',';
    });
    if(ids == ''){
        layer.msg('至少选择一项', {icon: 2, time: 2000});
        return false;
    }
    publicHandle(ids,type); //调用删除函数
}
/**
 * 公共操作（删，改）
 * @param type
 * @returns {boolean}
 */
function publicHandle(ids,handle_type){
    layer.confirm('确认当前操作？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    url: $('#flexigrid').data('url'),
                    type:'post',
                    data:{ids:ids,type:handle_type},
                    dataType:'JSON',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1){
                            layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                location.href = data.url;
                            });
                        }else{
                            layer.msg(data.msg, {icon: 2, time: 3000});
                        }
                    }
                });
            }, function (index) {
                layer.close(index);
            }
    );
}
</script>
</head>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<style>
	i.required {
		font: 12px/16px Tahoma;
		color: #F30;
		vertical-align: middle;
		margin-right: 4px;
	}
</style>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
	<div class="fixed-bar">
		<div class="item-title"><a class="back" href="javascript:history.back();" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
			<div class="subject">
				<h3>经销商管理</h3>
				<h5></h5>
			</div>
			<ul class="tab-base nc-row">
				<li><a class="current" onclick="$('#tab_store').show();$('#tab_info').hide();$(this).parent().parent().find('a').removeClass('current');$(this).addClass('current');"><span><?php echo (isset($title) && ($title !== '')?$title:'账号信息'); ?></span></a></li>
				<!--<li><a onclick="$('#tab_info').show();$('#tab_store').hide();$(this).parent().parent().find('a').removeClass('current');$(this).addClass('current');"><span>经销商信息</span></a></li>-->
			</ul>
		</div>
	</div>
	<?php if($action=="detail"): ?>

		<div class="form-horizontal" >
			<div class="ncap-form-default" id="tab_store">
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>经销商账号</label>
					</dt>
					<dd class="opt">
						<input  class="input-txt valid" name="seller_account" readonly value="<?php echo $seller['seller_account']; ?>"  type="text">
						<span class="err" id="tip_seller_account" style="color:#F00; display:none;"></span>
						<p class="notic">一旦输入不可修改。</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>经销商名称</label>
					</dt>
					<dd class="opt">
						<input type="text" value="<?php echo $seller['seller_name']; ?>" readonly name="seller_name" class="input-txt">
						<span class="err" id="err_seller_name" style="color:#F00; display:none;"></span>
						<span class="err" id="success_seller_name" style="color:#718c00; display:none;"></span>
						<p class="notic">必须输入</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>所属等级</label>
					</dt>
					<dd class="opt">
						<select disabled name="seller_level" id="grade_id" style="width:200px;">

							<?php if(is_array($seller_levels) || $seller_levels instanceof \think\Collection || $seller_levels instanceof \think\Paginator): if( count($seller_levels)==0 ) : echo "" ;else: foreach($seller_levels as $k=>$v): ?>
								<option value="<?php echo $k; ?>" <?php if($k == $seller['seller_level']): ?>selected="selected"<?php endif; ?>>
								<?php echo $v; ?>
								</option>
							<?php endforeach; endif; else: echo "" ;endif; ?>
						</select>
						<span class="err"></span>
						<p class="notic"></p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>经销商客服电话</label>
					</dt>
					<dd class="opt">
						<input readonly type="text" value="<?php echo $seller['seller_contact']; ?>" name="seller_contact" class="input-txt">
						<span class="err" id="err_seller_contact" style="color:#F00; display:none;"></span>
						<p class="notic">请正确填写联系方式。eg.0898-3675775或者400-7007-333</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>百度商桥</label>
					</dt>
					<dd class="opt">
						<input type="text" maxlength="100" value="<?php echo $seller['qiao']; ?>" readonly name="qiao" class="input-txt">
						<span class="err" id="err_qiao" style="color:#F00; display:none;"></span>
						<span class="err" id="success_qiao" style="color:#718c00; display:none;"></span>
						<p class="notic">必须输入</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>分期相关</label>
					</dt>
					<dd class="opt">
						<input type="text" value="<?php echo $seller['installment']; ?>" readonly  name="installment" class="input-txt" size="250">
						<span class="err" id="err_installment" style="color:#F00; display:none;"></span>
						<span class="err" id="success_installment" style="color:#718c00; display:none;"></span>
						<p class="notic">配置规则为：支付宝账号|公司名称</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>快钱相关</label>
					</dt>
					<dd class="opt">
						<input type="text" value="<?php echo $seller['quick']; ?>" readonly  name="quick" class="input-txt" size="450">
						<span class="err" id="err_quick" style="color:#F00; display:none;"></span>
						<span class="err" id="success_quick" style="color:#718c00; display:none;"></span>
						<p class="notic">配置规则为：经销商编号|经销商快钱邮箱|公司名称</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>银联分期商户号</label>
					</dt>
					<dd class="opt">
						<input type="text" maxlength="50" value="<?php echo $seller['union']; ?>" readonly  name="union" class="input-txt" size="450">
						<span class="err" id="err_union" style="color:#F00; display:none;"></span>
						<span class="err" id="success_union" style="color:#718c00; display:none;"></span>
						<p class="notic">配置规则为：</p>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label>服务介绍</label>
					</dt>
					<dd class="opt">
						<div><?php echo $seller['desc']; ?></div>
					</dd>
				</dl>
				<dl class="row">
					<dt class="tit">
						<label><i class="required">*</i>经销商logo</label>
					</dt>
					<dd class="opt">
						<a target="_blank" href="<?php echo (isset($seller['seller_logo']) && ($seller['seller_logo'] !== '')?$seller['seller_logo']:'javascript:void(0)'); ?>">
							<?php if($seller[seller_logo] == ''): ?>
								<img id="seller_logo" src="/public/images/not_adv.jpg" height="120">
								<?php else: ?>
								<img id="seller_logo" src="<?php echo $seller['seller_logo']; ?>" height="120">
							<?php endif; ?>
						</a>
						<!--<input type="hidden" name="seller_logo" value="<?php echo $seller['seller_logo']; ?>">-->
						<!--<input type="button" class="btn btn-default" onClick="upload_img('seller_logo')"  value="上传图片"/>-->
					</dd>
				</dl>


				<dl class="row">
					<dt class="tit">
						<label>状态</label>
					</dt>
					<dd class="opt">
						<div class="onoff">
							<?php if($seller[enabled] == 1): ?>
								<label for="seller_state1" onclick="$('#close_reason').hide();" class="cb-enable <?php if($seller[enabled] == 1): ?>selected<?php endif; ?>">开启</label>
								<?php else: ?>
									<label for="seller_state0" onclick="$('#close_reason').show();" class="cb-disable <?php if($seller[enabled] == 0): ?>selected<?php endif; ?>">关闭</label>
							<?php endif; ?>

						</div>
						<p class="notic"></p>
					</dd>
				</dl>
				<div class="bot"><a href="javascript:history.back();"  class="ncap-btn-big ncap-btn-green">返回</a>

			</div>

		</div>


		<?php else: ?>

			<form class="form-horizontal" action="/admin/Manage/manage" method="post" id="seller_info">
				<div class="ncap-form-default" id="tab_store">
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>经销商账号</label>
						</dt>
						<dd class="opt">
							<input class="input-txt valid" onblur="checkPhone()" name="seller_account" <?php if($seller['seller_account']): ?>readonly<?php endif; ?> value="<?php echo $seller['seller_account']; ?>"  type="text">
							<span class="err" id="tip_seller_account" style="color:#F00; display:none;"></span>
							<p class="notic">一旦输入不可修改。</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>经销商名称</label>
						</dt>
						<dd class="opt">
							<input type="text" maxlength="50" value="<?php echo $seller['seller_name']; ?>" name="seller_name" class="input-txt">
							<span class="err" id="err_seller_name" style="color:#F00; display:none;"></span>
							<p class="notic">必须输入</p>
						</dd>
					</dl>
					<?php if($seller['seller_id'] > 0): ?>
						<dl class="row">
							<dt class="tit">
								<label>更改密码</label>
							</dt>
							<dd class="opt">
								<input type="password"   name="password" class="input-txt">
								<p class="notic">请不要设置过于简单的密码,密码应该由数字和字母或特殊符号组成</p>
							</dd>
						</dl>
					<?php endif; ?>
					<dl class="row">
						<dt class="tit">
							<label>所属等级</label>
						</dt>
						<dd class="opt">
							<select name="seller_level" id="grade_id" style="width:200px;">

								<?php if(is_array($seller_levels) || $seller_levels instanceof \think\Collection || $seller_levels instanceof \think\Paginator): if( count($seller_levels)==0 ) : echo "" ;else: foreach($seller_levels as $k=>$v): ?>
									<option value="<?php echo $k; ?>" <?php if($k == $seller['seller_level']): ?>selected="selected"<?php endif; ?>>
									<?php echo $v; ?>
									</option>
								<?php endforeach; endif; else: echo "" ;endif; ?>
							</select>
							<span class="err"></span>
							<p class="notic"></p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>经销商客服电话</label>
						</dt>
						<dd class="opt">
							<input  maxlength="20" type="text" value="<?php echo $seller['seller_contact']; ?>" name="seller_contact" class="input-txt">
							<span class="err" id="err_seller_contact" style="color:#F00; display:none;"></span>
							<p class="notic">请正确填写手机/固话/400电话。eg.13088888888,0898-88888888,400-888-8888</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>百度商桥</label>
						</dt>
						<dd class="opt">
							<input type="text" value="<?php echo $seller['qiao']; ?>" maxlength="100"  name="qiao" class="input-txt">
							<span class="err" id="err_qiao" style="color:#F00; display:none;"></span>
							<span class="err" id="success_qiao" style="color:#718c00; display:none;"></span>
							<p class="notic">必须输入</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label>分期相关</label>
						</dt>
						<dd class="opt">
							<input type="text"  maxlength="100" value="<?php echo $seller['installment']; ?>"  name="installment" class="input-txt" size="250">
							<span class="err" id="err_installment" style="color:#F00; display:none;"></span>
							<span class="err" id="success_installment" style="color:#718c00; display:none;"></span>
							<p class="notic">配置规则为：支付宝账号|公司名称</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>快钱相关</label>
						</dt>
						<dd class="opt">
							<input type="text" maxlength="100" value="<?php echo $seller['quick']; ?>"  name="quick" class="input-txt" size="450">
							<span class="err" id="err_quick" style="color:#F00; display:none;"></span>
							<span class="err" id="success_quick" style="color:#718c00; display:none;"></span>
							<p class="notic">配置规则为：经销商编号|经销商快钱邮箱|公司名称</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label>银联分期商户号</label>
						</dt>
						<dd class="opt">
							<input type="text" maxlength="50" value="<?php echo $seller['union']; ?>" name="union" class="input-txt" size="450">
							<span class="err" id="err_union" style="color:#F00; display:none;"></span>
							<span class="err" id="success_union" style="color:#718c00; display:none;"></span>
							<p class="notic">配置规则为：</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label>服务介绍</label>
						</dt>
						<dd class="opt">
							<textarea rows="20" cols="40" name="desc"><?php echo $seller['desc']; ?></textarea>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label><i class="required">*</i>经销商logo</label>
						</dt>
						<dd class="opt">
							<a href="#">
								<?php if($seller[seller_logo] == ''): ?>
									<img id="seller_logo" src="/public/images/not_adv.jpg" height="120">
									<?php else: ?>
									<img id="seller_logo" src="<?php echo $seller['seller_logo']; ?>" height="120">
								<?php endif; ?>
							</a>
							<input type="hidden" name="seller_logo" value="<?php echo $seller['seller_logo']; ?>">
							<input type="button" class="btn btn-default" onClick="upload_img('seller_logo')"  value="上传图片"/>
							<p class="notic">高度不得高于48px，宽度不限</p>
						</dd>
					</dl>


					<dl class="row">
						<dt class="tit">
							<label>状态</label>
						</dt>
						<dd class="opt">
							<div class="onoff">
								<label for="seller_state1" onclick="$('#close_reason').hide();" class="cb-enable <?php if($seller[enabled] == 1): ?>selected<?php endif; ?>">开启</label>
								<label for="seller_state0" onclick="$('#close_reason').show();" class="cb-disable <?php if($seller[enabled] == 0): ?>selected<?php endif; ?>">关闭</label>
								<input id="seller_state1" name="enabled" <?php if($seller[enabled] == 1): ?>checked="checked"<?php endif; ?> value="1" type="radio">
								<input id="seller_state0" name="enabled" <?php if($seller[enabled] == 0): ?>checked="checked"<?php endif; ?> value="0" type="radio">
							</div>
							<p class="notic"></p>
						</dd>
					</dl>

					<div class="bot"><a href="JavaScript:void(0);" onclick="actsubmit();" class="ncap-btn-big ncap-btn-green">确认提交</a> &nbsp;<a href="JavaScript:void(0);" onclick="javascript:location.reload();" class="ncap-btn-big">刷新</a></div>

					<input type="hidden" name="user_id" value="<?php echo $seller['user_id']; ?>">
					<input type="hidden" name="seller_id" value="<?php echo $seller['seller_id']; ?>">

				</div>

			</form>

	<?php endif; ?>

</div>
<script type="text/javascript">
	$(document).ready(function(){

		// 加载默认选中
		var province_id = $('#province').val();
		if(province_id != 0){
			$('#province').trigger('blur');
		}
		var bank_province_id = $('#bank_province').val();
		if(bank_province_id != 0){
			$('#bank_province').trigger('blur');
		}
		// 加载默认选中
		<?php if($apply[company_district] > 0): ?>
				set_area('<?php echo $apply[company_city]; ?>','<?php echo $apply[company_district]; ?>');
		<?php endif; ?>
	});

	$('#store_end_time').layDate();
    $('#business_date_start').layDate();
    $('#business_date_end').layDate();

	/**
	 * 获取地区
	 */
	function set_area(parent_id,selected){
		if(parent_id <= 0){
			return;
		}
		$('#district').empty().css('display','inline');
		var url = '/index.php?m=Home&c=Api&a=getRegion&level=3&parent_id='+ parent_id+"&selected="+selected;
		$.ajax({
			type : "GET",
			url  : url,
			error: function(request) {
				layer.alert('服务器繁忙, 请联系管理员', {icon: 2});
				return;
			},
			success: function(v) {
				v = '<option>选择区域</option>'+ v;
				$('#district').empty().html(v);
			}
		});
	}
	var flag = true;

	function actsubmit(){
        if(!$('input[name=seller_name]').val()){
            layer.msg("经销商名称不能为空", {icon: 2,time: 2000});
            return;
        }

        if(!$('input[name=qiao]').val()){
            layer.msg("百度商桥不能为空", {icon: 2,time: 2000});
            return;
        }

        var qiao = $('input[name=qiao]').val();
        if(qiao.length>99){
            layer.msg("百度商桥值超过100限制", {icon: 2,time: 2000});
            return;
        }

        if(!$('input[name=quick]').val()){
            layer.msg("快钱设置不能为空", {icon: 2,time: 2000});
            return;
        }


        if(!$('input[name=installment]').val()){
            layer.msg("分期设置不能为空", {icon: 2,time: 2000});
            return;
        }




        if(!$('input[name=seller_account]').val()||!checkMobile($('input[name=seller_account]').val())){
            layer.msg("经销商账号必须为手机号", {icon: 2,time: 2000});
            return;
        }
        var seller_contct = $('input[name=seller_contact]').val();
        if(!seller_contct||(!checkMobile(seller_contct)&&!checkTelphone(seller_contct))){
            layer.msg("经销商客服电话不合要求", {icon: 2,time: 2000});
            return;
        }
        if(!$('input[name=seller_logo]').val()){
            layer.msg("经销商logo不能为空", {icon: 2,time: 2000});
            return;
        }
        if(!$('select[name=seller_level]').val()){
            layer.msg("经销商等级不能为空", {icon: 2,time: 2000});
            return;
        }

        if(!$('input[name=enabled]').val()){
            layer.msg("经销商状态不能为空", {icon: 2,time: 2000});
            return;
        }

        $('#seller_info').submit();
	}

    function changeBusinessDate(){
        var v = document.getElementById("3");
        if (v.checked==true) {
            document.getElementById("3").value="Y";
            document.getElementById("business_date_end").value="长期";
            document.getElementById("business_date_end").realvalue="";
            document.getElementById("business_date_end").disabled = true;
        } else {
            document.getElementById("3").value="N";
            document.getElementById("business_date_end").disabled = false;
        }
    }

	var tmp_type = '';
	function upload_img(cert_type){
		tmp_type = cert_type;
		//限制高度为48px,宽度无限
		GetUploadify(1,'store','seller','callback',100000,48);
	}

	function callback(img_str){
		$('#'+tmp_type).attr('src',img_str);
		$('input[name='+tmp_type+']').val(img_str);
	}


	//检查手机号码
	function checkPhone() {
	    console.log(222);
		$.post("<?php echo U('Manage/checkPhone'); ?>",{phone:$("input[name='seller_account']").val()},function (result) {
					res = JSON.parse(result)
				if(res.status==1){
                    $('#tip_seller_account').html('手机号码可用').show().css('color','#44b549')
				}else{
                    $("input[name='seller_account']").val(null)
                    $('#tip_seller_account').html(res.msg).show().css('color','#F00')
				}


        })
    }
</script>
</body>
</html>