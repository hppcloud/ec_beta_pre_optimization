<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:52:"./application/seller/new/goods/ajax_spec_select.html";i:1552894231;}*/ ?>

<table class="table table-bordered" id="goods_spec_table1">
    <tr>
        <td colspan="2"><b>商品规格:</b></td>
    </tr>
    <?php if(is_array($specList) || $specList instanceof \think\Collection || $specList instanceof \think\Paginator): if( count($specList)==0 ) : echo "" ;else: foreach($specList as $k=>$vo): ?>
    <tr>
        <td><?php echo $vo[name]; ?>:</td>
        <td >
            <!---->
            <!---->
            <!--ondrop="drop(event)" ondragover="allowDrop(event)"-->
            <ul data-sortable id="dragBox<?php echo $k; ?>" class="dragBox" >
                <?php if(is_array($vo[spec_item]) || $vo[spec_item] instanceof \think\Collection || $vo[spec_item] instanceof \think\Paginator): if( count($vo[spec_item])==0 ) : echo "" ;else: foreach($vo[spec_item] as $k2=>$vo2): ?>
                    <!---->
                    <!---->
                    <!--draggable="true" ondragstart="drag(event)"-->
                    <li style="display: inline-block;" class="draggable-element"  id="spec_item_<?php echo $k1; ?>_<?php echo $k2; ?>">
                        <button style="margin-bottom: 15px" id="spec_item_button_<?php echo $k2; ?>" type="button" data-spec_item="<?php echo $vo2; ?>" data-spec_id='<?php echo $vo[id]; ?>' data-item_id='<?php echo $k2; ?>' class="<?php echo $k2; ?> btn drag-btn <?php
                             if(in_array($k2,$items_ids))
                                    echo 'btn-success';
                             else
                                echo 'btn-default';
                             ?>" >
                            <?php echo $vo2; ?>
                        </button>
                        <?php if($isf == '0'): else: ?>
                        <div class="ys-btn-close" data-item-id="<?php echo $k2; ?>">×</div>
                        <?php endif; if(strstr($vo[name],'颜色')): ?>
                            <img width="35" height="35" src="<?php echo (isset($specImageList[$k2]) && ($specImageList[$k2] !== '')?$specImageList[$k2]:'/public/images/add-button.jpg'); ?>" id="item_img_<?php echo $k2; ?>" onclick="GetUploadify0('<?php echo $k2; ?>');"/>
                            <input type="hidden" name="item_img[<?php echo $k2; ?>]" value="<?php echo $specImageList[$k2]; ?>" />

                            <input onblur="upSpecColor(this,<?php echo $k2; ?>)"  type="text" class="w60" placeholder="颜色 eg.#fffff" name="item_color[<?php echo $k2; ?>]" value="<?php echo $specColorList[$k2]; ?>" />


                        <?php endif; ?>
                    </li>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </ul>

            <?php if($isf == '0'): else: ?>
            <input type="text" maxlength="200" data-spec_id="<?php echo $vo[id]; ?>" name="spec_item" placeholder="规格值名称" class="form-control" style="width:80px;vertical-align: middle;display: initial;">
            &nbsp;&nbsp;
            <a href="javascript:void(0);"  onclick="addSpecItem(this)">添加</a>
            <?php endif; ?>
        </td>
    </tr>                                    
    <?php endforeach; endif; else: echo "" ;endif; ?>    
</table>
<table class="table table-bordered" id="goods_spec_table">
    <tr>
        <td colspan="2">
            <div class="alert mt15 mb5"><strong>操作提示：</strong>
                <ul>
                    <li style="color:red">属性值设置规则</li>
                    <li>1、价格/库存/sku三个值任何一个参数有值，则代表该规格有效并且会储存。</li>
                    <li>2、如果某行记录全部为空（注意不是0）,则代表该行记录不存在</li>

                </ul>
            </div>
        </td>
    </tr>
</table>
<div class="batch-fill" id="batch-fill">
    <p class="">批量填充　:　
        <input style="width: 100px;" type="text" id="batch-fill-text1" />　　
        <input style="width: 100px;" type="text" id="batch-fill-text2" />
        <input style="width: 100px;" type="text" <?php if($isShare == 1): ?>readonly<?php endif; ?> id="batch-fill-text3" />　　
        <input style="width: 100px;" type="text" id="batch-fill-text4" />
        <!--<label><input id="taggle_all_share" type="checkbox" />共享库存</label>-->

    </p>

    <div class="ok-btn" style="margin-left: 30px">确定</div>
</div>
<input type="hidden" name="goodsid" value="<?php echo (\think\Request::instance()->get('goods_id') ?: ''); ?>">
<div id="goods_spec_table2"> <!--ajax 返回 规格对应的库存--> </div>


<script>

    $('#taggle_all_share').change(function (e) {

        if($('#taggle_all_share').is(':checked')){
            $('.js-input-share').attr("checked","checked");
        }else{
            $('.js-input-share').attr("checked",false);
        }



    })

    function upSpecColor(obj,id) {
        var specColor = obj.value;

        $.post('/index.php/Seller/Goods/changeSpecColor',{spec_color:specColor,spec_color_id:id,goods_id:$("input[name='goodsid']").val()});

    }


    $(function  () {

        <?php if(is_array($specList) || $specList instanceof \think\Collection || $specList instanceof \think\Paginator): if( count($specList)==0 ) : echo "" ;else: foreach($specList as $k=>$vo): ?>
        var el<?php echo $k; ?> = document.getElementById('dragBox<?php echo $k; ?>');
        new Sortable(el<?php echo $k; ?>,{sort: true,onEnd:function (e) {
            console.log(e.target.id,$('#'+e.target.id));

            var pid = e.target.id;
            parent = $('#'+pid);


            var specItemArr = [];
            parent.find('.drag-btn').each(function(idx,ele){
                console.log(ele,idx)
                var specItemId = $(ele).attr('data-item_id');//获取绑定的id
                specItemArr.push(specItemId+'-'+idx);//按照真正的排序来
            })

            refreshSpecItemSort(specItemArr)


            // return;

        }});
        <?php endforeach; endif; else: echo "" ;endif; ?>
    })


    /**
     * 每次拖动后需要改变该规格的各种可选值的排序
     * 就用在数组里面的idx就行了
     */
    function refreshSpecItemSort(specItemArr) {
        console.log(specItemArr)
        $.ajax({
            type: 'POST',
            data: {str:specItemArr.join(',')},
            dataType: 'json',
            url: "/index.php/Seller/Goods/changeSpecItemSort",
            success: function (data) {
                ajaxGetSpecInput();
            }
        });
    }


</script>
<script>
    //数字输入框判断
    var $_batchFill=$('#batch-fill');
    $_batchFill.find('input[type="text"]').blur(function () {
        var val=$(this).val();
        if(isNaN(val)){
            $(this).val('0');
        }else{
            if(val<0){
                $(this).val('0');
            }else{
                val=Math.round(val*100)/100;
                $(this).val(val);
            }
        }
    });
    $_batchFill.find('.ok-btn').click(function (){
        var $_goodSpec=$('#goods_spec_table2');
        $_goodSpec.find('.batch-fill-text1').val($('#batch-fill-text1').val());
        $_goodSpec.find('.batch-fill-text2').val($('#batch-fill-text2').val());
        $_goodSpec.find('.batch-fill-text3').val($('#batch-fill-text3').val());
        $_goodSpec.find('.batch-fill-text4').val($('#batch-fill-text4').val());
    })



    //删除规格值
    $(function () {
        $(document).on('click', '.spec_item_del', function () {
            $(this).parents('tr').remove();
        })
    })
    $(function () {
        $(document).on('click', '.delete_item', function () {
            $(this).parent().siblings().find('input').remove();
            $(this).parent().remove();
        })
    })
    $(function () {
        $(".ys-btn-close").click(function () {
            var spec_item_id = $(this).attr('data-item-id');//106
            var button = $('#spec_item_button_'+spec_item_id);
            var spec_id = button.attr('data-spec_id');//14
            var spec_item = button.attr('data-spec_item');//云雾灰色
            $.ajax({
                type: 'POST',
                data: {'spec_item': spec_item, 'spec_id': spec_id},
                dataType: 'json',
                url: "/index.php/Seller/Goods/delSpecItem",
                success: function (data) {
                    if (data.status < 0) {
                        layer.alert(data.msg, {icon: 2});
                    } else {
                        ajaxGetSpecAttr();
                    }
                }
            });
        });
    })
    // 添加规格
    function addSpecItem(obj){
        var spec_item = $(obj).siblings('input[name="spec_item"]').val();
        spec_item = $.trim(spec_item);
        var spec_id = $(obj).siblings('input[name="spec_item"]').data('spec_id');
        if(spec_item.length == 0)
        {
            layer.alert('请输入规格值', {icon: 2});  //alert('删除失败');
            return false;
        }
        
        $.ajax({
                type:'POST',
                data:{'spec_item':spec_item,'spec_id':spec_id},
                dataType:'json',
                url:"/index.php?m=Seller&c=Goods&a=addSpecItem",
                success:function(data){                            
                       if(data.status < 0)
                       {
                           layer.alert(data.msg, {icon: 2}); 
                       }else{
                           ajaxGetSpecAttr();
                       }                           
                }
        });

    }

    // 上传规格图片
    function GetUploadify0(k){        
        cur_item_id = k; //当前规格图片id 声明成全局 供后面回调函数调用
        GetUploadify3(1,'','goods','call_back3');
    }
    
    
    // 上传规格图片成功回调函数
    function call_back3(fileurl_tmp){
        $("#item_img_"+cur_item_id).attr('src',fileurl_tmp); //  修改图片的路径
        $("input[name='item_img["+cur_item_id+"]']").val(fileurl_tmp); // 输入框保存一下 方便提交
//        $(".spec_img_"+cur_item_id).attr('value',fileurl_tmp);
    }
    
   // 按钮切换 class
    $("#ajax_spec_data button").click(function () {
        var goods_id = $("input[name='goods_id']").val();
        var button = $(this);
        if (button.hasClass('btn-success')) {
            button.removeClass('btn-success');
            button.addClass('btn-default');
        }
        else {
            button.removeClass('btn-default');
            button.addClass('btn-success');
        }
        ajaxGetSpecInput();
    });
    

/**
*  点击商品规格处罚 下面输入框显示
*/
function ajaxGetSpecInput() {
    var spec_arr = {};// 用户选择的规格数组
    // 选中了哪些属性
    $("#goods_spec_table1  button").each(function () {
        if ($(this).hasClass('btn-success')) {
            var spec_id = $(this).data('spec_id');
            var item_id = $(this).data('item_id');
            if (!spec_arr.hasOwnProperty(spec_id))
                spec_arr[spec_id] = [];
            spec_arr[spec_id].push(item_id);
        }
    });
    // console.log(spec_arr);
    if (spec_arr.length > 4) {
        layer.msg('商品至多不能超过4种规格',{icon:2});
        $(this).removeClass('btn-success');
        $(this).addClass('btn-default');
        return false;
    }


    ajaxGetSpecInput2(spec_arr); // 显示下面的输入框

}


    
/**
* 根据用户选择的不同规格选项 
* 返回 不同的输入框选项
*/
function ajaxGetSpecInput2(spec_arr) {
    var goods_id = $("input[name='goods_id']").val();
    $.ajax({
        type: 'POST',
        data: {spec_arr: spec_arr, goods_id: goods_id},
        url: "/index.php/Seller/Goods/ajaxGetSpecInput",
        dataType:'json',
        success: function (data) {
            if(data.status == 0){
                layer.alert(data.msg);
            }else{
                $("#goods_spec_table2").empty().html(data.result);
            }
            hbdyg();  // 合并单元格
        }
    });
}
    
 // 合并单元格
 function hbdyg() {
            var tab = document.getElementById("spec_input_tab"); //要合并的tableID
            var maxCol = 2, val, count, start;  //maxCol：合并单元格作用到多少列  
            if (tab != null) {
                for (var col = maxCol - 1; col >= 0; col--) {
                    count = 1;
                    val = "";
                    for (var i = 0; i < tab.rows.length; i++) {
                        if (val == tab.rows[i].cells[col].innerHTML) {
                            count++;
                        } else {
                            if (count > 1) { //合并
                                start = i - count;
                                tab.rows[start].cells[col].rowSpan = count;
                                for (var j = start + 1; j < i; j++) {
                                    tab.rows[j].cells[col].style.display = "none";
                                }
                                count = 1;
                            }
                            val = tab.rows[i].cells[col].innerHTML;
                        }
                    }
                    if (count > 1) { //合并，最后几行相同的情况下
                        start = i - count;
                        tab.rows[start].cells[col].rowSpan = count;
                        for (var j = start + 1; j < i; j++) {
                            tab.rows[j].cells[col].style.display = "none";
                        }
                    }
                }
            }
        }
</script> 