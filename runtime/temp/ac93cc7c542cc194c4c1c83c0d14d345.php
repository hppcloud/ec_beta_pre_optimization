<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:44:"./application/seller/new/admin/_visitor.html";i:1547373299;s:58:"/mnt/www/test/shop/application/seller/new/public/head.html";i:1544286252;s:58:"/mnt/www/test/shop/application/seller/new/public/left.html";i:1529392734;s:58:"/mnt/www/test/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<div class="ncsc-layout wrapper">
     <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
    <div id="layoutRight" class="ncsc-layout-right">
        <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>账号<i class="icon-angle-right"></i>
            <?php if(empty($user) || (($user instanceof \think\Collection || $user instanceof \think\Paginator ) && $user->isEmpty())): ?>
           新增
                <?php else: ?>
                    编辑

            <?php endif; ?>

        </div>
        <div class="main-content" id="mainContent">
            <div class="item-publish">
                <form method="post" id="adminHandle" >
                    <input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>">
                    
                    <div class="ncsc-form-goods">
                        <h3 id="demo1"> <?php if(empty($user) || (($user instanceof \think\Collection || $user instanceof \think\Paginator ) && $user->isEmpty())): ?>
                            新增
                            <?php else: ?>
                                编辑

                        <?php endif; ?>体验账号</h3>

                        <?php if(empty($user) || (($user instanceof \think\Collection || $user instanceof \think\Paginator ) && $user->isEmpty())): ?>

                            <dl class="search-form">
                                <dt><i class="required">*</i> 店铺</dt>
				<dd>
                                    <select name="store_id" class="w200">
                                            <option value="">请选择...</option>
                                        <!--验证方式。email=>邮箱验证,sn=>邀请码-->
                                            <?php if(is_array($stores) || $stores instanceof \think\Collection || $stores instanceof \think\Paginator): if( count($stores)==0 ) : echo "" ;else: foreach($stores as $key=>$store): ?>

                                                    <option data-channel="<?php echo $store['validate_channel']; ?>" data-emails="<?php echo $store['store_email']; ?>" <?php if($store['store_id'] == \think\Request::instance()->get('store_id')): ?>selected="selected"<?php endif; ?>  value="<?php echo $store['store_id']; ?>"><?php echo $store['store_name']; ?></option>
                                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>

                            店铺名搜索
                            <input type="text" autocomplete="off" class="text w150"  name="store_title" value="<?php echo $store_name; ?>" placeholder="输入店铺名称" />


                               <!--<label class="submit-border"> <input type="button"  value="搜索" class="submit"/></label>-->
                                </dd>		
			     </dl>

                            <div id='prompt-box'></div>
                            <dl id="email-box">
                                <dt><i class="required">*</i> 邮箱：</dt>
                                <dd>
                                    <?php if($user[email]): ?>
                                        <span><?php echo $user['email']; ?></span>
                                        <?php else: ?>
                                        <input type="text" name="email" id="seller_name" class="text w400">
                                    <?php endif; ?>
                                    <p class="hint">必须为邮箱.可选邮箱如下 <span style="color: red" id="isAllowEmail"></span></p>
                                </dd>
                            </dl>
                            <dl id="mobile-box">
                                <dt><i class="required">*</i> 手机号码：</dt>
                                <dd>
                                    <?php if($user[mobile]): ?>
                                        <span><?php echo $user['mobile']; ?></span>
                                        <?php else: ?>
                                        <input type="text" name="mobile" id="" class="text w400">
                                    <?php endif; ?>
                                    <p class="hint">必须为手机号</p>
                                </dd>
                            </dl>
                            <dl>
                                <dt><i class="required">*</i> 用户密码：</dt>
                                <dd>
                                    <input type="password" name="password" value="<?php echo $user['password']; ?>" class="text w400" maxlength="6">
                                    <p class="hint">密码长度为6位数的数字</p>
                                </dd>
                            </dl>

                            <input type="hidden" name="act" value="add" />

                            <?php else: ?>
                            <input type="hidden" name="act" value="edit" />
                            <dl>
                                <dt><i class="required">*</i> 账号：</dt>
                                <dd>
                                    <p><?php echo $user['email']; ?></p>
                                </dd>
                            </dl>
                            <dl>
                                <dt><i class="required">*</i> 门店：</dt>
                                <dd>

                                    <p><p><?php echo $store['store_name']; ?></p></p>
                                </dd>
                            </dl>
                                <dl>
                                    <dt><i class="required">*</i> 原密码：</dt>
                                    <dd>
                                        <input type="password" value="" maxlength="6" name="old_password" id="old_password" class="text w400">

                                        <p class="hint">密码长度为6位数的数字</p>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt><i class="required">*</i> 新密码：</dt>
                                    <dd>
                                        <input type="password" value="" maxlength="6" name="new_password" id="new_password" class="text w400">

                                        <p class="hint">密码长度为6位数的数字</p>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt><i class="required">*</i> 确定新密码：</dt>
                                    <dd>
                                        <input type="password" value="" maxlength="6" name="verify_password" id="verify_password" class="text w400">
                                        <p class="hint">确认新密码</p>
                                    </dd>
                                </dl>



                        <?php endif; ?>

                    </div>
                    <input type="hidden" id="channel" name="channel" value="">
                    <div class="bottom tc hr32">
                        <label class="submit-border">
                            <input class="submit" value="保存" type="button" onclick="adsubmit();">
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
<script>
$("input[name='store_title']").promptFun({
    table:'store',
    field:'store_name,store_id',
    column:'store_name',
    w:180,
    len:10,
    call:function () {
        var type= $("select[name='store_id'] option:selected").attr('data-channel');
        if(type=='email'){
            $('#email-box').show();$('#mobile-box').hide();
            var emails = $("select[name='store_id'] option:selected").attr('data-emails');
            console.log(emails)
            $('#isAllowEmail').html(emails);$('#channel').val('email')
        }else{
            $('#email-box').hide();$('#mobile-box').show();$('#channel').val('sn');
        }
    }
    
})
</script>
<script>


    $(function () {
        $('#email-box').hide();
        $("select[name='store_id']").change(function (e) {

            var type= $("select[name='store_id'] option:selected").attr('data-channel');

            if(type=='email'){

                $('#email-box').show();$('#mobile-box').hide();
                var emails = $("select[name='store_id'] option:selected").attr('data-emails');
                console.log(emails)
                $('#isAllowEmail').html(emails);$('#channel').val('email')
            }else{
                $('#email-box').hide();$('#mobile-box').show();$('#channel').val('sn');
            }
        })
    })

    function servicePwd(str)
    {

       var  reg=/^\d{1,6}$/;
        if(!reg.test(str)){

            return false;
        }

        return true;
    }


    function adsubmit() {
        var type= $("select[name='store_id'] option:selected").attr('data-channel');

    <?php if(empty($user) || (($user instanceof \think\Collection || $user instanceof \think\Paginator ) && $user->isEmpty())): ?>


        if(!type||!$('#channel').val()){
            layer.msg('请选择店铺！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
            return false;
        }

        if(type=='email'){
            if ($('input[name=email]').val() == '') {
                layer.msg('邮箱不能为空！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
                return false;
            }


            var email = $('input[name=email]').val().split('@')[1],allowEmails = $('#isAllowEmail').html().split(',');

            var isAllow =false;
            for(var k in allowEmails){
                if(allowEmails[k]==email){
                    isAllow = true;
                    break;
                }
            }

            if(!isAllow){
                layer.msg('邮箱后缀只能为'+$('#isAllowEmail').html()+'中一个', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
                return false;
            }
        }

        if(type=='sn'){

        var phone = $('input[name=mobile]').val();
            if (!checkMobile(phone)) {
                layer.msg('手机号码格式不符！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
                return false;
            }
    }






        var pwd = $("input[name='password']").val();
        if (pwd=='') {
            layer.msg('密码不能为空！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
            return false;
        }

        if(!servicePwd(pwd)){
            layer.msg('请输入六位数的纯数字！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');

    return false;
    }

        // if ((pwd.length < 6 || pwd.length > 10) && pwd) {
        //     layer.msg('密码长度应该在6-10位！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
        //     return false;
        // }


    <?php else: ?>

        if ($('#old_password').val() == '') {
            layer.msg('原密码不能为空！', {icon: 2, time: 1000});//alert('少年，密码不能为空！');
            return false;
        }




        var new_password = $('#new_password').val();
        var verify_password = $('#verify_password').val();


        if (new_password == '') {
            layer.msg('新密码不能为空！', {icon: 2, time: 1000});//alert('少年，密码不能为空！');
            return false;
        }

        if (new_password&&(new_password.length!=6)) {
            layer.msg('密码长度应该在6位！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');
            return false;
        }

        if (verify_password != new_password) {
            layer.msg('两次新密码不一致！', {icon: 2, time: 1000});//alert('少年，密码不能为空！');
            return false;
        }

        if(!servicePwd(verify_password)){
            layer.msg('新密码应该为6位数字！', {icon: 2, time: 1000});//alert('少年，邮箱不能为空！');

            return false;
        }
        <?php endif; ?>



        // if ($('input[name=seller_name]').val() == '') {
        //     layer.msg('登录账号不能空！', {icon: 2, time: 1000});//alert('少年，密码不能为空！');
        //     return false;
        // }
        $.ajax({
            url:"<?php echo U('Seller/Admin/visitorAddEdit'); ?>",
            type:'POST',
            dataType:'JSON',
            data:$('#adminHandle').serialize(),
            success:function(data){
                if(data.status == 1){
                    layer.msg(data.msg, {icon: 1},function(){
                        location.href = data.url;
                    });
                }else{
                    layer.alert(data.msg, {icon: 2});
                }
            },
            error : function() {
                layer.alert('网络失败，请刷新页面后重试', {icon: 2});
            }
        })
    }
</script>
<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
</body>
</html>
