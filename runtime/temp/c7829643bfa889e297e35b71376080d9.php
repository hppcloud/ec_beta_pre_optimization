<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:42:"./application/seller/new/coupon/index.html";i:1548757881;s:58:"/mnt/www/test/shop/application/seller/new/public/head.html";i:1544286252;s:58:"/mnt/www/test/shop/application/seller/new/public/left.html";i:1529392734;s:58:"/mnt/www/test/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<div class="ncsc-layout wrapper">
     <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
    <div id="layoutRight" class="ncsc-layout-right">
        <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>促销<i class="icon-angle-right"></i>代金券管理
        </div>
        <div class="main-content" id="mainContent">
            <div class="tabmenu">
                <ul class="tab pngFix">
                    <li class="active"><a href="<?php echo U('coupon/index'); ?>">代金券列表</a></li>
                </ul>
                <!--<a href="" style="right:100px" class="ncbtn ncbtn-mint" title="新增虚拟商品抢购"><i class="icon-plus-sign"></i>新增虚拟抢购</a>-->
                <a href="<?php echo U('Coupon/coupon_info'); ?>" class="ncbtn ncbtn-mint" title="新增代金券"><i class="icon-plus-sign"></i>添加代金券</a>
            </div>

            <div class="alert alert-block mt10 mb10">
	            <ul>
					<!--<li>1、手工设置代金券失效后,用户将不能领取该代金券,但是已经领取的代金券仍然可以使用</li>-->
					<li>1、手工设置代金券失效后,用户将不能领取该代金券,已经领取的代金券也将不能使用</li>
					<li>2、代金券模版和已发放的代金券过期后自动失效</li>
					<li>3、<strong style="color: red">相关费用会在店铺的账期结算中扣除</strong></li>
				</ul>
			</div>
            <form action="<?php echo U('Coupon/index'); ?>" class="search-form" id="search-form2" method="post" >
                <table class="search-form">

                    <tr>
                        <td>&nbsp;</td>
                        <th>领用类型</th>
                        <td class="w80">
                            <select name="type" class="w100">
                                <option value="">请选择...</option>

                                <option <?php if(4 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="4">注册赠送</option>
                                <option <?php if(5 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="5">回收赠送</option>
                                <!--<option <?php if(1 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="1">按用户发放</option>-->
                                <!--<option <?php if(2 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="2">免费领取</option>-->
                                <!--<option <?php if(3 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="3">线下发放</option>-->
                                <!--<option <?php if(4 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="4">注册赠送</option>-->
                                <!--<option <?php if(5 == \think\Request::instance()->post('type')): ?>selected="selected"<?php endif; ?>  value="5">回收赠送</option>-->

                            </select>
                        </td>
                        <!--<th>店铺</th>-->
                        <!--<td class="w80">-->
                            <!--<select name="store_id" class="w300">-->
                                <!--<option value="">请选择...</option>-->
                                <!--<?php if(is_array($stores) || $stores instanceof \think\Collection || $stores instanceof \think\Paginator): if( count($stores)==0 ) : echo "" ;else: foreach($stores as $key=>$store): ?>-->
                                    <!--<option <?php if($store['store_id'] == \think\Request::instance()->post('store_id')): ?>selected="selected"<?php endif; ?>  value="<?php echo $store['store_id']; ?>"><?php echo $store['store_name']; ?></option>-->
                                <!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
                            <!--</select>-->
                        <!--</td>-->
                        <!--<th style="width: 120px">店铺名搜索</th>-->
                        <!--<td class="w200"><input type="text" autocomplete="off" class="text w150"  name="store_title" value="<?php echo $store_name; ?>" placeholder="输入店铺名称" /></td>-->
                        <td class="tc w70"><label class="submit-border">
                            <input type="submit" id="submit" class="submit" value="搜索" />
                        </label></td>
                    </tr>
                </table>
            </form>
            <div id='prompt-box'></div>
            <table class="ncsc-default-table">
                <thead>
                <tr>
                    <th class="w20"></th>
                    <th class="w100 tl">代金券名称</th>
                    <th class="w60">类型</th>
                    <th class="w60">使用类型</th>
                    <!--<th class="w160">店铺名称</th>-->
                    <th class="w50">面额</th>
                    <th class="w50">百分比</th>
                    <th class="w80">使用需满金额</th>
                    <th class="w50">发放总量</th>
                    <th class="w50">已发放数</th>
                    <th class="w50">已使用</th>
                    <th class="w120">使用截止日期</th>
                    <th class="w200">操作</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($lists) || (($lists instanceof \think\Collection || $lists instanceof \think\Paginator ) && $lists->isEmpty())): ?>
                    <tr>
                        <td colspan="20" class="norecord">
                            <div class="warning-option"><i class="icon-warning-sign"></i><span>暂无符合条件的数据记录</span></div>
                        </td>
                    </tr>
                    <?php else: if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): $i = 0; $__LIST__ = $lists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?>
                        <tr class="bd-line">
                            <td></td>
                            <td class="tl"><?php echo $list['name']; ?></td>
                            <td><?php echo $coupons[$list[type]]; ?></td>
                            <td><?php echo $coupon_user_arr[$list[use_type]]; ?></td>
                            <!--<td><?php echo $list['store_name']; ?>[<?php echo $list['store_id']; ?>]</td>-->
                            <td><?php echo $list['money']; ?></td>
                            <td><?php echo $list['percent']; ?></td>
                            <td><?php echo $list['condition']; ?></td>
                            <td>
                                <?php if($list[createnum] == 0): ?>
                                    无限制
                                <?php else: ?>
                                    <?php echo $list['createnum']; endif; ?>
                            </td>
                            <td><?php echo $list['send_num']; ?></td>
                            <td><?php echo $list['use_num']; ?></td>
                            <td><?php echo date('Y-m-d',$list['use_end_time']); ?></td>
                            <td class="nscs-table-handle" style="text-align:center;">
                                <?php if($list[type] == 3 and $list[status] == 1): ?>
                                    <span>
                                        <a href="<?php echo U('Coupon/make_coupon',array('id'=>$list['id'],'type'=>$list['type'])); ?>" class="btn-bluejeans">
                                            <i class="icon-plane"></i><p>发放</p>
                                        </a>
                                    </span>
                                <?php elseif($list[type] == 1 and $list[status] == 1): if(time() > $list[send_end_time]): ?>
                                        <span>
                                            <a class="btn-bluejeans" onclick="layer.alert('该优惠券已过了发放时间',{icon:2})"><i class="icon-plane">
                                            </i><p>发放</p></a>
                                        </span>
                                    <?php elseif(time() < $list[send_start_time]): ?>
                                        <span><a class="btn-bluejeans" onclick="layer.alert('该优惠券未到发放时间',{icon:2})">
                                            <i class="icon-plane"></i><p>发放</p></a>
                                        </span>
                                    <?php else: ?>
                                        <span>
                                            <a class="btn-bluejeans send_user" data-url="<?php echo U('Coupon/send_coupon',array('cid'=>$list[id])); ?>">
                                                <i class="icon-plane"></i><p>发放</p>
                                            </a>
                                        </span>
                                    <?php endif; else: ?>
                                    <!--<span><a class="btn-disabled" onclick="layer.msg('该优惠券类型为'+'<?php echo $coupons[$list[type]]; ?>'+'，不能手动发放',{icon:2})" ><i class="icon-plane"></i><p>发放</p></a></span>-->
                                <?php endif; ?>
                            <span><a href="<?php echo U('Coupon/coupon_list',array('id'=>$list['id'])); ?>" class="btn-bluejeans"><i class="icon-search"></i>

                                <p>详细</p></a></span>
                                <!--注册赠送的和回收赠送的不能在这里编辑和删除-->


                            <span><a href="<?php echo U('Coupon/coupon_info',array('id'=>$list['id'])); ?>" class="btn-bluejeans"><i class="icon-edit"></i>

                                <p>编辑</p></a></span>
                            <span><a onclick="delfun(this,<?php echo $list['id']; ?>)" class="btn-grapefruit"><i class="icon-trash"></i>

                                <p>删除</p></a></span>

                            </td>
                        </tr>
                    <?php endforeach; endif; else: echo "" ;endif; endif; ?>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="20">
                        <?php echo $page; ?>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
<script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
<script>
    $("input[name='store_title']").promptFun({
        table:'store',
        field:'store_name,store_id',
        column:'store_name',
        w:180,
        len:10,
        call:function () {
            $('#submit').click()
        }

    })
</script>
<script>
    $('.send_user').click(function(){
        var url = $(this).attr('data-url');
        layer.open({
            type: 2,
            title: '发放优惠券',
            shadeClose: true,
            shade: 0.5,
            area: ['1020px', '85%'],
            content: url,
        });
    });

    function delfun(obj,id){
        layer.confirm('确定要删除吗？', {
                    btn: ['确定','取消'] //按钮
                }, function(){
                    // 确定
                    $.ajax({
                        type : 'post',
                        url : "/index.php?m=seller&c=Coupon&a=del_coupon",
                        data:{id:id},
                        dataType : 'json',
                        success : function(data){
                            layer.closeAll();
                            if(data.status == 1){
                                $(obj).parent().parent().parent().remove();
                                layer.msg(data.msg, {icon: 1});
                            }else{
                                layer.alert(data.msg, {icon: 2});  //alert('删除失败');
                            }
                        }
                    })
                }, function(index){
                    layer.close(index);
                }
        );
    }
</script>
</body>
</html>