<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:44:"./application/admin/view/manage/visitor.html";i:1540644963;s:60:"/mnt/www/test/shop/application/admin/view/public/layout.html";i:1534500726;}*/ ?>
<!doctype html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-capable" content="yes">
<!-- Apple devices fullscreen -->
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<link href="/public/static/css/main.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/page.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/css/font-awesome.min.css" rel="stylesheet" />
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/css/font-awesome-ie7.min.css">
<![endif]-->
<link href="/public/static/js/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css"/>
<style type="text/css">
    html, body { overflow: visible;}
</style>
<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script><!-- 弹窗js 参考文档 http://layer.layui.com/-->
<script type="text/javascript" src="/public/static/js/admin.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<script src="/public/js/myFormValidate.js"></script>
<script src="/public/js/myAjax2.js"></script>
<script src="/public/js/global.js"></script>
<script type="text/javascript">
function delfunc(obj){
	layer.confirm('确认删除？', {
		  btn: ['确定','取消'] //按钮
		}, function(){
			$.ajax({
				type : 'post',
				url : $(obj).attr('data-url'),
				data : {act:'del',del_id:$(obj).attr('data-id')},
				dataType : 'json',
				success : function(data){
					layer.closeAll();
					if(data.status==1){
                        $(obj).parent().parent().parent().html('');
						layer.msg('操作成功', {icon: 1});
					}else{
						layer.msg('删除失败', {icon: 2,time: 2000});
					}
				}
			})
		}, function(index){
			layer.close(index);
		}
	);
}

function delAll(obj,name){
	var a = [];
	$('input[name*='+name+']').each(function(i,o){
		if($(o).is(':checked')){
			a.push($(o).val());
		}
	})
	if(a.length == 0){
		layer.alert('请选择删除项', {icon: 2});
		return;
	}
	layer.confirm('确认删除？', {btn: ['确定','取消'] }, function(){
			$.ajax({
				type : 'get',
				url : $(obj).attr('data-url'),
				data : {act:'del',del_id:a},
				dataType : 'json',
				success : function(data){
					layer.closeAll();
					if(data == 1){
						layer.msg('操作成功', {icon: 1});
						$('input[name*='+name+']').each(function(i,o){
							if($(o).is(':checked')){
								$(o).parent().parent().remove();
							}
						})
					}else{
						layer.msg(data, {icon: 2,time: 2000});
					}
				}
			})
		}, function(index){
			layer.close(index);
			return false;// 取消
		}
	);	
}

//表格列表全选反选
$(document).ready(function(){
	$('.hDivBox .sign').click(function(){
	    var sign = $('#flexigrid > table>tbody>tr');
	   if($(this).parent().hasClass('trSelected')){
	       sign.each(function(){
	           $(this).removeClass('trSelected');
	       });
	       $(this).parent().removeClass('trSelected');
	   }else{
	       sign.each(function(){
	           $(this).addClass('trSelected');
	       });
	       $(this).parent().addClass('trSelected');
	   }
	})
});

//获取选中项
function getSelected(){
	var selectobj = $('.trSelected');
	var selectval = [];
    if(selectobj.length > 0){
        selectobj.each(function(){
        	selectval.push($(this).attr('data-id'));
        });
    }
    return selectval;
}

function selectAll(name,obj){
    $('input[name*='+name+']').prop('checked', $(obj).checked);
}   

function get_help(obj){

//	window.open("http://www.baidu.com/");
//	return false;
//
//    layer.open({
//        type: 2,
//        title: '帮助手册',
//        shadeClose: true,
//        shade: 0.3,
//        area: ['70%', '80%'],
//        content: $(obj).attr('data-url'), 
//    });
}

//
///**
// * 全选
// * @param obj
// */
//function checkAllSign(obj){
//    $(obj).toggleClass('trSelected');
//    if($(obj).hasClass('trSelected')){
//        $('#flexigrid > table>tbody >tr').addClass('trSelected');
//    }else{
//        $('#flexigrid > table>tbody >tr').removeClass('trSelected');
//    }
//}
/**
 * 批量公共操作（删，改）
 * @returns {boolean}
 */
function publicHandleAll(type){
    var ids = '';
    $('#flexigrid .trSelected').each(function(i,o){
//            ids.push($(o).data('id'));
        ids += $(o).data('id')+',';
    });
    if(ids == ''){
        layer.msg('至少选择一项', {icon: 2, time: 2000});
        return false;
    }
    publicHandle(ids,type); //调用删除函数
}
/**
 * 公共操作（删，改）
 * @param type
 * @returns {boolean}
 */
function publicHandle(ids,handle_type){
    layer.confirm('确认当前操作？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    url: $('#flexigrid').data('url'),
                    type:'post',
                    data:{ids:ids,type:handle_type},
                    dataType:'JSON',
                    success: function (data) {
                        layer.closeAll();
                        if (data.status == 1){
                            layer.msg(data.msg, {icon: 1, time: 2000},function(){
                                location.href = data.url;
                            });
                        }else{
                            layer.msg(data.msg, {icon: 2, time: 3000});
                        }
                    }
                });
            }, function (index) {
                layer.close(index);
            }
    );
}
</script>
</head>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<body style="background-color: #FFF; overflow: auto;">
<div id="toolTipLayer" style="position: absolute; z-index: 9999; display: none; visibility: visible; left: 95px; top: 573px;"></div>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div class="page">
	<div class="fixed-bar">
		<div class="item-title"><a class="back" href="javascript:history.back();" title="返回列表"><i class="fa fa-arrow-circle-o-left"></i></a>
			<div class="subject">
				<h3>经销商临时账号管理</h3>
				<h5></h5>
			</div>
			<ul class="tab-base nc-row">
				<li><a class="current" onclick="$('#tab_store').show();$('#tab_info').hide();$(this).parent().parent().find('a').removeClass('current');$(this).addClass('current');"><span><?php echo (isset($title) && ($title !== '')?$title:'账号信息'); ?></span></a></li>

			</ul>
		</div>
	</div>


			<form class="form-horizontal" action="/admin/Manage/addEditSellerVisitor" method="post" id="seller_info">
				<div class="ncap-form-default" id="tab_store">
					<dl class="row">
						<dt class="tit">
							<label>账号</label>
						</dt>
						<dd class="opt">
							<input class="input-txt valid"  name="seller_account" <?php if($visitor['seller_account'] != null): ?>readonly<?php else: ?>onblur="checkPhone()"<?php endif; ?> value="<?php echo $visitor['seller_account']; ?>"  type="text">
							<span class="err" id="tip_seller_account" style="color:#F00; display:none;"></span>
							<p class="notic">一旦输入不可修改。</p>
						</dd>
					</dl>
					<dl class="row">
						<dt class="tit">
							<label>设置密码</label>
						</dt>
						<dd class="opt">
							<input type="text"   name="password" class="input-txt">
							<p class="notic">请不要设置过于简单的密码,密码应该由数字和字母或特殊符号组成</p>
						</dd>
					</dl>

					<div class="bot"><a href="JavaScript:void(0);" onclick="actsubmit();" class="ncap-btn-big ncap-btn-green">确认提交</a> &nbsp;<a href="JavaScript:void(0);" onclick="javascript:location.reload();" class="ncap-btn-big">刷新</a></div>

					<input type="hidden" name="user_id" value="<?php echo $visitor['user_id']; ?>">
					<input type="hidden" name="seller_id" value="<?php echo $visitor['seller_id']; ?>">

				</div>

			</form>



</div>
<script type="text/javascript">
	$(document).ready(function(){

		// 加载默认选中
		var province_id = $('#province').val();
		if(province_id != 0){
			$('#province').trigger('blur');
		}
		var bank_province_id = $('#bank_province').val();
		if(bank_province_id != 0){
			$('#bank_province').trigger('blur');
		}
		// 加载默认选中
		<?php if($apply[company_district] > 0): ?>
				set_area('<?php echo $apply[company_city]; ?>','<?php echo $apply[company_district]; ?>');
		<?php endif; ?>
	});

	$('#store_end_time').layDate();
    $('#business_date_start').layDate();
    $('#business_date_end').layDate();

	/**
	 * 获取地区
	 */
	function set_area(parent_id,selected){
		if(parent_id <= 0){
			return;
		}
		$('#district').empty().css('display','inline');
		var url = '/index.php?m=Home&c=Api&a=getRegion&level=3&parent_id='+ parent_id+"&selected="+selected;
		$.ajax({
			type : "GET",
			url  : url,
			error: function(request) {
				layer.alert('服务器繁忙, 请联系管理员', {icon: 2});
				return;
			},
			success: function(v) {
				v = '<option>选择区域</option>'+ v;
				$('#district').empty().html(v);
			}
		});
	}
	var flag = true;

	function actsubmit(){


        if(!$('input[name=seller_account]').val()||!checkMobile($('input[name=seller_account]').val())){
            layer.msg("账号必须为手机号", {icon: 2,time: 2000});
            return;
        }


        $('#seller_info').submit();
	}

    function changeBusinessDate(){
        var v = document.getElementById("3");
        if (v.checked==true) {
            document.getElementById("3").value="Y";
            document.getElementById("business_date_end").value="长期";
            document.getElementById("business_date_end").realvalue="";
            document.getElementById("business_date_end").disabled = true;
        } else {
            document.getElementById("3").value="N";
            document.getElementById("business_date_end").disabled = false;
        }
    }

	var tmp_type = '';
	function upload_img(cert_type){
		tmp_type = cert_type;
		//限制高度为48px,宽度无限
		GetUploadify(1,'store','seller','callback',100000,48);
	}

	function callback(img_str){
		$('#'+tmp_type).attr('src',img_str);
		$('input[name='+tmp_type+']').val(img_str);
	}


	//检查手机号码
	function checkPhone() {
	    console.log(222);

		$.post("<?php echo U('Manage/checkPhone'); ?>",{phone:$("input[name='seller_account']").val()},function (result) {
					res = JSON.parse(result)
				if(res.status==1){
                    $('#tip_seller_account').html('手机号码可用').show().css('color','#44b549')
				}else{
                    $("input[name='seller_account']").val(null)
                    $('#tip_seller_account').html(res.msg).show().css('color','#F00')
				}


        })
    }
</script>
</body>
</html>