<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:49:"./application/seller/new/goods/ajaxGoodsList.html";i:1554106179;}*/ ?>
<style>
    .hover img{
        cursor: pointer;
    }
</style>
<table class="ncsc-default-table multiTable" data-goods-examine="<?php echo $store['goods_examine']; ?>">
    <thead>
    <tr nc_type="table_header">
        <th class="taggleAll" axis="col0">
            <div style="width: 24px;"><i class="ico-check"></i></div>
        </th>
        <th class="w30"><a href="javascript:sort('goods_id');">ID</a></th>
        <th class="w50">&nbsp;</th>
        <th class="w250">商品名称</th>
        <th class="w100"><a href="javascript:sort('store_id');">所在店铺</a></th>
        <th class="w80"><a href="javascript:sort('shop_price');">价格</a></th>
        <th class="w30"><a >热门</a></th>
        <th class="w30"><a >上架</a></th>
        <th class="w30"><a >分期</a></th>
        <th class="w30"><a >预约</a></th>

        <th class="w30"><a >推荐</a></th>
        <th class="w30"><a >共享库存</a></th>
        <th class="w80"><a href="javascript:sort('store_count');">库存</a></th>
        <th class="w80"><a href="javascript:sort('on_time');">上架时间</a></th>
        <th class="w30"><a href="javascript:sort('sort');">排序</a></th>
        <th class="w250">操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(empty($goodsList) || (($goodsList instanceof \think\Collection || $goodsList instanceof \think\Paginator ) && $goodsList->isEmpty())): ?>
        <tr>
            <td colspan="20" class="norecord"><div class="warning-option"><i class="icon-warning-sign"></i><span>暂无符合条件的数据记录</span></div></td>
        </tr>
    <?php else: if(is_array($goodsList) || $goodsList instanceof \think\Collection || $goodsList instanceof \think\Paginator): $i = 0; $__LIST__ = $goodsList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?>
        <tr class="bd-line" data-id="<?php echo $list['goods_id']; ?>">
            <td class="sign" >
                <div style="width: 24px;"><i class="ico-check"></i></div>
            </td>
            <td><?php echo $list['goods_id']; ?></td>
            <td>
                <div class="pic-thumb">
                    <!--<?php echo \think\Config::get('client_url').'/productdetail/'.$list['goods_id']; ?>-->
                    <a href="#">
                        <img style="width:32px;height:32px" src="<?php echo goods_thum_images($list['goods_id'],50,50); ?>" />
                    </a>
                </div>
            </td>
            <td class="tl">
                <dl class="goods-name">
                    <dt style="max-width: 450px !important;">
                    <?php if($list[is_virtual] == 1): ?><span class="type-virtual" title="虚拟兑换商品">虚拟</span><?php endif; ?>
                        <!--<?php echo \think\Config::get('client_url').'/productdetail/'.$list['goods_id']; ?>-->
                        <a title="点击预览"  target="_blank" data-goodsid="<?php echo getGoodsPreviewRealId('514KYgngRvZBdzVmGH+FEw=='); ?>" href="<?php echo getGoodsPreviewUrl($list['goods_id']); ?>"><?php echo getSubstr($list['goods_name'],0,33); ?></a></dt>
                    <!--<dd>商品货号：<?php echo $list['goods_sn']; ?></dd>-->
                    <?php if($list['tmpl_id']): ?>
                        <dd>产品编号：<?php echo getTmplID($list['tmpl_id']); ?></dd>
                        <?php else: ?>
                        <dd>产品编号：<?php echo getTmplID($list['goods_id']); ?></dd>
                    <?php endif; ?>

                    <dd class="serve">
                        <?php if($list['is_recommend'] == 1): ?>
                            <span class="open" title="平台推荐商品"><i class="commend">荐</i></span>
                        <?php endif; ?>
                        <!--<a href="<?php echo U('Mobile/Goods/goodsInfo',array('id'=> $list['goods_id'])); ?>"><span title="手机端商品详情"><i class="icon-tablet"></i></span></a>-->
                        <!--<a onclick="ClearGoodsHtml('<?php echo $list[goods_id]; ?>')" title="清除静态缓存页面"><span title="清除静态缓存页面"><i class="icon-wrench"></i></span></a>-->
                        <a onclick="ClearGoodsThumb('<?php echo $list[goods_id]; ?>')" title="清除缩略图缓存"><span title="清除缩略图缓存"><i class="icon-picture"></i></span></a>
                    </dd>
                </dl>
            </td>
            <td><span><?php echo $list['store_name']; ?></span></td>
            <td><span>&yen;<?php echo $list['shop_price']; ?></span></td>
            <td>
                <?php if($list['is_hot'] == 1): ?>
                    <img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list[goods_id]; ?>,'is_hot',this)"/>
                    <?php else: ?>
                    <img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list[goods_id]; ?>,'is_hot',this)" width="20" height="20">
                <?php endif; ?>

            </td>


            <td class="hover">
                <img width="20"  height="20" src="/public/images/<?php if($list[is_on_sale] == 1): ?>yes.png<?php else: ?>cancel.png<?php endif; ?>" class="is_on_sale"/>


            </td>


            <!--<td class="hover">-->
                <!--<?php if($list['is_new'] == 1): ?>-->
                    <!--<img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_new',this)"/>-->
                <!--<?php else: ?>-->
                    <!--<img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_new',this)" width="20" height="20">-->
                <!--<?php endif; ?>-->
            <!--</td>-->

            <!--<td class="hover">-->
                <!--<?php if($list['is_recommend'] == 1): ?>-->
                    <!--<img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_recommend',this)"/>-->
                <!--<?php else: ?>-->
                    <!--<img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_recommend',this)" width="20" height="20">-->
                <!--<?php endif; ?>-->
            <!--</td>-->
            <td class="hover">
                <?php if($list['is_pay'] == 1): ?>
                    <img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_pay',this)"/>
                    <?php else: ?>
                    <img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_pay',this)" width="20" height="20">
                <?php endif; ?>
            </td>

            <td class="hover">
                <?php if($list['is_reserve'] == 1): ?>
                    <img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_reserve',this)"/>
                    <?php else: ?>
                    <img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_reserve',this)" width="20" height="20">
                <?php endif; ?>
            </td>
            <td class="hover">
            <?php if($list['is_recommend'] == 1): ?>
            <img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_recommend',this)"/>
            <?php else: ?>
            <img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_recommend',this)" width="20" height="20">
            <?php endif; ?>
            </td>
            <td class="hover">
                <?php if($list['is_share_stock'] == 1): ?>
                    <img width="20" height="20" src="/public/images/yes.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_share_stock',this)"/>
                    <?php else: ?>
                    <img src="/public/images/cancel.png" onclick="changeTableVal2('goods','goods_id',<?php echo $list['goods_id']; ?>,'is_share_stock',this)" width="20" height="20">
                <?php endif; ?>
            </td>
            <td><span  style="<?php if($list['store_count'] < $store_warning_storage): ?>color: red<?php endif; ?>"><?php echo $list['all_store_stock']; ?></span></td>
            <td><?php echo date('Y-m-d',$list['on_time']); ?></td>
            <td>
                <input class="txt-cen" type="text" onkeyup="this.value=this.value.replace(/[^\d]/g,'')" onpaste="this.value=this.value.replace(/[^\d]/g,'')" onchange="updateSort2('goods','goods_id','<?php echo $list['goods_id']; ?>','sort',this)" size="4" value="<?php echo $list['sort']; ?>" />
            </td>
            <td class="nscs-table-handle">
                <span><a href="javascript:void(0)" onclick="toEdit(this)" data-href="<?php echo U('Goods/addEditGoods',array('goods_id'=>$list['goods_id'])); ?>" class="btn-bluejeans"><i class="icon-edit"></i><p>编辑</p></a></span>
                <span><a href="<?php echo U('Goods/copyGoods',array('goods_id'=>$list['goods_id'])); ?>" class="btn-bluejeans"><i class="icon-copy"></i><p>复制</p></a></span>
                <span><a href="javascript:void(0);" onclick="del('<?php echo $list[goods_id]; ?>')" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>
            </td>
        </tr>
    <?php endforeach; endif; else: echo "" ;endif; endif; ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="20">
            <div class="tDiv">
                <div class="tDiv2">

                    <div class="fbutton">
                        <a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','1')" data-url="<?php echo U('Seller/goods/resume'); ?>">
                            <div class="add" title="批量上架">
                                <span><i class="fa fa-plus"></i>批量上架</span>
                            </div>
                        </a>
                    </div>
                    <div class="fbutton">
                        <a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','0',null,delAll)" data-url="<?php echo U('Seller/goods/forbid'); ?>">
                            <div class="add" title="批量下架">
                                <span><i class="fa fa-plus"></i>批量下架</span>
                            </div>
                        </a>
                    </div>
                    <!--<div class="fbutton">-->
                        <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','2')" data-url="<?php echo U('Seller/goods/forbid'); ?>">-->
                            <!--<div class="add" title="违规下架">-->
                                <!--<span><i class="fa fa-plus"></i>违规下架</span>-->
                            <!--</div>-->
                        <!--</a>-->
                    <!--</div>-->
                    <div class="fbutton">
                        <a href="javascript:void(0)" onclick="publicHandleAll(this,'delete',null,null,delAll)" data-url="<?php echo U('Seller/goods/del'); ?>">
                            <div class="add" title="批量删除">
                                <span><i class="fa fa-plus"></i>批量删除</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="20">
            <?php echo $page; ?>
        </td>
    </tr>
    </tfoot>
</table>

<script>
    function toEdit(ele) {

        var url = $(ele).data('href');
        sessionStorage.setItem('goodsDetail2ListUrl',location.href);
        location.href= url;

    }
    // 点击分页触发的事件
    $(".pagination  a").click(function(){
        cur_page = $(this).data('p');
        ajax_get_table('search-form2',cur_page);
    });

    /*
     * 清除静态页面缓存
     */
    function ClearGoodsHtml(goods_id)
    {
        $.ajax({
            type:'GET',
            url:"<?php echo U('Seller/Admin/ClearGoodsHtml'); ?>",
            data:{goods_id:goods_id},
            dataType:'json',
            success:function(data){
                if(data.status == 1){
                    layer.msg(data.msg, {icon: 1});
                }else{
                    layer.msg(data.msg, {icon: 2});
                }
            }
        });
    }
    /*
     * 清除商品缩列图缓存
     */
    function ClearGoodsThumb(goods_id)
    {
        $.ajax({
            type:'GET',
            url:"<?php echo U('Seller/Admin/ClearGoodsThumb'); ?>",
            data:{goods_id:goods_id},
            dataType:'json',
            success:function(data){
                if(data.status == 1){
                    layer.msg(data.msg, {icon: 1});
                }else{
                    layer.msg(data.msg, {icon: 2});
                }
            }
        });
    }
</script>
