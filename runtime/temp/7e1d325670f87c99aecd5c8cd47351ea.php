<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:49:"./application/seller/new/goods/goods_offline.html";i:1553841559;s:61:"/mnt/www/release/shop/application/seller/new/public/head.html";i:1544286252;s:61:"/mnt/www/release/shop/application/seller/new/public/left.html";i:1529392734;s:61:"/mnt/www/release/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<style>
	.sort{
		cursor: pointer;
	}
	.icon-caret-up,.icon-caret-down{
		color: #3BAEDA;
	}
</style>
<div class="ncsc-layout wrapper">
     <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
  <div id="layoutRight" class="ncsc-layout-right">
    <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>商品<i class="icon-angle-right"></i>仓库中的商品</div>
<div class="main-content" id="mainContent">
      
	<div class="tabmenu">
	  <ul class="tab pngFix">
	  <li class="<?php if($_GET[is_on_sale] == 0): ?>active<?php else: ?>normal<?php endif; ?>"><a  href="<?php echo U('Goods/goods_offline',array('is_on_sale'=>0)); ?>">仓库中的商品</a></li>
	  <li class="<?php if($_GET[is_on_sale] == 2): ?>active<?php else: ?>normal<?php endif; ?>"><a  href="<?php echo U('Goods/goods_offline',array('is_on_sale'=>2)); ?>">违规的商品</a></li>
	  </ul>
	</div>
	<form id="filter" method="get" action="">
	  <table  class="search-form">
	    <input type="hidden" name="act" value="goods_offline" />
	    <tr>
	      <td>&nbsp;</td>
			<th>店铺</th>
			<td class="w80">
				<select name="store_id" class="w300">
					<option value="">请选择...</option>
					<?php if(is_array($stores) || $stores instanceof \think\Collection || $stores instanceof \think\Paginator): if( count($stores)==0 ) : echo "" ;else: foreach($stores as $key=>$store): ?>
						<option <?php if($store['store_id'] == \think\Request::instance()->get('store_id')): ?>selected="selected"<?php endif; ?> value="<?php echo $store['store_id']; ?>" ><?php echo $store['store_name']; ?></option>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</td>
            <th style="width: 120px">店铺名搜索</th>
            <td class="w200"><input type="text" autocomplete="off" class="text w150"  name="store_title" value="<?php echo $store_name; ?>" placeholder="输入店铺名称" /></td>
	      <?php if($_GET[is_on_sale] == 0): ?>
	      <th>审核状态</th>
	      <td class="w90">
	        <select name="goods_state">
	          	<option value="">请选择...</option>
	            <option value="0" <?php if(\think\Request::instance()->param('goods_state') === 0): ?>selected<?php endif; ?>>待审核</option>
	            <option value="1" <?php if(\think\Request::instance()->param('goods_state') == 1): ?>selected<?php endif; ?>>审核通过</option>
	            <option value="2" <?php if(\think\Request::instance()->param('goods_state') == 2): ?>selected<?php endif; ?>>未通过</option>
	          </select>
	      </td>
	      <?php endif; ?>
	      <td class="w160"><input type="text" class="text" name="key_word" value="<?php echo \think\Request::instance()->param('key_word'); ?>" placeholder="搜索词"/></td>
	      <td class="tc w70"><label class="submit-border"><input type="submit" class="submit" value="搜索" /></label></td>
	    </tr>
	  </table>

		<!--排序规则-->
		<input type="hidden" name="orderby1" value="<?php echo (\think\Request::instance()->get('orderby1') ?: 'goods_id'); ?>" />
		<input type="hidden" name="orderby2" value="<?php echo (\think\Request::instance()->get('orderby2') ?: 'desc'); ?>" />
	</form>
    <div id='prompt-box'></div>
	<?php if($_GET[is_on_sale] == 2): ?>
	<table class="ncsc-default-table multiTable">
	  <thead>
	    <tr nc_type="table_header">
			<th class="taggleAll" axis="col0">
				<div style="width: 24px;"><i class="ico-check"></i></div>
			</th>
			<th class="w80 sort" onclick="sort('goods_id');">ID
				<?php if(\think\Request::instance()->get('orderby1') == 'goods_id'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
			</th>
	      <th class="w50"></th>
	      <th>商品名称</th>
	      <th class="w180">违规禁售原因</th>
			<th class="w100 sort" onclick="sort('shop_price');">价格
				<?php if(\think\Request::instance()->get('orderby1') == 'shop_price'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
			</th>
			<th class="w100 sort" onclick="sort('store_count');">库存
				<?php if(\think\Request::instance()->get('orderby1') == 'store_count'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
			</th>
	      <th class="w200">操作</th>
	    </tr>
	      <!--<tr>
	      <td class="tc"><input type="checkbox" id="all" class="checkall"/></td>
	      <td colspan="10"><label for="all">全选</label>
	        <a href="javascript:void(0);" class="ncbtn-mini" nc_type="batchbutton" uri="" name="commonid" confirm="您确定要删除吗?"><i class="icon-trash"></i>删除</a>
	      </tr>-->
	      </thead>
	  	  <tbody>
	  	  	  <?php if(is_array($goodsList) || $goodsList instanceof \think\Collection || $goodsList instanceof \think\Paginator): if( count($goodsList)==0 ) : echo "" ;else: foreach($goodsList as $key=>$vo): ?>
		      <tr data-id="<?php echo $vo[goods_id]; ?>">
				  <td class="sign" >
					  <div style="width: 24px;"><i class="ico-check"></i></div>
				  </td>
		      <td class="trigger">
                  <!--<i class="icon-plus-sign" nctype="ajaxGoodsList"></i>-->
                  <?php echo $vo['goods_id']; ?>
              </td>
		      <td><div class="pic-thumb">
		      	<img src="<?php echo goods_thum_images($vo['goods_id'],50,50); ?>"/></a></div>
		      </td>
		      <td class="tl">
		      	<dl class="goods-name">
		          <dt style="max-width: 450px !important;">
		          <?php if($vo[is_virtual] == 1): ?><span class="type-virtual" title="虚拟兑换商品">虚拟</span><?php endif; ?>
<!--		          <a href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$vo['goods_id'])); ?>" target="_blank"><?php echo getSubstr($vo['goods_name'],0,33); ?></a>-->
<!--                          <a title="点击预览"  target="_blank" data-goodsid="<?php echo getGoodsPreviewRealId('514KYgngRvZBdzVmGH+FEw=='); ?>" href="<?php echo getGoodsPreviewUrl($vo['goods_id']); ?>"><?php echo getSubstr($vo['goods_name'],0,33); ?></a>-->
                          <?php echo getSubstr($vo['goods_name'],0,33); ?>
                          </dt>
		          <!-- <dd>商家货号：<?php echo $vo['goods_sn']; ?></dd> -->
		        </dl>
		      </td>
		      <td><?php echo $vo['close_reason']; ?></td>
		      <td><span>&yen;<?php echo $vo['shop_price']; ?></span></td>
		      <td><span><?php echo $vo['store_count']; ?>件</span></td>
		      <td class="nscs-table-handle tr">
			  	<span><a href="<?php echo U('Goods/goodsUpLine',array('goods_ids'=>$vo['goods_id'])); ?>" class="btn-bluejeans"><i class="icon-refresh"></i><p>重新申请审核</p> </a></span>
		      	<span><a href="<?php echo U('Goods/addEditGoods',array('goods_id'=>$vo['goods_id'])); ?>" class="btn-bluejeans"><i class="icon-edit"></i><p>编辑</p> </a></span>
		        <span><a href="javascript:void(0);" onclick="del('<?php echo $vo[goods_id]; ?>')" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>
		      </td>
		      </tr>
		      <?php endforeach; endif; else: echo "" ;endif; ?>
	      </tbody>
	      <tfoot>
		    <tr>
		       <td colspan="20"><?php echo $page; ?></td>
		    </tr>
		  </tfoot>
	  </table>
	  <?php else: ?>
	  <table class="ncsc-default-table multiTable">
	  <thead>
	    <tr nc_type="table_header">
            <th class="taggleAll" axis="col0">
                <div style="width: 24px;"><i class="ico-check"></i></div>
            </th>
	      <th class="w80 sort" onclick="sort('goods_id');">ID
			  <?php if(\think\Request::instance()->get('orderby1') == 'goods_id'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
		  </th>
	      <th class="w50"></th>
	      <th>商品名称</th>
	      <th class="w80">上架</th>
	      <th class="w80">审核状态</th>
	      <th class="w100 sort" onclick="sort('shop_price');">价格
			  <?php if(\think\Request::instance()->get('orderby1') == 'shop_price'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
		  </th>
	      <th class="w100 sort" onclick="sort('store_count');">库存
			  <?php if(\think\Request::instance()->get('orderby1') == 'store_count'): if(\think\Request::instance()->get('orderby2') == 'desc'): ?><i class="icon-caret-down"></i><?php else: ?><i class="icon-caret-up"></i><?php endif; endif; ?>
		  </th>
	      <th class="w100">操作</th>
	    </tr>
	      <!--<tr>
	      <td class="tc"><input type="checkbox" id="all" class="checkall"/></td>
	      <td colspan="10"><label for="all">全选</label>
	        <a href="javascript:void(0);" class="ncbtn-mini" nc_type="batchbutton" uri="" name="commonid" confirm="您确定要删除吗?"><i class="icon-trash"></i>删除</a>
	      </tr>-->
	      </thead>
	  	  <tbody>
	  	  	  <?php if(is_array($goodsList) || $goodsList instanceof \think\Collection || $goodsList instanceof \think\Paginator): if( count($goodsList)==0 ) : echo "" ;else: foreach($goodsList as $key=>$vo): ?>
		      <tr id="list_<?php echo $vo[goods_id]; ?>" data-id="<?php echo $vo[goods_id]; ?>">
                  <td class="sign" >
                      <div style="width: 24px;"><i class="ico-check"></i></div>
                  </td>
		      <td class="trigger">
                  <!--<i class="icon-plus-sign" nctype="ajaxGoodsList"></i>-->
                  <?php echo $vo['goods_id']; ?></td>
		      <td><div class="pic-thumb">
		      <img src="<?php echo goods_thum_images($vo['goods_id'],50,50); ?>"/></a></div></td>
		      <td class="tl">
		      	<dl class="goods-name">
		          <dt style="max-width: 450px !important;">
		          <?php if($vo[is_virtual] == 1): ?><span class="type-virtual" title="虚拟兑换商品">虚拟</span><?php endif; ?>
<!--		          <a href="<?php echo U('Home/Goods/goodsInfo',array('id'=>$vo['goods_id'])); ?>" target="_blank"><?php echo getSubstr($vo['goods_name'],0,33); ?></a>-->
<!--                           <a title="点击预览"  target="_blank" data-goodsid="<?php echo getGoodsPreviewRealId('514KYgngRvZBdzVmGH+FEw=='); ?>" href="<?php echo getGoodsPreviewUrl($vo['goods_id']); ?>"><?php echo getSubstr($vo['goods_name'],0,33); ?></a>--><?php echo getSubstr($vo['goods_name'],0,33); ?></dt>
		          <!-- <dd>商家货号：<?php echo $vo['goods_sn']; ?></dd> -->
		        </dl>
		      </td>
		      <td><img width="20" height="20" src="/public/images/<?php if($vo[is_on_sale] == 1): ?>yes.png<?php else: ?>cancel.png<?php endif; ?>" <?php if($vo[goods_state] != 1): ?>onclick="layer.msg('该商品需通过审核才能上架',{icon:2});"<?php else: ?>onclick="changeTableVal2('goods','goods_id','<?php echo $vo['goods_id']; ?>','is_on_sale',this)"<?php endif; ?> /></td>
		      <td><?php echo $state[$vo[goods_state]]; ?></td>
		      <td><span>&yen;<?php echo $vo['shop_price']; ?></span></td>
		      <td><span><?php echo $vo['store_count']; ?>件</span></td>
		      <td class="nscs-table-handle tr">
		      	<span><a href="javascript:void(0)" onclick="toEdit(this)" data-href="<?php echo U('Goods/addEditGoods',array('goods_id'=>$vo['goods_id'])); ?>" class="btn-bluejeans"><i class="icon-edit"></i><p>编辑</p> </a></span>
		        <span><a href="javascript:void(0);" onclick="del('<?php echo $vo[goods_id]; ?>')" class="btn-grapefruit"><i class="icon-trash"></i><p>删除</p></a></span>
		      </td>
		      </tr>
		      <?php endforeach; endif; else: echo "" ;endif; ?>
	      </tbody>
	      <tfoot>
		  <tr>
			  <td colspan="20">
				  <div class="tDiv">
					  <div class="tDiv2">

						  <!-- <div class="fbutton">
							  <a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','1',null,load)" data-url="<?php echo U('Seller/goods/resume'); ?>">
								  <div class="add" title="批量上架">
									  <span><i class="fa fa-plus"></i>批量上架</span>
								  </div>
							  </a>
						  </div> -->
						  <!--<div class="fbutton">-->
							  <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','0',null,delAll)" data-url="<?php echo U('Seller/goods/forbid'); ?>">-->
								  <!--<div class="add" title="批量下架">-->
									  <!--<span><i class="fa fa-plus"></i>批量下架</span>-->
								  <!--</div>-->
							  <!--</a>-->
						  <!--</div>-->
						  <!--<div class="fbutton">-->
						  <!--<a href="javascript:void(0)" onclick="publicHandleAll(this,'is_on_sale','2')" data-url="<?php echo U('Seller/goods/forbid'); ?>">-->
						  <!--<div class="add" title="违规下架">-->
						  <!--<span><i class="fa fa-plus"></i>违规下架</span>-->
						  <!--</div>-->
						  <!--</a>-->
						  <!--</div>-->
						  <div class="fbutton">
							  <a href="javascript:void(0)" onclick="publicHandleAll(this,'delete',null,null,delAll)" data-url="<?php echo U('Seller/goods/del'); ?>">
								  <div class="add" title="批量删除经销商">
									  <span><i class="fa fa-plus"></i>批量删除</span>
								  </div>
							  </a>
						  </div>
					  </div>
					  <div style="clear:both"></div>
				  </div>
			  </td>
		  </tr>
		    <tr>
		       <td colspan="20"><?php echo $page; ?></td>
		    </tr>
		  </tfoot>
	  </table>
	  <?php endif; ?>
   </div>
  </div>
</div>
<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
<script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
<script>

    function sort(field) {
        $("input[name='orderby1']").val(field);
        var v = $("input[name='orderby2']").val() == 'desc' ? 'asc' : 'desc';
        $("input[name='orderby2']").val(v);
        $('#filter').submit();
    }
    function toEdit(ele) {

        var url = $(ele).data('href');
        sessionStorage.setItem('goodsDetail2ListUrl',location.href);
        location.href= url;

    }
    $("input[name='store_title']").promptFun({
        table:'store',
        field:'store_name,store_id',
        column:'store_name',
        w:180,
        len:10,
        call:function () {
            $('.search-form').submit();
        }

    })
</script>
<script>

    function load(){
        location.reload()
    }
// 删除操作
function del(id) {
	layer.confirm('确定要删除吗？', {
				btn: ['确定','取消'] //按钮
			}, function(){
				// 确定
				$.ajax({
					url: "/index.php?m=Seller&c=goods&a=delGoods&ids=" + id,
                    dataType:'json',
					success: function (data) {
						layer.closeAll();
						if (data.status == 1){
                            layer.msg(data.msg, {icon: 1, time: 1000},function () {
                               $('#list_'+id).remove();
                                ajax_get_table('search-form2', cur_page);
                            });

                        }else{
                            layer.msg(data.msg, {icon: 2, time: 1000}); //alert(v.msg);
                        }

					}
				});
			}, function(index){
				layer.close(index);
			}
	);
}
</script>
</body>
</html>
