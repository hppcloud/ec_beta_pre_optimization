<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:41:"./application/seller/new/admin/login.html";i:1529392734;}*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>经销商中心</title>
<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<link href="/public/static/css/css.css" rel="stylesheet" type="text/css" />
<script src="/public/static/js/layer/layer.js"></script>
</head>

<body>
<div class="wrapper">
	<div class="login">
		<div class="con w100pct">
			<div class="tit"><img src="/public/static/images/seller/login-title.png" /></div>
			<ul>
				<li class="inp user">
					<span class="ico"></span>
					<input name="mobile" type="text" id="mobile" autocomplete="off"  placeholder="用户名" />
					<span class="btn disable"><a href="JavaScript:void(0);">×</a></span>
				</li>
				<li class="tips"></li>
				<li class="inp pass">
					<span class="ico"></span>
					<input name="password" type="password" id="password" autocomplete="off"  placeholder="密码" />
				</li>
				<li class="tips"></li>
				<li class="inp yzm">
					<span class="ico"></span>
					<input type="text" placeholder="验证码" name="vertify" id="captcha" autocomplete="off" maxlength="4" size="10"/>
					<span class="btn cursor"><img src="<?php echo U('Admin/vertify'); ?>" onclick="fleshVerify();" title="换一张"  name="codeimage"  id="imgVerify" /></span>
				</li>
				<li class="tips"></li>
				<li class="line"></li>
				<li class="btn baise"><a href="JavaScript:void(0);" onclick="checkLogin();">登　录</a></li>
				<li class="txt qianlan"><a href="#">忘记密码</a></li>
			</ul>
		</div>
	</div><!--end login-->
	
</div><!--end wrapper-->
<script type="text/javascript">
    	$(".memory_user .checkbox").click(function(){
			if($(this).prop("checked") == true){
				$(this).parent('.memory_user').addClass("checked");
			}else{
				$(this).parent('.memory_user').removeClass("checked");
			}
		});
		
        function checkLogin(){
            var mobile = $('#mobile').val();
            var password = $('#password').val();
            var vertify = $('input[name="vertify"]').val();
            if( mobile == '' || password ==''){
          	  layer.alert('用户名或密码不能为空', {icon: 2}); //alert('用户名或密码不能为空');
          	  return;
            }
            if(vertify ==''){
             	  layer.alert('验证码不能为空', {icon: 2});
          	  return;
            }
            if(vertify.length !=4){
             	  layer.alert('验证码错误', {icon: 2});
             	  fleshVerify();
          	  return;
            }
            $.ajax({
    			url:'/index.php?m=Seller&c=Admin&a=login&t='+Math.random(),
  				type:'post',
  				dataType:'json',
  				data:{mobile:mobile,password:password,vertify:vertify},
  				success:function(res){
  					if(res.status==1){
  			     		top.location.href = res.url;
  					}else{
  						layer.alert(res.msg, {icon: 2});
                        fleshVerify();
  					}
  				},
  				error : function(XMLHttpRequest, textStatus, errorThrown) {
  					layer.alert('网络失败，请刷新页面后重试', {icon: 2});
  				}
            })
        }

        //回车提交
        $(document).keyup(function(event){
            if(event.keyCode ==13){
                var isFocus=$("#captcha").is(":focus");
                if(true==isFocus){
                    checkLogin();
                }
            }
        });

     function fleshVerify(){
         //重载验证码
         $('#imgVerify').attr('src','/index.php?m=Seller&c=Admin&a=vertify&r='+Math.floor(Math.random()*100));
     }
    </script>
</body>
</html>
