<?php if (!defined('THINK_PATH')) exit(); /*a:4:{s:41:"./application/seller/new/order/index.html";i:1552729777;s:58:"/mnt/www/test/shop/application/seller/new/public/head.html";i:1544286252;s:58:"/mnt/www/test/shop/application/seller/new/public/left.html";i:1529392734;s:58:"/mnt/www/test/shop/application/seller/new/public/foot.html";i:1545471227;}*/ ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>经销商中心</title>
<link href="/public/static/css/base.css" rel="stylesheet" type="text/css">
<link href="/public/static/css/seller_center.css" rel="stylesheet" type="text/css">
<link href="/public/static/font/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo (isset($tpshop_config['shop_info_store_ico']) && ($tpshop_config['shop_info_store_ico'] !== '')?$tpshop_config['shop_info_store_ico']:'/public/static/images/logo/storeico_default.png'); ?>" media="screen"/>
<!--[if IE 7]>
  <link rel="stylesheet" href="/public/static/font/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->

<!--fun-->
<link href="/public/js/seller/store.fun.css" rel="stylesheet" />

<script type="text/javascript" src="/public/static/js/jquery.js"></script>
<script type="text/javascript" src="/public/static/js/seller.js"></script>
<script type="text/javascript" src="/public/static/js/waypoints.js"></script>
<script type="text/javascript" src="/public/static/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/public/static/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="/public/static/js/layer/layer.js"></script>
<script type="text/javascript" src="/public/js/dialog/dialog.js" id="dialog_js"></script>
<script type="text/javascript" src="/public/js/global.js"></script>
<script type="text/javascript" src="/public/js/myAjax.js"></script>
<script type="text/javascript" src="/public/js/myFormValidate.js"></script>
<script type="text/javascript" src="/public/static/js/layer/laydate/laydate.js"></script>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/public/static/js/html5shiv.js"></script>
      <script src="/public/static/js/respond.min.js"></script>
<![endif]-->
  <script>

      function delAll() {
          $('.multiTable >tbody>tr').each(function (i, o) {
              console.log(i)
              if ($(o).hasClass('trSelected')) {
                  $(o).remove();
              }
          })
      }


      function bindMutliFun() {
          $('.multiTable .sign').click(function () {

              if ($(this).parent().hasClass('trSelected')) {
                  $(this).parent().removeClass('trSelected');
              } else {
                  $(this).parent().addClass('trSelected');
              }
          })


          $('.multiTable .taggleAll').click(function () {
              var sign = $('.multiTable >tbody>tr');
              console.log(sign)
              if ($(this).parent().hasClass('trSelected')) {
                  sign.each(function () {
                      $(this).removeClass('trSelected');
                  });
                  $(this).parent().removeClass('trSelected');
              } else {
                  sign.each(function () {
                      $(this).addClass('trSelected');
                  });
                  $(this).parent().addClass('trSelected');
              }
          })
      }

      //表格列表全选反选
      $(document).ready(function () {
          bindMutliFun()
      });

      //获取选中项
      function getSelected() {
          var selectobj = $('.trSelected');
          var selectval = [];
          if (selectobj.length > 0) {
              selectobj.each(function () {
                  selectval.push($(this).attr('data-id'));
              });
          }
          return selectval;
      }


      /**
       * 批量公共操作（删，改）
       * @returns {boolean}
       */
      function publicHandleAll(obj,field,val,type,call) {
          var url =$(obj).attr('data-url')
          var ids = '';
          $('.multiTable >tbody>tr.trSelected').each(function (i, o) {
              if (ids == '') {
                  ids += $(o).data('id');
              } else {
                  ids += ',' + $(o).data('id');
              }

          });
          if (ids == '') {
              layer.msg('至少选择一项', {icon: 2, time: 2000});
              return false;
          }
          publicHandle(url,ids,field,val,type,call); //调用删除函数
      }

      /**
       * 公共操作（删，改）
       * @param type
       * @returns {boolean}
       */
      function publicHandle(url,ids, field,val,type,call) {
          layer.confirm('确认当前操作？', {
                  btn: ['确定', '取消'] //按钮
              }, function () {
                  // 确定
                  $.ajax({
                      url: url,
                      type: 'post',
                      data: {id: ids,field:field,value:val,type: type},
                      dataType: 'JSON',
                      success: function (data) {
                          layer.closeAll();
                          if (data.status == 1) {
                              layer.msg(data.msg, {icon: 1, time: 2000}, function () {

                                  if(call){
                                      call();
                                      return;
                                  }

                                  // if(data.url){
                                  //     location.href = data.url;
                                  // }else{
                                  //     // location.reload()
                                  // }
                              });
                          } else {
                              layer.msg(data.msg, {icon: 2, time: 3000});
                          }
                      }
                  });
              }, function (index) {
                  layer.close(index);
              }
          );
      }





  </script>

    <style>
        #prompt-box{
            left: -111111px;
            background: white;
            padding: 10px;
            border: 1px solid #e7e7e7;
            position: fixed;
            z-index: -222;


        }
        #prompt-box>li{
            list-style: none;
            line-height: 30px;

        }
        #prompt-box>li:hover{
            cursor: pointer;
            background: #e7e7eb;
        }
    </style>

</head>
<body>
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<header class="ncsc-head-layout w">
  <div class="wrapper">
    <div class="ncsc-admin w252">
      <dl class="ncsc-admin-info">
        <dt class="admin-avatar"><img src="/public/static/images/seller/default_user_portrait.gif" width="32" class="pngFix" alt=""/></dt>
      </dl>
      <div class="ncsc-admin-function">

      <div class="index-search-container">
      <p class="admin-name"><a class="seller_name" href=""><?php echo $seller['seller_name']; ?></a></p>
      <!--<div class="index-sitemap">-->
          <!--<a class="iconangledown" href="javascript:void(0);">快捷导航 <i class="icon-angle-down"></i></a>-->
          <!--<div class="sitemap-menu-arrow"></div>-->
          <!--<div class="sitemap-menu">-->
              <!--<div class="title-bar">-->
                <!--<h2>管理导航</h2>-->
                <!--<p class="h_tips"><em>小提示：添加您经常使用的功能到首页侧边栏，方便操作。</em></p>-->
                <!--<img src="/public/static/images/obo.png" alt="">-->
                <!--<span id="closeSitemap" class="close">X</span>-->
              <!--</div>-->
              <!--<div id="quicklink_list" class="content">-->
	          	<!--<?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $k2=>$v2): ?>-->
	             <!--<dl>-->
	              <!--<dt><?php echo $v2['name']; ?></dt>-->
	                <!--<?php if(is_array($v2['child']) || $v2['child'] instanceof \think\Collection || $v2['child'] instanceof \think\Paginator): if( count($v2['child'])==0 ) : echo "" ;else: foreach($v2['child'] as $key=>$v3): ?>-->
	                <!--<dd class="<?php if(!empty($quicklink)){if(in_array($v3['op'].'_'.$v3['act'],$quicklink)){echo 'selected';}} ?>">-->
	                	<!--<i nctype="btn_add_quicklink" data-quicklink-act="<?php echo $v3[op]; ?>_<?php echo $v3[act]; ?>" class="icon-check" title="添加为常用功能菜单"></i>-->
	                	<!--<a href=<?php echo U("$v3[op]/$v3[act]"); ?>> <?php echo $v3['name']; ?> </a>-->
	                <!--</dd>-->
	            	<!--<?php endforeach; endif; else: echo "" ;endif; ?>-->
	             <!--</dl>-->
	            <!--<?php endforeach; endif; else: echo "" ;endif; ?>      -->
              <!--</div>-->
          <!--</div>-->
        <!--</div>-->
      </div>

      
      <a class="iconshop" href="<?php echo U('Admin/modify_pwd',array('seller_id'=>$seller['seller_id'])); ?>" title="修改密码" target="_blank"><i class="icon-wrench"></i>&nbsp;设置</a>
      <a class="iconshop" href="<?php echo U('Admin/logout'); ?>" title="安全退出"><i class="icon-signout"></i>&nbsp;退出</a></div>
    </div>
    <div class="center-logo"> <a href="/" target="_blank">
     
<!--    	<img src="<?php echo (isset($tpshop_config['shop_info_store_logo']) && ($tpshop_config['shop_info_store_logo'] !== '')?$tpshop_config['shop_info_store_logo']:'/public/static/images/logo/pc_home_logo_default.png'); ?>" class="pngFix" alt=""/></a>-->
      <h1>经销商</h1>
    </div>
    <nav class="ncsc-nav">
      <dl <?php if(ACTION_NAME == 'index' AND CONTROLLER_NAME == 'Index'): ?>class="current"<?php endif; ?>>
        <dt><a href="<?php echo U('Index/index'); ?>">首页</a></dt>
        <dd class="arrow"></dd>
      </dl>
      
      <?php if(is_array($menuArr) || $menuArr instanceof \think\Collection || $menuArr instanceof \think\Paginator): if( count($menuArr)==0 ) : echo "" ;else: foreach($menuArr as $kk=>$vo): ?>
      <dl <?php if(ACTION_NAME == $vo[child][0][act] AND CONTROLLER_NAME == $vo[child][0][op]): ?>class="current"<?php endif; ?>>
        <dt><a href="/index.php?m=Seller&c=<?php echo $vo[child][0][op]; ?>&a=<?php echo $vo[child][0][act]; ?>"><?php echo $vo['name']; ?></a></dt>
        <dd>
          <ul>	
          		<?php if(is_array($vo['child']) || $vo['child'] instanceof \think\Collection || $vo['child'] instanceof \think\Paginator): if( count($vo['child'])==0 ) : echo "" ;else: foreach($vo['child'] as $key=>$vv): ?>
                <li> <a href='<?php echo U("$vv[op]/$vv[act]"); ?>'> <?php echo $vv['name']; ?> </a> </li>
				<?php endforeach; endif; else: echo "" ;endif; ?>
           </ul>
        </dd>
        <dd class="arrow"></dd>
      </dl>
      <?php endforeach; endif; else: echo "" ;endif; ?>
	</nav>
  </div>
</header>
<style>
  .w150{
    margin-right: 35px;
  }
  .w378{
    display: inline-block !important;
  }
  .di-in{
    display: inline-block !important;
  }
  .w160{
    width: 160px !important;
  }
  .nscs-table-handle{
    border-right: 1px solid #dedede;
  }
    .output-btn{
        color: white !important;
        background-color: #48CFAE !important;
        font: 14px/36px "microsoft yahei";
        text-align: center;
        min-width: 100px;
        height: 32px;
        border-radius: 2px;
        line-height: 32px;
        cursor: pointer;
    }


</style>
<div class="ncsc-layout wrapper">
   <div id="layoutLeft" class="ncsc-layout-left">
   <div id="sidebar" class="sidebar">
     <div class="column-title" id="main-nav"><span class="ico-<?php echo $leftMenu['icon']; ?>"></span>
       <h2><?php echo $leftMenu['name']; ?></h2>
     </div>
     <div class="column-menu">
       <ul id="seller_center_left_menu">
      	 <?php if(is_array($leftMenu['child']) || $leftMenu['child'] instanceof \think\Collection || $leftMenu['child'] instanceof \think\Paginator): if( count($leftMenu['child'])==0 ) : echo "" ;else: foreach($leftMenu['child'] as $key=>$vu): ?>
           <li class="<?php if(ACTION_NAME == $vu[act] AND CONTROLLER_NAME == $vu[op]): ?>current<?php endif; ?>">
           		<a href="<?php echo U("$vu[op]/$vu[act]"); ?>"> <?php echo $vu['name']; ?></a>
           </li>
	 	<?php endforeach; endif; else: echo "" ;endif; ?>
      </ul>
     </div>
   </div>
 </div>
  <div id="layoutRight" class="ncsc-layout-right">
    <div class="ncsc-path"><i class="icon-desktop"></i>经销商中心<i class="icon-angle-right"></i>订单管理<i class="icon-angle-right"></i>订单列表</div>
    <div class="main-content" id="mainContent">
      
<div class="tabmenu">
  <ul id="tab" class="tab pngFix">
  	<li class="<?php if(is_null(\think\Request::instance()->param('order_status'))): ?>active<?php else: ?>normal<?php endif; ?>" data-val=""><a  href="#">所有订单</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') === 0): ?>active<?php else: ?>normal<?php endif; ?>" data-val="0"><a  href="#">待确认</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') == 1): ?>active<?php else: ?>normal<?php endif; ?>" data-val="1"><a  href="#">已确认</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') == 2): ?>active<?php else: ?>normal<?php endif; ?>" data-val="2"><a  href="#">已收货</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') == 3): ?>active<?php else: ?>normal<?php endif; ?>" data-val="3"><a  href="#">已取消</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') == 4): ?>active<?php else: ?>normal<?php endif; ?>" data-val="4"><a  href="#">已完成</a></li>
  	<li class="<?php if(\think\Request::instance()->param('order_status') == refund): ?>active<?php else: ?>normal<?php endif; ?>" data-val="refund"><a  href="#">退款订单</a></li>
  </ul>
  </div>
<form method="get" class="search-form" action="<?php echo U('order/export_order'); ?>" id="search-form2">
    <input type="hidden" name="order_by" value="order_id"/>
    <input type="hidden" name="sort" value="desc"/>
    <input type="hidden" name="order_status" id="order_status" value="<?php echo \think\Request::instance()->param('order_status'); ?>"/>
    <input type="hidden" value="<?php echo (isset($_GET['order_statis_id']) && ($_GET['order_statis_id'] !== '')?$_GET['order_statis_id']:0); ?>" name="order_statis_id" id="order_statis_id"/>
  <table class="search-form">
    <tr>

      <th>收货人<?php echo $shipping_status_chose; ?></th>
      <td class="w150"><input type="text" class="text w150" name="consignee" placeholder="收货人" value=""/></td>
      <th>订单编号<?php echo $pay_status_chose; ?></th>
      <td class="w150"><input type="text" class="text w150" name="order_sn" placeholder="订单编号" value=""/></td>
      <th>下单时间</th>
      <td class="w378">
	      <input type="text" autocomplete="off" class="text w150" name="start_time" id="start_time" placeholder="开始时间" value="<?php echo $start_time; ?>"/>
	      <input type="text" autocomplete="off" class="text w150" name="end_time" id="end_time" placeholder="结束时间" value="<?php echo $end_time; ?>"/>
	   </td>
     </tr>
      <!--32-->
     <tr>
      <th>支付状态</th>
      <td class="w160">
          <select id="pay" name="pay_status" class="w150 w160">
              <option value="">请选择</option>
              <option value="0" <?php if(\think\Request::instance()->param('pay_status') === '0'): ?>selected='selected'<?php endif; ?>>未支付</option>
              <option value="1" <?php if(\think\Request::instance()->param('pay_status') == 1): ?>selected='selected'<?php endif; ?>>已支付</option>
          </select>
      </td>
      <th>发货状态</th>
      <td class="w160">
      		<select id="shipping" name="shipping_status" class="w150 w160">
          		  <option value="">请选择</option>
                <option value="0" <?php if(\think\Request::instance()->param('shipping_status') === '0'): ?>selected='selected'<?php endif; ?>>未发货</option>
               	<option value="1" <?php if(\think\Request::instance()->param('shipping_status') == 1): ?>selected='selected'<?php endif; ?>>已发货</option>

          </select>
      </td>
         <th>支付方式</th>
         <td><select name="pay_code" class="w150 w160 di-in">
             <option value="">请选择</option>
             <option value="ali">支付宝支付</option>
             <option value="wx">微信支付</option>
             <option value="wap">快钱移动端</option>
             <!--<option value="cod">货到付款</option>-->
             <option value="kq">快钱</option>
             <option value="HBFQ">花呗分期</option>
             <option value="CCFQ">信用卡分期</option>
         </select>

         </td>
      
    </tr>
    <tr>
        <th>订单类型</th>
        <td class="w160">
            <select id="pay" name="prom_type" class="w150 w160">
                <option value="">请选择</option>
                <option value="0" <?php if(\think\Request::instance()->param('prom_type') === '0'): ?>selected='selected'<?php endif; ?>>普通订单</option>
                <option value="4" <?php if(\think\Request::instance()->param('prom_type') == 4): ?>selected='selected'<?php endif; ?>>预约订单</option>
                <option value="register" <?php if(\think\Request::instance()->param('prom_type') == 'register'): ?>selected='selected'<?php endif; ?>>注册券订单</option>
                <option value="recycle" <?php if(\think\Request::instance()->param('prom_type') == 'recycle'): ?>selected='selected'<?php endif; ?>>回收券订单</option>
            </select>
        </td>
        <th>店铺</th>
        <td class="w150">
            <select name="store_id" class="w150">
                <option value="">请选择</option>
                <?php if(is_array($stores) || $stores instanceof \think\Collection || $stores instanceof \think\Paginator): if( count($stores)==0 ) : echo "" ;else: foreach($stores as $key=>$store): ?>
                    <option value="<?php echo $store['store_id']; ?>" ><?php echo $store['store_name']; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

        </td>


        <th style="width: 120px">店铺名搜索</th>
        <td class="w200"><input type="text" autocomplete="off" class="text w150"  name="store_title" value="<?php echo $store_name; ?>" placeholder="输入店铺名称" /></td>

    </tr>
      <tr style="text-align: right">
          <th></th>
          <td></td>
          <th></th>
          <th></th>
          <th></th>
          <td  style="display: flex">

              <label class="submit-border"><input class="submit" value="搜索" style="width: 100px" onclick="ajax_get_table('search-form2',1)" type="button"></label>
              <input name="order_id_arr" id="order_id_arr" type="hidden" />
              <div onclick="outputFun()"  class="ncbtn-mini output-btn"  style="width: 100px;display: inline-block;margin-left: 10px" >导出订单</div>
          </td>
          <!--<td class="w100" style="text-align: right">-->
              <!---->

          <!--</td>-->
      </tr>
  </table>
</form>
        <div id='prompt-box'></div>
<div id="ajax_return">
	 
</div>
        <script type="text/javascript" src="/public/js/seller/prompt.fun.js"></script>
        <script>
            $("input[name='store_title']").promptFun({
                table:'store',
                field:'store_name,store_id',
                column:'store_name',
                w:180,
                len:10,
                call:function () {
                    ajax_get_table('search-form2',1)
                }

            })
        </script>
<script>
$(document).ready(function(){	
	   
 	// $('#start_time').layDate();
 	// $('#end_time').layDate();
 	 
 	ajax_get_table('search-form2',1);
 	
 	$("#tab > li").each(function(){
		$(this).click(function(){
			tabSelect(this);
		});
	});
 
    // 起始位置日历控件
	laydate.skin('molv');//选择肤色
	laydate({
	  elem: '#start_time',
	  format: 'YYYY-MM-DD', // 分隔符可以任意定义，该例子表示只显示年月
	  festival: true, //显示节日
        // value: '2018-08-18',
	  choose: function(datas){ //选择日期完毕的回调
		 // compare_time($('#start_time').val(),$('#end_time').val());
	  }
	});

	 // 结束位置日历控件
	laydate({
	  elem: '#end_time',
	  format: 'YYYY-MM-DD', // 分隔符可以任意定义，该例子表示只显示年月
	  festival: true, //显示节日
	  istime: false,
        // value: '2018-08-18',
	  choose: function(datas){ //选择日期完毕的回调
          console.log(datas)
		   compare_time($('#start_time').val(),$('#end_time').val());
	  }
	});
});

function tabSelect(obj){
	var currHasClass = $(obj).hasClass('active');
	if(currHasClass)return;
	
	$("#tab > li").each(function(){
		$(this).removeClass('active');
	});
	
	$(obj).addClass('active');
	var orderStatus = $(obj).attr("data-val");
	$("#order_status").val(orderStatus);
	ajax_get_table('search-form2',1);
}


function compare_time2(time1,time2)
{
    var time1 = time1.replace(/[\-:\s]/g, "");
    var time2 = time2.replace(/[\-:\s]/g, "");

    // if(time1.substring(0,4) != time2.substring(0,4))
    // {
    // layer.msg('不能跨年度查询!', {
    // 	//icon: 1,   // 成功图标
    // 	time: 2000 //2秒关闭（如果不配置，默认是3秒）
    // });
    //  return false;
    // }

    time1 = time1.substring(0,6);
    time2 = time2.substring(0,6);
    if((parseInt(time2) - parseInt(time1)) < 0 )
    {

        return false;
    }else{
        return true;
    }


}

function ajax_get_table(tab,page){

    if(!compare_time2($('#start_time').val(),$('#end_time').val())){
        layer.msg('开始时间不能大于结束时间!', {
            //icon: 1,   // 成功图标
            time: 2000 //2秒关闭（如果不配置，默认是3秒）
        });
        return;
    };

    var pay = $('#pay').val();
    var shipping = $('#shipping').val();
    var order_statis_id = $('#order_statis_id').val();
	cur_page = page; //当前页面 保存为全局变量
	var ajaxUrl = "/index.php/Seller/order/ajaxindex/p/"+page;
	if(order_statis_id>0){
		ajaxUrl = "/index.php/Seller/order/ajaxindex/p/"+page+"order_statis_id/"+order_statis_id+'/pay/'+pay+'/shipping/'+shipping;
	}
    $.ajax({
        type : "POST",
        url: ajaxUrl,
        data : $('#'+tab).serialize(),// 你的formid
        success: function(data){
            $("#ajax_return").html('');
            $("#ajax_return").append(data);
        }
    });
}



function outputFun() {
    var ids='';
    $("input.js-sign[type='checkbox']:checkbox:checked").each(function(){
        ids+=$(this).val()+',';
    })
    $('#order_id_arr').val(ids);
    $('#search-form2').submit()
}


var url='<?php echo U("Order/order_print"); ?>';
$("#S1").click(function(){ 
  var ids='';
  $("input[type='checkbox']:checkbox:checked").each(function(){ 
    ids+=$(this).val()+',';
  }) 
  url+='/ids/'+ids+'/template/picking';
  window.open(url); 
}) 
</script>    
</div>
  </div>
</div>
<div id="cti">
    <div class="wrapper">
        <ul>
        </ul>
    </div>
</div>
<div id="faq">
    <div class="wrapper">
    </div>
</div>

<div id="footer">
    <p>
        <?php $i = 1;
                                   
                                $md5_key = md5("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC");
                                $result_name = $sql_result_vv = S("sql_".$md5_key);
                                if(empty($sql_result_vv))
                                {                            
                                    $result_name = $sql_result_vv = \think\Db::query("SELECT * FROM `__PREFIX__navigation` where is_show = 1 AND position = 3 ORDER BY `sort` DESC"); 
                                    S("sql_".$md5_key,$sql_result_vv,31104000);
                                }    
                              foreach($sql_result_vv as $kk=>$vv): if($i > 1): ?>|<?php endif; ?>
            <a href="<?php echo $vv[url]; ?>"
            <?php if($vv[is_new] == 1): ?> target="_blank"<?php endif; ?>
            ><?php echo $vv[name]; ?></a>
            <?php $i++; endforeach; ?>
        <!--<a href="/">首页</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">招聘英才</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">合作及洽谈</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">联系我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">关于我们</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">物流自取</a>-->
        <!--| <a  href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">友情链接</a>-->
    </p>
    Copyright 2017 北京润泽金诚科技有限公司
    All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;服务热线：400-681-5866<br/>

</div>
<script type="text/javascript" src="/public/static/js/jquery.cookie.js"></script>
<link href="/public/static/js/perfect-scrollbar.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/public/static/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="/public/static/js/qtip/jquery.qtip.min.js"></script>
<link href="/public/static/js/qtip/jquery.qtip.min.css" rel="stylesheet" type="text/css">
<div id="tbox">

    <div class="btn" id="gotop" style="display: block;"><i class="top"></i><a href="javascript:void(0);">返回顶部</a></div>
</div>

<script type="text/javascript">
    var current_control = '<?php echo CONTROLLER_NAME; ?>/<?php echo ACTION_NAME; ?>';
    $(document).ready(function () {
        //添加删除快捷操作
        $('[nctype="btn_add_quicklink"]').on('click', function () {
            var $quicklink_item = $(this).parent();
            var item = $(this).attr('data-quicklink-act');
            if ($quicklink_item.hasClass('selected')) {
                $.post("<?php echo U('Seller/Index/quicklink_del'); ?>", {item: item}, function (data) {
                    $quicklink_item.removeClass('selected');
                    var idstr = 'quicklink_' + item;
                    $('#' + idstr).remove();
                }, "json");
            } else {
                var scount = $('#quicklink_list').find('dd.selected').length;
                if (scount >= 8) {
                    layer.msg('快捷操作最多添加8个', {icon: 2, time: 2000});
                } else {
                    $.post("<?php echo U('Seller/Index/quicklink_add'); ?>", {item: item}, function (data) {
                        $quicklink_item.addClass('selected');
                        if (current_control == 'Index/index') {
                            var $link = $quicklink_item.find('a');
                            var menu_name = $link.text();
                            var menu_link = $link.attr('href');
                            var menu_item = '<li id="quicklink_' + item + '"><a href="' + menu_link + '">' + menu_name + '</a></li>';
                            $(menu_item).appendTo('#seller_center_left_menu').hide().fadeIn();
                        }
                    }, "json");
                }
            }
        });
        //浮动导航  waypoints.js
        $("#sidebar,#mainContent").waypoint(function (event, direction) {
            $(this).parent().toggleClass('sticky', direction === "down");
            event.stopPropagation();
        });
    });
    // 搜索商品不能为空
    $('input[nctype="search_submit"]').click(function () {
        if ($('input[nctype="search_text"]').val() == '') {
            return false;
        }
    });

    function fade() {
        $("img[rel='lazy']").each(function () {
            var $scroTop = $(this).offset();
            if ($scroTop.top <= $(window).scrollTop() + $(window).height()) {
                $(this).hide();
                $(this).attr("src", $(this).attr("data-url"));
                $(this).removeAttr("rel");
                $(this).removeAttr("name");
                $(this).fadeIn(500);
            }
        });
    }

    if ($("img[rel='lazy']").length > 0) {
        $(window).scroll(function () {
            fade();
        });
    }
    ;
    fade();

    function delfunc(obj) {
        layer.confirm('确认删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                // 确定
                $.ajax({
                    type: 'post',
                    url: $(obj).attr('data-url'),
                    data: {act: 'del', del_id: $(obj).attr('data-id')},
                    dataType: 'json',
                    success: function (data) {
                        layer.closeAll();
                        if (data == 1) {
                            layer.msg('操作成功', {icon: 1, time: 1000}, function () {
                                window.location.href = '';
                            });
                        } else {
                            layer.msg(data, {icon: 2, time: 2000});
                        }
                    }
                })
            }, function (index) {
                layer.close(index);
                return false;// 取消
            }
        );
    }


</script>
</body>
</html>
