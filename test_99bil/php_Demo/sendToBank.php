<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<link href="../../../style.css" rel="stylesheet" type="text/css" />
<title>API付款到银行卡</title>

</head>
<body style="text-align:left;">
<?php
if($_POST[go_pay]){

$clientObj = new SoapClient('https://sandbox.99bill.com/webapp/services/BatchPayWS?wsdl');

//  取得 FORM 提交 数据  ======= 开始
$province_city=$_POST[province_city];
$bankName=$_POST[bankName];
$kaihuhang=$_POST[kaihuhang];
$creditName=$_POST[creditName];
$bankCardNumber=$_POST[bankCardNumber];
$amount=$_POST[amount];
$description=$_POST[description];
$orderId=$_POST[orderId];

$mer_id="10012138842";
$mer_ip="222.190.107.178";


	$key=$_POST[key];

	$kq_para=$bankCardNumber.$amount.$orderId.$key;
	$mac=strtoupper(md5($kq_para));


//  取得 FORM 提交 数据  ======= 结束

	$para[province_city]=$province_city;
	$para[bankName]=$bankName;
	$para[kaihuhang]=$kaihuhang;
	$para[creditName]=$creditName;
	$para[bankCardNumber]=$bankCardNumber;
	$para[amount]=$amount;
	$para[description]=$description;
	$para[orderId]=$orderId;	

	$para[mac]=$mac;
	

try {
	//  开始 读取 WEB SERVERS 上的 数据
	 //    $result=$clientObj->__soapCall('simplePay',array($para),$mer_id,$mer_ip);
	//$clientObj->simplePay(array($para),$mer_id,$mer_ip);

	$result=$clientObj->__soapCall("bankPay", array(array($para),$mer_id,$mer_ip));
				// 将 返回 的 数据 转为 数组的函数
			function object_array($array)
			{
			   if(is_object($array))
			   {
				$array = (array)$array;
			   }
			   if(is_array($array))
			   {
				foreach($array as $key=>$value)
				{
				 $array[$key] = object_array($value);
				}
			   }
			   return $array;
			}

	print_r($result);




	
} catch (SOAPFault $e) {
    print_r('Exception:'.$e);
}


}else{

}
?>

<BR>
<BR>
* 表示必填写
<BR><BR>
<form method=post action="" name="" >
	<table cellspacing="0" cellpadding="10" border="0" >
		<tr>
			<td style="width:300px">城市 * </td>
			<td><input type="text" name="province_city" value="北京"></td>
			<td>中文字符 主要只需要城市名，不需要省份名。也不要带“市” “自治区（县）”等。</td>
		</tr>
		<tr>
			<td>银行名称 *</td>
			<td><input type="text" name="bankName" value="工商银行"></td>
			<td>中文字符 请填写银行的标准名称。详见下文支持银行列表</td>
		</tr>
		<tr>
			<td>开户行 * </td>
			<td><input type="text" name="kaihuhang" value="南京支行"></td>
			<td>中文字符 银行卡开户行的名称</td>
		</tr>
		<tr>
			<td>收款人姓名 * </td>
			<td><input type="text" name="creditName" value="上海"></td>
			<td>中文字符 收款人的姓名，必须与银行卡账户名相同</td>
		</tr>
		<tr>
			<td>银行卡号 *</td>
			<td><input type="text" name="bankCardNumber" value="6225881256252189"></td>
			<td>中文字符 收款人的银行卡卡号</td>
		</tr>
		<tr>
			<td>交易金额 *</td>
			<td><input type="text" name="amount" value="2.8"></td>
			<td>整数或小数 小数为两位以内。单位为人民币元</td>
		</tr>
		<tr>
			<td>交易备注 </td>
			<td><input type="text" name="description" value="test pay"></td>
			<td></td>
		</tr>
		<tr>
			<td>订单号 * </td>
			<td><input type="text" name="orderId" value="<?php echo (date("YmdHis"));?>"></td>
			<td>字符串由字母、数字或 - 、_组成首字符需要是字母或数字</td>
		</tr>
		<tr>
			<td>KEY * </td>
			<td><input type="text" name="key" value="J8A4CKE7H3HE8NUZ" ></td>
			<td>merchant key</td>
		</tr>

	</table>
	

	<input type="submit" value="提交结果" name="go_pay" style="font-size:32px;padding:10px;font-weight:bold;font-family:arail">
</form>

</body>