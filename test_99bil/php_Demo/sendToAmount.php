<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<link href="../../../style.css" rel="stylesheet" type="text/css" />
<title>API付款到快钱账户</title>

</head>
<body style="text-align:left;">
<?php
if($_POST[go_search]){

// 提交地址
$clientObj = new SoapClient('https://sandbox.99bill.com/webapp/services/BatchPayWS?wsdl');

//  取得 FORM 提交 数据  ======= 开始
$creditName=$_POST[creditName];
$creditContact=$_POST[creditContact];
$currencyCode=$_POST[currencyCode];
$amount=$_POST[amount];
$description=$_POST[description];
$correctName=$_POST[correctName];
$tempAccount=$_POST[tempAccount];
$orderId=$_POST[orderId];
$key=$_POST[key];

$mer_id="10012138842";
$mer_ip="192.168.8.1";
//  取得 FORM 提交 数据  ======= 结束


//  判断 参数 是不是 为 空 ===== 结束

	$kq_all_para=$creditContact.$currencyCode.$amount.$orderId.$key;
	//echo '$kq_all_para='.$kq_all_para;
	//echo '<BR><BR>';

	$mac=strtoupper(md5($kq_all_para));	

	$para[creditName]=$creditName;
	$para[creditContact]=$creditContact;
	$para[currencyCode]=$currencyCode;
	$para[amount]=$amount;
	$para[description]=$description;
	$para[correctName]=$correctName;
	$para[tempAccount]=$tempAccount;
	$para[orderId]=$orderId;
	$para[mac]=$mac;


//print_r($para);
//echo '<BR><BR>';	
	
try {
	//  开始 读取 WEB SERVERS 上的 数据
     $result=$clientObj->__soapCall('simplePay', array(array($para),$mer_id,$mer_ip));
	 
	 //echo '$mer_ip='.$mer_ip;
	 //echo '<BR><BR>';	
	 
	// 将 返回 的 数据 转为 数组的函数
			function object_array($array)
			{
			   if(is_object($array))
			   {
				$array = (array)$array;
			   }
			   if(is_array($array))
			   {
				foreach($array as $key=>$value)
				{
				 $array[$key] = object_array($value);
				}
			   }
			   return $array;
			}

	print_r($result);
	//  输出 数组 各个订单数据==  结束
	
} catch (SOAPFault $e) {
    print_r('Exception:'.$e);
}
}else{

}
?>

<BR>
* 表示必填写
<BR><BR>
<form method=post action="" name="" >
	<table cellspacing="0" cellpadding="10" border="0" >
		<tr>
			<td>收款方姓名 * </td>
			<td><input type="text" name="creditName" value="李梦丽"></td>
			<td>中文或英文</td>
		</tr>
		<tr>
			<td>收款方快钱账户 * </td>
			<td><input type="text" name="creditContact" value="1971412292@qq.com"></td>
			<td>收款方的Email 或手机号</td>
		</tr>
		<tr>
			<td>货币类型 * </td>
			<td><input type="text" name="currencyCode" value="1"></td>
			<td>1 代表人民币</td>
		</tr>
		<tr>
			<td>交易金额 * </td>
			<td><input type="text" name="amount" value="10"></td>
			<td>整数或小数数字 以人民币“元”为单位</td>
		</tr>
		<tr>
			<td>交易备注 * </td>
			<td><input type="text" name="description" value="付给雪梅账户"></td>
			<td>字符串</td>
		</tr>
		<tr>
			<td>验 证收款方姓名标志 * </td>
			<td><input type="text" name="correctName" value="n"></td>
			<td>y：需要验证收款方姓名 n：不需要验证</td>
		</tr>
		<tr>
			<td>收 款方还不是快钱用户时是否付款标志 * </td>
			<td><input type="text" name="tempAccount" value="n"></td>
			<td>y：可以对非快钱用户直接付款 n：不能对非快钱用户付款</td>
		</tr>
		<tr>
			<td>订单号 * </td>
			<td><input type="text" name="orderId" value="<?php echo (date("YmdHis"));?>"></td>
			<td>数字串</td>
		</tr>
		<tr>
			<td>商家 KEY * </td>
			<td><input type="text" name="key" value="J8A4CKE7H3HE8NUZ"></td>
			<td>数字串</td>
		</tr>
	</table>
	

	<input type="submit" value="查看查询结果" name="go_search" style="font-size:32px;padding:10px;font-weight:bold;font-family:arail">
</form>

</body>
</html>