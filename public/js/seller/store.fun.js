/**
 * 三级联动筛选店铺
 *
 */

function emptyObject(obj) {
    for (var prop in obj) {
        if (obj[prop] === '' || obj[prop] === undefined || obj[prop] === null || obj[prop] === 'null' || obj[prop] === 'undefined') {
            delete obj[prop];
        }
    }
    return obj;
}

function in_array(search,array){
    for(var i in array){
        if(array[i]==search){
            return true;
        }
    }
    return false;
}

//数组的情况
function in_array_b(search,array){
    for(var i in array){
        if(array[i].store_id==search.store_id){
            return true;
        }
    }
    return false;
}

/**
 * 存储localStorage
 */
function setStore(name, content){
    if (!name) return;
    if (typeof content !== 'string') {
        content = JSON.stringify(content);
    }
    window.localStorage.setItem(name, content);
}

/**
 * 获取localStorage
 */
function getStore(name,format){
    if (!name) return '';
    var str = window.localStorage.getItem(name);
    if(format){
        if(!str){
            return null;
        }
        str = JSON.parse(str);
    }
    return str;
}




$.fn.storeFun = function (options) {


    var defaults = {
        isHas:''
    };

    //初始化的变量
    var history = [],//最近选择记录
        title = '',//快速查找
        seller_list = [],//经销商列表
        seller_address_list=[],//门店地址列表
        store_list = [];//店铺列表


    //记录已经选择的
    var select_seller_list = [],
        select_seller_address_list= [],
        select_store_list = [];


    var settings = $.extend(defaults, options);
    if(!settings.form){
        layer.msg('form初始化参数错误', {icon: 2, time: 2000});
    }

    var $ele = $(this),
        $box = $(this).find('#box'),
        $form = $(settings.form);//这里由配置给吧





    function init_data() {


        var index = layer.load(0, {shade: [0.3, '#000000']});

        $.post("/index.php/Seller/Store/loadAllStoreInfo",{

        },function (res) {

            layer.close(index);

            var res = JSON.parse(res);


            if(res.status!=1){
                layer.msg(res.msg||'请求失败', {icon: 2, time: 2000});
                return;
            }

            var data = res.data;

            //赋值给三个列表
            seller_list = data.seller_list;
            seller_address_list = data.seller_address_list;
            store_list = data.store_list;



            init_dom();

        })


    }

    function init_dom() {

        //1.创建最近选择
        var history_dom = "<div><span style='margin: 0 15px'>最近选择</span><select style='width: 320px' id='history_select' name='history_select'></select></div>";

        //2.创建快速查找
        var search_dom = "<div style='margin: 15px 0 10px;' class='flex'><span style='margin: 0 15px'>快速查找</span><input style='width: 320px' type='text' name='search_store_title' /><a id='search_store_btn' href='javascript:void(0)'  style='margin: 0 15px' class='ncbtn ncbtn-mint'>搜索</a></div>"

        //3.创建三个联动的box
        var box_seller = "<div class='seller-box box-item'>" +
            // "<div class='filter '><input disabled type='text' class='filter-input js-seller-filter' /></div>"+
            "<div class='taglle-all-check'><label><input disabled data-link='.js-seller-select' type='checkbox' name='seller_all_select' class='js-all-select' value='1' />全部选择</label></div>"+
            "<div class='seller-select-box select-box'>" +
            "<ul class='js-seller-select'></ul>"
            "</div>"+
            "</div>";

        var box_address = "<div class='address-box box-item'>" +
            // "<div class='filter '><input type='text' class='filter-input js-address-filter' /></div>"+
            "<div class='taglle-all-check'><label><input data-link='.js-address-select' type='checkbox' name='address_all_select' class='js-all-select' value='1' />全部选择</label></div>"+
            "<div class='address-select-box select-box'>" +
            "<ul class='js-address-select'></ul>"
        "</div>"+
        "</div>";

        var box_store = "<div class='store-box box-item'>" +
            // "<div class='filter '><input type='text' class='filter-input js-store-filter' /></div>"+
            "<div class='taglle-all-check'><label><input data-link='.js-store-select' type='checkbox' name='store_all_select' class='js-all-select' value='1' />全部选择</label></div>"+
            "<div class='store-select-box select-box'>" +
            "<ul class='js-store-select '></ul>"
        "</div>"+
        "</div>";

        var select_box = "<div id='select-box'><div style='margin: 0 15px'>已经选择</div><div id='select-store-box'></div></div>"

        $ele.prepend($(search_dom));
        $ele.prepend($(history_dom));


        $(box_seller).appendTo($box);
        $(box_address).appendTo($box);
        $(box_store).appendTo($box);

        $ele.append($(select_box));


        //初始化下拉菜单
        if(getStore('SELECT_HISTORY')){
            history = getStore('SELECT_HISTORY',1);

            var options = "<option>请选择</option>"
            if(history&&Object.prototype.toString.call(history)  === '[object Array]'){
                for(var i=0;i<history.length;i++){

                    options+= "<option value='"+history[i].store_id+"'>"+(i+1)+'.'+history[i].seller_name+">>"+history[i].address+'>>'+history[i].store_name+"</option>"
                }
            }
            console.log(options)
            $('#history_select').html(options);

            $("#history_select").change(function(){

                var store_id = $(this).val();

                var store_info = null;
                for(var i=0;i<store_list.length;i++){
                    if(store_id==store_list[i].store_id){
                        store_info = store_list[i];
                        break;
                    }
                }
                console.log(store_info,select_store_list)

                if(!in_array_b(store_info,select_store_list)){
                    select_store_list.push(store_info);
                }
                console.log(select_store_list)

                buildHtmlSelectStore(select_store_list)


            });


        }

        init_seller_data();
        init_address_data();

        init_event();

        init_store_data(store_list);

        $('input[name=store_all_select]').bind('click',function (e) {
            $input = $(this);
            console.log($input)

            $li = $($input.data('link')).find('li');
            console.log($li)
            if($input.hasClass('active')){
                $input.removeClass('active');
                $li.removeClass('active');
            }else{
                $input.addClass('active');
                $li.addClass('active');


            }


            refreshSelectStore(1);



        })


        //初始化的时候，如果isHas有数据，则就需要先来一波渲染了

        if(settings.isHas){
            var isHasStoreArr = settings.isHas.split(',');
            if(Object.prototype.toString.call(isHasStoreArr)  != '[object Array]'||isHasStoreArr.length<1)return;

            for(var i=0;i<store_list.length;i++){
                if(in_array(store_list[i].store_id,isHasStoreArr)){
                    select_store_list.push(store_list[i]);
                }
            }

            buildHtmlSelectStore(select_store_list)

        }








    }

    //店铺名字搜索
    function searchStoreTitle() {
        var resultStoreList = [],title = $("input[name='search_store_title']").val();

        if(!title)return;
        for(var i=0;i<store_list.length;i++){
            if(store_list[i].store_name.indexOf(title)>=0){
                resultStoreList.push(store_list[i]);
            }
        }

        init_store_data(resultStoreList);
    }

    /**
     * 获取所有选中的地址id
     */
    function getAddressSelectIds() {
        var $selectAddressLiArr = $('.js-address-select li.active'),ids=[];
        for(var i=0;i<$selectAddressLiArr.length;i++){
            ids.push($($selectAddressLiArr[i]).data('value'));
        }
        return ids;
    }


    function buildHtmlSelectStore(select_store_list_data) {

        //如果不做长度判断，就会有重复的元素。应该是slice方法的特性
        if(select_store_list_data.length>3){
            setStore('SELECT_HISTORY',select_store_list_data.slice(0,3));
        }else{
            setStore('SELECT_HISTORY',select_store_list_data);
        }


        list = [];
        for(var i =0;i<select_store_list_data.length;i++){
            if(!list[select_store_list_data[i].seller_id]){
                list[select_store_list_data[i].seller_id] = [];
            }
            if(!list[select_store_list_data[i].seller_id][select_store_list_data[i].seller_address_id]){
                list[select_store_list_data[i].seller_id][select_store_list_data[i].seller_address_id] = [];
            }
            if(!in_array_b(select_store_list_data[i],list[select_store_list_data[i].seller_id][select_store_list_data[i].seller_address_id])){
                list[select_store_list_data[i].seller_id][select_store_list_data[i].seller_address_id].push(select_store_list_data[i]);
            }

        }
        console.log(list);

        var alllen =0;
        for(var i in list){

            for(var j in list[i]){


                for(var k in list[i][j]){
                    alllen++;
                }


            }


        }


        var html = "<table><tr><th>一级分类</th><th>二级分类</th><th>三级分类</th></tr>";

        var ii=0;
        for(var i in list){

            var jj=0;
            for(var j in list[i]){


                var kk=0;
                for(var k in list[i][j]){




                    if(kk===0){

                        if(ii==0&&jj==0&&kk==0){
                            html+="<tr><td rowspan="+(alllen+1)+" >"+list[i][j][0].seller_name+"<span class='del js-del-span' data-action='seller' data-id='"+list[i][j][k].seller_id+"'>x</span><td rowspan="+list[i][j].length+">"+list[i][j][k].address+"<span class='del js-del-span' data-action='address' data-id='"+list[i][j][k].seller_address_id+"'>x</span></td><td>"+list[i][j][k].store_name+"<span class='del js-del-span' data-action='store' data-id='"+list[i][j][k].store_id+"'>x</span></td></tr>";
                        }else{
                            html+="<tr><td rowspan="+list[i][j].length+">"+list[i][j][k].address+"<span class='del js-del-span' data-action='address' data-id='"+list[i][j][k].seller_address_id+"'>x</span></td><td>"+list[i][j][k].store_name+"<span class='del js-del-span' data-action='store' data-id='"+list[i][j][k].store_id+"'>x</span></td></tr>";
                        }

                    }else{
                        html+="<tr><td>"+list[i][j][k].store_name+"<span class='del js-del-span' data-action='store' data-id='"+list[i][j][k].store_id+"'>x</span></td></tr>";

                    }


                    html+= "<input name='store[]' type='hidden' value='"+list[i][j][k].store_id+"' />"

                    kk++;





                }
                jj++;


            }
            ii++;


        }
        html += "</table>";

        $('#select-store-box').html(html);

        //每次新增都要重新定义事件
        $('.js-del-span').click(function () {

            var action = $(this).data('action'),id=$(this).data('id');

            var list = [];

            switch (action){
                case 'seller':
                    for(var i=0;i<select_store_list_data.length;i++){
                        if(select_store_list_data[i].seller_id!=id){
                            list.push(select_store_list_data[i]);
                        }
                    }
                    break;
                case 'address':
                    for(var i=0;i<select_store_list_data.length;i++){
                        if(select_store_list_data[i].seller_address_id!=id){
                            list.push(select_store_list_data[i]);
                        }
                    }
                    break;
                case 'store':
                    for(var i=0;i<select_store_list_data.length;i++){
                        if(select_store_list_data[i].store_id!=id){
                            list.push(select_store_list_data[i]);
                        }
                    }
                    break;
            }

            //这里要去修改
            select_store_list = list;


            buildHtmlSelectStore(select_store_list);


            var ids=[];
            $(".js-del-span[data-action='store']").each(function (idx,item) {
                console.log(item)
                ids.push($(item).data('id'));
            })

            console.log(ids)
            $(".js-store-select li").each(function (idx,item) {
                console.log(item)
                if(!in_array($(item).data('storeid'),ids)&&$(item).hasClass('active')){
                    $(item).removeClass('active')
                }
            })

            var ids2=[];
            $(".js-del-span[data-action='address']").each(function (idx,item) {
                console.log(item)
                ids2.push($(item).data('id'));
            })

            console.log(ids2)
            $(".js-address-select li").each(function (idx,item) {
                console.log(item)
                if(!in_array($(item).data('value'),ids2)&&$(item).hasClass('active')){
                    $(item).removeClass('active')
                }
            })

            // var ids3=[];
            // $(".js-del-span[data-action='seller']").each(function (idx,item) {
            //     console.log(item)
            //     ids3.push($(item).data('id'));
            // })
            //
            // console.log(ids3)
            // $(".js-seller-select li").each(function (idx,item) {
            //     console.log(item)
            //     if(!in_array($(item).data('value'),ids3)&&$(item).hasClass('active')){
            //         $(item).removeClass('active')
            //     }
            // })
            


        })


    }


    /**
     * 刷新已经选中的店铺
     */
    function refreshSelectStore(clear) {

        if(clear){
            select_store_list=[];
        }



        var $selectStoreLiArr = $('.js-store-select  li.active'),ids=[];
        for(var i=0;i<$selectStoreLiArr.length;i++){

            ids.push($($selectStoreLiArr[i]).data('storeid'));

        }

        //最终写入到这个变量里面去
        for(var i=0;i<store_list.length;i++){

            if(in_array(store_list[i].store_id,ids)){
                select_store_list.push(store_list[i]);
            }

        }

        //渲染已经筛选的
        buildHtmlSelectStore(select_store_list);





    }





    /**
     * 每次点击地址都选中
     */
    function refreshSelectAddress() {

         var ids = getAddressSelectIds();
         if(ids.length==0)return;

         var temp_select_store_list = [];

         for(var i=0;i<store_list.length;i++){

             if(in_array(store_list[i].seller_address_id,ids)){
                 temp_select_store_list.push(store_list[i]);
                 // if(!in_array_b(store_list[i],select_store_list)){
                 //
                 // }

             }
         }



         init_store_data(temp_select_store_list);

    }


    /**
     * 初始化各类事件
     */
    function init_event() {

        $('#search_store_btn').bind('click',searchStoreTitle);

        $("input[name='search_store_title']").bind('input propertychange',searchStoreTitle);





    }

    function init_seller_data() {
        var list = seller_list,html ='',$seller_select = $('.js-seller-select');

        list.map(function (seller) {
            html += "<li class='active'  data-value='"+seller.seller_id+"'>"+seller.seller_name+"</li>"
        })

        $seller_select.html(html);

    }

    var init_address_data = function () {
        var list = seller_address_list,html ='',$address_select = $('.js-address-select');
        list.map(function (address) {

            html += "<li data-value='"+address.seller_address_id+"'>"+(address.title?address.title:address.address)+"</li>"
        })

        $address_select.html(html);

        $('.js-address-select li').bind('click',function (e) {
            $li = $(this);
            if($li.hasClass('active')){
                $(this).removeClass('active');
            }else{
                $(this).addClass('active');
            }

            refreshSelectAddress()



        })

        $('input[name=address_all_select]').bind('click',function (e) {
            $input = $(this);
            console.log($input)

            $li = $($input.data('link')).find('li');
            if($input.hasClass('active')){
                $input.removeClass('active');
                $li.removeClass('active');
            }else{
                $input.addClass('active');
                $li.addClass('active');
            }

            refreshSelectAddress()

        })

    }

    function init_store_data(store_list_data) {

        var list = store_list_data,html ='',$store_select = $('.js-store-select');
        var isHasStoreArr = settings.isHas.split(',');


        list.map(function (store) {

            if(in_array(store.store_id,isHasStoreArr)||in_array_b(store,select_store_list)){
                html += "<li class='active' data-seller_id='"+store.seller_id+"' data-seller_address_id='"+store.seller_address_id+"'  data-storeid='"+store.store_id+"' >"+store.store_name+"</li>";
            }else{
                html += "<li data-seller_id='"+store.seller_id+"' data-seller_address_id='"+store.seller_address_id+"'  data-storeid='"+store.store_id+"' >"+store.store_name+"</li>";
            }


        })

        $store_select.html(html);


        $('.js-store-select li').bind('click',function (e) {
            $li = $(this);
            if($li.hasClass('active')){
                $(this).removeClass('active');
                refreshSelectStore(1);

            }else{
                $(this).addClass('active');
                refreshSelectStore(0);


            }









        })




    }



    init_data();




    function selectStore(store_id) {
        hideBox()
        $("select[name='store_id']").val(store_id);

    }



    //默认回调
    defaults.init = function (res) {





    }









}