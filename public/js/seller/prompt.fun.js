/**
 * 根据查询结果自动提示插件
 * @param options
 */

function emptyObject(obj) {
    for (var prop in obj) {
        if (obj[prop] === '' || obj[prop] === undefined || obj[prop] === null || obj[prop] === 'null' || obj[prop] === 'undefined') {
            delete obj[prop];
        }
    }
    return obj;
}









$.fn.promptFun = function (options) {


    var defaults = {
        table: '',//表名
        column:'',//模糊查询条件
        field:'',//需要返回的字段名,不写就是全部返回
        w:100,//容器宽度
        len:10//显示答案长度
    };

    var ele = $(this),$box = $("#prompt-box"),$form = $('.search-form');


    console.log($form)

    function selectStore(store_id) {
        hideBox()
        $("select[name='store_id']").val(store_id);


    }

    function showBox() {
        $box.css('z-index',999).css('visibility','visible');;
    }

    function hideBox() {

        $box.css('z-index',-222).css('visibility','hidden');
    }



    //默认回调
    defaults.init = function (res) {



        console.log(res)

        var storeList = [];
        storeList = storeList.concat(res);
        // storeList = storeList.concat(res);
        // storeList = storeList.concat(res);
        // storeList = storeList.concat(res);
        // storeList = storeList.concat(res);
        // storeList = storeList.concat(res);

        //获取自己的位置
        var X = ele.offset().top;
        var Y = ele.offset().left;
        var h = ele.outerHeight();//height();
        console.log(X,Y,h)

        $box.css('left',Y).css('top',X+h+10).css('width',settings.w)

        $box.html('')

        if(storeList.length>0){
            storeList.map(function (store) {
                var li = "<li  class='prompt-li' data-storeid='"+store.store_id+"'>"+store.store_name+"</li>";
                console.log($(li))


                $(li).appendTo($box);
            })
        }else{
            var li = "<li  class='prompt-error' >没有查询到哦</li>";

            $(li).appendTo($box);
        }


        var btn = "<li class='prompt-close'  style='font-size: 12px;text-align: center;color: #0e90d2' >点我关闭</li>";
        $(btn).appendTo($box);
        console.log($box)


        $box.find('.prompt-close').bind('click',function () {
            console.log(222)
            hideBox()
        })

        $box.find('li.prompt-li').bind('click',function (e) {
            console.log($(this).attr('data-storeid'))
            selectStore($(this).attr('data-storeid'))

            if(settings.call){
                settings.call()
            }
        })


        showBox();

    }



    var settings = $.extend(defaults, options);
    if(!settings.table){
        layer.msg('table参数必填', {icon: 2, time: 2000});
    }


    $box.hover(function(){

    },function(){
        // hideBox()
    });


    ele.on('input',function (e) {
        var val = ele.val();

        if(val.length<1)return;
        $.post("/index.php/Seller/Index/likeSerach",{
            field:settings.field,table:settings.table,len:settings.len,column:settings.column,val:val
        },function (res) {
            var data = JSON.parse(res);

            if(data.status!=1){
                layer.msg(data.msg||'请求失败', {icon: 2, time: 2000});
                return;
            }

            settings.init(data.result)

        })
    })

}