<?php
namespace app\mobile\controller;

/**
 * 客服IM控制器
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/13
 * Time: 17:29
 */
class Supplier extends MobileBase{

    //客服im界面
    public function index()
    {
        $store_id = input('get.store_id');
        //var_dump($store_id);
        $user = array();
        if(session("?user")){
            //var_dump(session('user'));die;
            $user = session('user');
            $user['head_pic'] = empty($user['head_pic']) ?  '' : 'http://'.$_SERVER['HTTP_HOST'].$user['head_pic'];

        }
        //店铺总会有
        $user['store_id'] = $store_id;
        //var_dump($user);
        $this->assign('user',$user);
        return $this->fetch();
    }
}