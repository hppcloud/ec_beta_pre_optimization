<?php return [

    // +----------------------------------------------------------------------
    // | api 调用的签名秘钥
    // +----------------------------------------------------------------------
    'API_SECRET_KEY' => 'qwe123456',

    // +----------------------------------------------------------------------
    // | api 响应格式
    // +----------------------------------------------------------------------
    'default_return_type'=>'json',

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------
    //默认错误跳转对应的模板文件
    'dispatch_error_tmpl' => 'public:dispatch_jump',
    //默认成功跳转对应的模板文件
    'dispatch_success_tmpl' => 'public:dispatch_jump',
    'template' => [
        // 模板引擎类型 支持 php think 支持扩展
        'type' => 'Think',
        // 模板路径
        'view_path' => './application/api/template/',
        // 模板后缀
        'view_suffix' => 'html',
        // 模板文件名分隔符
        'view_depr' => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin' => '{',
        // 模板引擎普通标签结束标记
        'tpl_end' => '}',
        // 标签库标签开始标记
        'taglib_begin' => '<',
        // 标签库标签结束标记
        'taglib_end' => '>',
        //模板文件名
        'default_theme' => 'default',
    ],
    'view_replace_str' => [
        '__PUBLIC__' => '/public',
        '__STATIC__' => '/template/app/default/static',
        '__ROOT__' => ''
    ],


    // +----------------------------------------------------------------------
    // | 主机环境地址 【第三方回调】
    // +----------------------------------------------------------------------

    // api接口
    'api_host'=>[
        'test'=>'https://api_test.employeechoice.cn',       // 测试
        'release'=>'https://api_release.employeechoice.cn', // 预发布
        'production'=>'https://api.employeechoice.cn',      // 正式(生产)
    ],

    // web前端
    'web_host'=>[
        'test'=>'https://test.employeechoice.cn',       // 测试
        'release'=>'https://release.employeechoice.cn', // 预发布
        'production'=>'https://www.employeechoice.cn',  // 正式（生产）
    ],

    // 移动web端
    'mobile_web_host'=>[
        'test'=>'https://m_test.employeechoice.cn',       // 测试
        'release'=>'https://m_release.employeechoice.cn', // 预发布
        'production'=>'https://m.employeechoice.cn',  // 正式（生产）
    ],


    // +----------------------------------------------------------------------
    // | api登录验证【 判断哪些控制器的 哪些方法需要登录验证的】
    // +----------------------------------------------------------------------
    'login_auth' => [
        'cart' => ['cart2', 'cart3', 'cart4', 'integral', 'integral2'],
        'distribut' => ['add_goods', 'goods_list', 'index', 'lower_list', 'my_store', 'order_list', 'rebate_log', 'store'],
        'goods' => ['collectGoodsOrNo','goodsList','goodsInfo','goodsCategoryList','goodsSecAndThirdCategoryList',
            'search'],
        'message' => ['message_read'],
        'order' => ['add_comment', 'ajaxZan', 'cancel_order', 'checkType', 'comment', 'complain_handle', 'conmment_add',
            'delComment', 'del_order', 'dispute', 'dispute_apply', 'dispute_info', 'dispute_list', 'expose',
            'expose_info', 'expose_list', 'expose_info', 'get_complain_talk', 'order_confirm', 'order_detail',
            'order_list', 'publish_complain_talk', 'reply_add', 'return_goods', 'return_goods_cancel',
            'return_goods_index', 'return_goods_info', 'return_goods_list', 'return_goods_refund'],
        'payment' => ['alipay_sign'],
        'store' => ['collectStoreOrNo'],
        'user' => ['account', 'account_list', 'addAddress', 'add_comment', 'add_service_comment', 'cancelOrder',
            'clear_message', 'clear_visit_log', 'comment', 'comment_num', 'del_address', 'del_visit_log',
            'getAddressList', 'getCollectStoreData', 'getCouponList', 'getOrderList', 'getUserCollectStore', 'logout',
            'message', 'message_switch', 'orderConfirm', 'password', 'points', 'points_list', 'recharge_list',
            'return_goods', 'return_goods_info', 'return_goods_list', 'return_goods_status', 'service_comment',
            'setDefaultAddress', 'updateUserInfo', 'upload_headpic', 'userInfo', 'visit_log', 'withdrawals',
            'withdrawals_list', 'paypwd'],
        'newjoin' => ['agreement', 'basicInfo', 'storeInfo', 'remark', 'getApply'],
        'v1.user'=>['addAddress','getAddressList','editAddress','delAddress','accInfo','setDefaultAddress'],
        'v1.cart'=>['editInvoice','addInvoice','invoiceList','getDefaultInvoice','setDefaultInvoice','delInvoice'],
        'v1.shoppingcart'=>['index','addGoods','delGoods'],
        'v1.order'=>['index','create','confirm','detail','cancel','deliveryWay','payWay','receiveGoods','changeReserveStatus'],
        'v1.goods'=>['recommend','price'],
        'v1.coupon'=>['ableUse','isNewNotify'],
        'v1.oss'=>['upload'],
        'v1.pay'=>['index','queryPayStatus'],
        'v1.fenqi'=>['pay','queryOrder','refund','refundQuery'],
        'v1.store'=>['address'],
        'v1.service'=>['index','doUse'],
        'v1.recycle'=>['orderDetail','cancelTransaction','prompt','qualityReport'],
        'v1.unionpay'=>['allBank','wapPay','refund']
    ]
]
?>