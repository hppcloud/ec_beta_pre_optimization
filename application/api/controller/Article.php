<?php
namespace app\api\controller;
use  think\Db;
 
class Article extends Base {
   
    /**
     * @param doc_id agreement:用户服务协议, open_store:开店协议 
     * @return \think\mixed
     */
    public function service_agreement(){
        $doc_code = I('doc_code/s', '');
        $article = Db::name('system_article')->where('doc_code',$doc_code)->find();
        $this->assign("article" , $article);
        return $this->fetch();
    }
   
    
}