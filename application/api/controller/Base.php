<?php

namespace app\api\controller;
use app\common\model\Seller;
use app\common\model\UserStore;
use think\Db;
use think\Controller;
use think\Session;

class Base extends Controller {
    public $http_url;
    public $token = '';
    public $user_id = 0;
    public $user = array();
    public $storeId = 0;
    public $store = array();
    public $sellerId = 0;
    public $seller = array();

    /**
     * 析构函数
     */
    function __construct() {       
        parent::__construct();
        if (isset($_REQUEST['test'])&&$_REQUEST['test'] == '1') {
            $test_str = 'POST'.print_r($_POST,true);
            $test_str .= 'GET'.print_r($_GET,true);
            file_put_contents('a.html', $test_str);            
        }

        // 跨域
        header("Access-Control-Allow-Origin:*");
        header("Access-Control-Allow-Headers:Origin,X-Requested-With,Content-Type,Accept");
        header("Access-Control-Allow-Methods:DELETE,PUT");
        
        $this->checkToken(); // 检查token
//        $this->user = M('users')->where('user_id', $this->user_id)->find();
        
        $unique_id = I("unique_id"); // 唯一id  类似于 pc 端的session id
        define('SESSION_ID',$unique_id); //将当前的session_id保存为常量，供其它方法调用                

    }    
    
    /*
     * 初始化操作
     */
    public function _initialize() {

        if(isset($_REQUEST["unique_id"])){           // 兼容手机app
            session_id($_REQUEST["unique_id"]);
            setcookie('token',$_REQUEST["token"]);
        }
        Session::start();
        
        $local_sign = $this->getSign();
        $api_secret_key = C('API_SECRET_KEY');
        
     
            
        // 不参与签名验证的方法
        //@modify by wangqh. add notify
        if(!in_array(strtolower(ACTION_NAME), array('getservertime','group_list','getconfig','alipaynotify', 'notify', 'goodslist','search','goodsthumimages','login','favourite','homepage')))
        {
            if(isset($_POST['sign']))  {
                if($local_sign != $_POST['sign'])
                {
                    $json_arr = array('status'=>-1,'msg'=>'签名失败!!!','result'=>'' );
                    //exit(json_encode($json_arr));

                }
                if(time() - $_POST['time'] > 600)
                {
                    $json_arr = array('status'=>-1,'msg'=>'请求超时!!!','result'=>'' );
                    //exit(json_encode($json_arr));
                }
            }
        }

    }
    
    /**
     *  app 端万能接口 传递 sql 语句 sql 错误 或者查询 错误 result 都为 false 否则 返回 查询结果 或者影响行数
     */
    public function sqlApi()
    {            
        exit(json_encode(array('status'=>-1,'msg'=>'使用万能接口必须开启签名验证才安全','result'=>''))); //  开启后注释掉这行代码即可
        
        C('SHOW_ERROR_MSG',1);
            $sql = $_REQUEST['sql'];
            try
            {
                 if(preg_match("/insert|update|delete/i", $sql))            
                     $result = Db::execute($sql);
                 else             
                     $result =  Db::query($sql);
             }
             catch (\Exception $e)
             {
                 $json_arr = array('status'=>-1,'msg'=>'系统错误','result'=>'');
                 $json_str = json_encode($json_arr);            
                 exit($json_str);            
             }            
                         
            if($result === false) // 数据非法或者sql语句错误            
                $json_arr = array('status'=>-1,'msg'=>'系统错误','result'=>'');
            else
                $json_arr = array('status'=>1,'msg'=>'成功!','result'=>$result);
                                   
            $json_str = json_encode($json_arr);            
            exit($json_str);            
    }

    /**
     * app端请求签名
     * @return type
     */
    protected function getSign(){
        header("Content-type:text/html;charset=utf-8");
        $data = $_POST;        
        unset($data['time']);    // 删除这两个参数再来进行排序     
        unset($data['sign']);    // 删除这两个参数再来进行排序
        ksort($data);
        $str = implode('', $data);
        if(isset($_POST['time'])){
            $str = $str.$_POST['time'].C('API_SECRET_KEY');
        }
        return md5($str);
    }
        
    /**
     * 获取服务器时间
     */
    public function getServerTime()
    {
        $json_arr = array('status'=>1,'msg'=>'成功!','result'=>time());
        $json_str = json_encode($json_arr);
        exit($json_str);       
    }
    
    /**
     * 校验token
     */
    public function checkToken()
    {
        $this->token = $this->request->param("token",''); // token
        if (empty($this->token)&&isset($_COOKIE['token'])) {
            $this->token = $_COOKIE['token'];
        }

        // 判断哪些控制器的 哪些方法需要登录验证的
        $check_arr = config('login_auth');
        
        // 保留状态的检查组
        $check_session_arr = [
            'cart' => ['cartlist','addcart'],
            'user' => ['getGoodsCollect']
        ];
        
        $not_session_arr = [
            'user' => ['reg']
        ];
       
        $controller_name = strtolower(CONTROLLER_NAME);
        $action_name = strtolower(ACTION_NAME);
        if(in_array($controller_name, array_keys($check_arr)) && in_array($action_name, array_map('strtolower',$check_arr[$controller_name])))
        {
            $return = $this->getUserByToken($this->token);
            if ($return['status'] != 1) {
                $this->ajaxReturn($return);
            }
            $this->user = $return['result'];
            $this->user_id = $this->user['user_id'];                    
             // 更新最后一次操作时间 如果用户一直操作 则一直不超时
            M('users')->where("user_id",$this->user_id)->save(array('last_login'=>time()));
            
        } elseif (in_array($controller_name, array_keys($check_session_arr)) && in_array($action_name, array_map('strtolower',$check_session_arr[$controller_name]))) {
            if ($this->token) {
                $this->user = M('users')->where("token",$this->token)->find();
            }
            !$this->user && $this->user = session('user');
            $this->user && $this->user_id = $this->user['user_id'];
            
        } elseif (in_array($controller_name, array_keys($not_session_arr)) && in_array($action_name, array_map('strtolower',$not_session_arr[$controller_name]))) {
            session('user', null);
        } else {
            $this->user = M('users')->where("token", $this->token)->find();
            $this->user && ($this->user_id = $this->user['user_id']);
        }

        if($this->user){
            $this->store = UserStore::getStore($this->user['user_id']);
            $this->storeId = $this->store['store_id'];
            $this->seller = Seller::get($this->store['seller_id']);
            $this->sellerId = $this->store['seller_id'];
            define('SELLER_ID',$this->sellerId);
        }

        session('user', $this->user);
    }
    
    protected function getUserByToken($token)
    {
        if (empty($token)) {
            return ['status'=>-100, 'msg'=>'请登录'];
        }

        $user = M('users')->where("token", $token)->find();
        if (empty($user)) {
            return ['status'=>-101, 'msg'=>'token已过期'];
        }
        
        // 登录超过72分钟 则为登录超时 需要重新登录.  //这个时间可以自己设置 可以设置为 20分钟
        if(time() - $user['last_login'] > C('APP_TOKEN_TIME')) {  //3600
            return ['status'=>-102, 'msg'=>'登录超时,请重新登录!', 'result'=>(time() - $user['last_login'])];
        }
        
        return ['status' => 1, 'msg' => '获取成功', 'result' => $user];
    }


    /**
     * 替换属性中的NULL为""
     * @param $data
     */
    public function ajaxReturn($data){
        if($data && isset($data['result'])){
            $result = $data['result'];
            if(is_array($result)){
                $dealResult = $this->batchFilterNullAttr($result);
                $data['result'] = $dealResult;
            }
        }
        exit(json_encode($data, JSON_UNESCAPED_UNICODE));
    }
    
    public function batchFilterNullAttr($arr){
         if(!$arr){
            return $arr;
        }
        if(is_array($arr)){
            foreach ($arr as $k => $v){
                if(!isset($v)){
                    $arr[$k] = "";
                }else if(is_array($v)){
                    $filterResult = $this->batchFilterNullAttr($arr[$k]);
                    $arr[$k] = $filterResult;
                }
            }
            return $arr;
        }else{
            return empty($arr) ? "" : $arr;
        }
    }

    /**
     * api返回
     */
    public function api($status=0,$msg='success',$result=[]){
        $data = [
            'status'=>$status,
            'msg'=>$msg,
            'result'=>$result
        ];
        header("Content-type: application/json;;charset=utf-8");
        exit(json_encode($data, JSON_UNESCAPED_UNICODE));
    }

    /**
     * api 成功返回
     */
    public function apiSuccess($result,$msg='操作成功！'){
        $this->api(1,$msg,$result);
    }

    /**
     * api 失败返回
     */
    public function apiError($status=-1,$msg='操作失败！',$data=[]){
        $this->api($status,$msg,$data);
    }

    /**
     * 表单验证
     */
    protected function validate($data, $validate, $message = [], $batch = false, $callback = null)
    {
        $result = parent::validate($data, $validate, $message, $batch, $callback);
        if (true !== $result) {
            $this->api(-1,$result);
        }
    }
}