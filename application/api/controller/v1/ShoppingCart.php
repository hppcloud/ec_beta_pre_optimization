<?php
namespace app\api\controller\v1;

use app\api\controller\Base;
use app\common\logic\CartLogic;
use app\common\logic\Integral;
use app\common\logic\CouponLogic;
use app\common\logic\Pay;
use app\common\logic\PlaceOrder;
use app\common\model\Cart;
use app\common\model\Goods;
use app\common\model\SpecGoodsPrice;
use app\common\util\TpshopException;
use think\Db;
use think\Loader;

use app\common\logic\GoodsLogic;
use app\api\model\Goods as GoodsModel;

class ShoppingCart extends Base {

    /**
     * 购物车商品列表
     */
    public function index()
    {
        $list = Cart::getList($this->user_id,$this->storeId,$this->sellerId);

        // 取最新价格
        foreach ($list as $v){
            $goods = GoodsModel::getGoods($this->storeId,$v['goods_id']);
            if($goods){
                $v['member_goods_price'] = GoodsModel::getSellPrice($goods,$v['spec_key'],$this->user_id);
            }
        }
        $this->apiSuccess($list);
    }

    /**
     * 将商品加入购物车
     */
    function addGoods()
    {
        $param = $this->request->post();
        $rule = [
            'goods_id'=>'require',
            'goods_num'=>'require',
        ];
        $this->validate($param,$rule);
        $specKey = $this->request->post('spec_key');
        $goods = Goods::getGoods($this->storeId,$param['goods_id']);

        $logic = new GoodsLogic();
        $rate = $logic->getRate($goods['goods_id'],$this->user_id);

        if($specKey){  // 不为单品
            $where = [
                'store_id'=>$this->storeId,
                'goods_id'=>$param['goods_id'],
                'key'=>$specKey
            ];
            $specGoodsPriceModel = SpecGoodsPrice::get($where);
            if(!$specGoodsPriceModel){
                $this->apiError(-1,'无该'.$param['spec_key'].'属性组合的商品！');
            }
            $attrPrice = $specGoodsPriceModel['price'];

            // 加上商品基础价
            $specGoodsPriceModel['price'] = ($attrPrice + $goods['shop_price'])*$rate;

            $cartLogic = new CartLogic();
            $cartLogic->setGoodsModel($goods);
            $cartLogic->setUniqueId($param['token']);
            $cartLogic->setUserId($this->user_id);
            $cartLogic->setSpecGoodsPriceModel($specGoodsPriceModel);
            $cartLogic->setGoodsBuyNum($param['goods_num']);
            try {
                $cartLogic->addGoodsToCart();
                $this->apiSuccess(['cart_num' => $cartLogic->getUserCartGoodsNum()],'加入购物车成功！');
            } catch (TpshopException $t) {
                $this->ajaxReturn($t->getErrorArr());
            }
        }else{
            // 如果是单品
            $cart = Cart::get(['user_id'=>$this->user_id,'goods_id'=>$goods['goods_id']]);
            if($cart){
                $cart['goods_num'] = $cart['goods_num']+$param['goods_num'];
                $cart->save();
            }else{
                $cart = Cart::create([
                    'user_id'=>$this->user_id,
                    'goods_id'=>$goods['goods_id'],
                    'goods_sn'=>$goods['goods_sn'],
                    'goods_name'=>$goods['goods_name'],
                    'market_price'=>$goods['market_price'],
                    'goods_price'=>$goods['shop_price'],// 本店价
                    'member_goods_price'=>round($goods['shop_price']*$rate),// 本店会员折扣价
                    'goods_num'=>$param['goods_num'],
                    'add_time'=>time(),
                    'store_id'=>$this->storeId,
                ]);
            }

            if($cart){
                $this->apiSuccess($cart);
            }else{
                $this->apiError(-1,'加入购物失败！');
            }
        }

    }

    /**
     * 修改购物车
     */
    public function change(){

        $param = $this->request->post();
        $rule = [
            'cart_id'=>'require',
            'goods_num'=>'require',
//            'spec_key'=>'require',
        ];
        $this->validate($param,$rule);

        $updateData = [];
        // 修改购物车数量 和勾选状态
        $cartLogic = new CartLogic();

        $updateData[0]['goods_num'] = $param['goods_num'];
//        $updateData[0]['spec_key'] = $param['spec_key'];
        $updateData[0]['id'] = $param['cart_id'];

        $changeResult = $cartLogic->changeNum($param['cart_id'], $param['goods_num']);
        if ($changeResult['status'] != 1) {
            $this->ajaxReturn($changeResult);
        }

        if($cartLogic->AsyncUpdateCart($updateData)){
            $this->apiSuccess($updateData[0]);
        }else{
            $this->apiError(-1,'修改失败！');
        }

    }
    
    /**
     * 删除购物车的商品
     */
    public function delGoods()
    {
        $param = $this->request->post();
        $rule = [
            'cart_id'=>'require',
        ];
        $this->validate($param,$rule);
        $where = [
            'user_id'=>$this->user_id,
            'id'=>["in", $param['cart_id']]
        ];
        $result = M("Cart")->where($where)->delete(); // 删除id为5的用户数据

        if($result){
            $this->apiSuccess($result,'删除成功！');
        }else{
            $this->apiError(-1,'你的购物车无该商品，删除失败！');
        }
    }
    


    /**
     * 购物车第二步确定页面
     */
    public function cart2(){
        $goods_id = input("goods_id/d"); // 商品id
        $goods_num = input("goods_num/d");// 商品数量
        $item_id = input("item_id/d"); // 商品规格id
        $prom_type = input('prom_type/d');//立即购买时才会用到.
        $action = input("action"); // 行为
        $address_id = input('address_id/d');
        if ($this->user_id == 0){
            $this->ajaxReturn(array('status'=>-1,'msg'=>'用户user_id不能为空','result'=>''));
        }
        //获取地址
        if($address_id){
            $userAddress = M('user_address')->where("address_id" , $address_id)->find();
        }else{
            $userAddress = Db::name('user_address')->where(['user_id' => $this->user_id])->order(['is_default' => 'desc'])->find();
        }
        if(empty($userAddress)){
            $this->ajaxReturn(['status' => -11, 'msg' => '请先添加收货地址', 'result' => null]);// 返回结果状态
        }else{
            $userAddress['total_address'] = getTotalAddress($userAddress['province'], $userAddress['city'], $userAddress['district'], $userAddress['twon'], $userAddress['address']);
        }

        $cartLogic = new CartLogic();
        $couponLogic = new CouponLogic();
        $cartLogic->setUserId($this->user_id);
        //立即购买
        if($action == 'buy_now'){
            $goods = Goods::getGoods($this->storeId,$goods_id);
            $cartLogic->setGoodsModel($goods);
            $cartLogic->setSpecGoodsPriceModel($item_id);
            $cartLogic->setGoodsBuyNum($goods_num);
            $cartLogic->setPromType($prom_type);
            $buyGoods = [];
            try{
                $buyGoods = $cartLogic->buyNow();
            }catch (TpshopException $t){
                $error = $t->getErrorArr();
                $this->ajaxReturn(['status' => 0, 'msg' =>$error['msg']]);
                $this->error($error['msg']);
            }
            $cartList[0] = $buyGoods;
            $cartGoodsTotalNum = $goods_num;
        }else{
            if ($cartLogic->getUserCartOrderCount() == 0){
                $this->ajaxReturn(['status' => 0, 'msg' => '你的购物车没有选中商品', 'result' => null]);// 返回结果状态
            }
            $cartList = $cartLogic->getCartList(1); // 获取用户选中的购物车商品
            $cartGoodsTotalNum = array_sum(array_map(function($val){return $val['goods_num'];}, $cartList));//购物车购买的商品总数
        }
        $usersInfo = get_user_info($this->user_id);  // 用户
        $cartGoodsList = get_arr_column($cartList,'goods');
        $cartGoodsId = get_arr_column($cartGoodsList,'goods_id');
        $cartGoodsCatId = get_arr_column($cartGoodsList,'cat_id3');
        $storeCartList = $cartLogic->getStoreCartList($cartList);//转换成带店铺数据的购物车商品
        $storeCartTotalPrice= array_sum(array_map(function($val){return $val['store_goods_price'];}, $storeCartList));//商品优惠总价
        $userCouponList = $couponLogic->getUserAbleCouponList($this->user_id, $cartGoodsId, $cartGoodsCatId);//用户可用的优惠券列表
        $cartLogic->getCouponCartList($storeCartList, $userCouponList);//计算优惠券数量
        $UserStoreCouponNum = $cartLogic->getUserStoreCouponNumArr();
        $couponNum = !empty($UserStoreCouponNum) ? $UserStoreCouponNum : [];
        $json_arr = array(
            'status'=>1,
            'msg'=>'获取成功',
            'result'=>array(
                'addressList' =>$userAddress, // 收货地址
                'couponNum'=>$couponNum,  //用户可用的优惠券列表
                'cartGoodsTotalNum'=>$cartGoodsTotalNum,   //购物车购买的商品总数
                'storeShippingCartList'=>$storeCartList,//购物车列表
                'storeCartTotalPrice'=>$storeCartTotalPrice,//商品总价
                'userInfo'    =>$usersInfo, // 用户详情
            ));
        $this->ajaxReturn($json_arr) ;
    }

    /**
     * ajax 获取订单商品价格 或者提交 订单
     */
    public function cart3()
    {
        if ($this->user_id == 0){
            $this->ajaxReturn(['status' => -100, 'msg' => "登录超时请重新登录!", 'result' => null]);// 返回结果状态
        }
        $address_id         = input("address_id/d"); //  收货地址id
        $goods_id           = input("goods_id/d"); // 商品id
        $goods_num          = input("goods_num/d");// 商品数量
        $item_id            = input("item_id/d"); // 商品规格id
        $action             = input("action"); // 立即购买
        $invoice_title      = input('invoice_title'); // 发票
        $taxpayer           = input('taxpayer');  // 纳税人识别号
        $invoice_desc       = input('invoice_desc'); // 内容详情
        $pay_points         = input("pay_points/d",0); //  使用积分
        $user_money         = input("user_money/f",0); //  使用余额
        $pay_pwd            = input('pwd');
        $cart_form_data     = input("cart_form_data"); // goods_num 购物车商品数量
        $cart_form_data     = urldecode($cart_form_data);
        $cart_form_data     = json_decode($cart_form_data,true); //app 端 json 形式传输过来
        $user_note          = $cart_form_data['user_note'] ?: ''; // $user_note = I('user_note'); // 给卖家留言      数组形式
        $coupon_id          = $cart_form_data['coupon_id'] ?: 0; // $coupon_id =  I("coupon_id/d",0); //  优惠券id  数组形式
        $data               = input('request.');
        $cart_validate = Loader::validate('Cart');
        if (!$cart_validate->check($data)) {
            $error = $cart_validate->getError();
            $this->ajaxReturn(['status' => 0, 'msg' => $error[0], 'result' => '']);
        }
        $address = Db::name('UserAddress')->where("address_id", $address_id)->find();
        $cartLogic = new CartLogic();
        $pay = new Pay();
        try {
            if ($action == 'buy_now') {
                $goods = Goods::getGoods($this->storeId,$goods_id);
                $cartLogic->setGoodsModel($goods);
                $cartLogic->setSpecGoodsPriceModel($item_id);
                $cartLogic->setGoodsBuyNum($goods_num);
                $cart_list[0] = $cartLogic->buyNow();
                $pay->payGoodsList($cart_list);
            } else {
                $cartLogic->setUserId($this->user_id);
                $cart_list = $cartLogic->getCartList(1);
                $cartLogic->checkStockCartList($cart_list);
                $pay->payCart($cart_list);
            }
            $pay->setUserId($this->user_id);
            $pay->delivery($address['district']);
            $pay->orderPromotion();
            $pay->useCoupons($coupon_id);
            $pay->usePayPoints($pay_points);
            $pay->useUserMoney($user_money);
            if ($_REQUEST['act'] == 'submit_order') {
                //保存发票信息
                $invoice_result = $cartLogic->save_invoice($invoice_title, $taxpayer, $invoice_desc);
                if($invoice_result['status'] ==-2){
                    $this->ajaxReturn($invoice_result);
                }
                $placeOrder = new PlaceOrder($pay);
                $placeOrder->setUserAddress($address);
                $placeOrder->setInvoiceTitle($invoice_title);
                $placeOrder->setUserNote($user_note);
                $placeOrder->setTaxpayer($taxpayer);
                $placeOrder->setPayPsw($pay_pwd);
                $placeOrder->addNormalOrder();
                $cartLogic->clear();
                $master_order_sn = $placeOrder->getMasterOrderSn();
                $this->ajaxReturn(['status'=>1,'msg'=>'提交订单成功','result'=>$master_order_sn]);
            }
            $result = $pay->toArray();
            $return_arr = array('status' => 1, 'msg' => '计算成功', 'result' => $result); // 返回结果状态
            $this->ajaxReturn($return_arr);
        }catch (TpshopException $t){
            $error = $t->getErrorArr();
            $this->ajaxReturn($error);
        }
    }
 
    /**
     * 订单支付页面
     */
    public function cart4()
    {
        // 如果是主订单号过来的, 说明可能是合并付款的
        $master_order_sn = I('master_order_sn','');
        $order_sn = I('order_sn','');
        $order_id = I('order_id','');
        $select_order_where = empty($master_order_sn) ? $order_sn : $master_order_sn;
        $select_order_where = empty($select_order_where) ? $order_id : $select_order_where;
        if (!$select_order_where) {
            $this->ajaxReturn(['status'=>-1, 'msg'=>'参数错误']);
        }
        $sum_order_amount = M('order')->where("order_sn|master_order_sn|order_id", $select_order_where)->sum('order_amount');
        if (!is_numeric($sum_order_amount)) {
            $this->ajaxReturn(['status'=>-1, 'msg'=>'订单不存在']);
        }
        $this->ajaxReturn(['status'=>1,'msg'=>'获取成功','result' => $sum_order_amount]);
    }    
    
    
    /**
     *  保存发票信息
     * @date2017/10/19 14:45
     */
    public function save_invoice(){
        
            //A.1获取发票信息
            $invoice_title = trim(I("invoice_title"));
            $taxpayer      = trim(I("taxpayer"));
            $invoice_desc  = trim(I("invoice_desc"));
    
            //B.1校验用户是否有历史发票记录
            $map            = [];
            $map['user_id'] =  $this->user_id;
            $info           = M('user_extend')->where($map)->find();
    
            //B.2发票信息
            $data=[];
            $data['invoice_title'] = $invoice_title;
            $data['taxpayer']      = $taxpayer;
            $data['invoice_desc']  = $invoice_desc;
    
            //B.3发票抬头
            if($invoice_title=="个人"){
                $data['invoice_title'] ="个人";
                $data['taxpayer']      = "";
            }
    
            //是否存贮过发票信息
            if(empty($info)){
                $data['user_id'] = $this->user_id;
                (M('user_extend')->add($data))?
                $status=1:$status=-1;
            }else{
                (M('user_extend')->where($map)->save($data))?
                $status=1:$status=-1;
            }
            $result = ['status' => $status, 'msg' => '报错成功', 'result' =>''];
            $this->ajaxReturn($result);
    }
    
    /**
     * 优惠券兑换
     */
    public function coupon_exchange()
    {
        $coupon_code = input('coupon_code');
        $couponLogic = new \app\common\logic\CouponLogic;
        $return = $couponLogic->exchangeCoupon($this->user_id, $coupon_code);
        if ($return['status'] != 1) {
            $this->ajaxReturn($return);
        }
        $limit_store = '平台';
        $store_id = $return['result']['coupon']['store_id'];
        if ($store_id) {
            $store = \app\common\model\Store::get($store_id);
            $limit_store = $store['store_name'];
        }
        $return['result']['coupon']['limit_store'] = $limit_store;
        $this->ajaxReturn($return);
    }


    /**
     * ajax 获取用户收货地址 用于购物车确认订单页面
     */
    public function ajaxAddress()
    {
        $address_id = I('address_id/d');
        //获取地址
        if ($address_id) {
            $userAddress = M('UserAddress')->where(['user_id' => $this->user_id, 'address_id' => $address_id])->find();
        }
        if (!$address_id || !$userAddress) {
            $addresslist = M('UserAddress')->where(['user_id' => $this->user_id])->select();
            $userAddress = $addresslist[0];
            foreach ($addresslist as $address) {
                if ($address['is_default'] == 1) {
                    $userAddress = $address;
                    break;
                }
            }
        }
        if ($userAddress) {
            $userAddress['total_address'] = getTotalAddress($userAddress['province'], $userAddress['city'], $userAddress['district'], $userAddress['twon'], $userAddress['address']);
        }
        if(empty($userAddress)){
            $this->ajaxReturn(['status' => -1, 'msg' => '请先添加收货地址', 'result' => null]);// 返回结果状态
        }
        $this->ajaxReturn(['status'=>1,'msg'=>'获取成功','result'=>$userAddress]);
    }


    /**
     *  积分商品结算页 1
     * @return mixed
     */
    public function integral(){
       // if(IS_POST) {
            $goods_id = input('goods_id/d');
            $item_id = input('item_id/d');
            $goods_num = input('goods_num/d');
            $address_id = input('address_id/d');
            if (empty($goods_id)) {
                $this->ajaxReturn(['status' => 0, 'msg' => '非法操作']);
            }
            if (empty($goods_num)) {
                $this->ajaxReturn(['status' => 0, 'msg' => '购买数不能为零']);
            }
            $Goods = new Goods();
            $goods = $Goods->with('store')->where(['goods_id' => $goods_id])->find();
            if (empty($goods)) {
                $this->ajaxReturn(['status' => 0, 'msg' => '该商品不存在']);
            }
            //获取地址
            if ($address_id) {
                $userAddress = M('UserAddress')->where(['user_id' => $this->user_id, 'address_id' => $address_id])->find();
            }
            if (!$address_id || !$userAddress) {
                $addresslist = M('UserAddress')->where(['user_id' => $this->user_id])->select();
                $userAddress = $addresslist[0];
                foreach ($addresslist as $address) {
                    if ($address['is_default'] == 1) {
                        $userAddress = $address;
                        break;
                    }
                }
            }
            if(empty($userAddress)){
                $this->ajaxReturn(['status' => -1, 'msg' => '请先添加收货地址', 'result' => null]);// 返回结果状态
            }else{
                $userAddress['total_address'] = getTotalAddress($userAddress['province'], $userAddress['city'], $userAddress['district'], $userAddress['twon'], $userAddress['address']);
            }

            if (empty($item_id)) {
                $goods_spec_list = SpecGoodsPrice::all(['goods_id' => $goods_id]);
                if (count($goods_spec_list) > 0) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '请传递规格参数']);
                }
                $goods_price = $goods['shop_price'];
                //没有规格
            } else {
                //有规格
                $specGoodsPrice = SpecGoodsPrice::get(['item_id' => $item_id, 'goods_id' => $goods_id]);
                if ($goods_num > $specGoodsPrice['store_count']) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '该商品规格库存不足，剩余' . $specGoodsPrice['store_count'] . '份']);
                }
                $goods_price = $specGoodsPrice['price'];
                $goods['item_id'] = $specGoodsPrice['item_id'];
                $goods['spec_key_name'] = $specGoodsPrice['key_name'];
            }
            $usersInfo = get_user_info($this->user_id);  // 用户
            $point_rate = tpCache('shopping.point_rate');
            $data = [
                'userAddress'=>$userAddress,  //用户地址
                'point_rate' => $point_rate,  //积分比例
                'goods' => $goods,  //商品信息
                'goods_price' => $goods_price,  //商品价格
                'goods_num' => $goods_num,     //商品购买数量
                'userInfo'    =>$usersInfo, // 用户详情
            ];
            $this->ajaxReturn(['status' => 1, 'msg' => '获取成功', 'result' => $data]);
       /*} else{
            $this->ajaxReturn(['status' => -100, 'msg' => '请求方式错误！', 'result' => null]);
        } */
    }

    /**
     *  积分商品价格提交(计算价格)
     * @return mixed
     */
    public function integral2(){
        if(IS_POST) {
            $goods_id       = input('goods_id/d');
            $item_id        = input('item_id/d');
            $goods_num      = input('goods_num/d');
            $address_id     = input("address_id/d"); //  收货地址id
            $user_note      = input('user_note',''); // 给卖家留言
            $invoice_title  = input('invoice_title'); // 发票
            $taxpayer       = input('taxpayer'); // 纳税人识别号
            $user_money     = input("user_money/f", 0); //  使用余额
            $pwd            = input('pwd');
            strlen($user_note) > 50 && exit(json_encode(['status'=>-1,'msg'=>"备注超出限制可输入字符长度！",'result'=>null]));
            if (empty($address_id)) {
                $this->ajaxReturn(['status' => -3, 'msg' => '请先填写收货人信息', 'result' => null]);
            }
            $integral = new Integral();
            try {
                $integral->setUser($this->user);
                $integral->setGoodsById($goods_id);
                $integral->setSpecGoodsPriceById($item_id);
                $integral->setUserAddressBydId($address_id);
                $integral->setBuyNum($goods_num);
                $integral->setUserMoney($user_money);
                $integral->checkBuy();
                $pay = $integral->pay();
                if ($_REQUEST['act'] == 'submit_order') {
                    $placeOrder = new PlaceOrder($pay);
                    $user_address = $integral->getUserAddress();
                    $placeOrder->setUserAddress($user_address);
                    $placeOrder->setInvoiceTitle($invoice_title);
                    $placeOrder->setUserNote($user_note);
                    $placeOrder->setTaxpayer($taxpayer);
                    $placeOrder->setPayPsw($pwd);
                    $placeOrder->addNormalOrder();
                    $master_order_sn = $placeOrder->getMasterOrderSn();
                    $this->ajaxReturn(['status'=>1,'msg'=>'提交订单成功','result'=>$master_order_sn]);
                }
                $result = ['status' => 1, 'msg' => '计算成功', 'result' => $pay->toArray()];
                $this->ajaxReturn($result);
            } catch (TpshopException $t) {
                $result = $t->getErrorArr();
                $this->ajaxReturn($result);
            }
        }else{
            $this->ajaxReturn(['status' => -100, 'msg' => '请求方式错误！', 'result' => null]);
        }
    }
}
