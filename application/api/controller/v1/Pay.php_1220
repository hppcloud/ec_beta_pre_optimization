<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date Time: 2018/6/6 14:40
 * Author: Wanzhou Chen
 * Email: 295124540@qq.com
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\model\Order as OrderModel;
use app\common\model\Seller;
use app\admin\model\Users;
use app\common\model\Store as StoreModel;
use QRcode;
use SoapClient;
use SoapFault;
use think\Log;

class Pay extends Base
{
    protected $merchantId;          // 商户号
    protected $subKey;              // 分账秘钥
    protected $refundKey;           // 退款秘钥
    protected $queryKey;            // 查询秘钥
    protected $webHost;             // 前端主机地址

    /**
     * 获取快钱配置信息
     */
    function _initialize()
    {
        parent::_initialize();
        $this->merchantId = '10210053347';
        $this->subKey     = 'UH9B32H975BI7DT8';
        $this->refundKey  = 'Y9JXU8GDI6JXSDG8';
        $this->queryKey   = '4IM4Y7AGEG2HCMLQ';

        $this->webHost = getWebHost();
    }

    protected function kqCkNull($v, $k)
    {
        if ($v == '') return '';
        return $k . '=' . $v . '&';
    }

    /**
     * 生成支付二维码
     * @param $url
     * @return string
     */
    protected function QRCode($url)
    {
        include_once dirname(APP_PATH) . '/extend/phpqrcode.php';
        $value = $url;                  //二维码内容
        $errorCorrectionLevel = 'H';    //容错级别
        $matrixPointSize = 6;           //生成图片大小
        //生成二维码图片
        $path = '/public/qrcode/' . md5(time()) . '.png';
        $filename = dirname(APP_PATH) . $path;
        QRcode::png($value, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

        $logo = dirname(APP_PATH) . '/public/icon/pay_logo.jpg';  //准备好的logo图片
        $QR = $filename;            //已经生成的原始二维码图

        if (file_exists($logo)) {
            $QR = imagecreatefromstring(file_get_contents($QR));        //目标图象连接资源。
            $logo = imagecreatefromstring(file_get_contents($logo));    //源图象连接资源。
            $QR_width = imagesx($QR);           //二维码图片宽度
            $QR_height = imagesy($QR);          //二维码图片高度
            $logo_width = imagesx($logo);       //logo图片宽度
            $logo_height = imagesy($logo);      //logo图片高度
            $logo_qr_width = $QR_width / 4;     //组合之后logo的宽度(占二维码的1/5)
            $scale = $logo_width / $logo_qr_width;    //logo的宽度缩放比(本身宽度/组合后的宽度)
            $logo_qr_height = $logo_height / $scale;  //组合之后logo的高度
            $from_width = ($QR_width - $logo_qr_width) / 2;   //组合之后logo左上角所在坐标点

            //重新组合图片并调整大小
            /*
             *  imagecopyresampled() 将一幅图像(源图象)中的一块正方形区域拷贝到另一个图像中
             */
            imagecopyresampled($QR, $logo, $from_width, $from_width, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
        }

        //输出图片
        imagepng($QR, $filename);
        imagedestroy($QR);
        imagedestroy($logo);
        return $this->request->domain() . $path;
    }

    function index()
    {
        $param = $this->request->post();
        $rule = [
            'order_sn' => 'require',
            'pay_way' => 'require|in:kq,wx,ali,wap,fq',
        ];
        $this->validate($param, $rule);

        $order = OrderModel::get(['order_sn' => $param['order_sn'], 'user_id' => $this->user_id, 'deleted' => 0]);
        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }

        // 检查订单状态
        if ($order['pay_status'] != 0) {
            $this->apiError(-1, '该订单已支付！');
        }

        // 检查订单金额
        if ($order['order_amount'] <= 0) {
            $this->apiError(-1, '该订单金额为' . $order['order_amount'] . ',无法支付！');
        }

        $order->goods;

        $version = "v2.0";
        if ($param['pay_way'] == 'kq') {
            //支付方式，一般为00，代表所有的支付方式。如果是银行直连商户，该值为10，必填。
            $payType = '21';
            $payName = '快捷支付';
        } elseif ($param['pay_way'] == 'wx') {
            $payType = '28-1';
            $payName = '微信支付';
        } elseif ($param['pay_way'] == 'ali') {
            $payType = '28-2';
            $payName = '支付宝支付';
        } elseif ($param['pay_way'] == 'wap') {
            $version = "mobile1.0";
            $payType = '21';
            $payName = '移动支付';
        } else {
            $this->apiError(-1, '目前不支持该支付方式！');
        }
        $data = $this->kuaiqian($order, $payType, $version);
        $order['pay_code'] = $param['pay_way'];
        $order['pay_name'] = $payName;
        $order->save();
        $this->apiSuccess($data);
    }

    protected function kuaiqian($order, $payType, $version)
    {

        //人民币网关账号，该账号为11位人民币网关商户编号+01,该参数必填。
        $merchantAcctId = $this->merchantId."01";
        //编码方式，1代表 UTF-8; 2 代表 GBK; 3代表 GB2312 默认为1,该参数必填。
        $inputCharset = "1";
        //接收支付结果的页面地址，该参数一般置为空即可。
        $pageUrl = "";
        //服务器接收支付结果的后台地址，该参数务必填写，不能为空。
        $bgUrl = $this->request->domain() . "/v1/pay/KQCallBack";
        //网关版本，固定值：v2.0,该参数必填。
        $version = $version;
        //语言种类，1代表中文显示，2代表英文显示。默认为1,该参数必填。
        $language = "1";
        //签名类型,该值为4，代表PKI加密方式,该参数必填。
        $signType = "4";
        //支付人姓名,可以为空。
        $payerName = "";
        //支付人联系类型，1 代表电子邮件方式；2 代表手机联系方式。可以为空。
        $payerContactType = "1";
        //支付人联系方式，与payerContactType设置对应，payerContactType为1，则填写邮箱地址；payerContactType为2，则填写手机号码。可以为空。
        $payerContact = "316639224@qq.com";
        //商户订单号，以下采用时间来定义订单号，商户可以根据自己订单号的定义规则来定义该值，不能为空。
        $orderId = $order['order_sn'];
        //订单金额，金额以“分”为单位，商户测试以1分测试即可，切勿以大金额测试。该参数必填。
        $orderAmount = $order['order_amount'] * 100;
        //订单提交时间，格式：yyyyMMddHHmmss，如：20071117020101，不能为空。
        $orderTime = date("YmdHis");

        // 商品信息
        if (isset($order['goods']) && count($order['goods']) > 0) {
            $productName = $order['goods'][0]['goods_name']; // 商品名称，可以为空。
            $productNum = $order['goods'][0]['goods_num']; //商品数量，可以为空。
            $productId = $order['goods'][0]['goods_sn']; //商品代码，可以为空。
            $productDesc = "";// 商品描述，可以为空。
        } else {
            $productName = '商品支付';
            $productNum = '';
            $productId = '';
            $productDesc = '';
        }

        //扩展字段1，商户可以传递自己需要的参数，支付完快钱会原值返回，可以为空。
        $ext1 = "";
        //扩展自段2，商户可以传递自己需要的参数，支付完快钱会原值返回，可以为空。
        $ext2 = "";
        //银行代码，如果payType为00，该值可以为空；如果payType为10，该值必须填写，具体请参考银行列表。
        $bankId = "";
        //同一订单禁止重复提交标志，实物购物车填1，虚拟产品用0。1代表只能提交一次，0代表在支付不成功情况下可以再提交。可为空。
        $redoFlag = "";
        //快钱合作伙伴的帐户号，即商户编号，可为空。
        $pid = "";
        // 平台分账，则为 NB2
        $extDataType = 'NB2';
        // 平台分账 Key 为 sharingInfo

        // 查询分销商快钱账号信息
        $seller = Seller::get(['seller_id' => $order['seller_id']]);
        if(!$seller){
            $this->apiError(-2,'分销商账户异常，请联系平台管理员！');
        }

        if(!$seller['quick']){
            $this->apiError(-2,'分销商还未配置快钱账号，请联系平台管理员！');
        }

        // 分割分销商账户信息
        $arr = explode('|', $seller['quick']);
        if (count($arr) < 3) {
            $this->apiError(-2,'分销商快钱账号配置异常，请联系平台管理员！');
        }

        $nb = [
            'sharingInfo' => [
                'sharingFlag' => '1',// 分账标识
                'feeMode' => '0',// 手续费收取方式
                'feePayer' => $arr[0],// 承担快钱手续费方
                'sharingData' => '2^'.$arr[0].'^'.$orderAmount.'^0^分账了'// 固定选择值^分账方^分账金额^分账模式^备注字段
            ]
        ];

        $extDataContent = '<NB2>'.json_encode($nb).'</NB2>';
        // signMsg 签名字符串 不可空，生成加密签名串

        $kq_all_para = $this->kqCkNull($inputCharset, 'inputCharset');
        $kq_all_para .= $this->kqCkNull($pageUrl, "pageUrl");
        $kq_all_para .= $this->kqCkNull($bgUrl, 'bgUrl');
        $kq_all_para .= $this->kqCkNull($version, 'version');
        $kq_all_para .= $this->kqCkNull($language, 'language');
        $kq_all_para .= $this->kqCkNull($signType, 'signType');
        $kq_all_para .= $this->kqCkNull($merchantAcctId, 'merchantAcctId');
        $kq_all_para .= $this->kqCkNull($payerName, 'payerName');
        $kq_all_para .= $this->kqCkNull($payerContactType, 'payerContactType');
        $kq_all_para .= $this->kqCkNull($payerContact, 'payerContact');
        $kq_all_para .= $this->kqCkNull($orderId, 'orderId');
        $kq_all_para .= $this->kqCkNull($orderAmount, 'orderAmount');
        $kq_all_para .= $this->kqCkNull($orderTime, 'orderTime');
        $kq_all_para .= $this->kqCkNull($productName, 'productName');
        $kq_all_para .= $this->kqCkNull($productNum, 'productNum');
        $kq_all_para .= $this->kqCkNull($productId, 'productId');
        $kq_all_para .= $this->kqCkNull($productDesc, 'productDesc');
        $kq_all_para .= $this->kqCkNull($ext1, 'ext1');
        $kq_all_para .= $this->kqCkNull($ext2, 'ext2');
        $kq_all_para .= $this->kqCkNull($payType, 'payType');
        $kq_all_para .= $this->kqCkNull($bankId, 'bankId');
        $kq_all_para .= $this->kqCkNull($redoFlag, 'redoFlag');
        $kq_all_para .= $this->kqCkNull($pid, 'pid');
        $kq_all_para .= $this->kqCkNull($extDataType, 'extDataType');
        $kq_all_para .= $this->kqCkNull($extDataContent, 'extDataContent');

        $kq_all_para = substr($kq_all_para, 0, strlen($kq_all_para) - 1);


        /////////////  RSA 签名计算 ///////// 开始 //
        $fp = fopen("/home/99bill/v1/99bill-rsa.pem", "r");
        $priv_key = fread($fp, 123456);
        fclose($fp);
        $pkeyid = openssl_get_privatekey($priv_key);

        // compute signature
        openssl_sign($kq_all_para, $signMsg, $pkeyid, OPENSSL_ALGO_SHA1);

        // free the key from memory
        openssl_free_key($pkeyid);

        $signMsg = base64_encode($signMsg);
        /////////////  RSA 签名计算 ///////// 结束 //

        if ($version == 'mobile1.0') {
            $payUrl = "https://www.99bill.com/mobilegateway/recvMerchantInfoAction.htm?";// 移动支付
        } else {
            $payUrl = 'https://www.99bill.com/gateway/recvMerchantInfoAction.htm?'; // pc支付
        }

        $payData = [

            // 协议参数
            'inputCharset' => $inputCharset,
            'pageUrl' => $pageUrl,
            'bgUrl' => $bgUrl,
            'version' => $version,
            'language' => $language,
            'signType' => $signType,
            'signMsg' => $signMsg,

            // 买卖双方信息参数
            'merchantAcctId' => $merchantAcctId,
            'payerName' => $payerName,
            'payerContactType' => $payerContactType,
            'payerContact' => $payerContact,

            // 业务参数
            'orderId' => $orderId,
            'orderAmount' => $orderAmount,
            'orderTime' => $orderTime,
            'productName' => $productName,
            'productNum' => $productNum,
            'productId' => $productId,
            'productDesc' => $productDesc,
            'ext1' => $ext1,
            'ext2' => $ext2,
            'payType' => $payType,
            'bankId' => $bankId,
            'redoFlag' => $redoFlag,
            'pid' => $pid,
            'extDataType' => $extDataType,
            'extDataContent' => $extDataContent
        ];

        Log::write($payData,'pay');

        if ($payType == '28-1') {
            $contents = httpRequest($payUrl, 'post', $payData);
            $rt = xmlToArray($contents);
            if ($rt['resultCode'] == 0) {
                $payUrl = $this->QRCode($rt['wechatQrcode']);
            } else {
                $this->apiError(-1, $rt['resultMessage']);
            }

        } elseif ($payType == '28-2') {
            $contents = httpRequest($payUrl, 'post', $payData);
            $rt = xmlToArray($contents);
            if ($rt['resultCode'] == 0) {
                $payUrl = $this->QRCode($rt['alipayQrcode']);
            } else {
                $this->apiError(-1, $rt['resultMessage']);
            }
        } else {
            foreach ($payData as $k => $v) {
                $payUrl .= $k . '=' . urlencode($v) . '&';
            }
        }
        return ['pay_way' => $_POST['pay_way'], 'pay_url' => $payUrl];
    }


    /**
     * 快钱支付后，回调前端页面
     */
    function KQCallBack()
    {

        //人民币网关账号，该账号为11位人民币网关商户编号+01,该值与提交时相同。
        $kq_check_all_para = $this->kqCkNull($_REQUEST['merchantAcctId'], 'merchantAcctId');
        //网关版本，固定值：v2.0,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['version'], 'version');
        //语言种类，1代表中文显示，2代表英文显示。默认为1,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['language'], 'language');
        //签名类型,该值为4，代表PKI加密方式,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['signType'], 'signType');
        //支付方式，一般为00，代表所有的支付方式。如果是银行直连商户，该值为10,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['payType'], 'payType');
        //银行代码，如果payType为00，该值为空；如果payType为10,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['bankId'], 'bankId');
        //商户订单号，,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['orderId'], 'orderId');
        //订单提交时间，格式：yyyyMMddHHmmss，如：20071117020101,该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['orderTime'], 'orderTime');
        //订单金额，金额以“分”为单位，商户测试以1分测试即可，切勿以大金额测试,该值与支付时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['orderAmount'], 'orderAmount');
        //已绑短卡号,信用卡快捷支付绑定卡信息后返回前六后四位信用卡号
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['bindCard'], 'bindCard');
        //已绑短手机尾号,信用卡快捷支付绑定卡信息后返回前三位后四位手机号码
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['bindMobile'], 'bindMobile');
        // 快钱交易号，商户每一笔交易都会在快钱生成一个交易号。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['dealId'], 'dealId');
        //银行交易号 ，快钱交易在银行支付时对应的交易号，如果不是通过银行卡支付，则为空
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['bankDealId'], 'bankDealId');
        //快钱交易时间，快钱对交易进行处理的时间,格式：yyyyMMddHHmmss，如：20071117020101
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['dealTime'], 'dealTime');
        //商户实际支付金额 以分为单位。比方10元，提交时金额应为1000。该金额代表商户快钱账户最终收到的金额。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['payAmount'], 'payAmount');
        //费用，快钱收取商户的手续费，单位为分。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['fee'], 'fee');
        //扩展字段1，该值与提交时相同
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['ext1'], 'ext1');
        //扩展字段2，该值与提交时相同。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['ext2'], 'ext2');
        //处理结果， 10支付成功，11 支付失败，00订单申请成功，01 订单申请失败
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['payResult'], 'payResult');
        //错误代码 ，请参照《人民币网关接口文档》最后部分的详细解释。
        $kq_check_all_para .= $this->kqCkNull($_REQUEST['errCode'], 'errCode');


        $trans_body = substr($kq_check_all_para, 0, strlen($kq_check_all_para) - 1);
        $MAC = base64_decode($_REQUEST['signMsg']);

        $fp = fopen("/home/99bill/v1/99bill.cert.rsa.20340630.cer", "r");
        $cert = fread($fp, 8192);
        fclose($fp);
        $pubkeyid = openssl_get_publickey($cert);
        $ok = openssl_verify($trans_body, $MAC, $pubkeyid);

        if ($ok == 1) {
            switch ($_REQUEST['payResult']) {
                case '10':
                    //此处做商户逻辑处理
                    $order = OrderModel::get(['order_sn' => $_REQUEST['orderId']]);
                    if ($order) {
                        $order['order_status'] = 1;
                        $order['pay_status'] = 1;
                        $order['paid_money'] = $_REQUEST['orderAmount'] / 100;
                        $order['pay_time'] = time();
                        $order['transaction_id'] = $_REQUEST['dealId'];
                        $order->save();

                        $this->notify($order);// 邮件通知，短信通知

                    }

                    $rtnOK = 1;
                    //以下是我们快钱设置的show页面，商户需要自己定义该页面。
                    $rtnUrl = $this->webHost . "/order/success";
                    break;
                default:
                    $rtnOK = 0;
                    //以下是我们快钱设置的show页面，商户需要自己定义该页面。
                    $rtnUrl = $this->webHost . "/order/error";
                    break;

            }

        } else {
            $rtnOK = 0;
            //以下是我们快钱设置的show页面，商户需要自己定义该页面。
            $rtnUrl = $this->webHost . "/order/error";

        }
        echo "<result>$rtnOK</result><redirecturl>$rtnUrl</redirecturl>";
    }

    /**
     * 快钱分账【版本升级，已废弃】
     */
    protected function subAccount($order, $rate)
    {

        $clientObj = new SoapClient('http://www.99bill.com/apipay/services/BatchPayWS?wsdl');

        // 查询经销商
        if (!$order['seller_id']) {
            return false;
        }
        $seller = Seller::get(['seller_id' => $order['seller_id']]);
        if (!($seller && $seller['quick'])) {
            return false;
        }

        // 分割
        $arr = explode('|', $seller['quick']);
        if (count($arr) < 2) {
            return false;
        }

        $creditName = $arr[1];
        $creditContact = $arr[0];
        $currencyCode = 1;                                  // 货币类型
        $amount = round($order['order_amount'] - $order['order_amount'] * $rate, 2);              // 分账金额 以人民币“元”为单位
        $description = $order['transaction_id'];            // 分账时填写原收款订单号
        $correctName = 'y';
        $tempAccount = 'n';
        $orderId = $order['order_sn'];
        $key = $this->subKey;

        $mer_id = $this->merchantId;                         // 商户号
        $mer_ip = "39.105.62.169";

        $kq_all_para = $creditContact . $currencyCode . $amount . $orderId . $key;  // 拼接参数数据
        $mac = strtoupper(md5($kq_all_para));
        $para = [
            'creditName' => $creditName,
            'creditContact' => $creditContact,
            'currencyCode' => $currencyCode,
            'amount' => $amount,
            'description' => $description,
            'correctName' => $correctName,
            'tempAccount' => $tempAccount,
            'orderId' => $orderId,
            'mac' => $mac
        ];
        Log::write(array(array($para), $mer_id, $mer_ip), 'pay');
        try {
            //  开始读取 WEB SERVERS 上的数据
            $result = $clientObj->__soapCall('simplePay', array(array($para), $mer_id, $mer_ip));
            Log::write($result, 'pay');
        } catch (SOAPFault $e) {
            Log::write($e, 'pay');
        }
    }

    /**
     * 快钱退款
     */
    public static function KQRefund($order_sn,$amount)
    {

        $merchantId ='10210053347';
        $refundKey = 'Y9JXU8GDI6JXSDG8';
        $url = "https://www.99bill.com/webapp/receiveDrawbackAction.do"; // 退款网关提交地址
        
        $order = OrderModel::get(['order_sn' => $order_sn,'deleted' => 0]);
       
        if (!$order) {
            return ['code'=>-1,'msg'=>'该订单不存在！','data'=>null];
        }

        // 检查订单金额
        if ($order['order_amount'] <= 0) {
            return ['code'=>-1,'msg'=>'该订单金额为' . $order['order_amount'] . ',无法退款！','data'=>null];

        }

        // 查询分销商快钱账号信息
        $seller = Seller::get(['seller_id' => $order['seller_id']]);
        if(!$seller){
            return ['code'=>-2,'msg'=>'分销商账户异常，请联系平台管理员！','data'=>null];
        }

        if(!$seller['quick']){
            return ['code'=>-2,'msg'=>'分销商还未配置快钱账号，请联系平台管理员！','data'=>null];
        }

        // 分割分销商账户信息
        $arr = explode('|', $seller['quick']);
        if (count($arr) < 3) {
            return ['code'=>-2,'msg'=>'分销商快钱账号配置异常，请联系平台管理员','data'=>null];
        }
        // 非扫码支付：flag^ sharingContact ^ amount ^ memo
        // 扫码支付：  flag^ sharingContact ^ amount ^ sharingSyncFlag^memo
        $sync = '';
        if($order['pay_code']=='wx'||$order['pay_code']=='ali'){
            // 扫码退款时间限制
            if(time()-$order['pay_time']<=300){
                return ['code'=>-4,'msg'=>'扫码退货发起时间与分账支付时间间隔必须大于 5 分钟!','data'=>null];
            }
            $sync = '^0';
        }
        $data = [
            'merchant_id' => $merchantId,                 // 商家用户编号
            'version' => 'bill_drawback_api_1',           // 固定值: bill_drawback_api_1
            'command_type' => '001',                      // 固定值: 001	表示下订单请求退款
            'orderid' => $order['order_sn'],              // 原商户的订单号
            'amount' => $amount,           // 退款金额 可以是2位小数 ，人民币 元为单位
            'postdate'=> date('YmdHis'),          // 退款提交时间 格式 20071117020101
            'txOrder' => time().rand(1000,9999),          // 退款流水号 只允许使用字母、数字、- 、_, 必须是数字字母开头 必须在商家自身账户交易中唯一(50)
            'merchant_key'=>$refundKey,
            'returnSharingFlag' => 1,                      // 1 为分账退款
            'returnData' => '2^'.$arr[0].'^'.$amount.$sync.'^退款订单', // flag^ sharingContact ^ amount ^ sharingSyncFlag^memo
        ];
        $str = '';
        foreach ($data as $k=>$v){
            $str = $str.$k.'='.$v;
        }
        $data['mac'] = strtoupper(md5($str));  // 加密字符串
        unset($data['merchant_key']);
        Log::write($data,'pay');

        $contents = httpRequest($url,'POST',$data);
        $curlData = xmlToArray($contents);


        Log::write($curlData,'pay');

        if($curlData['RESULT']!=='N'){
            $order['refund_no'] = 6;
            $order['refund_no'] = $data['txOrder'];
            $order['refund_time'] =time();
            $order->save();
            return ['code'=>1,'msg'=>'退款成功','data'=>$curlData];
        }else{
            return ['code'=>0,'msg'=>'退款失败,第三方信息:'.$curlData['CODE'],'data'=>$curlData];
        }
    }

    /**
     * 查询
     */
    function query()
    {
        $param = $this->request->post();
        $rule = [
            'order_sn' => 'require',
            'type'=>'require|in:1,2'
        ];
        $this->validate($param, $rule);
        $order = OrderModel::get(['order_sn' => $param['order_sn'],'deleted' => 0]);

        if (!$order) {
            return ['code'=>-1,'msg'=>'该订单不存在！','data'=>null];
        }

        $clientObj = new SoapClient('https://www.99bill.com/gatewayapi/services/hatMsgwOrderQuery?wsdl');
        if($param['type']==1){ // 查询交易
            $queryMode = '00';
            $field = 'orderId';
            $sn = $order['order_sn'];
        }else{ // 查询退款
            if(!$order['refund_no']){
                $this->apiError(-1,'还未生成退款单号！');
            }
            $queryMode = '00';
            $field = 'orderId';
            $sn = $order['refund_no'];
        }

        $para = [
            'version' => 'v2.0',                                // 查询接口版
            'signType' => 1,                                    // 签名类型 1.MD5 2.PKI
            'queryType'=>$queryMode,                            // 固定值： 00代表按商户订单号单笔查询，01代表按交易号查询；
            'queryMode'=>$param['type'],                        // 查询模式 1 分账查询 2 退款分账查询
            $field =>$sn,
            'merchantAcctId' => $this->merchantId.'01',         // 商家编号
            'tradeTime'=>date('YmdHis'),                // 查询提交时间
        ];
        $str = '';
        foreach ($para as $k => $v) {
            $str .= $k . '=' . $v . '&';
        }
        $para['signMsg'] = strtoupper(md5($str . "key=".$this->queryKey));

        Log::write($para,'pay');

        try {
            //  开始读取 WEB SERVERS 上的数据
            $result = $clientObj->__soapCall('hatMsgwOrderQuery', array($para));
            if($param['type']==1){ // 查询交易
                if($order['order_status']==0)$order['order_status']=1;
                if($order['pay_status']==0)$order['pay_status']=1;
                $order->save();
            }else{ // 查询退款

            }

            $this->apiSuccess($result);

        } catch (SOAPFault $e) {
            $this->apiError(-1, $e);
        }
    }

    /**
     * 退款查询
     */
    function queryRefund()
    {
        $param = $this->request->post();
        $rule = [
            'start_date' => 'dateFormat:Ymd',
            'end_date' => 'dateFormat:Ymd',
        ];
        $this->validate($param, $rule);
        // 提交地址
        $clientObj = new SoapClient('http://www.99bill.com/gatewayapi/services/gatewayRefundQuery?wsdl');
        $startDate = $this->request->post('start_date', date('Ymd', strtotime('-2 day')));
        $endDate = $this->request->post('end_date', date('Ymd'));
        $para = [
            'version' => 'v2.0',                            // 查询接口版
            'signType' => 1,                                  // 签名类型 1.MD5 2.PKI
            'merchantAcctId' => $this->merchantId . '01',     // 商家编号
            'startDate' => $startDate,                      // 退款生成时 间起点
            'endDate' => $endDate,                          // 如：20100811  startDate不endDate间隔丌能超过3天
            'requestPage' => $this->request->post('page', 1)       // 请求记彔集页码
        ];
        $str = '';
        foreach ($para as $k => $v) {
            $str .= $k . '=' . $v . '&';
        }

        $sign = strtoupper(md5($str . "key=" . $this->queryKey));
        $para['signMsg'] = $sign;

        try {
            //  开始读取 WEB SERVERS 上的数据
            $result = $clientObj->__soapCall('query', array($para));
            $this->apiSuccess($result);

        } catch (SOAPFault $e) {
            $this->apiError(-1, $e);
        }
    }

    /**
     * 查询支付状态
     */
    function queryPayStatus()
    {
        $param = $this->request->post();
        $rule = [
            'order_sn' => 'require'
        ];
        $this->validate($param, $rule);

        $order = OrderModel::get(['order_sn' => $param['order_sn'], 'user_id' => $this->user_id, 'deleted' => 0]);
        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }
        $order->goods;
        $order['company'] = '北京润泽金诚科技有限公司';
        // 检查订单状态
        if ($order['pay_status']) {
            $this->apiSuccess($order, '支付完成！');
        } else {
            $this->api(1020, '待支付状态', $order);
        }
    }

    /**
     * @param $order
     */
    private function notify($order)
    {
        foreach ($order->goods as $goods){
            $goods['spec_key_name'] = str_replace(' ',' &nbsp;&nbsp;&nbsp;',$goods['spec_key_name']);
            $goods['spec_key_name'] = str_replace(':','：&nbsp;',$goods['spec_key_name']);
        }
        $order['store_name'] = StoreModel::where(['store_id' => $order['store_id']])->value('store_name');
        $order['seller_contact'] = Seller::where(['seller_id' => $order['seller_id']])->value('seller_contact');
        $order['goods_item_num'] = count($order['goods']);
        $order['pay_time'] = date('Y-m-d', $order['pay_time']);

        // 地址信息
        if (!$order['ziti_info']) {
            $regions = M('region')->cache(true)->getField('id,name');
            $order['province'] = $regions[$order['province']] ?: '';
            $order['city'] = $regions[$order['city']] ?: '';
            $order['district'] = $regions[$order['district']] ?: '';
            $order['country'] = $regions[$order['country']] ?: '';
        }

        $user = Users::get(['user_id' => $order['user_id']]);
        if ($user && $user['email']) {
            $this->assign('order', $order);
            $this->assign('user', $user);
            $content = $this->fetch('email/pay_complete');
            send_email($user['email'], 'Apple Employee Choice 购买提醒', $content);
        }
    }
}

?>