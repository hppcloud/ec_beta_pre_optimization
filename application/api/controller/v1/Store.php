<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/10/23
 * Time: 19:03
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\model\SellerAddress;

class Store extends Base
{
    function address($type=null){
        $where = ['seller_id'=>$this->sellerId];
        if($type!==null){
            $where['type'] = $type;
        }
        $addressList = SellerAddress::all($where);
        $regions = M('region')->cache(true)->getField('id,name');
        foreach ($addressList as $addr) {
            $addr['province_name'] = $regions[$addr['province_id']] ?: '';
            $addr['city_name'] = $regions[$addr['city_id']] ?: '';
            $addr['district_name'] = $regions[$addr['district_id']] ?: '';
            $addr['twon_name'] = $regions[$addr['town_id']] ?: '';
            $addr['address'] = $addr['address'] ?: '';
        }
        $this->apiSuccess($addressList);
    }
}