<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/3
 * Time: 10:02
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\logic\GoodsLogic;
use app\api\model\Goods as GoodsModel;
use app\common\model\SpecGoodsPrice;

class Goods extends Base
{

    /**
     * 获取商品详情
     */
    public function read($id)
    {
        $goods_id = $id;
        $where['goods_id'] = $goods_id;
        $where['is_on_sale'] = 1;
        $where['goods_state'] = 1;
        $where['is_deleted'] = 0;

        $goods = GoodsModel::get($where);
        if(empty($goods)){
            $this->ajaxReturn(['status'=>-1, 'msg'=>'此商品不存在或者已下架']);
        }
        $goods['goods_package'] = htmlspecialchars_decode($goods['goods_package']);
        $goods['goods_specattr'] = htmlspecialchars_decode($goods['goods_specattr']);
        $goods['goods_content'] = htmlspecialchars_decode($goods['goods_content']);

        // 查询商品价格向下浮动率
        $logic = new GoodsLogic();
        $goods['discount'] = $logic->getRate($goods['goods_id'],$this->user_id);


        $return['goods'] = $goods->append(['comment_statistics'])->toArray();

        // 商品规格 价钱 库存表 找出 所有 规格项id
        $filter_spec = $logic->get_spec($goods_id);
        $goods_spec_list = [];
        foreach ($filter_spec as $key => $val) {
            $goods_spec_list[] = [
                'spec_name' => $key,
                'spec_list' => $val,
            ];
        }
        $return['goods_spec_list'] = $goods_spec_list;

        // 价格组合
        $return['goods_price_group'] = SpecGoodsPrice::priceArr($goods_id,$goods['discount'],$goods['shop_price']);

        // 图片
        $return['goods_images'] = M('goods_images')->field('image_url')->where(array('goods_id'=>$goods_id))->select();

        //增加四舍五入取整
        $goods['shop_price'] =  round($goods['shop_price']*$goods['discount']);

        //是否收藏店铺和商品
        $store_collect = M('store_collect')->where(['user_id' => $this->user_id, 'store_id' => $goods['store_id']])->find();
        if($store_collect){
            $return['store']['is_collect'] = 1;
        }else{
            $return['store']['is_collect'] = 0;
        }

        if (!$goods) {
            $json_arr = array('status'=>-1,'msg'=>'没有该商品');
        } else {
            $json_arr = array('status'=>1,'msg'=>'获取成功','result'=>$return);
        }

        $this->ajaxReturn($json_arr);
    }

    /**
     * 推荐的商品
     */
    function recommend(){
        $where = [
            'store_id'=>$this->storeId,
            'is_deleted'=>0,
            'is_on_sale' => 1,
            'is_recommend'=>1
        ];
        $goodsList = GoodsModel::field('goods_id,goods_sn,goods_name,is_recommend,shop_price,original_img')
            ->where($where)
            ->where('tmpl_id','>',0)
            ->order('on_time','desc')
            ->limit(4)
            ->select();
        $this->apiSuccess($goodsList);
    }

    /**
     * 商品价格
     */
    function price(){
        $param = $this->request->param();
        $rule = [
            'goods_id'=>'require',
        ];
        $this->validate($param,$rule);

        $goods = GoodsModel::getGoods($this->storeId,$param['goods_id']);
        $specKey = $this->request->param('spec_key',null);
        $sellPrice = GoodsModel::getSellPrice($goods,$specKey,$this->user_id);

        $data = [
            'good_id'=>$goods['goods_id'],
            'good_name'=>$goods['goods_name'],
            'spec_key'=>$specKey,
            'sell_price'=>$sellPrice,
        ];
        $this->apiSuccess($data);

    }
}