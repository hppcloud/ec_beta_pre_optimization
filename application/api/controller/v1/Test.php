<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/4
 * Time: 22:02
 */

namespace app\api\controller\v1;


use app\admin\model\Users;
use app\api\controller\Base;
use app\api\model\ServiceList;
use app\common\model\CouponList;
use app\common\model\Order as OrderModel;
use app\common\model\Store as StoreModel;
use app\common\model\Seller as SellerModel;

class Test extends Base
{
   function index(){

       if(is_from_mobile()){
           $this->apiSuccess([],'来至移动端web请求！');
       }else{
           $this->apiSuccess([],'来至PC web端请求！');
       }

       // 判断是否重复提交申请售后
       $returnGoods = M('return_goods')
           // -2用户取消 -1请求驳回 0待审核 1通过 2已发货 3已收货待检验
           ->where(['order_id'=>'7784','status'=>['in','0,1']])
           ->find();
       $this->apiError(-1,$returnGoods);
       $this->apiSuccess(StoreModel::getBindStore('15801616017'));
       //
       $order = OrderModel::get(['order_sn' => '201810212311004452']);
       if($order){
           foreach ($order->goods as $goods){
               $goods['spec_key_name'] = str_replace(' ',' &nbsp;&nbsp;&nbsp;',$goods['spec_key_name']);
               $goods['spec_key_name'] = str_replace(':','：&nbsp;',$goods['spec_key_name']);
           }
           $order['store_name'] = StoreModel::where(['store_id'=>$order['store_id']])->value('store_name');
           $order['seller_contact'] = SellerModel::where(['seller_id'=>$order['seller_id']])->value('seller_contact');
           $order['goods_item_num'] = count($order['goods']);
           $order['pay_time'] = date('Y-m-d',$order['pay_time']);

           // 地址信息
           if(!$order['ziti_info']){
               $regions = M('region')->cache(true)->getField('id,name');
               $order['province'] = $regions[$order['province']] ?: '';
               $order['city'] = $regions[$order['city']] ?: '';
               $order['district'] = $regions[$order['district']] ?: '';
               $order['country'] = $regions[$order['country']] ?: '';
           }

           //$this->apiSuccess($order);
           // 用户信息
           $user = Users::get(['user_id'=>$order['user_id']]);
           if($user&&$user['email']){
               $this->assign('order',$order);
               $this->assign('user',$user);
               echo $content = $this->fetch('email/pay_complete');die();
               send_email($user['email'],'Apple Employee Choice 购买提醒',$content);
           }
       }
   }

    /**
     * 注册发送服务
     */
   function sendService(){
       ServiceList::sendByRegister(2000);
   }

    /**
     * 注册发送服务
     */
    function  sendCoupon(){
        CouponList::sendFromPlatform(12,1111);
    }
}