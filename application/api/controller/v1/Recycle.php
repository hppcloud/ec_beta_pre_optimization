<?php
/**
 * 爱回收
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/11/22
 * Time: 10:01
 */

namespace app\api\controller\v1;


use aihuishou\AHSClient;
use app\api\controller\Base;
use app\common\model\Order as OrderModel;

class Recycle extends Base
{
    protected $aiHiShou;

    function _initialize()
    {
        parent::_initialize();
        $this->aiHiShou = new  AHSClient('api');
    }

    /**
     * @return null|static
     */
    private function getTransactionId()
    {
        $param = $this->request->param();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);
        $order = OrderModel::get(['user_id' => $this->user_id, 'order_sn' => $param['order_sn']]);
        if (!$order) {
            $this->apiError(-1, '你没有该id的订单！');
        }
        return $order['transaction_id'];
    }

    /**
     * 地区信息
     */
    function region(){
        $this->apiSuccess($this->aiHiShou->divisions());
    }

    function orderDetail(){
        $transactionId = $this->getTransactionId();
        $data = $this->aiHiShou->ordersDetail($transactionId);
        $data['order_no'] = $this->request->param('order_sn');
        $this->apiSuccess($data);
    }

    function cancelReasons(){
        $this->apiSuccess($this->aiHiShou->cancelReasons());
    }

    /**
     * 取消交易
     */
    function cancelTransaction(){
        $transactionId = $this->getTransactionId();
        $reasonId = $this->request->param('reason_id',null);
        $this->apiSuccess($this->aiHiShou->cancelTransaction($transactionId,$reasonId));
    }

    /**
     * 用户承认差异
     */
    function confirm(){
        $transactionId = $this->getTransactionId();
        $this->apiSuccess($this->aiHiShou->confirm($transactionId));
    }

    /**
     * 申请退货(催单)
     */
    function prompt(){
        $param = $this->request->param();
        $rule = [
            'region_id' => 'require|number',
            'address' => 'require',
            'receive_by' => 'require',
            'receive_mobile|电话号码无效,'=>'require|number|length:11'
        ];
        $this->validate($param, $rule);
        $data = [
            "order_no"=> $this->getTransactionId(),
            "region_id"=> $param['region_id'],
            "address"=> $param['address'],
            "receive_by"=> $param['receive_by'],
            "receive_mobile"=> $param['receive_mobile']
        ];
        $this->apiSuccess($this->aiHiShou->prompt($data));
    }

    /**
     * 通过order_no获取质检报告
     */
    function qualityReport(){
        $transactionId = $this->getTransactionId();
        $this->apiSuccess($this->aiHiShou->qualityReport($transactionId));
    }

}