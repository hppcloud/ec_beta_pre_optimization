<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date Time: 2018/7/31 15:56
 * Author: Wanzhou Chen
 * Email: 295124540@qq.com
 */

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\common\model\Order as OrderModel;
use app\common\model\Seller;
use fengyun\FYClient;
use fun\UnionPay;
use think\Db;
use think\Log;

class FenQi extends Base
{
    protected $webHost;

    function _initialize()
    {
        parent::_initialize();
        $this->webHost = getWebHost();
    }

    public function index(){
        echo 'ok';
    }

    /**
     * 分期支付
     */
    function pay()
    {

        $param = $this->request->post();
        $rule = [
            'order_sn' => 'require',
            'channel_type' => 'require|in:HBFQ,CCFQ',
            'periods' => 'require|in:3,6,12',
            'pay_type' => 'in:PAY_ONLINE,MOBILE_TERMINAL',
        ];
        $this->validate($param, $rule);

        $order = OrderModel::get(['order_sn' => $param['order_sn'], 'user_id' => $this->user_id, 'deleted' => 0]);

        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }

        // 检查订单状态
        if ($order['pay_status'] != 0) {
            $this->apiError(-1, '该订单已支付！');
        }

        // 检查订单金额
        if ($order['order_amount'] <= 0) {
            $this->apiError(-1, '该订单金额为' . $order['order_amount'] . ',无法支付！');
        }

        // 检查商品
        if (!$order->goods) {
            $this->apiError(-1, '无效订单');
        }

        // 查询分销商快钱账号信息
        $seller = Seller::get(['seller_id' => $order['seller_id']]);
        if (!$seller) {
            $this->apiError(-2, '分销商账户异常，请联系平台管理员！');
        }

        if (!$seller['installment']) {
            $this->apiError(-2, '分销商还未配置分期支付宝账号，请联系平台管理员！');
        }

        // 分割分销商账户信息
        $arr = explode('|', $seller['installment']);
        if (count($arr) < 2) {
            $this->apiError(-2, '分销商快钱账号配置异常，请联系平台管理员！');
        }

        if ($order['order_amount'] <= 0) {
            $this->apiError(-1, '分期金额为0，无法支付！');
        }

        // 支付地址是否创建过
        if($order['pay_url']){
            $this->apiSuccess(['pay_way' => $order['pay_code'], 'pay_url' => $order['pay_url']]);
        }

        $order['transaction_id'] = $order['order_sn'] . '_' . rand(1000, 9999);
        $data = [
            'order_no' => $order['transaction_id'],
            'goods_name' => $order['goods'][0]['goods_name'],
            'money' => $order['order_amount'],
            'pay_type' => isset($param['pay_type']) ? $param['pay_type'] : 'PAY_ONLINE',
            'channel_type' => $param['channel_type'],
            'periods' => $param['periods'],
        ];

        $fy = new FYClient();
        $res = $fy->processPay($arr[0], $data);
        if ($res['return_code'] == '0000') {
            $order['pay_code'] = $param['channel_type'];
            $order['periods'] = $param['periods'];
            $order['pay_name'] = $param['channel_type'] == 'HBFQ' ? '花呗分期' : '信用卡分期';
            $order['pay_url'] = $res['return_data']['pay_url'];
            $order->save();
            $this->apiSuccess($res['return_data']);
        } else {
            $this->apiError(-1, $res['return_msg'], $res);
        }
    }

    /**
     * 接收支付结果通知【前端】
     */
    function notify()
    {
        if ($this->request->param('status') == 'PAYED') {
            $rtnUrl = $this->webHost . "/order/success";
        } else {
            $rtnUrl = $this->webHost . "/order/error";
        }
        $this->redirect($rtnUrl);
    }

    
    
    /**
     * 接收支付结果通知【后端】
     */
    function reback()
    {
        $param = $this->request->param();
        if (!isset($param['parameters'])) {
            Log::write('parameters失败', 'pay');
            Log::write($param, 'pay');
            echo 'Fail';
            die();
        }

        // 验证签名 MD5(商户号+订单号 +下单时间+支付完成时间+金额+交易状态+支付方式+分期期数+商户密钥)。
        $data = json_decode(htmlspecialchars_decode($param['parameters']), true);
        Log::write($data, 'pay');
        $fy = new FYClient();
        $mySign = $fy->sign([
            'merchant_no' => $data['merchant_no'],
            'order_no' => $data['order_no'],
            'order_time' => $data['order_time'],
            'pay_date' => $data['pay_date'],
            'amount' => $data['amount'],
            'status' => $data['status'],
            'pay_way' => $data['pay_way'],
            'periods' => $data['periods']
        ]);
        Log::write('我的签名：' . $mySign, 'pay');
        if ($data['sign'] == $mySign) {

            if ($data['status'] == 'PAYED') {
                $arr = explode('_', $data['order_no']); // 分离出订单号
                $order = OrderModel::get(['order_sn' => $arr[0]]);
                
                if ($order) {
                    $order['order_status'] = 1;
                    $order['pay_status'] = 1;
                    $order['paid_money'] = $data['amount'];
                    $order['pay_time'] = time();
                    $order['transaction_id'] = $data['order_no'];
                    $order->save();
                    Log::write('支付成功！', 'pay');
                }
            }
            echo 'OK';
            die();
        } else {
            Log::write('签名失败：' . $mySign, 'pay');
            echo 'Fail';
            die();
        }

    }

    /**
     * 查询分期订单
     */
    function queryOrder()
    {
        $param = $this->request->param();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);

        $order = OrderModel::get(['order_sn' => $param['order_sn'], 'user_id' => $this->user_id, 'deleted' => 0]);
        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }

        $fy = new FYClient();
        $res = $fy->query($order['transaction_id']);
        if ($res['return_code'] == '0000') {
            $this->apiSuccess($res['return_data']);
        } else {
            $this->apiError(-1, $res['return_msg'], $res);
        }
    }

    /**
     * 退款申请
     */
    public static function refund($order_sn, $amount, $reason = '退款')
    {


        $order = OrderModel::get(['order_sn' => $order_sn, 'deleted' => 0]);
        if (!$order) {
            return ['code' => -1, 'msg' => '订单不存在'];
        }
        // 判断是否是分期订单
        if (!($order['pay_code'] == 'HBFQ' || $order['pay_code'] == 'CCFQ')) {
            return ['code' => -1, 'msg' => '该订单不属于分期订单，无法发起分期退款！'];

        }

        $time = time();
        $order['refund_no'] = $time . rand(100000, 999999);
        $order['refund_time'] = $time;


        $fy = new FYClient();
        $res = $fy->refund($order, $amount, $reason);
        if ($res['return_code'] == '0000') {
            $order['order_status'] = 5;//订单状态.0待确认，1已确认，2已收货，3已取消，4已完成，5已作废
            $order['pay_status'] = 3;// 支付状态.0待支付，1已支付，2部分支付，3已退款，4拒绝退款
            $order->save();
            return ['code' => 1, 'msg' => $res['return_data']];
        } else {
            $order['pay_status'] = 4;// 支付状态.0待支付，1已支付，2部分支付，3已退款，4拒绝退款
            $order->save();
            return ['code' => 1, 'msg' => $res['return_msg'], 'data' => $res];

        }
    }

    /**
     * 退款查询
     */
    function refundQuery()
    {
        $param = $this->request->param();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);

        $order = OrderModel::get(['order_sn' => $param['order_sn'], 'user_id' => $this->user_id, 'deleted' => 0]);
        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }

        $fy = new FYClient();
        $res = $fy->refundQuery($order['refund_no'], $order['pay_code']);
        if ($res['return_code'] == '0000') {
            $this->apiSuccess($res['return_data']);
        } else {
            $this->apiError(-1, $res['return_msg'], $res);
        }
    }
}