<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/4
 * Time: 22:02
 */

namespace app\api\controller\v1;

use app\api\controller\Base;
use app\common\model\Invoice;
use app\common\model\Users;
use app\common\model\UserStore;
use think\Db;


class Repair extends Base
{

    public function _initialize()
    {
       set_time_limit(0);
    }

    function index(){
       $this->apiSuccess([],'数据修复');
    }

    /**
     * 修复订单发票
     */
    function orderInvoice(){
        // 获取没门店ID的发票
        $list = Invoice::field('i.invoice_id,i.store_id,o.store_id as order_store_id')
            ->alias('i')
            ->join('Order o','o.order_id = i.order_id')
            ->where(['i.store_id'=>null])
            ->select();
        foreach ($list as $v){
            if(Invoice::update(['store_id'=>$v['order_store_id']],['invoice_id'=>$v['invoice_id']])){
                echo '<div>【修复成功】发票ID='.$v['invoice_id'].'，门店ID='.$v['order_store_id'].'</div>';
            }else{
                echo '<div style="color: red">【修复失败】发票ID='.$v['invoice_id'].'，门店ID='.$v['order_store_id'].'</div>';
            }
        }
        echo '<div>执行完毕，操作条数:'.count($list).'</div>';
    }

   /**
    *  2018-10-20
    *  修复发票数据
    */
   function invoice(){

       // 获取到异常发票
       $list = Invoice::field('i.invoice_id,i.invoice_money,o.order_amount')
           ->alias('i')
           ->join('Order o','o.order_id = i.order_id')
           ->where(['i.invoice_money'=>0])
           ->select();
       foreach ($list as $v){
           $invoice['invoice_money'] = $v['order_amount'];
           if(Invoice::update(['invoice_money'=>$v['order_amount']],['invoice_id'=>$v['invoice_id']])){
               echo '<div>【修复成功】发票ID='.$v['invoice_id'].'，发票金额='.$v['order_amount'].'</div>';
           }else{
               echo '<div style="color: red">【修复失败】发票ID='.$v['invoice_id'].'，发票金额='.$v['order_amount'].'</div>';
           }
       }
       echo '<div>执行完毕，操作条数:'.count($list).'</div>';
   }

    /**
     * 修改重复的用户电话
     */
   function user(){
       // 获取重复的用户
       $param = $this->request->param();
       $this->validate($param,['field'=>'require|in:email,mobile']);
       $field= $param['field'];
       $sql = "SELECT count($field) num,user_id,email,mobile
                from tp_users
                where $field!=''
                group by $field
                having num>1
                order by num desc";
       $users = Db::query($sql);
       foreach ($users as $user){
           echo '<h3>'.$user[$field].'</h3>';
           // 检查每个账号下是否有订单，如果有不删除
           $list = Users::all([$field=>$user[$field]]);
           $num = count($list);
           $is_exist = false;
           foreach ($list as $k=>$u){
               $order = \app\common\model\Order::get(['user_id'=>$u['user_id']]);
               if($order){
                   echo '<div style="color: green">user_id='.$u['user_id'].',有订单</div>';
                   $is_exist = true;
               }else{
                   echo 'user_id='.$u['user_id'].',无订单';
                   if($is_exist||(!$is_exist&&$k+1!=$num)){
                       // 已经有了，或者都没有且不是最后一个
                       $r1 = Users::destroy(['user_id'=>$u['user_id']]);
                       $r2 = UserStore::destroy(['user_id'=>$u['user_id']]);
                       if($r1){
                           echo '<span style="color: red">已删除！</span><br>';
                       }else{
                           echo '<span style="color: yellow">删除失败！</span><br>';
                       }
                   }
               }
           }
       }

       $users = Db::query($sql);
       if($users){
           foreach ($users as $user) {
               echo '<h3 style="color: red">' . $user[$field] . '修复没完成，还有重复，</h3>';
           }
       }else{
           echo '<h1>已全部修复！</h1>';
           // 修改字段 默认值null，可以为null
           if($field=='mobile'){
               Db::query("ALTER TABLE `tp_users` MODIFY COLUMN `mobile`  char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码' AFTER `email`");
           }else{
               Db::query("ALTER TABLE `tp_users` MODIFY COLUMN `email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件' AFTER `user_id`;");
           }
           // 把为''的值换成null,否则无法添加唯一索引
           Db::query("update tp_users set $field = null where $field = ''");
           // 添加唯一索引
           Db::query("create unique index `uk_$field` on tp_users($field)");

           echo '<h1 style="color: red">请手动检查tp_users表'.$field.'字段的唯一索引:uk_'.$field.'是否创建成功，如果没有请务必手动添加，请删除多余无用的索引！</h1>';
           // 删除原来的普通索引
           Db::query("ALTER TABLE `tp_users` DROP INDEX `$field`");
       }
   }

    /**
     * 修改重复的第三方平台交易流水号
     */
    function transactionId(){
        // 获取重复的用户
        $field= 'transaction_id';
        \app\common\model\Order::update(['transaction_id'=>null],['transaction_id'=>'']);
        $sql = "SELECT count($field) num,order_id,order_sn,mobile,$field
                from tp_order
                group by $field
                having num>1
                order by num desc";
        $orders = Db::query($sql);

        foreach ($orders as $order){
            echo '<h3>'.join(',',$order).'</h3>';
            \app\common\model\Order::update([$field=>null],['order_id'=>['<>',$order['order_id']]]);
        }

        $orders = Db::query($sql);
        if($orders){
            foreach ($orders as $order) {
                echo '<h3 style="color: red">' . $order[$field] . '修复没完成，还有重复，</h3>';
            }
        }else{
            echo '<h1>已全部修复！</h1>';

            // 添加唯一索引
            Db::query("create unique index `uk_$field` on tp_order($field)");

            echo '<h1 style="color: red">请手动检查tp_order表'.$field.'字段的唯一索引:uk_'.$field.'是否创建成功，如果没有请务必手动添加，请删除多余无用的索引！</h1>';

        }
    }


}