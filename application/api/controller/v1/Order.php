<?php

namespace app\api\controller\v1;

use aihuishou\AHSClient;
use app\admin\model\SpecGoodsPrice;
use app\api\controller\Base;
use app\common\logic\OrderLogic;
use app\common\logic\SmsLogic;
use app\common\model\Coupon;
use app\common\model\CouponList;
use app\common\model\Invoice;
use app\common\model\OrderGoods;
use app\common\model\OrderReserve;
use app\common\model\SellerAddress;
use app\common\model\Shipping;
use app\common\model\SpecItem;
use app\common\model\UserAddress;
use app\common\model\UserExtend;
use app\common\model\Users;
use think\Db;
use app\common\model\team\TeamFound;
use app\common\model\Goods as GoodsModel;
use app\common\model\Order as OrderModel;
use app\common\model\Cart as CartModel;
use app\common\logic\GoodsLogic;
use think\Log;
use utils\Aes;

class Order extends Base
{
    /**
     * 订单列表
     */
    public function index()
    {
        // 订单状态.0待确认，1已确认，2已收货，3已取消，4已完成，5已作废
        // 支付状态.0待支付，1已支付，2部分支付，3已退款，4拒绝退款
        // 发货状态 0未发 ,  1已发

        $where = ['user_id' => $this->user_id];

        $type = $this->request->post('type');
        if(!($type==null||$type=='')){
            $where['prom_type'] = $type;
        }

        $order = OrderModel::where($where)
            ->order('create_time','desc')
            ->paginate();
        foreach ($order as $v) {

            foreach ($v->goods as $goods){

                // 商品属性
                if($goods['spec_key']){
                    $goods['spec_key_name'] = SpecItem::field('s.id attr_cat_id,s.name attr_cat_name,si.id attr_id,si.item attr_name')
                        ->alias('si')
                        ->join('spec s','s.id=si.spec_id')
                        ->where(['si.id'=>['in',str_replace('_',',',$goods['spec_key'])]])
                        ->select();
                }

                // 如果订单未支付，且商品下架了，则取消订单
                if( $v['order_status']==0){
                    $where = [
                        'goods_id' =>$goods['goods_id'],
                        'is_on_sale' => 1,
                        'goods_state' => 1,
                        'is_deleted' => 0,
                        'store_id' => $this->storeId,
                        'seller_id' => $this->sellerId,
                    ];
                    if(empty(GoodsModel::get($where))){
                        OrderModel::update(['order_status'=>3],['order_id'=>$v['order_id']]);// 取消订单
                    }
                }

                // 预订状态
                if($type==4){
                    if($v['order_status']==3){
                        $v['reserve_status'] = -1;// 订单取消，则预定取消
                    }else{
                        $v['reserve_status'] = OrderLogic::getReserveOrderStatus($v['order_id']);
                    }
                }
            }
        }
        $this->apiSuccess($order);
    }

    /**
     * 获取订单详情
     */
    public function detail()
    {
        $param = $this->request->post();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);

        $map['order_sn'] = $param['order_sn'];
        $map['user_id'] = $this->user_id;

        $orderObj = OrderModel::where($map)->with('OrderGoods')->find();
        if (!$orderObj) {
            $this->apiError(-1, '订单不存在');
        }

        // 是否可以退货退款
        foreach ($orderObj->order_goods as $orderGoods) {
            $orderGoods['is_able_refund'] = $orderGoods->isAbleRefund($orderObj);
            $orderGoods['is_able_return_goods'] = $orderGoods->isAbleReturnGoods($orderObj);
        }

        //转为数字，并获取订单状态，订单状态显示按钮，订单商品
        if ($orderObj->prom_type == 5) {//虚拟订单
            $order_info = $orderObj->append(['order_status_detail', 'virtual_order_button'])->toArray();
        } else if ($orderObj->prom_type == 6) {
            $orderTeamFound = $orderObj->teamFound;
            $order_info = $orderObj->append(['order_status_detail'])->toArray();
            if ($orderTeamFound) {
                //团长的单
                $order_info['orderTeamFound'] = $orderTeamFound;//团长
            } else {
                //去找团长
                $TeamFound = new TeamFound();
                $teamFound = $TeamFound::get(['found_id' => $orderObj->teamFollow['found_id']]);
                $order_info['orderTeamFound'] = $teamFound;//团长
            }
        } else {
            $order_info = $orderObj->append(['order_status_detail'])->toArray();
        }

        $invoice_no = M('DeliveryDoc')->where("order_id", $order_info['order_id'])->getField('invoice_no', true);
        $order_info['invoice_no'] = implode(' , ', $invoice_no);
        // 获取 最新的 一次发货时间
        $order_info['shipping_time'] = M('DeliveryDoc')->where("order_id", $order_info['order_id'])->order('id desc')->getField('create_time');


        //订单收货地址
        $order_info['total_address'] = getTotalAddress($order_info['province'], $order_info['city'], $order_info['district'], $order_info['twon'], $order_info['address']);

        //返回商品规格组合id(item_id)
        foreach ($order_info['order_goods'] as &$v) {
            if ($v['spec_key']) {
                $item_id = M("SpecGoodsPrice")->where(['key' => $v['spec_key'], 'goods_id' => $v['goods_id']])->getField('item_id');
            }
            $v['item_id'] = empty($item_id) ? 0 : $item_id;

            // 退货信息
            $returnGoods =  Db::name('ReturnGoods')
                ->field('type,status')
                ->where(['rec_id' => $v['rec_id']])
                ->order('addtime','desc')
                ->find();
            $v['return_type'] = $returnGoods?$returnGoods['type']:null;
            $v['return_status'] = $returnGoods?$returnGoods['status']:null;

            // 属性组合
            $v['spec_key_name'] = SpecItem::field('s.id attr_cat_id,s.name attr_cat_name,si.id attr_id,si.item attr_name')
                ->alias('si')
                ->join('spec s','s.id=si.spec_id')
                ->where(['si.id'=>['in',str_replace('_',',',$v['spec_key'])]])
                ->select();

        }

        // 订单发票
        $order_info['invoice'] = Invoice::get(['order_id'=>$order_info['order_id']]);

        // 预订订单
        if($order_info['prom_type']==4){
            $order_info['reserve_status'] = OrderLogic::getReserveOrderStatus($order_info['order_id']);
        }

        unset($order_info['order_id']);
        $this->apiSuccess($order_info);
    }

    /**
     * 直接下单
     */
    function build()
    {
        $param = $this->request->post();
        $rule = [
            'goods_id' => 'require',
            'buy_num' => 'require',
            'type'=>'in:0,4'
        ];
        $this->validate($param, $rule);

        $goodsArr = [
            ['goods_id'=>$param['goods_id'],'spec_key'=>$this->request->post('spec_key'),'goods_num'=>$param['buy_num']]
        ];

        $order = $this->generate($goodsArr,$this->request->post('type',0));
        if ($order) {
            $this->apiSuccess($order);
        } else {
            $this->apiError(-1, '下单失败！');
        }
    }

    /**
     * 通过购物车下单
     */
    function buildByCart()
    {
        $param = $this->request->post();
        $rule = ['cart_id' => 'require'];
        $this->validate($param, $rule);

        // 从购物车取商品选择
        $cartGoods = CartModel::all(['id' => ['in', $param['cart_id']], 'user_id' => $this->user_id]);
        if (!$cartGoods) {
            $this->apiError(-1, '未发现你的该购物车以及商品！');
        }

        $order = $this->generate($cartGoods);
        if ($order) {
            CartModel::destroy(['id' => ['in', $param['cart_id']], 'user_id' => $this->user_id]);
            $this->apiSuccess($order);
        } else {
            $this->apiError(-1, '下单失败！');
        }
    }

    /**
     * 生成订单
     * 订单类型：0普通1抢购2团购3优惠4预定5虚拟6拼团
     */
    private function generate($goodsArr,$type=0){

        $param = $this->request->post();
        if($type==4){
            $rule = ['mobile'=>'require|number|length:11'];
        }else{
            $rule = [
                'delivery_id'=>'require',
                'blag_way'=>'requireWith:invoice_id|in:1,2,3',
                'final_amount'=>'require',
                'channel_type'=>'in:HBFQ,CCFQ',
                'periods'=>'requireWith:channel_type|in:3,6,12',
                'invoice_email'=>'email'
            ];
        }
        $this->validate($param, $rule);

        // 获取收货地址
        $masterOrderSn = $this->request->param('master_order_sn','');
        $addressId = $this->request->post('address_id');
        $addr = null;
        if($addressId){
            $addr = UserAddress::get(['address_id' =>$addressId, 'user_id' => $this->user_id]);
            if (!$addr) {
                $this->apiError(-1, '该收货地址不存在！');
            }
        }

        // 获取发票
        $invoiceId = $this->request->post('invoice_id');
        if($invoiceId){
            $invoice = UserExtend::get(['id'=>$invoiceId,'user_id'=>$this->user_id]);
            if(!$invoice){
                $this->apiError(-1,'该发票不存在！');
            }
        }

        // 获取运送方式
        if($type!=4){
            $deliveryInfo = '';
            $shipping = Shipping::get(['shipping_id'=>$param['delivery_id']]);
            if(!$shipping){
                $this->apiError(-1,'该运送方式不存在！');
            }


            // 如果是自提
            if($shipping['shipping_code']=='ziti'){
                $sellerAddrId = $this->request->post('seller_address_id',0);
                if(!$sellerAddrId){
                    $this->apiError(-1,'门店自提地址id不能为空！');
                }
                $deliveryInfo = SellerAddress::get(['seller_id' => $this->sellerId,'seller_address_id'=>$sellerAddrId]);
                if(!$deliveryInfo){
                    $this->apiError(-1,'本门店无该自提地址！');
                }
                $regions = M('region')->cache(true)->getField('id,name');

                $deliveryInfo['province_name'] = $regions[$deliveryInfo['province_id']] ?: '';
                $deliveryInfo['city_name'] = $regions[$deliveryInfo['city_id']] ?: '';
                $deliveryInfo['district_name'] = $regions[$deliveryInfo['district_id']] ?: '';
                $deliveryInfo['twon_name'] = $regions[$deliveryInfo['town_id']] ?: '';

                $zitiInfo['user_name'] = $this->request->param('user_name','');
                $zitiInfo['user_mobile'] = $this->request->param('user_mobile','');
                $zitiInfo['plan_time'] = $this->request->param('plan_time','');

            }
        }
        // 商品信息
        $buyGoods = [];$totalMoney = 0;$finalPrice = 0;
        foreach ($goodsArr as $v) {
            $goods = GoodsModel::getGoods($this->storeId,$v['goods_id']);
            if($goods['tmpl_id'] <= 0){
                $this->apiError(-1,'该商品已下架！');
            }
            $specKey = isset($v['spec_key'])?$v['spec_key']:'';
            if($specKey)$specPrice = SpecGoodsPrice::get(['goods_id'=>$v['goods_id'],'key'=>$v['spec_key']]);
            if($masterOrderSn){
                if(OrderModel::get(['master_order_sn'=>$masterOrderSn])){
                   $this->api(-1,'你的预定订单已经生成，请勿重复操作！');
                }
                $reserveOrder = OrderModel::get([
                    'user_id'=>$this->user_id,
                    'order_sn'=>$masterOrderSn,
                    'prom_type'=>4
                ]);
                if(!$reserveOrder)$this->api(-1,'你没该预定单不存在！');
                foreach ($reserveOrder['goods'] as $orderGoods){
                    $finalPrice = $finalPrice+($orderGoods['goods_price']*$orderGoods['goods_num']);
                }
            }else{
                $finalPrice = \app\api\model\Goods::getSellPrice($goods,$specKey,$this->user_id);
            }

            $buyGoods[] = [
                'goods_id' => $goods['goods_id'],
                'goods_sn' => $goods['goods_sn'],
                'goods_name' => $goods['goods_name'],
                'goods_cover' => $goods['original_img'],
                'goods_num' => $v['goods_num'],
                'goods_price' =>  $finalPrice,// 最终销售价格（已经除掉折扣）
                'final_price' => $finalPrice,
                'spec_key' => $specKey,
                'spec_key_name' => isset($specPrice)?$specPrice['key_name']:'',
                'sku'=>isset($specPrice)?$specPrice['sku']:'',
                'store_id'=>$this->storeId
            ];
            $totalMoney = $totalMoney + ($finalPrice*$v['goods_num']);

        }

        // 优惠券抵扣
        $userCouponId = $this->request->post('user_coupin_id');
        $couponPrice = 0;

        $coupon_is_refund = 1;//要记录优惠券是否释放，默认为1就是释放
        $coupon_condition = 0;
        if($userCouponId){
            $couponListItem = CouponList::get($userCouponId);
            $coupon = Coupon::get($couponListItem['cid']);
            $coupon_is_refund = $coupon['is_refund'];//如果有值就替换掉
            $coupon_condition = $coupon['condition'];
            // 优惠券优惠金额
            $couponPrice = CouponList::deduction($this->user_id,$this->storeId,$userCouponId,['order_amount'=>$totalMoney]);
        }

        $finalAmount = $totalMoney - $couponPrice;

        $charges = 0;
        // 如果是分期
        if(isset($param['channel_type'])&&$param['channel_type']){
            // 如果是信用卡分期总金额必须为600元以上，只能为6,12期
            if($param['channel_type']=='CCFQ'){
                if(!($param['periods']==6||$param['periods']==12)){
                    $this->apiError(-1,'信用卡分期只支持6,12期');
                }
                if($finalAmount<600){
                    $this->apiError(-1,'信用卡分期不支持600元下的金额！');
                }
            }

            // 获取分期配置参数
            $rate = 0;
            foreach (config('fenqi') as $val){
                if($val['type']==$param['channel_type']){
                    foreach ($val['item'] as $item){
                        if($param['periods']==$item['term']){
                            $rate = $item['rate'];
                            break;
                        }
                    }
                    break;
                }
            }
            $charges = round($finalAmount*$rate);
            // 用户分期总费用= 商品金额+（商品金额*买家费率）
            $finalAmount = round(($finalAmount+$charges));

        }

        // 校验前端计算的价格
        if($param['final_amount']!=$finalAmount){
            $this->apiError(-1,'价格有变化，请刷新页面！',['param'=>$param,'right_amount'=>$finalAmount]);
        }

        $order = new OrderModel();
        $r1 = $order->save([
            'order_sn' => date('YmdHis').rand(1000,9999),
            'user_id' => $this->user_id,
            'master_order_sn'=>$this->request->param('master_order_sn',''),
            'consignee' => $addr?$addr['consignee']:'',
            'province' =>  $addr?$addr['province']:'',
            'city' =>  $addr?$addr['city']:'',
            'address' =>  $addr?$addr['address']:'',
            'district' =>  $addr?$addr['district']:'',
            'mobile' => $addr?$addr['mobile']:(isset($param['mobile'])?$param['mobile']:''),
            'goods_price'=>$totalMoney,// 商品总价
            'coupon_price'=>$couponPrice,
            'coupon_is_refund'=>$coupon_is_refund,
            'coupon_condition'=>$coupon_condition,
            'total_amount' => $finalAmount,// 订单总价
            'order_amount' => $finalAmount,// 应付款金额
            'charges'=>$charges,
            'store_id' => $this->storeId,
            'seller_id' => $this->sellerId,
            'remark'=>$this->request->post('remark',''),
            'invoice_title'=>isset($invoice['invoice_title'])?$invoice['invoice_title']:'',
            'invoice_type'=>isset($invoice['type'])?$invoice['type']:'',
            'invoice_email'=>$this->request->param('invoice_email',''),
            'taxpayer'=>isset($invoice['taxpayer'])?$invoice['taxpayer']:'',
            'shipping_id'=>$param['delivery_id']?$param['delivery_id']:'',
            'shipping_info'=>$deliveryInfo?$deliveryInfo:'',
            'ziti_info'=>empty($zitiInfo)?'':$zitiInfo,
            'shipping_code'=>$shipping['shipping_code']?$shipping['shipping_code']:'',
            'shipping_name'=>$shipping['shipping_name']?$shipping['shipping_name']:'',
            'transaction_id' => time().rand(10000,99999),
            'prom_type'=>$type,
            'add_time'=>time()
        ]);

        // 订单商品
        if ($r1) {
            // 如果只有一种商品，return_goods表里的商品最终单价修改为order_amount/num
            // 解决有优惠，手续费时，不知道要退款多少问题
            if(count($buyGoods)==1){
                $buyGoods[0]['final_price'] = round($finalAmount/$buyGoods[0]['goods_num']);
            }
            $r2 = $order->goods()->saveAll($buyGoods);
        }

        if ($r1 && $r2) {
            $order->goods;

            // 预定时不验证库存
            if($type!=4) {
                // 修改库存
                foreach ($buyGoods as $goods) {
                    // 是否共享库存（经销商多个店铺的某一个商品，库存一样）
                    if ($goods['spec_key']) {
                        // $isShare = SpecGoodsPrice::where(['goods_id' => $goods['goods_id'], 'key' => $goods['spec_key']])->value('is_share',0);
                        # 201903 修改
                        $isShare = GoodsModel::where('goods_id',$goods['goods_id'])->value('is_share_stock',0);
                    } else {
                        $isShare = 0    ;
                    }
                    if ($isShare==1) {
                        $logic = new GoodsLogic();
                        $skuRes = $logic->handleShareStockOrder($goods['goods_id'], $goods['spec_key'], $goods['goods_num'], $this->user_id, $order['order_sn']);
                        if ($skuRes['code'] != 1) {
                            $order->delete();
                            $this->apiError(-1, $skuRes['msg']);
                        }
                    } else {
                        $goodsModel = GoodsModel::get(['goods_id' => $goods['goods_id']]);
                        if($goodsModel['store_count']< $goods['goods_num']){
                            $order->delete();
                            $this->apiError(-1, $goodsModel['goods_name'].'库存不足,系统库存'.$goodsModel['store_count']);
                        }

                        //如果在spec_goods_price有记录，则需要扣这个。如果没有则扣主表的
                        $isHasSpecGoodsPrice = SpecGoodsPrice::get(['goods_id' => $goods['goods_id'], 'key' => $goods['spec_key']]);
                        if($isHasSpecGoodsPrice){
                            $storeCount = $isHasSpecGoodsPrice['store_count'] - $goods['goods_num'];
                            if ($storeCount < 0) $storeCount = 0;
                            $isHasSpecGoodsPrice['store_count'] = $storeCount;
                            $isHasSpecGoodsPrice->save();

                            $goodsModel['key_name'] = $isHasSpecGoodsPrice['key_name'];//每次都替换掉这个属性，就可以搞定这呆逼方法了
                            $goodsModel['spec_key_name'] = $isHasSpecGoodsPrice['key_name'];
                            update_stock_log($this->user_id, -$goods['goods_num'], $goodsModel,$order['order_sn'],'订单，商品有规格信息');
                            
                        }else{
                            $storeCount = $goodsModel['store_count'] - $goods['goods_num'];
                            if ($storeCount < 0) $storeCount = 0;
                            $goodsModel['store_count'] = $storeCount;
                            $goodsModel->save();
    
                    
                            $goodsModel['key_name'] = '';//每次都替换掉这个属性，就可以搞定这呆逼方法了
                            $goodsModel['spec_key_name'] = '';
                            update_stock_log($this->user_id, -$goods['goods_num'], $goodsModel,$order['order_sn'],'订单，而且商品没有规格信息');
                            
                        }

                    }
                }
            }
            $order = $order->toArray();

            // 优惠券已使用
            if($couponPrice){
                CouponList::confirmUse($this->user_id,$userCouponId,$order);
            }

            // 把发票信息写到另一张表
            if(!empty($invoice)){
                Invoice::create([
                    'order_id'=>$order['order_id'],
                    'user_id'=>$this->user_id,
                    'seller_id'=>$this->sellerId,
                    'store_id'=>$this->storeId,
                    'blag_way'=>$param['blag_way'],//索要方式：1纸质版 2电子版 3纸质版+电子版
                    'type'=>$invoice['type'],//0个人 2普通 2专票
                    'invoice_money'=>$finalAmount,//0 2018-10-19修复错误，数据需要恢复,
                    'invoice_title'=>$invoice['invoice_title'],
                    'invoice_desc'=>'',
                    'invoice_rate'=>0,
                    'taxpayer'=>$invoice['taxpayer'],// 纳税人识别号
                    'ctime'=>time(),
                ]);
            }

            unset($order['order_id']);
            
            return $order;
        } else {
            return null;
        }
    }


    /**
     * 删除订单
     */
    public function del()
    {
        $order_id = I('post.order_id', 0);

        $orderLogic = new \app\common\logic\OrderLogic;
        $orderLogic->setUserId($this->user_id);
        $return = $orderLogic->delOrder($order_id);

        $this->ajaxReturn($return);
    }

    /**
     * 取消订单
     */
    public function cancel()
    {
        $param = $this->request->post();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);
        $order = OrderModel::get(['user_id'=>$this->user_id,'order_sn'=>$param['order_sn']]);
        if(!$order){
            $this->apiError(-1,'该订单不存在！');
        }

        // 检查是否有使用优惠券，有则退回
        CouponList::drawBack($order['order_id']);

        // 回退库存
        foreach ($order['goods'] as $goods){
            GoodsModel::rollbackStock($goods['goods_id'],$goods['spec_key'],$goods['goods_num']);
        }

        //检查是否有积分，余额支付
        $logic = new OrderLogic();
        $data = $logic->cancel_order($this->user_id, $order['order_id']);
        $this->ajaxReturn($data);
    }

    public function confirm()
    {
        $id = I('get.id/d', 0);
        $data = confirm_order($id, $this->user_id);
        if (!$data['status'])
            $this->error($data['msg']);
        else
            $this->success($data['msg']);
    }

    /**
     * 支付方式
     */
    public function payWay(){

        // 支付方式
        $path = $this->request->domain().'/public/icon/';
        $data = [
            ['pay_id'=>1,'type'=>'kq','name'=>'快捷支付','icon'=>$path.'icon-kq.gif'],
            ['pay_id'=>2,'type'=>'wx','name'=>'微信支付','icon'=>$path.'icon-wx.png'],
            //['pay_id'=>3,'type'=>'ali','name'=>'支付宝支付','icon'=>$path.'icon-zfb.png']
        ];

        // 是否购物车订单
        $isCar  = $this->request->post('is_cart');
        if($isCar){
            $this->apiSuccess($data);
        }

        $param = $this->request->post();
        $rule = ['goods_id' => 'require',];
        $this->validate($param, $rule);
        $specKey = $this->request->post('spec_key');

        // 查询商品
        $where = [
            'store_id'=>$this->storeId,
            'goods_id'=>$param['goods_id'],
            'is_deleted'=>0,
            'is_on_sale' => 1,
        ];
        $goods = GoodsModel::get($where);
        if(!$goods){
            $this->apiError(-1,'该商品不存在，或者已下架！');
        }

        // 不是单品时，查询商品组合属性价格
        if($specKey){
            $where = [
                'store_id'=>$this->storeId,
                'goods_id'=>$param['goods_id'],
                'key'=>$specKey
            ];
            $specGoodsPriceModel = SpecGoodsPrice::get($where);
            if(!$specGoodsPriceModel){
                $this->apiError(-1,'该商品无'.$param['spec_key'].'属性组合方式！');
            }
        }

        // 增加分期支付方式
        if($goods['is_pay']){

            // 如果是预定单的分期，取orderGoods表里的价格（为了兼容仅销售商中途修改商品价格）
            $reserveOrderSn = $this->request->param('reserve_order_sn');
            if($reserveOrderSn){
                $money = OrderGoods::getReservePrice($reserveOrderSn);
            }else{
                $money = round($specGoodsPriceModel['price']+$goods['shop_price']);
            }

            // 如果选择优惠券抵扣
            $userCouponId = $this->request->post('user_coupin_id');
            $couponPrice = 0;
            if($userCouponId){
                $couponPrice = CouponList::deduction($this->user_id,$this->storeId,$userCouponId,['order_amount'=>$money]);
            }
            $money = $money- $couponPrice;

            $fenqiList = config('fenqi');
            foreach ($fenqiList as $k=>$fenqi){
                foreach ($fenqi['item'] as $key =>$term){
                    $fenqiList[$k]['item'][$key]['each_term_money'] = round(($money+$money*$term['rate'])/$term['term']).'.00'; // 用户每期总费用=（商品金额+商品金额*买家费率）／期数；
                    $fenqiList[$k]['item'][$key]['each_term_charge'] = round($money*$term['rate']/$term['term']).'.00';// 用户每期手续费=（商品金额*买家费率）／期数；
                    $fenqiList[$k]['item'][$key]['is_include_charge'] = true;
                }
            }
            $data[] =   ['pay_id'=>3,'name'=>'分期','type'=>'fenqi', 'list'=>$fenqiList];
        }

        $this->apiSuccess($data);
    }

    /**
     * 运送方式
     */
    public function deliveryWay(){

        // 查询本门店，运送方式
        $wayList = Shipping::field('*')
            ->alias('a')
            ->join('seller_shipping b','a.shipping_id = b.shipping_id')
            ->where(['seller_id'=>$this->sellerId])
            ->select();

        // 方式明细
        $data = [];
        foreach ($wayList as $way){
            $zitiList = [];
            // 门店自提点
            if($way['shipping_code']=='ziti'){
                $zitiList = SellerAddress::all(['seller_id' => $this->sellerId,'type'=>2]);
                $regions = M('region')->cache(true)->getField('id,name');
                foreach ($zitiList as &$addr) {
                    $addr['province_name'] = $regions[$addr['province_id']] ?: '';
                    $addr['city_name'] = $regions[$addr['city_id']] ?: '';
                    $addr['district_name'] = $regions[$addr['district_id']] ?: '';
                    $addr['twon_name'] = empty($addr['town_id'])?'':$regions[$addr['town_id']];
                    $addr['address'] = $addr['address'] ?: '';
                }
            }
            $data[] = ['delivery_id'=>$way['shipping_id'],'name'=>$way['shipping_name'],'list'=>$zitiList];
        }

        $this->apiSuccess($data);
    }

    /**
     * 确认收货
     */
    public function receiveGoods()
    {
        $param = $this->request->post();
        $rule = ['order_sn' => 'require'];
        $this->validate($param, $rule);
        $order = OrderModel::get(['user_id'=>$this->user_id,'order_sn'=>$param['order_sn']]);
        if(!$order){
            $this->apiError(-1,'该订单不存在！');
        }
        $data = confirm_order($order['order_id'], $this->user_id);
        $this->ajaxReturn($data);
    }

    /**
     * 查询订单物流信息
     */
    function queryExpressInfo(){
        $param = $this->request->post();
        $rule = [
            'order_sn' => 'require'
        ];
        $this->validate($param, $rule);

        $express = Db::name('delivery_doc')->where("order_sn" , $param['order_sn'])->find();
        if(!$express){
            $this->apiError(1021,'还未发货！');
        }

        $info = queryExpressInfo($express['shipping_code'],$express['invoice_no'],$express['mobile']);
        if(!$info) {
            $this->apiError(1021,'暂无物流快递信息！');
        }

        $data = [
            'order_sn'=>$param['order_sn'],
            'shipping_code'=>$express['shipping_code'],
            'invoice_no'=>$express['invoice_no'],
        ];
        $this->apiSuccess(array_merge($data,$info));

    }

    /**
     * 申请退货
     */
    public function returnReason()
    {
        $this->apiSuccess([
            '购买型号与实际收货型号不符',
            '验证商品非国行正品',
            '商品质量问题',
            '申请7天无理由退货',
            '其他原因',
        ]);
    }

    /**
     * 申请退货
     */
    public function returnGoods()
    {
        $param = $this->request->param();
        $rule = [
            'type'=>'require|in:0,1',//  0仅退款 1退货退款  2换货 3维修
            'order_sn'=>'require',
            'rec_id'=>'require',
            'reason'=>'require',
            'serial_no'=>'requireIf:type,1',
            'describe'=>'require',
            'imgs'=>'requireIf:type,1',
        ];
        $this->validate($param,$rule);
        // 判断订单
        $order = OrderModel::where(['order_sn'=>$param['order_sn'],'user_id'=>$this->user_id])->find();
        if(!$order){
            $this->apiError(-1,'该订单不存在');
        }

        // 判断支付状态
        if($order['pay_status']!=1){
            $this->apiError(-1,'订单未支付，无法退款！');
        }
        //Db::execute('LOCK TABLE tp_return_goods READ');// 加表读锁

        // 判断是否重复提交申请售后
        $returnGoods = M('return_goods')
            ->where(['order_id'=>$order['order_id'],'status'=>['in','0,1']])
            ->find();
        if($returnGoods){
            $this->apiError(-1,'该订单已经申请售后，请勿重复操作');
        }

        //Db::execute('LOCK TABLE tp_return_goods WRITE');// 加表写锁
        $model = new OrderLogic();
        $res = $model->addReturnGoods($param['rec_id'],$order);  //申请售后
        if($res['status']!=1){
            $this->apiError(-1,$res['msg']);
        }

        //Db::execute('UNLOCK TABLES');// 解表锁

        // 状态
        foreach ($order->goods as $orderGoods){
            if($orderGoods->rec_id == $param['rec_id']){
               $orderGoods->doReturn($param['type']);
            }
        }

        $order->save();
        $this->ajaxReturn($res);
    }

    /**
     * 退换货列表
     */
    public function returnGoodsList()
    {
        $keywords = I('keywords', '');
        $addtime = I('addtime');
        $status = I('status', 0);


        $logic = new OrderLogic;
        $data = $logic->getReturnGoodsList($keywords, $addtime, $status,$this->user_id);

        $state = C('RETURN_STATUS');
        foreach ($data as $key => $val) {
            $val['goods_name'] = $data['goodsList'][$val['goods_id']];
            $val['status_name'] = $state[$val['status']];
        }
        $this->apiSuccess($data);

    }

    /**
     *  退货详情
     */
    public function returnGoodsInfo()
    {
        $param = $this->request->param();
        $rule = ['order_sn'=>'require'];
        $this->validate($param,$rule);

        // 判断订单
        $order = M('order')->where(['order_sn'=>$param['order_sn'],'user_id'=>$this->user_id])->find();
        if(!$order){
            $this->apiError(-1,'该订单不存在');
        }

        $return_goods = M('return_goods')
            ->where(['order_id' => $order['order_id'],'user_id'=>$this->user_id])
            ->order('addtime','desc')
            ->select();
        if(!$return_goods){
            $this->apiError(-1,'该订单未申请售后');
        }

        $state = C('RETURN_STATUS');
        $goodsIdArr = [];
        foreach ($return_goods as $k=>$v){

            // 去除重复退货申请
            if(in_array($v['goods_id'],$goodsIdArr)){
                unset($return_goods[$k]);
                continue;
            }
            $goodsIdArr[] = $v['goods_id'];

            if ($v['imgs']) {
                $return_goods[$k]['imgs'] = explode(',', $v['imgs']);
            }
            if ($v['seller_delivery']) {

                $return_goods[$k]['seller_delivery'] = unserialize($v['seller_delivery']);
            }
            if ($v['delivery']) {
                $return_goods[$k]['delivery'] = unserialize($v['delivery']);
            }
            $return_goods[$k]['status_name'] = $state[$v['status']];
            $return_goods[$k]['store_name'] = $this->store['store_name'];
        }


        $this->ajaxReturn(['status' => 1, 'msg' => '获取成功',
            'result' => [
                'order'=>$order,
                'return_goods' => $return_goods
            ]
        ]);
    }

    /**
     * 用户退货提交物流
     */
    public function returnLogistics(){

        $param = $this->request->param();
        $rule = [
            'return_id'=>'require',
            'delivery_id'=>'require'
        ];
        $this->validate($param,$rule);

        // 获取物流
        $shipping = Shipping::get(['shipping_id'=>$param['delivery_id']]);
        if(!$shipping){
            $this->apiError(-1,'该运送方式不存在！');
        }

        // 如果是自提
        if($shipping['shipping_code']=='ziti'){
            $sellerAddrId = $this->request->post('seller_address_id',0);
            if(!$sellerAddrId){
                $this->apiError(-1,'门店地址id不能为空！');
            }
            $deliveryInfo = SellerAddress::get(['seller_id' => $this->sellerId,'seller_address_id'=>$sellerAddrId]);
            if(!$deliveryInfo){
                $this->apiError(-1,'本门店无该自提地址！');
            }
            $regions = M('region')->cache(true)->getField('id,name');
            
            $deliveryInfo['province_name'] = $regions[$deliveryInfo['province_id']] ?: '';
            $deliveryInfo['city_name'] = $regions[$deliveryInfo['city_id']] ?: '';
            $deliveryInfo['district_name'] = $regions[$deliveryInfo['district_id']] ?: '';
            $deliveryInfo['twon_name'] = $regions[$deliveryInfo['town_id']] ?: '';
           
           
            $data['delivery'] = serialize($deliveryInfo);
    
            $data['delivery_type'] = 1;//物流
            
        }else{
            $expressSn = $this->request->post('express_sn');
            if(!$expressSn){
                $this->apiError(-1,'运单号不能为空！');
            }
    
            $data['delivery_type'] = 0;//快递

            $post_data['delivery']['express_name'] = $shipping['shipping_name'];    //快递公司
            //$post_data['delivery']['express_fee'] = $shipping['express_fee'];     //快递用费(元)
            $post_data['delivery']['express_sn'] = $expressSn;  //快递单号
            //$post_data['delivery']['express_time'] = $shipping['express_time'];  //发货时间
            $data['delivery'] = serialize($post_data['delivery']);
        }
        
        
        $data['status'] = 2;// -2用户取消 -1不同意 0待审核 1通过 2已发货 3待退款 4换货完成 5退款完成 6申诉仲裁 7退款失败
        $res = DB::name('return_goods')->where(['id'=>$param['return_id'],'user_id'=>$this->user_id])->save($data);
        if ($res){
            $this->ajaxReturn(['status' => 1, 'msg' => '退回物流信息添加成功！']);
        }else{
            $this->ajaxReturn(['status' => -1, 'msg' => '退回物流信息失败！']);
        }
    }

    public function returnGoodsRefund()
    {
        $order_sn = I('order_sn');
        $where = array('user_id'=>$this->user_id);
        if($order_sn){
            $where['order_sn'] = $order_sn;
        }
        $where['status'] = 5;
        $count = M('return_goods')->where($where)->count();
        $page = new Page($count,10);
        $list = M('return_goods')->where($where)->order("id desc")->limit($page->firstRow, $page->listRows)->select();
        $goods_id_arr = get_arr_column($list, 'goods_id');
        if(!empty($goods_id_arr))
            $goodsList = M('goods')->where("goods_id in (".  implode(',',$goods_id_arr).")")->getField('goods_id,goods_name');
        $this->assign('goodsList', $goodsList);
        $state = C('RETURN_STATUS');
        $this->assign('list', $list);
        $this->assign('state',$state);
        $this->assign('page', $page->show());// 赋值分页输出
        return $this->fetch();
    }

    public function returnGoodsCancel()
    {
        $id = I('id',0);
        $is_json = I('is_json', 0);
        $return_goods = M('return_goods')->where(array('id'=>$id, 'user_id'=>$this->user_id))->find();
        if (empty($return_goods)) {
            if ($is_json) {
                $this->ajaxReturn(['status' => -1, 'msg' => '参数错误']);
            }
            $this->error('参数错误');
        }

        M('return_goods')->where(array('id'=>$id))->save(array('status'=>-2,'canceltime'=>time()));
        if ($is_json) {
            // 订单商品表状态
            $orderGoods = OrderGoods::get($return_goods->rec_id);
            if($orderGoods){
                $orderGoods->cancelReturn();
            }
            $this->ajaxReturn(['status' => 1, 'msg' => '取消成功']);
        }
        $this->success('取消成功',U('order/return_goods_list'));
    }

    /**
     * 修预定状态
     */
    function changeReserveStatus(){
        $param = $this->request->param();
        $rule = [
            'order_sn'=>'require',
        ];
        $this->validate($param,$rule);

        $orderId = OrderReserve::alias('r')
            ->join('Order o','o.order_id=r.order_id')
            ->where(['o.order_sn'=>$param['order_sn'],'user_id'=>$this->user_id])
            ->value('o.order_id');
        if(!$orderId){
            $this->apiError(-1,'无该预定订单');
        }

        $res = OrderReserve::update(['reserve_status'=>2],['order_id'=>$orderId]);
        $this->apiSuccess($res,'修改成功！');
    }

    /**
     * 回收
     */
    function recycle(){

        // 验证数据
        $data = json_decode(file_get_contents('php://input'),true);
        Log::write($data,'recycle');
        $ahs = new AHSClient();
        if(!$ahs->verifyData($data)){
            Log::write('签名未通过！','recycle');
            $this->result([],500,'签名未通过！');
        }

        // 获取用户信息
        $str =  Aes::decrypt($data['user_id']);// 解密
        Log::write('解密后的用户信息：'.$str,'recycle');
        $arr = explode('_',$str);
        $userId =  $arr[1];
        $userInfo = Users::getUserById($userId);
        if(empty($userInfo['user'])){
            Log::write('用户不存在！user_id='.$userId,'recycle');
            $this->result([],500,'用户不存在！');
        }

        // 检查订单是否存在，存在则修改
        $order = OrderModel::get(['transaction_id'=>$data['order_no']]);
        if($order){
            $order['status'] = $data['status'];
            $order['status_tip'] = $data['t_status'];
            $order['can_cancel'] = $data['can_cancel']?1:0;
            $order->save();
        }else {
            $order = new OrderModel();
            $orderGoods[] = [
                'goods_id' => 0,
                'goods_sn' => 0,
                'goods_name' => $data['quality_report']['product_name'],
                'goods_cover' => $data['quality_report']['product_image'],
                'goods_num' => 1,
                'goods_price' => $data['quality_report']['final_price'],
                'final_price' => $data['quality_report']['final_price'],
                'spec_key' => '',
                'spec_key_name' => $data['quality_report']['sku_name'],
                'sku' => '',
                'store_id' => $this->storeId
            ];

            $r1 = $order->save([
                'order_sn' => date('YmdHis') . rand(1000, 9999),
                'user_id' => $userId,
                'status'=>$data['status'],
                'status_tip'=>$data['t_status'],
                'can_cancel'=>$data['can_cancel']?1:0,
                'mobile' => $data['recycle_order']['contact_phone'],
                'goods_price' => $data['submited_amount'],// 商品总价
                'total_amount' => $data['submited_amount'],// 订单总价
                'order_amount' => $data['submited_amount'],// 应付款金额
                'recycle_info'=> $data['product_amounts'],
                'charges' => 0,
                'store_id' => $userInfo['store']['store_id'],
                'seller_id' => $userInfo['seller']['seller_id'],
                'remark' => $this->request->post('remark', ''),
                'transaction_id' => $data['order_no'],
                'prom_type' => 7,// 订单类型：0默认，1抢购，2团购，3优惠，4预售，5虚拟，6拼团，7回收
                'add_time' => time()
            ]);
            if (!$r1) {
                Log::write('创建回收订单失败！', 'recycle');
                $this->result($data, 500, '创建回收订单失败！');
            }

            $r2 = $order->goods()->saveAll($orderGoods);
            if (!$r2) {
                $order->delete();
                Log::write('回收订单商品写入失败！', 'recycle');
                $this->result($data, 500, '回收订单商品写入失败！');
            }
        }

        $order->goods;
        Log::write('回收订单推送成功', 'recycle');
        Log::write($order->toArray(), 'recycle');
        $this->apiSuccess($data, '回收订单推送成功！');
    }

    /**
     * @param $order
     */
    private function notice($order)
    {
        if ($this->user['mobile']) { // 发短信
            $sms = new SmsLogic();
            $sms->send(4, $this->user['mobile'], [
                'user_name' => $this->user['mobile'],
                'order_no' => $order['order_sn'],
                'goods_name' => $order['goods'][0]['goods_name']
            ]);
        } else {
            $content = '尊敬的' . $this->user['email'] . '用户，您的订单' . $order['order_sn'] . '已生成，购买的商品是' . $order['goods'][0]['goods_name']. '，请您及时查收!';
            sendEmail($this->user['email'], '下单成功', $content);
        }
    }
}