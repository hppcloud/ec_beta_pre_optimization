<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date Time: 2018/5/30 17:20
 * Author: Wanzhou Chen
 * Email: 295124540@qq.com
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\model\Region;

class Address extends Base
{
    function index(){
        $parentId = $this->request->post('parent_id',0);
        $this->apiSuccess(Region::all(['parent_id'=>$parentId]));
    }

    /**
     * 全部
     */
    function all(){
        $location = cache('location_json');
        if($location){
            $this->apiSuccess($location);
        }
        set_time_limit(0);
        $province = Region::all(['parent_id'=>0],null,true);
        foreach ($province as $k=>$v){
            $city = Region::all(['parent_id'=>$v['id']],null,true);
            $province[$k]['list'] = $city;
            foreach ($city as $key=>$val){
                $district = Region::all(['parent_id'=>$val['id']],null,true);
                $province[$k]['list'][$key]['list'] = $district;
                unset($district);
            }
            unset($city);
        }
        cache('location_json',$province);
        $this->apiSuccess($province);
    }
}