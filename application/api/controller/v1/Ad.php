<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018\6\3 0003
 * Time: 19:24
 */

namespace app\api\controller\v1;

use think\Db;
use app\api\controller\Base;
class Ad extends Base
{
     public function getImg()
     {
         $time = time();
         // 公共广告5个
         $where = [
             'pid'=>402,
             'is_all'=>1,
             'enabled'=>1,
             'start_time'=>['<=',$time],
             'end_time'=>['>=',$time]
         ];
         $sysad=Db::table('tp_ad')
             ->field('ad_link,ad_code,target,enabled')
             ->where($where)
             ->order('orderby desc')
             ->limit(5)
             ->select();

         // 门店广告取5个
         $where = [
             'pid'=>402,
             "!find_in_set('$this->storeId',store_id_list)"=>'',
             'enabled'=>1,
             'is_all'=>0,
             'start_time'=>['<=',$time],
             'end_time'=>['>=',$time]
         ];
         $storead=Db::table('tp_ad')
             ->field('ad_link,ad_code,target,enabled,store_id_list')
             ->where($where)
             ->order('orderby desc')
             ->limit(5)
             ->select();
         $this->apiSuccess(array_merge($sysad,$storead));
     }
}