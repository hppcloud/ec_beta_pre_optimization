<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/9
 * Time: 16:08
 */

namespace app\api\controller\v1;

use app\common\model\Users;
use utils\Aes;
use aihuishou\AHSClient;
use app\api\controller\Base;
use app\common\logic\GoodsLogic;
use app\common\model\CouponList;
use app\common\model\GoodsCoupon;
use app\common\model\Goods as GoodsModel;
use app\common\model\Order as OrderModel;
use app\common\model\SpecGoodsPrice;
use think\Log;

class Coupon extends Base
{
    /**
     * 下单可用优惠券
     */
    function ableUse(){
        $param = $this->request->param();
        $rule = ['goods'=>'require|array'];
        $this->validate($param,$rule);
        $time = time();
        $where = [
            'uid'=>$this->user_id,
            'cl.store_id'=>$this->storeId,
            'cl.status'=>0,// 0未使用1已使用2已过期
            'use_time'=>0,
            'deleted'=>0,
            'c.status'=>1,
            'use_start_time'=>['<=',$time],
            'use_end_time'=>['>=',$time]
        ];

        $couponList = CouponList::field('c.id tpl_id,cl.id,name,cl.money,c.money amount,cl.type,condition,use_type,use_start_time,use_end_time,c.coupon_info')
            ->alias('cl')
            ->join('Coupon c','cl.cid=c.id')
            ->where($where)
            ->order('send_time','desc')
            ->limit(10)
            ->select();

        // 修复优惠券金额,兼容之前的规则
        foreach ($couponList as $v){
            if($v['money']==0&&$v['amount']!=0){
                $v['money'] = $v['amount'];
                CouponList::update(['money'=>$v['amount']],['id'=>$v['id']]);
            }
        }

        // 查询商品,价格
        $totalMoney = 0;$goodsCategoryArr = [];
        foreach ($param['goods'] as $v){
            $goods = GoodsModel::getGoods($this->storeId,$v['goods_id']);
            $goodsCategoryArr[] = $goods['cat_id3'];
            $goodsPrice = $goods['shop_price'] ? $goods['shop_price']:0;
            // 不为单品
            $attrPrice = 0;
            if($v['spec_key']){
                $specPrice = SpecGoodsPrice::where(['goods_id' => $v['goods_id'], 'key' => $v['spec_key']])->find();
                if (!$specPrice) {
                    $this->apiError(-1, '该spec_key='.$v['spec_key'].'的商品属性组合不存在！');
                }
                $attrPrice = $specPrice['price']?$specPrice['price']:0;
            }

            // 商品价格浮动率
            $logic = new GoodsLogic();
            $rate = $logic->getRate($v['goods_id'],$this->user_id);

            // 最终商品价格 = （商品基础价格 + 加上属性价格) * 浮动率
            $totalMoney = $totalMoney + (round($goodsPrice + $attrPrice)  * $rate)*$v['buy_num'];

        }

        // 使用范围：0全店通用 1指定商品可用 2指定分类商品可用 3设置指定店铺使用
        foreach ($couponList as $coupon) {

            $coupon['is_able_use'] = true;

//            // 直接在sql过滤
//            if(!empty($coupon['use_start_time']) && !empty($coupon['use_end_time'])){
//                if($time<$coupon['use_start_time']||$time>$coupon['use_end_time']){
//                    $coupon['is_able_use'] = false;
//                }
//            }
            

            // 高于订单总额-0
            if($coupon['money'] > $totalMoney){
                $coupon['is_able_use'] = false;
            }

            // 满额可用-0
            if($coupon['condition'] > $totalMoney){
                $coupon['is_able_use'] = false;
            }

            // 指定商品可用
//            if ($coupon['use_type'] == 1) {
//                foreach ($param['goods'] as $goods){
//                    $goods_id[] = $goods['goods_id'];
//                }
//                GoodsCoupon::all($goods_id);
//                foreach ($couponItem['goods_coupon'] as $goodsCoupon => $goodsCouponItem) {
//                    if (in_array($goodsCouponItem['goods_id'], $goods_ids)) {
//                        $tmp = $userCouponItem;
//                        $tmp['coupon'] = array_merge($couponItem->append(['use_type_title'])->toArray(), $goodsCouponItem->toArray());
//                        $userCouponArr[] = $tmp;
//                        break;
//                    }
//                }
//            }
            //限定商品类型
            if ($coupon['use_type'] == 2 ) {
                $goodsCategoryArr = array_unique($goodsCategoryArr);// 商品分类去重
                $couponGoodsCategoryArr = GoodsCoupon::where(['coupon_id'=>$coupon['tpl_id'],'goods_category_id'=>['<>',0]])->column('goods_category_id');
                foreach ($goodsCategoryArr as $category) {
                    // 购买商品的分类，不在设定的优惠券限制类型id内，不可用
                    if (!in_array($category, $couponGoodsCategoryArr)) {
                        $coupon['is_able_use'] = false;
                        break;
                    }
                }
            }

        }
        
        $this->apiSuccess($couponList);
    }


    /**
     * 发放优惠券【回收】
     */
    function giveOutByRecycle(){

        // 验证数据
        $data = $this->request->param();
        Log::write($data,'recycle');
        $ahs = new AHSClient();
        if(!$ahs->verifyData($data)){
            Log::write('签名未通过！','recycle');
            $this->result([],500,'签名未通过！');
        }

        // 获取用户信息
        $str =  Aes::decrypt($data['user_id']);
        $arr = explode('_',$str);
        $userId =  $arr[1];
        $userInfo = Users::getUserById($userId);
        if(!$userInfo){
            Log::write('用户不存在！','recycle');
            $this->result([],500,'用户不存在！');
        }

        // 获取订单信息
        $order = OrderModel::get(['transaction_id'=>$data['order_no']]);
        if(!$order){
            Log::write('查无订单','recycle');
            $this->result([],500,'查无订单！');
        }

        // 新增优惠券
        $rt = CouponList::presentByRecycle($userId,$userInfo['store']['store_id'],$userInfo['seller']['seller_id'],$order['order_id'],$data['final_amount']);
        if($rt){
            Log::write('优惠券新增成功！！','recycle');
            $this->result($rt,200,'优惠券新增成功！！');
        }else{
            Log::write('优惠券新增失败！','recycle');
            $this->result([],500,'优惠券新增失败！');
        }
    }

    /**
     * 是否有新优惠券通知
     */
    function isNewNotify(){
        $where = [
            'cl.uid'=>$this->user_id,
            'cl.status'=>0,
            'cl.type'=>4,// 只提示注册赠送的
            'cl.notify_time'=>null
        ];
        $list = CouponList::field('cl.*,c.use_start_time,c.use_end_time')
            ->alias('cl')
            ->join('Coupon c','c.id=cl.cid')
            ->where($where)
            ->select();
        foreach ($list as $coupon){
            $coupon['notify_time'] = time();
            $coupon->save();
        }
        $num = count($list);
        $this->apiSuccess([
            'have_notify'=>$num?true:false,
            'new_num'=>$num,
            'list'=>$list
        ]);
    }
}