<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/10
 * Time: 22:43
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\logic\OssLogic;

class Oss extends Base
{
    /**
     * 文件上传
     */
    function upload(){
        if(!isset($_FILES['file'])){
            $this->apiError(-1,'请上传文件！');
        }

        $fileName = $_FILES['file']['name'];

        $client = new OssLogic();
        $url = $client->uploadFile($_FILES['file']['tmp_name'],date('Y/m/').md5($fileName).'.'.pathinfo($fileName, PATHINFO_EXTENSION));

        $this->apiSuccess($url,'上传成功！');
    }
}