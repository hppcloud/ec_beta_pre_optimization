<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/6/17
 * Time: 20:26
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\common\model\CouponList;
use app\common\model\Goods as GoodsModel;
use app\common\model\Order as OrderModel;
use app\common\model\OrderReserve;

class Task extends Base
{
    public function _initialize()
    {
        set_time_limit(600);
    }

    /**
     * 定时任务，刷新订单【待支付，待收货】
     */
    function refreshOrder(){

        $config = config('order');

        // ================================ 待支付订单============================================
        $where = [
            'order_status'=>0,
            'pay_status'=>0,
            'prom_type'=>0,// 普通订单
            'create_time'=>['<',time()- $config['order_time']]
        ];
        $orderList = OrderModel::all($where,[],true);
        foreach ($orderList as $order){
            echo '<h3>待支付订单：'.$order['order_sn'].'已取消!</h3>';
            // 修改状态 0待确认，1已确认，2已收货，3已取消，
            $order['order_status'] = 3;
            $order->save();
            // 检查是否有使用优惠券，有则退回
            CouponList::drawBack($order['order_id']);
            // 回退库存
            foreach ($order['goods'] as $goods){
                echo '商品：'.$goods['goods_id'].'已回退'.$goods['goods_num'].'个库存！<br>';
                GoodsModel::rollbackStock($goods['goods_id'],$goods['spec_key'],$goods['goods_num']);
            }
        }

        // ==================================待收货订单=============================================
        $where = [
            'order_status'=>1,
            'pay_status'=>1,
            'shipping_status'=>1,
            'shipping_time'=>['<',time()-$config['shipping_time']]
        ];
        $orderList = OrderModel::all($where,[],true);
        foreach ($orderList as $order){
            $order['order_status'] = 4; // 已收货，交易完成
            $order->save();
        }

        // =================================预定订单，超过结束时间时关闭==============================
        $where  = [
            'reserve_status'=>['<>',-1],
            'end_time'=>['<',time()]
        ];
        $orderReserve = OrderReserve::all($where);
        foreach ($orderReserve as $reserve){
            OrderModel::update(['order_status'=>3],['order_id'=>$reserve['order_id']]);
            $reserve['reserve_status'] = -1; // 关闭
            $reserve->save();
        }

        $this->apiSuccess([],'刷新成功！');
    }
}