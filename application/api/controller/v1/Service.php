<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/11/10
 * Time: 15:08
 */

namespace app\api\controller\v1;


use app\api\controller\Base;
use app\api\model\Service as ServiceModel;
use app\api\model\ServiceList;

class Service extends Base
{
    function index(){
        $this->apiSuccess(ServiceModel::getList($this->user_id,$this->request->param('type',0)));
    }

    /**
     * 使用服务
     */
    function doUse(){
        $param = $this->request->param();
        $rule = [
            'service_id' => 'require'
        ];
        $this->validate($param, $rule);

        $service = ServiceList::get(['id'=>$param['service_id'],'user_id'=>$this->user_id]);
        if(!$service){
            $this->apiError(-1,'该服务不存在！');
        }
        if($service['status']===1){
            $this->apiError(-2,'该服务已被使用！');
        }
        if($service['use_end_time']<time()){
            $this->apiError(-3,'该服务已过期！');
        }
        if($service['status']===2){
            $this->apiError(-4,'该服务不可用！');
        }
        $service['status']=1;
        $service->save();

        $serviceTpl = ServiceModel::get($service['service_id']);
        if($serviceTpl){
            $serviceTpl['use_num'] = $serviceTpl['use_num']+1;
            $serviceTpl->save();
        }
        $this->apiSuccess($service);
    }
}