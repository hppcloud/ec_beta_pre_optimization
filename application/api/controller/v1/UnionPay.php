<?php
/**
 * Created by PhpStorm.
 * User: function
 * Date: 2018-10-26
 * Time: 21:31
 *
 * 1.先查询是否开通
 * 1.1 如果已经开通，就进行步骤2
 * 1.2.如果没有开通，就在data里面返回包含自动跳转到银联开通页面的数据
 * 3.用订单信息获取token
 * 4.交易
 */

namespace app\api\controller\v1;
use fun\UnionClient;
use app\api\controller\Base;
use app\common\model\Order as OrderModel;

class UnionPay extends Base
{
    /**
     * 获取订单
     */
    private function getOrder($orderSn)
    {
        $order = OrderModel::get(['order_sn' =>$orderSn, 'user_id' => $this->user_id, 'deleted' => 0]);
        if (!$order) {
            $this->apiError(-1, '该订单不存在！');
        }

        // 检查订单状态
        if ($order['pay_status'] != 0) {
            $this->apiError(-1, '该订单已支付！');
        }

        // 检查订单金额
        if ($order['order_amount'] <= 0) {
            $this->apiError(-1, '该订单金额为' . $order['order_amount'] . ',无法支付！');
        }

        $order->goods;

        return $order;
    }

    /**
     * 获取
     */
    public function index(){
        $this->apiSuccess([],'银联支付');
    }

    /**
     * H5支付 = 支付宝
     */
    public function wapPay(){
        $param = $this->request->param();
        $rule = [
            'order_sn' => 'require',
            'pay_channel|支付渠道'=>'require|in:h5_ali_pay,qr_pay'
        ];
        $this->validate($param,$rule);
        $order = $this->getOrder($param['order_sn']);
        $uClient =  new UnionClient();
        $this->apiSuccess($uClient->getPayUrl($param['pay_channel'],$order));
    }

    /**
     * 支付异步通知
     */
    function notify(){

    }

    /**
     * 前端回调
     */
    function callback(){

        if ($ok == 1) {
            switch ($_REQUEST['payResult']) {
                case '10':
                    //此处做商户逻辑处理
                    $order = OrderModel::get(['order_sn' => $_REQUEST['orderId']]);
                    if ($order) {
                        $order['order_status'] = 1;
                        $order['pay_status'] = 1;
                        $order['paid_money'] = $_REQUEST['orderAmount'] / 100;
                        $order['pay_time'] = time();
                        $order['transaction_id'] = $_REQUEST['dealId'];
                        $order->save();

                        $this->sendNotify($order);// 邮件通知，短信通知

                    }

                    $rtnOK = 1;
                    //以下是我们快钱设置的show页面，商户需要自己定义该页面。
                    $rtnUrl = $this->webHost . "/order/success";
                    break;
                default:
                    $rtnOK = 0;
                    //以下是我们快钱设置的show页面，商户需要自己定义该页面。
                    $rtnUrl = $this->webHost . "/order/error";
                    break;

            }

        } else {
            $rtnOK = 0;
            //以下是我们快钱设置的show页面，商户需要自己定义该页面。
            $rtnUrl = $this->webHost . "/order/error";

        }
        echo "<result>$rtnOK</result><redirecturl>$rtnUrl</redirecturl>";
    }

    /**
     * 发送支付通知（邮箱，短信）
     */
    private function sendNotify($order)
    {
        foreach ($order->goods as $goods){
            $goods['spec_key_name'] = str_replace(' ',' &nbsp;&nbsp;&nbsp;',$goods['spec_key_name']);
            $goods['spec_key_name'] = str_replace(':','：&nbsp;',$goods['spec_key_name']);
        }
        $order['store_name'] = StoreModel::where(['store_id' => $order['store_id']])->value('store_name');
        $order['seller_contact'] = Seller::where(['seller_id' => $order['seller_id']])->value('seller_contact');
        $order['goods_item_num'] = count($order['goods']);
        $order['pay_time'] = date('Y-m-d', $order['pay_time']);

        // 地址信息
        if (!$order['ziti_info']) {
            $regions = M('region')->cache(true)->getField('id,name');
            $order['province'] = $regions[$order['province']] ?: '';
            $order['city'] = $regions[$order['city']] ?: '';
            $order['district'] = $regions[$order['district']] ?: '';
            $order['country'] = $regions[$order['country']] ?: '';
        }

        $user = Users::get(['user_id' => $order['user_id']]);
        if ($user && $user['email']) {
            $this->assign('order', $order);
            $this->assign('user', $user);
            $content = $this->fetch('email/pay_complete');
            send_email($user['email'], 'Apple Employee Choice 购买提醒', $content);
        }
    }

    /**
     * 退款
     */
    function refund(){
        $param = $this->request->param();
        $rule = [
            'order_sn' => 'require',
            'money|退款金额'=>'require|number'
        ];
        $this->validate($param,$rule);
        $order = $this->getOrder($param['order_sn']);
        $uClient = new UnionClient();
        $rt = $uClient->refund($order,$param['money']);
        $this->apiSuccess($rt);
    }


    /**
     * 查询开通银联账户是否成功（tpshop数据库需要记录用户的银行卡，然后记录开通的情况（银联回调后台，会吧开通与否写到数据库里面），但是后台回调一般会有延迟，所以需要有接口实时查询开通情况（用于刚开通，马上跳转回商户页面的情况）
     *
     * 本方法必备的参数union_open_order_id对应着银联前端回调里面的
     * 见 https://open.UnionClient.com/ajweb/product/newProApiShow?proId=2&apiId=89
     * 商户订单号	orderId 参数，客户端可以自己获取
     */
    public function queryOpen()
    {
        //记住这个orderId是银联刚才发起的开通交易的id,
        $union_open_order_id = input('union_open_order_id', null);
        if (!$union_open_order_id) {
            $this->apiError('-1', '订单号(orderId)必传');
        }
        $txnTime = date('YmdHms', time());
    
        
        $rt = UnionClient::OpenQuery($union_open_order_id, $txnTime);
        
        if(!isset($rt['code'])){
            $this->apiError('-1', '程序错误');
        }
        
        switch ($rt['code']){
            
            case 1:
                $this->api('1','用户已开通',$rt);
                break;
            case 2:
                $this->api('2','用户未开通',$rt);
                break;
            case 0:
                $this->api('-1', '查询是否开通银联支付错误:' . $rt['msg']);
                break;
            default:
                $this->api('-1', '程序错误');
                break;
        }
        
    }
    
    
    /**
     * 类似支付宝自动跳转页面
     * 只有用户第一次新增银行卡或者银行卡开通token失效，才需要走这个流程获取一个html页面的字符串，然后前段document.write写入，会自动跳转到银联页面开通。
     *
     * 用户在银联侧开通好了之后，会有个按钮点击“返回商户”这个时候返回的是指定的商户着陆页（要根据环境构建不同的url，也是麻烦了）。
     * 要孙琦自己实现，并且根据url里面的参数来判断是否开通。（同时也会有后端的回调，后端需要储存后用户的银行卡信息，以及开通情况），此外提供接口，让用户根据银行卡来判断是否真正开通银联业务（前端只靠url毕竟不够稳妥，可以根据url判断先跳转页面，但是实际支付时候，选择好银行卡后还是要确认下是否开通）
     *
     * 前台通知参数开通状态	activateStatus	M	N1	取值为1表示已开通
     */
    public function getFrontOpenFromHtml(){
    
        //这个订单号估计还是要我来生成独一无二的。就用记录申请信息的自增id
        
        //而且开通的卡用户已经开通过，就需要拒绝掉（至于有的卡是token过期了那再说）
       
        $params = $this->request->param();
        $rule = [
            'order_sn|订单号'=>'require',
            'accNo|银行卡号'=>'require',
            'phoneNo|手机号'=>'require',
            'certifId|证件号'=>'require',
            'customerNm|名字'=>'require',
            'certifTp|证件类型'=>'require|between:01'
        ];
        
        $this->validate($params,$rule);
        
        if (!isEmptyObject($params['order_sn'])) {
            $this->apiError('-1', '订单号(order_sn)必传');
        }
        
        $txnTime = date('YmdHms', time());
        
        $rt = UnionClient::getFrontOpenFromHtml($params['order_sn'],$txnTime,$params['accNo'],$params['phoneNo'],$params['certifId'],$params['customerNm']);
    
        if(!isset($rt['code'])){
            $this->apiError('-1', '程序错误');
        }
    
        switch ($rt['code']){
        
            case 1:
                $this->api('1','获取开通银联页面参数成功',$rt['data']);
                break;
            case 0:
                $this->api('-1', '获取开通银联页面参数失败:' . $rt['msg']);
                break;
            default:
                $this->api('-1', '程序错误');
                break;
        }
        
        
        
    }
    
    
    /**
     * 银联支付
     * 获取所有银行列表
     */
    public function allBank()
    {
        
        //SELLER_ID常量在api里面，必须是要求登录的才可以使用
//        $seller = Db::name('seller')->where('seller_id', SELLER_ID)->find();
//
//        if($seller&&$seller['union']){
//
//        }else{
//            $this->apiError(-1,'该经销商暂未设置银联分期');
//        }
        
        //还是乖乖建表吧
        $payChannelList = config('union.bank');
        $this->apiSuccess($payChannelList);
    }
    
    /**
     * 测试银联
     */
    public function test()
    {
//        UnionClient::ApplyToken();
//        UnionClient::DeleteToken();
    }
    
    
    /**
     * 处理某张卡开通银联支付的回调
     */
    public function callFrontOpen(){
        
        //1.确认是哪张卡,可以用独一无二的序号来做编码  商户订单号
        
        //2.根据回调的结果，设置该卡是否开通交易（可以做成类似申请的，只有这里通过之后才写到用户的银行卡的表里面）。

        
        //3.同时保存其他信息，比如卡和token绑定，还有银行卡号、开户行id（会有个表是开户行的，记录了名称和图标，用银联的来），绑定的手机号也需要（需要发短信的）。后续支付会用到token，而不是用到卡号
        
        
    }
    
}