<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date Time: 2018/8/7 11:32
 * Author: Wanzhou Chen
 * Email: 295124540@qq.com
 */

namespace app\api\controller\v1;


use app\admin\model\Users;
use app\api\controller\Base;
use app\common\model\Seller;
use app\common\model\Store as StoreModel;
use OAuth2\GrantType\AuthorizationCode;
use OAuth2\GrantType\ClientCredentials;
use OAuth2\GrantType\RefreshToken;
use OAuth2\Request;
use OAuth2\Response;
use OAuth2\Server;
use OAuth2\Storage\Pdo;
use think\Config;
use think\Db;
use think\Log;
use utils\Aes;

class Oauth2 extends Base
{
    protected $server;

    function _initialize()
    {

        parent::_initialize();
        $config = Config::get('database');
        $dsn      = 'mysql:dbname='.$config['database'].';host='.$config['hostname'];
        $username = $config['username'];
        $password = $config['password'];
        $prefix   = $config['prefix'];

        // error reporting (this is a demo, after all!)
        ini_set('display_errors',1);
        error_reporting(E_ALL);

        // Autoloading (composer is preferred, but for this example let's just do this)
//        require_once('oauth2-server-php/src/OAuth2/Autoloader.php');
//        OAuth2\Autoloader::register();

        // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"

        $oauth2Config = array(
            'client_table' => $prefix.'oauth_clients',
            'access_token_table' => $prefix.'oauth_access_tokens',
            'refresh_token_table' => $prefix.'oauth_refresh_tokens',
            'code_table' => $prefix.'oauth_authorization_codes',
            'user_table' => $prefix.'oauth_users',
            'jwt_table'  => $prefix.'oauth_jwt',
            'jti_table'  => $prefix.'oauth_jti',
            'scope_table'  => $prefix.'oauth_scopes',
            'public_key_table'  => $prefix.'oauth_public_keys',
        );
        $storage = new Pdo([ 'dsn' => $dsn, 'username' => $username, 'password' => $password],$oauth2Config);

        // Pass a storage object or array of storage objects to the OAuth2 server class
        $this->server = new Server($storage);

        // Add the "Client Credentials" grant type (it is the simplest of the grant types)
        $this->server->addGrantType(new ClientCredentials($storage));

        // Add the "Authorization Code" grant type (this is where the oauth magic happens)
        $this->server->addGrantType(new AuthorizationCode($storage));

        // 刷新token类
        // create a storage object to hold refresh tokens
        $storage = new Pdo(array('dsn' => 'sqlite:refreshtokens.sqlite'));

        // create the grant type
        $grantType = new RefreshToken($storage);

        // add the grant type to your OAuth server
        $this->server->addGrantType($grantType);
    }

    /**
     * 授权
     */
    function authorize(){

        if(isset($_GET['client_id'])&&isset($_GET['redirect_uri'])&&$_GET['client_id']&&$_GET['redirect_uri']){
           Db::name( 'oauth_clients')
                ->where(['client_id'=>$_GET['client_id']])
                ->update(['redirect_uri'=>$_GET['redirect_uri']]);
        }

        $request = Request::createFromGlobals();
        $response = new Response();

        // validate the authorize request
        if (!$this->server->validateAuthorizeRequest($request, $response)) {
            $response->send();
            die;
        }
        // display an authorization form 【跳转自己前端授权界面】
        if (empty($_POST)) {
            header('Location: '.getWebHost().'/auth?url='.urlencode($this->request->domain().$this->request->url()),TRUE,301);
            exit;
        }

        // print the authorization code if the user has authorized your client
        $is_authorized = $this->user?true:false;
        $this->server->handleAuthorizeRequest($request, $response, $is_authorized,$this->user_id);
        if ($is_authorized) {
            // this is only here so that you get to see your code in the cURL request. Otherwise, we'd redirect back to the client
            $code = substr($response->getHttpHeader('Location'), strpos($response->getHttpHeader('Location'), 'code=')+5, 40);
            //$this->apiSuccess(['app_id'=>$_GET['client_id'],'auth_code'=>$code]);
        }
        $response->send();
    }

    /**
     * 获取令牌
     */
    function accessToken(){
        $this->server->handleTokenRequest(Request::createFromGlobals())->send();
    }

    /**
     * 刷新令牌
     */
    function refreshToken(){
        $this->server->handleTokenRequest(Request::createFromGlobals())->send();
    }

    /**
     * 开放平台open_id
     */
    function getOpenId(){

        // Handle a request to a resource and authenticate the access token
//        if (!$this->server->verifyResourceRequest(Request::createFromGlobals())) {
//            $this->server->getResponse()->send();
//            die;
//        }
        $access = $this->server->getAccessTokenData(Request::createFromGlobals());
        $str = $access['client_id'].'_'.$access['user_id'];
        $access['open_id'] = Aes::encrypt($str);
        Log::write('加密前的用户信息：'.$str,'recycle');
        unset($access['user_id']);
        Log::write($access,'recycle');
        $this->apiSuccess($access);
    }

    /**
     * 获取用户信息
     */
    function getUserInfo(){

        // Handle a request to a resource and authenticate the access token
        if (!$this->server->verifyResourceRequest(Request::createFromGlobals())) {
            $this->server->getResponse()->send();
            die;
        }
        $access = $this->server->getAccessTokenData(Request::createFromGlobals());
//        $user = Users::get(['user_id' => $access['user_id']]);
        $store = StoreModel::field('s.*')
            ->alias('s')
            ->join('UserStore us','s.store_id=us.store_id')
            ->where(['us.user_id'=>$access['user_id']])
            ->find();
        $seller = Seller::field('*')->where('seller_id',$store['seller_id'])->find();
        $data = [
//            'user_email'=>$user['email'],
//            'user_mobile'=>$user['mobile'],
//            'user_nick_name'=>$user['nickname'],
//            'user_real_name'=>$user['realname'],
//            'store_name'=>$store['store_name'],
//            'store_logo'=>$store['store_logo'],
//            'store_des'=>$store['seo_description'],
//            'store_state'=>$store['store_state'],
            'seller_name'=>$seller['seller_name'],
//            'seller_contact'=>$seller['seller_contact'],
        ];
        $this->apiSuccess($data);
    }
}