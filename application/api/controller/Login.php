<?php

namespace app\api\controller;


use app\common\model\Users;

class Login extends Base
{
    protected $webHost;
    protected $host;
    protected $redirectUri;
    protected $clientId;
    protected $clientSecret;


    public function _initialize()
    {
        parent::_initialize();

        $this->webHost = getWebHost();
        // 判断正式环境，还是测试环境
        if (isTest()) {
            $config = config('lilly.test');
        } else {
            $config = config('lilly.production');
        }

        $this->host = $config['host'];
        $this->redirectUri = $config['redirect_uri'];
        $this->clientId = $config['client_id'];
        $this->clientSecret = $config['client_secret'];

    }

    public function index()
    {
        $authUrl = $this->host.'/as/authorization.oauth2?client_id='.$this->clientId.'&scope=openid profile email&redirect_uri='.urlencode($this->redirectUri).'&calllback=&response_type=code&nonce=&scope=openid profile email';
        if (!(isset($_GET['code']) && $_GET['code'])) {
            $this->redirect($authUrl);
        }

        $token = $this->getToken($_GET['code']);
        if(!$token){
            $token = $this->getToken($_GET['code']);
        }
        
        if(!$token){
            //如果不能获取请跳转入口页面地址再次登录
            $this->redirect($authUrl);
        }

        $userInfo = $this->getUserInfo($token);
        
        if (!isset($userInfo['email'])) {
            $userInfo = $this->getUserInfo($token);
        }
        if (!isset($userInfo['email'])) {
            //如果不能获取请跳转入口页面地址再次登录
            $this->redirect($authUrl);
        }

        $info = Users::getUserByEmail($userInfo['email']);
        $user = Users::login($info['user']);

        if($user){
            //登录成功跳转我们的首页
            $this->redirect($this->webHost.'/login?access-token='.$user['token']);
        }else{
            //如果不能获取请跳转入口页面地址再次登录
            $this->redirect($authUrl);
        }
    }

    /**
     *  获取token
     */
    private function getToken($code)
    {
        // 获取token
        $url = $this->host.'/as/token.oauth2';
        $headers = [
            "Content-Type:application/x-www-form-urlencoded",
            "Authorization:Basic " . base64_encode($this->clientId.':'.$this->clientSecret),
        ];
        $postfields = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $this->redirectUri,
        ];
        $rt = httpRequest($url, $method = "POST", $postfields, $headers);
        
        $data = json_decode($rt, true);

        if (isset($data['access_token'])){
            return $data['access_token'];
        }else{
            return null;
        }
    }

    /**
     * 获取用户信息
     */
    private function getUserInfo($token)
    {
        // 获取用户信息
        $url = $this->host.'/idp/userinfo.openid';
        $headers = [
            'Authorization:Bearer ' .$token,
        ];

        $rt = httpRequest($url, $method = "POST", null, $headers);
        return json_decode($rt, true);
    }
}

?>
