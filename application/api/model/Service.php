<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/11/10
 * Time: 15:06
 */

namespace app\api\model;


use think\Model;

class Service extends Model
{
    static function getList($userId,$type=0){

        $where['sl.user_id'] = $userId;
        $time = time();
        // 0全部 1可用 2已使用 3已失效
        if($type==1){
            $where['sl.status'] = 0;// 0待使用，1已经使用，-1为不可使用（不会自动过期)
            $where['sl.use_start_time'] = ['<',$time];
            $where['sl.use_end_time'] = ['>',$time];
        }

        if($type==2){
            $where['sl.status'] = 1;// 0待使用，1已经使用，-1为不可使用（不会自动过期)
        }

        if($type==3){
            $where['sl.status'] = 0;// 0待使用，1已经使用，-1为不可使用（不会自动过期)
            $where['sl.use_end_time'] = ['<',$time];
        }

        $list =  parent::field('sl.*,title,pic_m,pic,market_price,selling_price,content_m,content')
            ->alias('s')
            ->join('ServiceList sl','s.id=sl.service_id')
            ->where($where)
            ->select();
        foreach($list as $service){
            $service['content'] =  htmlspecialchars_decode($service['content']);
            $service['content_m'] =  htmlspecialchars_decode($service['content_m']);
            $service['status'] = $type?$type:self::getStatus($service);
        }
        return $list;
    }

    /**
     * 判断状态
     */
    static function getStatus($service)
    {
        $time = time();
        if($service['status'] == 0 && $service['use_start_time'] < $time && $service['use_end_time'] > $time){
            return 1;// 可用
        }

        if( $service['status'] == 1){
            return 2;// 已用
        }

        if($service['status']==0 && $service['use_end_time'] < $time){
           return 3;// 已过期
        }

        return 3;// 已过期
    }
}