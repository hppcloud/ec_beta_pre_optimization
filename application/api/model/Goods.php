<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/11/17
 * Time: 17:01
 */

namespace app\api\model;

use app\common\model\Goods as GoodsModel;
use app\common\logic\GoodsLogic;
use app\common\model\GoodsCategory;
use app\common\model\OrderGoods;
use app\common\model\SpecGoodsPrice;

class Goods extends GoodsModel
{
    /**
     * 获取销售价格( 折扣限制数量，在指定时间内如果超过限制数量，就按原价卖)
     */
    static function getSellPrice($goods,$specKey=null,$user_id){

        $goodsPrice = $goods['shop_price'] ? $goods['shop_price']:0;

        // 不为单品
        $attrPrice = 0;
        if($specKey){
            // 获取属性组合信息
            $specPrice = SpecGoodsPrice::where(['goods_id' => $goods['goods_id'],'key' =>$specKey])->find();
            if (!$specPrice) {
                api(-1, '该spec_key='.$specKey.'的商品属性组合不存在！');
            }
            $attrPrice = $specPrice['price']?$specPrice['price']:0;
        }

        // 商品折扣
        $logic = new GoodsLogic();
        $rate = $logic->getRate($goods['goods_id'],$user_id);

        // 最终商品价格 = （商品基础价格 + 加上属性价格) * 折扣
        $finalPrice = ($goodsPrice + $attrPrice)  * $rate;
        return round($finalPrice);

    }

    /**
     * 商品秒杀折扣限购（先商品，再商品分类）
     */
    static function limitBuy($goods,$userId){
        $data = [
            'is_limit'=>false,      // 是否有限制
            'limit_way'=>null,
            'limit_num'=>null,      // 限制购买个数
            'discount'=>null,       // 秒杀折扣
            'can_buy'=>false,       // 是否可以购买
            'already_buy_num'=>0,    // 已经购买个数
            'can_buy_num'=>0,        // 可以购买个数
        ];
        $time = time();

        // ---------- 先判断【总后台商品分类】秒杀限购折扣 ------------------
        $cate = GoodsCategory::get($goods['cat_id3']);
        if($cate['discount']>0&&$cate['discount_limit']>0&&$cate['discount_start']<=$time&&$cate['discount_end']>=$time){
            $data['is_limit'] = true;
            $data['limit_way'] = 'by_admin_goods_category';
            $data['limit_num'] = $cate['discount_limit'];
            $data['discount'] = $cate['discount'];
            $data['can_buy'] = true;

            // 获取该分类下的商品id
            $goodIdArr = GoodsModel::where(['cat_id3'=>$goods['cat_id3']])->column('goods_id');

            // 从订单中洗出，该用户在秒杀折扣期间已经购买该分类的商品多少个
            $where = [
                'o.user_id'=>$userId,
                'o.order_status'=>['<>',3],// 排除取消的订单
                'o.create_time'=>['>=',$cate['discount_start']],
                'o.create_time '=>['<=',$cate['discount_end']]
            ];
            $goodsNum = OrderGoods::alias('og')
                ->join('Order o','o.order_id=og.order_id')
                ->where($where)
                ->whereIn('og.goods_id',$goodIdArr)
                ->sum('goods_num');

            $data['already_buy_num'] = $goodsNum;
            $data['can_buy_num'] = $cate['discount_limit'] - $goodsNum;
            if($goodsNum>=$cate['discount_limit']){
                $data['can_buy'] = false;
                $data['can_buy_num'] = 0;
            }
            return $data;
        }

        // ---------- 再判断【商品】秒杀限购折扣 ------------------
        if($goods['discount']>0&&$goods['discount_limit']>0&&$goods['discount_start']<=$time&&$goods['discount_end']>=$time){
            $data['is_limit'] = true;
            $data['limit_way'] = 'by_goods';
            $data['limit_num'] = $goods['discount_limit'];
            $data['discount'] = $goods['discount'];
            $data['can_buy'] = true;

            // 从订单中洗出，该用户在秒杀折扣期间已经购买该商品多少个
            $where = [
                'o.user_id'=>$userId,
                'o.order_status'=>['<>',3],// 排除取消的订单
                'og.goods_id'=>$goods['goods_id'],
                'o.create_time'=>['>=',$goods['discount_start']],
                'o.create_time '=>['<=',$goods['discount_end']]
            ];
            $goodsNum = OrderGoods::alias('og')
                ->join('Order o','o.order_id=og.order_id')
                ->where($where)
                ->sum('goods_num');
            $data['already_buy_num'] = $goodsNum;
            $data['can_buy_num'] = $goods['discount_limit'] - $goodsNum;
            if($goodsNum>=$goods['discount_limit']){
                $data['can_buy'] = false;
                $data['can_buy_num'] = 0;
            }
            return $data;
        }

        // ---------- 最后判断【经销商后台商品分类】秒杀限购折扣 ------------------
        $cate = SellerDiscount::get(['store_id'=>$goods['store_id'],'catid'=>$goods['cat_id3']]);
        if($cate['discount']>0&&$cate['discount_limit']>0&&$cate['discount_start']<=$time&&$cate['discount_end']>=$time){
            $data['is_limit'] = true;
            $data['limit_way'] = 'by_seller_goods_category';
            $data['limit_num'] = $cate['discount_limit'];
            $data['discount'] = $cate['discount'];
            $data['can_buy'] = true;

            // 获取该分类下的商品id
            $goodIdArr = GoodsModel::where(['cat_id3'=>$goods['cat_id3']])->column('goods_id');

            // 从订单中洗出，该用户在秒杀折扣期间已经购买该分类的商品多少个
            $where = [
                'o.user_id'=>$userId,
                'o.order_status'=>['<>',3],// 排除取消的订单
                'o.create_time'=>['>=',$cate['discount_start']],
                'o.create_time '=>['<=',$cate['discount_end']]
            ];
            $goodsNum = OrderGoods::alias('og')
                ->join('Order o','o.order_id=og.order_id')
                ->where($where)
                ->whereIn('og.goods_id',$goodIdArr)
                ->sum('goods_num');

            $data['already_buy_num'] = $goodsNum;
            $data['can_buy_num'] = $cate['discount_limit'] - $goodsNum;
            if($goodsNum>=$cate['discount_limit']){
                $data['can_buy'] = false;
                $data['can_buy_num'] = 0;
            }
        }

        return $data;
    }

}