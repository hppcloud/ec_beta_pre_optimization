<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/11/10
 * Time: 15:07
 */

namespace app\api\model;


use think\Model;

class ServiceList extends Model
{
    /**
     * 下发服务包
     */
    static function send($userId,$sellerId){
        // 获取有效的服务
        $time = time();
        $where = [
            'enabled'=>1,
            'is_deleted'=>0,
            'send_start_time'=>['<',$time],
            'send_end_time'=>['>',$time]
        ];
        $list = Service::all($where);
        foreach ($list as $service){

            // 是否关联本经销商
            if(!in_array($sellerId,explode(',',$service['seller_ids']))){
                continue;
            }

            // 是否有发
            $serviceList = self::get(['user_id'=>$userId,'service_id'=>$service['id']]);
            if($serviceList){
                continue;
            }

            $serviceList = self::create([
                'user_id'=>$userId,
                'service_id'=>$service['id'],
                'use_start_time'=>$service['use_start_time'],
                'use_end_time'=>$service['use_end_time'],
                'send_time'=>$time
            ]);
            if($serviceList){
                $service['send_num'] = $service['send_num']+1;
                $service->save();
            }
        }
    }
}