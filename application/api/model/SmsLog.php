<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/12/3
 * Time: 20:58
 */

namespace app\api\model;


use think\Model;

class SmsLog extends Model
{
    /**
     * @param $mobile
     * @param $code
     * @return false|int
     */
    static function verify($mobile,$code){
        $smsLog = self::where(['mobile'=>$mobile])->order('add_time','desc')->find();
        if ($code!=$smsLog['code']||$smsLog['status']!=1) {
            api(-1, '短信验证码不正确！');
        }
        $smsLog['status'] = 2;
        return $smsLog->save();
    }
}