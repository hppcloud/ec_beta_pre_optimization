<?php
/**
 * 评论咨询投诉管理
 */

namespace app\seller\controller;

use app\admin\model\Users;
use app\api\controller\v1\FenQi;
use app\api\controller\v1\Pay;
use app\common\model\CouponList;
use app\common\model\Order;
use app\common\model\OrderGoods;
use app\seller\logic\ApiLogic;
use think\AjaxPage;
use think\Db;
use think\Exception;
use think\Page;

use app\seller\logic\OrderLogic;


class Service extends Base
{

    public function ask_list()
    {
        checkIsBack();
        return $this->fetch();
    }

    public function ajax_ask_list()
    {
        $model = M('goods_consult');
        $username = I('nickname', '', 'trim');
        $content = I('content', '', 'trim');
        $where = array('parent_id' => 0, 'store_id' => STORE_ID);
        if ($username) {
            $where['username'] = $username;
        }
        if ($content) {
            $where['content'] = ['like', '%' . $content . '%'];
        }
        $count = $model->where($where)->count();
        $Page = new AjaxPage($count, 16);
        //是否从缓存中读取Page
        if (session('is_back') == 1) {
            $Page = getPageFromCache();
            //重置获取条件
            delIsBack();
        }

        $comment_list = $model->where($where)->order('add_time DESC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        if (!empty($comment_list)) {
            $goods_id_arr = get_arr_column($comment_list, 'goods_id');
            $goods_list = M('Goods')->where("goods_id", "in", implode(',', $goods_id_arr))->getField("goods_id,goods_name");
        }
        $consult_type = array(0 => '默认咨询', 1 => '商品咨询', 2 => '支付咨询', 3 => '配送', 4 => '售后');
        cachePage($Page);
        $show = $Page->show();
        $this->assign('consult_type', $consult_type);
        $this->assign('goods_list', $goods_list);
        $this->assign('comment_list', $comment_list);
        $this->assign('page', $show);// 赋值分页输出
        return $this->fetch();
    }

    public function ask_handle()
    {
        $type = I('post.type');
        $selected_id = I('post.selected/a');
        if (!in_array($type, array('del', 'show', 'hide')) || !$selected_id) {
            $this->error('异常操作');
        }
        $row = false;
        $selected_id = implode(',', $selected_id);
        if ($type == 'del') {
            //删除咨询
            $where = array(
                'id|parent_id' => ['IN', $selected_id],
                'store_id' => STORE_ID
            );
            $row = M('goods_consult')->where($where)->delete();
        }
        if ($type == 'show') {
            $row = M('goods_consult')->where(['id' => ['IN', $selected_id], 'store_id' => STORE_ID])->save(array('is_show' => 1));
        }
        if ($type == 'hide') {
            $row = M('goods_consult')->where(['id' => ['IN', $selected_id], 'store_id' => STORE_ID])->save(array('is_show' => 0));
        }
        if ($row !== false) {
            $this->success('操作完成');
        } else {
            $this->error('操作失败');
        }

    }

    /**
     * 换货维修申请列表
     */
    public function return_list(){
    	//搜索条件
    	$where['seller_id'] = SELLER_ID;
    	$where['type'] = array('gt',1);
    	$status = I('status');
    	if($status || $status == '0'){
    		$where['status'] = $status;
    	}
    	$order_sn = I('order_sn');
    	if($order_sn) $where['order_sn'] = $order_sn;
        $begin =  $this->begin;
        $end   =  $this->end;
    	if($begin && $end){
    		$where['addtime'] = array('between',"$begin,$end");
    	}
    	$count = M('return_goods')->where($where)->count();
    	$Page  = new Page($count,20);
    	$show = $Page->show();
    	$list = M('return_goods')->where($where)->order("id desc")->limit("{$Page->firstRow},{$Page->listRows}")->select();
    	$goods_id_arr = get_arr_column($list, 'goods_id');
    	if(!empty($goods_id_arr))
    		$goods_list = M('goods')->where("goods_id in (".implode(',', $goods_id_arr).")")->getField('goods_id,goods_name');
    	$this->assign('goods_list',$goods_list);
    	$state = C('RETURN_STATUS');
    	$this->assign('list', $list);
    	$this->assign('state',$state);
    	$this->assign('page',$show);// 赋值分页输出
    	return $this->fetch();
    }
    
    
    /**
     * 退款申请列表
     */
    public function refund_list(){
    
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
//    	$where['seller_id'] = SELLER_ID;
       
        if(I('store_id')){
            $where['store_id'] = I('store_id');
        }else{
            $where['store_id'] = array('in',STORES);
        }
        
    	$where['type'] = array('lt',2);
    	$status = I('status');
    	if($status || $status=='0'){
    		$where['status'] = $status;
    	}
    	$order_sn = I('order_sn');
    	if($order_sn) $where['order_sn'] = $order_sn;
        $begin =  $this->begin;
        $end   =  $this->end;
    	if($begin && $end){
    		$where['addtime'] = array('between',"$begin,$end");
    	}
    	$count = M('return_goods')->where($where)->count();
    	$Page  = new Page($count,20);
    	$show = $Page->show();
    	$list = M('return_goods')->where($where)->order("id desc")->limit("{$Page->firstRow},{$Page->listRows}")->select();
    	if(!empty($list)){
    		$goods_id_arr = get_arr_column($list, 'goods_id');
    		$user_id_arr =  get_arr_column($list, 'user_id');
    		$goods_list = M('goods')->where("goods_id in (".implode(',', $goods_id_arr).")")->getField('goods_id,goods_name');
    		$user_list = M('users')->where("user_id in (".implode(',', $user_id_arr).")")->getField('user_id,nickname');
    		$this->assign('goods_list',$goods_list);
    		$this->assign('user_list',$user_list);
    	}
    	$state = C('RETURN_STATUS');
    	$this->assign('list', $list);
    	$this->assign('state',$state);
    	$this->assign('page',$show);
    	return $this->fetch();
    }
    
    /**
     * 删除某个退换货申请
     */
    public function return_del(){
    	$id = I('get.id/d');
    	M('return_goods')->where("id = $id and store_id = ".STORE_ID)->delete();
    	$this->success('成功删除!');
    }
    
    /**
     * 换货操作
     */
    public function return_info()
    {
    	$id = I('id/d');
        $return_goods = M('return_goods')->where(array('id'=>$id,'seller_id'=>SELLER_ID))->find();
        $select_year = getTabByOrderId($return_goods['order_id']);
        empty($return_goods) && $this->error("参数有误");
    	if(IS_POST){
            $data = I('post.');

    		if($data['status'] == 1 || $data['status'] == -1){
    			$data['checktime'] = time();//审核换货申请
       
    		}else{
    			$data['status'] = 4;//处理发货
    			$data['seller_delivery']['express_time'] = date('Y-m-d H:i:s');
    			$data['seller_delivery'] = serialize($data['seller_delivery']);
    			M('order_goods'.$select_year)->where('rec_id', $return_goods['rec_id'])->save(array('is_send'=>2));
    		}
    		M('return_goods')->where(array('id'=>$data['id'],'seller_id'=>SELLER_ID))->save($data);
    		$this->success('操作成功!',U('Service/return_list'));exit;
    	}
    	if($return_goods['imgs']) $return_goods['imgs'] = explode(',', $return_goods['imgs']);
    	if($return_goods['delivery']) $return_goods['delivery'] = unserialize($return_goods['delivery']);
    	if($return_goods['seller_delivery']) $return_goods['seller_delivery'] = unserialize($return_goods['seller_delivery']);
    	$user = get_user_info($return_goods['user_id']);
    	$order_goods = M('order_goods'.$select_year)->where("order_id ={$return_goods['order_id']} and goods_id = {$return_goods['goods_id']} and spec_key = '{$return_goods['spec_key']}'")->find();
    	$this->assign('user',$user);
    	$order = M('order'.$select_year)->where(array('order_id'=>$return_goods['order_id']))->find();
    	$this->assign('order',$order);//退货订单信息
    	$this->assign('order_goods',$order_goods);//退货订单商品
    	$this->assign('return_goods',$return_goods);// 退换货申请信息
    	$this->assign('state',C('RETURN_STATUS'));
    	return $this->fetch();
    }

    
    
    /**
     * 退货操作
     */
    public function refund_info(){



        $id = I('id');
        $return_goods = M('return_goods')->where(array('id'=>$id,'store_id'=>array('in',STORES)))->find();

        $orderInfo = Db::name('order')->where('order_id',$return_goods['order_id'])
            ->find();
        $orderLogic = new OrderLogic();


        $select_year = getTabByOrderId($return_goods['order_id']);
        empty($return_goods) && $this->error("参数有误");
    	if(IS_POST||(IS_GET&&I('action',null))){
            $params = $this->request->param();
            $data = [];
            $data['id'] = $params['id'];
            $data['checktime'] = time();
            $data['remark'] = $params['remark'];
            if($params['status'] == 1){
                //0仅退款 1退货退款  2换货 3维修,只要大于0就是
                if($return_goods['type']==0){
                    //更新remark
                    M('return_goods')->where(array('id'=>$data['id'],'store_id'=>array('in',STORES)))->update(['remark'=>$params['remark']]);


                    $action = '确认退款';
                    $orderLogic->orderActionLog($orderInfo, $action,'确认退款:经销商同意退款', SELLER_ID, 1);
                    //0是直接退款了
                    $this->refundMoney($return_goods);
                }else{
                    //未发货商品会直接走  纯退款的逻辑
//                    if($return_goods['is_receive'] == 0) $data['status'] = 3;//未发货商品无需确认收货
                }
            }else{

                if($params['status'] == -1){
                    $action = '拒绝退款';

                    $order_goods = OrderGoods::get(['rec_id'=>$return_goods['rec_id']]);
                    $order_goods->refuseReturn($return_goods['type']);
                    $orderLogic->orderActionLog($orderInfo, $action,'拒绝退款:经销商拒绝退款', SELLER_ID, 1);
                }

            }


            $data['status'] = $params['status'];
    		if($data['status']==3){
                $data['receivetime']=time();

                $action = '退款确认收货';
                $orderLogic->orderActionLog($orderInfo, $action,'经销商确认收货（退货）', SELLER_ID, 1);
            }

            if($data['status']==4){


                $action = '退货检验';
                $orderLogic->orderActionLog($orderInfo, $action,'退货检验通过', SELLER_ID, 1);
            }

            if($data['status']==5){


                $action = '退货检验';
                $orderLogic->orderActionLog($orderInfo, $action,'退货检验不通过', SELLER_ID, 1);
            }
    		
//    		if($data['refund_money'] != $return_goods['refund_money']){
//    			$data['gap'] = $return_goods['refund_money'] - $data['refund_money'];//退款差额
//    		}

            $rt = M('return_goods')->where(array('id' => $data['id'], 'store_id' => array('in', STORES)))->save($data);
            //修改订单状态
            if ($rt !== false) {
                $order = Db::name('order')->where('order_id', $return_goods['order_id'])->find();
                //退货退款，经销商确认退款请求，后提醒客户发货
                if($data['status']==1&&$return_goods['type']==1){

                    $action = '同意退货';
                    $orderLogic->orderActionLog($orderInfo, $action,'退款:经销商同意退货', SELLER_ID, 1);

                    $this->send_by_seller_confirm($order,$return_goods);
                }
                //没有发货的才修改成1
                if ($order['shipping_status'] == 0 && $order['order_status'] == 5) {
//                    Db::name('order')->where('order_id', $return_goods['order_id'])->update(['order_status' => 1]);
                }

            }
            $this->success('操作成功!',U('Service/refund_list'));
            exit;
    	}
    	
    	if($return_goods['imgs']) $return_goods['imgs'] = explode(',', $return_goods['imgs']);
        if($return_goods['delivery']) {
    		$return_goods['delivery'] = unserialize($return_goods['delivery']);
    	}
    	$user = get_user_info($return_goods['user_id']);
    	$order_goods = M('order_goods'.$select_year)->where(['rec_id' =>$return_goods['rec_id']])->find();
        $order = Db::name('order')->where(['order_id'=>$return_goods['order_id']])->find();
        if($order['shipping_id']==3){
            $order['shipping_info']= json_decode($order['shipping_info'],true);
            $order['ziti_info']= json_decode($order['ziti_info'],true);
        }
//        $refund_money = $return_goods['refund_money'];
//        //不管优惠券是否退还,如果退款后不满足优惠券的使用情况。都不对
//        //&&$order['coupon_is_refund']!==1
//        //单纯货款的钱，退后剩下的钱不够的
//        if($order['paid_money']+$order['coupon_price']-$refund_money<$order['coupon_condition']){
//            $refund_money = $return_goods['refund_money']-$order['coupon_price'];//更改退款金额
//        }


    
//        $this->assign('refund_money',sprintf("%.2f",substr(sprintf("%.3f", $refund_money), 0, -2)));
//        if($order['paid_money']+$order['coupon_price']-$return_goods['refund_money']<$order['coupon_condition']){
//            $this->assign('iscoupon',1);
//
//        }else{
//            $this->assign('iscoupon',0);
//        }
     
        
    	$this->assign('user',$user);
    	$this->assign('order',$order);//退货订单信息
    	$this->assign('order_goods',$order_goods);//退货订单商品
    	$this->assign('return_goods',$return_goods);// 退换货申请信息
    	$this->assign('state',C('RETURN_STATUS'));
    	
        $order = Db::name('order')->where(['order_id'=>$return_goods['order_id'],'seller_id'=>SELLER_ID])->find();//Db::name('Order')->where('order_id',$return_goods['order_id'])->where('seller_id',SELLER_ID)->find();
        $order_goods_list = Db::name('OrderGoods')->where('order_id',$return_goods['order_id'])->select();
        //获取除了该退款记录外，该订单是否还有其他的退款记录。 所有的退款金额总和
        // 0待审核 1通过 2已发货 3已收货待检验 4检测货品完成 5检测货品不成功 6退款成功 7退款失败
        $return_order_goods_list = Db::name('ReturnGoods')->where('order_id',$return_goods['order_id'])->where('id','neq',$return_goods['id'])->where('status','in',[0,1,2,3,4,6])->select();
    
        $return_order_goods_total_money = Db::name('ReturnGoods')->where('order_id',$return_goods['order_id'])->where('id','neq',$return_goods['id'])->where('status','in',[0,1,2,3,4,6])->sum('refund_money');
    
        //是否还存在优惠券么有退
        $isHasNoRetunCoupon = Db::name('CouponList')->where('order_id',$return_goods['order_id'])->where('status',1)->find();
    
        if($order['pay_status']<1){
            //$this->error('订单支付状态异常');
        }
        //退还金额
        $refund_money = $return_goods['refund_money'];
        //退回优惠券
        //商品商品的总价，没有去掉优惠券之前的
        $order_goods_amount = $order['paid_money']+$order['coupon_price'];
        //如果存在未操作的优惠券
        if($isHasNoRetunCoupon){
            $coupon_list_id = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->value('id');
            //如果是多件商品的订单
            if(count($order_goods_list)>1){
            
                //1.不管多少个，把所有的退款的金额加起来+本次申请退款的金额。  如果大于订单的减免金额  则意味着优惠券要退了。
                //order表的coupon_price代表优惠的金额。
                //2.如果是最后一个商品退了，那么意味着订单需要全部退款
            
                //(支付价格+优惠券见面金额-已经有的退款订单涉及金额-这一笔的退款金额)<优惠券的减免条件
//                trace('hhhhhhhhhhhhhhhh'.($order_goods_amount-($return_order_goods_total_money+$return_goods['refund_money'])).'<br/>');
                //3.还有一种情况，是优惠券的门槛很低（甚至为0门槛的），但是面额却很大。所以还要比对面额

                if(($order_goods_amount-($return_order_goods_total_money+$return_goods['refund_money']))<$order['coupon_condition']||($order_goods_amount-($return_order_goods_total_money+$return_goods['refund_money']))<$order['coupon_price']||count($return_order_goods_list)+1==count($order_goods_list)){
                
//                $order['order_amount'] += $order['coupon_price'];
                    $orderCouponPrice = $order['coupon_price'];
//                $order['coupon_price'] = 0;
//                $changeOrderInfo =  Db::name('order')->where(['order_id'=>$return_goods['order_id'],'seller_id'=>SELLER_ID])->update(['coupon_price'=>0,'order_amount'=>$order['order_amount']]);
//                if($changeOrderInfo!==false){
//
//                }else{
//                    $this->error('退优惠券后修改订单金额失败');
//                }
                
                    //所以这一次应该退款是用 退款金额-优惠券的金额或者为0
                
                    if($return_goods['refund_money']>$orderCouponPrice){
                        $refund_money = $return_goods['refund_money']-$orderCouponPrice;//更改退款金额
                    }else{
                        $refund_money = 0;//最多退0元
                    }
                }
            }else{

                $refund_money = $order['paid_money'];
            }
        }
        $real_refund_money = sprintf("%.2f",substr(sprintf("%.3f", $refund_money), 0, -2));
//        $refund_money = sprintf("%.2f",substr(sprintf("%.3f", $refund_money), 0, -2));
        $this->assign('real_refund_money',$real_refund_money);
    	return $this->fetch();
    }


    //订单状态：0待确认，1已确认，2已收货，3已取消，4已完成，5退货退款中，6退货退款完成，7部分退货退款完成，8退货退款失败
    //商品退款状态：-2用户取消 -1不同意 0待审核 1通过 2已发货 3待退款 4检测货品完成 5检测货品不成功 6退款成功 7退款失败
    protected function refundMoney($return_goods){
    
        // 启动事务
        Db::startTrans();
        try{

    
            $order = Db::name('order')->where(['order_id'=>$return_goods['order_id'],'seller_id'=>SELLER_ID])->find();//Db::name('Order')->where('order_id',$return_goods['order_id'])->where('seller_id',SELLER_ID)->find();


            if($order['pay_code']=='wx'&&((time()-$order['add_time'])<360)){
               $this->error('微信支付退款五分钟后才可以操作');
            }

    
            $order_goods_list = Db::name('OrderGoods')->where('order_id',$return_goods['order_id'])->select();
            
            //获取除了该退款记录外，该订单是否还有其他的退款记录。 所有的退款金额总和
            // 0待审核 1通过 2已发货 3已收货待检验 4检测货品完成 5检测货品不成功 6退款成功 7退款失败
            $return_order_goods_list = Db::name('ReturnGoods')->where('order_id',$return_goods['order_id'])->where('id','neq',$return_goods['id'])->where('status','in',[0,1,2,3,4,6])->select();
    
            $return_order_goods_total_money = Db::name('ReturnGoods')->where('order_id',$return_goods['order_id'])->where('id','neq',$return_goods['id'])->where('status','in',[0,1,2,3,4,6])->sum('refund_money');
            

            //是否还存在优惠券么有退
            $isHasNoRetunCoupon = Db::name('CouponList')->where('order_id',$return_goods['order_id'])->where('status',1)->find();
            
            if($order['pay_status']<1){
                $this->error('订单支付状态异常');
            }
    
            if($return_goods['status']>4){
                $this->error('该订单已经处理完毕，无法二次操作');
            }
    
            //退还金额
            $refund_money = $return_goods['refund_money'];
    
    
            //退回优惠券
            //商品商品的总价，没有去掉优惠券之前的
            $order_goods_amount = $order['paid_money']+$order['coupon_price'];
            
            //如果存在未操作的优惠券
            if($isHasNoRetunCoupon){
    
                $coupon_list_id = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->value('id');
                
                //如果是多件商品的订单
                if(count($order_goods_list)>1){
        
                    //1.不管多少个，把所有的退款的金额加起来+本次申请退款的金额。  如果大于订单的减免金额  则意味着优惠券要退了。
                    //order表的coupon_price代表优惠的金额。
                    //2.如果是最后一个商品退了，那么意味着订单需要全部退款
                    
                    //(支付价格+优惠券见面金额-已经有的退款订单涉及金额-这一笔的退款金额)<优惠券的减免条件
                    if(($order_goods_amount-($return_order_goods_total_money+$return_goods['refund_money']))<$order['coupon_condition']||($order_goods_amount-($return_order_goods_total_money+$return_goods['refund_money']))<$order['coupon_price']||count($return_order_goods_list)+1==count($order_goods_list)){
    
                        //原因说细一点
                        if(count($return_order_goods_list)+1==count($order_goods_list)){
                            trace('多个商品订单,退款订单:order_sn:'.$return_goods['order_sn'].'所有商品已经退款了.'.'，所以退款时需要考虑取消优惠券coupon_list_id:'.$coupon_list_id,'refund');


                        }else{
                            trace('多个商品订单,退款订单:order_sn:'.$return_goods['order_sn'].'，原订单因为退款后总退款金额已经超过订单抵扣金额.'.$order['coupon_price'].'或者订单金额已经小于优惠券面额，所以退款时需要考虑取消优惠券coupon_list_id:'.$coupon_list_id,'refund');
                        }
                        


//                $order['order_amount'] += $order['coupon_price'];
                        $orderCouponPrice = $order['coupon_price'];
//                $order['coupon_price'] = 0;
//                $changeOrderInfo =  Db::name('order')->where(['order_id'=>$return_goods['order_id'],'seller_id'=>SELLER_ID])->update(['coupon_price'=>0,'order_amount'=>$order['order_amount']]);
//                if($changeOrderInfo!==false){
//
//                }else{
//                    $this->error('退优惠券后修改订单金额失败');
//                }
    
                        //所以这一次应该退款是用 退款金额-优惠券的金额或者为0
    
                        if($return_goods['refund_money']>$orderCouponPrice){
                            $refund_money = $return_goods['refund_money']-$orderCouponPrice;//更改退款金额
                        }else{
                            $refund_money = 0;//最多退0元
                        }
    
                        if($order['coupon_is_refund']!==1){
        
        
                            trace('用户发起退款的商品在order_goods表中的final_price价格为'.$return_goods['refund_money'],'refund');
                            trace('该订单记录的优惠券抵扣金额为'.$orderCouponPrice,'refund');
                            trace('最终退款金额为'.$refund_money,'refund');
                            trace('退款订单:order_sn:'.$return_goods['order_sn'].'，原订单因为退款后不满足优惠券满减.'.$order['coupon_condition'].'的条件，所以退款金额减少'.$orderCouponPrice,'refund');
                            
                            $coupon_list_id = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->value('id');

                            $couponInfo = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->find();
                            $rt = CouponList::update(['status'=>0,'use_time'=>0,'order_id'=>0],['order_id'=>$order['order_id'],'status'=>1]);


                            //优惠券已使用减一
                            Db::name('Coupon')->where('id',$couponInfo['cid'])->setDec('use_num',1);
                            
                            if($rt!==false){
                                Db::name('order')->where('order_id',$return_goods['order_id'])->update(['coupon_is_resert'=>$coupon_list_id]);
                            }else{
            
                                $this->error('退回优惠券发生错误');
                            }
        
                        }
                        
                    
                    }
                    

        
                }else{
    
                    trace('单个商品订单,退款订单:order_sn:'.$return_goods['order_sn'].'所有商品已经退款了，所以退款时需要考虑优惠券coupon_list_id:'.$coupon_list_id,'refund');


//                $order['order_amount'] += $order['coupon_price'];
                    $orderCouponPrice = $order['coupon_price'];
//                $order['coupon_price'] = 0;
//                $changeOrderInfo =  Db::name('order')->where(['order_id'=>$return_goods['order_id'],'seller_id'=>SELLER_ID])->update(['coupon_price'=>0,'order_amount'=>$order['order_amount']]);
//                if($changeOrderInfo!==false){
//
//                }else{
//                    $this->error('退优惠券后修改订单金额失败');
//                }
        
                    //原来5元，一个商品。优惠4元，实际支付1元  退款后的金额。
                    //就是实际支付的金额推掉就行了

                    $refund_money = $order['paid_money'];//如果是一件，而且优惠券不退，就退付款金额就行了。

        
                    if($order['coupon_is_refund']!==1){
    
                        
            
            
                        trace('用户发起退款的商品在order_goods表中的final_price价格为'.$return_goods['refund_money'],'refund');
                        trace('该订单记录的优惠券抵扣金额为'.$orderCouponPrice,'refund');
                        trace('最终退款金额为'.$refund_money,'refund');
                        trace('退款订单:order_sn:'.$return_goods['order_sn'].'，原订单因为退款后不满足优惠券满减.'.$order['coupon_condition'].'的条件，所以退款金额减少'.$orderCouponPrice,'refund');


                        $couponInfo = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->find();


                        $rt = CouponList::update(['status'=>0,'use_time'=>0,'order_id'=>0],['order_id'=>$order['order_id'],'status'=>1]);




                        //优惠券已使用减一
                        Db::name('Coupon')->where('id',$couponInfo['cid'])->setDec('use_num',1);
    
                        if($rt!==false){
                            //将该字段设置为coupon_list的id.仅仅作为标记
                            Db::name('order')->where('order_id',$return_goods['order_id'])->update(['coupon_is_resert'=>$coupon_list_id]);
                        }else{
        
                            $this->error('退回优惠券发生错误');
                        }
            
                    }else{

                    }
        
                }
                
            }
            


            
            
    
    
            $refund_money = sprintf("%.2f",substr(sprintf("%.3f", $refund_money), 0, -2));
    

            Db::name('ReturnGoods')->where('id',$return_goods['id'])->update(['real_refund_money'=>$refund_money]);
    
    

            trace('退款金额是'.$refund_money,'refund');



//            <option value="">支付方式</option>
//             <option value="ali">支付宝支付</option>
//             <option value="wx">微信支付</option>
//             <option value="kq">快钱</option>
//             <option value="HBFQ">花呗分期</option>
//             <option value="CCFQ">信用卡分期</option>
            
            if($refund_money>0){
                
                switch ($order['pay_code']){
                    case 'HBFQ':
                    case 'CCFQ':
                        $rt = FenQi::refund($order['order_sn'],$refund_money ,'订单'.$order['order_sn'].'发起退款'.date('YmdHms',time()));
                        break;
                    case 'ali':
                    case 'wx':
                    case 'kq':
                    case 'wap':
                        $rt = Pay::KQRefund($order['order_sn'],$refund_money);
                        break;
                    default:
                        $rt = ['code'=>0,'msg'=>'操作失败:支付渠道错误'];
                        break;
                }
                
            }elseif($refund_money==0){
                $rt = ['code'=>1,'msg'=>'退款金额为0'];
            }else{
                $this->error("操作失败:退款金额异常");
            }
            
    
            $returnGoodsNum =  Db::name('ReturnGoods')
                ->where(array('order_id'=>$return_goods['order_id'],'store_id'=>array('in',STORES)))
                ->count('rec_id');
            $orderGoodsNum = OrderGoods::where(['order_id'=>$return_goods['order_id']])->count('rec_id');
    
            //需要正确的返回码才行
            if($rt['code']===1){
        
                M('return_goods')->where(array('id'=>$return_goods['id'],'store_id'=>array('in',STORES)))->save(array('status'=>6));
        
                //5退货退款完成，7部分退货退款完成
                if($returnGoodsNum == $orderGoodsNum){
                    //所有的都退完了，就要标记为完成
                    Db::name('Order')->where('order_id',$order['order_id'])->update(['order_status'=>4]);
//                    $data = ['order_status'=>6,'pay_status'=>3,'part_refund_status'=>0];
                }else{
                    // $orde_status = 7; 废弃，为了不影响剩余商品发货流程，退货流程的状态值
                    // part_refund_status 部分商品退款状态:0无部分退款 1部分退款成功 2部分退款失败
//                    $data = [
//                        'order_status'=>1,// 订单状态回到确认状态
//                        'part_refund_status'=>1
//                    ];
            
                }



                //不需要更新了
//                Db::name('Order')->where('order_id',$return_goods['order_id'])->where('seller_id',SELLER_ID)->update($data);
        
        
                //退款
                $refer = $return_goods['type']>1 ? U('Service/return_list') : U('Service/refund_list');
        
                //仅退款
                if($return_goods['type']==0){
                    $this->send_by_only_money($order,$return_goods);
                }
        
                //退货退款成功退款
                if($return_goods['type']==1){
                    $this->send_by_complete($order,$return_goods);
                }
                
                
                //库存退回
                $order_goods_data = Db::name('OrderGoods')->where('rec_id',$return_goods['rec_id'])->find();


                //finish order_goods
                $order_goods = OrderGoods::get(['rec_id'=>$return_goods['rec_id']]);
                $order_goods->finishReturn($return_goods['type']);


                if($order_goods_data){
                    
                    //修改为已经退货
                    Db::name('OrderGoods')->where('rec_id',$return_goods['rec_id'])->update(['is_send'=>3]);
                    
                    //没有规格就直接改goods表里面的
                    if(isEmptyObject($order_goods_data['spec_key'])){
                        Db::name('Goods')->where(['goods_id'=>$return_goods['goods_id']])->setInc('store_count',$return_goods['goods_num']);
                    }else{
                        //有规格就修改SpecGoodsPrice表的值
                        Db::name('SpecGoodsPrice')->where(['key'=>$order_goods_data['spec_key'],'goods_id'=>$return_goods['goods_id']])->setInc('store_count',$return_goods['goods_num']);
                    }
                }
                
                
        
                Db::commit();
                $this->success('操作成功!',$refer);exit;
            }else{
                
                Db::rollback();
                if($rt['code']!=-4){
                    M('return_goods')->where(array('id'=>$return_goods['id'],'store_id'=>array('in',STORES)))->save(array('status'=>7,'receivetime'=>time()));
            
                    if($returnGoodsNum == $orderGoodsNum){
//                        $data = ['order_status'=>8];
                    }else{
                        // $orde_status = 7; 废弃，为了不影响剩余商品发货流程，退货流程的状态值
                        // part_refund_status 部分商品退款状态:0无部分退款 1部分退款成功 2部分退款失败
//                        $data = [
//                            'order_status'=>1,// 订单状态回到确认状态
//                            'part_refund_status'=>2
//                        ];
                    }
//                    Db::name('Order')->where('order_id',$return_goods['order_id'])->where('seller_id',SELLER_ID)->update($data);
                }
    
                
//            Db::name('Order')->where('order_id',$return_goods['order_id'])->where('seller_id',SELLER_ID)->update(['status'=>6]);
                $this->error("操作失败:".$rt['msg']);
            }
            
        }catch (Exception $e){
            Db::rollback();
            trace('退款操作失败return_goods_id:'.$return_goods['id'].'，errorMsg:'.$e->getMessage());
            send_email('function@qingclouds.cn','退款错误','退款操作失败return_goods_id:'.$return_goods['id'].'，errorMsg:'.$e->getMessage());
            $this->error($e->getMessage());
        }

        

        
       




    }
    
    /**
     * 退货退款订单，经销商检测货品通过退款后发送邮件
     * @param $order
     * @param $return_goods
     * @throws \think\exception\DbException
     */
    protected function send_by_complete($order,$return_goods){
        
        $users = Users::get($order['user_id']);
        
        if(!$users['email']||$users['email']==''){
            return;
        }
        
        
        
        $nickname = '';
        //按照优先级来
        if(!isEmptyObject($users['nickname'])){
            $nickname = $users['nickname'];
        }elseif ($order['shipping_code']==='ziti'){
            $ziti_info = json_decode($order['ziti_info'],true);
            if(isset($ziti_info['user_name'])&&!isEmptyObject($ziti_info['user_name'])){
                $nickname = $ziti_info['user_name'];
            }
        }elseif($order['shipping_code']==='shunfeng'&&!isEmptyObject($order['consignee'])){
            $nickname = $order['consignee'];
        }
        
        
        $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
　

<head>
    　　
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    　　
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        * {
            font-family: "SF Pro SC", "SF Pro Text", "SF Pro Icons", "PingFang SC", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
        }
        .top-txt {
            position: absolute;
            width: 100%;
            top: 66px;
            left: 64px;
            color: white;
            font-size: 24px;
        }
        .top-small {
            position: absolute;
            bottom: 52px;
            color: white;
            left: 64px;
            width: 100%;
            font-size: 15px;
        }
       
        .cell {
            display: block;
            margin: 24px auto;
            padding: 18px;
            box-sizing: border-box;
            font-size: 14px;
            color: #585858;
            background: white;
            border-radius: 4px;
            border-collapse: separate;
            border-spacing: 10px 5px;
        }
        .tit {
            font-size: 16px;
            text-align: center;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 10px;
            margin-top: 0;
        }
        .bb {
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 16px;
            color: #a7a4a4;
        }
        a {
            color: #2196F3;
            text-decoration: none;
        }
    </style>
    　
</head>

<body style="margin: 0 auto 24px; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="600px" style="margin: 0 auto;position: relative;">
        <tr style="position:relative;display:block;">
            <td>
                <img border="0" width="600" src="http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/tuihuotuikuan.jpg" style="display:block;outline:none;text-decoration:none;border:none;">
            </td>
        </tr>
        <tr style="background: #f3f4f5;">
            <td class="cell" width="544">
                <p style="font-size: 18px;margin-bottom: 0px;">尊敬的 '.$nickname.' 您好！</p>
                <p class="bb"> 感谢您在Apple Employee Choice购物！</p>
                <p style="line-height: 2;">
                    您于<a href="#">'.date('Y-m-d',$return_goods['addtime']).'</a>日提交的订单 <a href="#">'.$order['order_sn'].'</a> 退货退款申请，经销商已确认收货并检测完成，退款将于 <a href="#">3-7个工作日内退回原处</a>，请耐心等待。
                </p>
            </td>
        </tr>
        <tr style="background: #e2e2e2;">
            <td width="540" style="text-align:center;display: block;margin: 16px auto;line-height: 4;font-size: 14px;">
                温馨提示：如果您有任何售后问题，请您联系经销商，电话：010-54353566
            </td>
        </tr>
    </table>
</body>

</html>';
        
        
        sendEmail($users['email'], '退货退款订单进度:检测通过',$content);
        
    }
    
    
    /**
     * 退货退款订单，经销商确认后给用户发邮件
     * @param $order
     * @param $return_goods
     * @throws \think\exception\DbException
     */
    protected function send_by_seller_confirm($order,$return_goods){
        
        $users = Users::get($order['user_id']);
        if(!$users['email']||$users['email']==''){
            return;
        }
        
        $nickname = '';
        //按照优先级来
        if(!isEmptyObject($users['nickname'])){
            $nickname = $users['nickname'];
        }elseif ($order['shipping_code']==='ziti'){
            $ziti_info = json_decode($order['ziti_info'],true);
            if(isset($ziti_info['user_name'])&&!isEmptyObject($ziti_info['user_name'])){
                $nickname = $ziti_info['user_name'];
            }
        }elseif($order['shipping_code']==='shunfeng'&&!isEmptyObject($order['consignee'])){
            $nickname = $order['consignee'];
        }
        
        
        $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
　

<head>
    　　
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    　　
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        * {
            font-family: "SF Pro SC", "SF Pro Text", "SF Pro Icons", "PingFang SC", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
        }
        .top-txt {
            position: absolute;
            width: 100%;
            top: 66px;
            left: 64px;
            color: white;
            font-size: 24px;
        }
        .top-small {
            position: absolute;
            bottom: 52px;
            color: white;
            left: 64px;
            width: 100%;
            font-size: 15px;
        }
       
        .cell {
            display: block;
            margin: 24px auto;
            padding: 18px;
            box-sizing: border-box;
            font-size: 14px;
            color: #585858;
            background: white;
            border-radius: 4px;
            border-collapse: separate;
            border-spacing: 10px 5px;
        }
        .tit {
            font-size: 16px;
            text-align: center;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 10px;
            margin-top: 0;
        }
        .bb {
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 16px;
            color: #a7a4a4;
        }
        a {
            color: #2196F3;
            text-decoration: none;
        }
    </style>
    　
</head>

<body style="margin: 0 auto 24px; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="600px" style="margin: 0 auto;position: relative;">
        <tr style="position:relative;display:block;">
            <td>
                <img border="0" width="600" src="http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/jingxiaoshang.jpg" style="display:block;outline:none;text-decoration:none;border:none;">
            </td>
        </tr>
        <tr style="background: #f3f4f5;">
            <td class="cell" width="544">
                <p style="font-size: 18px;margin-bottom: 0px;">尊敬的 '.$nickname.' 您好！</p>
                <p class="bb"> 感谢您在Apple Employee Choice购物！</p>
                <p style="line-height: 2;">
                    您于<a href="#">'.date('Y-m-d',$return_goods['addtime']).'</a>日提交的订单 <a href="#"> '.$order['order_sn'].'</a> 退货退款申请，经销商已确认，请您及时寄回商品并在 <a href="#">Apple Employee Choice</a> 填写快递单号或选择自送门店。
                </p>
            </td>
        </tr>
        <tr style="background: #e2e2e2;">
            <td width="540" style="text-align:center;display: block;margin: 16px auto;line-height: 4;font-size: 14px;">
                温馨提示：如果您有任何售后问题，请您联系经销商，电话：010-54353566
            </td>
        </tr>
    </table>
</body>

</html>';
    
    
        sendEmail($users['email'], '退货退款订单进度:经销商确认',$content);
        
        
    }
    
    
    /**
     * 仅退款订单处理成功
     */
    protected function send_by_only_money($order,$return_goods){
    
//        var_dump($order);die;
        
        $users = Users::get($order['user_id']);
        if(!$users['email']||$users['email']==''){
            return;
        }
        $nickname = '';
        
        
      
        //按照优先级来
        if(!isEmptyObject($users['nickname'])){
            $nickname = $users['nickname'];
        }elseif ($order['shipping_code']==='ziti'){
            $ziti_info = json_decode($order['ziti_info'],true);
            if(isset($ziti_info['user_name'])&&!isEmptyObject($ziti_info['user_name'])){
                $nickname = $ziti_info['user_name'];
            }
        }elseif($order['shipping_code']==='shunfeng'&&!isEmptyObject($order['consignee'])){
            $nickname = $order['consignee'];
        }
    
    
        $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
　

<head>
    　　
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    　　
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        * {
            font-family: "SF Pro SC", "SF Pro Text", "SF Pro Icons", "PingFang SC", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
        }
        .top-txt {
            position: absolute;
            width: 100%;
            top: 66px;
            left: 64px;
            color: white;
            font-size: 24px;
        }
        .top-small {
            position: absolute;
            bottom: 52px;
            color: white;
            left: 64px;
            width: 100%;
            font-size: 15px;
        }
       
        .cell {
            display: block;
            margin: 24px auto;
            padding: 18px;
            box-sizing: border-box;
            font-size: 14px;
            color: #585858;
            background: white;
            border-radius: 4px;
            border-collapse: separate;
            border-spacing: 10px 5px;
        }
        .tit {
            font-size: 16px;
            text-align: center;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 10px;
            margin-top: 0;
        }
        .bb {
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 16px;
            color: #a7a4a4;
        }
        a {
            color: #2196F3;
            text-decoration: none;
        }
    </style>
    　
</head>

<body style="margin: 0 auto 24px; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="600px" style="margin: 0 auto;position: relative;">
        <tr style="position:relative;display:block;">
            <td>
                <img border="0" width="600" src="http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/jintuikuan.jpg" style="display:block;outline:none;text-decoration:none;border:none;">
            </td>
        </tr>
        <tr style="background: #f3f4f5;">
            <td class="cell" width="544">
                <p style="font-size: 18px;margin-bottom: 0px;">尊敬的 '.$nickname.' 您好！</p>
                <p class="bb"> 感谢您在Apple Employee Choice购物！</p>
                <p style="line-height: 2;">
                    您于<a href="#">'.date('Y-m-d',$return_goods['addtime']).'</a>日提交的订单 <a href="#"> '.$order['order_sn'].'</a> 退款申请，经销商已确认，退款将于<a href="#">3-7个工作日内退回原处</a>，请耐心等待。
                </p>
            </td>
        </tr>
        <tr style="background: #e2e2e2;">
            <td width="540" style="text-align:center;display: block;margin: 16px auto;line-height: 4;font-size: 14px;">
                温馨提示：如果您有任何售后问题，请您联系经销商，电话：010-54353566
            </td>
        </tr>
    </table>
</body>

</html>';
    
    
        sendEmail($users['email'], '退款订单进度:已确认',$content);
        
    }
    
    
    /**
     * 商家确认退款
     */
    public function confirm_refund_money(){
        
        $id = I('id');
        $return_goods = M('return_goods')->where(array('id'=>$id,'store_id'=>array('in',STORES)))->find();
        
        if($return_goods){

            $orderInfo = Db::name('order')->where('order_id',$return_goods['order_id'])
                ->find();
            $orderLogic = new OrderLogic();

            $action = '确认退款';
            $orderLogic->orderActionLog($orderInfo, $action,'退款:检验商品合格后退款', SELLER_ID, 1);

            $this->refundMoney($return_goods);
        
        }else{
            $this->error("参数无效");
        }
    }
    
    /**
     * 经销商检测
     */
    public function confirm_goods(){
    	$id = I('id');
    	$return_goods = M('return_goods')->where(array('id'=>$id,'store_id'=>array('in',STORES)))->find();
        $params = $this->request->param();
       
    	if($return_goods){
    	    
    	    
    	    if($params['status']==1){
    	        $status = 4;
            }else{
    	        $status = 5;//细化状态，5是检测商品不通过，
                $order_goods = OrderGoods::get(['rec_id'=>$return_goods['rec_id']]);
                $order_goods->refuseReturn($return_goods['type']);
            }
        
            $rt = M('return_goods')->where(array('id'=>$return_goods['id'],'store_id'=>array('in',STORES)))->save(array('status'=>$status,'checktime'=>time()));
    	    if($rt!==false){
                $refer = $return_goods['type']>1 ? U('Service/return_list') : U('Service/refund_list');
                $this->success('操作成功!',$refer);exit;
            }else{
                
                $this->error('操作失败!');exit;
            }
    	    
    	    
    	}else{
//    		$this->error("参数无效");
    	}
    }

    public function complain_list()
    {
		$complain_state = I('complain_state');
		$map = array();
		$map['store_id'] = STORE_ID;
		if($complain_state){
			$map['complain_state'] = $complain_state;
		}
        $begin =  $this->begin;
        $end   =  $this->end;
		if($begin && $end){
			$map['complain_time'] = array('between',"$begin,$end");
		}
        $type = I('type');
        $key = trim(I('key'));
        if(!empty($key)){
            $map["$type"]=$key;
        }
		$count = M('complain')->where($map)->count();
		$page = new Page($count,10);
		$lists  = M('complain')->where($map)->order('complain_time desc')->limit($page->firstRow.','.$page->listRows)->select();
		if(!empty($lists)){
			$goods_id_arr = get_arr_column($lists, 'order_goods_id');
			$goodsList = M('goods')->where("goods_id in (".  implode(',',$goods_id_arr).")")->getField('goods_id,goods_name');
			$this->assign('goodsList',$goodsList);
		}
		$this->assign('page',$page->show());
		$this->assign('lists',$lists);
		$complain_state = array(1=>'待处理',2=>'对话中',3=>'待仲裁',4=>'已完成');
		$this->assign('state',$complain_state);
        return $this->fetch();
    }
    
    public function complain_info(){
    	$complain_id = I('complain_id/d');
    	$complain = M('complain')->where(array('complain_id'=>$complain_id,'store_id'=>array('in',STORES)))->find();
        $select_year = getTabByOrderId($complain['order_id']);
    	$order = M('order'.$select_year)->where(array('order_id'=>$complain['order_id']))->find();
    	$order_goods = M('order_goods'.$select_year)->where(array('order_id'=>$complain['order_id'],'goods_id'=>$complain['order_goods_id']))->find();
		$ids[] = $order['province'];
		$ids[] = $order['city'];
		$ids[] = $order['district'];
		if(!empty($ids)){
			$regionLits = M('region')->where("id", "in" , implode(',', $ids))->getField("id,name");
			$this->assign('regionLits',$regionLits);
		}
    	if(!empty($complain['complain_pic'])){
    		$complain['complain_pic'] = unserialize($complain['complain_pic']);
    	}
    	if(!empty($complain['appeal_pic'])){
    		$complain['appeal_pic'] = unserialize($complain['appeal_pic']);
    	}
    	$this->assign('complain',$complain);
    	$this->assign('order',$order);
    	$this->assign('order_goods',$order_goods);
    	return $this->fetch();
    }
    
    public function complain_appeal(){
    	if(IS_POST){
    		$data = I('post.');
    		$complain = M('complain')->where(array('complain_id'=>$data['complain_id'],'store_id'=>array('in',STORES)))->find();
    		if(!$complain) $this->ajaxReturn(['status'=>0,'msg'=>'非法操作或参数有误']);
    		if(is_array($data['appeal_pic'])){
    			$data['appeal_pic'] = serialize($data['appeal_pic']);
    		}
    		$data['appeal_time'] = time();
    		$data['complain_state'] = 2;
    		M('complain')->where(array('complain_id'=>$data['complain_id'],'store_id'=>array('in',STORES)))->save($data);
            $this->ajaxReturn(['status'=>1,'msg'=>'申诉成功','url'=>U('Service/complain_list')]);
    	}
    }
    
    public function get_complain_talk(){
    	$complain_id = I('complain_id/d');
    	$complain_info = M('complain')->where(array('complain_id'=>$complain_id,'store_id'=>array('in',STORES)))->find();
    	$complain_info['member_status'] = 'accused';
    	$complain_talk_list = M('complain_talk')->where(array('complain_id'=>$complain_id))->order('talk_id desc')->select();
    	$talk_list = array();
    	if(!empty($complain_talk_list)){
    		foreach($complain_talk_list as $i=>$talk) {
    			$talk_list[$i]['css'] = $talk['talk_member_type'];
    			$talk_list[$i]['talk'] = date("Y-m-d H:i:s",$talk['talk_time']);
    			switch($talk['talk_member_type']){
    				case 'accuser':
    					$talk_list[$i]['talk'] .= '投诉人';
    					break;
    				case 'accused':
    					$talk_list[$i]['talk'] .= '被投诉店铺';
    					break;
    				case 'admin':
    					$talk_list[$i]['talk'] .= '管理员';
    					break;
    				default:
    					$talk_list[$i]['talk'] .= '未知';
    			}
    			if(intval($talk['talk_state']) === 2) {
    				$talk['talk_content'] = '<该对话被管理员屏蔽>';
    			}
    			$talk_list[$i]['talk'].= '('.$talk['talk_member_name'].')说:'.$talk['talk_content'];
    		}
    	}
    	echo json_encode($talk_list);
    }
    
    public function publish_complain_talk(){
    	$complain_id = I('complain_id/d');
    	$complain_talk = trim(I('complain_talk'));
    	$talk_len = strlen($complain_talk);
    	if($talk_len>0 && $talk_len<255){
    		$complain_info = M('complain')->where(array('complain_id'=>$complain_id,'store_id'=>array('in',STORES)))->find();
    		$complain_state = intval($complain_info['complain_state']);
    		$param = array();
    		$param['complain_id'] = $complain_id;
    		$param['talk_member_id'] = session('seller')['seller_id'];  //记录为处理客户的ID
    		$param['talk_member_name'] = $this->storeInfo['store_name']; //记录为店铺名
    		$param['talk_member_type'] = 'accused';
    		$param['talk_content'] = $complain_talk;
    		$param['talk_state'] = 1;
    		$param['talk_admin'] = 0;
    		$param['talk_time'] = time();
    		if(M('complain_talk')->add($param)){
    			echo json_encode('success');
    		}else{
    			echo json_encode('error2');
    		}
    	}else{
    		echo json_encode('error1');
    	}
    }
}