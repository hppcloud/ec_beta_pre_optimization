<?php
namespace app\seller\controller;

use app\common\model\team\TeamActivity;
use app\common\model\team\TeamFound;
use app\seller\logic\TeamActivityLogic;
use think\Loader;
use think\Db;
use think\Page;

class Team extends Base
{
	public function index()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 拼团详情
	 * @return mixed
	 */
	public function info()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 保存
	 * @throws \think\Exception
	 */
	public function save(){
	header("Content-type: text/html; charset=utf-8");
exit("请联系TPshop官网客服购买高级版支持此功能");
	}

	/**
	 * 删除拼团
	 */
	public function delete(){
	header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 确认拼团
	 * @throws \think\Exception
	 */
	public function confirmFound(){
	header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 拼团退款
	 */
	public function refundFound(){
	header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 拼团抽奖
	 */
	public function lottery(){
	header("Content-type: text/html; charset=utf-8");
	}
}
