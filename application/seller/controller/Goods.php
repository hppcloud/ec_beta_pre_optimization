<?php
namespace app\seller\controller;

use app\common\model\Goods as GoodsModel;
use app\common\model\SpecGoodsPrice;
use app\seller\logic\GoodsCategoryLogic;
use app\seller\logic\GoodsLogic;
use think\AjaxPage;
use think\Db;
use think\Loader;
use think\Page;

class Goods extends Base
{
    
    public $table = 'goods';


    public function muliti_list(){

        $goods_name = I('goods_name');
        $cat_id3 = I('cat_id3');


        // $filter = ['g.tmpl_id'=>0];
        # 201903  =0 或者 =-1 and seller

        if(!isEmptyObject($goods_name)){
            $filter['g.goods_name'] = array('like', "%$goods_name%");
        }

        if($cat_id3){
            $filter['g.cat_id3'] = $cat_id3;
        }

        # 201903 添加主产品/非主产品筛选
        if(I('prod_t')){
            if(I('prod_t') == 'notmain'){
//                $filter['g.tmpl_sn'] = ['like','F%'];
                $fgid = GoodsModel::where(['is_deleted' => 0,'seller_id' => SELLER_ID])->where('tmpl_id','exp','=-1 or tmpl_id is null')->column('goods_id');
                $filter['g.goods_id'] = ['in',$fgid];
            }else{
//                $filter['g.tmpl_sn'] = ['not like','F%'];

            }
            $this->assign('prod_t',I('prod_t'));
        }


        $cates = Db::name('GoodsCategory')->where('level',3)->select();
        $this->assign('cate_list',$cates);

        if(I('prod_t') == 'notmain') {
            $count = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->where('tmpl_id', 'exp', ' = -1 and seller_id = ' . SELLER_ID)
                ->field('g.*,gc.name as cate_title,gc.first_name as cate_first_name')->count();
        }elseif(I('prod_t') == 'main'){
            $filter['g.tmpl_id']  = 0;
            $count = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->field('g.*,gc.name as cate_title,gc.first_name as cate_first_name')->count();
        }else{
            $filter['g.tmpl_id']  = 0;
            $count = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->whereOr('tmpl_id', 'exp', ' = -1 and seller_id = ' . SELLER_ID)
                ->field('g.*,gc.name as cate_title,gc.first_name as cate_first_name')->count();
        }
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);// 赋值分页输出

        if(I('prod_t') == 'notmain') {
            $tmpls = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->where('tmpl_id', 'exp', ' = -1 and seller_id = ' . SELLER_ID)
                ->field('g.*,gc.name as cate_title')
                ->limit($Page->firstRow . ',' . $Page->listRows)->select();
        }elseif(I('prod_t') == 'main'){
            $filter['g.tmpl_id']  = 0;
            $tmpls = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->field('g.*,gc.name as cate_title')
                ->limit($Page->firstRow . ',' . $Page->listRows)->select();
        }else{
            $filter['g.tmpl_id']  = 0;
            $tmpls = Db::name('Goods')
                ->alias('g')
                ->join('tp_goods_category gc', 'gc.id=g.cat_id3')
                ->where($filter)
                ->whereOr('tmpl_id', 'exp', ' = -1 and seller_id = ' . SELLER_ID)
                ->field('g.*,gc.name as cate_title')
                ->limit($Page->firstRow . ',' . $Page->listRows)->select();
        }
        # 201903 添加
        foreach($tmpls as $k=>$v){
            $tmpls[$k]['lx'] = isset($v['tmpl_sn']) ? (substr($v['tmpl_sn'],0,1) == 'F' ? '非主产品' : '主产品') : '主产品';
        }

        $this->assign('tmpls',$tmpls);


        return $this->fetch();


    }
    
    
    /**
     * 批量修改商品属性
     */
    public function _muliti(){
        
        //
        
        
        //编辑页面
        if($this->request->isGet()){
            $tmpl_id = input('tmpl_id');
            # 201903
            $w['goods_id'] =  $tmpl_id;
            $w['tmpl_id'] = ['in','0,-1'];
            $goods = Db::name('goods')->where($w)->find();
            if(!$goods){
                $this->error('模板商品不存在');
            }
            $this->assign('goodsInfo',$goods);
            # 201903 判断主产品和非主产品 显示不同编辑页面
            if( substr($goods['tmpl_sn'], 0, 1) == 'F' ){
                // 非主产品
                $goodsImages = Db::name("GoodsImages")->where('goods_id', $tmpl_id)->select();
                $this->assign('goodsImages',$goodsImages);
                return $this->fetch('f_muliti');
            }else{
                return $this->fetch('_muliti');
            }
        }
        
        //编辑逻辑
        if($this->request->isPost()){
    
            $tmpl_id = input('tmpl_id');
            # 201903 whereOr
            $goods = Db::name('goods')->where('goods_id',$tmpl_id)->where('tmpl_id',0)->whereOr('tmpl_id',-1)->find();
            if(!$goods){
               
                $this->ajaxReturn(array('status' => 0, 'msg' => '模板商品不存在','result'=>''));
            }
            
            $rule = [
                'tmpl_id|模板id'=>'require',
                'shop_price|本店售价'=>'require',
                'market_price|市场价'=>'require',
                'is_reserve|是否预约'=>'require|between:0,1',
                'is_pay|是否分期'=>'require',
//                'is_on_sale|上下架'=>'require|between:0,1',
//                'is_on_sale|上下架'=>'require|between:0,1',
                'is_recommend|是否推荐'=>'require|between:0,1',
                'is_hot|是否热门'=>'require|between:0,1',
                'is_share_stock|是否共享库存'=>'require|between:0,1',
                'is_on_sale|上下架' => 'require|between:0,1',
            ];
            
            $params = $this->request->param();
    
            // $check=$this->validate($params,$rule);
            // if(true !== $check){
            //     $this->error($check);
            // }
            
            $post = [
                'shop_price'=>input('shop_price'),
                'market_price'=>input('market_price'),
                'is_reserve'=>input('is_reserve'),
                'is_pay'=>input('is_pay'),
//                'is_on_sale'=>input('is_on_sale'),
//                'is_on_sale'=>input('is_on_sale'),
                'is_recommend'=>input('is_recommend'),
                'is_hot'=>input('is_hot'),
                'is_share_stock' => input('is_share_stock'),
                'is_on_sale'    => input('is_on_sale'),
            ];
            
            # 201903
            $post1 = array(
                'goods_content' => input('goods_content'),
                'goods_specattr' => input('goods_specattr'),
                'goods_package' => input('goods_package'),
            );
            $post = array_merge($post, $post1);
            if(!input('is-shop')){
                unset($post['shop_price']);
            }
            if(!input('is-market')){
                unset($post['market_price']);
            }
            if(!input('is-res')){
                unset($post['is_reserve']);
            }
            if(!input('is-p')){
                unset($post['is_pay']);
            }
            if(!input('is-stock')){
                unset($post['is_share_stock']);
            }
            if(!input('is-tj')){
                unset($post['is_recommend']);
            }
            if(!input('is-rm')){
                unset($post['is_hot']);
            }
            if(!input('is-sale')){
                unset($post['is_on_sale']);
            }
            if(!input('is-ms')){
                unset($post['goods_content']);
                unset($post['goods_specattr']);
                unset($post['goods_package']);
            }

            $store_id_arr = input('store/a');
            if(count($store_id_arr)<1){
                $this->ajaxReturn(array('status' => 0, 'msg' => '请至少选择一个店铺','result'=>''));
            }
            
            if (!input('is-zk')) {
                
            }else{
                if(isset($params['discount'])&&$params['discount']!=''){
        
                    if($params['discount']<0||$params['discount']>100){
                        $this->ajaxReturn(array('status' => 0, 'msg' => '折扣数值只能填写0-99','result'=>''));
                    }
            
                    if(!$params['discount_start']||!$params['discount_end']){
                        $this->ajaxReturn(array('status' => 0, 'msg' => '折扣的起止时间必填','result'=>''));
                    }

                    $post['discount'] = $params['discount'];
                    $post['discount_limit'] = $params['discount_limit'];
                    $post['discount_start'] = strtotime("{$params['discount_start']} 00:00:00");
                    $post['discount_end'] = strtotime("{$params['discount_end']} 23:59:59");
            
                    if($params['discount_start']>$params['discount_end']){
                        $this->ajaxReturn(array('status' => 0, 'msg' => '结束时间应该大于起始时间','result'=>''));
                    }
                    
            
                }else{
                    $post['discount'] = null;
                    $post['discount_start'] = null;
                    $post['discount_end'] = null;
                    $post['discount_limit'] = null;
                }
            }


            
            $where = [
                'store_id'=>array('in',$store_id_arr),
                'tmpl_id'=>$tmpl_id,
                'goods_id'=>array('neq',$tmpl_id),
                # 201903
                'is_on_sale' => 1,
            ];
            
            $rt = Db::name('goods')->where($where)->update($post);
            
            // 201903 批量更改商品规格、商品相册图片
            $goodsIdsArr = Db::name('goods')->where($where)->field('goods_id,store_id')->select();
            if(is_array($goodsIdsArr) && $goodsIdsArr){
                $type_id = M('goods_category')->where("id", input('cat_id3'))->getField('type_id');
                foreach($goodsIdsArr as $k=>$v){
                    // 判断是否需要更新
                    if(input('is-spec') == 'spec'){
                        $this->updateGoodsSpec($v['goods_id'],$v['store_id']);
                    }
                    if(input('is-img') == 'img'){
                        $this->updateGoodsImg($v['goods_id']);
                    }
                    if(input('is-attr') == 'attr'){
                        $this->updateGoodsAttr($v['goods_id'], $type_id);
                    }
                }
            }

            if($rt!==false){
                $this->ajaxReturn(array('status' =>1, 'msg' => '信息覆盖成功','result'=>''));
            }else{
                $this->ajaxReturn(array('status' => 0, 'msg' => '信息修改失败','result'=>''));
            }
            
            
        }
        
    }
   

    /**
     *  商品列表
     */
    public function goodsList()
    {
        
        checkIsBack();
    
    
        $nowPage = 1;
        
        
//        $store_goods_class_list = M('store_goods_class')->where(['parent_id' => 0, 'store_id' => STORE_ID])->select();
//        $this->assign('store_goods_class_list', $store_goods_class_list);
//        $suppliers_list = M('suppliers')->where(array('store_id'=>STORE_ID))->select();
//        $this->assign('suppliers_list', $suppliers_list);
        $this->assign("now_page" , $nowPage);
    
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        return $this->fetch('goodsList');
    }
    
    
    public function copyGoodsList()
    {
        
        checkIsBack();
        
        
        $nowPage = 1;
        
//        $store_goods_class_list = M('store_goods_class')->where(['parent_id' => 0, 'store_id' => STORE_ID])->select();
//        $this->assign('store_goods_class_list', $store_goods_class_list);
//        $suppliers_list = M('suppliers')->where(array('store_id'=>STORE_ID))->select();
//        $this->assign('suppliers_list', $suppliers_list);
        $this->assign("now_page" , $nowPage);
        $stores = Db::name('store')->where('is_tmpl',1)->field('store_id,store_name')->select();
//        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        return $this->fetch('copyGoodsList');
    }

    // # 201903 非主产品
    public function copyNotMajorGoodsList(){
        checkIsBack();
        $nowPage = 1;

        $this->assign("now_page" , $nowPage);
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        return $this->fetch();
    }
    
    /**
     *  可以复制的商品列表
     */
    public function ajaxCopyGoodsList()
    {
        $where = array();
        # 201903
        $t = input('t');
        //如果有搜索就指定店铺啦
        if(I('store_id')){
            $where['store_id'] = I('store_id');
            # 201903
            if($t == 'n'){
                $where['seller_id'] = SELLER_ID;
                $where['tmpl_id'] = -1;
            }
        }else{
            # 201903
            if($t == 'n'){
                // 当前经销商下的非主产品 seler_id、tmpl_id=-1
                $where['seller_id'] = SELLER_ID;
                $where['tmpl_id'] = -1;
            }else{
                $stores = Db::name('store')->where('is_tmpl',1)->field('store_id,store_name')->column('store_id');
                $where['store_id'] = array('in',$stores);
            }
        }
        
        
        
        $where['is_deleted'] = 0;
        
        $intro = I('intro', 0);
        $store_cat_id1 = I('store_cat_id1', '');
        $key_word = trim(I('key_word', ''));
        $orderby1 = I('post.orderby1', '');
        $orderby2 = I('post.orderby2', '');
        $suppliers_id = input('suppliers_id','');
        if($suppliers_id !== ''){
            $where['suppliers_id'] = $suppliers_id;
        }
        if (!empty($intro)) {
            $where[$intro] = 1;
        }
        if ($store_cat_id1 !== '') {
            $where['store_cat_id1'] = $store_cat_id1;
        }
        $where['is_on_sale'] = 1;
        $where['goods_state'] = 1;
        if ($key_word !== '') {
            $where['goods_name|goods_sn'] = array('like', '%' . $key_word . '%');
        }
        $order_str = array();
        if ($orderby1 !== '') {
            $order_str[$orderby1] = $orderby2;
        }
        $model = M('Goods');
        $count = $model->where($where)->count();
        $Page = new AjaxPage($count, 10);
        
        
        
        //是否从缓存中获取Page
        if (session('is_back') == 1) {
            $Page = getPageFromCache();
            //重置获取条件
            delIsBack();
        }
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $goodsList = $model->where($where)->order($order_str)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        foreach ($goodsList as &$goods){
            foreach ($stores as $store){
                if($store['store_id']==$goods['store_id']){
                    $goods['store_name'] = $store['store_name'];
                    break;
                }
            }
        }
        cachePage($Page);
        $show = $Page->show();
        
        $catList =  M('goods_category')->cache(true)->select();
        $catList = convert_arr_key($catList, 'id');
//        $store_warning_storage = M('store')->where('store_id', STORE_ID)->getField('store_warning_storage');
//        $this->assign('store_warning_storage', $store_warning_storage);
        $this->assign('catList', $catList);
        $this->assign('goodsList', $goodsList);
        $this->assign('page', $show);// 赋值分页输出
        
        //用来显示商品所在类别，又学一个技巧
        
//        echo '<pre>';
//        var_dump($goodsList);die;
        
        
        return $this->fetch();
    }
    

    /**
     *  商品列表
     */
    public function ajaxGoodsList()
    {
        
        $where = array();
        $store_id = I('store_id',null);
        //如果有搜索就指定店铺啦
        if($store_id){
            $where['store_id'] = $store_id;
        }else{
            $where['store_id'] = array('in',STORES);
        }


        $where['seller_id'] = SELLER_ID;
        
        $where['is_deleted'] = 0;
        
        $intro = I('intro', 0);
        $store_cat_id1 = I('store_cat_id1', '');
        $key_word = trim(I('key_word', ''));
        $orderby1 = I('post.orderby1', '');
        $orderby2 = I('post.orderby2', '');
        $suppliers_id = input('suppliers_id','');
        if($suppliers_id !== ''){
            $where['suppliers_id'] = $suppliers_id;
        }
        if (!empty($intro)) {
            $where[$intro] = 1;
        }
        if ($store_cat_id1 !== '') {
            $where['store_cat_id1'] = $store_cat_id1;
        }
        $where['is_on_sale'] = 1;
        $where['goods_state'] = 1;
        if ($key_word !== '') {
            $where['goods_name|goods_sn'] = array('like', '%' . $key_word . '%');
        }

        # 201903 添加主产品/非主产品筛选
        if(I('prod_t')){
            $wh['is_on_sale'] = 1;
            $wh['goods_state'] = 1;
            $wh['is_deleted'] = 0;
            if(I('prod_t') == 'notmain'){
                $wh['tmpl_sn'] = ['like','F%'];
                $wh['tmpl_id'] = -1;
                $goodsId = GoodsModel::where($wh)->column('goods_id');
                $where['tmpl_id'] = ['in',$goodsId];
            }else{
                $wh['tmpl_sn'] = ['not like','F%'];
                $wh['tmpl_id'] = 0;
                $goodsId = GoodsModel::where($wh)->column('goods_id');
                $where['tmpl_id'] = ['in',$goodsId];
            }
            $this->assign('prod_t',I('prod_t'));
        }else{
            $where['tmpl_id'] = array('exp','is null or tmpl_id != -1');
//            $where['tmpl_id'] = array('neq',-1);
        }

        $order_str = array();
        if ($orderby1 !== '') {
            $order_str[$orderby1] = $orderby2;
        }
        $model = M('Goods');
        $count = $model->where($where)->count();
        $Page = new AjaxPage($count, 10);
        
        
        
        //是否从缓存中获取Page
        if (session('is_back') == 1&&\think\Session::has('TPSHOP_PAGE')) {
            $Page = getPageFromCache();
            //重置获取条件
            delIsBack();
        }
        
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        
        
        $goodsList = $model->where($where)->order($order_str)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $goods_ids = get_arr_column($goodsList,'goods_id');
        
        $goods_stock_list = Db::name('SpecGoodsPrice')->where('goods_id','in',$goods_ids)->field('goods_id,sum(store_count) as all_store_stock')->group('goods_id')->select();
        
        
        foreach ($goodsList as &$goods){
            foreach ($goods_stock_list as $stock){
                if($stock['goods_id']==$goods['goods_id']){
                    $goods['all_store_stock'] = $stock['all_store_stock'];
                }
            }
            foreach ($stores as $store){
                if($store['store_id']==$goods['store_id']){
                    $goods['store_name'] = $store['store_name'];
                    break;
                }
            }
        }
        if(input('is_back')==1){
            cacheIsBack();
            
        }
    
        cachePage($Page);
        $show = $Page->show();
        
        $catList =  M('goods_category')->cache(true)->select();
        $catList = convert_arr_key($catList, 'id');
//        $store_warning_storage = M('store')->where('store_id', STORE_ID)->getField('store_warning_storage');
//        $this->assign('store_warning_storage', $store_warning_storage);
        $this->assign('catList', $catList);
        $this->assign('goodsList', $goodsList);
        $this->assign('page', $show);// 赋值分页输出
    
        //用来显示商品所在类别，又学一个技巧
        
        
        return $this->fetch();
    }
    
    
    public function copyGoods(){
        
        if(IS_GET){
            $goods_id = I('goods_id');
            if(!$goods_id){
                $this->error('goods_id必传',U('Goods/goodsList'));
            }
    
            $goods = Db::name('goods')->where('goods_id',$goods_id)->find();
            if(!$goods){
                $this->error('goods_id必传',U('Goods/goodsList'));
            }

            $store = Db::name('store')->where('store_id',$goods['store_id'])->field('store_id,is_tmpl')->find();

            if($goods['seller_id']!=SELLER_ID&&(!$store||$store['is_tmpl']!=1)){
              $this->ajaxReturn([ 'status' => 0, 'msg' => '该商品不属于当前经销商或者不属于模板店铺', 'result' => null]);
            }

            $this->assign('seller_id',SELLER_ID);
            //店铺列表
            $sotres = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();
            $this->assign('stores',$sotres);
    
            $this->assign('goodsInfo',$goods);
            return $this->fetch('copy');
        }
        
        //提交了
        if(IS_POST){
            $goods_id = I('goods_id');
            if(!$goods_id){
                $this->ajaxReturn([ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null]);
            }
    
            $goods = Db::name('goods')->where('goods_id',$goods_id)->find();
            if(!$goods){
                $this->ajaxReturn([ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null]);
            }
    
            //2018-07-22 取消不能复制自己的店铺的限制
            //if($goods['seller_id']!=SELLER_ID){
              //  $this->ajaxReturn([ 'status' => 0, 'msg' => '该商品不属于当前经销商', 'result' => null]);
            //}
            $store = Db::name('store')->where('store_id',$goods['store_id'])->field('store_id,is_tmpl')->find();

            //两个都不满足才报错
            if($goods['seller_id']!=SELLER_ID&&(!$store||$store['is_tmpl']!=1)){
                $this->ajaxReturn([ 'status' => 0, 'msg' => '该商品不属于当前经销商或者不属于模板店铺', 'result' => null]);
            }



            if(!$goods['cat_id3']){
                $this->ajaxReturn([ 'status' => 0, 'msg' => '模板商品的栏目信息无效', 'result' => null]);
            }



            
            $stores = I('store/a');
            if(!$stores||count($stores)<1){
                $this->ajaxReturn([ 'status' => 0, 'msg' => '请至少选择一个店铺', 'result' => null]);
            }
            
            $tmpl_goods_id = $goods_id;//简单值传递，所以不存在什么问题。
            
            //复制商品要形成绑定关系
            # 201903
            if(($goods['tmpl_id']==0 || $goods['tmpl_id']==-1)&&$goods['tmpl_sn']){
                $goods['tmpl_id'] = $goods_id;
            }else{
                if($goods['tmpl_id']>0){

                }else{
                    unset($goods['tmpl_id']);
                }

            }
            
            //删掉不合适的属性
            unset($goods['tmpl_sn']);
            
            unset($goods['goods_id']);
            unset($goods['goods_sn']);
            $err = 0;
            $GoodsLogic = new GoodsLogic();
            $type_id = $goods['cat_id3'];//这里是给新商品加属性的
            foreach ($stores as $store){

                $goods['store_id'] = $store;
                $goods['seller_id'] = SELLER_ID;//保险一点
                $rt = Db::name('goods')->insert($goods);
                $goods_id = Db::name('goods')->getLastInsID();
                if(!$rt){
                    $err++;
                }
    
//                if (empty($spec_goods_item)) {
//                    update_stock_log(session('admin_id'), $goods['store_count'], array('goods_id' => $goods_id, 'goods_name' => $goods['goods_name'], 'store_id' => $store));
//                }
    
                $GoodsLogic->afterSave_copy($goods_id,$store,$tmpl_goods_id);
                
                $GoodsLogic->saveGoodsAttr_copy($goods_id,$type_id,$tmpl_goods_id); // 处理商品 属性,这type_id其实就是叶子类目
            }
            
            $this->ajaxReturn([ 'status' => 1, 'msg' => '成功复制到'.(count($stores)-$err).'个店铺，失败'.$err.'个', 'result' => null]);
            
            
        }
        
        
    }
    
    public function goods_offline(){
    
        
        $where = array();
        //如果有搜索就指定店铺啦
        if(I('store_id')){
            $where['store_id'] = I('store_id');
//            $this->assign('store_id',STORES);
        }else{
            $where['store_id'] = array('in',STORES);
        }
    
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
        $where['is_deleted'] = 0;
        
//    	$where['store_id'] = STORE_ID;
    	$model = M('Goods');
        $suppliers_id = input('suppliers_id');
        if($suppliers_id){
            $where['suppliers_id'] = $suppliers_id;
        }
    	if(I('is_on_sale') == 2){
    		$where['is_on_sale'] = 2;
    	}else{
  			$where['is_on_sale'] = 0;
    	}
    	$goods_state = I('goods_state', '', 'string'); // 商品状态  0待审核 1审核通过 2审核失败
    	if($goods_state != ''){
    		$where['goods_state'] = intval($goods_state);
    	}
    	$store_cat_id1 = I('store_cat_id1', '');
    	if ($store_cat_id1 !== '') {
    		$where['store_cat_id1'] = $store_cat_id1;
    	}
    	$key_word = trim(I('key_word', ''));
    	if ($key_word !== '') {
    		$where['goods_name|goods_sn'] = array('like', '%' . $key_word . '%');
    	}
    	
    	$count = $model->where($where)->count();
    	$Page = new Page($count, 10);

        $order_str = array();
        if (I('orderby1') !== '') {
            $order_str[I('orderby1')] = I('orderby2');
        }

    
        //加入排序
    	$goodsList = $model->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order($order_str)->select();

    	$show = $Page->show();
    	$store_goods_class_list = M('store_goods_class')->where(['parent_id' => 0, 'store_id' => STORE_ID])->select();
    	$this->assign('store_goods_class_list', $store_goods_class_list);
//    	$suppliers_list = M('suppliers')->where(array('store_id'=>STORE_ID))->select();
//    	$this->assign('suppliers_list', $suppliers_list);
        
  
		$this->assign('state',C('goods_state'));
    	$this->assign('goodsList', $goodsList);
    	$this->assign('page', $show);// 赋值分页输出
    	return $this->fetch();
    } 

    public function stock_log()
    {
       
    
        if(I('store_id')){
            $map['store_id'] = I('store_id');
        }else{
            $map['store_id'] = array('in',STORES);
        }
        

        $mtype = I('mtype');
        if ($mtype == 1) {
            $map['stock'] = array('gt', 0);
        }
        if ($mtype == -1) {
            $map['stock'] = array('lt', 0);
        }
        $goods_name = I('goods_name');
        if ($goods_name) {
            $map['goods_name'] = array('like', "%$goods_name%");
        }
        $ctime = urldecode(I('post.ctime'));
        if ($ctime) {
            $gap = explode(' - ', $ctime);
            $this->assign('ctime', $gap[0] . ' - ' . $gap[1]);
            $this->assign('start_time', $gap[0]);
            $this->assign('end_time', $gap[1]);
            $map['ctime'] = array(array('gt', strtotime($gap[0])), array('lt', strtotime($gap[1])));
        }
        $count = Db::name('stock_log')->where($map)->count();
        $Page = new Page($count, 20);
        $show = $Page->show();
        $this->assign('page', $show);// 赋值分页输出
        $stock_list = Db::name('stock_log')->where($map)->order('id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $mids = get_arr_column($stock_list,'muid');
        $users = Db::name('users')->where('user_id','in',$mids)->select();
        
        foreach ($stock_list as &$stock){
            foreach ($users as $user){
                if($stock['muid']==$user['user_id']){
                    $stock['muinfo'] = $user;
                }
            }
        }
        
        $this->assign('stock_list', $stock_list);
    
    
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
        return $this->fetch();
    }


    public function stock_index(){


//        if(I('store_id')){
//            $map['g.store_id'] = I('store_id');
//        }else{
//
//        }
    
        $map = ['sgp.goods_id'=>null];
    
    
        $map['g.store_id'] = array('in',STORES);

        
        $spec_name = I('spec_name');
        $goods_name = I('goods_name');
        $cat_id3 = I('cat_id3');
        $status = I('status');
    
        $filter = [];
        if(!isEmptyObject($goods_name)){
            $filter['goods_name'] = array('like', "%$goods_name%");
        }

//        if (isset($_POST['goods_name'])&&!isEmptyObject($_POST['goods_name'])) {
//
//            $goods_name = $_POST['goods_name'];
//
//            $this->assign('goods_name',$goods_name);
//            $gids = Db::name('goods')->where($map)->column('goods_id');
//            unset($map['goods_name']);
//            $map['goods_id'] = array('in',$gids);
//
//        }
        
        if($cat_id3){
            $filter['cat_id3'] = $cat_id3;
           
        }
        $filter['is_on_sale'] = 1;
        $filter['is_deleted'] = 0;

        # 201903 添加主产品/非主产品 筛选
        if(I('prod_t')){
            if(I('prod_t') == 'notmain'){
                $filter['tmpl_id'] = -1;
            }else{
                $filter['tmpl_id'] = 0;
            }
            $this->assign('prod_t',I('prod_t'));
        }else{
            $filter['tmpl_id'] = ['in','0,-1'];
        }

        $pgids = Db::name('goods')->where($filter)->column('goods_id');
        $gids = Db::name('goods')->where('tmpl_id','in',$pgids)->column('goods_id');
       
        # 201903
//        $ids = array_merge($gids,$pgids);
        $ids = $gids;
        
       
        
        
        $cates = Db::name('GoodsCategory')->where('level',3)->select();
    
        //$cate_list = arr2table($cates,'id','parent_id');
        //echo '<pre>';
        //var_dump($cate_list);die;
        $this->assign('cate_list',$cates);
        
        
//        if($spec_name){
//            $map['key_name'] = array('like', "%$spec_name%");
//        }
        
//        $spec_sku = I('spec_sku');
//
//        if(!isEmptyObject($spec_sku)){
//            $map['sku'] = $spec_sku;
//        }else{
//            $map['sku'] = array('neq','');
//        }
    
        
        
        $statusStr = null;
        if(!isEmptyObject($status)){
            if($status){
                $statusStr = 'sum(sgp.store_count)>30';//充足的
            }
            if($status==0){
                $statusStr = 'sum(sgp.store_count)<30';//预警
            }
        }
    
        $map['g.tmpl_id'] = array('exp','is not null');
    
    
        
//        if(count($ids)<1){
//
//            $pgids = Db::name('goods')->where('tmpl_id',0)->column('goods_id');
//            $gids = Db::name('goods')->where('tmpl_id','in',$pgids)->column('goods_id');
//
//            $ids = array_merge($gids,$pgids);
//
//        }
    
        
        
        $map['sgp.goods_id'] = array('in',array_unique($ids));
        //$map['sgp.is_share'] = 1;
        # 2019.3.4 修改  共享库存
        $map['g.is_share_stock'] = 1;

        $map['g.is_on_sale'] = 1;
        $map['g.is_deleted'] = 0;
        
        $countQuyery = Db::name('spec_goods_price')
            ->alias('sgp')
            ->join('tp_goods g','g.goods_id=sgp.goods_id')
            ->join('tp_goods_category gc','gc.id=g.cat_id3')
            ->where($map)
            ->field('gc.name as cate_title,g.tmpl_id,g.goods_name,sum(sgp.store_count) as all_store_stock')
            ->group('g.tmpl_id,gc.name,g.goods_name');
        if($statusStr){
            $count = $countQuyery->having($statusStr)->count();
        }else{
            $count = $countQuyery->count();
        }
        
        $Page = new Page($count, 20);
        $show = $Page->show();
        $this->assign('page', $show);// 赋值分页输出
        $SpecGoodsPrice = new SpecGoodsPrice();
//        ->where('sku','neq','')
        $stock_list_query = $SpecGoodsPrice
            ->alias('sgp')
            ->join('tp_goods g','g.goods_id=sgp.goods_id')
            ->join('tp_goods_category gc','gc.id=g.cat_id3')
            ->where($map)
            ->field('gc.name as cate_title,g.tmpl_id,g.goods_name,sum(sgp.store_count) as all_store_stock')
            ->group('g.tmpl_id,gc.name,g.goods_name')
//            ->select();
            ->limit($Page->firstRow . ',' . $Page->listRows);
//
        if($statusStr){
            $stock_list = $stock_list_query->having($statusStr)->select();
        }else{
            $stock_list = $stock_list_query->select();
        }


        $this->assign('stock_list', $stock_list);


        return $this->fetch();

    }
    
    
    /**
     * 指定模板的sku库存
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    
    public function stock_list(){
        
        
        
        $tmpl_id = input('tmpl_id');
        $tmpl = Db::name('Goods')->where('goods_id',$tmpl_id)->find();
        if(!$tmpl){
            $this->error('模板商品id错误');
        }
        
        $this->assign('tmpl',$tmpl);
    
        $filter['tmpl_id']=$tmpl_id;
        $filter['store_id'] = array('in',STORES);

        $filter['is_deleted']= 0;
        $filter['is_on_sale'] = 1;

        # 201903 共享库存
        $filter['is_share_stock'] = 1;
    
        $gids = M('goods')->where($filter)->getField('goods_id',true);
    
        $map['goods_id'] = null;
        if(count($gids)>0){
            $map['goods_id'] = array('in',$gids);
        }
        
        
//        $goods_name = I('goods_name');
//        $spec_name = I('spec_name');
//        if ($goods_name) {
//            $map['goods_name'] = array('like', "%$goods_name%");
//            $gids = M('goods')->where($map)->getField('goods_id',true);
//            unset($map['goods_name']);
//            if($gids){
//                $map['goods_id'] = array('in',$gids);
//            }
//        }
//        if($spec_name){
//            $map['key_name'] = array('like', "%$spec_name%");
//        }
//        $spec_sku = $_GET['spec_sku']?$_GET['spec_sku']:$_POST['spec_sku'];
//        if($spec_sku){
//
//            $this->assign('spec_sku',$spec_sku);
//            $spec_sku = urldecode($spec_sku);
//            $map['sku'] = $spec_sku;
//        }else{
//            $this->error('规格信息缺失');
//        }
//
//        $key_name = $_GET['key_name']?$_GET['key_name']:$_POST['key_name'];
//        if($key_name){
//
//            $this->assign('key_name',$key_name);
//            $key_name = urldecode($key_name);
//            $map['key_name'] = $key_name;
//        }else{
//            $this->error('规格信息缺失');
//        }
//
        // $map['is_share'] = 1;
//        $count = Db::name('spec_goods_price')->where($map)->group('sku,key_name,`key`')->count();
        $count = Db::name('spec_goods_price')->where($map)->group('sku,`key`')->count();
//        $Page = new Page($count, 20);
//        $show = $Page->show();
//        $this->assign('page', $show);// 赋值分页输出

        
        $stock_list = Db::name('SpecGoodsPrice')
            ->where($map)
            ->field('sku,any_value(key_name) as key_name,`key`,sum(store_count) as all_store_stock')
            //->group('sku,key_name,`key`')
            ->group('sku,`key`')
            ->select();
        
//        echo '<pre>';
//        var_dump($stock_list);die;
               
        
        $this->assign('stock_list', $stock_list);
       
        
        return $this->fetch();
    }
    
    public function updateGoodsStock(){
        
        $tmpl_id = input('tmpl_id');
        $sku = input('sku');
        $num = (int)input('num');
        $key = input('key');
    
        // $goods_ids = Db::name('goods')->where('tmpl_id',$tmpl_id)->where('is_on_sale',1)->where('is_deleted',0)->where('store_id',STORES)->where('store_id','in',STORES)->column('goods_id');
        # 201903 共享库存条件
        $goods_ids = Db::name('goods')->where('is_share_stock',1)->where('tmpl_id',$tmpl_id)->where('is_on_sale',1)->where('is_deleted',0)->where('store_id',STORES)->where('store_id','in',STORES)->column('goods_id');

        
//        $goods_spec_price_list = Db::name('SpecGoodsPrice')->where('sku',$sku)->where('goods_id','in',$goods_ids)->select();
        //$first = Db::name('SpecGoodsPrice')->where('sku',$sku)->where('key',$key)->where('goods_id','in',$goods_ids)->find();
        // $first = Db::name('SpecGoodsPrice')->where('is_share',1)->where('sku',$sku)->where('key',$key)->where('goods_id','in',$goods_ids)->find();

        # 201903 取消规格中共享库存条件
        $first = Db::name('SpecGoodsPrice')->where('sku',$sku)->where('key',$key)->where('goods_id','in',$goods_ids)->find();

        
        //var_dump($first);exit;
        if(!$first){
            exit(json_encode(array('status'=>0,'msg'=>'批量操作失败：没有数据')));
        }

        # 201903
        $other_goods_id = Db::name('goods')->where('is_share_stock',1)->where('tmpl_id',$tmpl_id)->where('is_on_sale',1)->where('is_deleted',0)->where('goods_id','<>',$first['goods_id'])->where('seller_id',SELLER_ID)->column('goods_id');
        Db::name('SpecGoodsPrice')->where('goods_id','in',$other_goods_id)->update(['store_count'=>0]);

        //$r = Db::name('SpecGoodsPrice')->where('item_id',$first['item_id'])->where('sku',$sku)->update(['store_count'=>$num]);
        $r = Db::name('SpecGoodsPrice')->where('item_id',$first['item_id'])->update(['store_count'=>$num]);
        
//
//        $item_id = I('item_id/d');
//        $store_count = I('store_count/d');
//        $old_stock = I('old_stock');
//        $spec_goods = Db::name('spec_goods_price')->alias('s')->field('s.*,g.goods_name')->join('__GOODS__ g', 'g.goods_id = s.goods_id', 'LEFT')->where(['s.item_id'=>$item_id])->find();
//        $r = M('spec_goods_price')->where(array('item_id'=>$item_id))->save(array('store_count'=>$store_count));
        if($r!==false){
            
            trace('修改共享库存'.$tmpl_id.'seller_id:'.SELLER_ID);
            exit(json_encode(array('status'=>1,'msg'=>'修改成功')));
        }else{
            
            exit(json_encode(array('status'=>0,'msg'=>'修改失败')));
        }
    }
    
    
//    public function stock_list(){
//
//
//
//        if(I('store_id')){
//            $map['store_id'] = I('store_id');
//        }else{
//            $map['store_id'] = array('in',STORES);
//        }
//
//
//    	$goods_name = I('goods_name');
//    	$spec_name = I('spec_name');
//    	if ($goods_name) {
//    		$map['goods_name'] = array('like', "%$goods_name%");
//    		$gids = M('goods')->where($map)->getField('goods_id',true);
//            unset($map['goods_name']);
//    		if($gids){
//    			$map['goods_id'] = array('in',$gids);
//    		}
//    	}
//    	if($spec_name){
//    		$map['key_name'] = array('like', "%$spec_name%");
//    	}
//        $spec_sku = $_GET['spec_sku']?$_GET['spec_sku']:$_POST['spec_sku'];
//        if($spec_sku){
//
//            $this->assign('spec_sku',$spec_sku);
//            $spec_sku = urldecode($spec_sku);
//            $map['sku'] = $spec_sku;
//        }else{
//            $this->error('规格信息缺失');
//        }
//
//        $key_name = $_GET['key_name']?$_GET['key_name']:$_POST['key_name'];
//        if($key_name){
//
//            $this->assign('key_name',$key_name);
//            $key_name = urldecode($key_name);
//            $map['key_name'] = $key_name;
//        }else{
//            $this->error('规格信息缺失');
//        }
//
//
//    	$count = Db::name('spec_goods_price')->where($map)->count();
//    	$Page = new Page($count, 20);
//    	$show = $Page->show();
//    	$this->assign('page', $show);// 赋值分页输出
//        $SpecGoodsPrice = new SpecGoodsPrice();
//    	$stock_list = $SpecGoodsPrice->where($map)->order('item_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
//
//
//        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
//        $this->assign('stores',$stores);
//
//        foreach ($stock_list as &$stock){
//            foreach ($stores as $store){
//                if($stock['store_id']==$store['store_id']){
//                    $stock['store_name'] = $store['store_name'];
//                }
//            }
//
//        }
//
//
//
//    	$this->assign('stock_list', $stock_list);
//    	if($stock_list){
//            $stock_list = collection($stock_list)->toArray();
//    		$goodsIds = get_arr_column($stock_list, 'goods_id');
//    		$goods = M('goods')->where(array('goods_id'=>array('in',$goodsIds)))->getField('goods_id,goods_name');
//    		$this->assign('goods',$goods);
//    	}
//
//
//
//
//    	return $this->fetch();
//    }
    
//    public function updateGoodsStock(){
//
//
//    	$item_id = I('item_id/d');
//    	$store_count = I('store_count/d');
//    	$old_stock = I('old_stock');
//    	$spec_goods = Db::name('spec_goods_price')->alias('s')->field('s.*,g.goods_name')->join('__GOODS__ g', 'g.goods_id = s.goods_id', 'LEFT')->where(['s.item_id'=>$item_id])->find();
//    	$r = M('spec_goods_price')->where(array('item_id'=>$item_id))->save(array('store_count'=>$store_count));
//    	if($r){
//    		$stock = $store_count - $old_stock;
//    		$goodsInfo = Db::name('goods')->where('goods_id',$spec_goods['goods_id'])->find();
//    		$goods = array('goods_id'=>$spec_goods['goods_id'],'goods_name'=>$spec_goods['goods_name'],'key_name'=>$spec_goods['key_name'],'store_id'=>$goodsInfo['store_id']);
//    		update_stock_log($goodsInfo['store_id'], $stock,$goods);
//    		exit(json_encode(array('status'=>1,'msg'=>'修改成功')));
//    	}else{
//
//    		exit(json_encode(array('status'=>0,'msg'=>'修改失败')));
//    	}
//    }

    /**
     *
     */
    public function addStepOne(){
        //限制发布商品数量，0为不限制
        //不限制数量
//        $alreadyPushNum = Db::name('goods')->where(['store_id' => STORE_ID])->count();
//        $sgGoodsLimit = Db::name('store_grade')->where(['sg_id' => $this->storeInfo['grade_id']])->getField('sg_goods_limit');
//        if($alreadyPushNum >= $sgGoodsLimit && $sgGoodsLimit > 0 && $this->storeInfo['is_own_shop'] !=1){
//            $this->error("可发布商品数量已达到上限", U('Goods/goodsList'));
//        }
        $goods_id = input('goods_id');
        if($goods_id){
            $goods = Db::name('goods')->where('goods_id',$goods_id)->find();
            $this->assign('goods',$goods);
        }
        $GoodsCategoryLogic = new GoodsCategoryLogic();
//        $GoodsCategoryLogic->setStore($this->storeInfo);
        $goodsCategoryLevelOne = $GoodsCategoryLogic->getStoreGoodsCategory();
        $this->assign('goodsCategoryLevelOne',$goodsCategoryLevelOne);
        return $this->fetch();
    }

    /**
     * 添加修改商品
     */
    public function addEditGoods()
    {
    
        cacheIsBack();
    
       
        $this->assign('seller_id',SELLER_ID);
        $goods_id = I('goods_id/d', 0);
        $goods_cat_id3 = I('cat_id3/d', 0);
        if(empty($goods_id)){

            $this->assign('isadd',1);
            if(empty($goods_cat_id3)){
                $this->error("您选择的分类不存在，或没有选择到最后一级，请重新选择分类。", U('Goods/addStepOne'));
            }
            $goods_cat[0] = Db::name('goods_category')->where('id', $goods_cat_id3)->find();
            $goods_cat[1] = Db::name('goods_category')->where('id', $goods_cat[0]['parent_id'])->find();
            $goods_cat[2] = Db::name('goods_category')->where('id', $goods_cat[1]['parent_id'])->find();
        }else{
            $this->assign('isadd',0);
            $Goods = new GoodsModel();
            $goods_info = $Goods->where(['goods_id' => $goods_id])->find();
            if(empty($goods_info)){
                $this->error("非法操作", U('Goods/goodsList'));
            }else{

                if($goods_info['seller_id']!=SELLER_ID){
                    $this->error("该商品不属于您", U('Goods/goodsList'));
                }

                if($goods_info['discount_start']){
                    $goods_info['discount_start'] = date('Y-m-d',$goods_info['discount_start']);
                }
                if($goods_info['discount_end']){
                    $goods_info['discount_end'] = date('Y-m-d',$goods_info['discount_end']);
                }
                $this->assign('goodsInfo', $goods_info);  // 商品详情
            }
            $goods_cat = Db::name('goods_category')->where('id','IN',[$goods_info['cat_id1'],$goods_info['cat_id2'],$goods_info['cat_id3']])->order('level desc')->select();
        }

        //版式
//        $plate_1=M('store_plate')->where('plate_position=1 and store_id='.STORE_ID)->field('plate_id,plate_name')->select();
//        $plate_0=M('store_plate')->where('plate_position=0 and store_id='.STORE_ID)->field('plate_id,plate_name')->select();
//        $this->assign('plate_1',$plate_1);
//        $this->assign('plate_0',$plate_0);
        
        //店铺列表
        $sotres = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();
        $this->assign('stores',$sotres);
        
        $GoodsLogic = new GoodsLogic();
        $store_goods_class_list = Db::name('store_goods_class')->where(['parent_id' => 0, 'store_id' => STORE_ID])->select(); //店铺内部分类
        $brandList = $GoodsLogic->getSortBrands();
        $goodsType = Db::name("GoodsType")->select();
        $suppliersList = Db::name("suppliers")->where(['is_check'=>1,'store_id'=>STORE_ID])->select();
        $goodsImages = Db::name("GoodsImages")->where('goods_id', $goods_id)->select();
        $freight_template = Db::name('freight_template')->where(['store_id' => STORE_ID])->select();
        $this->assign('freight_template',$freight_template);
        $this->assign('goods_cat', $goods_cat);

        # 201903 是否是非产品
        $isf = M('goods')->where('goods_id',$goods_info['tmpl_id'])->getField('tmpl_id');
        $this->assign('isf',$isf);
        
//        $this->assign('store_id', STORE_ID);
        $this->assign('store_goods_class_list', $store_goods_class_list);
        $this->assign('brandList', $brandList);
        $this->assign('goodsType', $goodsType);
        $this->assign('suppliersList', $suppliersList);
        $this->assign('goodsImages', $goodsImages);  // 商品相册
        return $this->fetch('_goods');
    }

    /**
     * 商品保存
     */
    public function save(){

        // 数据验证
        $data =input('post.');
/*        echo "<pre>";
        print_r($data);exit();*/
        
        $goods_id = input('post.goods_id');
        $goods_cat_id3 = input('post.cat_id3');
        $spec_goods_item = input('post.item/a',[]);
        $store_count = input('post.store_count');
        $is_virtual = input('post.is_virtual');
        $virtual_indate = I('post.virtual_indate');//虚拟商品有效期
        $exchange_integral = I('post.exchange_integral');//虚拟商品有效期
        $validate = Loader::validate('Goods');
        
        if (!$validate->batch()->check($data)) {
            $error = $validate->getError();
            $error_msg = array_values($error);
            $return_arr = array('status' => -1,'msg' => $error_msg[0],'data' => $error);
            $this->ajaxReturn($return_arr);
        }
        $data['on_time'] = time(); // 上架时间
        $type_id = M('goods_category')->where("id", $goods_cat_id3)->getField('type_id'); // 找到这个分类对应的type_id
        $data['goods_state'] = 1; // 出售中
        //无需审查
//        $stores = M('store')->where(array('store_id' => STORE_ID))->getField('store_id , goods_examine,is_own_shop' , 1);
//        $store_goods_examine = $stores[STORE_ID]['goods_examine'];
//        if ($store_goods_examine) {
//            $data['goods_state'] = 0; // 待审核
//            $data['is_on_sale'] = 0; // 下架
//        } else {
//
//        }
        //总平台自营标识为2 , 第三方自营店标识为1
        $is_own_shop = 1;//(STORE_ID == 1) ? 2 : ($stores[STORE_ID]['is_own_shop']);
        $data['is_own_shop'] = $is_own_shop;
        $data['goods_type'] = $type_id ? $type_id : 0;
        $data['virtual_indate'] = !empty($virtual_indate) ? strtotime($virtual_indate) : 0;
        $data['exchange_integral'] = ($is_virtual == 1) ? 0 : $exchange_integral;
        //序列化保存手机端商品描述数据
//        if ($_POST['m_body'] != '') {
//        	$_POST['m_body'] = str_replace('&quot;', '"', $_POST['m_body']);
//        	$_POST['m_body'] = json_decode($_POST['m_body'], true);
//        	if (!empty($_POST['m_body'])) {
//        		$_POST['m_body'] = serialize($_POST['m_body']);
//        	} else {
//        		$_POST['m_body'] = '';
//        	}
//        }
//        $data['mobile_content'] = $_POST['m_body'];
        
       
        if(isset($data['discount'])&&$data['discount']!=''){

            if($data['discount']<0||$data['discount']>100){
                $this->ajaxReturn(array('status' => 0, 'msg' => '折扣数值只能填写0-99','result'=>''));
            }
    
            if(!$data['discount_start']||!$data['discount_end']){
                $this->ajaxReturn(array('status' => 0, 'msg' => '折扣的起止时间必填','result'=>''));
            }
            
            $data['discount_start'] = strtotime($data['discount_start'].' 00:00:00');
            $data['discount_end'] = strtotime($data['discount_end'].' 23:59:59');
    
            if($data['discount_start']>$data['discount_end']){
                $this->ajaxReturn(array('status' => 0, 'msg' => '结束时间应该大于起始时间','result'=>''));
            }
            
//            if($data['discount']==0){
//                $data['discount'] = null;
//                $data['discount_start'] = null;
//                $data['discount_end'] = null;
//                $data['discount_limit'] = null;
//            }else{
//
//                if(!$data['discount_start']||!$data['discount_end']){
//                    $this->ajaxReturn(array('status' => 0, 'msg' => '折扣的起止时间必填','result'=>''));
//                }
//                $data['discount_start'] = strtotime($data['discount_start']);
//                $data['discount_end'] = strtotime($data['discount_end']);
//
//                if($data['discount_start']>$data['discount_end']){
//                    $this->ajaxReturn(array('status' => 0, 'msg' => '结束时间应该大于起始时间','result'=>''));
//                }
//
//            }
            

            
        }else{
            $data['discount'] = null;
            $data['discount_start'] = null;
            $data['discount_end'] = null;
            $data['discount_limit'] = null;
        }
        
        
        $cate = Db::name('GoodsCategory')->where('id',$goods_cat_id3)->find();
        
        //修改商品，所以无需搞什么
        if ($goods_id > 0) {
            
            
            $Goods = GoodsModel::get(['goods_id' => $goods_id]);
            $sellerId = $Goods['store_id'];
            if(empty($Goods)){
                $this->ajaxReturn(array('status' => 0, 'msg' => '非法操作','result'=>''));
            }
            if (empty($spec_goods_item) && $store_count != $Goods['store_count']) {
                $real_store_count = $store_count - $Goods['store_count'];
                update_stock_log(session('admin_id'), $real_store_count, array('goods_id' => $goods_id, 'goods_name' => $Goods['goods_name'], 'store_id' => $Goods['store_id']));
            } else {
                unset($data['store_count']);
            }
            $Goods->data($data, true); // 收集数据
            $update = $Goods->save(); // 写入数据到数据库
            // 更新成功后删除缩略图
            if($update !== false){
                // 修改商品后购物车的商品价格也修改一下
                Db::name('cart')->where("goods_id", $goods_id)->where("spec_key = ''")->save(array(
                    'market_price' => $Goods['market_price'], //市场价
                    'goods_price' => $Goods['shop_price'], // 本店价
                    'member_goods_price' => $Goods['shop_price'], // 会员折扣价
                ));
                delFile("./public/upload/goods/thumb/$goods_id", true);
            }
    
            $GoodsLogic = new GoodsLogic();
            $r1 = $GoodsLogic->afterSave($goods_id,$sellerId);
            $r2 = $GoodsLogic->saveGoodsAttr($goods_id, $type_id); // 处理商品 属性
            
            
            if($r1&&$r2){
                $this->ajaxReturn([ 'status' => 1, 'msg' => '操作成功', 'result' => ['goods_id'=>$Goods->goods_id]]);
            }else{
                $this->ajaxReturn([ 'status' => 0, 'msg' => '操作失败', 'result' => ['goods_id'=>$Goods->goods_id]]);
            }
            
            
        } else {
            //新商品，需要同时发布到多个店铺的。
            
            $stores = I('store/a',null);
            
            
            if(!$stores||!is_array($stores)){
                $this->ajaxReturn([ 'status' => 0, 'msg' => '至少选择一个店铺', 'result' => null]);
            }
            
            $store_list = Db::name('store')->where('store_id','in',$stores)->select();
            $GoodsLogic = new GoodsLogic();
            # 201903
            $Goods = new GoodsModel();
            foreach ($store_list as $store){

                $storeId = $store['store_id'];

                //如果是模板店铺，就标记为模板商品
                if($store['is_tmpl']){

                }else{
                    # 201903 非主产品 -1 标志
//                    $data['tmpl_id'] =null;
                    $data['tmpl_id'] = -1;
                    $data['tmpl_sn'] = $notNo = $this->create_tmpl_sn($cate, 'n');
                    $data['store_id'] = $storeId;
                    $Goods->data($data, true); // 收集数据
                    $Goods->save(); // 新增数据到数据库
                    $no_goods_id = $Goods->getLastInsID();

                    $GoodsLogic->afterSave($no_goods_id, $storeId);
                    $GoodsLogic->saveGoodsAttr($no_goods_id, $type_id); // 处理商品 属性
                    break;
                }
            }

            foreach ($store_list as $store){
                
                $storeId = $store['store_id'];
                
                //如果是模板店铺，就标记为模板商品
                if($store['is_tmpl']){
                    $data['tmpl_id'] =0;
                    //还要设置模板序号
                    $data['tmpl_sn'] = $this->create_tmpl_sn($cate);
                }else{
                    // $data['tmpl_id'] =null;
                    # 201903
                    if($no_goods_id){
                        $data['tmpl_id'] = $no_goods_id;
                        $data['tmpl_sn'] = $notNo;
                    }
                }
                
                
                
                $data['store_id'] = $storeId;
                $Goods = new GoodsModel();
                $Goods->data($data, true); // 收集数据
                $Goods->save(); // 新增数据到数据库
                $goods_id = $Goods->getLastInsID();
                
                
                //商品进出库记录日志
                if (empty($spec_goods_item)) {
                    update_stock_log(session('admin_id'), $store_count, array('goods_id' => $goods_id, 'goods_name' => $Goods['goods_name'], 'store_id' => $storeId));
                }
                
                $GoodsLogic->afterSave($goods_id, $storeId);
                $GoodsLogic->saveGoodsAttr($goods_id, $type_id); // 处理商品 属性
            }
            
            
            $this->ajaxReturn([ 'status' => 1, 'msg' => '操作成功', 'result' => ['goods_id'=>$Goods->goods_id]]);
        }
        
    }
    
    
    /**
     * 五位随机不够0填充
     * @param $cate
     * @return string
     */

    protected function create_tmpl_sn($cate,$n = ''){
        
        //如果么有设置，那就用xx代理
        
        if(empty($cate['first_name'])){
            $first_name = 'xx';
        }else{
            $first_name = $cate['first_name'];
        }
        if($n == 'n'){
            $isHasTmplSn = Db::name('goods')->where('tmpl_sn','like','F'.$first_name.'%')->column('tmpl_sn');
        }else{
            $isHasTmplSn = Db::name('goods')->where('tmpl_sn','like',$first_name.'%')->column('tmpl_sn');
        }

        if(!empty($isHasTmplSn)){
            foreach ($isHasTmplSn as $tmpl_sn){
                if($n=='n'){
                    $hasTmplSnArr[] = (int)substr($tmpl_sn,2,4);
                }else{
                    $hasTmplSnArr[] = (int)substr($tmpl_sn,1,5);
                }
            }
            sort($hasTmplSnArr);
            $num = end($hasTmplSnArr);
        }else{
            $num = 0;
        }
        
        // $newTmplSn = sprintf('%05s', ($num+1));
        if($n == 'n'){
            // 非主产品模板编号
            $newTmplSn = sprintf('%04s', ($num+1));
            $newTmplSn = 'F'.$first_name.$newTmplSn;
        }else{
            $newTmplSn = sprintf('%05s', ($num+1));
            $newTmplSn = $first_name.$newTmplSn;
        }
        // $newTmplSn = $first_name.$newTmplSn;
        return $newTmplSn;
    }

    // protected function create_tmpl_sn($cate){
        
    //     //如果么有设置，那就用xx代理
        
    //     if(empty($cate['first_name'])){
    //         $first_name = 'xx';
    //     }else{
    //         $first_name = $cate['first_name'];
    //     }
        
    //     $isHasTmplSn = Db::name('goods')->where('tmpl_sn','like',$first_name.'%')->column('tmpl_sn');
        
    //     if(!empty($isHasTmplSn)){
    //         foreach ($isHasTmplSn as $tmpl_sn){
    //             $hasTmplSnArr[] = (int)substr($tmpl_sn,1,5);
    //         }
    //         sort($hasTmplSnArr);
    //         $num = end($hasTmplSnArr);
    //     }else{
    //         $num = 0;
    //     }
        
    //     $newTmplSn = sprintf('%05s', ($num+1));
    //     $newTmplSn = $first_name.$newTmplSn;
       
    //     return $newTmplSn;
    // }




    public function changeSpecItemSort(){
        $str = I("str",null);
        if(!$str){
            $this->ajaxReturn([ 'status' => 0, 'msg' => '参数错误', 'result' =>null]);
        }

        $arr = explode(',',$str);

        if(count($arr)<1){
            $this->ajaxReturn([ 'status' => 0, 'msg' => '参数错误2', 'result' =>null]);
        }

        foreach ($arr as $item){
            $temp = explode('-',$item);
            if(!$temp){
                $this->ajaxReturn([ 'status' => 0, 'msg' => '参数错误3', 'result' =>null]);
            }
            $rt = Db::name('SpecItem')->where('id',$temp[0])->update(['sort'=>$temp[1]]);
        }


        $this->ajaxReturn([ 'status' => 1, 'msg' => '更改spec排序成功', 'result' =>null]);



    }
    
    public function changeSpecColor(){
        
        $rule = [
            'goods_id'=>'require',
            'spec_color'=>'require',
            'spec_color_id'=>'require'
        ];
        $params = $this->request->param();
        $check = $this->validate($params,$rule);
        if (true !== $check) {
            $this->ajaxReturn([ 'status' => 0, 'msg' => '参数缺失', 'result' =>$check]);
        }
        
        $rt = Db::name('SpecColor')->where(['goods_id'=>$params['goods_id'],'spec_color_id'=>$params['spec_color_id']])->update(['spec_color'=>$params['spec_color']]);
        if($rt!==false){
            $this->ajaxReturn([ 'status' => 1, 'msg' => '更改颜色成功', 'result' =>null]);
        }else{
            $this->ajaxReturn([ 'status' => 0, 'msg' => '更改颜色失败', 'result' =>null]);
        }
        
       
        
        
        
    }
    

    /**
     * 更改指定表的指定字段
     */
    public function updateField()
    {
        $primary = array(
            'goods' => 'goods_id',
            'goods_attribute' => 'attr_id',
            'ad' => 'ad_id',
        );
        $id = I('id/d', 0);
        $field = I('field');
        $value = I('value');
        Db::name($_POST['table'])->where($primary[$_POST['table']], $id)->where('store_id', STORE_ID)->save(array($field => $value));
        $return_arr = array(
            'status' => 1,
            'msg' => '操作成功',
            'data' => array('url' => U('Goods/goodsAttributeList')),
        );
        $this->ajaxReturn($return_arr);
    }

    /**
     * 动态获取商品属性输入框 根据不同的数据返回不同的输入框类型
     */
    public function ajaxGetAttrInput()
    {
        $cat_id3 = I('cat_id3/d', 0);
        $goods_id = I('goods_id/d', 0);
        
        empty($cat_id3) && exit('');
        $type_id = M('goods_category')->where("id", $cat_id3)->getField('type_id'); // 找到这个分类对应的type_id
        
        empty($type_id) && exit('');
        
        $GoodsLogic = new GoodsLogic();
        $str = $GoodsLogic->getAttrInput($goods_id, $type_id);
        
        exit($str);
    }

    /**
     * 删除商品
     */
    public function delGoods()
    {
        $ids= I('ids');
        $GoodsLogic = new GoodsLogic();
        $res = $GoodsLogic->delStoreGoods($ids);
        $this->ajaxReturn($res);
    }

    /**
     * ajax 获取 品牌列表
     */
    public function getBrandByCat()
    {
        $db_prefix = C('database.prefix');
        $type_id = I('type_id/d');
        if ($type_id) {
//            $list = M('brand')->join("left join {$db_prefix}brand_type on {$db_prefix}brand.id = {$db_prefix}brand_type.brand_id and  type_id = $type_id")->order('id')->select();
            $list = Db::name('brand')->alias('b')->join('__BRAND_TYPE__ t', 't.brand_id = b.id', 'LEFT')->where(['t.type_id' => $type_id])->order('b.id')->select();
        } else {
            $list = M('brand')->order('id')->select();
        }
//        $goods_category_list = M('goods_category')->where("id in(select cat_id1 from {$db_prefix}brand) ")->getField("id,name,parent_id");
        $goods_category_list = Db::name('goods_category')
            ->where('id', 'IN', function ($query) {
                $query->name('brand')->where('')->field('cat_id1');
            })
            ->getField("id,name,parent_id");
        $goods_category_list[0] = array('id' => 0, 'name' => '默认');
        asort($goods_category_list);
        $this->assign('goods_category_list', $goods_category_list);
        $this->assign('type_id', $type_id);
        $this->assign('list', $list);
        return $this->fetch();
    }


    /**
     * ajax 获取 规格列表
     */
    public function getSpecByCat()
    {

        $db_prefix = C('database.prefix');
        $type_id = I('type_id/d');
        if ($type_id) {
//            $list = M('spec')->join("left join {$db_prefix}spec_type on {$db_prefix}spec.id = {$db_prefix}spec_type.spec_id  and  type_id = $type_id")->order('id')->select();
            $list = Db::name('spec')->alias('s')->join('__SPEC_TYPE__ t', 't.spec_id = s.id', 'LEFT')->where(['t.type_id' => $type_id])->order('s.id')->select();
        } else {
            $list = M('spec')->order('id')->select();
        }
//        $goods_category_list = M('goods_category')->where("id in(select cat_id1 from {$db_prefix}spec) ")->getField("id,name,parent_id");
        $goods_category_list = Db::name('goods_category')
            ->where('id', 'IN', function ($query) {
                $query->name('spec')->where('')->field('cat_id1');
            })
            ->getField("id,name,parent_id");
        $goods_category_list[0] = array('id' => 0, 'name' => '默认');
        asort($goods_category_list);
        $this->assign('goods_category_list', $goods_category_list);
        $this->assign('type_id', $type_id);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 动态获取商品规格选择框 根据不同的数据返回不同的选择框
     * 用户拖拽也要更新这里的顺序才行，然后读取的时候按照排序来
     */
    public function ajaxGetSpecSelect()
    {
        $goods_id = I('goods_id/d', 0);
        $cat_id3 = I('cat_id3/d', 0);
        
        
        empty($cat_id3) && exit('');
        $goods_id = $goods_id ? $goods_id : 0;

        $type_id = M('goods_category')->where("id", $cat_id3)->getField('type_id'); // 找到这个分类对应的type_id
        empty($type_id) && exit('');
        $spec_id_arr = M('spec_type')->where("type_id", $type_id)->getField('spec_id', true); // 找出这个类型的 所有 规格id

        empty($spec_id_arr) && exit('');
    

        $specList = D('Spec')->where("id", "in", implode(',', $spec_id_arr))->order('`order` desc')->select(); // 找出这个类型的所有规格
        if ($specList) {
            foreach ($specList as $k => $v) {
                //'store_id' => STORE_ID,
                //只能拿自己的
                $specList[$k]['spec_item'] = D('SpecItem')->where(['spec_id' => $v['id'],'seller_id'=>SELLER_ID])->order('sort asc')->getField('id,item'); // 获取规格项
            }
        }


        $items_id = M('SpecGoodsPrice')->where("goods_id", $goods_id)->getField("GROUP_CONCAT(`key` SEPARATOR '_') AS items_id");
        $items_ids = explode('_', $items_id);

        // 获取商品规格图片                
        if ($goods_id) {
            $specImageList = M('SpecImage')->where("goods_id", $goods_id)->getField('spec_image_id,src');
            $specColorList = M('SpecColor')->where("goods_id", $goods_id)->getField('spec_color_id,spec_color');
//            $specColorList = Db::name('SpecColor')->where("goods_id", $goods_id)->select();
//            var_dump($specColorList);die;
        }

        # 201903 是否共享库存
        $isShare = M('goods')->where("goods_id", $goods_id)->field('is_share_stock,tmpl_id')->find();
        $isf = M('goods')->where('goods_id',$isShare['tmpl_id'])->getField('tmpl_id');
        $this->assign('isShare',$isShare['is_share_stock']);
        $this->assign('isf',$isf);

        
        $this->assign('specImageList', $specImageList);
        $this->assign('specColorList',$specColorList);
        $this->assign('items_ids', $items_ids);
        $this->assign('specList', $specList);
        return $this->fetch('ajax_spec_select');
    }

    /**
     * 动态获取商品规格输入框 根据不同的数据返回不同的输入框
     */
    public function ajaxGetSpecInput()
    {
        $GoodsLogic = new GoodsLogic();
        $goods_id = I('get.goods_id/d', 0);
        $spec_arr = I('spec_arr/a', []);
        
        $str = $GoodsLogic->getSpecInput($goods_id, $spec_arr, SELLER_ID);
        $this->ajaxReturn(['status'=>1,'msg'=>'','result'=>$str]);
    }

    /**
     * 商家发布商品时添加的规格
     */
    public function addSpecItem()
    {
        $spec_id = I('spec_id/d', 0); // 规格id
        $spec_item = I('spec_item', '', 'trim');// 规格项

        $c = M('spec_item')->where(['seller_id' => SELLER_ID ,'item' => $spec_item, 'spec_id' => $spec_id])->count();
        if ($c > 0) {
            $return_arr = array(
                'status' => -1,
                'msg' => '规格已经存在',
                'data' => '',
            );
            exit(json_encode($return_arr));
        }
        $data = array(
            'spec_id' => $spec_id,
            'item' => $spec_item,
            'seller_id' => SELLER_ID
        );
        M('spec_item')->add($data);

        $return_arr = array(
            'status' => 1,
            'msg' => '添加成功!',
            'data' => '',
        );
        exit(json_encode($return_arr));
    }

    /**
     * 商家发布商品时删除的规格
     */
    public function delSpecItem()
    {
        $spec_id = I('spec_id/d', 0); // 规格id 14
        $spec_item = I('spec_item', '', 'trim');// 规格项 云雾灰色
        $spec_item_id = I('spec_item_id/d', 0); //规格项 id

        if (!empty($spec_item_id)) {
            $id = $spec_item_id;
        } else {
            $id = M('spec_item')->where(['seller_id' => SELLER_ID, 'item' => $spec_item, 'spec_id' => $spec_id])->getField('id');
        }

        if (empty($id)) {
            $return_arr = array('status' => -1, 'msg' => '规格不存在');
            exit(json_encode($return_arr));
        }


        //改成了经销商模式，所以这里是in
        $c = M("SpecGoodsPrice")->where("store_id",'in', STORES)->where(" `key` REGEXP :id1 OR `key` REGEXP :id2 OR `key` REGEXP :id3 or `key` = :id4")->bind(['id1' => '^' . $id . '_', 'id2' => '_' . $id . '_', 'id3' => '_' . $id . '$', 'id4' => $id])->count(); // 其他商品用到这个规格不得删除
        if ($c) {
            $return_arr = array('status' => -1, 'msg' => '此规格其他商品使用中,不得删除');
            exit(json_encode($return_arr));
        }

        M('spec_item')->where(['id' => $id, 'seller_id' => SELLER_ID])->delete(); // 删除规格项
        M('spec_image')->where(['spec_image_id' => $id])->where("store_id",'in', STORES)->delete(); // 删除规格图片选项
        $return_arr = array('status' => 1, 'msg' => '删除成功!');
        exit(json_encode($return_arr));
    }

    /**
     * 商品规格列表
     */
    public function specList()
    {
        $GoodsCategoryLogic = new GoodsCategoryLogic();
        $GoodsCategoryLogic->setStore($this->storeInfo);
        $goodsCategoryLevelOne = $GoodsCategoryLogic->getStoreGoodsCategory();
        $this->assign('cat_list', $goodsCategoryLevelOne);
        return $this->fetch();
    }

    /**
     *  商品规格列表
     */
    public function ajaxSpecList()
    {
        //ob_start('ob_gzhandler'); // 页面压缩输出
        $cat_id3 = I('cat_id3/d', 0);
        $spec_id = I('spec_id/d', 0);
        $type_id = M('goods_category')->where("id", $cat_id3)->getField('type_id'); // 获取这个分类对应的类型
        if (empty($cat_id3) || empty($type_id)) exit('');

        $spec_id_arr = M('spec_type')->where("type_id", $type_id)->getField('spec_id', true); // 获取这个类型所拥有的规格
        if (empty($spec_id_arr)) exit('');

        $spec_id = $spec_id ? $spec_id : $spec_id_arr[0]; //没有传值则使用第一个

        $specList = M('spec')->where("id", "in", implode(',', $spec_id_arr))->getField('id,name,cat_id1,cat_id2,cat_id3');
        $specItemList = M('spec_item')->where(['store_id' => STORE_ID, 'spec_id' => $spec_id])->order('id')->select(); // 获取这个类型所拥有的规格
        
        //I('cat_id1')   && $where = "$where and cat_id1 = ".I('cat_id1') ;                       
        $this->assign('spec_id', $spec_id);
        $this->assign('specList', $specList);
        $this->assign('specItemList', $specItemList);
        return $this->fetch();
    }

    /**
     *  批量添加修改规格
     */
    public function batchAddSpecItem()
    {
        $spec_id = I('spec_id/d', 0);
        $item = I('item/a');
        $spec_item = M('spec_item')->where(['store_id' => STORE_ID, 'spec_id' => $spec_id])->getField('id,item');
        foreach ($item as $k => $v) {
            $v = trim($v);
            if (empty($v)) continue; // 值不存在 则跳过不处理
            // 如果spec_id 存在 并且 值不相等 说明值被改动过
            if (array_key_exists($k, $spec_item) && $v != $spec_item[$k]) {
                M('spec_item')->where(['id' => $k, 'store_id' => STORE_ID])->save(array('item' => $v));
                // 如果这个key不存在 并且规格项也不存在 说明 需要插入
            } elseif (!array_key_exists($k, $spec_item) && !in_array($v, $spec_item)) {
                M('spec_item')->add(array('spec_id' => $spec_id, 'item' => $v, 'store_id' => STORE_ID));
            }
        }
        $this->ajaxReturn(['status'=>1,'msg'=>'保存成功','result'=>'']);
    }

    /**
     * 品牌列表
     */
    public function brandList()
    {
        $keyword = I('keyword');
        $brand_where['store_id'] = STORE_ID;
        if ($keyword) {
            $brand_where['name'] = ['like', '%' . $keyword . '%'];
        }
        $count = Db::name('brand')->where($brand_where)->count();
        $Page = new Page($count, 16);
        $brandList = Db::name('brand')->where($brand_where)->order("`sort` asc")->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $show = $Page->show();
        $cat_list = M('goods_category')->where("parent_id = 0")->getField('id,name'); // 已经改成联动菜单
        $this->assign('cat_list', $cat_list);
        $this->assign('show', $show);
        $this->assign('brandList', $brandList);
        return $this->fetch('brandList');
    }

    /**
     * 添加修改编辑  商品品牌
     */
    public function addEditBrand()
    {
        $id = I('id/d', 0);
        if (IS_POST) {
            $data = input('post.');
            $brandVilidate = Loader::validate('Brand');
            if (!$brandVilidate->batch()->check($data)){
                $error_msg = '';
                foreach ($brandVilidate->getError() as $key =>$value){
                    $error_msg .= $value.'，';
                }
                $this->ajaxReturn(['status'=>-1,'msg'=>$error_msg]);
            }
            if ($id) {
                Db::name('brand')->update($data);
            } else {
                $data['store_id'] = STORE_ID;
                M("Brand")->insert($data);
            }
            $this->ajaxReturn(['status'=>1,'msg'=>'操作成功！！','url'=>U('Goods/brandList')]);
        }
        $GoodsCategoryLogic = new GoodsCategoryLogic();
        $GoodsCategoryLogic->setStore($this->storeInfo);
        $goodsCategoryLevelOne = $GoodsCategoryLogic->getStoreGoodsCategory();
        $this->assign('cat_list', $goodsCategoryLevelOne);
        $brand = Db::name('brand')->where(array('id' => $id, 'store_id' => STORE_ID))->find();
        $this->assign('brand', $brand);
        return $this->fetch('_brand');
    }

    /**
     * 删除品牌
     */
    public function delBrand()
    {
        $model = M("Brand");
        $id = I('id/d');
        $model->where(['id' => $id, 'store_id' => STORE_ID])->delete();
        $return_arr = array('status' => 1, 'msg' => '操作成功', 'data' => '',);
        $this->ajaxReturn($return_arr);
    }

    public function brand_save()
    {
        $data = I('post.');
        if ($data['act'] == 'del') {
            $goods_count = M('Goods')->where("brand_id", $data['id'])->count('1');
            if ($goods_count) respose(array('status' => -1, 'msg' => '此品牌有商品在用不得删除!'));
            $r = M('brand')->where('id', $data['id'])->delete();
            if ($r) {
                respose(array('status' => 1));
            } else {
                respose(array('status' => -1, 'msg' => '操作失败'));
            }
        } else {
            if (empty($data['id'])) {
                $data['store_id'] = STORE_ID;
                $r = M('brand')->add($data);
            } else {
                $r = M('brand')->where('id', $data['id'])->save($data);
            }
        }
        if ($r) {
            $this->success("操作成功", U('Store/brand_list'));
        } else {
            $this->error("操作失败", U('Store/brand_list'));
        }
    }

    /**
     * 删除商品相册图
     */
    public function del_goods_images()
    {
        $path = I('filename', '');
        $goods_images = M('goods_images')->where(array('image_url' => $path))->select();
        foreach ($goods_images as $key => $val) {
            $goods = M('goods')->where(array('goods_id' => $goods_images[$key]['goods_id']))->find();
            if ($goods['store_id'] == STORE_ID) {
                M('goods_images')->where(array('img_id' => $goods_images[$key]['img_id']))->delete();
            }
        }
    }

    /**
     * 重新申请商品审核
     */
    public function goodsUpLine()
    {
        $goods_ids = input('goods_ids');
        $res = Db::name('goods')->where('goods_id', 'in', $goods_ids)->where('store_id', STORE_ID)->update(['is_on_sale' => 0, 'goods_state' => 0]);
        if($res !== false){
            $this->success('操作成功');
        }else{
            $this->success('操作失败');
        }

    }
    
    public function pic_list(){
    	$path = UPLOAD_PATH.'store/'.session('store_id').'/goods';
    	$listSize = 14;
    	$key = empty($_GET['key']) ? '' : $_GET['key'];
    	/* 获取参数 */
    	$size = isset($_GET['size']) ? htmlspecialchars($_GET['size']) : $listSize;
    	$page = isset($_GET['p']) ? htmlspecialchars($_GET['p']) : 1;
    	$start = ($page-1)*$size;
    	$end = $start + $size;
    	/* 获取文件列表 */
    	$allowFiles = 'png|jpg|jpeg|gif|bmp';
    	$files = $this->getfiles($path, $allowFiles, $key);
    	if (!count($files)) {
    		$this->assign('result',array());
    	}else{
    		/* 获取指定范围的列表 */
    		$len = count($files);
    		for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--){
    			$list[] = $files[$i];
    		}
    		/* 返回数据 */
    		$Page = new Page($len, $size);
    		$show = $Page->show();
    		$this->assign('show_page', $show);
    		$this->assign('result',$list);
    	}
    	return $this->fetch();
    }
    
    /**
     * 遍历获取目录下的指定类型的文件
     * @param $path
     * @param array $files
     * @return array
     */
    private function getfiles($path, $allowFiles, $key, &$files = array()){
    	if (!is_dir($path)) return null;
    	if(substr($path, strlen($path) - 1) != '/') $path .= '/';
    	$handle = opendir($path);
    	while (false !== ($file = readdir($handle))) {
    		if ($file != '.' && $file != '..') {
    			$path2 = $path . $file;
    			if (is_dir($path2)) {
    				$this->getfiles($path2, $allowFiles, $key, $files);
    			} else {
    				if (preg_match("/\.(".$allowFiles.")$/i", $file) && preg_match("/.*". $key .".*/i", $file)) {
    					$files[] = array(
    							'url'=> '/'.$path2,
    							'name'=> $file,
    							'mtime'=> filemtime($path2)
    					);
    				}
    			}
    		}
    	}
    	return $files;
    }

    /**
     * 获取商品分类
     */
    public function getSellerBindGategory()
    {
        $parent_id = I('parent_id/d', '0'); // 商品分类 父id
        $rank = I('rank'); // 商品分类 父id
        $next_class = I('next_class'); // 商品分类 父id
        empty($parent_id) && $this->ajaxReturn(['status'=>-1,'msg'=>'获取失败','data'=>'']);
        $where=[
            'store_id'  =>  STORE_ID,
            "$rank"     =>  $parent_id,
            'state'     =>  1
        ];
        $bind_class_arr = Db::name('store_bind_class')->where($where)->group("$next_class")->getField("$next_class",true);
        $bind_class_ids = implode(',',$bind_class_arr);
        $list = Db::name('goods_category')->whereIn('id',$bind_class_ids)->select();
        if ($list){
            $this->ajaxReturn(['status'=>1,'msg'=>'获取成功','data'=>$list]);
        }
        $this->ajaxReturn(['status'=>-1,'msg'=>'获取失败','data'=>'']);
    }


    /**
     * 删除菜单
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function del()
    {


        $ids = I('id');
        $GoodsLogic = new GoodsLogic();
        $res = $GoodsLogic->delStoreGoods($ids);
        $this->ajaxReturn($res);

    }


    /**
     * 菜单禁用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function forbid()
    {

        $params = $this->request->post();
        try {

            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => null]);
            }
            return $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => null]);
        } catch (Exception $e) {

            return $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => $e->getMessage()]);

        }
    }

    /**
     * 启用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function resume()
    {
        $params = $this->request->post();
        try {

            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => null]);
            }
            return $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => null]);
        } catch (Exception $e) {

            return $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => $e->getMessage()]);
        }


    }


    /**
     * 删除
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function changeColumn(&$dbQuery, $where = [])
    {
        $db = is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery;

        list($pk, $table, $map) = [$db->getPk(), $db->getTable(), []];
        list($field, $value) = [$this->request->post('field', ''), $this->request->post('value', '')];;

        $map = [empty($pk) ? 'id' : $pk => array('in', explode(',', $this->request->post('id', '')))];


        // 删除模式，如果存在 is_deleted 字段使用软删除
        if ($field === 'delete') {
            if (method_exists($db, 'getTableFields') && in_array('is_deleted', $db->getTableFields())) {


                return $db->where($where)->where($map)->update(['is_deleted' => '1']) !== false;
            }

            return $db->where($where)->where($map)->delete() !== false;
        }

        // 更新模式，更新指定字段内容
        return $db->where($where)->where($map)->update([$field => $value]) !== false;

    }

    // 201903 批量更新商品相册
    protected function updateGoodsImg($goods_id){
        $goods_images = I('goods_images/a');
        $img_sorts = I('img_sorts/a');
        if(count($goods_images) > 1)
        {

            array_pop($goods_images); // 弹出最后一个
            $goodsImagesArr = M('GoodsImages')->where("goods_id = $goods_id")->getField('img_id,image_url'); // 查出所有已经存在的图片

            // 删除图片
            foreach($goodsImagesArr as $key => $val)
            {
                if(!in_array($val, $goods_images)){
                    M('GoodsImages')->where("img_id = {$key}")->delete();

                    //同时删除物理文件
                    $filename = $val;
                    $filename= str_replace('../','',$filename);
                    $filename= trim($filename,'.');
                    $filename= trim($filename,'/');
                    $is_exists = file_exists($filename);

                    //同时删除物理文件
                    if($is_exists){
                        unlink($filename);
                    }
                }
            }
            $goodsImagesArrRever = array_flip($goodsImagesArr);
            // 添加图片
            foreach($goods_images as $key => $val)
            {
                $sort = $img_sorts[$key];
                if($val == null)  continue;
                if(!in_array($val, $goodsImagesArr))
                {
                    $data = array( 'goods_id' => $goods_id,'image_url' => $val , 'img_sort'=>$sort);
                    M("GoodsImages")->insert($data); // 实例化User对象
                }else{
                    $img_id = $goodsImagesArrRever[$val];
                    //修改图片顺序
                    M('GoodsImages')->where("img_id = {$img_id}")->save(array('img_sort' => $sort));
                }
            }
        }
        return true;
    }

    // 201903 批量更改商品规格
    protected function updateGoodsSpec($goods_id, $store_id){

        $item_img = I('item_img/a');
        $item_color= I('item_color/a');

        $goodsSepcLogList = Db::name('spec_goods_price')->where('goods_id',$goods_id)->field('item_id,goods_id,key,key_name')->select();
        // 商品规格价钱处理
        $goods_item = I('item/a');
//        var_dump($goods_item,$goods_id);
        if ($goods_item) {
            //删掉所有的，重新排序
            Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->delete();

            $tempArr = Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->select();

            $store_count = 0;
            $keyArr = '';//规格key数组
            foreach ($goods_item as $k => $v) {


                //如果库存价格还有sku都是null，就删掉。注意不是0.00.
                if($v['price']==''&&$v['store_count']==''&&$v['sku']==''){
                    continue;//如果价格或者商品为0，则不保存
                }
                //批量添加数据
                $v['price'] = trim($v['price']);

                $v['market_price'] = trim($v['market_price']);

                //当其他两个值有一个为真时.库存的默认值为0
                if(($v['price']&&$v['price']!='')||($v['store_count']&&$v['store_count']!='')){
                    $v['store_count'] = $v['store_count']?$v['store_count']:0;
                }


                $store_count += $v['store_count']; // 记录商品总库存
                $v['sku'] = trim($v['sku']);

                //var_dump($v);die;

                $keyArr .= $k.',';
                $data = array('goods_id' => $goods_id, 'key' => $k, 'key_name' => $v['key_name'], 'price' => $v['price'], 'store_count' => $v['store_count'], 'sku' => $v['sku'], 'store_id' => $store_id,'market_price'=>$v['market_price'],'is_share'=>$v['is_share']);
                $specGoodsPrice = Db::name('spec_goods_price')->where(['goods_id' => $data['goods_id'], 'key' => $data['key']])->find();
                if ($item_img) {
                    $spec_key_arr = explode('_', $k);
                    foreach ($item_img as $key => $val) {
                        if (in_array($key, $spec_key_arr)) {
                            $data['spec_img'] = $val;
                            break;
                        }
                    }
                }

                if($specGoodsPrice){
                    Db::name('spec_goods_price')->where(['goods_id' => $goods_id, 'key' => $k])->update($data);
                    $stock = $specGoodsPrice['store_count'] - $data['store_count'];
                    if($stock != 0){
//                        update_stock_log(session('seller_id'), $stock,array('goods_id'=>$goods_id,'key_name'=>$data['key_name'],'goods_name'=>I('goods_name'),'store_id'=>$store_id));
                    }
                }else{

                    Db::name('spec_goods_price')->insert($data);
//                    update_stock_log(session('seller_id'), $data['store_count'],array('goods_id'=>$data['goods_id'],'key_name'=>$data['key_name'],'goods_name'=>I('goods_name'),'store_id'=>$store_id));
                }
            }

            if($keyArr){
//                ->whereOr(['store_count'=>0,'price'=>0])
                //同时删掉价格或者库存为0
                \think\log::info('商品价格未删除前记录'.json_encode($goodsSepcLogList));
                Db::name('spec_goods_price')->where('goods_id',$goods_id)->whereNotIn('key',$keyArr)->delete();
            }
        }else{
            \think\log::info('商品价格未删除前记录'.json_encode($goodsSepcLogList));
            Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->delete();
        }

        //统一去掉所有的

        // 商品规格图片处理
        if($item_img)
        {
            M('SpecImage')->where("goods_id = $goods_id")->delete(); // 把原来是删除再重新插入
            foreach ($item_img as $key => $val)
            {
                M('SpecImage')->insert(array('goods_id'=>$goods_id ,'spec_image_id'=>$key,'src'=>$val,'store_id'=>$store_id));
            }
        }

        //已经可以单独修改，就不需要再保存一次了.
        //如果是新增的规格或者新增的商品，那不就尴尬了。
        if($item_color)
        {
            M('SpecColor')->where("goods_id = $goods_id")->delete(); // 把原来是删除再重新插入
            foreach ($item_color as $key => $val)
            {
                M('SpecColor')->insert(array('goods_id'=>$goods_id ,'spec_color_id'=>$key,'spec_color'=>$val,'store_id'=>$store_id));
            }
        }
        refresh_stock($goods_id); // 刷新商品库存

        return true;
    }

    // 201903 批量更改商品属性
    protected function updateGoodsAttr($goods_id, $goods_type)
    {
        $GoodsAttr = M('GoodsAttr');
        //$Goods = M("Goods");

        // 属性类型被更改了 就先删除以前的属性类型 或者没有属性 则删除
        if ($goods_type == 0) {
            $GoodsAttr->where('goods_id', $goods_id)->delete();
            return true;
        }

        $GoodsAttrList = $GoodsAttr->where('goods_id', $goods_id)->select();

        $old_goods_attr = array(); // 数据库中的的属性  以 attr_id _ 和值的 组合为键名
        foreach ($GoodsAttrList as $k => $v) {
            $old_goods_attr[$v['attr_id'] . '_' . $v['attr_value']] = $v;
        }

        // post 提交的属性  以 attr_id _ 和值的 组合为键名
        $post_goods_attr = array();
        foreach ($_POST as $k => $v) {
            $attr_id = str_replace('attr_', '', $k);
            if (!strstr($k, 'attr_') || strstr($k, 'attr_price_'))
                continue;
            foreach ($v as $k2 => $v2) {
                $v2 = str_replace('_', '', $v2); // 替换特殊字符
                $v2 = str_replace('@', '', $v2); // 替换特殊字符
                $v2 = trim($v2);

                if (empty($v2))
                    continue;


                $tmp_key = $attr_id . "_" . $v2;
                $attr_price = $_POST["attr_price_$attr_id"][$k2];
                $attr_price = $attr_price ? $attr_price : 0;
                if (array_key_exists($tmp_key, $old_goods_attr)) // 如果这个属性 原来就存在
                {
                    if ($old_goods_attr[$tmp_key]['attr_price'] != $attr_price) // 并且价格不一样 就做更新处理
                    {
                        $goods_attr_id = $old_goods_attr[$tmp_key]['goods_attr_id'];
                        $GoodsAttr->where("goods_attr_id", $goods_attr_id)->save(array('attr_price' => $attr_price));
                    }
                } else // 否则这个属性 数据库中不存在 说明要做删除操作
                {
                    $GoodsAttr->add(array('goods_id' => $goods_id, 'attr_id' => $attr_id, 'attr_value' => $v2, 'attr_price' => $attr_price));
                }
                unset($old_goods_attr[$tmp_key]);
            }

        }
        //file_put_contents("b.html", print_r($post_goods_attr,true));
        // 没有被 unset($old_goods_attr[$tmp_key]); 掉是 说明 数据库中存在 表单中没有提交过来则要删除操作

        foreach ($old_goods_attr as $k => $v) {
            $GoodsAttr->where('goods_attr_id', $v['goods_attr_id'])->delete(); //
        }


        return true;

    }
    
    
}