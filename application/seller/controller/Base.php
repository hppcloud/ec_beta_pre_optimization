<?php
namespace app\seller\controller;

use app\admin\logic\UpgradeLogic;
use think\Controller;
use think\Db;
use think\Session;

class Base extends Controller
{
    public $storeInfo = array();
    public $store = array();
    public $seller = array();
    public $sellerInfo = array();
    protected $allowAction = [];
    public $begin;
    public $end;
    public $select_year;


    /**
     * 析构函数
     */
    function __construct()
    {
        Session::start();
        header("Cache-control: private");
        parent::__construct();
        $upgradeLogic = new UpgradeLogic();
        $upgradeMsg = $upgradeLogic->checkVersion(); //升级包消息        
        $this->assign('upgradeMsg', $upgradeMsg);
        //用户中心面包屑导航
        $seller = session('seller');
        tpversion();
        $this->assign('seller', $seller);
    }

    /*
     * 初始化操作
     */
    public function _initialize()
    {
        $this->assign('action', ACTION_NAME);
        //过滤不需要登陆的行为
        if (in_array(ACTION_NAME, array('login', 'logout', 'vertify','repairSpecItem','delRepeatSpecItem'))||in_array(ACTION_NAME, $this->allowAction)) {
            //return;
        } else {
           

            if (session('seller_id') > 0) {
//                define('STORE_ID', session('store_id')); //将当前的session_id保存为常量，供其它方法调用


                //灵魂操作,全局变量SELELR_ID不变，即使是子账号登录，也要用is_admin=1的经销商父账号来。因为所有的业务都需要用到。只不过权限校验的时候用的是session（seller）里面的值，是各个子账号的真实seller_id。
                $seller = Db::name('seller')->where('seller_id', session('seller_id'))->find();


                $this->seller = $seller;


                if($seller['is_admin']==1){
                    define('SELLER_ID', session('seller_id')); //将当前的session_id保存为常量，供其它方法调用
                }else{
                    if(!$seller['seller_pid']){

                        session::clear();
                        $this->error('该子账户没有绑定父经销商账号，请绑定.', U('Admin/login'), 1);
                    }
                    define('SELLER_ID', $seller['seller_pid']); //将当前子账户的父账户seller_id，作为常量，供其它方法调用
                }


                //全局保护该商户下的所有店铺数组,转成字符串吧毕竟
                $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');
                define('STORES',$stores);
//                $store = M('store')->where(array('store_id' => STORE_ID))->find();
//                $store_grade = Db::name('store_grade')->where(['sg_id'=>$store['grade_id']])->find();
//                $store['grade_name'] = $store_grade['sg_name'];
//                $store['sg_act_limits'] = $store_grade['sg_act_limits'];
//                $this->storeInfo = $store;
//                if($store['store_state'] == 0 && CONTROLLER_NAME != 'Index')
//                    $this->error('店铺已关闭', U('Index/index'), 1);

                $isCheckRole = $this->check_seller_priv(session('seller'));//检查管理员菜单操作权限
                if(!$isCheckRole){
                    $this->error('权限不足');
                    die;

                }


                $menu = include APP_PATH . 'seller/conf/menu.php';
                $menuArr = $menu;
//                if($store_grade && $store_grade['sg_act_limits']){
//                    $right = Db::name('system_menu')->where("id", "in", $store_grade['sg_act_limits'])->cache(true)->getField('right', true);
//                    $role_right = '';
//                    if (count($right) > 0) {
//                        foreach ($right as $val) {
//                            $role_right .= $val . ',';
//                        }
//                    }
//                    $role_right = explode(',', $role_right);
//                    foreach($menu as $menuKey=>$menuVal){
//                        $childArr = [];
//                        foreach($menuVal['child'] as $childKey => $childVal){
//                            $op_act = $childVal['op'].'@'.$childVal['act'];
//                            if (in_array($op_act, $role_right)) {
//                                array_push($childArr, $childVal);
//                            }
//                        }
//                        if(count($childArr) == 0){
//                            unset($menuArr[$menuKey]);
//                        }else{
//                            $menuArr[$menuKey]['child'] = $childArr;
//                        }
//                    }
//                }
                $this->assign('menuArr', $menuArr);//所有菜单
                $this->assign('leftMenu', get_left_menu($menuArr));//左侧导航菜单

//                if(is_array($_SESSION['seller_quicklink'])){
//                    $this->assign('quicklink',array_keys($_SESSION['seller_quicklink']));//快捷操作菜单
//                }
//                $store['full_address'] = getRegionName($store['province_id']) . ' ' . getRegionName($store['city_id']) . ' ' . getRegionName($store['district']);
//                $storeMsgNoReadCount = Db::name('store_msg')->where(['store_id'=>STORE_ID,'open'=>0])->count();
//                $this->assign('storeMsgNoReadCount', $storeMsgNoReadCount);
                //$this->store = $store;
                //$this->assign('store', $store);
                $this->assign('seller',$this->seller);
                
                
            } else {
                 (ACTION_NAME == 'index') && $this->redirect( U('Seller/Admin/login'));
                $this->error('请先登录', U('Admin/login'), 1);
            }
        }
        $this->public_assign();
    }

    /**
     * 保存公告变量到 smarty中 比如 导航
     */
    public function public_assign()
    {
        $tpshop_config = array();
        $tp_config = M('config')->cache(true)->select();
        foreach ($tp_config as $k => $v) {
            $tpshop_config[$v['inc_type'] . '_' . $v['name']] = $v['value'];
        }
        if(I('start_time')&&I('start_time')!=''){
            $begin = I('start_time');
            $end = I('end_time');
        }else{
            $begin = date('Y-m-d H:i:s', strtotime("-1 month"));//30天前
            $end = date('Y-m-d H:i:s', strtotime('+1 days'));
        }
        $this->assign('start_time',$begin);
        $this->assign('end_time',$end);
        $this->select_year = getTabByTime(I('start_time')); // 表后缀
        $this->begin = strtotime($begin);
        $this->end = strtotime($end)+86399;
        $this->assign('tpshop_config', $tpshop_config);
    }

    /**
     * @param $seller
     * @desc 校验子经销商账户权限
     */
//    is_admin=0的才限制
//    并且该seller变量包含
// $seller['act_limits'] = $group['act_limits'];
//$seller['smt_limits'] = $group['smt_limits'];
    public function check_seller_priv($seller){


        $ctl = request()->controller();
        $act = request()->action();
        $uneed_check = array('login', 'logout', 'vertifyHandle', 'vertify', 'imageUp','delupload','videoUp','upload', 'login_task', 'modify_pwd','index');//修改密码不需要验证权限
        if ($seller['is_admin'] == 0) {

            $act_list = $seller['act_limits'];
            //无需验证的操作
            if ($ctl == 'Index' || $act_list == 'all') {
                //后台首页控制器无需验证,超级管理员无需验证
                return true;
            }elseif(request()->isAjax() || strpos($act,'ajax')!== false || in_array($act,$uneed_check)){
                //所有ajax请求不需要验证权限，所有添加的豁免的控制器也不需要
                return true;
            } else {
//                ->cache(true)
                $right = Db::name('system_menu')->where("id", "in", $act_list)->getField('right', true);
                $role_right = '';
                if (count($right) > 0) {
                    foreach ($right as $val) {
                        $role_right .= $val . ',';
                    }
                }
                $role_right = explode(',', $role_right);
                //检查是否拥有此操作权限
                if (!in_array($ctl.'@'.$act, $role_right)) {
                    $this->error('您没有操作权限'.($ctl.'@'.$act).',请联店铺超级管理员分配权限', U('Index/index'));
                }
            }
        }

        if($ctl == 'Index' || request()->isAjax() || strpos($act,'ajax')!== false || in_array($act,$uneed_check)){
            return true;
        }

        return true;

    }
    public function check_priv($store_grade)
    {
        $seller = session('seller');
        $ctl = request()->controller();
        $act = request()->action();
        $uneed_check = array('login', 'logout', 'vertifyHandle', 'vertify', 'imageUp','delupload','videoUp','upload', 'login_task', 'modify_pwd','index');//修改密码不需要验证权限
        if ($seller['is_admin'] == 0) {

            $act_list = $seller['act_limits'];
            //无需验证的操作
            if ($ctl == 'Index' || $act_list == 'all') {
                //后台首页控制器无需验证,超级管理员无需验证
                return true;
            }elseif(request()->isAjax() || strpos($act,'ajax')!== false || in_array($act,$uneed_check)){
                //所有ajax请求不需要验证权限
                return true;
            } else {
                $right = Db::name('system_menu')->where("id", "in", $act_list)->cache(true)->getField('right', true);
                $role_right = '';
                if (count($right) > 0) {
                    foreach ($right as $val) {
                        $role_right .= $val . ',';
                    }
                }
                $role_right = explode(',', $role_right);
                //检查是否拥有此操作权限
                if (!in_array($ctl.'@'.$act, $role_right)) {
                    $this->error('您没有操作权限'.($ctl.'@'.$act).',请联店铺超级管理员分配权限', U('Index/index'));
                }
            }
        }
        if($ctl == 'Index' || request()->isAjax() || strpos($act,'ajax')!== false || in_array($act,$uneed_check)){
            return true;
        }
        if($store_grade && $store_grade['sg_act_limits']){
            $right = Db::name('system_menu')->where("id", "in", $store_grade['sg_act_limits'])->cache(true)->getField('right', true);
            $role_right = '';
            if (count($right) > 0) {
                foreach ($right as $val) {
                    $role_right .= $val . ',';
                }
            }
            $role_right = explode(',', $role_right);
            //检查是否拥有此操作权限
            if (!in_array($ctl.'@'.$act, $role_right)) {
                $this->error('店铺等级没有操作权限'.($ctl.'@'.$act).',请联系平台管理员分配权限', U('Index/index'));
            }
        }
        return true;
    }

    public function ajaxReturn($data, $type = 'json')
    {
        exit(json_encode($data));
    }
    
    
    
    /**
     * 删除菜单
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function del()
    {
        
        $params = $this->request->post();
        try{
            
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'删除成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'删除失败','result'=>null]);
        }catch (Exception $e){
            
            return $this->ajaxReturn(['status'=>0,'msg'=>'删除失败','result'=>$e->getMessage()]);
            
        }
    }
    
    /**
     * 菜单禁用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function forbid()
    {
        
        $params = $this->request->post();
        try{
            
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'操作成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'操作失败','result'=>null]);
        }catch (Exception $e){
            
            return $this->ajaxReturn(['status'=>0,'msg'=>'操作失败','result'=>$e->getMessage()]);
            
        }
    }
    
    /**
     * 启用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function resume()
    {
        $params = $this->request->post();
        try{
            
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'操作成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'操作失败','result'=>null]);
        }catch (Exception $e){
            
            return $this->ajaxReturn(['status'=>0,'msg'=>'操作失败','result'=>$e->getMessage()]);
        }
        
        
    }
    
    
    /**
     * 删除
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function changeColumn(&$dbQuery,$where = [])
    {
        $db = is_string($dbQuery) ? Db::name($dbQuery) : $dbQuery;
        
        list($pk, $table, $map) = [$db->getPk(), $db->getTable(), []];
        list($field, $value) = [$this->request->post('field', ''), $this->request->post('value', '')];;
        
        $map = [empty($pk) ? 'id' : $pk=>array('in', explode(',', $this->request->post('id', '')))];
        
       
        // 删除模式，如果存在 is_deleted 字段使用软删除
        if ($field === 'delete') {
            if (method_exists($db, 'getTableFields') && in_array('is_deleted', $db->getTableFields())) {
               
                
                return $db->where($where)->where($map)->update(['is_deleted' => '1']) !== false;
            }
            
            return $db->where($where)->where($map)->delete() !== false;
        }
        
        // 更新模式，更新指定字段内容
        return  $db->where($where)->where($map)->update([$field => $value]) !== false;
        
    }

}