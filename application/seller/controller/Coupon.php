<?php
namespace app\seller\controller;

use app\common\model\Store;
use think\AjaxPage;
use think\Db;
use think\Loader;
use think\Page;

class Coupon extends Base
{

    public function delStoreCoupon(){
        $rt = Db::name('SellerDiscount')->where('discount_id',input('id'))->delete();
        $rt2 = Db::name('SellerDiscountStore')->where('discount_id',input('id'))->delete();
        if($rt!==false&&$rt2!==false){
            $this->ajaxReturn(['status' => 1, 'msg' => '删除成功', 'result' => '']);
        }else{
            $this->ajaxReturn(['status' => 1, 'msg' => '删除失败', 'result' => '']);
        }
    }


    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 根据cid和type获取可以勾选和已经勾选中的店铺
     */
    public function getStoreList()
    {
        header("Content-type: text/html; charset=utf-8");

        $cid = I('id',null);
        $type = I('type',null);

        empty($type) && exit('');
        $return_arr = array(
            'status' => 1,
            'msg' => '操作成功',
            'data' => array('url' => U('Goods/goodsAttributeList')),
        );
        $query = Db::name('coupon')
            ->alias('c')
            ->join('tp_coupon_store s','c.id=s.cid')
            ->where('c.send_start_time','lt',time())
            ->where('c.send_end_time','gt',time())
            ->where("c.type", $type)->where('s.seller_id',SELLER_ID);


        $list = [];

        //获取经销商下并且有效的店铺
        $allStores = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();


        //已有商品的需要勾选check
        if($cid){
    
            $query = Db::name('coupon')
                ->alias('c')
                ->join('tp_coupon_store s','c.id=s.cid')
//                ->where('c.send_start_time','lt',time())
//                ->where('c.send_end_time','gt',time())
                ->where("c.type", $type)->where('s.seller_id',SELLER_ID);
            
            $isHasStores = $query->where('c.id',$cid)->field('c.*,s.store_id,s.seller_id')->select();
            // 找到这个分类对应的type_id
            $isHasStoreIds = get_arr_column($isHasStores,'store_id');
            //把已有的放进来
            foreach ($allStores as $store){
                if(in_array($store['store_id'],$isHasStoreIds)){
                    $store['check'] = 1;
                    $list[] = $store;
                }
            }

            //过滤已经设置了这种优惠券的店铺,过滤掉过期的优惠券
            $isHasStores2 = Db::name('coupon')
                ->alias('c')
                ->join('tp_coupon_store s','c.id=s.cid')
                ->where('c.send_start_time','lt',time())
                ->where('c.send_end_time','gt',time())
                ->where("c.type", $type)->where('s.seller_id',SELLER_ID)->where('c.id','neq',$cid)->field('c.*,s.store_id,s.seller_id')->select();
            //echo Db::name('coupon')->getLastSql();exit;
            $isHasStoreIds2 = get_arr_column($isHasStores2,'store_id');


            $isHasStoreIds = array_merge($isHasStoreIds,$isHasStoreIds2);

        }else{
            $isHasStores = $query->field('c.*,s.store_id,s.seller_id')->select();
            // 找到这个分类对应的type_id
            $isHasStoreIds = get_arr_column($isHasStores,'store_id');
        }

        //把还可以选择的放进来
        foreach ($allStores as $store){
            if(!in_array($store['store_id'],$isHasStoreIds)){
                $list[] = $store;
            }
        }


        $this->assign('stores',$list);
        return $this->fetch();
    }

    /**
     *  商品分类列表
     */
    public function storeList(){


        //只拿有效的
        //不能用seller_id来做了。因为store可能需要迁移，一迁移就尴尬了额
//        'seller_id'=>SELLER_ID
        $where = [];

        //优先按照store_id的下来框来，而且store_name也是独一无二的，所以可以用来检索。
        $store_id = I('store_id',null);
        $store_name = I("store_name",null);
        if($store_id){
            $discount_ids = Db::name('SellerDiscountStore')->where('store_id',$store_id)->column('discount_id');
            $this->assign('store_id',$store_id);
        }else{
            if($store_name){
                $store = Store::get(['store_name'=>$store_name]);
                if($store){
                    $discount_ids = Db::name('SellerDiscountStore')->where('store_id',$store['store_id'])->column('discount_id');
                }
            }
            $this->assign('store_name',$store_name);
        }
    
      

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        
        $this->assign('stores',$stores);
    
        $store_id_arr = get_arr_column($stores,'store_id');
        
        if(!isset($discount_ids)){
            $discount_ids = Db::name('SellerDiscountStore')->where('store_id','in',$store_id_arr)->column('discount_id');
        }
        
        $where['discount_id'] = array('in',$discount_ids);
        
        

        $cats =  Db::name('goods_category')->where('level',3)->order('sort_order desc')->select();


        $discountList = Db::name('seller_discount')->where($where)->order('store_id')->select();
        
        $discount_ids = get_arr_column($discountList,'discount_id');
        
        $sotre_list = Db::name('store')
            ->alias('s')
            ->join('tp_seller_discount_store sds','s.store_id=sds.store_id')
            ->where('sds.discount_id','in',$discount_ids)
            ->field('s.*,sds.discount_id')
            ->select();

        foreach ($discountList as &$discount){
            $discount['stores'] = [];

            if(isset($discount['discount_start'])){
                $discount['discount_start'] = date('Y-m-d',$discount['discount_start']);

            }

            if(isset($discount['discount_end'])){

                $discount['discount_end'] = date('Y-m-d',$discount['discount_end']);
            }

            //加上名字好区分
            foreach ($sotre_list as $store){
                if($discount['discount_id']==$store['discount_id']){
                    $discount['stores'][] = $store;
                }
            }

            foreach ($cats as $cat){
                if($discount['catid']==$cat['id']){
                    $discount['catname'] = $cat['name'];
                }
            }
        }

        $this->assign('lists',$discountList);

        return $this->fetch('storeList');
    }


    
    public function editStoreCoupon(){

        //获取storelist
        if($this->request->isPost()&&input('act')=='getStoreList'){
            
            
            $cid = I('id',null);
            $type = I('type',null);
    
            empty($type) && exit('');
            $return_arr = array(
                'status' => 1,
                'msg' => '操作成功',
                'data' => array('url' => U('Goods/goodsAttributeList')),
            );
            $query = Db::name('coupon')
                ->alias('c')
                ->join('tp_coupon_store s','c.id=s.cid')
                ->where("c.type", $type)->where('s.seller_id',SELLER_ID);
    
    
            $list = [];
    
            //获取经销商下并且有效的店铺
            $allStores = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();
    
    
            //已有商品的需要勾选check
            if($cid){
                $isHasStores = $query->where('c.id',$cid)->field('c.*,s.store_id,s.seller_id')->select();
                // 找到这个分类对应的type_id
                $isHasStoreIds = get_arr_column($isHasStores,'store_id');
                //把已有的放进来
                foreach ($allStores as $store){
                    if(in_array($store['store_id'],$isHasStoreIds)){
                        $store['check'] = 1;
                        $list[] = $store;
                    }
                }
        
            }else{
                $isHasStores = $query->field('c.*,s.store_id,s.seller_id')->select();
                // 找到这个分类对应的type_id
                $isHasStoreIds = get_arr_column($isHasStores,'store_id');
            }
    
            //把还可以选择的放进来
            foreach ($allStores as $store){
                if(!in_array($store['store_id'],$isHasStoreIds)){
                    $list[] = $store;
                }
            }
    
    
            $this->assign('stores',$list);
            
            return $this->fetch('getStoreList');
            
            die;
        }

        if(IS_GET){
            
            
            $id = I('id',null);

            $cats =  Db::name('goods_category')->where('level',3)->order('sort_order desc')->select();
            $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();


            //编辑
            if($id){
                $discount = Db::name('sellerDiscount')->where('discount_id',$id)->find();
                if(!$discount){
                    $this->error('数据有误');
                }
                if($discount['discount_start']){
                    $discount['discount_start'] = date('Y-m-d',$discount['discount_start']);
                }
                if($discount['discount_end']){
                    $discount['discount_end'] = date('Y-m-d',$discount['discount_end']);
                }
                
                
                $select_store_list  = Db::name('SellerDiscountStore')->where('discount_id',$id)->column('store_id');
    
                $discount['select_store_list'] = implode(',',$select_store_list);

                $this->assign('model',$discount);
                
            }else{
                //新增，要过滤有的类别不允许选择
                //只拿有效的
//                $where = ['seller_id'=>SELLER_ID,'store_id'=>array('neq',null)];
//                $discountList = Db::name('sellerDiscount')->where($where)->order('store_id')->select();
//                foreach ($cats as &$cat){
//                    foreach ($discountList as $discount){
//                        if($discount['catid']==$cat['id']){
//                            $cat['isHas'] = true;//标记已经有的
//                        }
//                    }
//
//                }
//
//                foreach ($stores as &$store){
//                    foreach ($discountList as $discount){
//                        if($discount['store_id']==$store['store_id']){
//                            $store['isHas'] = true;//标记已经有的
//                        }
//                    }
//
//                }


            }


            $this->assign('cats',$cats);


            $this->assign('stores',$stores);


            return $this->fetch('storeCoupon');
        }

        if(IS_POST){


            $data = [];
            //$data['store_id'] = I('store_id',null);
    
            $store_id_arr = I('store/a', array());
            if(count($store_id_arr)<1){
                $this->ajaxReturn(['status' => 0, 'msg' => '提交失败:至少选择一个店铺', 'result' => '']);
            }
            
            
            $data['catid'] = I('catid',null);
            $data['discount'] = I('discount',null);
            $data['discount_start'] = I('discount_start',null);
            $data['discount_end'] = I('discount_end',null);
            $data['discount_limit'] = I('discount_limit');
            //$data['seller_id'] = SELLER_ID;

            $data['discount_start'] = strtotime($data['discount_start'].' 00:00:00');
            $data['discount_end'] = strtotime($data['discount_end'].' 23:59:59');


//            if(!isset($data['store_id'])||$data['store_id']=='null'){
//                $this->ajaxReturn(array('status' => 0, 'msg' => '店铺必选','result'=>''));
//            }

            if(!isset($data['catid'])||$data['catid']=='null'){
                $this->ajaxReturn(array('status' => 0, 'msg' => '分类信息必选','result'=>''));
            }


            if(isset($data['discount'])&&$data['discount']!=''){


                if($data['discount']<0||$data['discount']>100){
                    $this->ajaxReturn(array('status' => 0, 'msg' => '折扣数值只能填写大于等于0到小于等于100的数值','result'=>''));
                }

//                if($data['discount']==0||$data['discount']==100){
//                    $this->ajaxReturn(array('status' => 0, 'msg' => '折扣数值只能填写0-99','result'=>''));
//                    die;
//                }


                if(!$data['discount_start']||!$data['discount_end']){
                    $this->ajaxReturn(array('status' => 0, 'msg' => '折扣的起止时间必填','result'=>''));
                }

                if($data['discount_start']>$data['discount_end']){
                    $this->ajaxReturn(array('status' => 0, 'msg' => '结束时间应该大于起始时间','result'=>''));
                }

            }else{
                $this->ajaxReturn(array('status' => 0, 'msg' => '折扣信息必填','result'=>''));
            }

            $discount_id = I('discount_id',null);

            //寻找交叉时间的
            $isHas = Db::name('SellerDiscount')
                ->alias('sd')
                ->join('tp_seller_discount_store sds','sd.discount_id=sds.discount_id')
                ->where(array('sd.catid'=>$data['catid'],'sd.discount'=>array('gt',0)))
                ->where('sds.store_id','in',$store_id_arr)
                ->where('sd.discount_start','between',array($data['discount_start'],$data['discount_end']))->order('sd.discount_id desc')
                ->field('sd.discount_id,sds.store_id')
                ->find();

            $isHas2 = Db::name('SellerDiscount')
                ->alias('sd')
                ->join('tp_seller_discount_store sds','sd.discount_id=sds.discount_id')
                ->where(array('sd.catid'=>$data['catid'],'sd.discount'=>array('gt',0)))
                ->where('sds.store_id','in',$store_id_arr)
                ->where('sd.discount_end','between',array($data['discount_start'],$data['discount_end']))->order('sd.discount_id desc')
                ->field('sd.discount_id,sds.store_id')
                ->find();
    
    
            //需要检查，时间段不能有同一个类别
            if($isHas&&$isHas['discount_id']!=$discount_id){
                $store_title = Db::name('store')->where('store_id',$isHas['store_id'])->value('store_name');
                $this->ajaxReturn(array(
                    'status' => 0,
                    'msg' => '操作失败:店铺'.$store_title.'['.$isHas['store_id'].']在指定时间内已经设置折扣',
                    'data' => null,
                ));
            }
            if($isHas2&&$isHas2['discount_id']!=$discount_id){
                $store_title = Db::name('store')->where('store_id',$isHas2['store_id'])->value('store_name');
                $this->ajaxReturn(array(
                    'status' => 0,
                    'msg' => '操作失败:店铺'.$store_title.'['.$isHas2['store_id'].']在指定时间内已经设置折扣',
                    'data' => null,
                ));
            }

            if($discount_id){



                $data['store_id'] = $store_id_arr[0];
                $id = Db::name('sellerDiscount')->where('discount_id',$discount_id)->update($data);
            }else{

                $data['store_id'] = $store_id_arr[0];
                $id = Db::name('sellerDiscount')->insertGetId($data);
    
                $discount_id = $id;

            }

            if($id!==false){
                
                //不管是新增还是更新，都要删掉所有绑定关系。然后新建
                Db::name('SellerDiscountStore')->where('discount_id',$discount_id)->delete();
                
                $post = [];
                foreach ($store_id_arr as $store_id){
                    $post[] = ['store_id'=>$store_id,'discount_id'=>$discount_id];
                }
                $rt = Db::name('SellerDiscountStore')->insertAll($post);
                
                
                if($rt){
                    $return_arr = array(
                        'status' => 1,
                        'msg' => '操作成功',
                        'data' => array('url' => U('Seller/coupon/storeList')),
                    );
                }else{
                    
                    $return_arr = array(
                        'status' => 0,
                        'msg' => '保存店铺和折扣绑定信息失败',
                        'data' => null,
                    );
                    
                }
                
            }else{
                $return_arr = array(
                    'status' => 0,
                    'msg' => '操作失败',
                    'data' => null,
                );
            }

            $this->ajaxReturn($return_arr);


        }

    }

    /**
     * 删除分类
     */
    public function delGoodsCategory()
    {
        // 判断子分类
        $GoodsCategory = M("GoodsCategory");
        $count = $GoodsCategory->where("parent_id", $_GET['id'])->count("id");
        $count > 0 && $this->error('该分类下还有分类不得删除!', U('Admin/Goods/categoryList'));
        // 判断是否存在商品
        $goods_count = M('Goods')->where("cat_id", $_GET['id'])->count('1');
        $goods_count > 0 && $this->error('该分类下有商品不得删除!', U('Admin/Goods/categoryList'));
        // 删除分类
        $GoodsCategory->where("id", $_GET['id'])->delete();
        $this->success("操作成功!!!", U('Admin/Goods/categoryList'));
    }


    /**
     * 优惠券类型列表
     */
    public function index()
    {
        //获取优惠券列表
        //
        $where =   [
            "seller_id" =>SELLER_ID,
        ];


        if(I('store_id',null)){
            $where['store_id'] = I('store_id',null);
        }

        if(I('type',null)!=null){
            $where['type'] = I('type');
        }
        $count = M('coupon')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $lists = M('coupon')->where($where)->order('add_time desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);

        foreach ($lists as &$item){
            if($item['type']==4&&$item['add_time']<1534003200){
                unset($item);
                continue;
            }
            foreach ($stores as $store){
                if($store['store_id']==$item['store_id']){
                    $item['store_name'] = $store['store_name'];
                }
            }
        }
//        var_dump($lists);die;

        $this->assign('lists', $lists);
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('coupons', C('COUPON_TYPE'));

        $this->assign('coupon_user_arr',C('COUPON_USE_TYPE'));
        return $this->fetch();
    }

    /**
     * 添加编辑一个优惠券类型
     */
    public function coupon_info()
    {

        $this->assign('isadd',1);
        
        if (IS_POST) {

            $data = I('post.');
            
           
            if ($data['type']>0){
                $data['send_start_time'] = strtotime($data['send_start_time'].' 00:00:00');
                $data['send_end_time'] = strtotime($data['send_end_time'].' 23:59:59');
            }else{
                $data['send_start_time'] = '';
                $data['send_end_time'] = '';
            }
//            $data['store_name'] = Db::name('store')->where('id',$data['store_id'])->column('store_name');
            $data['use_end_time'] = strtotime($data['use_end_time'].' 23:59:59');
            $data['use_start_time'] = strtotime($data['use_start_time'].' 00:00:00');

            if(!empty($data['use_start_time']) && !empty($data['use_end_time']) && $data['use_start_time'] > $data['use_end_time']){
                $this->ajaxReturn(['status' => 0, 'msg' => '优惠券使用结束时间要大于开始时间', 'result' => '']);
                exit;
            }
            if(!empty($data['send_start_time']) && !empty($data['send_end_time']) && $data['send_start_time'] > $data['send_end_time']){
                $this->ajaxReturn(['status' => 0, 'msg' => '优惠券发放结束时间要大于开始时间', 'result' => '']);
                exit;
            }
    
    
            $type = input('type', null);
            $money = input('money', null);
            $percent = input('percent/d', null);
    
            
           
            if (empty($data['id'])) {


                //只有新增才做校验
                $couponValidate = Loader::validate('Coupon');
                if (!$couponValidate->batch()->check($data)) {
                    $this->ajaxReturn([
                        'status' => 0, 'msg' => '操作失败',
                        'result' => $couponValidate->getError(),
                        'token'    => \think\Request::instance()->token()
                    ]);
                }
                
                
                
                


                


//                var_dump($data);
//                die;

                if ($type == 4 && !$money) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '注册赠送优惠券必须设置金额', 'result' => '']);
                }


                if ($type == 5 && (!$percent || $percent < 1)) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '回收赠送优惠券必须设置百分比', 'result' => '']);
                }
                


                $store_id_arr = I('store/a');
                if(count($store_id_arr)<1){
                    $this->ajaxReturn(['status' => 0, 'msg' => '提交失败:至少选择一个店铺', 'result' => '']);
                }


//                //寻找交叉时间的
                $isHas =  Db::name('coupon')
                    ->alias('c')
                    ->join('tp_coupon_store s','c.id=s.cid')
                    ->where('c.send_start_time','between',array($data['send_start_time'],$data['send_end_time']))
                    ->where('c.store_id','in',$store_id_arr)
                    ->where("c.type", $type)->where('s.seller_id',SELLER_ID)->field('c.id,s.store_id')->find();
    
                $isHas2 =  Db::name('coupon')
                    ->alias('c')
                    ->join('tp_coupon_store s','c.id=s.cid')
                    ->where('c.send_end_time','between',array($data['send_start_time'],$data['send_end_time']))
                    ->where('c.store_id','in',$store_id_arr)
                    ->where("c.type", $type)->where('s.seller_id',SELLER_ID)->field('c.id,s.store_id')->find();
    
    
                //需要检查，时间段不能有同一个类别
                if($isHas&&$isHas['id']!=$data['id']){
                    $store_title = Db::name('store')->where('store_id',$isHas['store_id'])->value('store_name');
                    $this->ajaxReturn(array(
                        'status' => 0,
                        'msg' => '操作失败:店铺'.$store_title.'['.$isHas['store_id'].']在指定时间内已经存在优惠券',
                        'data' => null,
                    ));
                }
    
                if($isHas2&&$isHas2['id']!=$data['id']){
                    $store_title = Db::name('store')->where('store_id',$isHas2['store_id'])->value('store_name');
                    $this->ajaxReturn(array(
                        'status' => 0,
                        'msg' => '操作失败:店铺'.$store_title.'['.$isHas2['store_id'].']在指定时间内已经存在优惠券',
                        'data' => null,
                    ));
                }
                
                
                
                
                //店铺通用
                if($data['use_type'] == 3){

//                    $stores = I('store/a');
//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选', 'result' => '']);
//                    }

                    $data['add_time'] = time();
                    $data['seller_id'] = SELLER_ID;
                    $newCid = Db::name('coupon')->insertGetId($data);

//                    $newCid = Db::name('goods_coupon')->insertGetId(['coupon_id'=>$row,'seller_id'=>SELLER_ID]);
                    $temp = [];
                    foreach ($store_id_arr as $store_id){
                        $temp[]=['store_id'=>$store_id,'cid'=>$newCid,'seller_id'=>SELLER_ID];
                    }
                    if(count($temp)>0){
                        Db::name('CouponStore')->insertAll($temp);
                    }

                }
                
//                if($data['use_type'] == 1){
//
//                    $data['add_time'] = time();
//                    $data['seller_id'] = SELLER_ID;
//                    $row = Db::name('coupon')->insertGetId($data);
//
//                    //指定商品
//                    foreach ($data['goods_id'] as $v) {
//                        Db::name('goods_coupon')->add(['coupon_id'=>$row,'goods_id'=>$v,'seller_id'=>SELLER_ID]);
//                    }
//
//                }
    
                //指定类别，则店铺是可选的了
                if($data['use_type'] == 2){

//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '设置指定分类优惠券时店铺至少选择一个', 'result' => '']);
//                    }

                    $data['add_time'] = time();
                    $data['seller_id'] = SELLER_ID;
                    $newCid = Db::name('coupon')->insertGetId($data);
                    
                    

                    $temp = [];
                    foreach ($store_id_arr as $store_id){
                        $temp[]=['store_id'=>$store_id,'cid'=>$newCid,'seller_id'=>SELLER_ID];
                    }
                    if(count($temp)>0){
                        Db::name('CouponStore')->insertAll($temp);
                    }

                    Db::name('goods_coupon')->insertGetId(['coupon_id'=>$newCid,'goods_category_id'=>$data['cat_id3'],'seller_id'=>SELLER_ID]);

                }
                
                
                
            } else {




                
               
                $store_id_arr = I('store/a');
                if(count($store_id_arr)<1){
                    $this->ajaxReturn(['status' => 0, 'msg' => '提交失败:至少选择一个店铺', 'result' => '']);
                }



                $row = M('coupon')->where(array('id' => $data['id'], 'seller_id' => SELLER_ID))->save($data);


                //删除之前的老的
                Db::name('CouponStore')->where('cid',$data['id'])->delete();//先删除后添加
                //Db::name('goods_coupon')->where('coupon_id',$data['id'])->delete();//先删除后添加
    
    
                Db::name('GoodsCoupon')->where('coupon_id',$data['id'])->delete();


                $temp = [];
                foreach ($store_id_arr as $store_id) {
                    $temp[] = ['store_id' => $store_id, 'cid' => $data['id'], 'seller_id' => SELLER_ID];
                }

//                var_dump($temp);
//                die;
                if (count($temp) > 0) {

                    Db::name('CouponStore')->insertAll($temp);
                }



                if($data['use_type'] == 3){

//                    $temp = [];
//                    foreach ($store_id_arr as $store_id){
//                        $temp[]=['store_id'=>$store_id,'cid'=>$data['id'],'seller_id'=>SELLER_ID];
//                    }
//                    if(count($temp)>0){
//                        Db::name('CouponStore')->insertAll($temp);
//                    }

//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选', 'result' => '']);
//                    }
//                    Db::name('goods_coupon')->add(['coupon_id'=>$data['id'],'store_id'=>$store_id,'seller_id'=>SELLER_ID,]);


                }


                //指定商品
//                if($data['use_type'] == 1){
//                    foreach ($data['goods_id'] as $v) {
//                        Db::name('goods_coupon')->add(['coupon_id'=>$data['id'],'goods_id'=>$v,'seller_id'=>SELLER_ID,'add_time'=>time()]);
//                    }
//                }


                //指定商品分类id
                if($data['use_type'] == 2){
    
                    $goodsCoupon = ['coupon_id'=>$data['id'],'goods_category_id'=>$data['cat_id3']];
                    Db::name('GoodsCoupon')->insertGetId($goodsCoupon);

//                    $temp = [];
//                    foreach ($store_id_arr as $store_id){
//                        $temp[]=['store_id'=>$store_id,'cid'=>$data['id'],'seller_id'=>SELLER_ID];
//                    }
//                    if(count($temp)>0){
//                        Db::name('CouponStore')->insertAll($temp);
//                    }

//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选', 'result' => '']);
//                    }

//                    foreach ($stores as $store){
//
//
//
//
//                    }
//                    Db::name('goods_coupon')->add([
//                        'coupon_id'=>$data['id'],
//                        'goods_category_id'=>$data['cat_id3'],
//                        'seller_id'=>SELLER_ID,
//                        'add_time'=>time(),
//                        'store_id'=>$store_id
//                    ]);

                }

            }
            if ($row !== false) {
                $this->ajaxReturn(['status' => 1, 'msg' => '提交成功', 'result' => '']);
            } else {
                $this->ajaxReturn(['status' => 0, 'msg' => '提交失败', 'result' => '']);
            }
        }

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();

        $coupon_price_list = Db::name('coupon_price')->where('')->select();
        if(empty($coupon_price_list)){
            $this->error('总平台没有设置优惠券面额，商家不能添加优惠券');
        }
        $cid = I('get.id/d');
        if ($cid) {


            $coupon = M('coupon')->where(array('id' => $cid, 'seller_id' => SELLER_ID))->find();
            if (empty($coupon)) {
                $this->error('代金券不存在');
            }else{

                $this->assign('isadd',0);


                if($coupon['use_type'] == 2){
                    $goods_coupon = Db::name('goods_coupon')->where('coupon_id',$cid)->find();
                    $cat_info = M('goods_category')->where(array('id'=>$goods_coupon['goods_category_id']))->find();
                    $cat_path = explode('_', $cat_info['parent_id_path']);
                    $coupon['cat_id1'] = $cat_path[1];
                    $coupon['cat_id2'] = $cat_path[2];
                    $coupon['cat_id3'] = $goods_coupon['goods_category_id'];

                    //$isStores = Db::name('goods_coupon')->where(['coupon_id'=>$cid,'seller_id'=>SELLER_ID])->column('store_id');

                }


                if($coupon['use_type'] == 1){
                    $coupon_goods_ids = Db::name('goods_coupon')->where('coupon_id',$cid)->getField('goods_id',true);
                    $enable_goods = M('goods')->where("goods_id", "in", $coupon_goods_ids)->select();
                    $this->assign('enable_goods',$enable_goods);

                    //$isStores = Db::name('goods_coupon')->where(['coupon_id'=>$cid,'seller_id'=>SELLER_ID])->column('store_id');

                }


                if($coupon['use_type'] == 3){

                    //$isStores = Db::name('goods_coupon')->where('coupon_id',$cid)->column('store_id');

                }


                //三种渠道都有选择店铺列表
//                foreach ($stores as &$store) {
//                    $store['select'] = 0;
//                    foreach ($isStores as $selectStore) {
//                        if ($store['store_id'] == $selectStore) {
//                            $store['select'] = 1;
//                            break;
//                        }
//                    }
//                }


            }
//            var_dump($coupon);
            $this->assign('coupon', $coupon);


        } else {
            $def['send_start_time'] = time();//strtotime("+1 day");
            $def['send_end_time'] = strtotime("+1 month");
            $def['use_start_time'] = strtotime("+1 day");
            $def['use_end_time'] = strtotime("+2 month");
            $this->assign('coupon', $def);
        }
        
        $cat_list = M('goods_category')->where(['parent_id' => 0])->select();//自营店已绑定所有分类
//        $bind_all_gc = M('store')->where(array('store_id'=>STORE_ID))->getField('bind_all_gc');
//        if ($bind_all_gc == 1) {
//            $cat_list = M('goods_category')->where(['parent_id' => 0])->select();//自营店已绑定所有分类
//        } else {
//            //自营店已绑定所有分类
//            $cat_list = Db::name('goods_category')->where(['parent_id' => 0])->where('id', 'IN', function ($query) {
//                $query->name('store_bind_class')->where('store_id', STORE_ID)->where('state', 1)->field('class_1');
//            })->select();
//        }

        $this->assign('stores',$stores);


        $this->assign('cat_list',$cat_list);
        $this->assign('coupon_price_list',$coupon_price_list);
        return $this->fetch();
    }

    /**
     * 优惠券发放
     */
    public function make_coupon()
    {
        //获取优惠券ID
        $cid = I('id/d');
        $type = I('type');
        //查询是否存在优惠券
        $data = M('coupon')->where(array('id' => $cid, 'store_id' => STORE_ID))->find();
        if($data['send_start_time'] > time()) $this->error('该优惠券未到发放时间');
        if($data['send_end_time']< time()) $this->error('该优惠券已过发放时间');
        if (IS_POST) {
            if ($type != 3) $this->ajaxReturn(['status'=>'-1','msg'=>'该优惠券类型不支持发放']);
            if (!$data) $this->ajaxReturn(['status'=>'-1','msg'=>'优惠券类型不存在']);
            if($data['createnum']>0){    //不是无限发放的，要计算剩余派发量
                $remain = $data['createnum'] - $data['send_num'];
                if ($remain <= 0) $this->ajaxReturn(['status'=>'-1','msg'=>$data['name'].'已经发放完了']);
            }
            $num = I('post.num/d');
            if ($num > $remain and $data['createnum']>0) $this->ajaxReturn(['status'=>'-1','msg'=>$data['name'] . '发放量不够了，只剩下'.$remain.'份了']);
            if (!$num > 0) $this->ajaxReturn(['status'=>'-1','msg'=>'发放数量不能小于0']);
            $add['cid'] = $cid;
            $add['type'] = $type;
            $add['send_time'] = time();
            $add['store_id'] = STORE_ID;
            for ($i = 0; $i < $num; $i++) {
                do {
                    $code = get_rand_str(8, 0, 1);//获取随机8位字符串
                    $check_exist = M('coupon_list')->where(array('code' => $code))->find();
                } while ($check_exist);
                $add['code'] = $code;
                M('coupon_list')->add($add);
            }
            $coupon_where = array('id' => $cid, 'store_id' => STORE_ID);
            M('coupon')->where($coupon_where)->setInc('send_num', $num);
            sellerLog("发放" . $num . '张' . $data['name']);
            $this->ajaxReturn(['status'=>'1','msg'=>'发放成功','url'=>U('Coupon/coupon_list',['id'=>$cid])]);
            exit;
        }
        $this->assign('coupon', $data);
        return $this->fetch();
    }

    /**
     * 获取用户列表
     */
    public function ajax_get_user()
    {
        //搜索条件
        $condition = array();
        I('mobile') ? $condition['u.mobile'] = I('mobile') : false;
        I('email') ? $condition['u.email'] = I('email') : false;
        $cid = I('cid');
        $nickname = I('nickname');
        if(!empty($nickname)){
            $condition['u.nickname'] = array('like',"%$nickname%");
        }
        $level_id = I('level_id/d');
        if($level_id > 0){
            $condition['u.level'] = $level_id;
        }
        if($level_id  ==  -1){
            $tb = C('DB_PREFIX').'store_collect';
            $condition['c.store_id'] = STORE_ID;
            $count = M('users')->alias('u')->join($tb.' c', 'u.user_id = c.user_id','LEFT')->where($condition)->count();
            $Page  = new AjaxPage($count,10);
            $userList = M('users')->field('u.*')->alias('u')
                ->join($tb.' c', 'u.user_id = c.user_id','LEFT')
                ->where($condition)->order("u.user_id desc")
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        }else{
            $exclude_uids  = M('coupon_list')->where(['cid'=>$cid])->getField('uid',true);  //查找看一集给谁发过了
            $count = M('users')->alias('u')->whereNotIn('u.user_id',$exclude_uids)->where($condition)->count();
            $Page  = new AjaxPage($count,10);
            $userList = M('users')->alias('u')
                ->where($condition)
                ->whereNotIn('u.user_id',$exclude_uids)
                ->order("u.user_id desc")
                ->limit($Page->firstRow.','.$Page->listRows)
                ->select();
        }
        $user_level = M('user_level')->getField('level_id,level_name',true);
        $this->assign('user_level',$user_level);
        $this->assign('userList',$userList);
        $show = $Page->show();
        $this->assign('page',$show);
        return $this->fetch();
    }

    /**
     * 发放优惠券
     */
    public function send_coupon()
    {
        $cid = I('cid/d');
        if (IS_POST) {
            $level_id = I('level_id/d');
            $user_id = I('user_id/a');
            $insert = '';
            $coupon_where = array('id' => $cid, 'store_id' => STORE_ID);
            $coupon = M('coupon')->where($coupon_where)->find();
            if ($coupon['createnum'] > 0) {
                $remain = $coupon['createnum'] - $coupon['send_num'];//剩余派发量
                if ($remain <= 0) $this->error($coupon['name'] . '已经发放完了');
            }

            if (empty($user_id) && $level_id >= 0) {
                $user_where = array('is_lock' => 0);
                if ($level_id == 0) {
                    $user = M('users')->where($user_where)->select();
                } else {
                    $user_where['level'] = $level_id;
                    $user = M('users')->where($user_where)->select();
                }
                if ($user) {
                    $able = count($user);//本次发送量
                    if ($coupon['createnum'] > 0 && $remain < $able) {
                        $this->error($coupon['name'] . '派发量只剩' . $remain . '张');
                    }
                    foreach ($user as $k => $val) {
                        $user_id = $val['user_id'];
                        $time = time();
                        $gap = ($k + 1) == $able ? '' : ',';
                        $insert .= "($cid,1,$user_id,$time," . STORE_ID . ")$gap";
                    }
                }
            } else {
                $able = count($user_id);//本次发送量
                if ($coupon['createnum'] > 0 && $remain < $able) {
                    $this->error($coupon['name'] . '派发量只剩' . $remain . '张');
                }
                foreach ($user_id as $k => $v) {
                    $time = time();
                    $gap = ($k + 1) == $able ? '' : ',';
                    $insert .= "($cid,1,$v,$time," . STORE_ID . ")$gap";
                }
            }
            $sql = "insert into __PREFIX__coupon_list (`cid`,`type`,`uid`,`send_time`,store_id) VALUES $insert";
            Db::execute($sql);
            M('coupon')->where("id", $cid)->setInc('send_num', $able);
            sellerLog("发放" . $able . '张' . $coupon['name']);
            $this->success("发放成功");
            exit;
        }
        $level = M('user_level')->select();
        $this->assign('level', $level);
        $this->assign('cid', $cid);
        return $this->fetch();
    }

    /**
     * 删除优惠券类型
     */
    public function del_coupon()
    {
        //获取优惠券ID
        $cid = I('get.id/d');
        $coupon = M('coupon')->where(array('id' => $cid, 'seller_id' => SELLER_ID))->find();

        $listCount = M('coupon_list')->where(array('cid' => $cid))->count();
        if($listCount>0||$coupon['send_num']>0){
            $this->ajaxReturn(['status'=>'-1','msg'=>'删除失败:该优惠券已经有领取，无法删除']);
        }

        //查询是否存在优惠券
        $row = M('coupon')->where(array('id' => $cid, 'seller_id' => SELLER_ID))->delete();
        if ($row) {
            //删除此类型下的优惠券
            //M('coupon_list')->where(array('cid' => $cid))->delete();

            M('coupon_store')->where(array('cid' => $cid))->delete();
            $this->ajaxReturn(['status'=>'1','msg'=>'删除成功']);
        } else {
            $this->ajaxReturn(['status'=>'-1','msg'=>'删除失败']);
        }
    }

    /**
     * 优惠券详细查看
     */
    public function coupon_list()
    {
        //获取优惠券ID
        $cid = I('get.id/d');

        $where =   ['cid' => $cid]  ;


        //查询是否存在优惠券        
        $check_coupon = M('coupon')->field('id,type')->where(array('id' => $cid, 'seller_id' => SELLER_ID))->find();
        if (!$check_coupon['id'] > 0) {
            $this->error('不存在该类型优惠券');
        }
        $this->assign('cid',$cid);

        //查询该优惠券的列表的数量
        $sql = "SELECT count(1) as c FROM __PREFIX__coupon_list  l " .
            "LEFT JOIN __PREFIX__coupon c ON c.id = l.cid " . //联合优惠券表查询名称
            "LEFT JOIN __PREFIX__order o ON o.order_id = l.order_id " .     //联合订单表查询订单编号
            "LEFT JOIN __PREFIX__users u ON u.user_id = l.uid WHERE l.cid = :cid";    //联合用户表去查询用户名
        if(I('store_id',null)){
            $sql.= (" AND l.store_id = ".I('store_id',null));
        }

        $sql .= ' ORDER BY l.send_time DESC';


        $count = Db::query($sql, ['cid' => $cid]);
        $count = $count[0]['c'];
        $Page = new Page($count, 10);
        $show = $Page->show();

        //查询该优惠券的列表
        $sql = "SELECT l.*,c.name,o.order_sn,u.nickname,u.email FROM __PREFIX__coupon_list  l " .
            "LEFT JOIN __PREFIX__coupon c ON c.id = l.cid " . //联合优惠券表查询名称
            "LEFT JOIN __PREFIX__order o ON o.order_id = l.order_id " .     //联合订单表查询订单编号
            "LEFT JOIN __PREFIX__users u ON u.user_id = l.uid WHERE l.cid = :cid" ;    //联合用户表去查询用户名

        if(I('store_id',null)){
            $sql.= (" AND l.store_id = ".I('store_id',null));
        }
        $sql .= ' ORDER BY l.send_time DESC';

        $sql .= " limit {$Page->firstRow} , {$Page->listRows}";


        $coupon_list = Db::query($sql,['cid' => $cid]);


        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);

        foreach ($coupon_list as &$item){
            foreach ($stores as $store){
                if($store['store_id']==$item['store_id']){
                    $item['store_name'] = $store['store_name'];
                }
            }
        }


        $this->assign('coupon_type', C('COUPON_TYPE'));
        $this->assign('type', $check_coupon['type']);
        $this->assign('lists', $coupon_list);
        $this->assign('page', $show);
        return $this->fetch();
    }

    /**
     * 删除一张优惠券
     */
    public function coupon_list_del()
    {
        //获取优惠券ID
        $cid = I('get.id/d');
        if (!$cid)
            $this->ajaxReturn(['status'=>'1','msg'=>'删除成功']);
        //查询是否存在优惠券
        $row = M('coupon_list')->where(array('id' => $cid, 'store_id' => STORE_ID))->delete();
        Db::name('goods_coupon')->where('coupon_id', $cid)->delete();
        if ($row){
            $this->ajaxReturn(['status'=>'1','msg'=>'删除成功']);
        } else {
            $this->ajaxReturn(['status'=>'-1','msg'=>'删除失败']);
        }
    }
}