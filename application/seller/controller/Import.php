<?php
namespace app\seller\controller;

use app\seller\logic\GoodsCategoryLogic;
use think\Db;
use think\Page;

//淘宝CSV导入功能
class Import extends Base{
	public function index(){
            header("Content-type: text/html; charset=utf-8");
      
	}

	/*
	ajax返回平台商品分类 
	*/
	public function return_goods_category(){
		$parent_id = I('get.parent_id/d', '0'); //商品分类父id
        empty($parent_id) && exit('');

        $GoodsCategoryLogic = new GoodsCategoryLogic();
        $GoodsCategoryLogic->setStore($this->storeInfo);
        $list = $GoodsCategoryLogic->getStoreGoodsCategory($parent_id);

        foreach ($list as $k => $v) {
            $html .= "<option value='{$v['id']}' rel='{$v['commission']}'>{$v['name']}</option>";
        }
        exit($html);
	}

	//上传的csv文件及图片文件 返回数组结果
	public function upload_data(){
            header("Content-type: text/html; charset=utf-8");

	}

	public function add_data(){
        header("Content-type: text/html; charset=utf-8");

	}

	/**
	 * csv文件转码为utf8
	 * @param  string 文件路径
	 * @return resource  打开文件后的资源类型
	 */
	private function fopen_utf8($filename){  
        $encoding='';  
        $handle = fopen($filename, 'r');  
        $bom = fread($handle, 2);   
        rewind($handle);  
       
        if($bom === chr(0xff).chr(0xfe)  || $bom === chr(0xfe).chr(0xff)){   
            $encoding = 'UTF-16';  
        } else {  
            $file_sample = fread($handle, 1000) + 'e';    
            rewind($handle);  
            $encoding = mb_detect_encoding($file_sample , 'UTF-8, UTF-7, ASCII, EUC-JP,SJIS, eucJP-win, SJIS-win, JIS, ISO-2022-JP');  
        }  
        if ($encoding){  
            stream_filter_append($handle, 'convert.iconv.'.$encoding.'/UTF-8');  
        }  
        return ($handle);  
    } 

    //csv文件读取为数组形式返回
	private function str_getcsv($string, $delimiter=',', $enclosure='"'){ 
        $fp = fopen('php://temp/', 'r+');
        fputs($fp, $string);
        rewind($fp);
        while($t = fgetcsv($fp, strlen($string), $delimiter, $enclosure)) {
            $r[] = $t;
        }
        if(count($r) == 1) 
            return current($r);
        return $r;
    }
}