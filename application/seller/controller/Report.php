<?php
namespace app\seller\controller;

use app\seller\logic\OrderLogic;
use think\Db;
use think\Page;
use app\common\model\SellerAddress;

class Report extends Base{
	public $store_id;
    public function _initialize(){
        parent::_initialize();
//		$this->store_id = STORE_ID;
	}


	
	public function index(){

        $filter = array();
        
        
        

        if(!isEmptyObject(input('store_id'))){
            $filter['store_id'] = input('store_id');
        }else{
            $filter['seller_id'] = SELLER_ID;
        }

        $stores = Db::name('store')->where($filter)->column('store_id');

        $OrderMobile = new OrderLogic();
        
        $today=$OrderMobile->getTodayAmount($this->store_id);
        
		if($today['today_order'] == 0){
			$today['sign'] = round(0, 2);
		}else{
			$today['sign'] = round($today['today_amount']/$today['today_order'],2);
		}
		$this->assign('today',$today);
		
		
		
		
		$sql = "SELECT COUNT(*) as tnum,sum(goods_price-order_prom_amount) as amount, FROM_UNIXTIME(add_time,'%Y-%m-%d') as gap from  __PREFIX__order{$this->select_year} ";

		//这里才是区分是否某个店和所有店的地方
        $store_id = I('store_id',null);
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个
        
        
//        'o.pay_status'=>1,
//            'o.order_status'=>['in','1,2,4'],
//            'o.store_id'=>['in',$stores],
//            'o.add_time'=>['between', [$begin,$end_time]],
//
        
       
        $q2 = Db::name('order')->where('order_status',3)->whereBetween('add_time',[$this->begin,$this->end]);
        
        if($store_id){
            //指定店铺的
            $sql .= " where add_time>$this->begin and add_time<$this->end and store_id=$store_id AND pay_status=1 and order_status in(1,2,4) group by gap order by gap desc";
            $q2 = $q2->where('store_id',$store_id);
        }else{
            //所有店铺的
            $q2 = $q2->where('store_id','in',$stores);
            $sql .= " where add_time>$this->begin and add_time<$this->end and store_id in (".join($stores,',').") AND pay_status=1  and order_status in(1,2,4) group by gap order by gap desc";
        }
        
        



        $res = Db::query($sql);//订单数,交易额
        
        
      
        $tnum = 0;
        $tamount = 0;
		foreach ($res as $val){
			$arr[$val['gap']] = $val['tnum'];
			$brr[$val['gap']] = $val['amount'];
			$tnum += $val['tnum'];
			$tamount += $val['amount'];
		}

		
		
		 $all = ['money_total'=>0,'complete_order_total'=>0,'fail_order_total'=>0,'sign'=>round(0, 2)];
        $all['fail_order_total'] = $q2->count();
		
		for($i=$this->end;$i>$this->begin;$i=$i-24*3600){
			$tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
			$tmp_amount = empty($brr[date('Y-m-d',$i)]) ? 0 : $brr[date('Y-m-d',$i)];
			$tmp_sign = empty($tmp_num) ? 0 : round($tmp_amount/$tmp_num,2);						
			$order_arr[] = $tmp_num;
			$amount_arr[] = $tmp_amount;			
			$sign_arr[] = $tmp_sign;
			$date = date('Y-m-d',$i);
			$list[] = array('day'=>$date,'order_num'=>$tmp_num,'amount'=>$tmp_amount,'sign'=>$tmp_sign,'end'=>date('Y-m-d',$i+24*60*60));
			$all['money_total'] += $tmp_amount;
			$all['complete_order_total'] +=$tmp_num;
			$day[] = $date;
		}
        
       if($all['complete_order_total']>0){
           $all['sign'] = round($all['money_total']/$all['complete_order_total'],2);
       }
       $this->assign('all',$all);
       
//        if($all == 0){
//            $today['sign'] =
//        }else{
//            $today['sign'] = round($today['today_amount']/$today['today_order'],2);
//        }

        
        
        $this->assign('list',$list);
		
		$result = array('order'=>array_reverse($order_arr),'amount'=>array_reverse($amount_arr),'sign'=>array_reverse($sign_arr),'time'=>array_reverse($day));
		$this->assign('result',json_encode($result));


        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);

		return $this->fetch();
	}

    /**
     * @desc 经销商用户列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
	public function customer(){
        $filter = array();
	    $store_id = I('store_id',null);
	    if($store_id){
	        $filter['store_id'] = $store_id;

        }else{
	        $filter['seller_id'] =  SELLER_ID;
        }
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个
        
        
        $begin = $this->begin;
        $end_time = $this->end;
        
        
        $stores = Db::name('store')->where($filter)->column('store_id');
        $userIdArr = Db::name('userStore')->alias('s')->join('users u','s.user_id=u.user_id')->where('s.store_id','in',$stores)->where('u.reg_time','between', [$begin,$end_time])->column('s.user_id');

        $count = Db::name('userStore')->alias('s')->join('users u','s.user_id=u.user_id')->where('s.user_id','in',$userIdArr)->count();
        $Page  = new Page($count,10);
        $show = $Page->show();

        $this->assign('pager',$Page);
        $this->assign('page', $show);// 赋值分页输出

        $lists = Db::name('users')->where('user_id','in',$userIdArr)->order('reg_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        
        foreach ($lists as &$user){
            $userStoreInfo = Db::name('userStore')->where('user_id',$user['user_id'])->find();
//            $user['store_name'] = $userStoreInfo['store_name'];
            $user['store_id'] = $userStoreInfo['store_id'];
        }
        $this->assign('users',$lists);

        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);

        return $this->fetch();
    }
    
    
    /**
     * 按照店铺统计数量
     * @return mixed
     */
    public function customerByStore(){
    
    
      
    
        $storeUser = Db::name('UserStore')
            ->alias('su')
            ->join('Users u','su.user_id=u.user_id')
            ->join('store s','su.store_id=s.store_id')
            ->where('s.deleted',0)
            ->where('s.seller_id',SELLER_ID)
            ->field('s.store_id,s.store_name,s.seller_name,count(*) as total')->group('store_id')->select();
        
//        $query = 'select s.store_id,s.store_name,s.seller_name,count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id WHERE s.seller_id='.SELLER_ID.'  group by s.store_id';
//        $list = Db::query($query);
        $this->assign('list',$storeUser);
        
        
//        $countQuery = 'select count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id WHERE s.seller_id='.SELLER_ID;
        $count = Db::name('UserStore')
            ->alias('su')
            ->join('Users u','su.user_id=u.user_id')
            ->join('store s','su.store_id=s.store_id')
            ->where('s.deleted',0)
            ->where('s.seller_id',SELLER_ID)
            ->count();
        
        
//        var_dump($count);die;
        $this->assign('count',$count);
//        echo '<pre>';
//        var_dump($list);die;
        return $this->fetch();
    }
    
    /**
     * 新增用户
     */
    
    public function register(){
        
        $filter = array();
        $store_id = I('store_id',null);
        if($store_id){
            $filter['store_id'] = $store_id;
            
        }else{
            $filter['seller_id'] =  SELLER_ID;
        }

        $filter['deleted'] = 0;
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个


        
        $begin = $this->begin;
        $end_time = $this->end;
        //修改，因为前台传过来的时间不能把店铺过滤掉的
        //$stores = Db::name('store')->where($filter)->whereBetween('add_time', [$this->begin, $this->end])->column('store_id');
        $stores = Db::name('store')->where($filter)->column('store_id');

        $userIdArr = Db::name('userStore')->alias('s')->join('users u','s.user_id=u.user_id')->where('s.store_id','in',$stores)->where('u.reg_time','between', [$begin,$end_time])->column('s.user_id');
    
        if(!$userIdArr){
            $userIdArr = [];//给个空数组，后面不会出错
        }
        
        
        $sql = "SELECT COUNT(*) as tnum,FROM_UNIXTIME(reg_time,'%Y-%m-%d') as gap from  __PREFIX__users ";
    
        
    
        if($store_id){
            //指定店铺的
            $sql .= " where  reg_time>$this->begin and reg_time<$this->end and  user_id in (".join($userIdArr,',').")   group by gap order by gap desc";
        }else{
            //所有店铺的
            $sql .= " where reg_time>$this->begin and reg_time<$this->end and user_id in (".join($userIdArr,',').") group by gap order by gap desc";
        }
    
    
        if($userIdArr){
            
            
            $res = Db::query($sql);//订单数,交易额
        }else{
            $res = [];
        }
       
        $tnum = 0;

        foreach ($res as $val){
            $arr[$val['gap']] = $val['tnum'];
            $tnum += $val['tnum'];
        }
    
        for($i=$this->end;$i>$this->begin;$i=$i-24*3600){
            $tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
            
            $order_arr[] = $tmp_num;

            $date = date('Y-m-d',$i);
            $list[] = array('day'=>$date,'order_num'=>$tmp_num,'end'=>date('Y-m-d',$i+24*60*60));
            $day[] = $date;
        }
    
        $this->assign('tnum',$tnum);
        $this->assign('list',$list);
        
        if($order_arr&&$day){
            $result = array('order'=>array_reverse($order_arr),'time'=>array_reverse($day));
            $this->assign('result',json_encode($result));
        }else{
            $this->assign('result',json_encode([]));
        }
        
        
    
    
       
       $this->assign('totalCustomer',Db::name('userStore')->alias('s')->join('users u','s.user_id=u.user_id')->where('s.store_id','in',$stores)->count());
        
        
//
//        $count = Db::name('userStore')->alias('s')->join('users u','s.user_id=u.user_id')->where('s.user_id','in',$userIdArr)->count();
//        $Page  = new Page($count,10);
//        $show = $Page->show();
//
//        $this->assign('pager',$Page);
//        $this->assign('page', $show);// 赋值分页输出
//
//        $lists = Db::name('users')->where('user_id','in',$userIdArr)->limit($Page->firstRow.','.$Page->listRows)->select();
//
//        foreach ($lists as &$user){
//            $userStoreInfo = Db::name('userStore')->where('user_id',$user['user_id'])->find();
////            $user['store_name'] = $userStoreInfo['store_name'];
//            $user['store_id'] = $userStoreInfo['store_id'];
//        }
//        $this->assign('users',$lists);
        
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
        return $this->fetch();
    }

    /**
     * 销售排行
     * @return mixed
     */
	public function saleTop(){

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');
        $store_id = I('store_id',null);
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个
        if($store_id){
            $filter['store_id'] = $store_id;
        }else{
            $filter['store_id'] =  array('in',$stores);
        }

        $count = Db::name('order_goods'.$this->select_year)
            ->field('goods_name,goods_sn,sum(goods_num) as sale_num,sum(goods_num*goods_price) as sale_amount')
            ->where("is_send = 1")->where($filter)->group(' goods_id,goods_sn,goods_name')->order('sale_amount DESC')->cache(true,3600)->count();
        $Page  = new Page($count,20);
        $show = $Page->show();

        $this->assign('pager',$Page);
        $this->assign('page', $show);// 赋值分页输出

        $res = Db::name('order_goods'.$this->select_year)
            ->field('goods_name,goods_sn,sum(goods_num) as sale_num,sum(goods_num*goods_price) as sale_amount')
            ->where("is_send = 1")->where($filter)->group(' goods_id,goods_sn,goods_name')->order('sale_amount DESC')->limit($Page->firstRow.','.$Page->listRows)->cache(true,3600)->select();

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
		$this->assign('list',$res);
		return $this->fetch();
	}
    
    public function visit(){
	    
	    
        set_time_limit(0);
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');

        $store_id = I('store_id',null);
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个

        if($store_id){
            $filter['store_id'] = $store_id;
        }else{
            $filter['store_id'] =  array('in',$stores);
        }

        $goods = Db::name('goods')->where($filter)->column('goods_id');


        $count = Db::name('goodsVisit')
            ->field('goods_id,count(goods_id) as visit_num')
            ->where('goods_id','in',$goods)->group(' goods_id')->order('visit_num DESC')->cache(true,3600)->count();
        $Page  = new Page($count,20);
        $show = $Page->show();

        $this->assign('pager',$Page);
        $this->assign('page', $show);// 赋值分页输出



        $res = Db::name('goodsVisit')
            ->field('goods_id,count(goods_id) as visit_num')
            ->where('goods_id','in',$goods)->group(' goods_id')->order('visit_num DESC')->limit($Page->firstRow.','.$Page->listRows)->cache(true)->select();
        
        $goods_id_arr = [];
        foreach ($res as $item){
            $goods_id_arr[] = $item['goods_id'];
        }
        
        $goodsList = Db::name('goods')->where('goods_id','in',$goods_id_arr)->field('goods_name,goods_sn,goods_id')->select();
        
        foreach ($res as &$visit){
            foreach ($goodsList as $goods){
                if($goods['goods_id']==$visit['goods_id']){
                    $visit['goods_name'] = $goods['goods_name'];
                    $visit['goods_sn'] = $goods['goods_sn'];
                }
            }
            
        }
        $this->assign('list',$res);

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);

        return $this->fetch();
        
    }
    
    
    
    public function saleList(){
	    
        $begin = $this->begin;
        $end_time = $this->end;
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');
    
//        $sql .= " where add_time>$this->begin and add_time<$this->end and store_id in (".join($stores,',').") AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4) group by gap order by gap desc";
        $order_where = [
            'o.pay_status'=>1,
            'o.order_status'=>['in','1,2,4'],
            'o.store_id'=>['in',$stores],
            'o.add_time'=>['between', [$begin,$end_time]],
        ];  //交易成功的有效订单
       
        $order_count =  Db::name('order')->alias('o')
            ->where($order_where)->count();
        $this->assign('count',$order_count);
//        $order_count = Db::name('order')->alias('o')
//            ->join('order_goods og','o.order_id = og.order_id','left')->where($order_where)
//            ->group('o.order_id')->count();
        $Page = new Page($order_count,50);
//        $order_list = Db::name('order')->alias('o')
//            ->field('o.*,SUM(og.cost_price) as coupon_amount')
//            ->join('order_goods og','o.order_id = og.order_id','left')
//            ->where($order_where)->group('o.order_id')->limit($Page->firstRow,$Page->listRows)->select();
    
        $order_list = Db::name('order')->alias('o')
            ->where($order_where)->limit($Page->firstRow,$Page->listRows)->select();
        $this->assign('order_list',$order_list);
        $this->assign('page',$Page->show());
        return $this->fetch();
	}

    /**
     * 导出用户列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_store_customer_list(){
    
        $begin = $this->begin;
        $end_time = $this->end;
//        $query = 'select s.store_id,s.store_name,s.seller_name,count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id WHERE s.seller_id='.SELLER_ID.'  group by s.store_id order by total desc';
//        $list = Db::query($query);

        $query = Db::name('UserStore')
            ->alias('su')
            ->join('Users u','su.user_id=u.user_id')
            ->join('store s','su.store_id=s.store_id')
            ->where('s.deleted',0)
            ->where('s.seller_id',SELLER_ID)
            ->whereBetween('reg_time',[$begin,$end_time])
            ->field('s.store_id,s.store_name,s.seller_name,u.user_id,u.mobile,u.email,u.reg_time');

        if(!isEmptyObject(input('store_id'),true)){
            $query->where('s.store_id',input('store_id'));
        }
        
        $list  = $query->select();



        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">店铺id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:220px;">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">邮箱</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">手机号码</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">注册时间</td>';
        $strTable .= '</tr>';


        foreach ($list as $k => $val) {

            $val['reg'] = null;
            if($val['reg_time']){
                $val['reg'] = date('Y-m-d H:m:s',$val['reg_time']);
            }

            $strTable .= '<tr>';

            $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['store_id'] . '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['store_name'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_id'] .  ' </td>';//店铺名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] .  ' </td>';//用户id
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] .  ' </td>';//邮箱
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['reg'] .  ' </td>';//手机号码
            $strTable .= '</tr>';

        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '经销商所有用户列表');
        exit();
    }
    
    /**
     * 经销商导出各个店铺的用户统计数据
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
	public function export_store_customer(){
 
        
//        $query = 'select s.store_id,s.store_name,s.seller_name,count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id WHERE s.seller_id='.SELLER_ID.'  group by s.store_id order by total desc';
//        $list = Db::query($query);
        
        $list = Db::name('UserStore')
            ->alias('su')
            ->join('Users u','su.user_id=u.user_id')
            ->join('store s','su.store_id=s.store_id')
            ->join('tp_store_apply sa','s.store_id=sa.store_id','LEFT')
            ->where('s.deleted',0)
            ->where('s.seller_id',SELLER_ID)
            ->field('s.store_id,s.store_name,s.seller_name,count(*) as total,sa.company_staff,sa.seller_address_id,sa.protocol_createtime,sa.protocol_starttime,sa.protocol_endtime')->group('store_id')->select();
        
        
        $SellerAddress = new SellerAddress();
        $address_list = $SellerAddress->where('type', 2)->where('seller_id',SELLER_ID)->order('seller_address_id desc')->select();
        
        
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">店铺id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:220px;">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">对应ESC门店</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户数量</td>';
        $strTable .= '</tr>';

        
        foreach ($list as $k => $val) {
    
            $val['seller_full_address'] = null;
            $val['seller_address_title'] = null;
    
            foreach ($address_list as $address){
                if($address['seller_address_id']===$val['seller_address_id']){
                    $val['seller_full_address'] = $address['fulladdress'];
                    $val['seller_address_title'] = $address['title'];
                }
            }
                
            $strTable .= '<tr>';
            
            $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['store_id'] . '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['store_name'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['seller_address_title'] . ' </td>';//ESC门店名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total'] .  ' </td>';//店铺名称
            $strTable .= '</tr>';
           
        }
        $strTable .= '</table>';
        
        unset($orderList);
        downloadExcel($strTable, '经销商各店铺用户数量统计');
        exit();
    }

    //财务统计

    /**
     * @desc 默认显示所有店铺汇总，如果手动选择某个店铺
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function finance(){

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');

        $store_id = I('store_id',null);
        $this->assign('selectStoreId',$store_id);//确认当前选择的店铺是哪个



        $begin = $this->begin;
        $end_time = $this->end;


        if($store_id){
            $order = Db::name('order')->alias('o')
                ->where(['o.pay_status'=>1,'o.shipping_status'=>1,'o.order_status'=>['in','1,2,4'],'o.store_id'=>$store_id])->whereTime('o.create_time', 'between', [$begin, $end_time])
                ->order('o.create_time asc')->getField('order_id,o.*');  //以时间升序
        }else{
            $order = Db::name('order')->alias('o')
                ->where(['o.pay_status'=>1,'o.shipping_status'=>1,'o.order_status'=>['in','1,2,4']])->where('o.store_id','in',$stores)->whereTime('o.create_time', 'between', [$begin, $end_time])
                ->order('o.create_time asc')->getField('order_id,o.*');  //以时间升序
        }



        $order_id_arr = get_arr_column($order,'order_id');

        if(count($order_id_arr)>0){
            $this->assign('ishas',1);
        }else{
            $this->assign('ishas',0);
        }

        $order_ids = implode(',',$order_id_arr);            //订单ID组
        $order_goods = Db::name('order_goods')->where(['is_send'=>['in','1,2'],'order_id'=>['in',$order_ids]])->group('order_id')
            ->order('order_id asc')->getField('order_id,sum(goods_num*cost_price) as cost_price,sum(goods_num*member_goods_price) as goods_amount');  //订单商品退货的不算
        $frist_key = key($order);  //第一个key
        $sratus_date = strtotime(date('Y-m-d',$order["$frist_key"]['create_time']));  //有数据那天为循环初始时间，大范围查询可以避免前面输出一堆没用的数据
        $key = array_keys($order);
        $lastkey = end($key);//最后一个key
        $end_date = strtotime(date('Y-m-d',$order["$lastkey"]['create_time']))+24*3600;  //数据最后时间为循环结束点，大范围查询可以避免前面输出一堆没用的数据
        for($i=$sratus_date;$i<=$end_date;$i=$i+24*3600){   //循环时间
            $date = $day[] = date('Y-m-d',$i);
            $everyday_end_time = $i+24*3600;
            $goods_amount=$cost_price =$shipping_amount=$coupon_amount=$order_prom_amount=$total_amount=0.00; //初始化变量
            foreach ($order as $okey => $oval){   //循环订单
                $for_order_id = $oval['order_id'];
                if (!isset($order_goods["$for_order_id"])){
                    unset($order[$for_order_id]);           //去掉整个订单都了退货后的
                }
                if($oval['create_time'] >= $i && $oval['create_time']<$everyday_end_time){      //统计同一天内的数据
                    $goods_amount      += $oval['goods_price'];
                    $cost_price        += $order_goods["$for_order_id"]['cost_price']; //订单成本价
                    $shipping_amount   += $oval['shipping_price'];
                    $coupon_amount     += $oval['coupon_amount'];
                    $order_prom_amount += $oval['order_prom_amount'];
                    unset($order[$okey]);  //省的来回循环
                }
            }
            //拼装输出到图表的数据
            $goods_arr[]    = $goods_amount;
            $cost_arr[]     = $cost_price ;
            $shipping_arr[] = $shipping_amount;
            $coupon_arr[]   = $coupon_amount;

            $list[] = [
                'day'=>$date,
                'goods_amount'      => $goods_amount,
                'cost_amount'       => $cost_price,
                'shipping_amount'   => $shipping_amount,
                'coupon_amount'     => $coupon_amount,
                'order_prom_amount' => $order_prom_amount,
                'end'=>$everyday_end_time,
            ];  //拼装列表
        }
        rsort($list);
        $this->assign('list',$list);
        $result = ['goods_arr'=>$goods_arr,'cost_arr'=>$cost_arr,'shipping_arr'=>$shipping_arr,'coupon_arr'=>$coupon_arr,'time'=>$day];
        $this->assign('result',json_encode($result));

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);


        return $this->fetch();
    }
}