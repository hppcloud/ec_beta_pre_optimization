<?php

namespace app\seller\controller;

use app\common\logic\Store as CommonStoreLogic;
use app\common\model\SellerAddress;
use app\seller\logic\GoodsLogic;
use app\seller\logic\StoreLogic;
use think\Db;
use think\Page;


class Store extends Base
{


    protected $store_rule = [
//        'store_email|邮箱后缀'=>'require|max:250',
        'store_name|店铺名称'=>'require|max:20',
        'validate_channel|验证机制'=>'require',
        'company_name|公司名称'=>'require|max:20',
        'company_branch|签约部门'=>'require|max:20',
        'company_staff|覆盖门店'=>'require',
        'seller_address_id|ESC门店'=>'require',
        'protocol_createtime|签约时间'=>'require',
        'protocol_starttime|协议开始有效时间'=>'require',
        'protocol_endtime|协议结束时间'=>'require',
        'protocal_photo|协议多张图片'=>'require',
        'protocol_file|协议文件电子版'=>'require'
    ];


    /**
     * 导出经销商名下的店铺
     * 店铺ID、店铺名称、店铺URL、邮箱后缀、店铺logo、店铺状态
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_store()
    {
        
    
        $filter = ['seller_id'=>SELLER_ID];
    
//        $storeList = Db::name('store')->where($filter)->field('store_id,store_name,store_domain,store_email,store_logo,store_enabled')->select();
        
        $storeList = Db::name('store')
            ->alias('s')
            ->join('tp_store_apply sa','s.store_id=sa.store_id','LEFT')
            ->where('s.seller_id',SELLER_ID)
            ->field('s.*,sa.company_staff,sa.seller_address_id,sa.protocol_createtime,sa.protocol_starttime,sa.protocol_endtime')
            ->select();

        $SellerAddress = new SellerAddress();
        $address_list = $SellerAddress->where('type', 2)->where('seller_id',SELLER_ID)->order('seller_address_id desc')->select();

        
       
        
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">店铺名称</td>';



        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">验证机制</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">公司名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">签约时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="300">协议有效时间</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺URL</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">邮箱后缀</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">覆盖员工人数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">对应ESC门店</td>';



        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">店铺状态</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;width:200px;" width="*">店铺logo</td>';
        $strTable .= '</tr>';
        
        
        foreach ($storeList as $k => &$val) {
    
    
            $val['seller_full_address'] = null;
            $val['seller_address_title'] = null;
           
            foreach ($address_list as $address){
                if($address['seller_address_id']===$val['seller_address_id']){
                    $val['seller_full_address'] = $address['fulladdress'];
                    $val['seller_address_title'] = $address['title'];
                }
            }

            if(isset($val['protocol_createtime'])){
                $val['protocol_createtime'] = date('Y-m-d',strtotime($val['protocol_createtime']));
            }

            if(isset($val['protocol_starttime'])){
                $val['protocol_starttime'] = date('Y-m-d',strtotime($val['protocol_starttime']));
            }


            if(isset($val['protocol_endtime'])){
                $val['protocol_endtime'] = date('Y-m-d',strtotime($val['protocol_endtime']));
            }

            
           
            $store_enabled = $val['store_enabled']==1?'开启中':'已关闭';
            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' .  $val['store_id']. '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['store_name'] . '</td>';//订单编号

            $validate_channel = '邀请码';
            if($val['validate_channel'] =='email'){
                $validate_channel = '邮箱';
            }



            $strTable .= '<td style="text-align:left;font-size:12px;">' . $validate_channel . ' </td>';//验证机制
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['company_name'] . ' </td>';//公司名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['protocol_createtime'] . ' </td>';//签约时间


            $protocol_str ='';
            if($val['protocol_starttime']&&$val['protocol_endtime']){
                $protocol_str = $val['protocol_starttime'].'-'.$val['protocol_endtime'];
            }

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $protocol_str  . ' </td>';//协议有效时间


            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_domain'] . ' </td>';//店铺名称

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_email'] . ' </td>';//日期


            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['company_staff'] . ' </td>';//覆盖员工数量
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['seller_address_title'] . ' </td>';//ESC门店名称


            $strTable .= '<td style="text-align:left;font-size:12px;">'.$store_enabled. ' </td>';//订单状态
//            $strTable .= '<td style="height:70px;text-align:left;font-size:12px;"><img src ="' .$val['store_logo']. '" /> </td>';//订单状态
//            $strTable .= '<td style="text-align:left;font-size:12px;">"' .$val['store_logo']. ' </td>';//订单状态
            
            
            $strTable .= '</tr>';
        }
        $strTable .= '</table>';
        
        
        unset($storeList);
        downloadExcel($strTable, '经销商端:店铺列表');
        exit();
    }


    
    public function store_list()
    {


        $filter = array();
        $store_id = I('store_id',null);
        //如果有搜索就指定店铺啦
        if($store_id){
            $filter['s.store_id'] = $store_id;
        }else{
            $filter['s.store_id'] = array('in',STORES);
        }
        
        
        $store_name = I('store_title');
        if($store_name){
            $filter['s.store_name'] = array('like','%'.$store_name.'%');
        }
        
        if(!isEmptyObject(input('validate'))){
            $filter['s.validate'] = input('validate');
        }

        
        $filter['s.seller_id'] = SELLER_ID;
        
        if(!isEmptyObject(I('seller_address_id'))){
            $filter['a.seller_address_id'] = I('seller_address_id');
        }
        

        
        $count = Db::name('store')
            ->alias('s')
            ->join('StoreApply a','s.store_id=a.store_id','LEFT')
            ->where($filter)->count();
        
    
        $Page = new Page($count, 10);
        
        $lists =  Db::name('Store')
            ->alias('s')
            ->join('StoreApply a','s.store_id=a.store_id','LEFT')
            ->join('SellerAddress sa','sa.seller_address_id=a.seller_address_id','LEFT')
            ->where($filter)
            ->limit($Page->firstRow, $Page->listRows)
            ->field('s.*,a.protocol_endtime,a.protocol_starttime,sa.title')
            ->select();
        


        $store_ids = get_arr_column($lists,'store_id');
        $logs = Db::name('StoreValidateLog')->order('id desc')->where('store_id','in',$store_ids)->select();
    
    
        
        
        $ok = [];
        foreach ($lists as &$store){
            
            
            if(isset($store['protocol_starttime'])){
                $store['protocol_starttime'] = date('Y-m-d',strtotime($store['protocol_starttime']));
            }
    
    
            if(isset($store['protocol_endtime'])){
                $store['protocol_endtime'] = date('Y-m-d',strtotime($store['protocol_endtime']));
            }
            
            
            //最后一条记录
            foreach ($logs as $log){
                if($store['store_id']==$log['store_id']&&!in_array($log['store_id'],$ok)){
                    $ok[] = $log['store_id'];
                    $log['remark'] = trim($log['remark']);
                    $store['log'] = $log;
                }
            }
            
           
            
            
        }
        $this->assign('lists', $lists);
        
        
        
        $show = $Page->show();
        $this->assign('count',$count);
        $this->assign('page', $show);// 赋值分页输出
        
        $this->assign('validates',['0'=>'待审核','1'=>'审核通过','-1'=>'未通过']);

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
    
    
        $seller_address_list = Db::name('SellerAddress')->where('seller_id',SELLER_ID)->where('type',2)->field('seller_address_id,title')->select();
        $this->assign('seller_address_list',$seller_address_list);

        return $this->fetch();
    }
    
    public function checkCompanyName(){
        
        $isHasCompany = Db::name('StoreApply')
            ->alias('sa')
            ->join('tp_store s','sa.store_id=s.store_id')
            ->where('sa.company_name',input('company_name'))->find();
//        var_dump($isHasCompany['store_id']);die;
        if($isHasCompany&&$isHasCompany['store_id']!=input('store_id',null)){
            $this->ajaxReturn(['status' => 0, 'msg' => '公司名称已存在']);
            die;
        }
        $this->ajaxReturn(['status' => 1, 'msg' => '']);
        
    }
    
    public function single()
    {
        $commonLogic = new CommonStoreLogic();
        Db::startTrans();

        if (IS_POST) {
            
            $post = I('post.');
            
           
            $seller = Db::name('seller')->where('seller_id',SELLER_ID)->find();
            $post['seller_name'] = $seller['seller_name'];
            
            if(I('act')&&I('act')=='del'){
                
                $store = Db::name('store')->where(["store_id" => I('store_id')])->find();
//                $count = 0;
                $store_email_arr = explode(',',$store['store_email']);
                
                
//                foreach ($store_email_arr as $email){
//                    $tempUserCount = Db::name('users')->where('email','like','%'.$email.'%')->count();
//                    $count += $tempUserCount;
//                }
    
                $count = Db::name('UserStore')->where('store_id',$store['store_id'])->count();
    
                
                
                if($count>0){
                    $this->ajaxReturn(array('status' => 0, 'msg' => '该店铺下面有用户共'.$count.'人，无法删除','result'=>''));
                    die;
                }
               
                
                $r = M('store')->where(["store_id" => I('store_id')])->delete();
                Db::name('StoreApply')->where('store_id',I('store_id'))->delete();//相关表也要删掉
//                Db::name('goods')->where(["store_id" => I('store_id'),'seller_id'=>SELLER_ID])->delete();
                if ($r){
    
                    sellerLog('删除店铺'.$store['store_id']);//日志功能做的不错哦，可以学习
                    
                    Db::commit();
                    
                    exit(json_encode(1));

                }
                die;
            }
    
    
            if(input('validate_channel')==='email'&&isEmptyObject(input('store_email'))){
                $this->error('验证机制为邮箱时，邮箱必填');
            }
    
            //检查全局唯一的公司名称
            $isHasCompany = Db::name('StoreApply')
                ->alias('sa')
                ->join('tp_store s','s.store_id=sa.store_id')
                ->where('sa.company_name',input('company_name'))
                ->field('sa.*')
                ->find();
            if($isHasCompany&&$isHasCompany['store_id']!=input('store_id')){
                $this->error('企业名称已存在');
            }
            
            
            
            //修改
            if(I('store_id/d')){

                $rt = $this->validate($post,$this->store_rule);
                if($rt!==true){
                    $this->error($rt);
                }
                
                $isHasDomain = Db::name('store')->where('store_id','neq',I('store_id'))->where('store_domain',$post['store_domain'])->count();
                if($isHasDomain>0){
                    $this->error('URL参数与现有记录重复');
                    die;
                }
    
                $emails = explode(',',$post['store_email']);
//                foreach ($emails as $email){
//                    $isHasEmail = Db::name('store')->where('store_id','neq',I('store_id'))->where('store_email',$email)->count();
//                    if($isHasEmail>0){
//                        $this->error('邮箱地址'.$email.'已经被使用');
//                        die;
//                    }
//                }
                
                
                

                $post['validate'] = 0;//每次保存都需要重新审核
                
                $rt = M('store')->where(["store_id" => I('store_id')])->save($post);

                $isHas = M('StoreApply')->where(["store_id" => I('store_id')])->count();
                if($isHas){
                    $rt2 = M('StoreApply')->where(["store_id" => I('store_id')])->save($post);
                }else{
                    $rt2 = Db::name('StoreApply')->insertGetId($post);
                }

                
//                $rt2 = CommonStoreLogic::upRegSendCoupon(I('store_id'),SELLER_ID,I('reg_send_coupon',0),I('reg_send_coupon_val',0),I('reg_send_coupon_limit',0));
//
//
//
//                if($rt2['code']!==1){
//                    $this->error($rt2['msg']?$rt2['msg']:'更新注册是否赠送优惠券功能时出错');
//                    die;
//                }

//                $rt3 = CommonStoreLogic::upRecoverSendCoupon(I('store_id'),SELLER_ID,I('recover_send_coupon',0),I('recover_coupon_percent',0),I('recover_coupon_limit',0));
//                if($rt3['code']!==1){
//                    $this->error($rt3['msg']?$rt3['msg']:'更新回收是否赠送优惠券功能时出错');
//                    die;
//                }



                if ($rt!==false&&$rt2!==false) {
                    Db::commit();
                    $this->success("修改成功", U('Store/store_list'));
                } else {
                    Db::rollback();
                    $this->error("修改失败");
                }
                die;
                
            }

            $isHasDomain = Db::name('store')->where('store_domain',$post['store_domain'])->count();
            if($isHasDomain>0){
                $this->error('URL参数与现有记录重复');
                die;
            }
    
            //模板店铺出来的毛病
            $post['seller_id'] = SELLER_ID;
    
    
            $rt = $this->validate($post,$this->store_rule);
            if($rt!==true){
                $this->error($rt);
            }
            $emails = explode(',',$post['store_email']);
//            foreach ($emails as $email){
//                $isHasEmail = Db::name('store')->where('store_email',$email)->count();
//                if($isHasEmail>0){
//                    $this->error('邮箱地址'.$email.'已经被使用');
//                    die;
//                }
//            }

            $store_name = I('store_name',null);
            if($store_name===null||$store_name==''){
                $this->error("店铺名称必传");
            }
            
            $name_exist_count = Db::name('store')->where('store_name', $store_name)->count();
            if ($name_exist_count){
                $this->error("店铺名称已存在");
            }

           
            $store_logo = I('store_logo',null);
            if($store_logo===null||$store_logo==''){
                $this->error("logo必传");
            }
            
            
            $post['add_time'] = time();//店铺添加时间
    
            
            $post['unique_sn'] = create_unique_id();//写入唯一的序号
            
            $newStoreId = Db::name('store')->insertGetId($post);
            
           

            if(!$newStoreId){
                Db::rollback();
                $this->error('店铺新增失败');
            }


            $post['store_id'] = $newStoreId;
            $newStoreApplyId = Db::name('StoreApply')->insertGetId($post);
            if(!$newStoreApplyId){
                Db::rollback();
                $this->error('保存公司信息失败');
            }

            //自己先写默认的，如果有复制的话。再改一样的
//            $rt2 = CommonStoreLogic::upRegSendCoupon(I('store_id'),I('reg_send_coupon',1),I('reg_send_coupon_val',0),I('reg_send_coupon_limit',0));
//
//            if($rt2['code']!==1){
//                $this->error($rt2['msg']?$rt2['msg']:'更新注册是否赠送优惠券功能时出错');
//                die;
//            }
//
//            $rt3 = CommonStoreLogic::upRecoverSendCoupon(I('store_id'),I('recover_send_coupon',1),I('recover_coupon_percent',0),I('recover_coupon_limit',0));
//            if($rt3['code']!==1){
//                $this->error($rt3['msg']?$rt3['msg']:'更新回收是否赠送优惠券功能时出错');
//                die;
//            }

            //可选复制的
            $copayStoreId = I('copy_store_id',null);

            if($copayStoreId&&!isEmptyObject($copayStoreId)){
                $logic = new GoodsLogic();
                $rt = $logic->copyGoodsByStore2Store($newStoreId,$copayStoreId);

                if($rt['status']!=1){
//                    Db::name('store')->where('store_id',$newStoreId)->delete();
//                    Db::name('goods')->where('store_id',$newStoreId)->delete();
                    trace('新建店铺复制失败,模板店铺'.$copayStoreId.'==========>新店铺:'.$newStoreId);
                    Db::rollback();
                    $this->error("新增失败:".$rt['msg']);
                }else{
                    Db::commit();
                    $this->success("新增成功", U('Store/store_list'));
                }


            }else{
                Db::commit();
                $this->success("新增成功", U('Store/store_list'));
            }

            
        }


    
        $this->assign('seller_id', SELLER_ID);
        
        if(I('store_id')){

            $store = Db::name('store')
                ->alias('s')
                ->join('tp_store_apply a','s.store_id=a.store_id','LEFT')
                ->where('s.store_id',I('store_id'))
                ->field('s.*,a.company_name,a.company_branch,a.company_staff,a.seller_address_id,a.protocol_createtime,a.protocol_starttime,a.protocol_endtime,a.protocal_photo,a.protocol_file')
                ->find();


            $store['protocal_photo_arr'] = explode('|',$store['protocal_photo']);

//            $reg_send_coupon = CommonStoreLogic::getRegSendCoupon(I('store_id'),SELLER_ID);//没有的话会自动创建
//            $store['reg_send_coupon'] = $reg_send_coupon['reg_send_coupon'];
//            $store['reg_send_coupon_val'] = (int)$reg_send_coupon['reg_send_coupon_val'];
//            $store['reg_send_coupon_limit'] = (int)$reg_send_coupon['reg_send_coupon_limit'];
//
//            $recover_send_coupon = CommonStoreLogic::getRecoverSendCoupon(I('store_id'),SELLER_ID);//没有的话会自动创建
//            $store['recover_send_coupon'] = $recover_send_coupon['recover_send_coupon'];
//            $store['recover_coupon_percent'] = $recover_send_coupon['recover_coupon_percent'];
//            $store['recover_coupon_limit'] = (int)$recover_send_coupon['recover_coupon_limit'];


            $this->assign('storeInfo',$store);
        }



        //回收赠送优惠券相关

        
        //加入后台的模板店铺
        $tmplStores = Db::name('store')->where('is_tmpl',1)->select();
        
        //再拿自己的
        $selfStores = Db::name('store')->where('seller_id',SELLER_ID)->select();
        
        $merge = array_merge($tmplStores,$selfStores);
        
        $stores = unique_arr($merge);
    
        array_multisort(array_column($stores,'is_tmpl'),SORT_DESC,$stores);
        
        $this->assign('stores', $stores);


        $SellerAddress = new SellerAddress();

        $address_list = $SellerAddress->where('seller_id', SELLER_ID)->where('type', 2)->order('seller_address_id desc')->select();

        $this->assign('address_list', $address_list);
    
        return $this->fetch('_form');
        
        
    }
    
    
    
    
    public function store_info()
    {
        $apply = M('store_apply')->where("user_id", $this->storeInfo['user_id'])->find();
        
        $bind_class_list = M('store_bind_class')->where("store_id", STORE_ID)->select();
        $goods_class = M('goods_category')->getField('id,name');
        for ($i = 0, $j = count($bind_class_list); $i < $j; $i++) {
            $bind_class_list[$i]['class_1_name'] = $goods_class[$bind_class_list[$i]['class_1']];
            $bind_class_list[$i]['class_2_name'] = $goods_class[$bind_class_list[$i]['class_2']];
            $bind_class_list[$i]['class_3_name'] = $goods_class[$bind_class_list[$i]['class_3']];
        }
        $region = Db::name('region')->getField('id,name');
        $this->assign('apply', $apply);
        $this->assign('region', $region);
        $this->assign('store', $this->storeInfo);
        $this->assign('bind_class_list', $bind_class_list);
        return $this->fetch();
    }
    
    public function store_setting()
    {
        $this->storeInfo = M('store')->where("store_id", STORE_ID)->find();
        if ($this->storeInfo) {
            $grade = M('store_grade')->where("sg_id", $this->storeInfo['grade_id'])->find();
            $this->storeInfo['grade_name'] = $grade['sg_name'];
            $province = M('region')->where(array('parent_id' => 0))->select();
            $city = M('region')->where(array('parent_id' => $this->storeInfo['province_id']))->select();
            $area = M('region')->where(array('parent_id' => $this->storeInfo['city_id']))->select();
            $this->assign('province', $province);
            $this->assign('city', $city);
            $this->assign('area', $area);
        }
        return $this->fetch();
    }
    
    public function setting_save()
    {
        $data = I('post.');
        $store_domain = $data['store_domain'];
        if ($store_domain) {
            
            ($store_domain === 'www') && $this->error("店铺二级域名不能设置为www", U('Store/store_setting'));
            $hostDomain = strtolower($_SERVER['HTTP_HOST']);
            $hosts = explode('.', $hostDomain);
            if ($store_domain == $hosts[0]) $this->error("店铺二级域名不能跟主域名相同", U('Store/store_setting'));
            
            $domain_where['store_domain'] = $store_domain;
            if (STORE_ID) {
                $exists_store_domain = M('Store')->where(['store_domain' => $store_domain])->where('store_id', '<>', STORE_ID)->getField('store_domain');
            } else {
                $exists_store_domain = M('Store')->where(['store_domain' => $store_domain])->getField('store_domain');
            }
            $exists_store_domain && $this->error("已经有相同二级域名存在,请重新设置", U('Store/store_setting'));
        }
        
        if ($_POST['act'] == 'update') {
            if (!file_exists('.' . $data['themepath'] . '/style/' . $data['store_theme'] . '/images/preview.jpg')) {
                respose(array('status' => -1, 'msg' => '缺少模板文件'));
            }
            if (M('store')->where(["store_id" => STORE_ID])->save($data)) {
                respose(array('status' => 1));
            } else {
                respose(array('status' => -1, 'msg' => '没有修改模板'));
            }
        } else {
            if (M('store')->where(["store_id" => STORE_ID])->save($data)) {
                $this->success("操作成功", U('Store/store_setting'));
            } else {
                $this->error("没有修改数据", U('Store/store_setting'));
            }
        }
    }
    
    public function store_slide()
    {
        $store_slide = $store_slide_url = array();
        if (IS_POST) {
            $store_slide = I('post.store_slide/a');
            $store_slide_url = I('post.store_slide_url/a');
            $store_slide = implode(',', $store_slide);
            $store_slide_url = implode(',', $store_slide_url);
            M('store')->where("store_id", STORE_ID)->save(array('store_slide' => $store_slide, 'store_slide_url' => $store_slide_url));
            $this->success("操作成功", U('Store/store_slide'));
            exit;
        }
        if ($this->storeInfo['store_slide']) {
            $store_slide = explode(',', $this->storeInfo['store_slide']);
            $store_slide_url = explode(',', $this->storeInfo['store_slide_url']);
        }
        $this->assign('store_slide', $store_slide);
        $this->assign('store_slide_url', $store_slide_url);
        return $this->fetch();
    }
    
    public function mobile_slide()
    {
        $store_slide = $store_slide_url = array();
        if (IS_POST) {
            $store_slide = I('post.store_slide/a');
            $store_slide_url = I('post.store_slide_url/a');
            $store_slide = implode(',', $store_slide);
            $store_slide_url = implode(',', $store_slide_url);
            M('store')->where("store_id", STORE_ID)->save(array('mb_slide' => $store_slide, 'mb_slide_url' => $store_slide_url));
            $this->success("操作成功", U('Store/mobile_slide'));
            exit;
        }
        if ($this->storeInfo['mb_slide']) {
            $store_slide = explode(',', $this->storeInfo['mb_slide']);
            $store_slide_url = explode(',', $this->storeInfo['mb_slide_url']);
        }
        $this->assign('store_slide', $store_slide);
        $this->assign('store_slide_url', $store_slide_url);
        return $this->fetch();
    }
    
    public function store_theme()
    {
        $template = include APP_PATH . 'seller/conf/style_inc.php';
        $theme = include APP_PATH . 'home/html.php';
        $storeGrade = M('store_grade')->where("sg_id", $this->storeInfo['grade_id'])->find();
        $this->assign('static_path', $theme['view_replace_str']['__STATIC__']);
        if ($storeGrade['sg_template_limit'] > 0)
            $template = array_slice($template, 0, $storeGrade['sg_template_limit']); //限制模板使用数量
        $this->assign('template', $template);
        return $this->fetch();
    }
    
    public function bind_class_list()
    {
        $where = [];
        $goods_class = Db::name('goods_category')->alias('gc')->getField('gc.id,gc.*');
        if ($this->store['bind_all_gc'] == 0) {
            $where['store_id'] = STORE_ID;
            $bind_class_list = Db::name('store_bind_class')->where($where)->select();
            $count = count($bind_class_list);
            for ($i = 0, $j = $count; $i < $j; $i++) {
                $bind_class_list[$i]['class_1_name'] = $goods_class[$bind_class_list[$i]['class_1']]['name'];
                $bind_class_list[$i]['class_2_name'] = $goods_class[$bind_class_list[$i]['class_2']]['name'];
                $bind_class_list[$i]['class_3_name'] = $goods_class[$bind_class_list[$i]['class_3']]['name'];
                $bind_class_list[$i]['commission'] = $goods_class[$bind_class_list[$i]['class_3']]['commission'];
            }
        } else {   //自营店铺
            $goods_class1 = Db::name('goods_category')->alias('gc')->where(['level' => 1])->getField('gc.id,gc.*');
            $goods_class2 = Db::name('goods_category')->alias('gc')->where(['level' => 2])->getField('gc.id,gc.*');
            $goods_class3 = Db::name('goods_category')->alias('gc')->where(['level' => 3])->getField('gc.id,gc.*');
            foreach ($goods_class1 as $k1 => $clv1) {
                foreach ($goods_class2 as $k2 => $clv2) {
                    if ($clv2['parent_id'] == $k1) {
                        foreach ($goods_class3 as $k3 => $clv3) {
                            if ($clv3['parent_id'] == $k2) {
                                $bind_class_list[$k3] = [
                                    "store_id" => STORE_ID,
                                    "commission" => $clv3['commission'],
                                    "class_1" => $k1,
                                    "class_2" => $k2,
                                    "class_3" => $k3,
                                    "state" => 1,
                                    'class_1_name' => $clv1['name'],
                                    'class_2_name' => $clv2['name'],
                                    'class_3_name' => $clv3['name'],
                                ];
                            }
                        }
                    }
                }
            }
        }
        $this->assign('bind_class_list', $bind_class_list);
        $this->assign('store', $this->store);
        return $this->fetch();
    }
    
    public function get_bind_class()
    {
        $cat_list = M('goods_category')->where("parent_id = 0")->select();
        $this->assign('cat_list', $cat_list);
        if (IS_POST) {
            $data = I('post.');
            $where = ['class_3' => $data['class_3'], 'store_id' => STORE_ID];
            if (M('store_bind_class')->where($where)->count() > 0) {
                respose(array('status' => -1, 'msg' => '您已申请过该类目'));
            }
            $data['store_id'] = STORE_ID;
            $data['commis_rate'] = M('goods_category')->where("id", $data['class_3'])->getField('commission');
            if (M('store_bind_class')->add($data)) {
                respose(array('status' => 1));
            } else {
                respose(array('status' => -1, 'msg' => '操作失败'));
            }
        }
        return $this->fetch();
    }
    
    public function bind_class_del()
    {
        $data = I('post.');
        $r = M('store_bind_class')->where(array('bid' => $data['bid']))->delete();
        if ($r) {
            $res = array('status' => 1);
        } else {
            $res = array('status' => -1, 'msg' => '操作失败');
        }
        respose($res);
    }
    
    public function navigation_list()
    {
        $res = Db::name('store_navigation')->where("sn_store_id", STORE_ID)->order('sn_sort')->page($_GET['p'] . ',10')->select();
        if ($res) {
            foreach ($res as $val) {
                $val['sn_new_open'] = $val['sn_new_open'] > 0 ? '开启' : '关闭';
                $val['sn_is_show'] = $val['sn_is_show'] > 0 ? '是' : '否';
                $list[] = $val;
            }
        }
        $this->assign('list', $list);
        $count = Db::name('store_navigation')->where("sn_store_id", STORE_ID)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }
    
    public function navigation()
    {
        $sn_id = I('sn_id/d', 0);
        if ($sn_id > 0) {
            $info = M('store_navigation')->where("sn_id", $sn_id)->find();
            $this->assign('info', $info);
        }
        return $this->fetch();
    }
    
    public function navigationHandle()
    {
        $data = I('post.');
        if ($data['act'] == 'del') {
            $r = M('store_navigation')->where('sn_id', $data['sn_id'])->delete();
            if ($r) exit(json_encode(1));
        }
        $data['sn_add_time'] = time();
        if (empty($data['sn_id'])) {
            $data['sn_store_id'] = STORE_ID;
            $r = M('store_navigation')->add($data);
        } else {
            $r = M('store_navigation')->where('sn_id', $data['sn_id'])->save($data);
        }
        if ($r) {
            $this->success("操作成功", U('Store/navigation_list'));
        } else {
            $this->error("操作失败", U('Store/navigation_list'));
        }
    }
    
    public function suppliers_list()
    {
        $map = array();
        $map['store_id'] = STORE_ID;
        $suppliers_name = trim(I('suppliers_name'));
        if ($suppliers_name) {
            $map['suppliers_name'] = array('like', "%$suppliers_name%");
        }
        $suppliers_list = M('suppliers')->where($map)->select();
        $this->assign('suppliers_list', $suppliers_list);
        return $this->fetch();
    }
    
    public function suppliers_info()
    {
        if (IS_POST) {
            $data = I('post.');
            $data['store_id'] = STORE_ID;
            if ($data['act'] == 'del') {
                Db::name('goods')->where(array('suppliers_id' => $data['suppliers_id']))->update(['suppliers_id' => 0]);
                $r = M('suppliers')->where(array('suppliers_id' => $data['suppliers_id']))->delete();
            } elseif ($data['suppliers_id'] > 0) {
                $r = M('suppliers')->where(array('suppliers_id' => $data['suppliers_id']))->save($data);
            } else {
                $r = M('suppliers')->add($data);
            }
            if ($r) {
                $this->ajaxReturn(1, 'json');
            } else {
                $this->ajaxReturn(0, 'json');
            }
        }
        $suppliers_id = I('suppliers_id/d');
        if ($suppliers_id) {
            $suppliers = M('suppliers')->where(array('suppliers_id' => $suppliers_id))->find();
            $this->assign('suppliers', $suppliers);
        }
        return $this->fetch();
    }
    
    public function goods_class()
    {
        $Model = M('store_goods_class');
        $res = $Model->where(['store_id' => STORE_ID])->select();
        $cat_list = $this->getTreeClassList(2, $res);
        $this->assign('cat_list', $cat_list);
        return $this->fetch();
    }
    
    public function goods_class_info()
    {
        $cat_id = I('get.cat_id/d', 0);
        $info['parent_id'] = I('get.parent_id/d', 0);
        if ($cat_id > 0) {
            $info = M('store_goods_class')->where("cat_id", $cat_id)->find();
        }
        $this->assign('info', $info);
        $parent = M('store_goods_class')->where(['parent_id' => 0, 'store_id' => STORE_ID])->select();
        $this->assign('parent', $parent);
        return $this->fetch();
    }
    
    public function goods_class_save()
    {
        $data = I('post.');
        if ($data['act'] == 'del') {
            $r = M('store_goods_class')->where(['cat_id|parent_id' => $data['cat_id']])->delete();
        } else {
            if (empty($data['cat_id'])) {
                $data['store_id'] = STORE_ID;
                $r = M('store_goods_class')->add($data);
            } else {
                $r = M('store_goods_class')->where('cat_id', $data['cat_id'])->save($data);
            }
        }
        if ($r) {
            $res = array('status' => 1);
        } else {
            $res = array('status' => -1, 'msg' => '操作失败');
        }
        respose($res);
    }
    
    public function store_im()
    {
        $chat_msg = M('chat_msg')->select();
        $this->assign('chat_msg', $chat_msg);
        return $this->fetch();
    }
    
    function store_collect()
    {
        $keywords = I('keywords');
        $map['store_id'] = STORE_ID;
        if (!empty($keywords)) {
            $map['user_name'] = array('like', "%$keywords%");
        }
        $count = M('store_collect')->where($map)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();
        $collect = M('store_collect')->where(array('store_id' => STORE_ID))->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('page', $show);
        $this->assign('collect', $collect);
        return $this->fetch();
    }
    
    public function store_decoration()
    {
        if (IS_POST) {
            //店铺装修设置
            $data = I('post.');
            M('store')->where(array('store_id' => STORE_ID))->save($data);
            $this->success("操作成功", U('Store/store_decoration'));
            exit;
        }
        $decoration = M('store_decoration')->where(array('store_id' => STORE_ID))->find();
        if (empty($decoration)) {
            $decoration = array('decoration_name' => '默认装修', 'store_id' => STORE_ID);
            $decoration['decoration_id'] = M('store_decoration')->add($decoration);
        }
        $this->assign('decoration', $decoration);
        return $this->fetch();
    }
    
    /**
     * 递归 整理分类
     *
     * @param int $show_deep 显示深度
     * @param array $class_list 类别内容集合
     * @param int $deep 深度
     * @param int $parent_id 父类编号
     * @param int $i 上次循环编号
     * @return array $show_class 返回数组形式的查询结果
     */
    private function getTreeClassList($show_deep = 2, $class_list, $deep = 1, $parent_id = 0, $i = 0)
    {
        static $show_class = array();//树状的平行数组
        if (is_array($class_list) && !empty($class_list)) {
            $size = count($class_list);
            if ($i == 0) $show_class = array();//从0开始时清空数组，防止多次调用后出现重复
            for ($i; $i < $size; $i++) {//$i为上次循环到的分类编号，避免重新从第一条开始
                $val = $class_list[$i];
                $cat_id = $val['cat_id'];
                $cat_parent_id = $val['parent_id'];
                if ($cat_parent_id == $parent_id) {
                    $val['deep'] = $deep;
                    $show_class[] = $val;
                    if ($deep < $show_deep && $deep < 2) {//本次深度小于显示深度时执行，避免取出的数据无用
                        $this->getTreeClassList($show_deep, $class_list, $deep + 1, $cat_id, $i + 1);
                    }
                }
                //if($cat_parent_id > $parent_id) break;//当前分类的父编号大于本次递归的时退出循环
            }
        }
        return $show_class;
    }
    
    /**
     * 三级分销设置
     */
    public function distribut()
    {
        // 每个店铺有一个分销 记录
        $store_distribut = M('store_distribut')->where("store_id", STORE_ID)->find();
        $result_url = I('result_url', 'Store/distribut');
        if (IS_POST) {
            $Model = M('store_distribut');
            $data = input('post.');
            $data['store_id'] = STORE_ID;
            if ($store_distribut)
                $Model->update($data);
            else
                $Model->insert($data);
            $this->success("操作成功", U($result_url));
            exit;
        }
        $distribut_set_by = M('config')->where("name = 'distribut_set_by'")->getField('value');
        $this->assign('distribut_set_by', $distribut_set_by);
        $this->assign('config', $store_distribut);
        return $this->fetch();
    }
    
    /*
     * 设置店铺经纬度
     * */
    public function getpoint()
    {
        if (IS_POST) {
            $coordinate = trim(I('coordinate/s'));
            $coordinate = explode(',', $coordinate);  //以,炸开获得经纬度
            if (empty($coordinate[0]) || $coordinate[0] == 0) {
                $this->success('请输入正确的经度');
            }
            if (empty($coordinate[1]) || $coordinate[1] == 0) {
                $this->success('请输入正确的纬度');
            }
            $data['longitude'] = $coordinate[0];
            $data['latitude'] = $coordinate[1];
            $res = M('store')->where(array('store_id' => STORE_ID))->save($data);  //修改
            if ($res)
                $this->success('成功');
            $this->success('失败');
            exit();
        }
        $coordinate = M('store')->field('longitude,latitude')->where("store_id", STORE_ID)->find();
        $this->assign('coordinate', $coordinate);
        return $this->fetch();
    }
    
    /**
     * 申请升级列表
     */
    public function store_reopen()
    {
        $StoreReopenModel = new  \app\common\model\StoreReopen();
        $count = $StoreReopenModel->where(['re_store_id' => STORE_ID])->count();
        $page = new Page($count, 20);
        $StoreReopenObj = $StoreReopenModel->where(['re_store_id' => STORE_ID])->order('re_id desc')->limit($page->listRows, $page->firstRow)->select();
        if ($StoreReopenObj) {
            $store_reopen = collection($StoreReopenObj)->append(['reopen_state'])->toArray();
        }
        $info['re_end_time'] = $re_end_time = $this->store['store_end_time'];  //到期时间
        $info['earlier'] = $earlier = 30; //可提前申请时间
        $info['start_apply_time'] = $re_end_time - ($earlier * 60 * 60 * 24);  //继续续期开始时间
        $reopen_count = Db::name('store_reopen')->where(['re_store_id' => STORE_ID, 're_state' => ['notIn', '-1,2']])->count();  //店铺等级
        if ($info['start_apply_time'] < time() && $reopen_count < 1) {    //是否可续签时间
            $info['apply_status'] = true;
        } else {
            $info['apply_status'] = false;
        }
        $this->assign('page', $page->show());
        $this->assign('info', $info);
        $this->assign('store_reopen', $store_reopen);
        return $this->fetch();
    }
    
    /**
     * 店铺当前等级，获取所有等级
     */
    public function getStoreGrade()
    {
        $store_grade_id = $this->storeInfo['grade_id'];
        $earlier = 30; //可提前申请时间
        $start_apply_time = $this->storeinfo['store_end_time'] - ($earlier * 60 * 60 * 24);  //继续续期开始时间
        if ($start_apply_time < time()) {    //是否可续签时间
            $store_grade['apply_status'] = true;
        } else {
            $store_grade['apply_status'] = false;
        }
        $grade = Db::name('store_grade')->alias('sg')->order('sg_id')->getField('sg_id,sg.*');
        $store_grade = $grade["$store_grade_id"];
        $this->assign('store_grade', $store_grade);
        $this->assign('grade', $grade);
        return $this->fetch();
    }
    
    /**
     * 申请升级店铺等级
     */
    public function applyStoreGrade()
    {
        $post_data = I('post.');
        $StoreLogic = new StoreLogic();
        $StoreLogic->setStoreInfo($this->storeInfo);
        $res = $StoreLogic->editStoreReopen($post_data);
        $this->ajaxReturn($res);
    }
    
    /**
     * 申请升级店铺等级
     */
    public function reopen_info()
    {
        $re_id = I('id/d', 0);
        !$re_id && $this->error('非法操作！！');
        $StoreReopenModel = new  \app\common\model\StoreReopen();
        $reopen = $StoreReopenModel::get(['re_id' => $re_id, 're_store_id' => STORE_ID]);
        $data = $reopen->append(['reopen_state'])->toArray();
        $this->assign('data', $data);
        return $this->fetch();
    }
    
    /*
     *关联版式
     */
    public function plate_list()
    {
        $sql = 'store_id=' . STORE_ID;
        if (I('get.p_name')) {
            $name = I('get.p_name');
            $sql .= ' and plate_name like "%' . $name . '%"';
            $this->assign('p_name', $name);
        }
        if (is_numeric(I('get.p_position'))) {
            $type = I('get.p_position');
            $sql .= ' and plate_position=' . $type;
            $this->assign('p_position', $type);
        }
        $list = M('store_plate')->where($sql)->select();
        $this->assign('list', $list);
        return $this->fetch();
    }
    
    public function plate_edit()
    {
        $id = I('get.plate_id');
        if ($id) {
            $info = M('store_plate')->where('plate_id=' . $id)->find();
            $this->assign('info', $info);
        }
        return $this->fetch();
    }
    
    //添加 更新操作
    public function plate_handle()
    {
        $data = I('post.');
        $id = $data['plate_id'];
        $data['store_id'] = STORE_ID;
        if (!$id) {
            unset($data['plate_id']);
            $res = M('store_plate')->add($data);
        }
        if ($id) {
            $res = M('store_plate')->where('plate_id=' . $id)->save($data);
        }
        
        if ($res) {
            $this->success("操作成功", U('Store/plate_list'));
        } else {
            $this->error("操作失败", U('Store/plate_list'));
        }
    }
    
    public function plate_delete()
    {
        $plate_id = I('post.plate_id');
        $res = Db::name('store_plate')->where('plate_id=' . $plate_id)->delete();
        if ($res) {
            echo "1";
        } else {
            echo "0";
        }
    }
    
    /**
     * 返回当前经销商所有列表.
     */
    public function loadAllStoreInfo(){
        $sellerWhere = ['seller_id'=>SELLER_ID];
        
        
        
        $data = [];
        $data['seller_list'] = [['seller_id'=>SELLER_ID,'seller_name'=>'经销商ESC']];
        $data['seller_address_list'] = Db::name('sellerAddress')->where($sellerWhere)->field('seller_address_id,seller_id,title,address')->select();
        $data['seller_address_list'][]=['seller_address_id'=>-99,'seller_id'=>999,'title'=>'其他','address'=>'其他'];
        
        $data['store_list'] = Db::name('store')
            ->alias('s')
            ->join('tp_store_apply sa','s.store_id=sa.store_id','LEFT')
            ->where('s.seller_id',SELLER_ID)->field('s.store_id,s.store_name,s.seller_id,sa.seller_address_id')->select();
        
        foreach ($data['store_list'] as &$store){
            if(!$store['seller_address_id']){
                $store['seller_address_id'] = -99;
            }
    
            $store['seller_name'] = '经销商ESC';
            foreach ($data['seller_address_list'] as $address){
                if($address['seller_address_id']==$store['seller_address_id']){
                   
                    if(isEmptyObject($address['title'])){
                        $store['address'] = $address['address'];
                    }else{
                        $store['address'] = $address['title'];
                    }
                    
                }
            }
            
            
            
        }
        
        $rt = ['status'=>1,'msg'=>'','data'=>$data];
        
        $this->ajaxReturn($rt);
        
    }
    
}