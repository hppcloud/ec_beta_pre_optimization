<?php
namespace app\seller\controller;

use app\seller\logic\GoodsCategoryLogic;
use app\seller\logic\StoreLogic;
use think\Db;
use think\Page;

class Index extends Base
{


    public function index()
    {
        $this->pushVersion();
        
        $seller = session('seller');
        
        $menu_list = getMenuList($seller['act_limits']);
        //从获取指定店铺的变成所有店铺的

        $count['handle_order'] = M('order')->where("seller_id = " .SELLER_ID. C('WAITSEND'))->count();//待处理订单
        
        $order_list = M('order')->where([
            'deleted'   => 0,
            'seller_id'  =>SELLER_ID,
            'add_time'  =>['gt',strtotime("-7 day")]
            ])->select();//最近7天订单统计
        $count['wait_shipping'] = $count['wait_pay'] = $count['wait_confirm'] = $count['refund_pay'] = 0;
        $count['refund_goods'] = $count['part_shipping'] = $count['order_sum'] = 0;
        $count['refund_pay'] = M('return_goods')->where("seller_id = " . SELLER_ID . " and type<2")->count();

        $count['refund_goods'] = M('return_goods')->where("seller_id = " . SELLER_ID . " and type>1")->count();
        if ($order_list) {
            $count['order_sum'] = count($order_list);
            foreach ($order_list as $v) {
                if ($v['order_status'] == 1 && $v['shipping_status'] == 0 ) {
                    $count['wait_shipping']++;
                }
                if ($v['pay_status'] == 0 && ($v['order_status'] != 3 && $v['order_status'] != 5)) {
                    $count['wait_pay']++;
                }
                if ($v['order_status'] == 0) {
                    $count['wait_confirm']++;
                }
                if ($v['shipping_status'] == 2) {
                    $count['part_shipping']++;
                }
            }
        }

        $count['goods_sum'] = $count['pass_goods'] = $count['warning_goods'] = $count['new_goods'] = 0;
        $count['prom_goods'] = $count['off_sale_goods'] = $count['below_goods'] = $count['verify_goods'] = 0;

        $stores = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');

        $count['goods_sum'] = M('goods')->where(array('store_id'=>array('in',$stores)))->count();
        $count['verify_goods'] = M('goods')->where(array('goods_state' => 0, 'store_id'=>array('in',$stores)))->count();
        $count['pass_goods'] = M('goods')->where(array('goods_state' => 1, 'store_id'=>array('in',$stores),'is_on_sale'=>1))->count();
        $count['below_goods'] = M('goods')->where(array('goods_state' => 2, 'store_id'=>array('in',$stores)))->count();
        $count['off_sale_goods'] = M('goods')->where(array('is_on_sale' => 2, 'store_id'=>array('in',$stores)))->count();
        $count['prom_goods'] = M('goods')->where(array('prom_id' => array('gt', 0), 'store_id'=>array('in',$stores)))->count();
        $count['new_goods'] = M('goods')->where(array('is_new' => 1, 'store_id'=>array('in',$stores)))->count();

        //$count['article'] =  M('article')->where(array('store_id'=>STORE_ID))->count();//文章总数

        $users = M('users')->where(array('user_id' => $seller['user_id']))->find();
        $seller['user_name'] = empty($users['email']) ? $users['mobile'] : $users['email'];
        //今天销售总额
        $count['yesterday_order'] = Db::name('order')->field('sum(order_amount) as order_amount_sum,count(order_id) as order_count')->whereTime('add_time', 'yesterday')->where(array('seller_id' => SELLER_ID, 'pay_status' => 1))->find();
        $count['month_order'] = Db::name('order')->field('sum(order_amount) as order_amount_sum,count(order_id) as order_count')->whereTime('add_time', 'month')->where(array('seller_id' => SELLER_ID, 'pay_status' => 1))->find();
        $count['comment'] = M('comment')->where(array('is_show' => 0, 'seller_id' => SELLER_ID))->count();//最新评论
        $count['consult'] = M('goods_consult')->where(array('seller_id' => SELLER_ID,'status'=>0,'parent_id'=>0))->count();//最新未回复咨询

        $order_sn_arr = Db::name('order')->whereTime('add_time','-1 month')->where(array('seller_id' => SELLER_ID, 'pay_status' => 1))->column('order_sn');
        
//        if($count)
       


        //库存记录里面，销售的库存变成了负数。。所以要修改下
        $count['hot_goods_list'] = Db::name('stock_log')->field('goods_id,goods_name,sum(stock) as goods_stock')->where('store_id','in',$stores)->where(['order_sn'=>array('in',$order_sn_arr),'stock'=>['<',0]])->whereTime('ctime','-1 month')->order('goods_stock')->group('goods_id,goods_name')->limit(20)->select();
//        $count['hot_goods_list'] = Db::name('stock_log')->field('goods_id,goods_name,sum(stock) as goods_stock')->where('store_id','in',$stores)->where(['order_sn'=>['<>',''],'stock'=>['>',0]])->whereTime('ctime','-1 month')->order('goods_stock desc')->group('goods_id,goods_name')->limit(10)->select();



        //$store = M('store')->where(array('store_id' => STORE_ID))->find();
        
        //不要这个鬼东西了 20180520
//        if (true||$store['store_warning_storage'] > 0) {
//        	$count['warning_storage'] = M('goods')->where(array('store_id' => STORE_ID, 'store_count' => array('lt', $store['store_warning_storage'])))->count();
//        } else {
//        	$count['warning_storage'] = '未设置';
//        }
        
        
//        $seller_level = Db::name('store_grade')->where('sg_id',$store['grade_id'])->getField('sg_name');
        
        $seller_group = Db::name('seller_group')->where('group_id', $seller['group_id'])->find();
        
//        $this->assign('seller_level', $seller_level);
//        $this->assign('seller_group', $seller_group);
        
        $this->assign('count', $count);
        $this->assign('menu_list', $menu_list);
        $this->assign('seller', $seller);
//        $this->assign('seller_info', $this->storeInfo);
        return $this->fetch();
    }


    /**
     * 商家查看消息
     */
    public function store_msg()
    {
        $where = "store_id=" . STORE_ID;
        $count = M('store_msg')->where($where)->count();
        $Page = new Page($count, 10);
        $show = $Page->show();

        $msg_list = M('store_msg')->where($where)->order('sm_id DESC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('msg_list', $msg_list);
        $this->assign('page', $show);// 赋值分页输出
        return $this->fetch();
    }

    /**
     * 删除操作
     */
    public function del_store_msg()
    {
        $sm_id = I('sm_id/d', 0);
        $seller = session('seller');
        if($seller['is_admin'] != 1){
            $this->ajaxReturn(['status'=>0,'msg'=>'你没有此项操作权限！！']);
        }
        $where = array('sm_id' => ['in', $sm_id], 'store_id' => STORE_ID);
        if(M('store_msg')->where($where)->delete())
            $this->ajaxReturn(['status'=>1,'msg'=>'操作成功!']);
        else
            $this->ajaxReturn(['status'=>0,'msg'=>'操作失败!']);
    }

    /**
     * 消息批量操作
     */
    public function store_msg_batch()
    {
        $action = I('action', 0);
        $sm_id = I('sm_id/a');
        $seller = session('seller');
        if($seller['is_admin'] != 1){
            $this->ajaxReturn(['status'=>0,'msg'=>'你没有此项操作权限！！']);
        }
        // 如果是标记已读
        if ($action == 'del' && !empty($sm_id)) {
            $where = array('sm_id' => ['in', implode(',', $sm_id)], 'store_id' => STORE_ID);
            M('store_msg')->where($where)->delete();
        }
        // 如果是标记已读
        if ($action == 'open' && !empty($sm_id)) {
            $where = array('sm_id' => ['in', implode(',', $sm_id)], 'store_id' => STORE_ID);
            M('store_msg')->where($where)->save(array('open' => 1));
        }
        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功!', 'result' => '']);
    }

    /**
     *  添加修改客服
     */
    public function store_service()
    {
        $url = 'http://xxxxx/index/api/apiCustomerAdd';
        // post提交
        if (IS_POST) {
            $pre = I('pre/a');
            $after = I('after/a');
            $working_time = I('working_time');

            foreach ($pre as $k => $v) {
                if (empty($v['name']) || empty($v['account']))
                    unset ($pre[$k]);
            }
            foreach ($after as $k => $v) {
                if (empty($v['name']) || empty($v['account']))
                    unset ($after[$k]);
            }
            $data = array(
                'store_presales' => serialize($pre),
                'store_aftersales' => serialize($after),
                'store_workingtime' => $working_time,
            );
            //想IM注册客服 售前 售后合并
            $customer = array_merge($pre,$after);
            foreach($customer as $key => $value){
                $customer[$key]['work_time'] = $working_time;
            }
            //组合需要发送的数据
            $send_data = [
                'store_id' => STORE_ID,
                'customer' => $customer,
            ];
            //$send_data = json_encode($send_data);
            $send_data = http_build_query($send_data);
            $res = $this->sendToImCustomer($url,$send_data);

            M('store')->where("store_id", STORE_ID)->save($data);
            $this->success('修改成功');
            exit();
        }
        //
        $store = M('store')->where("store_id", STORE_ID)->find();
        $store['store_presales'] = unserialize($store['store_presales']);
        $store['store_aftersales'] = unserialize($store['store_aftersales']);
        $this->assign('store',$store);
        return $this->fetch();
    }

    //向im 发送数据 客服数据
    public function sendToImCustomer($url,$data)
    {
        //初始化一个curl会话
        $ch = curl_init();
        //设置curl传输选项。
        curl_setopt($ch, CURLOPT_URL, $url); 	//定义表单提交地址
        curl_setopt($ch, CURLOPT_POST, true);   	//定义提交类型 1：POST ；0：GET
        //curl_setopt($ch, CURLOPT_HEADER, 0); 	//定义是否显示状态头 1：显示 ； 0：不显示
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	//定义是否直接输出返回流
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 	//定义提交的数据，这里是XML文件
        //执行一个curl会话
        $res = curl_exec($ch);
        //关闭一个curl会话
        curl_close($ch);
        return $res;
    }

    public function pushVersion()
    {
        if (!empty($_SESSION['isset_push']))
            return false;
        $_SESSION['isset_push'] = 1;
        error_reporting(0);//关闭所有错误报告
        $app_path = dirname($_SERVER['SCRIPT_FILENAME']) . '/';
        $version_txt_path = $app_path . '/Application/Admin/Conf/version.txt';
        $curent_version = file_get_contents($version_txt_path);

        $vaules = array(
            'domain' => $_SERVER['SERVER_NAME'],
            'last_domain' => $_SERVER['SERVER_NAME'],
            'key_num' => $curent_version,
            'install_time' => INSTALL_DATE,
            'cpu' => '0001',
            'mac' => '0002',
            'serial_number' => SERIALNUMBER,
        );
        $url = "http://service.tp" . '-' . "shop" . '.' . "cn/index.php?m=Home&c=Index&a=user_push&" . http_build_query($vaules);
        stream_context_set_default(array('http' => array('timeout' => 3)));
        file_get_contents($url);
    }

    /**
     * ajax 修改指定表数据字段  一般修改状态 比如 是否推荐 是否开启 等 图标切换的
     * table,id_name,id_value,field,value
     */
    public function changeTableVal()
    {
        
        
        $table = I('table'); // 表名
        $id_name = I('id_name'); // 表主键id名
        $id_value = I('id_value'); // 表主键id值
        $field = I('field'); // 修改哪个字段
        $value = I('value'); // 修改字段值
        
        $db = Db::name($table);
        $where = [$id_name => $id_value];

        if (strtolower($table)!='seller'&&method_exists($db, 'getTableFields') && in_array('seller_id', $db->getTableFields())) {
            $where['seller_id'] = SELLER_ID;
        }
        
        M($table)->where($where)->save(array($field => $value)); // 根据条件保存修改的数据
    }
 
    /**
     * 获取店铺商品分类
     */
    public function goods_category()
    {
        $parent_id = input('parent_id/d', 0); // 商品分类 父id
        $GoodsCategoryLogic = new GoodsCategoryLogic();
        $GoodsCategoryLogic->setStore($this->storeInfo);
        $goods_category_list = $GoodsCategoryLogic->getStoreGoodsCategory($parent_id);
        $this->ajaxReturn($goods_category_list);
    }

    /**
     * 添加快捷操作
     */
    function quicklink_add() {
    	if(!empty($_POST['item'])) {
    		$_SESSION['seller_quicklink'][$_POST['item']] = $_POST['item'];
    	}
    	$this->_update_quicklink();
    	echo 'true';
    }
    
    /**
     * 删除快捷操作
     */
    function quicklink_del() {
    	if(!empty($_POST['item'])) {
    		unset($_SESSION['seller_quicklink'][$_POST['item']]);
    	}
    	$this->_update_quicklink();
    	echo 'true';
    }
    
    private function _update_quicklink() {
    	$quicklink = implode(',', $_SESSION['seller_quicklink']);
    	$update_array = array('seller_quicklink' => $quicklink);
    	$condition = array('seller_id' => $_SESSION['seller_id']);
    	M('seller')->where($condition)->save($update_array);
    }
    
    public function likeSerach(){
        $params = $this->request->param();
        $rule = [
            'table'=>'require',
            'column'=>'require',
            'val'=>'require',
            'field'=>'require',
            'len'=>'require',
        ];
        $rt = $this->validate($params,$rule);
        if(!$rt){
            $this->ajaxReturn(['status' => 1, 'msg' => json_encode($rt), 'result' => '']);
        }
        try{
            $db = Db::name($params['table']);
            $query = $db->whereLike($params['column'],'%'.$params['val'].'%');
            
            //如果有这个字段，则只能查自己的
            if (method_exists($db, 'getTableFields') && in_array('seller_id', $db->getTableFields())) {
                $query = $query->where('seller_id',SELLER_ID);
            }
            
            if($params['field']&&$params['field']!=''){
                $query->field($params['field']);
            }
    
            $lists = $query->limit($params['len'])->select();
    
            
            $this->ajaxReturn(['status' => 1, 'msg' => 'success', 'result' => $lists]);
            
        }catch (Exception $e){
            $this->ajaxReturn(['status' => 0, 'msg' =>$e->getMessage(), 'result' => '']);
        }
        
    }
    
    
    public function selectStore($forward=null){
//        if(!$forward){
//            $this->error('跳转参数必传');
//        }
//
//        $this->assign('forward',$forward);
        
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
        return $this->fetch('select_store');
    }

    public function testrun(){
        $rt = Db::name('goods')->where('goods_id',55555555555)->update(['goods_name'=>3]);
        var_dump($rt);
    }


    public function filterBadGoods()
    {
//        $list = Db::name('goods')
//            ->alias('g')
//            ->join('tp_spec_goods_price sp','g.goods_id=sp.goods_id','LEFT')
//            ->field('g.goods_id,g.goods_name,sp.key,sp.key_name')
//            ->where('sp.key',null)
//            ->select();
        $list = Db::name('goods')
            ->alias('g')
            ->join('tp_spec_goods_price sp', 'g.goods_id=sp.goods_id', 'LEFT')
            ->field('g.goods_name,sp.key,sp.key_name')
            ->where('sp.key', null)
            ->group('g.goods_name,sp.key,sp.key_name')
            ->select();
        echo '<pre>';
        foreach ($list as $goods) {
            echo $goods['goods_name'] . '<br/>';
        }
        echo '总行数:' . count($list);
    }

//    public function repaiFailGoodsSpec(){
//        $XRs = [
//            'goods_name'=>'iPhone XR',
//            'ids'=>'80566,80584,80597,80607,80658,80730,80738,80752,80765,80786,80821,80829,80894,80915,80931,80946,97861,97879,97892,97902,97953,98025,98033,98047,98060,98081,98116,98124,98189,98210,98226,98241,100628,100646,100659,100669,100720,100792,100800,100814,100827,100848,100883,100891,100956,100977,100993,101008'
//        ];
//
//        $Maxs = [
//            'goods_name'=>'iPhone XS Max',
//            'ids'=>'81538,81556,81569,81579,81630,81702,81710,81724,81737,81758,81793,81801,81866,81887,81903,81918,92673,92682,92688,92700,92715,92727,92744,92746,92767,92772,92774,92780,92785,92790,92792,92794,92819,92822,92864,92913,92916,92921,92995,92996,93177'
//        ];
//
//        $XSs = [
//            'goods_name'=>'iPhone XS',
//            'ids'=>'81052,81070,81083,81093,81144,81216,81224,81238,81251,81272,81307,81315,81380,81401,81417,81432,92120,92129,92135,92147,92162,92174,92191,92193,92215,92220,92222,92228,92233,92238,92240,92242,92267,92270,92312,92361,92364,92369,92443,92444,92625'
//        ];
//
//        $todolist = [$XRs,$Maxs,$XSs];
//
//
//        set_time_limit(0);
////        ob_end_clean();
////        ob_implicit_flush(1);
//
//        echo '<pre>';
//        Db::startTrans();
////        1.获取规格信息
//        foreach ($todolist as $item){
////            $list = Db::name('goods')
////                ->where('goods_name',$item['goods_name'])
////                ->where('goods_id','in',$item['ids'])
////                ->field('goods_id,store_id,seller_id')
////                ->select();
//
//            $list = explode(',',$item['ids']);
//
//            foreach ($list as $id){
//
//
//
//                $spec_price_arr= Db::name('SpecGoodsPrice')->where('goods_id',$id)->order('item_id asc')->select();
//
//                //获取所有的
//
//                $spec_price_list = [];
//                foreach ($spec_price_arr as $spec_price){
//                    $tempArr = explode(' ',$spec_price['key_name']);
//                    sort($tempArr);
//                    $spec_price['key_name_sort'] = implode(' ',$tempArr);
//                    $spec_price_list[] = $spec_price;
//
//                }
//
//
//                $spec_price_list2 = array_merge($spec_price_list,[]);
//
////                var_dump($spec_price_list[18],$spec_price_list2[18]);
//
//
//                foreach ($spec_price_list as $key=>$spec_price){
//
//                    foreach ($spec_price_list2 as $key2=>$spec_price_2){
//                        echo $key.'-----------------'.$key2.'<br/>';
//                        echo $spec_price['item_id'].'-----------------'.$spec_price_2['item_id'].'<br/>';
//                        echo $spec_price['key_name_sort'].'-----------------'.$spec_price_2['key_name_sort'].'<br/>';
//                        if($spec_price['item_id']==$spec_price_2['item_id']){
//                            unset($spec_price_2[$key2]);
//                            continue;
//                        }
//
//                        //如果key_name_sort一样的两行记录那就是实锤了。
//                        if($spec_price['key_name_sort']==$spec_price_2['key_name_sort']&&$spec_price['item_id']<$spec_price_2['item_id']){
//
//
//                            //订单购买表
//                            $rt1 = Db::name('OrderGoods')->where('spec_key',$spec_price['key'])->where('goods_id',$spec_price['goods_id'])->update(['spec_key'=>$spec_price_2['key'],'spec_key_name'=>$spec_price_2['key_name']]);
//                            //退货表
//                            $rt2 = Db::name('ReturnGoods')->where('rch_id',$spec_price['item_id'])->update(['rch_id'=>$spec_price_2['item_id']]);
//
//                            //退货表
//                            $rt3 = Db::name('Cart')->where('spec_key',$spec_price['key'])->update(['spec_key'=>$spec_price_2['key']]);
//
//                            $rt4 = Db::name('SpecGoodsPrice')->where('item_id',$spec_price['item_id'])->delete();
//                            trace('删除多余的记录：'.$spec_price['item_id'].':'.json_encode($spec_price).'替换新的:'.json_encode($spec_price_2));
//                            echo 'delted item_id'.$spec_price_list[$key]['item_id'];
//                            unset($spec_price_list[$key]);
//                            if($rt1===false||$rt2===false||$rt3===false||$rt4===false){
//                                echo '处理错误';
//                                Db::rollback();
//                                die;
//                            }
//
//
//                        }
//                    }
//                }
//
//
//            }
//
////            ob_end_flush();
////            if(ob_get_level()>0){
////                ob_flush();
////            }
////
////            flush();
//        }
//
//        Db::commit();
//
////        2.把key_name打乱按照字符顺序重排，找出重复的规格。
////
////        3.用新的规格替换掉旧的（要依靠goods_id和错误的key来判断）
////            订单购买表order_goods+购物车表cart+退货表return_goods
////
////        4.删除旧的
//
//
//
//
//    }

    /**
     * 统计指定的商品id筛选错误的数据
     */
//    public function countFailGoodsSpec(){
//
//        set_time_limit(0);
//        ob_end_clean();
//        ob_implicit_flush(1);
//        $goods_name = input('goods_name',null);
//
//        if(!$goods_name||$goods_name==''){
//            echo '需要输入有效的goods_name';
//            die;
//        }
//
//        $list = Db::name('goods')
//            ->alias('g')
//            ->join('tp_spec_goods_price p','g.goods_id=p.goods_id')
//            ->where('g.goods_name',$goods_name)
//            ->field('g.goods_id,g.store_id,g.seller_id,p.key,p.item_id')
////            ->field('g.goods_id,g.goods_name,g.store_id,g.seller_id,p.key,p.key_name,p.item_id')
//            ->group('item_id')
////            ->limit(10)
//            ->select();
//
//
//        $require_handle_list = [];
//        foreach ($list as &$goods){
//            $goods['count_row'] = 1;
//            foreach ($list as $key=>$goods2){
//                if($goods2['goods_id']==$goods['goods_id']){
//                    if($goods2['item_id']!=$goods['item_id']){
//                        $goods['count_row']++;
//
//
//                    }
//                    unset($list[$key]);
//                }
//            }
//
//            echo json_encode($goods).'<br/>';
//
//            if (!in_array($goods['goods_id'], $require_handle_list) && $goods['count_row'] > 17) {
//                $require_handle_list[] = $goods['goods_id'];
//            }
//
//
//            ob_end_flush();
//            if(ob_get_level()>0){
//                ob_flush();
//            }
//
//            flush();
//        }
//
////        var_dump($require_handle_list);die;
//        echo implode(',',$require_handle_list);
//
////        foreach ($list as $goods){
////            echo json_encode($goods).'<br/>';
////        }
//    }
    
    
    /**
     * 转移店铺
     */
    public function transferStore(){
        set_time_limit(0);
 
        $storeLogic = new StoreLogic();

        $todoList = [
            [
                'store_id' => 104,
                'old_seller_id'=>1,//现在的seller_id
                'new_seller_id' => 20//要替换的seller_id
            ],
            [
                'store_id' => 2181,
                'old_seller_id'=>1,//现在的seller_id
                'new_seller_id' => 20//要替换的seller_id
            ],
        ];

        $len = count($todoList);
        $success = $error = 0;
        $begin = time();
        ob_end_clean();
        ob_implicit_flush(1);
        foreach ($todoList as $item){
            $info = json_encode($item);
            echo $info;
//            $rt = $this->repairCopyGoodsSpecInfoByStore($item['store_id']);
            $rt = $storeLogic->transferStore_test($item['store_id'], $item['new_seller_id'], $item['old_seller_id']);
            echo '处理完毕,处理结果';
            if($rt['code']===1){
                $rt_txt =  '成功';
                $success++;
            }else{
                $rt_txt = '失败:'.$rt['msg'];
                $error++;
            }
            echo $rt_txt;
            trace('修改店铺绑定经销商:'.$info.'操作结果:'.$rt_txt);
            echo '<br/>';

            ob_end_flush();
            if (ob_get_level() > 0) {
                ob_flush();
            }

            flush();

        }
        $end = time();

        $timeLine = $end-$begin;
        echo '总共操作:'.$len.'个店铺,'.'成功:'.$success.'个,'.'失败:'.$error.'个'.'耗时:'.($timeLine/60).'分钟';


    }


    public function repairCopyGoodsSpecInfoByGoodsName()
    {


        $goods_name = input('goods_name', null);

        if (!$goods_name || $goods_name == '') {
            echo '需要输入有效的goods_name';
            die;
        }


        $tmpl_goods_id = input('tmpl_goods_id', null);

        if (!$tmpl_goods_id || $tmpl_goods_id == '') {
            echo '需要输入有效的tmpl_goods_id';
            die;
        }


        $this->repairCopyGoodsSpecInfo($goods_name, $tmpl_goods_id);


    }
    
    
    /**
     * 修复指定的商品。用于输入多个名称和价格、指定同一个id
     * 比如ipad wifi 和ipad wlan是同一个商品，应对同一个模板id.
     *
     */
    public function repairCopyGoodsSpecInfoByMutiName()
    {
    
        $goods_name = input('goods_name', null);
    
        if (!$goods_name || $goods_name == '') {
            echo '需要输入有效的goods_name';
            die;
        }
        
        $goods_name_arr = explode(',',$goods_name);
        
    
    
        $tmpl_goods_id = input('tmpl_goods_id', null);
    
        if (!$tmpl_goods_id || $tmpl_goods_id == '') {
            echo '需要输入有效的tmpl_goods_id';
            die;
        }
    
        $price = input('price', null);
    
        if (!$price || $price == '') {
            echo '需要输入有效的price';
            die;
        }
        
//       var_dump($goods_name_arr,$price,$tmpl_goods_id);die;
        
        set_time_limit(0);
        
        //读取所有的商品,按照时间排序
        //先读取九月十号之后的1536914262
//        ->where('on_time', 'gt', 1536681600)
//        ->where('goods_id', 'in', [93450, 615])
        $goods = Db::name('goods')->where('goods_id', $tmpl_goods_id)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->find();
    
        Db::name('goods')->where('goods_name','in', $goods_name_arr)->where('market_price',$price)->update(['goods_name'=>$goods_name_arr[0]]);
        
        
        $allGoodsList = Db::name('goods')->where('goods_name','in', $goods_name_arr)->where('market_price',$price)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->select();
        
        ob_end_clean();
        ob_implicit_flush(1);
        
        foreach ($allGoodsList as $goods2) {
            
            //确认是同一个复制的
            //刚刚已经处理完一样的了，现在应该处理不一样的
            
            
            //&&$goods2['on_time']!=$goods['on_time']&&
//            if ($goods2['goods_id'] != $goods['goods_id'] && $goods2['goods_name'] == $goods['goods_name']) {
            if ($goods2['goods_id'] != $goods['goods_id']) {

//                    var_dump($goods2,$goods);continue;
                echo str_repeat("<div></div>", 200) . $goods2['goods_id'] . '------------' . $goods['goods_id'] . '<br/>';
                $rt = $this->afterSave_copy($goods2, $goods2['store_id'], $goods);//重走长征路,复制一下

//                $rt = [];
//                $rt['status'] = rand(0, 2) == 1 ? 1 : 0;
                if ($rt['status'] == 1) {

//                    $is_success[] = $goods2['goods_id'];
                    trace('已经修改成功的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id']);
                    echo str_repeat("<div></div>", 200) . '已经修改成功的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id'] . '<br/>';
                } else {
                    trace('已经修改失败的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id']);
                    echo str_repeat("<div></div>", 200) . '已经修改失败的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id'] . $rt['msg'] . '<br/>';
                }
                
                unset($goods2);
                
            } else {
//                    echo '继续下一行'.'<br/>';
            }
            
            ob_end_flush();
            if (ob_get_level() > 0) {
                ob_flush();
            }
            
            flush();
            
        }

        
    }


    public function repairCopyGoodsSpecInfo($goods_name, $tmpl_goods_id)
    {


        set_time_limit(0);

        //读取所有的商品,按照时间排序
        //先读取九月十号之后的1536914262
//        ->where('on_time', 'gt', 1536681600)
//        ->where('goods_id', 'in', [93450, 615])
        $goods = Db::name('goods')->where('goods_id', $tmpl_goods_id)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->find();

        $allGoodsList = Db::name('goods')->where('goods_name', $goods_name)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->select();
        
        ob_end_clean();
        ob_implicit_flush(1);

        foreach ($allGoodsList as $goods2) {

            //确认是同一个复制的
            //刚刚已经处理完一样的了，现在应该处理不一样的


            //&&$goods2['on_time']!=$goods['on_time']&&
            if ($goods2['goods_id'] != $goods['goods_id'] && $goods2['goods_name'] == $goods['goods_name']) {

//                    var_dump($goods2,$goods);continue;
                echo str_repeat("<div></div>", 200) . $goods2['goods_id'] . '------------' . $goods['goods_id'] . '<br/>';
                $rt = $this->afterSave_copy($goods2, $goods2['store_id'], $goods);//重走长征路,复制一下

//                $rt = [];
//                $rt['status'] = rand(0, 2) == 1 ? 1 : 0;
                if ($rt['status'] == 1) {

//                    $is_success[] = $goods2['goods_id'];
                    trace('已经修改成功的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id']);
                    echo str_repeat("<div></div>", 200) . '已经修改成功的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id'] . '<br/>';
                } else {
                    trace('已经修改失败的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id']);
                    echo str_repeat("<div></div>", 200) . '已经修改失败的id:' . $goods2['goods_id'] . '，模板商品id是:' . $goods['goods_id'] . $rt['msg'] . '<br/>';
                }

                unset($goods2);

            } else {
//                    echo '继续下一行'.'<br/>';
            }

            ob_end_flush();
            if (ob_get_level() > 0) {
                ob_flush();
            }

            flush();

        }



        $is_success = [];


//        foreach ($tmpl_goods as $key => $goods) {
//
//
////            if (in_array($goods['goods_id'], $is_success)) {
//////                echo '商品已经复制过了id是:'.$goods['goods_id'].'<br/>';
//////                trace('商品已经复制过了id是:'.$goods['goods_id']);
////
////                continue;
////            }
//
//
//            $is_success[] = $goods['goods_id'];
//
//
//
//
//        }
//
//        die;


    }

    protected function afterSave_copy($goods, $store_id, $tmpl_goods)
    {

        $goods_id = $goods['goods_id'];
        $tmpl_goods_id = $tmpl_goods['goods_id'];



        //先确保模板商品所在的经销商有的规格，需要复制的经销商也有。

        $type_id = M('goods_category')->where("id", $tmpl_goods['cat_id3'])->getField('type_id'); // 找到这个分类对应的type_id
        if (empty($type_id)) {
            return ['status' => 0, 'msg' => '目标商品类别不能为空', 'result' => null];
        }
        $spec_id_arr = M('spec_type')->where("type_id", $type_id)->getField('spec_id', true); // 找出这个类型的 所有 规格id
        if (empty($spec_id_arr)) {
            return ['status' => 0, 'msg' => '目标商品类别绑定的规格为空', 'result' => null];
        }

        $linkSpecArr = [];  //33=>343  用旧的做键名，新的做键值。来记录下来
        $specList = D('Spec')->where("id", "in", implode(',', $spec_id_arr))->order('`order` desc')->select(); // 找出这个类型的所有规格

        if ($specList) {
            $tempArr = [];
            foreach ($specList as $k => $v) {

                //去拿所有的目标商品关联的specItem
                $rows = Db::name('SpecItem')->where(['spec_id' => $v['id'], 'seller_id' => $tmpl_goods['seller_id']])->order('id asc')->select(); // 获取规格项

                foreach ($rows as &$row) {
                    $tempOldId = $row['id'];
                    unset($row['id']);
                    $row['seller_id'] = $goods['seller_id'];//从已经复制好的商品里面读seller_id是对的
                    //查看自己有没有
                    $isHas = Db::name('SpecItem')->where($row)->find();
                    if (!$isHas) {

                        $newRowId = Db::name('SpecItem')->insertGetId($row);//写进去
                        echo '经销商没有规格记录:' . json_encode($tempOldId) . ',新增新的:' . $newRowId . '内容是：' . $newRowId;
                    } else {
                        $newRowId = $isHas['id'];
                        echo '经销商有与模板规格相同的记录:' . json_encode($tempOldId) . ',查找到:' . $newRowId . '内容是：' . $newRowId;
                    }
                    $linkSpecArr[$tempOldId] = $newRowId;

                }

            }
        }


        //提前取出来所有的图片和颜色组合
//        $hasSpecImage = Db::name('spec_image')->where(['goods_id' => $tmpl_goods_id])->select();
//        $hasSpecColor = Db::name('spec_color')->where(['goods_id' => $tmpl_goods_id])->select();

        // 商品规格价钱处理
        $hasSpecGoodsPrice = Db::name('spec_goods_price')->where(['goods_id' => $tmpl_goods_id])->order('item_id asc')->select();


        Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->delete();

        foreach ($hasSpecGoodsPrice as $tmplSpec) {
            unset($tmplSpec['item_id']);
            $tmplSpec['goods_id'] = $goods_id;
            $tmplSpec['store_id'] = $store_id;


            $keyArr = explode('_', $tmplSpec['key']);//key组成的数组 biubiu 88_99_110_120

            //用对照表就完成了，不用那么费事
            foreach ($keyArr as $j => $item) {
                foreach ($linkSpecArr as $k => $link) {
                    //一旦有新旧对应的，这里只能是颜色之类的。就替换掉,而所有商品的属性肯定都在这里了
                    if ((int)$item == (int)$k) {

                        $hasSpecImage = Db::name('spec_image')->where(['goods_id' => $tmpl_goods_id, 'spec_image_id' => $item])->find();
                        if ($hasSpecImage) {
                            $hasSpecImage['goods_id'] = $goods_id;
                            $hasSpecImage['store_id'] = $store_id;
                            $hasSpecImage['spec_image_id'] = $link;

                            $has = Db::name('spec_image')->where(['goods_id' => $goods_id, 'store_id' => $store_id, 'spec_image_id' => $link])->find();
                            if (!$has) {
                                Db::name('spec_image')->insert($hasSpecImage);
                            } else {
                                if ($has && $has['src'] != $hasSpecImage['src']) {
                                    Db::name('spec_image')->where(['goods_id' => $goods_id, 'spec_image_id' => $has['spec_image_id']])->update(['src' => $hasSpecImage['src']]);
                                }

                            }


                        }

                        $hasSpecColor = Db::name('spec_color')->where(['goods_id' => $tmpl_goods_id, 'spec_color_id' => $item])->find();
                        if ($hasSpecColor) {
                            $hasSpecColor['goods_id'] = $goods_id;
                            $hasSpecColor['store_id'] = $store_id;
                            $hasSpecColor['spec_color_id'] = $link;
                            $has = Db::name('spec_color')->where(['goods_id' => $goods_id, 'store_id' => $store_id, 'spec_color_id' => $link])->find();
                            if (!$has) {
                                Db::name('spec_color')->insert($hasSpecColor);
                            } else {
                                if ($has && $has['spec_color'] != $hasSpecColor['spec_color']) {
                                    Db::name('spec_color')->where(['spec_color_id' => $has['spec_color_id'], 'goods_id' => $goods_id])->update(['spec_color' => $hasSpecColor['spec_color']]);
                                }

                            }


                        }
                        $keyArr[$j] = $link;//用新的替换掉老的
                    }
                }

            }

            sort($keyArr);

            $tmplSpec['key'] = join('_', $keyArr);

            $isHasSpecPrice = Db::name('spec_goods_price')->where(['goods_id' => $goods_id, 'key' => $tmplSpec['key']])->find();
            if (!$isHasSpecPrice) {
                Db::name('spec_goods_price')->insert($tmplSpec);
            }


        }


        refresh_stock($goods_id); // 刷新商品库存

        return ['status' => 1, 'msg' => '修改成功', 'result' => null];


    }


    public function previewGoodsInfo()
    {
        $link = getGoodsPreviewUrl(input('goods_id'));
        $this->redirect($link);

    }


    public function repaierGoodsSku()
    {

//        错误SKU	正确SKU
//MNYG2CH/A	MNYJ2CH/A
//MNYL2CH/A	MNYN2CH/A
//MNYJ2CH/A	MNYL2CH/A
//MNYN2CH/A	MNYG2CH/A


        set_time_limit(0);
        $todo = [
            [
//                'goods_id' => 1212,
                'yanse' => '银色',
                'yingpan' => '512GB',
                'fail_sku' => 'MNYG2CH/A',
                'sku' => 'MNYJ2CH/A'
            ],
            [
//                'goods_id' => 1212,
                'yanse' => '玫瑰金色',
                'yingpan' => '512GB',
                'fail_sku' => 'MNYL2CH/A',
                'sku' => 'MNYN2CH/A'
            ],
            [
//                'goods_id' => 1212,
                'yanse' => '金色',
                'yingpan' => '512GB',
                'fail_sku' => 'MNYJ2CH/A',
                'sku' => 'MNYL2CH/A'
            ],
            [
//                'goods_id' => 1212,
                'yanse' => '深空灰色',
                'yingpan' => '512GB',
                'fail_sku' => 'MNYN2CH/A',
                'sku' => 'MNYG2CH/A'
            ]
        ];

        Db::startTrans();
        $ok_item_arr = [];


        ob_end_clean();
        ob_implicit_flush(1);


        foreach ($todo as $goods) {
            //->where('goods_id', $goods['goods_id'])//去掉商品限制，整个库里面的
            $skuList = Db::name('SpecGoodsPrice')
                ->alias('s')
                ->join('goods g', 's.goods_id=g.goods_id')
                ->where('g.goods_name', 'MacBook')
                ->where('s.sku', $goods['fail_sku'])->field('s.*,g.goods_name')->select();

            //挨个修改
            foreach ($skuList as $sku) {
                echo '记录:' . json_encode($sku['item_id']) . '<br/>';
                if (!in_array($sku['item_id'], $ok_item_arr)) {

                    $key_name = $sku['key_name'];
                    $key_name_arr = explode(' ', $key_name);
                    $isLike = 0;
                    foreach ($key_name_arr as $str) {
                        if ($str == '硬盘:' . $goods['yingpan'] || $str == '颜色:' . $goods['yanse']) {
                            $isLike++;
                        }

                    }

                    if ($isLike < 2) {
                        continue;
                    }

                    $rt = Db::name('SpecGoodsPrice')->where('item_id', $sku['item_id'])->update(['sku' => $goods['sku']]);
                    if ($rt !== false) {
                        $ok_item_arr[] = $sku['item_id'];
                        trace('#特殊情况批量修改规格#处理成功:' . $sku['item_id']);
                        echo '处理成功:' . json_encode($sku['item_id']) . '原sku:' . $goods['fail_sku'] . '====================>新sku:' . $goods['sku'] . '<br/>';
                    } else {
                        echo '处理失败:' . json_encode($sku['item_id']) . '原sku:' . $goods['fail_sku'] . '====================>新sku:' . $goods['sku'] . '<br/>';
                        Db::rollback();
                        die;
                    }



                }


                ob_end_flush();
                if (ob_get_level() > 0) {
                    ob_flush();
                }

                flush();


            }

        }

        Db::commit();


    }

}