<?php

namespace app\seller\controller;

use app\common\logic\ModuleLogic;
use app\seller\logic\AdminLogic;
use think\Cache;
use think\Page;
use think\Verify;
use think\Db;
use think\Session;

use app\admin\logic\StoreLogic;

use think\AjaxPage;

class Admin extends Base
{
    /**
     * 体验账号管理
     */
    public function visitor(){

        $where = array();
        //如果有搜索就指定店铺啦
        if(I('store_id')){
            $where['store_id'] = I('store_id');
        }else{
            $storesIds = Db::name('store')->where('seller_id',SELLER_ID)->column('store_id');
            $where['store_id'] = array('in',$storesIds);
        }
        
        if(I('email',null)){
            $where['email'] = I('email');
        }
//        ->field('store_id,store_name,validate_channel,store_domain')
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->select();

        $this->assign('stores',$stores);
    
        $model = M('Users');
        $count = $model->where($where)->where('is_visitor','<>',0)->count();//默认为0 ，不为0就行了。1是启用，-1是禁用
        $Page = new Page($count, 10);
        
        $userList = Db::name('Users')->where($where)->where('is_visitor','<>',0)->field('store_id,email,user_id,mobile,is_visitor,reg_time')->order('user_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
    
        $show = $Page->show();
        $this->assign('page', $show);// 赋值分页输出
        
        foreach ( $userList as &$user){
            foreach ($stores as $store){
                if($store['store_id']==$user['store_id']){
                    $user['store_name'] = $store['store_name'];
                    break;
                }
            }
        }
        
        $this->assign('list', $userList);
        return $this->fetch();
        
    }
    
    
    public function visitorAddEdit(){
        
        
        if(IS_GET&&!I('act',null)){
            
            $user_id = I('get.user_id/d');
            $store_name = I('store_title');
            $filter = '';
            if($store_name){
                $filter['store_name'] = array('like','%'.$store_name.'%');
            }
           
            
            if ($user_id  > 0) {

                $user = Db::name('users')->where(array('user_id ' => $user_id , 'is_visitor' => array('<>',0)))->find();
                $filter['store_id'] = $user['store_id'];
                if (!$user){
                    $this->error('找不到该体验者账户', U('Seller/admin/visitor'));
                }
                $store = Db::name('store')->where($filter)->find();
                $this->assign('store',$store);
                $this->assign('user', $user);
            }
            $filter['seller_id'] = SELLER_ID;
            
            $stores = Db::name('store')->where($filter)->field('store_id,store_name,validate_channel,store_email')->select();
            $this->assign('stores',$stores);
    
            
            
            
            return $this->fetch('_visitor');
        }
    
    
        $act = I('act',null);
        
        if($act){
    
    
            $user_id = I('get.user_id/d');
            $user = M('users')->where(array('user_id ' => $user_id , 'is_visitor' => array('<>',0)))->find();
            
            //$user为空而且不是新增
           if(!$act=='add'&&!$user){
               $this->error('找不到该体验者账户', U('Seller/admin/visitor'));
           }
           
            switch ($act){
                
                case 'forbid':
    
                    $user = M('users')->where(array('user_id ' => $user_id , 'is_visitor' => array('<>',0)))->find();
                    if (!$user){
                        $this->error('找不到该体验者账户', U('Seller/admin/visitor'));
                    }
                    
                    $rt = Db::name('users')->where(array('user_id ' => $user_id ))->update(['is_visitor'=>-1]);
                    break;
                case 'resume':
                    $rt = Db::name('users')->where(array('user_id ' => $user_id ))->update(['is_visitor'=>1]);
                    break;
                case 'add':

                    $channel = input('channel');
                    if(isEmptyObject($channel)){
                        $this->error('店铺登录校验方式错误');
                    }

                    $email='';$mobile='';

                    switch ($channel){
                        case 'sn':
                            $mobile = trim(input('mobile',null));

                            $isHas = Db::name('users')->where('mobile',$mobile)->find();
                            if($isHas){

                                $this->ajaxReturn(['status' => 0, 'msg' => '该手机号已经存在']);
                            }
                            break;
                        case 'email':
                            $email = trim(input('email',null));

                            $isHas = Db::name('users')->where('email',$email)->find();
                            if($isHas){

                                $this->ajaxReturn(['status' => 0, 'msg' => '该邮箱已经存在']);
                            }
                            break;
                        default:
                            $this->error('店铺登录校验方式错误');
                            break;
                    }
                    

                    
                    
                    $password = md5(C("AUTH_CODE").input('password',null));
                    $store_id = input('store_id',null);


                    if(!$store_id){
                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选']);
                    }

                    $store = Db::name('store')->where('store_id',$store_id)->find();
                    if(!$store){
                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺不存在']);
                    }
                    $post = ['email'=>$email,'mobile'=>$mobile,'password'=>$password,'reg_time'=>time(),'is_visitor'=>1,'store_id'=>$store_id];

                    switch ($channel){
                        case 'sn':
                            unset($post['email']);
                            break;
                        case 'email':
                            unset($post['mobile']);
                            break;
                        default:
                            $this->error('店铺登录校验方式错误');
                            break;
                    }




                    $rt = Db::name('users')->insertGetId($post);
                    $rt2 = Db::name('UserStore')->insertGetId(['user_id'=>$rt,'store_id'=>$store_id,'store_name'=>$store['store_name'],'store_img'=>$store['store_logo'],'store_time'=>time()]);
                    if($rt&&$rt2){
                        $this->ajaxReturn(['status' => 1, 'msg' => '操作成功','url'=>U('Seller/admin/visitor')]);
                    }else{
                        $this->ajaxReturn(['status' => 0, 'msg' => '操作失败']);
                        
                    }
                    
                    break;
                case 'edit':
    
                    $old_password = trim(I('old_password'));
                    $new_password = trim(I('new_password'));
                    if ($old_password == $new_password) {
                        $this->ajaxReturn(['status' => 0, 'msg' => '两次密码一致']);
                       
                    } else {
    
                        $user = Db::name('users')->where(array('user_id' => $user_id))->find();
                        if ($user) {
                            
//                            var_dump($user['password'],md5(C("AUTH_CODE").$old_password));die;
                            if($user['password']!=md5(C("AUTH_CODE").$old_password)){
                                $this->ajaxReturn(['status' => 0, 'msg' => '操作失败:用户密码错误']);
                                die;
                            }
                            
                            $r = M('users')->where(array('user_id' => $user_id))->save(array('password' =>md5(C("AUTH_CODE").$new_password)));
                            
                            if ($r !== false) {
                                $this->ajaxReturn(['status' => 1, 'msg' => '密码修改成功','url'=>U('/seller/Admin/visitor')]);
//                        M('seller')->where(['seller_id'=>$data['seller_id']])->save(array('enabled' => $data['enabled'],'group_id'=>$data['group_id']));
                            
                            } else {
                                $this->ajaxReturn(['status' => 0, 'msg' => '操作失败:修改密码失败']);
                            }
                        } else {
                            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败:用户不存在']);
                        }
                    }
                    
                    break;
                case 'del':
                    $rt = Db::name('users')->where(array('user_id ' => $user_id ))->delete();
                    if($rt!==fase){
                        $this->ajaxReturn(1);
                    }else{
                        $this->ajaxReturn(0);
                    }
                    break;
                default:
                    break;
            }
    
            if($rt!==fase){
                $this->error('操作成功', U('Seller/admin/visitor'));
            }else{
                $this->error('操作失败', U('Seller/admin/visitor'));
            }
            
        }
        
     
    }
   
    
    public function visitorHandle(){
        $data = I('post.');
    
        if ($data['act'] == 'del' && $data['seller_id'] > 0) {
            //删除店铺管理员
            $manage = M('seller')->where(array('seller_id' => $data['seller_id']))->find();
            if ($manage['seller_pid'] == SELLER_ID) {
            
                $childSeller = Db::name('seller')->where('seller_id',$data['seller_id'])->find();
            
                M('users')->where('user_id', $childSeller['user_id'])->delete();
                M('seller')->where('seller_id', $data['seller_id'])->delete();
                sellerLog('删除店铺管理员' . $manage['seller_name']);
            } else {
                exit(json_encode(0));//只能删除本店的管理员
            }
            exit(json_encode(1));
        }
        if ($data['seller_id'] > 0) {
            //修改密码
            $AdminLogic = new AdminLogic();
            $res = $AdminLogic->alterAdminPassword($data);
            $this->ajaxReturn($res);
        } else {
            //添加管理员
            $AdminLogic = new AdminLogic();
            $res = $AdminLogic->addStoreAdmin($data);
            $this->ajaxReturn($res);
        }
    }

    public function index()
    {
        $list = array();
        $keywords = I('keywords');
        if (empty($keywords)) {
            $res = D('seller')->where("seller_pid", SELLER_ID)->where('is_admin',0)->where('is_deleted',0)->select();
        } else {
            $seller_where = array(
                'seller_pid' => SELLER_ID,
                'seller_name' => ['like', '%' . $keywords . '%']
            );
            $res = Db::name('seller')->where($seller_where)->where('is_admin',0)->where('is_deleted',0)->order('seller_id')->select();
        }
        
        

        //用pid来搞
        $group = D('seller_group')->where(array('seller_pid' => SELLER_ID))->getField('group_id,group_name');


        if ($res && $group) {
            foreach ($res as $val) {
                $val['role'] = $group[$val['group_id']];
                $val['enabled'] = $val['enabled'] == 1 ? '启用' : '停用';
                $val['add_time'] = date('Y-m-d H:i:s', $val['add_time']);
                $list[] = $val;
            }
        }
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 修改管理员密码
     * @return \think\mixed
     */
    public function modify_pwd()
    {
        
        $seller_id = I('get.seller_id/d');
        if ($seller_id > 0) {
            $info = D('seller')->where(array('seller_id' => $seller_id))->find();
            if ($info) {
                $user = M('users')->where("user_id", $info['user_id'])->find();
            } else {
                $this->error('找不到该经销商', U('Seller/admin/index'));
            }
            $info['user_name'] = empty($user['mobile']) ? $user['email'] : $user['mobile'];
            $this->assign('info', $info);
        } 
        $data = I('post.');
        if(IS_POST){
            if ($data['seller_id'] > 0) {
                //修改密码
                $AdminLogic = new AdminLogic();
                $res = $AdminLogic->alterAdminPassword($data);
                $this->ajaxReturn($res);
            }
        }
        return $this->fetch();
    }
    
    public function admin_info()
    {
        $seller_id = I('get.seller_id/d');
        if ($seller_id > 0) {
            $info = M('seller')->where(array('seller_id' => $seller_id, 'seller_pid' => SELLER_ID))->find();
            if ($info) {
                $user = M('users')->where("user_id", $info['user_id'])->find();
            } else {
                $this->error('找不到该管理员', U('Seller/admin/index'));
            }
            $info['user_name'] = empty($user['mobile']) ? $user['email'] : $user['mobile'];
            $this->assign('info', $info);
        }
        $role = D('seller_group')->where(array('seller_pid' => SELLER_ID))->select();
        if(!$role){
            $this->error('需先添加账号组', U('Seller/Admin/role'));
            exit();
        }
        $this->assign('role', $role);
        return $this->fetch();
    }

    public function adminHandle()
    {
        $data = I('post.');

        if ($data['act'] == 'del' && $data['seller_id'] > 0) {
            //删除店铺管理员
            $manage = M('seller')->where(array('seller_id' => $data['seller_id']))->find();
            if ($manage['seller_pid'] == SELLER_ID) {
    
                $childSeller = Db::name('seller')->where('seller_id',$data['seller_id'])->find();
                
                M('users')->where('user_id', $childSeller['user_id'])->delete();
                M('seller')->where('seller_id', $data['seller_id'])->delete();
                sellerLog('删除店铺管理员' . $manage['seller_name']);
            } else {
                exit(json_encode(0));//只能删除本店的管理员
            }
            exit(json_encode(1));
        }
        if ($data['seller_id'] > 0) {
            //修改密码
            $AdminLogic = new AdminLogic();
            $res = $AdminLogic->alterAdminPassword($data);
            $this->ajaxReturn($res);
        } else {
            //添加管理员
            $AdminLogic = new AdminLogic();
            $res = $AdminLogic->addStoreAdmin($data);
            $this->ajaxReturn($res);
        }
    }

 
    /*
     * 管理员登陆
     */
    public function login()
    {
        if (session('seller_id') && session('seller_id') > 0) {
            
            $this->error("您已登录", U('Index/index'));
            die;
        }

        if (IS_POST) {
            $verify = new Verify();
            if (!$verify->check(I('post.vertify'), "seller_login")) {
                exit(json_encode(array('status' => 0, 'msg' => '验证码错误')));
            }
            $mobile = I('post.mobile');
            $password = I('post.password');
            if (!empty($mobile) && !empty($password)) {
                $seller = M('seller')->where(array('seller_account' => $mobile))->find();
                if ($seller) {
                    //经销商和店铺之间是一对多关系，不再要求旗下必须有的店铺
                	//$store = M('store')->where(array('store_id'=>$seller['store_id'],'store_state'=>1))->find();
                	//if(!$store) exit(json_encode(array('status' => 0, 'msg' => '店铺已关闭，请联系平台客服')));
                	//if($store['store_end_time']>0 && $store['store_end_time']<time()){
                		//M('store')->where(array('store_id'=>$seller['store_id']))->save(array('store_state'=>0));
                		//M('goods')->where(array('store_id'=>$seller['store_id']))->save(array('is_on_sale'=>0));
                		//exit(json_encode(array('status' => 0, 'msg' => '店铺已到期自动关闭，请联系平台客服')));
                	//}
                	
                    $user_where = array('user_id' => $seller['user_id'],'password' => encrypt($password));
                    $user = M('users')->where($user_where)->find();
                    
                    if ($user) {

                        
                        
                        
                        
                        
                        //如果是体验账号
                        if($seller['is_admin']==-1&&$seller['seller_pid']){
                            $seller = Db::name('seller')->where('seller_id',$seller['seller_pid'])->find();
                            if(!$seller){
                                exit(json_encode(array('status' => 0, 'msg' => '临时账户登录失败：所属经销商账号不存在')));
                            }
                            
                        }
    
                        if ($seller['enabled'] == 0) {
                            exit(json_encode(array('status' => 0, 'msg' => '该经销商账户被禁用')));
                        }
    
                        if ($seller['is_deleted'] == 1) {
                            exit(json_encode(array('status' => 0, 'msg' => '该经销商已被删除')));
                        }
                        
                        if ($seller['group_id'] > 0) {
                            $group = M('seller_group')->where(array('group_id' => $seller['group_id']))->find();
                            $seller['act_limits'] = $group['act_limits'];
                            $seller['smt_limits'] = $group['smt_limits'];
                        } else {
                            $seller['act_limits'] = 'all';
                            $seller['smt_limits'] = 'all';
                        }
                        session('seller', $seller);
                        session('seller_id', $seller['seller_id']);
                        //session('store_id', $seller['store_id']);//去掉这个store_id不知道对业务影响大不大20180519   影响真的好大。。。20180520
                        M('seller')->where(array('seller_id' => $seller['seller_id']))->save(array('last_login_time' => time()));
                        sellerLog('商家管理中心登录');//日志功能做的不错哦，可以学习
                        $url = session('from_url') ? session('from_url') : U('Index/index');
                        exit(json_encode(array('status' => 1, 'url' => $url)));
                    } else {
                        exit(json_encode(array('status' => 0, 'msg' => '账号密码不正确')));
                    }
                } else {
                    exit(json_encode(array('status' => 0, 'msg' => '账号不存在')));
                }
            } else {
                exit(json_encode(array('status' => 0, 'msg' => '请填写账号密码')));
            }
        }
        return $this->fetch();
    }

    /**
     * 退出登陆
     */
    public function logout()
    {
        session_unset();
        session_destroy();
        $this->success("退出成功", U('Seller/Admin/login'));
    }

    /**
     * 验证码获取
     */
    public function vertify()
    {
        $config = array(
            'fontSize' => 30,
            'length' => 4,
            'useCurve' => false,
            'useNoise' => false,
            'reset' => false
        );
        $Verify = new Verify($config);
        $Verify->entry("seller_login");
		exit();
    }

    public function role()
    {
        $list = D('seller_group')->where(array('seller_pid' => SELLER_ID))->order('group_id desc')->select();
        $this->assign('list', $list);
        return $this->fetch();
    }

    public function role_info()
    {
        $role_id = I('get.group_id/d');
        if ($role_id) {
            $detail = M('seller_group')->where(array('seller_pid' => SELLER_ID, 'group_id' => $role_id))->find();
            if ($detail) {
                $detail['act_limits'] = explode(',', $detail['act_limits']);
                $this->assign('detail', $detail);
            } else {
                $this->error('找不到该账号组', U('Seller/Admin/role'));
            }
        }

        $right = M('system_menu')->where(array('type' => 1))->order('id')->select();
        foreach ($right as $k => $val) {
            if (!empty($detail)) {
                $val['enable'] = in_array($val['id'], $detail['act_limits']);
            }
            $modules[$val['group']][] = $val;
        }
        //权限组
        $moduleLogic = new ModuleLogic;
        $group = $moduleLogic->getPrivilege(1);
        $this->assign('group', $group);
        $this->assign('modules', $modules);
        $this->assign('smt_list', M('store_msg_tpl')->select());
        return $this->fetch();
    }

    public function roleSave()
    {
        $data = I('post.');
        $data['act_limits'] = is_array($data['act_limits']) ? implode(',', $data['act_limits']) : '';
        $data['smt_limits'] = is_array($data['smt_limits']) ? implode(',', $data['smt_limits']) : '';
        if (empty($data['group_id'])) {
            $data['seller_pid'] = SELLER_ID;
            $r = M('seller_group')->add($data);
        } else {
            $r = M('seller_group')->where('group_id', $data['group_id'])->save($data);
        }
        

        //如果涉及更新的问题，需要这样来做！！！！
        if ($r !== false ) {
            sellerLog('管理角色');
            $this->success("操作成功!", U('Admin/role'));
        } else {
            $this->error("操作失败!");
        }
    }

    /**
     * 商家角色删除
     */
    public function roleDel()
    {
        $group_id = I('post.group_id/d');
        $seller = D('seller')->where(array('group_id' => $group_id))->find();
        if ($seller) {
            exit(json_encode("请先清空所属该角色的管理员"));
        } else {
            $d = M('seller_group')->where(array('group_id' => $group_id))->delete();
            if ($d) {
                exit(json_encode(1));
            } else {
                exit(json_encode("删除失败"));
            }
        }
    }

    public function log()
    {
        $Log = M('seller_log');
        $p = I('p', 1);
//        $seller_id = session('seller_id');
        $logs = Db::name('seller_log')->alias('sl')
            ->join('seller s', 's.seller_id = sl.log_seller_id')
            ->where('s.seller_id', SELLER_ID)->order('log_time DESC')
            ->page($p . ',20')
            ->select();
        $this->assign('list', $logs);
        $count = $Log->alias('sl')
            ->join('__SELLER__ s', 's.seller_id = sl.log_seller_id')
            ->where('s.seller_id', SELLER_ID)
            ->count();
        $Page = new Page($count, 20);
        $show = $Page->show();
        $this->assign('page', $show);
        return $this->fetch();
    }

    /**
     *  商家登录后 处理相关操作
     */
    public function login_task()
    {

        // 多少天后自动分销记录自动分成
		if(file_exists(APP_PATH.'common/logic/DistributLogic.php')){
			$distributLogic = new \app\common\logic\DistributLogic();
            $distributLogic->autoConfirm(STORE_ID); // 自动确认分成
        }

        // 商家结算 
        $storeLogic = new StoreLogic();
        $storeLogic->auto_transfer(STORE_ID); // 自动结算

    }

    /**
     * 清空系统缓存
     */
    public function cleanCache()
    {
        delFile('./public/upload/goods/thumb');// 删除缩略图
        clearCache();
        //$html_arr = glob("./Application/Runtime/Html/*.html");
        //foreach ($html_arr as $key => $val) {
            // 删除详情页
        //    if (strstr($val, 'Home_Goods_goodsInfo') || strstr($val, 'Home_Goods_ajaxComment') || strstr($val, 'Home_Goods_ajax_consult'))
        //        unlink($val);
        //}
        $this->success("清除成功!!!", U('Index/index'));
    }

    /**
     * 商品静态页面缓存清理
     */
    public function ClearGoodsThumb()
    {
        $goods_id = I('goods_id/d');
        delFile("./public/upload/goods/thumb/$goods_id"); // 删除缩略图
        clearCache();
        $json_arr = array('status' => 1, 'msg' => '清除成功,请清除对应的缩略图', 'result' => '');
        $json_str = json_encode($json_arr);
        exit($json_str);
    }

    /**
     * 清空静态商品页面缓存
     */
    public function ClearGoodsHtml()
    {
        clearCache();
        $json_arr = array('status' => 1, 'msg' => '清除成功', 'result' => '');       
        $json_str = json_encode($json_arr);
        exit($json_str);
    }

}