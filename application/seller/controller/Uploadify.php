<?php
namespace app\seller\controller;
use think\Db;

class Uploadify extends Base{
    
    protected $allowAction = ['index'];
	
	private $sub_name = array('date', 'Y/m-d');
	private $savePath = 'temp/';
	
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Shanghai");
		$this->savePath = I('savepath','temp').'/';
		error_reporting(E_ERROR | E_WARNING);
		header("Content-Type: text/html; charset=utf-8");
	}


    public function upload(){
        $func = I('func');
        $path = I('path','temp');
		$image_upload_limit_size = config('image_upload_limit_size');
        $fileType = I('fileType','Images');  //上传文件类型，视频，图片
        if($fileType == 'Flash'){
            $upload = U('Uploadify/videoUp',array('savepath'=>$path,'pictitle'=>'banner','dir'=>'video'));
        }else{
            $upload = U('Uploadify/imageUp',array('savepath'=>$path,'pictitle'=>'banner','dir'=>'images'));
        }
        $info = array(
        	'num'=> I('num/d'),
            'fileType'=> $fileType,
            'title' => '',       	
            'upload' =>$upload,
        	'fileList'=>U('Uploadify/fileList',array('path'=>$path)),
            'size' => $image_upload_limit_size/(1024 * 1024).'M',
            'type' =>'jpg,png,gif,jpeg',
            'input' => I('input'),
            'func' => empty($func) ? 'undefined' : $func,
        );
    
        $limitH = I('limitH',null);
        $limitW = I('limitW',null);
        $this->assign('limitH',$limitH);
        $this->assign('limitW',$limitW);
        
        
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function upload_file()
    {
        $func = I('func');
        $path = I('path', 'temp');
        $image_upload_limit_size = config('image_upload_limit_size');
        $fileType = I('fileType', 'office');  //上传文件类型，视频，图片
        $upload = U('Uploadify/index')."?action=uploadfile&savepath=".$savepath."&pictitle=banner&dir=images";
        $info = array(
            'num' => I('num/d'),
            'fileType' => $fileType,
            'title' => '',
            'upload' => $upload,
            'fileList' => U('Uploadify/fileList', array('path' => $path)),
            'size' => $image_upload_limit_size / (1024 * 1024) . 'M',
            'type' => 'jpg,png,gif,jpeg',
            'input' => I('input'),
            'func' => empty($func) ? 'undefined' : $func,
        );

        $limitH = I('limitH', null);
        $limitW = I('limitW', null);
        $this->assign('limitH', $limitH);
        $this->assign('limitW', $limitW);


        $this->assign('info', $info);
        return $this->fetch();
    }

    /**
     * 删除上传的图片
     * @throws \think\Exception
     */
    public function delupload(){
        $action = I('action','del');
        $filename= I('filename');
        $store_id = session('store_id');
        $filename= empty($filename) ? I('url') : $filename;
        $filename= str_replace('../','',$filename);
        $filename= trim($filename,'.');
        $filename= trim($filename,'/');
        if($action=='del' && !empty($filename) && file_exists($filename)){
            //modify by wangqh fix: 文件路径存在多个.时无法截取后缀问题 @{
            $pos = strripos($filename,'.');
            $filetype = substr($filename, $pos);
            // }
             
            $phpfile = strtolower(strstr($filename,'.php'));  //排除PHP文件
            $erasable_type = C('erasable_type');  //可删除文件
            if(!in_array($filetype,$erasable_type) || $phpfile){
                exit;
            }
            if(unlink($filename)){
                Db::name('store_extend')->where(['store_id'=>$store_id])->setDec('pic_num',1);
                echo 1;
            }else{
                echo 0;
            }
            exit;
        }
    }
    
    public function fileList(){
    	/* 判断类型 */
    	$type = I('type','Images');
    	switch ($type){
    		/* 列出图片 */
    		case 'Images' : $allowFiles = 'png|jpg|jpeg|gif|bmp';break;
    	
    		case 'Flash' : $allowFiles = 'flash|swf';break;
    	
    		/* 列出文件 */
    		default : $allowFiles = '.+';
    	}
    	
    	$path = UPLOAD_PATH.'store/'.session('store_id').'/'.I('path','temp');
    	//echo file_exists($path);echo $path;echo '--';echo $allowFiles;echo '--';echo $key;exit;
    	$listSize = 100000;
    	
    	$key = empty($_GET['key']) ? '' : $_GET['key'];
    	
    	/* 获取参数 */
    	$size = isset($_GET['size']) ? htmlspecialchars($_GET['size']) : $listSize;
    	$start = isset($_GET['start']) ? htmlspecialchars($_GET['start']) : 0;
    	$end = $start + $size;
    	
    	/* 获取文件列表 */
    	$files = $this->getfiles($path, $allowFiles, $key);
    	if (!count($files)) {
    		echo json_encode(array(
    				"state" => "没有相关文件",
    				"list" => array(),
    				"start" => $start,
    				"total" => count($files)
    		));
    		exit;
    	}
    	
    	/* 获取指定范围的列表 */
    	$len = count($files);
    	for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--){
    		$list[] = $files[$i];
    	}
    	
    	/* 返回数据 */
    	$result = json_encode(array(
    			"state" => "SUCCESS",
    			"list" => $list,
    			"start" => $start,
    			"total" => count($files)
    	));

    	echo $result;
    }

    /**
     * 遍历获取目录下的指定类型的文件
     * @param $path
     * @param array $files
     * @return array
     */
    private function getfiles($path, $allowFiles, $key, &$files = array()){
    	if (!is_dir($path)) return null;
    	if(substr($path, strlen($path) - 1) != '/') $path .= '/';
    	$handle = opendir($path);
    	while (false !== ($file = readdir($handle))) {
    		if ($file != '.' && $file != '..') {
    			$path2 = $path . $file;
    			if (is_dir($path2)) {
    				$this->getfiles($path2, $allowFiles, $key, $files);
    			} else {
    				if (preg_match("/\.(".$allowFiles.")$/i", $file) && preg_match("/.*". $key .".*/i", $file)) {
    					$files[] = array(
    							'url'=> '/'.$path2,
    							'name'=> $file,
    							'mtime'=> filemtime($path2)
    					);
    				}
    			}
    		}
    	}
    	return $files;
    }
    
    public function index(){
    	$CONFIG2 = json_decode(preg_replace("/\/\*[\s\S]+?\*\//", "", file_get_contents("./public/plugins/Ueditor/php/config.json")), true);
    	$action = I('action');

    	switch ($action) {
    		case 'config':
    			$result =  json_encode($CONFIG2);
    			break;
    			/* 上传图片 */
    		case 'uploadimage':
    			$fieldName = $CONFIG2['imageFieldName'];
    			$result = $this->imageUp();
    			break;
    			/* 上传涂鸦 */
    		case 'uploadscrawl':
    			$config = array(
    			"pathFormat" => $CONFIG2['scrawlPathFormat'],
    			"maxSize" => $CONFIG2['scrawlMaxSize'],
    			"allowFiles" => $CONFIG2['scrawlAllowFiles'],
    			"oriName" => "scrawl.png"
    					);
    					$fieldName = $CONFIG2['scrawlFieldName'];
    					$base64 = "base64";
    					$result = $this->upBase64($config,$fieldName);
    					break;
    					/* 上传视频 */
    		case 'uploadvideo':
    			$fieldName = $CONFIG2['videoFieldName'];
    			$result = $this->upFile($fieldName);
    			break;
    			/* 上传文件 */
    		case 'uploadfile':
    			$fieldName = $CONFIG2['fileFieldName'];
    			$result = $this->upFile($fieldName);
    			break;
    			/* 列出图片 */
    		case 'listimage':
    			$allowFiles = $CONFIG2['imageManagerAllowFiles'];
    			$listSize = $CONFIG2['imageManagerListSize'];
    			$path = $CONFIG2['imageManagerListPath'];
    			$get = $_GET;
    			$result =$this->fileList2($allowFiles,$listSize,$get);
    			break;
    			/* 列出文件 */
    		case 'listfile':
    			$allowFiles = $CONFIG2['fileManagerAllowFiles'];
    			$listSize = $CONFIG2['fileManagerListSize'];
    			$path = $CONFIG2['fileManagerListPath'];
    			$get = $_GET;
    			$result = $this->fileList2($allowFiles,$listSize,$get);
    			break;
    			/* 抓取远程文件 */
    		case 'catchimage':
    			$config = array(
    			"pathFormat" => $CONFIG2['catcherPathFormat'],
    			"maxSize" => $CONFIG2['catcherMaxSize'],
    			"allowFiles" => $CONFIG2['catcherAllowFiles'],
    			"oriName" => "remote.png"
    					);
    					$fieldName = $CONFIG2['catcherFieldName'];
    					/* 抓取远程图片 */
    					$list = array();
    					isset($_POST[$fieldName]) ? $source = $_POST[$fieldName] : $source = $_GET[$fieldName];
    
    					foreach($source as $imgUrl){
    						$info = json_decode($this->saveRemote($config,$imgUrl),true);
    						array_push($list, array(
    						"state" => $info["state"],
    						"url" => $info["url"],
    						"size" => $info["size"],
    						"title" => htmlspecialchars($info["title"]),
    						"original" => htmlspecialchars($info["original"]),
    						"source" => htmlspecialchars($imgUrl)
    						));
    					}
    
    					$result = json_encode(array(
    							'state' => count($list) ? 'SUCCESS':'ERROR',
    							'list' => $list
    					));
    					break;
    		default:
    			$result = json_encode(array(
    			'state' => '请求地址出错'
    					));
    					break;
    	}
    
    	/* 输出结果 */
    	if(isset($_GET["callback"])){
    		if(preg_match("/^[\w_]+$/", $_GET["callback"])){
    			echo htmlspecialchars($_GET["callback"]).'('.$result.')';
    		}else{
    			echo json_encode(array(
    					'state' => 'callback参数不合法'
    			));
    		}
    	}else{
    		echo $result;
    	}
    }
    
    //上传文件
    private function upFile($fieldName){
    	$file = request()->file('file');


    	if(empty($file)) $file = request()->file('upfile');
    	
    	$result = $this->validate(
    			['file' => $file],
    			['file'=>'fileSize:40000000|fileExt:pdf,doc,docx'],
    			['file.fileSize' => '上传文件过大','file.fileExt'=>'上传文件后缀名必须为pdf,doc,docx']
    	);
    	
    	if (true !== $result || !$file) {
    		$state = "ERROR" . $result;
    		return json_encode(['state' =>$state]);
    	}else{
    		// 移动到框架应用根目录/public/uploads/ 目录下
    		$savePath = 'store/'.session('store_id').'/'.$this->savePath.'/';
    		// 使用自定义的文件保存规则
    		$info = $file->rule(function ($file) {
    			return  md5(mt_rand());
    		})->move('public/upload/'.$this->savePath);
    	}
    	if($info){
            $return_data = array(
    				'title' => $info->getFilename(),
    				'original' => $info->getFilename(),
    				'type' => '.' . $info->getExtension(),
    				'size' => $info->getSize(),
    		);

            $return_url = '/public/upload/'.$this->savePath.$info->getSaveName();
            $ossConfig = tpCache('oss');
            $res =  $this->upOSS($savePath,$return_url,null);
            $return_data['state'] = $res['state'];
            $return_data['url']   = $res['return_url'];
            $this->ajaxReturn($return_data,'json');


    		//图片加水印
//    		if($this->savePath=='goods/'){
//        		$imgresource = ".".$data['url'];
//        		$image = \think\Image::open($imgresource);
//        		$water = tpCache('water');
//        		//$image->open($imgresource);
//        		$return_data['mark_type'] = $water['mark_type'];
//        		if($water['is_mark']==1 && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
//        			if($water['mark_type'] == 'text'){
//        				//$image->text($water['mark_txt'],'./hgzb.ttf',20,'#000000',9)->save($imgresource);
//        				$ttf = './hgzb.ttf';
//        				if (file_exists($ttf)) {
//        					$size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
//        					$color = $water['mark_txt_color'] ?: '#000000';
//        					if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
//        						$color = '#000000';
//        					}
//        					$transparency = intval((100 - $water['mark_degree']) * (127/100));
//        					$color .= dechex($transparency);
//        					$image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['sel'])->save($imgresource);
//        					$return_data['mark_txt'] = $water['mark_txt'];
//        				}
//        			}else{
//        				//$image->water(".".$water['mark_img'],9,$water['mark_degree'])->save($imgresource);
//        				$waterPath = "." . $water['mark_img'];
//        				$quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
//        				$waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
//        				$image->open($waterPath)->save($waterTempPath, null, $quality);
//        				$image->open($imgresource)->water($waterTempPath, $water['sel'], $water['mark_degree'])->save($imgresource);
//        				@unlink($waterTempPath);
//        			}
//        		}
//        	}
    	}else{
    		$data = array('state' => 'ERROR'.$file->getError());
    	}
    	return json_encode($data);
    }
    
    //列出图片
    private function fileList2($allowFiles,$listSize,$get){
    	$type = I('type','Images');
    	switch ($type){
    		/* 列出图片 */
    		case 'Images' : $allowFiles = 'png|jpg|jpeg|gif|bmp';break;
    		case 'Flash' : $allowFiles = 'flash|swf';break;
    		/* 列出文件 */
    		default : $allowFiles = '.+';
    	}
    	
    	$path = './'.UPLOAD_PATH.'store/'.session('store_id').'/'.$this->savePath;
    	$listSize = 100000;
    	$key = empty($_GET['key']) ? '' : $_GET['key'];
    	/* 获取参数 */
    	$size = isset($_GET['size']) ? htmlspecialchars($_GET['size']) : $listSize;
    	$start = isset($_GET['start']) ? htmlspecialchars($_GET['start']) : 0;
    	$end = $start + $size;
    	/* 获取文件列表 */
    	$files = $this->getfiles($path, $allowFiles, $key);
    	if (!count($files)) {
    		echo json_encode(array(
    				"state" => "没有相关文件",
    				"list" => array(),
    				"start" => $start,
    				"total" => count($files)
    		));
    		exit;
    	}
    	
    	/* 获取指定范围的列表 */
    	$len = count($files);
    	for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--){
    		$list[] = $files[$i];
    	}
    	
    	/* 返回数据 */
    	$result = json_encode(array(
    			"state" => "SUCCESS",
    			"list" => $list,
    			"start" => $start,
    			"total" => count($files)
    	));
    
    	return $result;
    }
    
    //抓取远程图片
    private function saveRemote($config,$fieldName){
    	$imgUrl = htmlspecialchars($fieldName);
    	$imgUrl = str_replace("&amp;","&",$imgUrl);
    
    	//http开头验证
    	if(strpos($imgUrl,"http") !== 0){
    		$data=array(
    				'state' => '链接不是http链接',
    		);
    		return json_encode($data);
    	}
    	//获取请求头并检测死链
    	$heads = get_headers($imgUrl);
    	if(!(stristr($heads[0],"200") && stristr($heads[0],"OK"))){
    		$data=array(
    				'state' => '链接不可用',
    		);
    		return json_encode($data);
    	}
    	//格式验证(扩展名验证和Content-Type验证)
    	$fileType = strtolower(strrchr($imgUrl,'.'));
    	if(!in_array($fileType,$config['allowFiles']) || stristr($heads['Content-Type'],"image")){
    		$data=array(
    				'state' => '链接contentType不正确',
    		);
    		return json_encode($data);
    	}
    
    	//打开输出缓冲区并获取远程图片
    	ob_start();
    	$context = stream_context_create(
    			array('http' => array(
    					'follow_location' => false // don't follow redirects
    			))
    	);
    	readfile($imgUrl,false,$context);
    	$img = ob_get_contents();
    	ob_end_clean();
    	preg_match("/[\/]([^\/]*)[\.]?[^\.\/]*$/",$imgUrl,$m);
    
    	$dirname = './public/upload/remote/';
    	$file['oriName'] = $m ? $m[1] : "";
    	$file['filesize'] = strlen($img);
    	$file['ext'] = strtolower(strrchr($config['oriName'],'.'));
    	$file['name'] = uniqid().$file['ext'];
    	$file['fullName'] = $dirname.$file['name'];
    	$fullName = $file['fullName'];
    
    	//检查文件大小是否超出限制
    	if($file['filesize'] >= ($config["maxSize"])){
    		$data=array(
    				'state' => '文件大小超出网站限制',
    		);
    		return json_encode($data);
    	}
    
    	//创建目录失败
    	if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
    		$data=array(
    				'state' => '目录创建失败',
    		);
    		return json_encode($data);
    	}else if(!is_writeable($dirname)){
    		$data=array(
    				'state' => '目录没有写权限',
    		);
    		return json_encode($data);
    	}
    
    	//移动文件
    	if(!(file_put_contents($fullName, $img) && file_exists($fullName))){ //移动失败
    		$data=array(
    				'state' => '写入文件内容错误',
    		);
    		return json_encode($data);
    	}else{ //移动成功
    		$data=array(
    				'state' => 'SUCCESS',
    				'url' => substr($file['fullName'],1),
    				'title' => $file['name'],
    				'original' => $file['oriName'],
    				'type' => $file['ext'],
    				'size' => $file['filesize'],
    		);
    	}
    
    	return json_encode($data);
    }
    
    /*
     * 处理base64编码的图片上传
    * 例如：涂鸦图片上传
    */
    private function upBase64($config,$fieldName){
    	$base64Data = $_POST[$fieldName];
    	$img = base64_decode($base64Data);
    
    	$dirname = './public/upload/scrawl/';
    	$file['filesize'] = strlen($img);
    	$file['oriName'] = $config['oriName'];
    	$file['ext'] = strtolower(strrchr($config['oriName'],'.'));
    	$file['name'] = uniqid().$file['ext'];
    	$file['fullName'] = $dirname.$file['name'];
    	$fullName = $file['fullName'];
    
    	//检查文件大小是否超出限制
    	if($file['filesize'] >= ($config["maxSize"])){
    		$data=array(
    				'state' => '文件大小超出网站限制',
    		);
    		return json_encode($data);
    	}
    
    	//创建目录失败
    	if(!file_exists($dirname) && !mkdir($dirname,0777,true)){
    		$data=array(
    				'state' => '目录创建失败',
    		);
    		return json_encode($data);
    	}else if(!is_writeable($dirname)){
    		$data=array(
    				'state' => '目录没有写权限',
    		);
    		return json_encode($data);
    	}
    
    	//移动文件
    	if(!(file_put_contents($fullName, $img) && file_exists($fullName))){ //移动失败
    		$data=array(
    				'state' => '写入文件内容错误',
    		);
    	}else{ //移动成功
    		$data=array(
    				'state' => 'SUCCESS',
    				'url' => substr($file['fullName'],1),
    				'title' => $file['name'],
    				'original' => $file['oriName'],
    				'type' => $file['ext'],
    				'size' => $file['filesize'],
    		);
    	}
    
    	return json_encode($data);
    }

    /**
     * @function imageUp
     */
    public function imageUp()
    {

        //上传图片框中的描述表单名称，
        $pictitle = I('pictitle');
        $dir = I('dir');
//        $store_id = session('store_id');
        $pic_num = Db::name('store_extend')->where(['store_id'=>$store_id])->getField('pic_num');  //查找店铺已经传了多少图片
        $sg_album_limit = Db::name('store_grade')->where(['sg_id'=>$this->storeInfo['grade_id']])->getField('sg_album_limit');  //查找店铺已经传了多少图片
        if ($pic_num >= $sg_album_limit && $sg_album_limit>0)
            $this->ajaxReturn(['state'=>'当前允许上传图片数量已到达店铺等级限制的【'.$sg_album_limit.'张】'],'json');
        $title = htmlspecialchars($pictitle , ENT_QUOTES);
        $path = htmlspecialchars($dir, ENT_QUOTES);
        // 获取表单上传文件
        $file = request()->file('file');
        if(empty($file)) $file = request()->file('upfile');

        $result = $this->validate(
            ['file' => $file],
            ['file'=>'image|fileSize:40000000|fileExt:jpg,jpeg,gif,png'],
            ['file.image' => '上传文件必须为图片','file.fileSize' => '上传文件过大','file.fileExt'=>'上传文件后缀名必须为jpg,jpeg,gif,png']
        );
        if (true !== $result || !$file) {
            $state = "ERROR" . $result;
        } else {
            $savePath = 'seller/'.SELLER_ID.'/'.$this->savePath;
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $file->rule(function ($file) {
                return  md5(mt_rand()); // 使用自定义的文件保存规则
            })->move('public/upload/'.$savePath);
            if ($info) {
                $state = "SUCCESS";
                Db::name('store_extend')->where(['store_id'=>$store_id])->setInc('pic_num',1);
            } else {
                $state = "ERROR" . $file->getError();
            }
            $return_url = '/public/upload/'.$savePath.$info->getSaveName();
            unset($info);   //关闭文件句柄，不然无法删除
            $imgresource = ".".$return_url;
            $return_data['url'] = $return_url;
        }

        $return_data['title'] = $title;
        $return_data['original'] = '';
        $return_data['state'] = $state;
        $return_data['path'] = $path;
        $water = tpCache('water');
        $return_data['mark_type'] = $water['mark_type'];
        if($state == 'SUCCESS' && $water['is_mark']==1 && $this->savePath=='goods/'){
            vendor('topthink.think-image.src.Image');
            if(strstr(strtolower($imgresource),'.gif'))
            {
                vendor('topthink.think-image.src.image.gif.Encoder');
                vendor('topthink.think-image.src.image.gif.Decoder');
                vendor('topthink.think-image.src.image.gif.Gif');
            }
            $image = \think\Image::open($imgresource);
            $pos = strripos($return_url,'.');
            $filetype = substr($return_url, $pos);
            //gif格式的不加水印
            if($filetype != '.gif' && $image->width()>$water['mark_width'] && $image->height()>$water['mark_height']){
                if($water['mark_type'] == 'text'){   //文字水印
                    //$image->text($water['mark_txt'],'./hgzb.ttf',20,'#000000',9)->save($imgresource);
                    $ttf = './hgzb.ttf';
                    if (file_exists($ttf)) {
                        $size = $water['mark_txt_size'] ? $water['mark_txt_size'] : 30;
                        $color = $water['mark_txt_color'] ?: '#000000';
                        if (!preg_match('/^#[0-9a-fA-F]{6}$/', $color)) {
                            $color = '#000000';
                        }
                        $transparency = intval((100 - $water['mark_degree']) * (127/100));
                        $color .= dechex($transparency);
                        $image->open($imgresource)->text($water['mark_txt'], $ttf, $size, $color, $water['sel'])->save($imgresource);
                        $return_data['mark_txt'] = $water['mark_txt'];
                    }
                }else{    //图片水印
                    //$image->water(".".$water['mark_img'],9,$water['mark_degree'])->save($imgresource);
                    $waterPath = "." . $water['mark_img'];
                    $quality = $water['mark_quality'] ? $water['mark_quality'] : 80;
                    $waterTempPath = dirname($waterPath).'/temp_'.basename($waterPath);
                    $image->open($waterPath)->save($waterTempPath, null, $quality);
                    $image->open($imgresource)->water($waterTempPath, $water['sel'], $water['mark_degree'])->save($imgresource);
                    @unlink($waterTempPath);
                }
            }
        }
        $ossConfig = tpCache('oss');
        $ossSupportPath = ['goods', 'water', 'storelogo', 'ad', 'sellerAddressPic', 'store_protocol_photo'];
        if (in_array(I('savepath'), $ossSupportPath) && $ossConfig['oss_switch']) {
            $res =  $this->upOSS($savePath,$return_url,$store_id);
            $return_data['state'] = $res['state'];
            $return_data['url']   = $res['return_url'];
        }
        $this->ajaxReturn($return_data,'json');
    }

    /**
     * 上传到OSS
     * @param $savePath
     * @param $file_url
     * @param $store_id
     * @return array
     */
    function upOSS($savePath,$file_url,$store_id){

            //商品图片可选择存放在oss
            $object = 'public/upload/'.$savePath.md5(time()).randString().randString().'.'.pathinfo($file_url, PATHINFO_EXTENSION);
            $ossClient = new \app\common\logic\OssLogic;
            $url = ".".$file_url;
            $return_url = $ossClient->uploadFile($url, $object);
            if (!$return_url) {
                $state = "ERROR" . $ossClient->getError();
                $return_url  = '';
            } else {
                $state = "SUCCESS";
                Db::name('store_extend')->where(['store_id'=>$store_id])->setDec('pic_num',1);
            }
            $delurl = trim($file_url,'/');
            @unlink($delurl);
        return  ['state'=>$state,'return_url'=>$return_url];
    }

    /**
     * 上传视频
     */
    public function videoUp()
    {
        $pictitle = I('pictitle');
        $dir = I('dir');
        $store_id = session('store_id');
        $title = htmlspecialchars($pictitle , ENT_QUOTES);
        $path = htmlspecialchars($dir, ENT_QUOTES);
        // 获取表单上传文件
        $file = request()->file('file');
        if (empty($file)) {
            $file = request()->file('upfile');
        }
        $result = $this->validate(
            ['file' => $file],
            ['file'=>'fileSize:40000000|fileExt:mp4,3gp,flv,avi,wmv'],
            ['file.fileSize' => '上传文件过大','file.fileExt'=>'上传文件后缀名必须为mp4,3gp,flv,avi,wmv']
        );
        if (true !== $result || !$file) {
            $state = "ERROR" . $result;
        } else {
            // 移动到框架应用根目录/public/uploads/ 目录下
            $new_path = 'store/'.$store_id.'/'.$this->savePath;
            // 使用自定义的文件保存规则
            $info = $file->rule(function ($file) {
                return  md5(mt_rand());
            })->move(UPLOAD_PATH.$new_path);
            if ($info) {
                $state = "SUCCESS";
            } else {
                $state = "ERROR" . $file->getError();
            }
            $return_data['url'] = '/'.UPLOAD_PATH.$new_path.$info->getSaveName();
        }

        $return_data['title'] = $title;
        $return_data['original'] = '';
        $return_data['state'] = $state;
        $return_data['path'] = $path;
        $this->ajaxReturn($return_data);
    }
}