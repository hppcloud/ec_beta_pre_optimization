<?php

namespace app\seller\controller;

use app\common\model\SellerAddress;
use think\Db;
use think\Loader;
use think\Page;

class Address extends Base
{

    /**
     * 店铺地址
     */
    public function index()
    {
        $SellerAddress = new SellerAddress();
        $count = $SellerAddress->where('seller_id', SELLER_ID)->count();
        $page = new Page($count, 10);
        $list = $SellerAddress->where('seller_id', SELLER_ID)->order('seller_address_id desc')->limit($page->firstRow . ',' . $page->listRows)->select();
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    
    public function info()
    {
        $id = input('id');
        if($id){
            $SellerAddress = SellerAddress::get(['seller_address_id'=>$id,'seller_id'=>SELLER_ID]);
            if(empty($SellerAddress)){
                $this->error('非法操作');
            }
            $city = Db::name('region')->where('parent_id', $SellerAddress['province_id'])->select();
            $area = Db::name('region')->where('parent_id', $SellerAddress['city_id'])->select();
            $this->assign('city', $city);
            $this->assign('area', $area);
            $this->assign('SellerAddress', $SellerAddress);
        }
        $province = Db::name('region')->where('parent_id', 0)->select();
        $this->assign('province', $province);
        return $this->fetch();
    }

    public function save()
    {
        $data = input('post.');
        $addressValidate = Loader::validate('SellerAddress');
        if (!$addressValidate->batch()->check($data)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => $addressValidate->getError()]);
        }
        
        
        if($data['seller_address_id']){
            $SellerAddress = SellerAddress::get(['seller_address_id' => $data['seller_address_id'], 'seller_id' => SELLER_ID]);
            if(empty($SellerAddress)){
                $this->ajaxReturn(array('status' => 0, 'msg' => '非法操作','result'=>''));
            }
        }else{
            $SellerAddress = new SellerAddress();
        }
        $SellerAddress->data($data, true);
        $SellerAddress['seller_id'] = SELLER_ID;
//
        $is_default_count = Db::name('seller_address')->where(['seller_id' => SELLER_ID,'is_default'=>1,'type'=>$data['type'],])->count();
        if($is_default_count == 0){
            $SellerAddress->is_default = 1;
        }
        $row = $SellerAddress->allowField(true)->save();
        if ($SellerAddress['is_default'] == 1) {
            Db::name('seller_address')->where(['seller_id' => SELLER_ID, 'type' => $SellerAddress['type'], 'is_default' => 1, 'seller_address_id' => ['neq', $SellerAddress->seller_address_id]])
                ->update(['is_default' => 0]);
        }
        if($row !== false){
            $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => '']);
        }else{
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => '']);
        }
    }

    public function delete()
    {
        $id = input('id');
        if(empty($id)){
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => '']);
        }
        $delete = Db::name('seller_address')->where(['seller_address_id'=>$id,'store_id'=>STORE_ID])->delete();
        if($delete !== false){
            $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => '']);
        }else{
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => '']);
        }

    }

}