<?php
/**
 * 预售控制器
 */

namespace app\seller\controller;

use app\common\model\Order;
use think\Loader;
use think\Db;
use app\common\model\PreSell as PreSellModel;
use think\Page;

class PreSell extends Base
{
	public function index()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 预售详情
	 * @return mixed
	 */
	public function info()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 保存
	 */
	public function save()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	/**
	 * 删除
	 */
	public function delete(){
		header("Content-type: text/html; charset=utf-8");
	}

	public function succeed()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	public function fail()
	{
		header("Content-type: text/html; charset=utf-8");
	}

	public function finish()
	{
		header("Content-type: text/html; charset=utf-8");
	}
}
