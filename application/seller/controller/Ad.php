<?php

namespace app\seller\controller;


use app\common\model\Seller;
use think\Db;
use think\Loader;
use think\Page;

class Ad extends Base
{
    
    public function position(){
        $count = Db::name('ad')->where('open', 1)->count();
        $page = new Page($count, 10);
        $list = Db::name('adPosition')->where('open', 1)->order('orderby desc')->limit($page->firstRow . ',' . $page->listRows)->select();

        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }

    /**
     * 获取广告列表
     */
    public function index()
    {
        
        $count = Db::name('ad')->where('seller_id', SELLER_ID)->count();
        $page = new Page($count, 10);
        
        
        $map = ['seller_id'=>SELLER_ID];
        
        $store_id = I('store_id',null);
        if($store_id){
            $map['store_id_list'] = array('like','%'.$store_id.'%');
        }
    
        $list = Db::name('ad')->where($map)->order('orderby desc')->limit($page->firstRow . ',' . $page->listRows)->select();
        
        
        $stores = Db::name('store')->where(['seller_id'=>SELLER_ID])->field('store_id,store_name')->select();
    
        
        foreach ($list as &$ad){
            $position = Db::name('adPosition')->where('position_id',$ad['pid'])->field('position_name')->find();
            $ad['position_name'] = $position['position_name'];
            $ad['store_name_arr_str'] = '';
             foreach ($stores as $store){
                 if(in_array($store['store_id'],explode(',',$ad['store_id_list']))){
                     if($ad['store_name_arr_str']===''){
                         $ad['store_name_arr_str'].= $store['store_name'];
                     }else{
                         $ad['store_name_arr_str'].=','.$store['store_name'];
                     }
                     
                 }
             }
        }
        

        
        
        $this->assign('stores',$stores);
        $this->assign('page', $page);
        $this->assign('list', $list);
        return $this->fetch();
    }
    
    public function getPositionInfo(){
        $pid = I('pid');
        if(!$pid){
            $this->ajaxReturn(array('status' => 0, 'msg' => 'pid不存在','result'=>''));
        }
        $position = Db::name('adPosition')->where('position_id',$pid)->find();
        if(!$position){
            $this->ajaxReturn(array('status' => 0, 'msg' => '非法操作','result'=>''));
        }else{
            $this->ajaxReturn(array('status' => 1, 'msg' => '获取成功','result'=>$position));
        }
        
    }

    public function selectpid(){
        
        if($this->request->isGet()){
            $positions = Db::name('adPosition')->where('is_open',1)->select();
            $this->assign('positions',$positions);
            return $this->fetch();
        }else{
            $pid = I('pid');
            $this->redirect('info',['pid'=>$pid]);
        }
        
       
        
    }
    public function info()
    {
        $id = input('id');
    
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        
        
        
        if($id){
            
            $ad = Db::name('ad')->where('ad_id',$id)->find();
            if(empty($ad)){
                $this->error('非法操作');
            }
            $ad['start_time'] = format_datetime_timestamp($ad['start_time']);
            $ad['end_time'] = format_datetime_timestamp($ad['end_time']);
            $this->assign('ad',$ad);
            $this->assign('act','edit');
            
            
            $pid = $ad['pid'];
    
    
            foreach ($stores as &$store){
                $store['is_select'] = false;
                if(in_array($store['store_id'],explode(',',$ad['store_id_list']))){
                    $store['is_select'] = true;
                    
                }
            }
    
            
            
            
            
            
        }else{
            
            $pid = I('pid');
            $this->assign('starttime',format_datetime_timestamp(time()));
            $this->assign('endtime',format_datetime_timestamp((time()+3600*24*60)));
            $this->assign('act','add');
        }
        
        if(!$pid){
            $this->error('pid参数缺失','index');
        }
        
        
       
    
        $this->assign('stores',$stores);
        $position = Db::name('adPosition')->where('is_open', 1)->where('position_id',$pid)->find();
        $this->assign('position', $position);
        return $this->fetch();
    }
    
    public function adHandle(){
        $data = I('post.');

        $data['start_time'] = strtotime($data['begin']);
        $data['end_time'] = strtotime($data['end']);

        if($data['end_time']<=$data['start_time']){
            $this->error('截止时间必须大于开始时间');
        }
        
        $media_type = $data['media_type'];
        if($media_type == 3){//商品
            $data['ad_link'] = $data['goods_id'];
        }else if($media_type == 4){//分类
            $data['ad_link'] = $data['cat_id1'].'_'.$data['cat_id2'].'_'.$data['cat_id3'];
        }
    
        if($data['act'] == 'add'){
            $data['seller_id'] = SELLER_ID;
        
            $stores = $data['store'];
            if(count($stores)<1){
                $this->error("请至少选择一个店铺");
            }
            //多个店铺修改为发布到一个记录，然后关联多家店铺
            $data['store_id_list'] = join(',',$stores);
        
            $r = D('ad')->add($data);
        
        
        }
        
//        if($data['act'] == 'add'){
//            $data['seller_id'] = SELLER_ID;
//
//            $stores = $data['store'];
//            if(count($stores)<1){
//                $this->error("请至少选择一个店铺");
//            }
//            $num = 0;$len= count($stores);
//            foreach ($stores as $store_id){
//                $data['store_id'] = $store_id;
//                $r = D('ad')->add($data);
//                if($r){
//                    $num++;
//                }
//            }
//
//        }
        if($data['act'] == 'edit'){
            
            $stores = $data['store'];
            if(count($stores)<1){
                $this->error("请至少选择一个店铺");
            }
            //多个店铺修改为发布到一个记录，然后关联多家店铺
            $data['store_id_list'] = join(',',$stores);
            
            $r = D('ad')->where('ad_id', $data['ad_id'])->save($data);
        }
        
        if($data['act'] == 'del'){
            $r = D('ad')->where('ad_id', $data['del_id'])->delete();
            if($r) $this->ajaxReturn(['status'=>1,'msg'=>'删除成功']);
        }
        $referurl = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : U('Admin/Ad/adList');
        // 不管是添加还是修改广告 都清除一下缓存
        //delFile(RUNTIME_PATH.'html'); // 先清除缓存, 否则不好预览
        clearCache();
        
        if($r!==false){

            if($data['act'] == 'add'){
                $this->success("成功发布",U('Seller/Ad/index'));
            }
//    	    $redirect_url = session("ad_request_url");
//    	    $redirect_url && $this->success("操作成功",U('Admin/Ad/editAd' , array('request_url'=>$redirect_url)));
            $this->success("操作成功",U('Seller/Ad/index'));
        }else{
            $this->error("操作失败");
        }
    }

    public function save()
    {
        $data = input('post.');
        $addressValidate = Loader::validate('SellerAddress');
        if (!$addressValidate->batch()->check($data)) {
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => $addressValidate->getError()]);
        }
        if($data['seller_address_id']){
            $SellerAddress = SellerAddress::get(['seller_address_id' => $data['seller_address_id'], 'seller_id' => SELLER_ID]);
            if(empty($SellerAddress)){
                $this->ajaxReturn(array('status' => 0, 'msg' => '非法操作','result'=>''));
            }
        }else{
            $SellerAddress = new SellerAddress();
        }
        $SellerAddress->data($data, true);
        $SellerAddress['seller_id'] = SELLER_ID;
//
        $is_default_count = Db::name('seller_address')->where(['seller_id' => SELLER_ID,'is_default'=>1,'type'=>$data['type'],])->count();
        if($is_default_count == 0){
            $SellerAddress->is_default = 1;
        }
        $row = $SellerAddress->allowField(true)->save();
        if ($SellerAddress['is_default'] == 1) {
            Db::name('seller_address')->where(['seller_id' => SELLER_ID, 'type' => $SellerAddress['type'], 'is_default' => 1, 'seller_address_id' => ['neq', $SellerAddress->seller_address_id]])
                ->update(['is_default' => 0]);
        }
        if($row !== false){
            $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => '']);
        }else{
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => '']);
        }
    }

    public function delete()
    {
        $id = input('id');
        if(empty($id)){
            $this->ajaxReturn(['status' => 0, 'msg' => '参数错误', 'result' => '']);
        }
        $delete = Db::name('ad')->where(['ad_id'=>$id,'seller_id'=>SELLER_ID])->delete();
        if($delete !== false){
            $this->ajaxReturn(['status' => 1, 'msg' => '操作成功', 'result' => '']);
        }else{
            $this->ajaxReturn(['status' => 0, 'msg' => '操作失败', 'result' => '']);
        }

    }

}