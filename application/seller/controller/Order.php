<?php

namespace app\seller\controller;

use app\common\logic\Pay;
use app\common\model\DeliveryDoc;
use app\common\model\Order as OrderModel;
use app\common\model\OrderAction;
use app\common\model\OrderGoods;
use app\common\model\SellerAddress;
use app\common\util\TpshopException;
use app\seller\logic\GoodsLogic;
use app\seller\logic\OrderLogic;
use app\seller\logic\SellerOrder;
use think\AjaxPage;
use think\Db;
use think\Exception;
use think\Page;

use app\api\controller\v1\FenQi;
use app\api\controller\v1\Pay as refundPay;

use aihuishou\AHSClient;

class Order extends Base
{
    public $order_status;
    public $shipping_status;
    public $pay_status;

    /*
     * 初始化操作
     */
    public function _initialize()
    {
        parent::_initialize();
        C('TOKEN_ON', false); // 关闭表单令牌验证
        // 订单 支付 发货状态
        $this->order_status = C('ORDER_STATUS');
        $this->pay_status = C('PAY_STATUS');
        $this->shipping_status = C('SHIPPING_STATUS');
        $this->assign('order_status', $this->order_status);
        $this->assign('pay_status', $this->pay_status);
        $this->assign('shipping_status', $this->shipping_status);
    }
    
    public function handelReserve(){
        $order_id_str = I('order_id_arr',null);
        $order_id_arr = explode(',',$order_id_str);
        
        
    
       
        if(!I('reserve_start_time',null)){
            $this->error('起始时间必选');
        }
        if(!I('reserve_end_time',null)){
            $this->error('终止时间必选');
        }
        
        $reserve_start_time = strtotime(I('reserve_start_time'));
        $reserve_end_time = strtotime(I('reserve_end_time'));
        
        
        if(count($order_id_arr)<1){
            $this->error('请至少勾选一条记录');
        }
        
       
        
        $len = count($order_id_arr);
        $complete = 0;//有一个是空的
        foreach ($order_id_arr as $orderId){
            if(!$orderId){
                continue;
            }
            $isHas = Db::name('OrderReserve')->where('order_id',$orderId)->find();
            if($isHas){
                $rt = Db::name('OrderReserve')->where('order_id',$orderId)->update(['start_time'=>$reserve_start_time,'end_time'=>$reserve_end_time,'reserve_status'=>1]);
            }else{
                $rt = Db::name('OrderReserve')->insertGetId(['start_time'=>$reserve_start_time,'end_time'=>$reserve_end_time,'reserve_status'=>1,'order_id'=>$orderId]);
            }
            if($rt!==false){
                $order = Db::name('order')->where('order_id',$orderId)->field('order_id,user_id,store_id,order_sn,create_time,mobile')->find();
                if(!$order){
                    continue;
                }
                $phone = $order['mobile'];
                $user = Db::name('users')->where('user_id',$order['user_id'])->find();
                $email = $user['email'];
                
                $storeInfo = Db::name('store')->where('store_id',$order['store_id'])->find();
                $seller = Db::name('seller')->where('seller_id',$storeInfo['seller_id'])->find();
                $goodsInfo = Db::name('OrderGoods')->whereLike('order_id',$orderId)->find();//这里规定预订订单只能有一个商品的
                
                
                
                $start_time = I('reserve_start_time');
                $end_time =  I('reserve_end_time');
                
                
                $link = getReserveOrderUrl($orderId);//根据测试环境/正式环境拿不同配置


                trace('phone:'.$phone.'emial22222222222222222'.$email);
                
                $order_time = date('Y-m-d H:i:s',$order['create_time']);
//                $content = "尊敬的{$user['nickname']}客户：<br/>";
//                $content.="我们欣喜地通知您，您在{$order_time}预约的产品<br/>";
//                $content.="<h5>产品名称: {$goodsInfo['goods_name']} 型号:{$goodsInfo['spec_key_name']} 价格:{$goodsInfo['final_price']}元  订单号:{$order['order_sn']}</h5>";
//                $content.="<br/>目前已到货。开始下单时间为{$start_time},需要您在{$end_time}前完成预约订单的支付操作，若未在规定时间内完成支付，系统将视为自动取消预约订单。<br/>";
//                $content.="点击此链接获取订单界面：{$link}<br/>";
//                $content.="烦请您接到此到货通知后，在规定时间内完成支付以保障您可以及时购入产品<br/>";
//                $content.='服务热线:'.$seller['seller_contact'];

//                http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/banner.png
                
                $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>　
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />　
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        html,
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            font-family:"Microsoft YaHei", PingFangSC-Light，PingFang SC, sans-serif,Arial,Helvetica,sans-serif,"宋体";
        }
        img {
            display: block;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            border: none;
        }
        table {
            margin:0 auto;
            width:640px;
            padding:24px;
            background: white;
        }
        .btn {
            width: 200px;
            left: 220px;
            margin: 0 auto;
            z-index: 999;
        }
        .email-t {
            font-size: 25px;
            font-weight: bold;
            line-height: 2;
            color: #423e3e;
            padding: 24px 48px 12px;
        }
        .email-p {
            font-size: 20px;
            line-height: 2;
            padding: 24px 48px;
            color: #1e1c19;
        }
        .lg {
            background: #f2f2f2;
        }
    </style> 　
</head>
<body>
    　<table border="0" cellpadding="0" cellspacing="0" width="640px">
        　　<tr>
            　　<td style="position:relative;">
            
               <img style="width:100%;margin-bottom: 24px;" src="http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/WechatIMG2684.jpeg" />
            </td>
            <tr class="lg"> 
                <td class="email-t">
                    尊敬的客户'.$user['nickname'].'：
                </td>
            </tr>
            <tr class="lg">
                <td class="email-p">
                    我们欣喜地通知您，您在'.$order_time.'预约的产品
                    <br/>
                    产品名称: '.$goodsInfo['goods_name'].' 型号:'.$goodsInfo['spec_key_name'].' 价格:'.$goodsInfo['final_price'].'元  订单号:'.$order['order_sn'].'
                    <br/>
                    <br/>
                    目前已到货。开始下单时间为'.$start_time.'需要您在'.$end_time.'前完成预约订单的支付操作，若未在规定时间内完成支付，系统将视为自动取消预约订单。
                    <br/>
                    <br/>
                    烦请您接到此到货通知后，在规定时间内完成支付以保障您可以及时购入产品
                    <br/>
                    服务热线:'.$seller['seller_contact'].'
                </td>
            </tr>
            <tr class="lg">
                <td style="text-align: center;padding: 12px 0 36px;">
                    <a href="'.$link.'" target="_blank">
                        <img class="btn" src="http://shop-pic-video.oss-cn-beijing.aliyuncs.com/pic/btn.png" />
                    </a>
                </td>
            </tr>

        </tr>
        　</table>
</body>
</html>';

                try{

                    if($email&&$email!=''){
                        $emialRt = sendEmail($email, $goodsInfo['goods_name'].'新品到货通知', $content);
                        //'尊敬的' . $email . '用户，您的预订订单'.$orderId.'状态发生变动。开放购买时间为'.I('reserve_start_time').'至'.I('reserve_end_time').'.请收到邮件后尽快登录系统完成购买');


                    }

                    if($phone&&$phone!=''){
                        $params = array('name'=>$phone , 'time' => $end_time);
                        trace('给用户发短信'.$phone.'信息是'.json_encode($params));
                        sendSms("9",$phone,$params);
                    }



                }catch (Exception $e){
                    continue;
                }




               
                
                $complete++;
            }
        }
        
        $this->success('操作成功'.$complete.'个，失败'.($len-$complete).'个');
        
    }
   
    
    /**
     * 预订订单
     */
    public function reserve(){
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
    
        $reserve_begin = date('Y-m-d H:i:s', time());//30天前
        $reserve_end = date('Y-m-d H:i:s', strtotime('+1 days'));

        $this->assign('reserve_begin',$reserve_begin);
        $this->assign('reserve_end',$reserve_end);

        $begin =  $this->begin;
        $end   =  $this->end;
        $this->assign('begin',date('Y-m-d 00:00:00', $begin));
        $this->assign('end',date('Y-m-d 23:59:59', $end));
        return $this->fetch();
    }
    
    
    public function ajaxReserve()
    {
        $select_year = $this->select_year;
    
        $begin = null;
        $end = null;
        
        
        //两个都有值才算
        if(!isEmptyObject(I('start_time'))&&!isEmptyObject(I('end_time'))){
            $begin = strtotime(I('start_time'));
            $end = strtotime(I('end_time'));
        }elseif(isEmptyObject(I('start_time'))&&!isEmptyObject(I('end_time'))){
            $begin = strtotime("2018-01-01 00:00:00");//30天前
            $end = strtotime(I('end_time'));
        }elseif(!isEmptyObject(I('start_time'))&&isEmptyObject(I('end_time'))){
            $begin = strtotime(I('start_time'));
            $end = time();
        }
        
       
        
        
//        $begin =  $this->begin;
//        $end   =  $this->end;
        
        // 搜索条件 STORE_ID
        $condition = array('o.seller_id' => SELLER_ID, 'o.deleted' => 0); // 商家id
        I('store_id') ? $condition['o.store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = array('like','%'.trim(I('consignee')).'%') : false;
    
        $condition['o.prom_type'] = 4;//只要预订的订单

        if ($begin && $end) {
            $condition['o.create_time'] = array('between', "$begin,$end");
        }
    
//        var_dump($condition);die;

        I('order_status') != '' ? $condition['o.order_status'] = I('order_status/d') : false;
//        I('pay_status') != '' ? $condition['o.pay_status'] = I('pay_status/d') : false;
//        I('pay_code') != '' ? $condition['o.pay_code'] = I('pay_code') : false;
//        I('shipping_status') != '' ? $condition['o.shipping_status'] = I('shipping_status/d') : false;
//        I('order_statis_id/d') != '' ? $condition['o.order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
    
        I('reserve_status') != '' ? $condition['r.reserve_status'] = I('reserve_status/d') : false;


        if (I('reserve_status/d') === 0) {
            $condition['r.reserve_status'] = null;
//            $condition['o.pay_status'] = 1;
        }
        
        
        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
//        $condition['prom_type'] = array('lt',5);

//
//        $condition['g.is_on_sale'] = 1;//必须要上架
//        $condition['g.is_deleted'] = 0;//必须非删除


        I('order_sn') ? $condition['o.order_sn'] = array('like','%'.trim(I('order_sn')).'%') : false;

//        if (I('order_sn')) {
//            $condition = [];
//            $condition['o.order_sn'] = trim(I('order_sn'));
//        }


        $count = OrderModel::alias('o')
            ->join('tp_order_reserve r', 'o.order_id = r.order_id', 'LEFT')
            ->join('tp_order_goods og','o.order_id = og.order_id')
            ->join('tp_goods g','g.goods_id = og.goods_id')
            ->where($condition)
            ->field('o.*,r.reserve_status,r.start_time,r.end_time')
            ->count();


        $Page = new AjaxPage($count, 20);
        $show = $Page->show();
        //获取订单列表
        //$orderList = $orderLogic->getOrderList($condition, $sort_order, $Page->firstRow, $Page->listRows);
        $orderList = OrderModel::alias('o')
            ->join('tp_order_reserve r', 'o.order_id = r.order_id', 'LEFT')
            ->join('tp_order_goods og','o.order_id = og.order_id')
            ->join('tp_goods g','g.goods_id = og.goods_id')
            ->where($condition)->limit("{$Page->firstRow},{$Page->listRows}")->order($sort_order)
            ->field('o.*,r.reserve_status,r.start_time,r.end_time')
            ->select();

        foreach ($orderList as &$order){
            if ($order['order_status'] == 3 ) {
                $order['status_tip'] = '已取消';
                //return -1;
            }

            if ($order['order_status'] == 0 && $order['pay_status'] == 0 && $order['shipping_status'] == 0) {
                $order['status_tip'] = '待支付';
                //return 0;
            }

            if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 0) {
                $order['status_tip'] = '待发货';
                //return 1;
            }

            if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 1) {
                $order['status_tip'] = '待收货';
                //return 2;
            }

            if (in_array($order['order_status'] ,[4,5,6,7,8])) {
                $order['status_tip'] = '交易完成';
                //return 3;
            }
        }


        //获取每个订单的商品列表
        
        
        $store_id_arr = get_arr_column($orderList, 'store_id');
        $stores = Db::name('store')->where('store_id','in',$store_id_arr)->field('store_id,store_name')->select();
        foreach ($orderList as &$order){
            foreach ($stores as $store){
                if($order['store_id']==$store['store_id']){
                    $order['store_name'] = $store['store_name'];
                }
            }
        }
        $order_id_arr = get_arr_column($orderList, 'order_id');
        if (!empty($order_id_arr)) ;
        if ($order_id_arr) {
            $goods_list = Db::name('order_goods'.$select_year)->where("order_id", "in", implode(',', $order_id_arr))->select();
            $goods_arr = array();
            
            
            
            foreach ($goods_list as $v) {
                $goods_arr[$v['order_id']][] = $v;
            }
            
           
            
            $this->assign('goodsArr', $goods_arr);
        }
        $this->assign('orderList', $orderList);
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('pager', $Page);
        return $this->fetch('ajax_reserve');
    }
    
    /*
    *回收订单首页
    */
    public function recycle_list()
    {
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        $RECYCLE_ORDER_STATUS = config('RECYCLE_ORDER_STATUS');
        $this->assign('RECYCLE_ORDER_STATUS',$RECYCLE_ORDER_STATUS);
        return $this->fetch();
    }
    
    /*
    *Ajax首页
    */
    public function ajax_recycle_list()
    {
        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;
        
        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;
        
        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1
        
        if (I('start_time') && I('end_time')) {
            $condition['create_time'] = array('between', "$begin,$end");
        }
        
        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
        I('order_status') != '' ?  : false;
        
        
        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }
            
        }

        if(!isEmptyObject(I('status'))){
            $condition['status'] = I('status');
        }


//        32
        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
        
        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
        $condition['prom_type'] = array('eq',7);//只显示爱回收
        
        
        $count = Db::name('order'.$select_year)->where($condition)->count();
        $Page = new AjaxPage($count, 20);
        $show = $Page->show();
        //获取订单列表
        //$orderList = $orderLogic->getOrderList($condition, $sort_order, $Page->firstRow, $Page->listRows);
        $orderList = Db::name('order'.$select_year)->where($condition)->limit("{$Page->firstRow},{$Page->listRows}")->order($sort_order)->select();
        
        
        //获取每个订单的商品列表
        
        
        $store_id_arr = get_arr_column($orderList, 'store_id');
        $stores = Db::name('store')->where('store_id','in',$store_id_arr)->field('store_id,store_name')->select();
        foreach ($orderList as &$order){
            foreach ($stores as $store){
                if($order['store_id']==$store['store_id']){
                    $order['store_name'] = $store['store_name'];
                }
            }
        }
        $order_id_arr = get_arr_column($orderList, 'order_id');
        if (!empty($order_id_arr)) ;
        if ($order_id_arr) {
            $goods_list = Db::name('order_goods'.$select_year)->where("order_id", "in", implode(',', $order_id_arr))->select();
            $goods_arr = array();
            foreach ($goods_list as $v) {
                $goods_arr[$v['order_id']][] = $v;
            }
            $this->assign('goodsArr', $goods_arr);
        }
        
        $coupon_list = Db::name('CouponList')->where('get_order_id','in',$order_id_arr)->select();
        
        foreach ($orderList as &$order){
            foreach ($coupon_list as $coupon){
                if($order['order_id']==$coupon['get_order_id']){
                    $order['isSendCoupon'] = true;
                }
            }

            if(!isEmptyObject($order['recycle_info'])){
                $order['recycle_info'] = json_decode($order['recycle_info'],true);
            }else{
                $order['recycle_info'] = [];
            }
        }


        
        
        
        
        
        $state = config('ORDER_STATUS');
        $this->assign('state',$state);
        $this->assign('orderList', $orderList);
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('pager', $Page);
        return $this->fetch();
    }
    
    public function recycle_detail(){
        
        $param = $this->request->param();
        $order = OrderModel::get(['order_sn' => $param['order_sn']]);
        if (!$order) {
            $this->error('订单不存在！');
        }
    
    
        $transactionId = $order['transaction_id'];
        //var_dump($transactionId);die;
    
        $aiHiShouClass = new  AHSClient('api');
        
        $info = $aiHiShouClass->ordersDetail_manager($transactionId);
        if(isset($info['code'])&&$info['code']!=0){
            $this->error('爱回收返回信息:'.$info['message']);
        }
        
        $isSendCoupon = false;
        
        $isHasSendCouponList = Db::name('CouponList')->where('get_order_id',$order['order_id'])->find();
        if($isHasSendCouponList){
            $isSendCoupon = true;
            if(isset($isHasSendCouponList['order_id'])&&!isEmptyObject($isHasSendCouponList['order_id'])){
                $isHasSendCouponList['order_sn'] = Db::name('order')->where('order_id',$isHasSendCouponList['order_id'])->value('order_sn');
            }
        }

        if(!isEmptyObject($order['recycle_info'])){
            $info['recycle_info'] = $order['recycle_info'];//model里面有转换的
        }else{
            $info['recycle_info'] = null;
        }
        
        $this->assign('isSendCoupon',$isSendCoupon);
        $this->assign('isHasSendCouponList',$isHasSendCouponList);
        
        $user = Db::name('users')->where('user_id',$order['user_id'])->find();
        $this->assign('user',$user);
        $this->assign('info',$info);
        $this->assign('order',$order);
        return $this->fetch();
    }
    

    /*
     *订单首页
     */
    public function index()
    {
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        return $this->fetch();
    }
    
    
    
    

    /*
     *Ajax首页
     */
    public function ajaxindex()
    {
        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;
        
        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = array('like','%'.trim(I('consignee')).'%') : false;
    
        //hack注册优惠券和回收优惠券
        if(''!== I('prom_type')){
            
            if(!empty(I('prom_type'))){
                if(in_array(I('prom_type'),array('register','recycle'))){
                    
                    //还是正常
                    $condition['prom_type'] = array('eq',0);

                    $filter['cl.order_id'] = array('gt',0);
                    if(I('prom_type')=='register'){
                        $filter['c.type'] = 4;
                    }

                    if(I('prom_type')=='recycle'){
                        $filter['c.type'] = 5;
                    }


                    $order_list =  $coupon_list = Db::name('CouponList')
                        ->alias('cl')
                        ->join('tp_coupon c','c.id=cl.cid')
                        ->where($filter)->where('cl.status',1)
                        ->field('cl.order_id')->select();
                    
                    $order_ids = get_arr_column($order_list,'order_id');

                    $condition['order_id'] = array('in',$order_ids);
                }else{
                    $condition['prom_type'] = array('eq',I('prom_type'));
                }
            }else{
                $condition['prom_type'] = array('eq',I('prom_type'));;
            } 
        }   
         //? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1
        
        if (I('start_time') && I('end_time')) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

        I('order_sn') ? $condition['order_sn'] = array('like','%'.trim(I('order_sn')).'%') : false;
        I('order_status') != '' ?  : false;
        
       
        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }
            
        }
        
        
//        32
        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
        
        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
        
        
        $condition['prom_type'] = !empty($condition['prom_type']) ? $condition['prom_type'] : array('neq',7);//干掉爱回收
        
        //print_r($condition);
        $count = Db::name('order'.$select_year)->where($condition)->count();
        $Page = new AjaxPage($count, 20);
        $show = $Page->show();
        //获取订单列表
        //$orderList = $orderLogic->getOrderList($condition, $sort_order, $Page->firstRow, $Page->listRows);
//        $orderList = Db::name('order'.$select_year)->where($condition)->limit("{$Page->firstRow},{$Page->listRows}")->order($sort_order)->select();

        $tempList = OrderModel::where($condition)->limit("{$Page->firstRow},{$Page->listRows}")->order($sort_order)->select();


        $orderList = [];
        foreach ($tempList as &$item){
//            $item->getStatus();
            $orderList[] = json_decode($item,true);
        }



        foreach ($orderList as &$order){
            if($order['prom_type']==4||$order['prom_type']==7){

                if ($order['order_status'] == 3 ) {
                    $order['status_tip'] = '已取消';
                    //return -1;
                }

                if ($order['order_status'] == 0 && $order['pay_status'] == 0 && $order['shipping_status'] == 0) {
                    $order['status_tip'] = '待支付';
                    //return 0;
                }

                if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 0) {
                    $order['status_tip'] = '待发货';
                    //return 1;
                }

                if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 1) {
                    $order['status_tip'] = '待收货';
                    //return 2;
                }

                if (in_array($order['order_status'] ,[4,5,6,7,8])) {
                    $order['status_tip'] = '交易完成';
                    //return 3;
                }


            }
        }








        //获取每个订单的商品列表
        
    
        $store_id_arr = get_arr_column($orderList, 'store_id');
        $stores = Db::name('store')->where('store_id','in',$store_id_arr)->field('store_id,store_name')->select();
        foreach ($orderList as &$order){
            foreach ($stores as $store){
                if($order['store_id']==$store['store_id']){
                    $order['store_name'] = $store['store_name'];
                }
            }
        }
        $order_id_arr = get_arr_column($orderList, 'order_id');
        if (!empty($order_id_arr)) ;
        if ($order_id_arr) {

            $goods_list = OrderGoods::where("order_id", "in", implode(',', $order_id_arr))->select();


//            $goods_list = Db::name('order_goods'.$select_year)->where("order_id", "in", implode(',', $order_id_arr))->select();
            $goods_arr = array();
            foreach ($goods_list as $v) {
//                $v->getStatus();

//                $goods_arr[$v['order_id']][] = $v;
                $goods_arr[$v['order_id']][] = json_decode($v,true);
            }

            //var_dump($goods_arr);die;



    
            //20190214-20190331 买东西送耳机
            $tmpls = [1224,1223,1222,1216,1215,1214];
            $s = 1549814400;
            $e = 1553961599;
            
            if($goods_arr){   
                foreach ($goods_arr as &$k){


                    if(strtotime($k[0]['create_time'])>$s && strtotime($k[0]['create_time'])<$e){
                        $goods_id_arr = get_arr_column($k,'goods_id');
                        $isHasTmplActiveGoods = Db::name('goods')->where('goods_id','in',$goods_id_arr)->where('tmpl_id','in',$tmpls)->find();//只要有商品tmpl_id满足的，就这么弄。
                        if($isHasTmplActiveGoods){
                            $k[] = ['goods_name'=>'Beats Solo 3 Wireless','goods_num'=>1,'goods_id'=>null,'final_price'=>'0.00','pic'=>'http://shop-pic-video.oss-cn-beijing.aliyuncs.com/public/upload/seller/18/goods/03ce6b76971af429f494af8ce15e4eb6215890694830.jpg'];
                        }

                    }
                }
            }

//            var_dump($goods_arr);die;



            
            $this->assign('goodsArr', $goods_arr);
            
            $coupon_list = Db::name('CouponList')
                ->alias('cl')
                ->join('tp_coupon c','c.id=cl.cid')
                ->where('cl.order_id','in',$order_id_arr)->where('cl.status',1)
                ->field('c.type,c.id,cl.order_id')->select();
            
            foreach ($coupon_list as $coupon){
                foreach ($orderList as &$order){
                    if($coupon['order_id']==$order['order_id']){
                        $order['coupon_type'] = $coupon['type'];
                    }
                }
            }
        }


       
        //print_r($orderList);exit;
        
        $state = config('ORDER_STATUS');
        $this->assign('state',$state);
        $this->assign('orderList', $orderList);
        $this->assign('page', $show);// 赋值分页输出
        $this->assign('pager', $Page);
        return $this->fetch();
    }

    // 虚拟商品
    public function virtual_list(){
   	header("Content-type: text/html; charset=utf-8");
exit("请联系TPshop官网客服购买高级版支持此功能");
    }
    
    public function virtual_info(){
    	$order_id = I('order_id/d');
        // 获取操作表
        $select_year = getTabByOrderId($order_id);        
    	$order = Db::name('order'.$select_year)->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->find();
        !$order && $this->error('非法操作！！');
    	if($order['pay_status'] == 1){
    		$vrorder = Db::name('vr_order_code')->where(array('order_id'=>$order_id))->select();
            if(!empty($vrorder)) {
                foreach ($vrorder as $k=>$vrorderVal) {
                    if ($vrorderVal['vr_indate'] < time() && $vrorder['vr_state'] == 0) { 
                        $vrorder[$k]['vr_state'] = 2;//看看有没有过期
                        Db::name('vr_order_code')->where(array('order_id' => $order_id))->update(['vr_state' => 2]);
                        break;
                    }
                }
            }
    		$this->assign('vrorder',$vrorder);
    	}
    	$order_goods = Db::name('order_goods'.$select_year)->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->find();
    	$order_goods['virtual_indate'] = Db::name('goods')->where(array('goods_id'=>$order_goods['goods_id'],'store_id'=>STORE_ID))->getField('virtual_indate');
    	$this->assign('order',$order);
    	$this->assign('order_goods',$order_goods);
    	return $this->fetch();
    }
    
    public function virtual_cancel(){
    	$order_id = I('order_id/d');
    	if(IS_POST){
    		$admin_note = I('admin_note');
    		$order = Db::name('order')->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->find();
    		if($order){
    			$r = Db::name('order')->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->save(array('order_status'=>3,'admin_note'=>$admin_note));
    			if($r){
    				$orderLogic = new OrderLogic();
                    $seller_id = session('seller_id');
                    $orderLogic->orderActionLog($order, '取消订单', $admin_note, $seller_id, 1);
    				$this->ajaxReturn(array('status'=>1,'msg'=>'操作成功'));
    			}else{
    				$this->ajaxReturn(array('status'=>-1,'msg'=>'操作失败'));
    			}
    		}else{
    			$this->ajaxReturn(array('status'=>-1,'msg'=>'订单不存在'));
    		}
    	}
    	$order = Db::name('order')->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->find();
    	$this->assign('order',$order);
    	return $this->fetch();
    }
    
    public function verify_code(){
    	if(IS_POST){
    		$vr_code = trim(I('vr_code'));
    		if (!preg_match('/^[a-zA-Z0-9]{15,18}$/',$vr_code)) {
    			$this->ajaxReturn(array('status' =>-1,'msg'=>'兑换码格式错误，请重新输入'));
    		}
    		$vr_code_info = Db::name('vr_order_code')->where(array('vr_code'=>$vr_code))->find();
            $order = Db::name('order')->where(['order_id'=>$vr_code_info['order_id']])->field('order_status,order_sn,user_note')->find();
            if($order['order_status'] < 1 ){
                $this->ajaxReturn(array('status' =>-1,'msg'=>'兑换码对应订单状态不符合要求'));
            }
            if(empty($vr_code_info) || $vr_code_info['store_id'] != STORE_ID){
                $this->ajaxReturn(array('status' =>-1,'msg'=>'该兑换码不存在'));
            }
    		if(empty($vr_code_info) || $vr_code_info['store_id'] != STORE_ID){
    			$this->ajaxReturn(array('status' =>-1,'msg'=>'该兑换码不存在'));
    		}
    		if ($vr_code_info['vr_state'] == '1') {
    			$this->ajaxReturn(array('status' =>-1,'msg'=>'该兑换码已被使用'));
    		}
    		if ($vr_code_info['vr_indate'] < time()) {
    			$this->ajaxReturn(array('status' =>-1,'msg'=>'该兑换码已过期，使用截止日期为： '.date('Y-m-d H:i:s',$vr_code_info['vr_indate'])));
    		}
    		if ($vr_code_info['refund_lock'] > 0) {//退款锁定状态:0为正常,1为锁定(待审核),2为同意
    			$this->ajaxReturn(array('status' =>-1,'msg'=>'该兑换码已申请退款，不能使用'));
    		}
    		$update['vr_state'] = 1;
    		$update['vr_usetime'] = time();
    		Db::name('vr_order_code')->where(array('vr_code'=>$vr_code))->save($update);
    		//检查订单是否完成
    		$condition = array();
    		$condition['vr_state'] = 0;
    		$condition['refund_lock'] = array('in',array(0,1));
    		$condition['order_id'] = $vr_code_info['order_id'];
    		$condition['vr_indate'] = array('gt',time());
    		$vr_order = Db::name('vr_order_code')->where($condition)->select();
    		if(empty($vr_order)){
    			$data['order_status'] = 2;  //此处不能直接为4，不然前台不能评论
    			$data['confirm_time'] = time();
    			Db::name('order')->where(['order_id'=>$vr_code_info['order_id']])->save($data);
    			Db::name('order_goods')->where(['order_id'=>$vr_code_info['order_id']])->save(['is_send'=>1]);  //把订单状态改为已收货
    		}
    		$order_goods = Db::name('order_goods')->where(['order_id'=>$vr_code_info['order_id']])->find();
    		if($order_goods){
                $result = [
                    'vr_code'=>$vr_code,
                    'order_goods'=>$order_goods,
                    'order'=>$order,
                    'goods_image'=>goods_thum_images($order_goods['goods_id'],240,240),
                ];
                $this->ajaxReturn(['status'=>1,'msg'=>'兑换成功','result'=>$result]);
    		}else{
    			$this->ajaxReturn(['status' =>-1,'msg'=>'虚拟订单商品不存在']);
    		}
    	}
    	 return $this->fetch();
    }

    /*
     * ajax 发货订单列表
    */
    public function ajaxdelivery()
    {
        $begin =  $this->begin;
        $end   =  $this->end;
        $select_year = $this->select_year; // 表后缀

        $where = array();
        //如果有搜索就指定店铺啦
        if(I('store_id')){
            $where['store_id'] = I('store_id');
        }else{
            $where['store_id'] = array('in',STORES);
        }


        $condition = array('deleted' =>0);
        if ($begin && $end) {
            $condition['add_time'] = array('between', "$begin,$end");
        }
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;
        I('order_sn') != '' ? $condition['order_sn'] = trim(I('order_sn')) : false;
        $shipping_status = I('shipping_status');
        $condition['shipping_status'] = empty($shipping_status) ? 0 : $shipping_status;
        $condition['order_status'] = array('in', '1,2,4,5,7');
        $condition['prom_type'] = array('in','0,1,2,3,4,6');
        $count = Db::name('order'.$select_year)->where($condition)->where($where)->count();
        $Page = new AjaxPage($count, 10);
        $show = $Page->show();
        $orderList = Db::name('order'.$select_year)->where($condition)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->order('add_time DESC')->select();
        //@new 新UI 需要 {
        //获取每个订单的商品列表
        $order_id_arr = get_arr_column($orderList, 'order_id');
        //查询所有订单的所有商品
        if (count($order_id_arr) > 0) {

            //过滤掉已经退货或者退款成功或者在过程中的
            //退款成功的
            $goods_id_arr_cancel = Db::name('return_goods')->where("order_id", "in", implode(',', $order_id_arr))->where('status','in',[0,1,2,3,4,5,6])->column('rec_id');




            if(count($goods_id_arr_cancel)){


                $goods_list = Db::name('order_goods'.$select_year)->where("order_id", "in", implode(',', $order_id_arr))
                    ->where('rec_id','notin',$goods_id_arr_cancel)
                    ->select();
            }else{
                $goods_list = Db::name('order_goods'.$select_year)->where("order_id", "in", implode(',', $order_id_arr))
                    ->select();
            }

            $goods_arr = array();
            foreach ($goods_list as $v) {
                $goods_arr[$v['order_id']][] = $v;
            }
            $this->assign('goodsArr', $goods_arr);
        }
        //查询所有订单的用户昵称
        $user_id_arr = get_arr_column($orderList, 'user_id');
        if (count($user_id_arr) > 0) {
            $users = Db::name('users')->where("user_id", "in", implode(',', $user_id_arr))->getField("user_id,email,mobile");
           
            $this->assign('users', $users);
//            echo '<pre>';
//            var_dump($users);die;
        }
        

        //}
        $this->assign('orderList', $orderList);
        $this->assign('page', $show);// 赋值分页输出
        return $this->fetch();
    }

    /**
     * 订单详情
     */
    public function detail()
    {
        $order_id = input('order_id');
        $Order = new OrderModel();
        $OrderGoods = new OrderGoods();
        $OrderAction = new OrderAction();
        $select_year = getTabByOrderId($order_id);
        $order = OrderModel::get(['order_id'=>$order_id]);
        if(!$order){
        	$this->error('该订单不存在或没有权利查看', U('Seller/Order/index'));
        }

        if ($order['order_status'] == 3 ) {
            $order['status_tip'] = '已取消';
            //return -1;
        }

        if ($order['order_status'] == 0 && $order['pay_status'] == 0 && $order['shipping_status'] == 0) {
            $order['status_tip'] = '待支付';
            //return 0;
        }

        if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 0) {
            $order['status_tip'] = '待发货';
            //return 1;
        }

        if ($order['order_status'] == 1 && $order['pay_status'] == 1 && $order['shipping_status'] == 1) {
            $order['status_tip'] = '待收货';
            //return 2;
        }

        if (in_array($order['order_status'] ,[4,5,6,7,8])) {
            $order['status_tip'] = '交易完成';
            //return 3;
        }
        
        
//        echo '<pre>';
//        var_dump($order);die;

//        $order->getStatus();

//        $order_goods = Db::name('order_goods'.$select_year)
//            ->alias('g')
//            ->join('tp_return_goods r','g.rec_id = r.rec_id','LEFT')
//            ->where(['g.order_id'=>$order_id])
//            ->field('g.*,r.status')
//            ->select();

        $order_goods = OrderGoods::where('order_id', $order_id)->select();

//        foreach ($order_goods as &$goods){
//            $goods->getStatus();
//        }

        //20190214-20190331 买东西送耳机
        $tmpls = [1224,1223,1222,1216,1215,1214];
        $s = 1549814400;
        $e = 1553961599;
        if(strtotime($order_goods[0]['create_time'])>$s && strtotime($order_goods[0]['create_time'])<$e){
    
            $goods_id_arr = get_arr_column($order_goods,'goods_id');
            $isHasTmplActiveGoods = Db::name('goods')->where('goods_id','in',$goods_id_arr)->where('tmpl_id','in',$tmpls)->find();//只要有商品tmpl_id满足的，就这么弄。
            if($isHasTmplActiveGoods){
                $order_goods[] = ['goods_name'=>'Beats Solo 3 Wireless','goods_num'=>1,'goods_id'=>null,'final_price'=>'0.00','pic'=>'http://shop-pic-video.oss-cn-beijing.aliyuncs.com/public/upload/seller/18/goods/03ce6b76971af429f494af8ce15e4eb6215890694830.jpg'];
            }
        }
        
        $RETURN_STATUS = config('RETURN_STATUS');
        foreach ($order_goods as &$goods){
            $return_order = Db::name('ReturnGoods')->where('rec_id', $goods['rec_id'])->order('id desc')->find();
            if ($return_order) {
                $goods['status'] = '退款结果:' . $RETURN_STATUS[$return_order['status']];
            } else {
                $goods['status'] = null;
            }
        }
        
        
        
        // 获取操作记录
        $action_log = $OrderAction->name('order_action'.$select_year)->where(['order_id'=>$order_id])->order('log_time desc')->select();



        $express = Db::name('delivery_doc'.$select_year)->where("order_id" , $order_id)->select();  //发货信息（可能多个）
        if($order['is_comment'] == 1){
            $comment_time = Db::name('comment')->where('order_id' , $order['order_id'])->order('comment_id desc')->value('add_time');
            $this->assign('comment_time', $comment_time); //查询评论时间
        }
        $orderLogic = new OrderLogic();
        $seller_button = $orderLogic->getOrderButton($order);


        $user = Db::name('users')->where('user_id',$order['user_id'])->find();
        $this->assign('user',$user);
    
        $coupon_info = Db::name('CouponList')
            ->alias('cl')
            ->join('tp_coupon c','c.id=cl.cid')
            ->where('cl.order_id',$order_id)->where('cl.status',1)
            ->field('c.type,c.id,cl.order_id')->find();
    
        if($order&&$coupon_info){
            $order['coupon_type'] = $coupon_info['type'];
        }
       
        

        
        $invoice = Db::name('invoice')->where('order_id',$order_id)->find();
        $this->assign('invoice',$invoice);
        
        $this->assign('seller_button',$seller_button);
        $this->assign('order', $order);
        $this->assign('action_log', $action_log);
        $this->assign('order_goods', $order_goods);
        $this->assign('express', $express);

        //var_dump($order);die;
        return $this->fetch();
    }

    /**
     * 订单编辑
     */
    public function edit_order()
    {
        $order_id = I('order_id/d');
        $orderLogic = new OrderLogic();
        $order = $orderLogic->getOrderInfo($order_id);
        if ($order['store_id'] != STORE_ID) {
            $this->error('该订单不存在', U('Seller/Order/index'));
        }
        if ($order['shipping_status'] != 0) {
            $this->error('已发货订单不允许编辑');
            exit;
        }

        $orderGoods = $orderLogic->getOrderGoods($order_id);

        if (IS_POST) {
            $order['consignee'] = I('consignee');// 收货人
            $order['province'] = I('province'); // 省份
            $order['city'] = I('city'); // 城市
            $order['district'] = I('district'); // 县
            $order['address'] = I('address'); // 收货地址
            $order['mobile'] = I('mobile'); // 手机           
            $order['invoice_title'] = I('invoice_title');// 发票
            $order['admin_note'] = I('admin_note'); // 管理员备注
            $order['admin_note'] = I('admin_note'); //                  
            $order['shipping_code'] = I('shipping');// 物流方式
            $order['shipping_name'] = Db::name('shipping')->where(['shipping_code' => $order['shipping_code']])->getField('shipping_name');
            $order['pay_code'] = I('payment');// 支付方式
            $order['pay_name'] = I('payname');
            $goods_id_arr = I("goods_id/a");
            $new_goods = $old_goods_arr = array();
            //################################订单添加商品
            if ($goods_id_arr) {
                $new_goods = $orderLogic->get_spec_goods($goods_id_arr);
                foreach ($new_goods as $key => $val) {
                    $val['order_id'] = $order_id;
                    $val['store_id'] = STORE_ID;
                    $rec_id = Db::name('order_goods')->add($val);//订单添加商品
                    if (!$rec_id)
                        $this->error('添加失败');
                }
            }

            if($order['pay_status']==0){   //订单未支付才修改商品，订单费用
                //################################订单修改删除商品
                $old_goods = I('old_goods/a');
                foreach ($orderGoods as $val) {
                    if (empty($old_goods[$val['rec_id']])) {
                        Db::name('order_goods')->where("rec_id", $val['rec_id'])->delete();//删除商品
                    } else {
                        //修改商品数量
                        if ($old_goods[$val['rec_id']] != $val['goods_num']) {
                            $val['goods_num'] = $old_goods[$val['rec_id']];
                            Db::name('order_goods')->where("rec_id", $val['rec_id'])->save(array('goods_num' => $val['goods_num']));
                        }
                        $old_goods_arr[] = $val;
                    }
                }

                $goodsArr = array_merge($old_goods_arr, $new_goods);
                $pay = new Pay();
                try{
                    $pay->setUserId($order['user_id']);
                    $pay->payOrder($goodsArr);
                    $pay->delivery($order['district']);
                }catch (TpshopException $t){
                    $error = $t->getErrorArr();
                    $this->error($error['msg']);
                }

                //################################修改订单费用
                $order['goods_price'] = $pay->getGoodsPrice(); // 商品总价
                $order['shipping_price'] = $pay->getShippingPrice();//物流费
                $order['order_amount'] = $pay->getOrderAmount(); // 应付金额
                $order['total_amount'] = $pay->getTotalAmount(); // 订单总价
                $o = Db::name('order')->where(['order_id' => $order_id, 'store_id' => STORE_ID])->save($order);
            }else{  //已支付订单只能修改下配送方式，配送地址
                $o = Db::name('order')->where(['order_id' => $order_id, 'store_id' => STORE_ID])->save($order);
            }


            $seller_id = session('seller_id');
            $l = $orderLogic->orderActionLog($order, '编辑订单', '修改订单', $seller_id, 1);//操作日志
            if ($o && $l) {
                $this->success('修改成功', U('Order/editprice', array('order_id' => $order_id)));
            } else {
                $this->success('修改失败', U('Order/detail', array('order_id' => $order_id)));
            }
            exit;
        }
        // 获取省份
        $province = Db::name('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        //获取订单城市
        $city = Db::name('region')->where(array('parent_id' => $order['province'], 'level' => 2))->select();
        //获取订单地区
        $area = Db::name('region')->where(array('parent_id' => $order['city'], 'level' => 3))->select();
        //获取支付方式
        $payment_list = Db::name('plugin')->where(array('status' => 1, 'type' => 'payment'))->select();
        //获取配送方式
        $shipping_list = Db::name('store_shipping')->field('s.shipping_name,s.shipping_code')
            ->alias('ss')->join('__SHIPPING__ s','ss.shipping_id = s.shipping_id')->where('ss.store_id',STORE_ID)->select();
        $this->assign('order', $order);
        $this->assign('province', $province);
        $this->assign('city', $city);
        $this->assign('area', $area);
        $this->assign('orderGoods', $orderGoods);
        $this->assign('shipping_list', $shipping_list);
        $this->assign('payment_list', $payment_list);
        return $this->fetch();
    }

    /*
     * 拆分订单
     */
    public function split_order()
    {
        $order_id = I('order_id/d');
        $orderLogic = new OrderLogic();
        $order = $orderLogic->getOrderInfo($order_id);
        if ($order['store_id'] != STORE_ID) {
            $this->error('该订单不存在', U('Seller/Order/index'));
        }
        if ($order['pay_status'] != 1) {
            $this->error('未支付订单不允许编辑');
            exit;
        }
        if ($order['shipping_status'] != 0) {
            $this->error('已发货订单不允许编辑');
            exit;
        }
        $orderGoods = $orderLogic->getOrderGoods($order_id);
        if (IS_POST) {
            //################################先处理原单剩余商品和原订单信息
            $old_goods = I('old_goods/a');
            $oldGoodsPrice  = 0;
            foreach ($orderGoods as $val) {
                if (empty($old_goods[$val['rec_id']])) {
                    Db::name('order_goods')->where("rec_id", $val['rec_id'])->delete();//删除商品
                } else {
                    //修改商品数量
                    if ($old_goods[$val['rec_id']] != $val['goods_num']) {
                        $val['goods_num'] = $old_goods[$val['rec_id']];
                        Db::name('order_goods')->where("rec_id", $val['rec_id'])->save(array('goods_num' => $val['goods_num']));
                    }
                    $oldArr[] = $val;//剩余商品
                    $oldGoodsPrice += $val['goods_price'];
                }
                $all_goods[$val['rec_id']] = $val;//所有商品信息
            }
            $pay = new Pay();
            try{
                $pay->setUserId($order['user_id']);
                $pay->payOrder($oldArr);
                $pay->delivery($order['district']);
            }catch (TpshopException $t){
                $error = $t->getErrorArr();
                $this->error($error['msg']);
            }
            //修改订单费用
            $res['goods_price'] = $pay->getGoodsPrice(); // 商品总价
            $res['order_amount'] = $pay->getOrderAmount(); // 应付金额
            $res['status'] = $order['status']; // 订单状态
            $res['pay_status'] = $order['pay_status']; // 支付状态
            $res['total_amount'] = $pay->getTotalAmount(); // 订单总价
            if($order['user_money'] > 0 || $order['integral']>0){
            	$res['user_money'] = round($oldGoodsPrice/$order['goods_price']*$order['user_money'],2);
            	$res['integral'] = floor($oldGoodsPrice/$order['goods_price']*$order['integral']);
            }
            Db::name('order')->where(['order_id' => $order_id, 'store_id' => STORE_ID])->save($res);
            //################################原单处理结束

            //################################新单处理
            for ($i = 1; $i < 20; $i++) {
                if (!empty($_POST[$i . '_old_goods'])) {
                    $split_goods[] = $_POST[$i . '_old_goods'];//新订单商品
                }
            }
            foreach ($split_goods as $key => $vrr) {
                foreach ($vrr as $k => $v) {
                    $all_goods[$k]['goods_num'] = $v;
                    $brr[$key][] = $all_goods[$k];
                }
            }

            foreach ($brr as $goods) {
                $pay = new Pay();
                try{
                    $pay->setUserId($order['user_id']);
                    $pay->payOrder($goods);
                    $pay->delivery($order['district']);
                }catch (TpshopException $t){
                    $error = $t->getErrorArr();
                    $this->error($error['msg']);
                }
                $new_order = $order;
                $new_order['order_sn'] = date('YmdHis') . mt_rand(1000, 9999);
                $new_order['parent_sn'] = $order['order_sn'];
                $new_order['user_money'] = 0;
                $new_order['integral'] = 0;
                if($order['user_money'] > 0 || $order['integral']>0){
                	$new_order['user_money'] = round($pay->getGoodsPrice()/$order['goods_price']*$order['user_money'],2);
                	$new_order['integral'] = floor($pay->getGoodsPrice()/$order['goods_price']*$order['integral']);
                	$new_order['integral_money'] = $new_order['integral']/((int)tpCache('shopping.point_rate'));
                }
                //修改订单费用
                $new_order['goods_price'] = $pay->getGoodsPrice(); // 商品总价
                $new_order['order_amount'] = $pay->getGoodsPrice() - $new_order['user_money'] - $new_order['integral_money']; // 应付金额
                $new_order['total_amount'] = $pay->getGoodsPrice(); // 订单总价
                $new_order['add_time'] = time();

                $new_order['shipping_price'] = 0;//商家主动拆单物流费用忽略
                $new_order['pay_status'] = $order['pay_status'];
                $new_order['shipping_status'] = $order['shipping_status'];
                $new_order['order_status'] = $order['order_status'];
                unset($new_order['order_id']);
                $new_order_id = Db::name('order')->add($new_order);//插入订单表
               foreach ($goods as $vv) {
                    $vv['order_id'] = $new_order_id;//新订单order_id
                    unset($vv['rec_id']);
                    $vv['store_id'] = STORE_ID;
                    $nid = Db::name('order_goods')->add($vv);//插入订单商品表
                }
            }
            $orderLogic->orderActionLog($order, 'split','拆分订单',session('seller_id'));
            //################################新单处理结束
            $this->success('操作成功', U('Order/detail', array('order_id' => $order_id)));
            exit;
        }

        foreach ($orderGoods as $val) {
            $brr[$val['rec_id']] = array('goods_num' => $val['goods_num'], 'goods_name' => getSubstr($val['goods_name'], 0, 35) . $val['spec_key_name']);
        }
        $this->assign('order', $order);
        $this->assign('goods_num_arr', json_encode($brr));
        $this->assign('orderGoods', $orderGoods);
        return $this->fetch();
    }

    /*
     * 价钱修改
     */
    public function editprice($order_id)
    {
        $orderLogic = new OrderLogic();
        $order = $orderLogic->getOrderInfo($order_id);
        // if ($order['store_id'] != STORE_ID) {
        //     $this->error('该订单不存在', U('Seller/Order/index'));
        // }
        if ($order['shipping_status'] != 0) {
            $this->error('已发货订单不允许编辑');
        }
        $this->assign('order', $order);
        return $this->fetch();
    }

    /**
     * 修改订单价格
     */
    function editOrderPrice(){
        try{
            $post_data = I('post.',0);
            $orderLogic = new SellerOrder($post_data['order_id']);
            $orderLogic->updataOrderPrice($post_data);
            $this->ajaxReturn(['status'=>1,'msg'=>'提交订单成功']);
        }catch (TpshopException $t){
            $error = $t->getErrorArr();
            $this->ajaxReturn($error);
        }
    }

    /**
     * 订单删除
     */
    public function delete_order()
    {
        $orderLogic = new OrderLogic();
        $order_id = input('order_id/d');
        if(empty($order_id)){
            $this->error('参数错误');
        }
        $order = Db::name('order')->where('order_id',$order_id)->find();
        if(empty($order)){
            $this->error('订单记录不存在');
        }
        if($order['deleted'] == 1){
            $this->error('订单记录已经删除');
        }
        if($order['order_status'] != 5){
            $this->error('只有作废的订单才能删除');
        }
        $del = $orderLogic->delOrder($order_id,STORE_ID);
        if ($del !== false) {
            $this->success('删除订单成功');
        } else {
            $this->error('订单删除失败');
        }
    }

    /**
     * 订单取消付款
     */
    public function pay_cancel($order_id)
    {
        if (I('remark')) {
            $data = I('post.');
            $orderLogic = new OrderLogic();
            $order = $orderLogic->getOrderInfo($data['order_id']);
            if ($order['store_id'] != STORE_ID) {
                $this->error('该订单不存在', U('Seller/Order/index'));
            }
            $note = array('退款到用户余额', '已通过其他方式退款', '不处理，误操作项');
            if ($data['refundType'] == 0 && $data['amount'] > 0) {
                accountLog($data['user_id'], $data['amount'], 0, '退款到用户余额');
            }
            $orderLogic->orderProcessHandle($data['order_id'], 'pay_cancel', STORE_ID);
            $seller_id = session('seller_id');
            $d = $orderLogic->orderActionLog($order, '取消付款', $data['remark'] . ':' . $note[$data['refundType']], $seller_id, 1);
            if ($d) {
                exit("<script>window.parent.pay_callback(1);</script>");
            } else {
                exit("<script>window.parent.pay_callback(0);</script>");
            }
        } else {
            $order = Db::name('order')->where("order_id", $order_id)->find();
            $this->assign('order', $order);
            return $this->fetch();
        }
    }


    /**
     * 退换货操作
     */
    public function return_info()
    {
        $id = I('id');

        $return_goods = M('return_goods')->where("order_id = $id")->find();
        if(!empty($return_goods))
        {
            $this->ajaxReturn(['status'=>0,'msg'=>'已经提交过退货申请!', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);

            exit;
        }


        $order = Db::name('order')->where('order_id',$id)
            ->field('order_id,order_sn,user_id,store_id,seller_id,order_status,pay_status,shipping_status')
            ->find();//订单信息
        $rec = Db::name('order_goods')->where('order_id',$id)->field('rec_id,goods_id,goods_num')->find();//订单商品表

        $data = array_merge($order,$rec);
        $data['reason'] = '商家后台操作退换货';
        $data['type'] = I('type');
        $data['status'] = I('status');
        $data['refund_mark'] = I('refund_mark');
        $data['addtime'] = time();
        $data['is_receive'] = 1;//因为只有确认收货的才能换货。所以这里直接写好
        $data['type'] = 2;//后台这样的逻辑，只能在换货里面了
        $note ="退换货:, 状态:,处理备注：商家后台主动操作退换货";
        $result = M('return_goods')->add($data);
        if($result)
        {
            $type = empty($data['type']) ? 2 : 3;
            $where = " order_id = ".$data['order_id']." and goods_id=".$data['goods_id'];
            M('order_goods')->where($where)->save(array('is_send'=>$type));//更改商品状态
            $orderLogic = new OrderLogic();

            $log = $orderLogic->orderActionLog($order,'退换货',$note,SELLER_ID);
            $this->ajaxReturn(['status'=>1,'msg'=>'修改成功!', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);

            exit;
        }

    }

    /**
     * 取消订单
     */
    public function seller_order_cancel(){

        $id = I('id');


        $order = Db::name('order')->where('order_id',$id)->find();//订单信息

        if(!$order){
            $this->ajaxReturn(['status'=>0,'msg'=>'订单不存在', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);
            exit;
        }
        

        $refund_money = $order['paid_money'];
        if($refund_money>0){
            //1.退款
            switch ($order['pay_code']){
                case 'HBFQ':
                case 'CCFQ':
                    $rt = FenQi::refund($order['order_sn'],$refund_money ,'订单'.$order['order_sn'].'发起退款'.date('YmdHms',time()));
                    break;
                case 'ali':
                case 'wx':
                case 'kq':
                case 'wap':
                    $rt = refundPay::KQRefund($order['order_sn'],$refund_money);
                    break;
                default:

                    $this->ajaxReturn(['status'=>0,'msg'=>'操作失败:支付渠道错误', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);
                    break;
            }

            if($rt['code']!=1){
                $this->ajaxReturn(['status'=>0,'msg'=>'操作失败:'.$rt['msg'], 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);
                die;
            }
        }
    
        //修改商品状态
        Db::name('order')->where('order_id',$id)->update(['order_status'=>3]);



        //必须标记为需要退还的订单，才给退
        if($order['coupon_is_refund']!==1){

            $couponInfo = Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->find();

            
            //2.退优惠券
            Db::name('CouponList')->where(['order_id'=>$order['order_id'],'status'=>1])->update(['status'=>0,'use_time'=>0,'order_id'=>0]);

            
            //优惠券已使用减一
            Db::name('Coupon')->where('id',$couponInfo['cid'])->setDec('use_num',1);


        }
        

        //3.归还商品库存
        $order_goods_list = Db::name('OrderGoods')->where('order_id',$order['order_id'])->select();

        foreach ($order_goods_list as $sku){
            Db::name('SpecGoodsPrice')->where(['goods_id'=>$sku['goods_id'],'key'=>$sku['spec_key']])->setInc('store_count',$sku['goods_num']);
        }


        $orderLogic = new OrderLogic();
        $action = '取消订单';
        $orderLogic->orderActionLog($order, $action,'经销商后台取消订单', SELLER_ID, 1);

        $this->ajaxReturn(['status'=>1,'msg'=>'操作成功', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);
        exit;
    }

    /**
     * 退换货操作
     */
    public function refund_info()
    {
        $id = I('id');

        $return_goods = M('return_goods')->where("order_id = $id")->find();
        if(!empty($return_goods))
        {
            $this->ajaxReturn(['status'=>0,'msg'=>'已经提交过退货申请!', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);

            exit;
        }


        $order = Db::name('order')->where('order_id',$id)
            ->field('order_id,order_sn,user_id,store_id,seller_id,order_status,pay_status,shipping_status')
            ->find();//订单信息
        $rec = Db::name('order_goods')->where('order_id',$id)->field('rec_id,goods_id,goods_num')->find();//订单商品表

        $data = array_merge($order,$rec);
        $data['reason'] = '商家后台操作退货退款';
        $data['type'] = I('type');
        $data['status'] = I('status');
        $data['refund_mark'] = I('note');
        $data['addtime'] = time();
        $data['is_receive'] = 1;//因为只有确认收货的才能换货。所以这里直接写好
        $data['type'] = 1;//后台这样的逻辑，看来也可以出现在退款里面。金钱的力量果然强大。
        $note ="退换货:, 状态:,处理备注：商家后台主动操作退货/款";
        $result = M('return_goods')->add($data);
        if($result)
        {
            $type = empty($data['type']) ? 2 : 3;
            $where = " order_id = ".$data['order_id']." and goods_id=".$data['goods_id'];
            M('order_goods')->where($where)->save(array('is_send'=>$type));//更改商品状态
            $orderLogic = new OrderLogic();

            $log = $orderLogic->orderActionLog($order,'退换货',$note,SELLER_ID);
            $this->ajaxReturn(['status'=>1,'msg'=>'修改成功!', 'url'=>U('Seller/Order/detail',['order_id'=>$id])]);

            exit;
        }

    }

    /**
     * 订单打印
     * @return mixed
     */
    public function order_print()
    {        
        $ids=I('get.ids');
        $ids=trim($ids,',');
        $ids=explode(',', $ids);
        if(!is_numeric($ids[0])){
            unset($ids[0]);
        }

        $orderLogic = new OrderLogic();

        $orders=array();
        foreach ($ids as $k => $v) {
            $orders[] = $orderLogic->getOrderInfo($v);
        }

//        foreach ($orders as $k => $v) {
//            if($v['store_id'] != STORE_ID){
//                unset($orders[$k]);//删掉不存在订单
//            }
//        }

        if(!$orders){
           $this->error('所选订单不存在', U('Seller/Order/index')); 
        }

        foreach ($orders as $k => $v) {
            $orders[$k]['province'] = getRegionName($v['province']);
            $orders[$k]['city'] = getRegionName($v['city']);
            $orders[$k]['district'] = getRegionName($v['district']);
            $orders[$k]['full_address'] = $orders[$k]['province'] . ' ' . $orders[$k]['city'] . ' ' . $orders[$k]['district'] . ' ' . $orders[$k]['address'];
        }

        foreach ($orders as $k => $v) {
            $orders[$k]['orderGoods'] = $orderLogic->getOrderGoods($v['order_id']);
        }
        
        foreach ($orders as $k => $v){
            $orders[$k]['order_num_arr'] = get_arr_column($v['orderGoods'],'goods_num');
        }


        foreach ($orders as $k => $v){
            if($v['order_num_arr']){
                $orders[$k]['goodsCount'] = array_sum($v['order_num_arr']);
            }
        }
        $DeliveryDoc = new DeliveryDoc();
        $deliveryDoc = $DeliveryDoc->where('order_id', 'in', $ids)->select();
        $shop = tpCache('shop_info');
        $this->assign('orders', $orders);
        $this->assign('shop', $shop);
        $this->assign('deliveryDoc', $deliveryDoc);
        $template = I('template', 'picking');
        return $this->fetch($template);
    }

    /**
     * 订单批量打印
     * @return mixed
     */
    public function order_print_pl()
    {        
        
        $ids=I('get.ids');
        $ids=trim($ids,',');
        $ids=explode(',', $ids);
        if(!is_numeric($ids[0])){
            unset($ids[0]);
        }

        $orderLogic = new OrderLogic();

        $orders=array();
        foreach ($ids as $k => $v) {
            $orders[] = $orderLogic->getOrderInfo($v);
        }

        foreach ($orders as $k => $v) {
            if($v['store_id'] != STORE_ID){
                unset($orders[$k]);//删掉不存在订单
            }
        }

        if(!$orders){
           $this->error('所选订单不存在', U('Seller/Order/index')); 
        }

        foreach ($orders as $k => $v) {
            $orders[$k]['province'] = getRegionName($v['province']);
            $orders[$k]['city'] = getRegionName($v['city']);
            $orders[$k]['district'] = getRegionName($v['district']);
            $orders[$k]['full_address'] = $orders[$k]['province'] . ' ' . $orders[$k]['city'] . ' ' . $orders[$k]['district'] . ' ' . $orders[$k]['address'];
        }

        foreach ($orders as $k => $v) {
            $orders[$k]['orderGoods'] = $orderLogic->getOrderGoods($v['order_id']);
        }
        
        foreach ($orders as $k => $v){
            $orders[$k]['order_num_arr'] = get_arr_column($v['orderGoods'],'goods_num');
        }
        
/*        if($order_num_arr){
            $goodsCount = array_sum($order_num_arr);
            $this->assign('goods_count', $goodsCount);
        }*/


        foreach ($orders as $k => $v){
            if($v['order_num_arr']){
                $orders[$k]['goodsCount'] = array_sum($v['order_num_arr']);
            }
        }

        $shop = tpCache('shop_info');
        $this->assign('orders', $orders);
        $this->assign('shop', $shop);
        $template = I('template', 'picking');
        return $this->fetch($template);
        
    }    
    
    /**
     * 快递单打印
     */
    public function shipping_print($id='')
    {
        $order_id=$id?$id:I('get.order_id/d');

        $orderLogic = new OrderLogic();
        $order = $orderLogic->getOrderInfo($order_id);
        if (in_array($order['store_id'],STORES)) {
            $this->error('该订单不存在', U('seller/Order/index'));
        }
        //查询是否存在订单及物流
        $shipping = Db::name('shipping')->where(['shipping_code'=>$order['shipping_code']])->find();
        if (!$shipping) {
            $this->error('物流插件不存在');
        }
        if (empty($shipping['template_html'])) {
            $this->error('请联系平台管理员设置' . $shipping['shipping_name'] . '打印模板');
        }
        $shop = Db::name('store')->field('store_name')->where(['store_id' => STORE_ID])->find();
        $delivery_doc = Db::name('delivery_doc')->where(['order_id'=>$order_id])->find();
        $delivery_doc['province'] = empty($delivery_doc['store_address_province_id']) ? '' : getRegionName($delivery_doc['store_address_province_id']);
        $delivery_doc['city'] = empty($delivery_doc['store_address_city_id']) ? '' : getRegionName($delivery_doc['store_address_city_id']);
        $delivery_doc['district'] = empty($delivery_doc['store_address_district_id']) ? '' : getRegionName($delivery_doc['store_address_district_id']);

        $order['province'] = getRegionName($order['province']);
        $order['city'] = getRegionName($order['city']);
        $order['district'] = getRegionName($order['district']);
        $template_var = array("发货点-名称", "发货点-联系人", "发货点-电话", "发货点-省份", "发货点-城市",
            "发货点-区县", "发货点-手机", "发货点-详细地址", "收件人-姓名", "收件人-手机", "收件人-电话",
            "收件人-省份", "收件人-城市", "收件人-区县", "收件人-邮编", "收件人-详细地址", "时间-年", "时间-月",
            "时间-日", "时间-当前日期", "订单-订单号", "订单-备注", "订单-配送费用");
        $content_var = array($shop['store_name'], $delivery_doc['store_address_consignee'], $delivery_doc['store_address_mobile'], $delivery_doc['province'], $delivery_doc['city'],
            $delivery_doc['district'], $delivery_doc['store_address_mobile'], $delivery_doc['store_address'], $order['consignee'], $order['mobile'], $order['phone'],
            $order['province'], $order['city'], $order['district'], $order['zipcode'], $order['address'], date('Y'), date('M'),
            date('d'), date('Y-m-d'), $order['order_sn'], $order['admin_note'], $order['shipping_price'],
        );
        $shipping['template_html_replace'] = str_replace($template_var, $content_var, $shipping['template_html']);
        if($id){
            return $shipping;
        }else{
            $shippings[]=$shipping;
            $this->assign('shipping', $shippings);
            return $this->fetch("print_express");
        }
    }

    /*
    *批量打印快递单 
    */
   public function shipping_print_batch(){
        $ids=I('get.ids');
        $ids=substr($ids,0,-1);
        $ids=explode(',', $ids);
        if(!is_numeric($ids[0])){
            unset($ids[0]);
        }
       
        $shippings=array();
        foreach ($ids as $k => $v) {
            $shippings[$k]=$this->shipping_print($v);
        }
        $this->assign('shipping', $shippings);
        return $this->fetch("print_express");
   }

    /**
     * 生成发货单
     */
    public function deliveryHandle()
    {
        $orderLogic = new OrderLogic();
        $data = I('post.');
        $res = $orderLogic->deliveryHandle($data, SELLER_ID);

        

        if ($res['code']) {
            $this->ajaxReturn(['status'=>1,'msg'=>'操作成功', 'url'=>U('Seller/Order/index')]);
        } else {
            $this->ajaxReturn(['status'=>0,'msg'=>'操作失败:'.$res['msg'], 'url'=>U('Order/delivery_info', array('order_id' => $data['order_id']))]);
        }
    }


    public function delivery_info()
    {
        //商家发货
        $order_id = I('order_id/d');
        $orderLogic = new OrderLogic();
        $order = $orderLogic->getOrderInfo($order_id);
        if ($order['seller_id'] != SELLER_ID) {
            $this->error('该订单不存在', U('Seller/Order/index'));
        }
        $return_goods = Db::name('return_goods')->where(array('order_id' => $order_id))->getField('rec_id,goods_num,order_id,status');


        $orderGoods = $orderLogic->getOrderGoods($order_id);



        $DeliveryDoc = new DeliveryDoc();
        $delivery_record = $DeliveryDoc->where(['order_id' => $order_id])->select();
        if ($delivery_record) {
            $order['invoice_no'] = $delivery_record[count($delivery_record) - 1]['invoice_no'];
        }
        $order['unsend'] = $return_goods ?  1 : 0;
        foreach ($orderGoods as $k=>$v){

        	if(!empty($return_goods[$v['rec_id']])){
        	    //如果是退款被拒绝或者用户取消退货，是可以发货的

        	    if(isset($return_goods[$v['rec_id']]['status'])&&$return_goods[$v['rec_id']]['status']!=-1&&$return_goods[$v['rec_id']]['status']!=-2){
                    $orderGoods[$k]['unsend'] = 1;
                }

        	}else{
        		$orderGoods[$k]['unsend'] = 0;
        	}
        }
//        die;

        $shipping_list = Db::name('seller_shipping')->field('shipping_name,shipping_code')
            ->alias('ss')->join('__SHIPPING__ s','s.shipping_id = ss.shipping_id','left')->where('ss.seller_id',SELLER_ID)->select();
        $SellerAddress = new SellerAddress();
        $deliver_address = $SellerAddress->where(['seller_id'=>SELLER_ID,'type'=>0])->order('is_default desc')->select();
        $order_extend = Db::name('order_extend')->where(['seller_Id' => SELLER_ID, 'order_id' => $order_id])->find();
        $this->assign('shipping_list',$shipping_list);
        $this->assign('deliver_address',$deliver_address);//发货地址
        $this->assign('order', $order);
        $this->assign('order_extend', $order_extend);
        $this->assign('orderGoods', $orderGoods);
        $this->assign('delivery_record', $delivery_record);//发货记录
        return $this->fetch();
    }

    //批量发货页面
    public function send_show(){
        
        $ids=I('get.ids');
        $ids=substr($ids,0,-1);
        $ids=explode(',', $ids);
        if(!is_numeric($ids[0])){
            unset($ids[0]);
        }

        $orderLogic = new OrderLogic();
        $order=array();
        foreach ($ids as $k => $v) {
            $order[$k]=$orderLogic->getOrderInfo($v);
            $order[$k]['orderGoods']=$orderLogic->getOrderGoods($v);
        }
        $shipping_list = Db::name('store_shipping')->field('shipping_name,shipping_code')
            ->alias('ss')->join('__SHIPPING__ s','s.shipping_id = ss.shipping_id','left')->where('ss.store_id',STORE_ID)->select();


        $SellerAddress = new SellerAddress();
        $deliver_address = $SellerAddress->where(['seller_id'=>SELLER_ID,'type'=>0])->order('is_default desc')->select();
        $this->assign('shipping_list',$shipping_list);
        $this->assign('deliver_address',$deliver_address);
        $this->assign('order',$order);
        $this->assign('num',count($order));
        return $this->fetch();
        
    }

    /*
    *批量发货处理
    */
    public function send_batch(){
        $data=I('post.');
        $param=$data['order'];
        //参数拼装
        foreach ($param as $k => $v) {
            $param[$k]['shipping_name']=$data['shipping_name'];
            $param[$k]['shipping_code']=$data['shipping_code'];
            $param[$k]['note']='';//好像是必填项 没有此字段会数据库报错 先赋个空值
        }
        
        $orderLogic = new OrderLogic();
        foreach ($param as $k => $v) {
            $res=$orderLogic->deliveryHandle($v, STORE_ID);
            if($res === false){
                $this->ajaxReturn(['status'=>0,'msg'=>'操作失败']);
            }
        }
        $this->ajaxReturn(['status'=>1,'msg'=>'操作成功']);
    }

    /**
     * 发货单列表
     */
    public function delivery_list()
    {
        $this->assign('begin', date('Y-m-d', strtotime("-3 month")+86400));
        $this->assign('end', date('Y-m-d', strtotime('+1 days')));
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        return $this->fetch();
    }
    
    /**
     * 订单操作
     */
    public function order_action()
    {
        $orderLogic = new OrderLogic();
        $type = I('get.type');
        $order_id = I('get.order_id/d');
        if ($type && $order_id) {
        	$order = $orderLogic->getOrderInfo($order_id);
            $button = $orderLogic->getOrderButton($order);
        	if($order){
        		$a = $orderLogic->orderProcessHandle($order_id, $type);
        		$seller_id = session('seller_id');
                $action = '';
                if(in_array($type,array_keys($button))){
                    $action = $button[$type];
                }
        		$res = $orderLogic->orderActionLog($order, $action, I('note'), $seller_id, 1);
        		if ($res && $a) {
        			exit(json_encode(array('status' => 1, 'msg' => '操作成功')));
        		} else {
        			exit(json_encode(array('status' => 0, 'msg' => '操作失败')));
        		}
        	}else{
        		exit(json_encode(array('status' => 0, 'msg' => '非法操作')));
        	}
        } else {
            $this->error('参数错误', U('Seller/Order/detail', array('order_id' => $order_id)));
        }
    }

    public function order_log()
    {
        $condition = array();
        $log = Db::name('order_action');
        $admin_id = I('admin_id/d');
        if ($admin_id > 0) {
            $condition['action_user'] = $admin_id;
        }
        $condition['store_id'] = STORE_ID;
        $count = $log->where($condition)->count();
        $Page = new Page($count, 20);
        foreach ($condition as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show();
        $list = $log->where($condition)->order('action_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $this->assign('list', $list);
        $this->assign('page', $show);
        $seller = Db::name('seller')->getField('seller_id,seller_name');
        $this->assign('admin', $seller);
        return $this->fetch();
    }


    /**
     * 订单信息导出
     * 表头 订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_recycle_order(){

        set_time_limit(0);

        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;

        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1

        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

        $condition['prom_type'] = array('eq',7);//只显示爱回收

        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
        I('order_status') != '' ?  : false;

        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }

        }



        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单

        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');

//        $condition['prom_type'] = array('lt',5);


//        $count = Db::name('order'.$select_year)->where($condition)->count();


//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }


        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
        $order_id_arr = I('order_id_arr',null);
        if($order_id_arr&&$order_id_arr!=''){
            $condition = [];
            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
        }




        $region = Db::name('region')->cache(true)->getField('id,name');



//        var_dump($where);die;

        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order'.$select_year)->where($condition)->order('order_id')->select();


        $user_id_arr = [];
        foreach ($orderList as $order){
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id','in',$user_id_arr)->field('user_id,email')->select();
        foreach ($orderList as &$order){
            foreach ($userList as $user){
                if($order['user_id']==$user['user_id']){
                    $order['email'] = $user['email'];
                }
            }
        }


        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
//        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();


        $order_id_arr = get_arr_column($orderList,'order_id');


        //还需要跟踪赠送优惠券是否使用了
        $isHasSendCouponList = Db::name('CouponList')->where('get_order_id','in',$order_id_arr)->select();
        if($isHasSendCouponList){

            foreach ($orderList as &$order){
                $order['isSendCouponUse'] = false;
                $order['couponListOrderSn'] = null;
                foreach ($isHasSendCouponList as $couponList){
                    if($couponList['get_order_id']==$order['order_id']&&$couponList['order_id']&&$couponList['status']==1){
                        $order['isSendCouponUse'] = true;
                        $couponListOrderSn= Db::name('order')->where('order_id',$couponList['order_id'])->value('order_sn');
                        $order['couponListOrderSn'] = ''.$couponListOrderSn;
                    }
                }
            }

        }


        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');



        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">下单时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">支付时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品名称 </td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品属性</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品数量</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">SKU名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">单价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">补贴金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">预约时间</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">快递公司</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;" >物流单号</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货联系方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货地址</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货时间</td>';



        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">索要类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票抬头</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">优惠券是否使用</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*" >成交订单号</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';


        $strTable .= '</tr>';


        $cat3_arr = Db::name('GoodsCategory')->column('id,name');



        foreach ($orderList as $k => $val) {

            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {

                if ($store['store_id'] === $val['store_id']) {

                    $tempStore = $store;

                }
            }

//            foreach ($sellers as $seller) {
//
//                if ($seller['seller_id'] === $val['seller_id']) {
//
//                    $tempSeller = $seller;
//
//                }
//            }

            $orderGoods = Db::name('OrderGoods')
                ->alias('og')
                ->join('tp_goods g', 'g.goods_id=og.goods_id','LEFT')
                ->where('order_id', $val['order_id'])
                ->field('og.*,g.cat_id3')
                ->select();

            $row = count($orderGoods);





            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价

//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();

            foreach ($orderGoods as $key => $goods) {


                $strGoods = "";

                $strTable .= '<tr>';

                $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
                $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] .  ' </td>';//店铺名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_id']. ' </td>';//店铺id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:m:s', $val['create_time']) . ' </td>';//日期

                $pay_time = '';
                if($val['pay_time']){
                    $pay_time = date('Y-m-d H:m:s', $val['pay_time']);
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' .  $pay_time . ' </td>';//支付时间

                if(!isEmptyObject($val['master_order_sn'])){
                    $order_type ='预约转化';
                }else{
                    if(!empty($val['status'])){
                        $order_type = '回收订单';
                    }else{
                        $order_type = '直接购买';
                    }
                }

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $order_type . ' </td>';//订单类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['status_tip'] . ' </td>';//订单状态




                if (isset($cat3_arr[$goods['cat_id3']])) {
                    $cat3_name = $cat3_arr[$goods['cat_id3']];
                } else {
                    $cat3_name = '';
                }
                if(!isEmptyObject($val['recycle_info'])){
                    $val['recycle_info'] = json_decode($val['recycle_info'],true);
                }else{
                    $val['recycle_info'] = [];
                }
                
                $amount = !empty($val['recycle_info'][2]['amount']) ? $val['recycle_info'][2]['amount'] : 0;
                
                
//                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'] . ' 商品类别:' . $cat3_name . ' ';
//                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
//                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $cat3_name . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['spec_key_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_num'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['sku'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['final_price'] . ' </td>';


                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['paid_money'] . ' </td>';//实付金额，可能有优惠什么的
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $amount . ' </td>';//优惠金额
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式


                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] . '</td>';//用户账号,邮箱




                /**
                 * 物流信息
                 * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
                 */

                $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
//



                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话

                $region = Db::name('region')->where('id', 'IN', [$val['province'], $val['city'], $val['district']])->column('name');
                $sendFullAddress = implode('', $region) . $val['address'];

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sendFullAddress . ' </td>';//收货地址


                if($val['shipping_id']==3){
                    $shipping_info = json_decode($val['shipping_info'],true);
                    $ziti_info= json_decode($val['ziti_info'],true);

                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_name'] . '</td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_mobile'] . '</td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['plan_time'] . '</td>';//取件时间
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件时间
                }


//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['shipping_name'] . '</td>';//快递公司

                $strTable .= '<td style="text-align:left;font-size:12px;">&nbsp;' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的


                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['seller_address_province_id'], $deliver_doc['seller_address_city_id'], $deliver_doc['seller_address_district_id']])->column('name');
                $sellerFullAddress = implode('', $region) . $deliver_doc['seller_address'];

                $depot = '联系人:'.$deliver_doc['seller_address_consignee'].'<br/>'
                    .'联系电话:'.$deliver_doc['seller_address_mobile'].'<br/>'.
                    '详细地址:'.$sellerFullAddress;



                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_consignee'] . '</td>';//发货仓库
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_mobile'] . '</td>';//发货仓库
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sellerFullAddress . '</td>';//发货仓库

                $send_time = !empty($deliver_doc['create_time']) ? date('y-m-d H:m:s',$deliver_doc['create_time']) : '';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $send_time . '</td>';//发货时间


                /**
                 * 发票信息
                 *
                 */
                $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();


                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_TYPE[$val['invoice_type']] . '</td>';//发票类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_BLAG_WAY[$invoice['blag_way']] . '</td>';//索要类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['invoice_title'] . '</td>';//发票抬头


                $isSendCouponUse = $val['isSendCouponUse']?'是':'否';

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $isSendCouponUse . ' </td>';//优惠券是否使用
                $strTable .= '<td style="text-align:left;font-size:12px;">&nbsp;' . (string)$val['couponListOrderSn'] . ' </td>';//成交订单号

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言


                $strTable .= '</tr>';

            }
//            unset($orderGoods);



        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '经销商:回收订单');
        exit();
    }

    /**
     * 订单信息导出
     * 表头 订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_order()
    {
        
        set_time_limit(0);
        
        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;

        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1

        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
        I('order_status') != '' ?  : false;

        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }

        }



        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单

        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');

//        $condition['prom_type'] = array('lt',5);


//        $count = Db::name('order'.$select_year)->where($condition)->count();


//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }


        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
        $order_id_arr = I('order_id_arr',null);
        if($order_id_arr&&$order_id_arr!=''){
            $condition = [];
            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
        }




        $region = Db::name('region')->cache(true)->getField('id,name');



//        var_dump($where);die;

        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order'.$select_year)->where($condition)->order('order_id')->select();


        $user_id_arr = [];
        foreach ($orderList as $order){
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id','in',$user_id_arr)->field('user_id,email')->select();
        foreach ($orderList as &$order){
            foreach ($userList as $user){
                if($order['user_id']==$user['user_id']){
                    $order['email'] = $user['email'];
                }
            }
        }
    
        $order_id_arr = get_arr_column($orderList, 'order_id');
        
        if ($order_id_arr) {
            
            $coupon_list = Db::name('CouponList')
                ->alias('cl')
                ->join('tp_coupon c','c.id=cl.cid')
                ->where('cl.order_id','in',$order_id_arr)->where('cl.status',1)
                ->field('c.type,c.id,cl.order_id')->select();
        
            foreach ($coupon_list as $coupon){
                foreach ($orderList as &$order){
                    if($coupon['order_id']==$order['order_id']){
                        $order['coupon_type'] = $coupon['type'];
                    }
                }
            }
        }
//        echo '<pre>';
//        var_dump($orderList);die;
        


        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
//        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();



        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');



        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">下单时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">支付时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品名称 </td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品属性</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品数量</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">SKU名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">单价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">优惠金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">预约时间</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">快递公司</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;" >物流单号</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货联系方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货地址</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货时间</td>';
    
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">索要类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票抬头</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票税号</td>';
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
        
        
        $strTable .= '</tr>';


        $cat3_arr = Db::name('GoodsCategory')->column('id,name');



        foreach ($orderList as $k => $val) {

            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {

                if ($store['store_id'] === $val['store_id']) {

                    $tempStore = $store;

                }
            }

//            foreach ($sellers as $seller) {
//
//                if ($seller['seller_id'] === $val['seller_id']) {
//
//                    $tempSeller = $seller;
//
//                }
//            }

            $orderGoods = Db::name('OrderGoods')
                ->alias('og')
                ->join('tp_goods g', 'g.goods_id=og.goods_id')
                ->where('order_id', $val['order_id'])
                ->field('og.*,g.cat_id3')
                ->select();
    
    
            //20190214-20190331 买东西送耳机
            $tmpls = [1224,1223,1222,1216,1215,1214];
            $s = 1549814400;
            $e = 1553961599;
            
            if($orderGoods[0]['create_time']>$s && $orderGoods[0]['create_time']<$e){
    
                $goods_id_arr = get_arr_column($orderGoods,'goods_id');
                $isHasTmplActiveGoods = Db::name('goods')->where('goods_id','in',$goods_id_arr)->where('tmpl_id','in',$tmpls)->find();//只要有商品tmpl_id满足的，就这么弄。
                if($isHasTmplActiveGoods){
                    
                    $orderGoods[] = [
                        'spec_key_name'=>null,
                        'sku'=>null,
                        'goods_name'=>'Beats Solo 3 Wireless',
                        'goods_num'=>1,
                        'final_price'=>'0.00',
                    ];
                }
            }
            

            $row = count($orderGoods);
            
            





            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价

//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();

            foreach ($orderGoods as $key => $goods) {
    
                $strGoods = "";
    
                $strTable .= '<tr>';
    
                $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
                $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] .  ' </td>';//店铺名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_id']. ' </td>';//店铺id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . ' </td>';//日期
                
                $pay_time = '';
                if($val['pay_time']){
                    $pay_time = date('Y-m-d H:i:s', $val['pay_time']);
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' .  $pay_time . ' </td>';//支付时间
    
                if(!isEmptyObject($val['master_order_sn'])){
                    $order_type ='预约转化';
                }else{
                    $order_type = '直接购买';
                }
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $order_type . ' </td>';//订单类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态
    
    
    
    
                if (isset($cat3_arr[$goods['cat_id3']])) {
                    $cat3_name = $cat3_arr[$goods['cat_id3']];
                } else {
                    $cat3_name = '';
                }

//                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'] . ' 商品类别:' . $cat3_name . ' ';
//                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
//                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $cat3_name . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['spec_key_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_num'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['sku'] . ' </td>';
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_price'] . ' </td>';
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . format_price($goods['goods_price']*$goods['goods_num']) . ' </td>';//应付金额
                $strTable .= '<td style="text-align:left;font-size:12px;">' . format_price($goods['final_price']*$goods['goods_num']) . ' </td>';//实付金额，可能有优惠什么的
    
                //$strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
                //$strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['final_price'] . ' </td>';
//
//
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['paid_money'] . ' </td>';//实付金额，可能有优惠什么的
                
                
                
                if($val['coupon_price']>0){
                    if($val['coupon_type']==4){
                        $val['coupon_price'] = '注册券金额'.$val['coupon_price'];
                    }elseif($val['coupon_type']==5){
                        $val['coupon_price'] = '回收券金额'.$val['coupon_price'];
                    }else{
                        $val['coupon_price'] = '优惠'.$val['coupon_price'];
                    }
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式
    
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] . '</td>';//用户账号,邮箱
    
    
    
    
                /**
                 * 物流信息
                 * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
                 */
    
                $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
//
                
                
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
    
                $region = Db::name('region')->where('id', 'IN', [$val['province'], $val['city'], $val['district']])->column('name');
                $sendFullAddress = implode('', $region) . $val['address'];
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sendFullAddress . ' </td>';//收货地址
    
    
                if($val['shipping_id']==3){
                    $shipping_info = json_decode($val['shipping_info'],true);
                    $ziti_info= json_decode($val['ziti_info'],true);
        
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_name'] . '</td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_mobile'] . '</td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['plan_time'] . '</td>';//取件时间
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件时间
                }
                

//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['shipping_name'] . '</td>';//快递公司
    
                $strTable .= '<td style="text-align:left;font-size:12px;">&nbsp;' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的
    
    
                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['seller_address_province_id'], $deliver_doc['seller_address_city_id'], $deliver_doc['seller_address_district_id']])->column('name');
                $sellerFullAddress = implode('', $region) . $deliver_doc['seller_address'];
    
                $depot = '联系人:'.$deliver_doc['seller_address_consignee'].'<br/>'
                    .'联系电话:'.$deliver_doc['seller_address_mobile'].'<br/>'.
                    '详细地址:'.$sellerFullAddress;
                
                
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_consignee'] . '</td>';//发货仓库
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_mobile'] . '</td>';//发货仓库
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sellerFullAddress . '</td>';//发货仓库
                
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s',$deliver_doc['create_time']) . '</td>';//发货时间
    
    
                /**
                 * 发票信息
                 *
                 */
                $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();
    
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_TYPE[$val['invoice_type']] . '</td>';//发票类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_BLAG_WAY[$invoice['blag_way']] . '</td>';//索要类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['invoice_title'] . '</td>';//发票抬头

                $strTable .= '<td style="text-align:left;font-size:12px;vnd.ms-excel.numberformat:@;">' . $val['taxpayer'] . '</td>';//发票税号


                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言
    
    
                $strTable .= '</tr>';

            }
//            unset($orderGoods);



        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '经销商:支付交易订单');
        exit();
    }
    
    /**
     * 订单信息导出
     * 表头 订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_order_old()
    {
        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;
    
        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;
    
        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1
    
        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }
    
        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
        I('order_status') != '' ?  : false;
    
        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }
        
        }
    
    
    
        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
    
        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
//        $condition['prom_type'] = array('lt',5);
    
    
//        $count = Db::name('order'.$select_year)->where($condition)->count();
        
        
//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }
    
        
        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
        $order_id_arr = I('order_id_arr',null);
        if($order_id_arr&&$order_id_arr!=''){
            $condition = [];
            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
        }
        
        
        
        
        $region = Db::name('region')->cache(true)->getField('id,name');
        
        
        
//        var_dump($where);die;
    
        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order'.$select_year)->where($condition)->order('order_id')->select();
        
        
        $user_id_arr = [];
        foreach ($orderList as $order){
            $user_id_arr[] = $order['user_id'];
        }
        
        $userList = Db::name('users')->where('user_id','in',$user_id_arr)->field('user_id,email')->select();
        foreach ($orderList as &$order){
            foreach ($userList as $user){
                if($order['user_id']==$user['user_id']){
                    $order['email'] = $user['email'];
                }
            }
        }
        
    
    
        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');
        
        
        
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';
    
//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">日期</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">物流单号</td>';
        $strTable .= '</tr>';
        
        
        foreach ($orderList as $k => $val) {
            
            $tempStore = [];
           
            foreach ($stores as $store){
    
                if($store['store_id']===$val['store_id']){
                   
                    $tempStore = $store;
                   
                }
            }
            
            
            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' .  ' '. '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] . '['.$tempStore['store_id'].']' . ' </td>';//店铺名称
            
            
            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:m:s',$val['create_time']) . ' </td>';//日期
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态
    
    
            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价
            $orderGoods = Db::name('OrderGoods')->where('order_id', $val['order_id'])->select();
//                ->alias('g')
//                ->join('SpecGoodsPrice p','g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $strGoods = "";
            foreach ($orderGoods as $goods) {

                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'];
                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price']. " SKU：" . $goods['sku'];
                $strGoods .= "<br />";
            }
            unset($orderGoods);
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strGoods . ' </td>';
    
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式
            
            /**
             * 发票信息
             *
             */
            $invoice = Db::name('invoice')->where('order_id',$val['order_id'])->find();
            $strInvoice = '';
            
            
            $strInvoice .= "发票类型：" . $INVOICE_TYPE[$val['invoice_type']] . " 索要类型：" . $INVOICE_BLAG_WAY[$invoice['blag_way']];
            $strInvoice .= "<br />";
            $strInvoice .= "抬头：" . $val['invoice_title'] ;
            $strInvoice .= "<br />";
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strInvoice . '</td>';//发票信息
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] . '</td>';//用户账号,邮箱
            
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言
     
    
            /**
             * 物流信息
             * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
             */
            
            $deliver_doc = Db::name('deliveryDoc')->where('order_id',$val['order_id'])->find();
            
            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['address']}" . ' </td>';//收货地址
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的
            
            
            
    
//
//
//
//
//
//
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['consignee']}" . ' </td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['address'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['goods_price'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . '</td>';
//            if($val['pay_name'] == ''){
//                $strTable .= '<td style="text-align:left;font-size:12px;">在线支付</td>';
//            }else{
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';
//            }
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->pay_status[$val['pay_status']] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->shipping_status[$val['shipping_status']] . '</td>';
            
            $strTable .= '</tr>';
        }
        $strTable .= '</table>';
       
        unset($orderList);
        downloadExcel($strTable, 'order');
        exit();
    }

    /**
     * 用于测试使用，旧模板删除，新模板没有套
     * 添加一笔订单
     */
    public function add_order()
    {
        $order = array('store_id' => STORE_ID);
        //  获取省份
        $province = Db::name('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        //  获取订单城市
        $city = Db::name('region')->where(array('parent_id' => $order['province'], 'level' => 2))->select();
        //  获取订单地区
        $area = Db::name('region')->where(array('parent_id' => $order['city'], 'level' => 3))->select();
        //  获取配送方式
        $shipping_list = Db::name('store_shipping')->field('s.shipping_name,s.shipping_code')
            ->alias('ss')->join('__SHIPPING__ s','ss.shipping_id = s.shipping_id')->where('ss.store_id',STORE_ID)->select();
        //  获取支付方式
        $payment_list = Db::name('plugin')->where(array('status' => 1, 'type' => 'payment'))->select();
        if (IS_POST) {
            $order['user_id'] = I('user_id/d');// 用户id 可以为空
            $order['consignee'] = I('consignee');// 收货人
            $order['province'] = I('province'); // 省份
            $order['city'] = I('city'); // 城市
            $order['district'] = I('district'); // 县
            $order['address'] = I('address'); // 收货地址
            $order['mobile'] = I('mobile'); // 手机           
            $order['invoice_title'] = I('invoice_title');// 发票
            $order['admin_note'] = I('admin_note'); // 管理员备注            
            $order['order_sn'] = date('YmdHis') . mt_rand(1000, 9999); // 订单编号;
            $order['admin_note'] = I('admin_note'); // 
            $order['add_time'] = time(); //                    
            $order['shipping_code'] = I('shipping');// 物流方式
            $order['shipping_name'] = Db::name('shipping')->where(['shipping_code' =>  I('shipping')])->getField('shipping_name');
            $order['pay_code'] = I('payment');// 支付方式
            $order['pay_name'] = Db::name('plugin')->where(array('status' => 1, 'type' => 'payment', 'code' => I('payment')))->getField('name');

            $goods_id_arr = I("goods_id/d");
            $orderLogic = new OrderLogic();
            $order_goods = $orderLogic->get_spec_goods($goods_id_arr);
            $pay = new Pay();
            try{
                $pay->setUserId($order['user_id']);
                $pay->payOrder($order_goods);
                $pay->delivery($order['district']);
            }catch (TpshopException $t){
                $error = $t->getErrorArr();
                $this->error($error['msg']);
            }
            $order['goods_price'] = $pay->getGoodsPrice(); // 商品总价
            $order['shipping_price'] = $pay->getShippingPrice(); //物流费
            $order['order_amount'] = $pay->getOrderAmount(); // 应付金额
            $order['total_amount'] = $pay->getTotalAmount(); // 订单总价

            // 添加订单
            $order_id = Db::name('order')->add($order);
            if ($order_id) {
                foreach ($order_goods as $key => $val) {
                    $val['order_id'] = $order_id;
                    $val['store_id'] = STORE_ID;
                    $rec_id = Db::name('order_goods')->add($val);
                    if (!$rec_id)
                        $this->error('添加失败');
                }
                $this->success('添加商品成功', U("Order/detail", array('order_id' => $order_id)));
                exit();
            } else {
                $this->error('添加失败');
            }
        }
        $this->assign('shipping_list', $shipping_list);
        $this->assign('payment_list', $payment_list);
        $this->assign('province', $province);
        $this->assign('city', $city);
        $this->assign('area', $area);
        return $this->fetch();
    }

    /**
     * 选择搜索商品
     */
    public function search_goods()
    {
        $GoodsLogic = new GoodsLogic;
        $brandList = $GoodsLogic->getSortBrands();
        $categoryList = $GoodsLogic->getSortCategory();
        $where = array('store_id' => STORE_ID, 'is_on_sale' => 1);
        I('intro') && $where[I('intro')] = 1;
        $brand_id = I('brand_id/d');
        $cat_id = I('cat_id/d');
        $keywords = I('keywords');
        $where['is_virtual'] = I('is_virtual/d',0); //默认不查虚拟商品
        if ($cat_id) {
            $goods_category = Db::name('goods_category')->where("id", $cat_id)->find();
            $where['cat_id' . $goods_category['level']] = $cat_id; // 初始化搜索条件
            $this->assign('cat_id', $cat_id);
        }
        if ($brand_id) {
            $this->assign('brand_id', $brand_id);
            $where['brand_id'] = $brand_id;
        }
        if ($keywords) {
            $this->assign('keywords', $keywords);
            $where['goods_name|keywords'] = array('like', '%' . $keywords . '%');
        }
        $count = Db::name('goods')->where($where)->count();
        $Page = new Page($count, 10);
        $goodsList = Db::name('goods')->where($where)->order('goods_id DESC')->limit($Page->firstRow . ',' . $Page->listRows)->select();
        foreach ($goodsList as $key => $val) {
            $spec_goods = Db::name('spec_goods_price')->where("goods_id", $val['goods_id'])->select();
            $goodsList[$key]['spec_goods'] = $spec_goods;
        }
        $store_bind_class = Db::name('store_bind_class')->where("store_id", STORE_ID)->select();
        $cat_id1 = get_arr_column($store_bind_class, 'class_1');
        $cat_id2 = get_arr_column($store_bind_class, 'class_2');
        $cat_id3 = get_arr_column($store_bind_class, 'class_3');
        $cat_id0 = array_merge($cat_id1, $cat_id2, $cat_id3);

        $this->assign('categoryList', $categoryList);
        $this->assign('brandList', $brandList);
        $this->assign('page', $Page->show());//赋值分页输出
        $this->assign('cat_id0', $cat_id0);
        $this->assign('goodsList', $goodsList);
        return $this->fetch();
    }

    public function ajaxOrderNotice()
    {
        $order_amount = Db::name('order')->where(array('order_status' => 0))->count();
        echo $order_amount;
    }

    //虚拟订单临时支付方法，以后要删除
    public function generateVirtualCode(){
        $order_id = I('order_id/d');
        // 获取操作表
        $select_year = getTabByOrderId($order_id);
        $order = Db::name('order'.$select_year)->where(array('order_id'=>$order_id,'store_id'=>STORE_ID))->find();
        $res = update_pay_status($order['order_sn'], 1);
        $this->success('成功生成兑换码', U('Order/virtual_info',['order_id'=>$order_id]), 1);
    }

    /**
     * 拼团订单
     */
    public function team_list()
    {
    	header("Content-type: text/html; charset=utf-8");
exit("请联系TPshop官网客服购买高级版支持此功能");
    }

    /**
     * 拼团订单详情
     * @return mixed
     */
    public function team_info()
    {
    	header("Content-type: text/html; charset=utf-8");
exit("请联系TPshop官网客服购买高级版支持此功能");
    }

    //拼团订单
    public function team_order(){
	header("Content-type: text/html; charset=utf-8");
exit("请联系TPshop官网客服购买高级版支持此功能");
    }
}
