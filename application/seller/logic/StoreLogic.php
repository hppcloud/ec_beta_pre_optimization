<?php
namespace app\seller\logic;

use think\Db;
use think\Exception;
use think\Model;

class StoreLogic extends Model
{
    private $storeInfo=[];

    public function setStoreInfo($storeInfo){
        $this->storeInfo = $storeInfo;
    }


    /**
     * @param $store_id 需要修改的店铺id
     * @param $new_seller_id //新的经销商id
     */
    public static function transferStore_test($store_id, $new_seller_id, $old_seller_id)
    {

        if (!$store_id || !$new_seller_id || !$old_seller_id) {
            return ['code' => 0, 'msg' => 'store_id和new_seller_id和old_seller_id均不能为空', 'data' => null];
        }


        $store = Db::name('store')->where('store_id', $store_id)->find();
        if ($store['seller_id'] != $old_seller_id) {
            return ['code' => 0, 'msg' => 'old_seller_id:' . $old_seller_id . '和店铺不匹配store_id:' . $store_id, 'data' => null];
        }

        $new_seller = Db::name('seller')->where('seller_id', $new_seller_id)->find();

        if (!$new_seller) {
            return ['code' => 0, 'msg' => 'new_seller_id:' . $new_seller_id . '经销商不存在', 'data' => null];
        }

        $old_seller = Db::name('seller')->where('seller_id', $old_seller_id)->find();
        if (!$old_seller) {
            return ['code' => 0, 'msg' => 'old_seller_id:' . $old_seller_id . '经销商不存在', 'data' => null];
        }


        Db::startTrans();
        try {


            $table_name_arr = ['store', 'coupon', 'coupon_store', 'delivery_doc', 'goods', 'goods_coupon', 'invoice', 'order', 'order_action', 'order_extend', 'return_goods'];

            $success = 0;
            $len = count($table_name_arr);

            foreach ($table_name_arr as $table_name) {
                $rt = Db::name($table_name)->where('store_id', $store_id)->update(['seller_id' => $new_seller_id]);
                if ($rt !== false) {
                    $success++;
                } else {
                    Db::rollback();
                    return ['code' => 0, 'msg' => $table_name . '表的数据替换失败', 'data' => null];
                    break;
                }
            }

            //1.修改store表里面的seller_id字段

            Db::name('store')->where('store_id', $store_id)->update(['seller_name' => $new_seller['seller_name']]);


// =============================================================================
//            //2.相关的seller_id冗余字段替换
//
//            //2.1 ad表
//            Db::name('ad')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.2coupon优惠券表
//            Db::name('coupon')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.3coupon_store 优惠券店铺关联表
//            Db::name('coupon_store')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.4delivery_doc 发货信息表
//            Db::name('delivery_doc')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.5goods 商品表
//            Db::name('goods')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.6goods_coupon 商品优惠券表
//            Db::name('goods_coupon')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.7invoice 发票表
//            Db::name('invoice')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.8order 订单表
//            Db::name('order')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.9order_action 订单操作记录
//            Db::name('order_action')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.10 order_extend 订单配送
//            Db::name('order_extend')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.11 return_goods 退货表
//            Db::name('return_goods')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//  ======================================================================================================


//            //2.12seller_log 日志表
            Db::name('seller_log')->where('log_store_id', $store_id)->update(['log_seller_id' => $new_seller_id]);

            //ad表里面存的是店铺id的合集，所以更麻烦
            //加两个逗号确保完全是一样的
            $isHasStoreAds = Db::name('ad')->where('store_id_list', 'like', '%,' . $store_id . ',%')->select();

            foreach ($isHasStoreAds as $ad) {
                trace('迁移店铺' . $store_id . '涉及广告修改' . json_encode($ad));
//                echo '迁移店铺' . $store_id . '涉及广告修改ad_id' . $ad['ad_id'];
                $store_id_arr = explode(',', $ad['store_id_list']);
                $new_store_idarr = [];
                foreach ($store_id_arr as &$tempId) {
                    if ($tempId != $store_id) {
                        $new_store_idarr[] = $tempId;
                    }
                }
                Db::name('ad')->where('ad_id', $ad['ad_id'])->update(['store_id_list' => implode(',', $new_store_idarr)]);//更新
            }


            //处理商品规格
            //->where('goods_id',7066)
            $allGoodsList = Db::name('goods')->where('store_id', $store_id)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->select();

            echo '开始修改商品，共有' . count($allGoodsList) . '个';
            foreach ($allGoodsList as $key => $goods) {
                echo '更新商品===============' . $goods['id'];
                $skuList = Db::name('SpecGoodsPrice')->where('goods_id', $goods['goods_id'])->field('item_id,key,key_name')->select();

                foreach ($skuList as $sku) {


                    $spec_item_arr = explode('_', $sku['key']);


//                $require_change_key = false;
                    $new_item_id_arr = [];
                    $new_item_name_arr = [];
                    foreach ($spec_item_arr as $spec_item_id) {
                        $old_spec_item = Db::name('SpecItem')->where('id', $spec_item_id)->find();

                        //如果这个规格在原有经销商名下也没有，那就报错
                        if (!$old_spec_item) {
                            echo '处理失败:这个规格再原来的经销商居然都不存在goods_id:' . $goods['goods_id'] . 'spec_item_id:' . $spec_item_id;
                            Db::rollback();
                            return ['code' => 0, 'msg' => '处理失败:这个规格再原来的经销商居然都不存在goods_id:' . $goods['goods_id'] . 'spec_item_id:' . $spec_item_id, 'data' => null];
                        }
                        $isHasSpecItem = Db::name('SpecItem')->where(['seller_id' => $new_seller_id, 'spec_id' => $old_spec_item['spec_id'], 'item' => $old_spec_item['item']])->find();
                        //如果新的经销商不存在这个记录，就新增一个


                        $tempSpecItemId = null;//记录老的数据要替换新的
                        if ($isHasSpecItem) {


                            $new_item_id_arr[] = $isHasSpecItem['id'];
                            $new_item_name_arr[] = $isHasSpecItem['item'];

                            //修复一下商品颜色
                            Db::name('SpecColor')->where(['spec_color_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->update(['spec_color_id' => $isHasSpecItem['id']]);
                            Db::name('SpecImage')->where(['spec_image_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->update(['spec_image_id' => $isHasSpecItem['id']]);


//                            echo '新的经销商spec_item有记录,所以不用新增:' . $isHasSpecItem['id'] . '<br/>';

                        } else {

                            //用原来的做模板

                            $data = ['seller_id' => $new_seller_id, 'spec_id' => $old_spec_item['spec_id'], 'item' => $old_spec_item['item']];

                            $new_spec_item_id = Db::name('SpecItem')->insertGetId($data);
//                            echo '新的经销商spec_item没有记录,所以新增:' . $new_spec_item_id . '<br/>';
                            $new_item_id_arr[] = $new_spec_item_id;
                            $new_item_name_arr[] = $isHasSpecItem['item'];

                            //还要检查同商品的颜色、图片
                            $isHasSpecColor = Db::name('SpecColor')->where(['spec_color_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->find();
                            if ($isHasSpecColor) {
                                Db::name('SpecColor')->where(['spec_color_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->update(['spec_color_id' => $new_spec_item_id]);
                            } else {
                                Db::name('SpecColor')->insertGetId(['spec_color_id' => $new_spec_item_id, 'goods_id' => $goods['goods_id'], 'spec_color' => $isHasSpecColor['color']]);
                            }

                            //有就切换没有就新增
                            $isHasSpecImage = Db::name('SpecImage')->where(['spec_image_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->find();


                            if ($isHasSpecImage) {
                                Db::name('SpecImage')->where(['spec_image_id' => $spec_item_id, 'goods_id' => $goods['goods_id']])->update(['spec_image_id' => $new_spec_item_id]);
                            } else {
                                Db::name('SpecImage')->insertGetId(['spec_image_id' => $new_spec_item_id, 'goods_id' => $goods['goods_id'], 'src' => $isHasSpecImage['src']]);
                            }


                        }


                    }
                    sort($new_item_id_arr);


                    //算新的
                    $new_key = implode('_', $new_item_id_arr);
                    $new_key_name = implode(' ', $new_item_name_arr);
//                    echo $sku['key'] . '==========================================' . $new_key . '<br/>';
                    //如果新旧不一样的话
                    if ($sku['key'] != $new_key) {
                        $rt = Db::name('SpecGoodsPrice')->where(['goods_id' => $goods['goods_id'], 'item_id' => $sku['item_id']])->update(['key' => $new_key, 'key_name' => $new_key_name]);


                        //订单购买表
                        $rt1 = Db::name('OrderGoods')->where('spec_key', $sku['key'])->where('goods_id', $sku['goods_id'])->update(['spec_key' => $new_key, 'spec_key_name' => $new_key_name]);
                        //退货表
//                    $rt2 = Db::name('ReturnGoods')->where('rch_id',$sku['item_id'])->update(['rch_id'=>$spec_price_2['item_id']]);

                        //退货表
                        $rt3 = Db::name('Cart')->where('spec_key', $sku['key'])->update(['spec_key' => $new_key]);


                        if ($rt !== false && $rt1 !== false && $rt3 !== false) {
                            echo '处理成功:' . json_encode($sku) . '<br/>';
                        } else {
                            echo '处理失败:' . json_encode($sku) . '<br/>';
                            Db::rollback();
                            return ['code' => 0, 'msg' => '处理失败:' . json_encode($sku), 'data' => null];
                        }


                    }


                }


            }


            if ($success < $len) {

                Db::rollback();
                return ['code' => 0, 'msg' => '部分操作失败', 'data' => null];
            }
            Db::commit();
            return ['code' => 1, 'msg' => '修改成功', 'data' => null];
        } catch (Exception $e) {
            echo '更换店铺的经销商id错误,store_id:' . $store_id . 'new_seller_id:' . $new_seller_id . '错误原因:' . $e->getMessage(), 'error';
            trace('更换店铺的经销商id错误,store_id:' . $store_id . 'new_seller_id:' . $new_seller_id . '错误原因:' . $e->getMessage(), 'error');


            Db::rollback();
            return ['code' => 0, 'msg' => '错误原因:' . $e->getMessage(), 'data' => null];
        }


    }


    /**
     * @param $store_id 需要修改的店铺id
     * @param $new_seller_id //新的经销商id
     */
    public static function transferStore($store_id, $new_seller_id, $old_seller_id){

        if (!$store_id || !$new_seller_id || !$old_seller_id) {
            return ['code' => 0, 'msg' => 'store_id和new_seller_id和old_seller_id均不能为空', 'data' => null];
        }


        $store = Db::name('store')->where('store_id', $store_id)->find();
        if ($store['seller_id'] != $old_seller_id) {
            return ['code' => 0, 'msg' => 'old_seller_id:' . $old_seller_id . '和店铺不匹配store_id:' . $store_id, 'data' => null];
        }

        $new_seller = Db::name('seller')->where('seller_id', $new_seller_id)->find();

        if (!$new_seller) {
            return ['code' => 0, 'msg' => 'new_seller_id:' . $new_seller_id . '经销商不存在', 'data' => null];
        }

        $old_seller = Db::name('seller')->where('seller_id', $old_seller_id)->find();
        if (!$old_seller) {
            return ['code' => 0, 'msg' => 'old_seller_id:' . $old_seller_id . '经销商不存在', 'data' => null];
        }


        Db::startTrans();
        try{


            $table_name_arr = ['store', 'coupon', 'coupon_store', 'delivery_doc', 'goods', 'goods_coupon', 'invoice', 'order', 'order_action', 'order_extend', 'return_goods'];

            $success = 0;
            $len = count($table_name_arr);

            foreach ($table_name_arr as $table_name) {
                $rt = Db::name($table_name)->where('store_id', $store_id)->update(['seller_id' => $new_seller_id]);
                if ($rt !== false) {
                    $success++;
                } else {
                    Db::rollback();
                    return ['code' => 0, 'msg' => $table_name . '表的数据替换失败', 'data' => null];
                    break;
                }
            }

            //1.修改store表里面的seller_id字段

            Db::name('store')->where('store_id', $store_id)->update(['seller_name' => $new_seller['seller_name']]);


// =============================================================================
//            //2.相关的seller_id冗余字段替换
//
//            //2.1 ad表
//            Db::name('ad')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.2coupon优惠券表
//            Db::name('coupon')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.3coupon_store 优惠券店铺关联表
//            Db::name('coupon_store')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.4delivery_doc 发货信息表
//            Db::name('delivery_doc')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.5goods 商品表
//            Db::name('goods')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.6goods_coupon 商品优惠券表
//            Db::name('goods_coupon')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.7invoice 发票表
//            Db::name('invoice')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.8order 订单表
//            Db::name('order')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.9order_action 订单操作记录
//            Db::name('order_action')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.10 order_extend 订单配送
//            Db::name('order_extend')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//            //2.11 return_goods 退货表
//            Db::name('return_goods')->where('store_id',$store_id)->update(['seller_id'=>$new_seller_id]);
//  ======================================================================================================



//            //2.12seller_log 日志表
            Db::name('seller_log')->where('log_store_id', $store_id)->update(['log_seller_id' => $new_seller_id]);

            //ad表里面存的是店铺id的合集，所以更麻烦
            //加两个逗号确保完全是一样的
            $isHasStoreAds = Db::name('ad')->where('store_id_list', 'like', '%,' . $store_id . ',%')->select();

            foreach ($isHasStoreAds as $ad) {
                trace('迁移店铺' . $store_id . '涉及广告修改' . json_encode($ad));
//                echo '迁移店铺' . $store_id . '涉及广告修改ad_id' . $ad['ad_id'];
                $store_id_arr = explode(',', $ad['store_id_list']);
                $new_store_idarr = [];
                foreach ($store_id_arr as &$tempId) {
                    if ($tempId != $store_id) {
                        $new_store_idarr[] = $tempId;
                    }
                }
                Db::name('ad')->where('ad_id', $ad['ad_id'])->update(['store_id_list' => implode(',', $new_store_idarr)]);//更新
            }


            //处理商品规格
            //->where('goods_id',7066)
            $allGoodsList = Db::name('goods')->where('store_id', $store_id)->field('goods_id,on_time,goods_name,store_id,seller_id,cat_id3')->select();

//            echo '开始修改商品，共有'.count($allGoodsList).'个';
            foreach ($allGoodsList as $key => $goods) {
//                echo '更新商品==============='.$goods['id'];
                $skuList = Db::name('SpecGoodsPrice')->where('goods_id', $goods['goods_id'])->field('item_id,key,key_name')->select();

                foreach ($skuList as $sku) {


                    $spec_item_arr = explode('_', $sku['key']);


//                $require_change_key = false;
                    $new_item_id_arr = [];
                    $new_item_name_arr = [];
                    foreach ($spec_item_arr as $spec_item_id) {
                        $old_spec_item = Db::name('SpecItem')->where('id', $spec_item_id)->find();

                        //如果这个规格在原有经销商名下也没有，那就报错
                        if (!$old_spec_item) {
//                            echo '处理失败:这个规格再原来的经销商居然都不存在goods_id:' . $goods['goods_id'] . 'spec_item_id:' . $spec_item_id;
                            Db::rollback();
                            return ['code' => 0, 'msg' => '处理失败:这个规格再原来的经销商居然都不存在goods_id:' . $goods['goods_id'] . 'spec_item_id:' . $spec_item_id, 'data' => null];
                        }
                        $isHasSpecItem = Db::name('SpecItem')->where(['seller_id' => $new_seller_id, 'spec_id' => $old_spec_item['spec_id'], 'item' => $old_spec_item['item']])->find();
                        //如果新的经销商不存在这个记录，就新增一个
    
                       
                        
                        $tempSpecItemId = null;//记录老的数据要替换新的
                        if ($isHasSpecItem) {

                            
                            
                            
                            
                            $new_item_id_arr[] = $isHasSpecItem['id'];
                            $new_item_name_arr[] = $isHasSpecItem['item'];
    
                            //修复一下商品颜色
                            Db::name('SpecColor')->where(['spec_color_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->update(['spec_color_id'=>$isHasSpecItem['id']]);
                            Db::name('SpecImage')->where(['spec_image_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->update(['spec_image_id'=>$isHasSpecItem['id']]);


//                            echo '新的经销商spec_item有记录,所以不用新增:' . $isHasSpecItem['id'] . '<br/>';

                        } else {

                            //用原来的做模板

                            $data = ['seller_id' => $new_seller_id, 'spec_id' => $old_spec_item['spec_id'], 'item' => $old_spec_item['item']];

                            $new_spec_item_id = Db::name('SpecItem')->insertGetId($data);
//                            echo '新的经销商spec_item没有记录,所以新增:' . $new_spec_item_id . '<br/>';
                            $new_item_id_arr[] = $new_spec_item_id;
                            $new_item_name_arr[] = $isHasSpecItem['item'];
    
                            //还要检查同商品的颜色、图片
                            $isHasSpecColor = Db::name('SpecColor')->where(['spec_color_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->find();
                            if($isHasSpecColor){
                                Db::name('SpecColor')->where(['spec_color_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->update(['spec_color_id'=>$new_spec_item_id]);
                            }else{
                                Db::name('SpecColor')->insertGetId(['spec_color_id'=>$new_spec_item_id,'goods_id'=>$goods['goods_id'],'spec_color'=>$isHasSpecColor['color']]);
                            }
    
                            //有就切换没有就新增
                            $isHasSpecImage = Db::name('SpecImage')->where(['spec_image_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->find();
                            
                            
                            if($isHasSpecImage){
                                Db::name('SpecImage')->where(['spec_image_id'=>$spec_item_id,'goods_id'=>$goods['goods_id']])->update(['spec_image_id'=>$new_spec_item_id]);
                            }else{
                                Db::name('SpecImage')->insertGetId(['spec_image_id'=>$new_spec_item_id,'goods_id'=>$goods['goods_id'],'src'=>$isHasSpecImage['src']]);
                            }
                            
                            
                            
                        }
                        
                        
                       
                        
                        

                    }
                    sort($new_item_id_arr);


                    //算新的
                    $new_key = implode('_', $new_item_id_arr);
                    $new_key_name = implode(' ', $new_item_name_arr);
//                    echo $sku['key'] . '==========================================' . $new_key . '<br/>';
                    //如果新旧不一样的话
                    if ($sku['key'] != $new_key) {
                        $rt = Db::name('SpecGoodsPrice')->where(['goods_id' => $goods['goods_id'], 'item_id' => $sku['item_id']])->update(['key' => $new_key, 'key_name' => $new_key_name]);


                        //订单购买表
                        $rt1 = Db::name('OrderGoods')->where('spec_key', $sku['key'])->where('goods_id', $sku['goods_id'])->update(['spec_key' => $new_key, 'spec_key_name' => $new_key_name]);
                        //退货表
//                    $rt2 = Db::name('ReturnGoods')->where('rch_id',$sku['item_id'])->update(['rch_id'=>$spec_price_2['item_id']]);

                        //退货表
                        $rt3 = Db::name('Cart')->where('spec_key', $sku['key'])->update(['spec_key' => $new_key]);


                        if ($rt !== false && $rt1 !== false && $rt3 !== false) {
//                            echo '处理成功:' . json_encode($sku) . '<br/>';
                        } else {
//                            echo '处理失败:' . json_encode($sku) . '<br/>';
                            Db::rollback();
                            return ['code' => 0, 'msg' => '处理失败:' . json_encode($sku), 'data' => null];
                        }


                    }


                }


            }


            if ($success < $len) {

                Db::rollback();
                return ['code' => 0, 'msg' => '部分操作失败', 'data' => null];
            }
            Db::commit();
            return ['code'=>1,'msg'=>'修改成功','data'=>null];
        }catch (Exception $e){
//            echo '更换店铺的经销商id错误,store_id:'.$store_id.'new_seller_id:'.$new_seller_id.'错误原因:'.$e->getMessage(),'error';
            trace('更换店铺的经销商id错误,store_id:'.$store_id.'new_seller_id:'.$new_seller_id.'错误原因:'.$e->getMessage(),'error');


            Db::rollback();
            return ['code' => 0, 'msg' => '错误原因:' . $e->getMessage(), 'data' => null];
        }


    }

    /**
     * 获取指定店铺信息
     * @param $uid int 用户UID
     * @param bool $relation 是否关联查询
     *
     * @return mixed 找到返回数组
     */
    public function detail($store_id, $relation = true)
    {
        $user = D('Store')->where(array('store_id' => $store_id))->relation($relation)->find();
        return $user;
    }

    /**
     * 添加申请店铺等级
     * @param $post_data
     * @return array
     */
    public function editStoreReopen($post_data){
        $re_grade_id = $post_data['re_grade_id'];
        $re_year = $post_data['re_year'];
        $store_id =$this->storeInfo['store_id'];
        $store_grade = Db::name('store_grade')->where(['sg_id'=>$re_grade_id])->find();  //店铺等级
        if (empty($store_grade))return ['status' => -1, 'msg' => '参数错误！'];
        $reopen_count = Db::name('store_reopen')->where(['re_store_id'=>$store_id,'re_state'=>['notIn','-1,2']])->count();  //店铺等级
        if ($reopen_count>0)return ['status' => -1, 'msg' => '您有申请未完成！'];
        $data=[
            're_grade_id'       =>$re_grade_id,    //申请等级
            're_year'           =>$re_year,        //续签时长
            're_grade_price'    =>$store_grade['sg_price'],    //等级收费(元/年)
            're_grade_name'     =>$store_grade['sg_name'],     //等级名称
            're_pay_amount'     =>$store_grade['sg_price']*$re_year,    //应付总金额
            're_store_id'       =>$this->storeInfo['store_id'],     //店铺ID
            're_store_name'     =>$this->storeInfo['store_name'],   //店铺名称
            're_start_time'     =>time(),                           //有效期开始时间
            're_end_time'       =>strtotime("+$re_year year"),      //有效期结束时间
            're_create_time'    =>time(),                           //记录创建时间
            're_state'          =>1,
            're_pay_cert'       =>$post_data['re_pay_cert'],
            're_pay_cert_explain'       =>$post_data['re_pay_cert_explain'],
        ];
        $res = Db::name('store_reopen')->add($data);
        if ($res === false){
            return ['status' => -1, 'msg' => '申请失败！'];
        }else{
            return ['status' => 1, 'msg' => '申请成功！'];
        }
    }

}