<?php

namespace app\seller\logic;

use think\Db;
use think\Model;

/**
 * 分类逻辑定义
 * Class CatsLogic
 * @package Home\Logic
 */
class GoodsLogic extends Model
{

    public function copyGoodsByStore2Store($toStoreId,$fromStoreId){

        //获取所有有效的商品
        $goodsList = Db::name('goods')->where('store_id',$fromStoreId)->where('is_deleted',0)->select();
        $stores = [];
        $stores[] = $toStoreId;
        foreach ($goodsList as $goods){
            //$this->copyGoods($goods['goods_id'],$tempArr);

            $goods_id = $goods['goods_id'];
            if(!$goods_id){
                return [ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null];
            }

            if(!is_array($stores)||count($stores)<1){
                return [ 'status' => 0, 'msg' => '复制的店铺id必须是数组', 'result' => null];
            }

            $goods = Db::name('goods')->where('goods_id',$goods_id)->find();
            if(!$goods){
                return [ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null];
            }




            if(!$stores||count($stores)<1){
                return [ 'status' => 0, 'msg' => '请至少选择一个店铺', 'result' => null];
            }

            $tmpl_goods_id = $goods_id;//简单值传递，所以不存在什么问题。
    
            //复制商品要形成绑定关系
            if($goods['tmpl_id']==0&&$goods['tmpl_sn']){
                $goods['tmpl_id'] = $goods_id;
            }else{
                if($goods['tmpl_id']>0){

                }else{
                    unset($goods['tmpl_id']);
                }
            }
    
            //删掉不合适的属性
            unset($goods['tmpl_sn']);
            
            
            //删掉不合适的属性
            unset($goods['goods_id']);
            
            unset($goods['goods_sn']);
            unset($goods['goods_store']);
            $err = 0;

            $type_id = $goods['cat_id3'];//这里是给新商品加属性的
            foreach ($stores as $store){
                $goods['store_id'] = $store;

                $tmplStoreInfo = Db::name('store')->where('store_id',$fromStoreId)->find();

                if($goods['seller_id']!=SELLER_ID&&(!$tmplStoreInfo ||$tmplStoreInfo['is_tmpl']!=1)){
                    return [ 'status' => 0, 'msg' => '该商品不属于当前经销商或者不属于模板店铺', 'result' => null];
                }


                $goods['seller_id'] = SELLER_ID;//保险一点


                $rt = Db::name('goods')->insert($goods);
                $goods_id = Db::name('goods')->getLastInsID();
                if(!$rt){
                    $err++;
                }

//                if (empty($spec_goods_item)) {
//                    update_stock_log(session('admin_id'), $goods['store_count'], array('goods_id' => $goods_id, 'goods_name' => $goods['goods_name'], 'store_id' => $store));
//                }

                $rt_afterSave = $this->afterSave_copy($goods_id,$store,$tmpl_goods_id);
                
                if($rt_afterSave['status']!=1){
                    return [ 'status' => 0, 'msg' => $rt_afterSave['msg'], 'result' => null];
                }

                $this->saveGoodsAttr_copy($goods_id,$type_id,$tmpl_goods_id); // 处理商品 属性,这type_id其实就是叶子类目
            }



        }


        return [ 'status' => 1, 'msg' => '成功复店铺', 'result' => null];

    }

//    public function copyGoods($goods_id,$stores=[]){
//
//
//        if(!$goods_id){
//            return [ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null];
//        }
//
//        if(!is_array($stores)||count($stores)<1){
//            return [ 'status' => 0, 'msg' => '复制的店铺id必须是数组', 'result' => null];
//        }
//
//        $goods = Db::name('goods')->where('goods_id',$goods_id)->find();
//        if(!$goods){
//            return [ 'status' => 0, 'msg' => 'goods_id必传', 'result' => null];
//        }
//
//        if($goods['seller_id']!=SELLER_ID){
//            return [ 'status' => 0, 'msg' => '该商品不属于当前经销商', 'result' => null];
//        }
//
//
//        if(!$stores||count($stores)<1){
//            return [ 'status' => 0, 'msg' => '请至少选择一个店铺', 'result' => null];
//        }
//
//        $tmpl_goods_id = $goods_id;//简单值传递，所以不存在什么问题。
//
//        //删掉不合适的属性
//        unset($goods['goods_id']);
//        unset($goods['goods_sn']);
//        unset($goods['goods_store']);
//        $err = 0;
//
//        $type_id = $goods['cat_id3'];//这里是给新商品加属性的
//        foreach ($stores as $store){
//            $goods['store_id'] = $store;
//            $rt = Db::name('goods')->insert($goods);
//            $goods_id = Db::name('goods')->getLastInsID();
//            if(!$rt){
//                $err++;
//            }
//
//            if (empty($spec_goods_item)) {
//                update_stock_log(session('admin_id'), $goods['store_count'], array('goods_id' => $goods_id, 'goods_name' => $goods['goods_name'], 'store_id' => $store));
//            }
//
//            $this->afterSave_copy($goods_id,$store,$tmpl_goods_id);
//
//            $this->saveGoodsAttr_copy($goods_id,$type_id,$tmpl_goods_id); // 处理商品 属性,这type_id其实就是叶子类目
//        }
//
//        return [ 'status' => 1, 'msg' => '成功复制到'.(count($stores)-$err).'个店铺，失败'.$err.'个', 'result' => null];
//
//    }

    /**
     * 动态获取商品属性输入框 根据不同的数据返回不同的输入框类型
     * @param int $goods_id 商品id
     * @param int $type_id 商品属性类型id
     * @return string
     */
    public function getAttrInput($goods_id, $type_id)
    {
        header("Content-type: text/html; charset=utf-8");
        $GoodsAttribute = D('GoodsAttribute');
        $attributeList = $GoodsAttribute->where(["type_id"=>$type_id,'attr_index'=>1])->select();

        foreach ($attributeList as $key => $val) {

            $curAttrVal = $this->getGoodsAttrVal(NULL, $goods_id, $val['attr_id']);
            //促使他 循环
            if (count($curAttrVal) == 0)
                $curAttrVal[] = array('goods_attr_id' => '', 'goods_id' => '', 'attr_id' => '', 'attr_value' => '', 'attr_price' => '');
            foreach ($curAttrVal as $k => $v) {
                $str .= "<tr class='attr_{$val['attr_id']}'>";
                $addDelAttr = ''; // 加减符号
                // 单选属性 或者 复选属性
                if ($val['attr_type'] == 1 || $val['attr_type'] == 2) {
                    if ($k == 0)
                        $addDelAttr .= "<a onclick='addAttr(this)' href='javascript:void(0);'>[+]</a>&nbsp&nbsp";
                    else
                        $addDelAttr .= "<a onclick='delAttr(this)' href='javascript:void(0);'>[-]</a>&nbsp&nbsp";
                }

                $str .= "<td>$addDelAttr {$val['attr_name']}</td> <td>";
 
                // 手工录入
                if ($val['attr_input_type'] == 2) {
                    $str .= "<input type='text' size='40' value='{$v['attr_value']}' name='attr_{$val['attr_id']}[]' />";
                }
                // 从下面的列表中选择（一行代表一个可选值）
                if ($val['attr_input_type'] == 1) {
                    $str .= "<select name='attr_{$val['attr_id']}[]'>";
                    $tmp_option_val = explode(',', $val['attr_values']);

                    $str .= "<option value=''>不限</option>";
                    foreach ($tmp_option_val as $k2 => $v2) {
                        // 编辑的时候 有选中值
                        $v2 = preg_replace("/\s/", "", $v2);
                        if ($v['attr_value'] == $v2)
                            $str .= "<option selected='selected' value='{$v2}'>{$v2}</option>";
                        else
                            $str .= "<option value='{$v2}'>{$v2}</option>";
                    }
                    $str .= "</select>";
                    //$str .= "属性价格<input type='text' maxlength='10' size='5' value='{$v['attr_price']}' name='attr_price_{$val['attr_id']}[]'>";
                }
                $str .= "</td></tr>";
                //$str .= "<br/>";
            }

        }
        return $str;
    }

    /**
     * 获取 tp_goods_attr 表中指定 goods_id  指定 attr_id  或者 指定 goods_attr_id 的值 可是字符串 可是数组
     * @param int $goods_attr_id tp_goods_attr表id
     * @param int $goods_id 商品id
     * @param int $attr_id 商品属性id
     * @return array 返回数组
     */
    public function getGoodsAttrVal($goods_attr_id = 0, $goods_id = 0, $attr_id = 0)
    {
        $GoodsAttr = D('GoodsAttr');
        if ($goods_attr_id > 0)
            return $GoodsAttr->where("goods_attr_id", $goods_attr_id)->select();
        if ($goods_id > 0 && $attr_id > 0)
            return $GoodsAttr->where(['goods_id' => $goods_id, 'attr_id' => $attr_id])->select();
    }

    /**
     *  给指定商品添加属性 或修改属性 更新到 tp_goods_attr
     * @param int $goods_id 商品id
     * @param int $goods_type 商品类型id
     */
    public function saveGoodsAttr($goods_id, $goods_type)
    {
        $GoodsAttr = M('GoodsAttr');
        //$Goods = M("Goods");

        // 属性类型被更改了 就先删除以前的属性类型 或者没有属性 则删除
        if ($goods_type == 0) {
            $GoodsAttr->where('goods_id', $goods_id)->delete();
            return true;
        }

        $GoodsAttrList = $GoodsAttr->where('goods_id', $goods_id)->select();

        $old_goods_attr = array(); // 数据库中的的属性  以 attr_id _ 和值的 组合为键名
        foreach ($GoodsAttrList as $k => $v) {
            $old_goods_attr[$v['attr_id'] . '_' . $v['attr_value']] = $v;
        }

        // post 提交的属性  以 attr_id _ 和值的 组合为键名
        $post_goods_attr = array();
        foreach ($_POST as $k => $v) {
            $attr_id = str_replace('attr_', '', $k);
            if (!strstr($k, 'attr_') || strstr($k, 'attr_price_'))
                continue;
            foreach ($v as $k2 => $v2) {
                $v2 = str_replace('_', '', $v2); // 替换特殊字符
                $v2 = str_replace('@', '', $v2); // 替换特殊字符
                $v2 = trim($v2);

                if (empty($v2))
                    continue;


                $tmp_key = $attr_id . "_" . $v2;
                $attr_price = $_POST["attr_price_$attr_id"][$k2];
                $attr_price = $attr_price ? $attr_price : 0;
                if (array_key_exists($tmp_key, $old_goods_attr)) // 如果这个属性 原来就存在
                {
                    if ($old_goods_attr[$tmp_key]['attr_price'] != $attr_price) // 并且价格不一样 就做更新处理
                    {
                        $goods_attr_id = $old_goods_attr[$tmp_key]['goods_attr_id'];
                        $GoodsAttr->where("goods_attr_id", $goods_attr_id)->save(array('attr_price' => $attr_price));
                    }
                } else // 否则这个属性 数据库中不存在 说明要做删除操作
                {
                    $GoodsAttr->add(array('goods_id' => $goods_id, 'attr_id' => $attr_id, 'attr_value' => $v2, 'attr_price' => $attr_price));
                }
                unset($old_goods_attr[$tmp_key]);
            }

        }
        //file_put_contents("b.html", print_r($post_goods_attr,true));
        // 没有被 unset($old_goods_attr[$tmp_key]); 掉是 说明 数据库中存在 表单中没有提交过来则要删除操作

        foreach ($old_goods_attr as $k => $v) {
            $GoodsAttr->where('goods_attr_id', $v['goods_attr_id'])->delete(); //
        }


        return true;

    }

    /**
     * 获取 规格的 笛卡尔积
     * @param $goods_id 商品 id
     * @param $spec_arr 笛卡尔积
     * @return string 返回表格字符串
     */
    //,$store_id = 0
    public function getSpecInput($goods_id, $spec_arr, $seller_id)
    {
        // <input name="item[2_4_7][price]" value="100" /><input name="item[2_4_7][name]" value="蓝色_S_长袖" />        
        /*$spec_arr = array(         
            20 => array('7','8','9'),
            10=>array('1','2'),
            1 => array('3','4'),
            
        );  */

        // 排序
//        $spec_arr_sort = $spec_arr2 = array();
//        foreach ($spec_arr as $k => $v) {
//            $spec_arr_sort[$k] = count($v);
//        }
//        ksort($spec_arr_sort);
//        foreach ($spec_arr_sort as $key => $val) {
//            $spec_arr2[$key] = $spec_arr[$key];
//        }
    
        $spec_arr2 = $spec_arr;




        $clo_name = array_keys($spec_arr2);
        $spec_arr2 = combineDika($spec_arr2); //  获取 规格的 笛卡尔积


        $spec = M('Spec')->getField('id,name'); // 规格表
//        $specItem = M('SpecItem')->where("store_id", $store_id)->getField('id,item,spec_id');//规格项
//        ->where("store_id", $seller_id)
        $specItem = M('SpecItem')->getField('id,item,spec_id');//规格项
        $keySpecGoodsPrice = M('SpecGoodsPrice')->where(['goods_id' => $goods_id])->getField('key,key_name,price,market_price,store_count,sku,is_share');//规格项
        
       # 201903 判断是否共享库存
        $isShare = M('goods')->where(['goods_id' => $goods_id])->field('is_share_stock,tmpl_id')->find();
        $isf = M('goods')->where(['goods_id' => $isShare['tmpl_id']])->getField('tmpl_id');

        $str = "<table class='table table-bordered' id='spec_input_tab'>";
        $str .= "<tr>";
        // 显示第一行的数据
        foreach ($clo_name as $k => $v) {
            $str .= " <td><b>{$spec[$v]}</b></td>";
        }
        $str .= "<td><b>价格</b></td>
                <td><b>市场价</b></td>
               <td><b>库存</b></td>
               <td><b>SKU</b></td>
               <!-- 201903 去掉共享库存 <td><b>共享库存</b></td> --!>
               <td><b>操作</b></td>
             </tr>";
        // 显示第二行开始
        foreach ($spec_arr2 as $k => $v) {
            $str .= "<tr>";
            $item_key_name = array();
            $spec_img_class = '';
            foreach ($v as $k2 => $v2) {
                $str .= "<td>{$specItem[$v2][item]}</td>";
                $item_key_name[$v2] = $spec[$specItem[$v2]['spec_id']] . ':' . $specItem[$v2]['item'];
                $spec_img_class .= 'spec_img_'.$v2.' ';
            }

//            var_dump($item_key_name);
            ksort($item_key_name);
            $item_key = implode('_', array_keys($item_key_name));
            $item_name = implode(' ', $item_key_name);

 
//            var_dump($item_key,$item_name,$keySpecGoodsPrice[$item_key]);die;
//            var_dump($keySpecGoodsPrice[$item_key][price],$keySpecGoodsPrice[$item_key][store_count]);
//            $keySpecGoodsPrice[$item_key][price] ? false : $keySpecGoodsPrice[$item_key][price] = 0; // 价格默认为0
//            $keySpecGoodsPrice[$item_key][store_count] ? false : $keySpecGoodsPrice[$item_key][store_count] = 0; //库存默认为0
            $keySpecGoodsPrice[$item_key][price] ? false : $keySpecGoodsPrice[$item_key][price] = null; // 价格默认为0
//            $keySpecGoodsPrice[$item_key][price]==0? $keySpecGoodsPrice[$item_key][price] = null:false;
            //初始化的时候需要区分是0还是null
            if(!$keySpecGoodsPrice[$item_key][store_count]){
                
                if($keySpecGoodsPrice[$item_key][store_count]===0){
                    $keySpecGoodsPrice[$item_key][store_count] = 0;
                }else{
                    $keySpecGoodsPrice[$item_key][store_count] = null;
                }
            }

            # 201903
            $checked = $isShare['is_share_stock']==1? 'readonly' :'';
            $isf = $goods_id && ($isf==0) ? 'readonly' :'';
            
            
            $str .= "<td><input class='batch-fill-text1' name='item[$item_key][price]' value='{$keySpecGoodsPrice[$item_key][price]}' onkeyup='this.value=this.value.replace(/[^\d.]/g,\"\")' onpaste='this.value=this.value.replace(/[^\d.]/g,\"\")' /></td>";
            $str .= "<td><input class='batch-fill-text2' name='item[$item_key][market_price]' value='{$keySpecGoodsPrice[$item_key][market_price]}' onkeyup='this.value=this.value.replace(/[^\d.]/g,\"\")' onpaste='this.value=this.value.replace(/[^\d.]/g,\"\")'/></td>";
            $str .= "<td><input class='batch-fill-text3' name='item[$item_key][store_count]' value='{$keySpecGoodsPrice[$item_key][store_count]}' onkeyup='this.value=this.value.replace(/[^\d.]/g,\"\")' onpaste='this.value=this.value.replace(/[^\d.]/g,\"\")'/></td>";
            $str .= "<td><input class='batch-fill-text4' name='item[$item_key][sku]' value='{$keySpecGoodsPrice[$item_key][sku]}' /><input type='hidden' name='item[$item_key][key_name]' value='$item_name' /></td>";

            # 20190304 去掉共享库存
            // if($keySpecGoodsPrice[$item_key][is_share]==1){
            //     $str .= "<td><input class='batch-fill-text4 js-input-share' checked  type='checkbox' name='item[$item_key][is_share]' value='1' /></td>";
            // }else{
            //     $str .= "<td><input class='batch-fill-text4 js-input-share'   type='checkbox' name='item[$item_key][is_share]' value='1' /></td>";
            // }

            $str .= "<td><a href='javascript:void(0);' class='delete_item'>删除</a></td>";
            $str .= "</tr>";
        }
        $str .= "</table>";
        return $str;
    }
    /**
     *  获取排好序的品牌列表

     */
    function getSortBrands()
    {
        $brandList = S('getSortBrands');
        if(!empty($brandList))
            return $brandList;

        $brandList = M("Brand")->select();
//        $brandIdArr =  M("Brand")->where("name in (select `name` from `".C('database.prefix')."brand` group by name having COUNT(id) > 1)")->getField('id,cat_id2');
        $brandIdArr = Db::name('brand')->where('id', 'IN', function ($query) {
            $query->name('brand')->group('name')->having('COUNT(id) > 1')->field('name');
        })->getField('id,cat_id2');
        $goodsCategoryArr = M('goodsCategory')->where("level", 1)->getField('id,name');
        $nameList = array();
        foreach ($brandList as $k => $v) {

            $name = getFirstCharter($v['name']) . '  --   ' . $v['name']; // 前面加上拼音首字母

            if (array_key_exists($v[id], $brandIdArr) && $v[cat_id]) // 如果有双重品牌的 则加上分类名称
                $name .= ' ( ' . $goodsCategoryArr[$v[cat_id]] . ' ) ';

            $nameList[] = $v['name'] = $name;
            $brandList[$k] = $v;
        }
        array_multisort($nameList, SORT_STRING, SORT_ASC, $brandList);
        S('getSortBrands',$brandList); 
        return $brandList;
    }

    /**
     *  获取排好序的分类列表
     */
    function getSortCategory()
    {
        $categoryList = Db::name("GoodsCategory")->where(['level'=>3,])->getField('id,name,parent_id,level');
        $nameList = array();
        foreach ($categoryList as $k => $v) {
            $name = getFirstCharter($v['name']) . ' ' . $v['name']; // 前面加上拼音首字母
            $nameList[] = $v['name'] = $name;
            $categoryList[$k] = $v;
        }
        array_multisort($nameList, SORT_STRING, SORT_ASC, $categoryList);

        return $categoryList;
    }

    /**
     * 删除商品(可批量删除)
     * @param $goods_ids
     * @return array
     */
    function delStoreGoods($goods_ids=''){
        if(empty($goods_ids)) return ['status' => -1, 'msg' => "非法操作！"];
        // 判断此商品是否有订单
        $ordergoods_count = Db::name('order_goods')->whereIn('goods_id', $goods_ids, 'and')->group('goods_id')->getField('goods_id', true);
        if ($ordergoods_count) {
            $goods_count_ids = implode(',', $ordergoods_count);
            return ['status' => -1, 'msg' => "ID为【{$goods_count_ids}】的商品有订单,不得删除!"];
        }

        //判断此商品是否有预定订单





        // 商品团购
        $groupBuy_goods = M('group_buy')->whereIn('goods_id', $goods_ids, 'and')->group('goods_id')->getField('goods_id', true);
        if ($groupBuy_goods) {
            $groupBuy_goods_ids = implode(',', $groupBuy_goods);
            return ['status' => -1, 'msg' => "ID为【{$groupBuy_goods_ids}】的商品有团购,不得删除!"];
        }
        // 商品退货记录
        $return_goods = Db::name('return_goods')->whereIn('goods_id', $goods_ids, 'and')->group('goods_id')->getField('goods_id', true);
        if ($return_goods) {
            $return_goods_ids = implode(',', $groupBuy_goods);
            return ['status' => -1, 'msg' => "ID为【{$return_goods_ids}】的有退货记录,不得删除!"];
        }
        // 删除此商品
//        $result = Db::name('Goods')->where(['store_id' => STORE_ID])->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品表
        $result = Db::name('Goods')->where(['seller_id' => SELLER_ID])->whereIn('goods_id', $goods_ids, 'and')->update(['is_deleted'=>1]);  //商品表

        if(!$result){
            return ['status' => -1, 'msg' => '操作失败,无法获取到指定的商品',];
        }

        if ($result !== false) {
            Db::name('cart')->whereIn('goods_id', $goods_ids, 'and')->delete();  // 购物车
            Db::name('comment')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品评论
            Db::name('goods_consult')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品咨询
            Db::name('goods_images')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品相册
            Db::name('spec_goods_price')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品规格
            Db::name('spec_image')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品规格图片
            Db::name('goods_attr')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品属性
            Db::name('goods_collect')->whereIn('goods_id', $goods_ids, 'and')->delete();  //商品收藏
            return ['status' => 1, 'msg' => '操作成功',];
        }else{
            return ['status' => -1, 'msg' => '操作失败',];
        }
    }
    /**
     * 后置操作方法
     * 自定义的一个函数 用于数据保存后做的相应处理操作, 使用时手动调用
     * @param int $goods_id 商品id
     * @param int $store_id 店铺id
     */
    public function afterSave($goods_id,$store_id)
    {
        // 商品货号
        $goods_sn = "TP".str_pad($goods_id,7,"0",STR_PAD_LEFT);
        Db::name('goods')->where("goods_id = $goods_id and goods_sn = ''")->save(array("goods_sn"=>$goods_sn)); // 根据条件更新记录
        $goods_images = I('goods_images/a');
        $img_sorts = I('img_sorts/a');
        $original_img = I('original_img');
        $item_img = I('item_img/a');
        $item_color= I('item_color/a');

        // 商品图片相册  图册
        if(count($goods_images) > 1)
        {

            array_pop($goods_images); // 弹出最后一个
            $goodsImagesArr = M('GoodsImages')->where("goods_id = $goods_id")->getField('img_id,image_url'); // 查出所有已经存在的图片

            // 删除图片
            foreach($goodsImagesArr as $key => $val)
            {
                if(!in_array($val, $goods_images)){
                    M('GoodsImages')->where("img_id = {$key}")->delete();

                    //同时删除物理文件
                    $filename = $val;
                    $filename= str_replace('../','',$filename);
                    $filename= trim($filename,'.');
                    $filename= trim($filename,'/');
                    $is_exists = file_exists($filename);

                    //同时删除物理文件
                    if($is_exists){
                        unlink($filename);
                    }
                }
            }
            $goodsImagesArrRever = array_flip($goodsImagesArr);
            // 添加图片
            foreach($goods_images as $key => $val)
            {
                $sort = $img_sorts[$key];
                if($val == null)  continue;
                if(!in_array($val, $goodsImagesArr))
                {
                    $data = array( 'goods_id' => $goods_id,'image_url' => $val , 'img_sort'=>$sort);
                    M("GoodsImages")->insert($data); // 实例化User对象
                }else{
                    $img_id = $goodsImagesArrRever[$val];
                    //修改图片顺序
                    M('GoodsImages')->where("img_id = {$img_id}")->save(array('img_sort' => $sort));
                }
            }
        }

        // 查看主图是否已经存在相册中
        $c = M('GoodsImages')->where("goods_id = $goods_id and image_url = '{$original_img}'")->count();

        //@modify by wangqh fix:修复删除商品详情的图片(相册图刚好是主图时)删除的图片仍然在相册中显示. 如果主图存物理图片存在才添加到相册 @{
        $deal_orignal_img = str_replace('../','',$original_img);
        $deal_orignal_img= trim($deal_orignal_img,'.');
        $deal_orignal_img= trim($deal_orignal_img,'/');
        if($c == 0 && $original_img && file_exists($deal_orignal_img))//@}
        {
            M("GoodsImages")->add(array('goods_id'=>$goods_id,'image_url'=>$original_img));
        }
        clearCache();
    
    
    
        $goodsSepcLogList = Db::name('spec_goods_price')->where('goods_id',$goods_id)->field('item_id,goods_id,key,key_name')->select();
        // 商品规格价钱处理
        $goods_item = I('item/a');
//        var_dump($goods_item,$goods_id);
        if ($goods_item) {
            //删掉所有的，重新排序
            Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->delete();
            
            $tempArr = Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->select();
            
            $store_count = 0;
            $keyArr = '';//规格key数组
            foreach ($goods_item as $k => $v) {
                
                
                //如果库存价格还有sku都是null，就删掉。注意不是0.00.
                if($v['price']==''&&$v['store_count']==''&&$v['sku']==''){
                    continue;//如果价格或者商品为0，则不保存
                }
                //批量添加数据
                $v['price'] = trim($v['price']);
    
                $v['market_price'] = trim($v['market_price']);
    
                //当其他两个值有一个为真时.库存的默认值为0
                if(($v['price']&&$v['price']!='')||($v['store_count']&&$v['store_count']!='')){
                    $v['store_count'] = $v['store_count']?$v['store_count']:0;
                }
                
                
                $store_count += $v['store_count']; // 记录商品总库存
                $v['sku'] = trim($v['sku']);
                
                //var_dump($v);die;
                
                $keyArr .= $k.',';
                $data = array('goods_id' => $goods_id, 'key' => $k, 'key_name' => $v['key_name'], 'price' => $v['price'], 'store_count' => $v['store_count'], 'sku' => $v['sku'], 'store_id' => $store_id,'market_price'=>$v['market_price'],'is_share'=>$v['is_share']);
                $specGoodsPrice = Db::name('spec_goods_price')->where(['goods_id' => $data['goods_id'], 'key' => $data['key']])->find();
                if ($item_img) {
                    $spec_key_arr = explode('_', $k);
                    foreach ($item_img as $key => $val) {
                        if (in_array($key, $spec_key_arr)) {
                            $data['spec_img'] = $val;
                            break;
                        }
                    }
                }
                
                if($specGoodsPrice){
                    Db::name('spec_goods_price')->where(['goods_id' => $goods_id, 'key' => $k])->update($data);
                    $stock = $specGoodsPrice['store_count'] - $data['store_count'];
                    if($stock != 0){
                        update_stock_log(session('seller_id'), $stock,array('goods_id'=>$goods_id,'key_name'=>$data['key_name'],'goods_name'=>I('goods_name'),'store_id'=>$store_id));
                    }
                }else{
                    
                    Db::name('spec_goods_price')->insert($data);
                    update_stock_log(session('seller_id'), $data['store_count'],array('goods_id'=>$data['goods_id'],'key_name'=>$data['key_name'],'goods_name'=>I('goods_name'),'store_id'=>$store_id));
                }
                // 修改商品后购物车的商品价格也修改一下
                M('cart')->where("goods_id = $goods_id and spec_key = '$k'")->save(array(
                    'market_price' => $v['price'], //市场价
                    'goods_price' => $v['price'], // 本店价
                    'member_goods_price' => $v['price'], // 会员折扣价
                ));
            }
           
            if($keyArr){
//                ->whereOr(['store_count'=>0,'price'=>0])
                //同时删掉价格或者库存为0
                \think\log::info('商品价格未删除前记录'.json_encode($goodsSepcLogList));
                Db::name('spec_goods_price')->where('goods_id',$goods_id)->whereNotIn('key',$keyArr)->delete();
            }
        }else{
            \think\log::info('商品价格未删除前记录'.json_encode($goodsSepcLogList));
            Db::name('spec_goods_price')->where(['goods_id' => $goods_id])->delete();
        }
        
        //统一去掉所有的

        // 商品规格图片处理
        if($item_img)
        {
            M('SpecImage')->where("goods_id = $goods_id")->delete(); // 把原来是删除再重新插入
            foreach ($item_img as $key => $val)
            {
                M('SpecImage')->insert(array('goods_id'=>$goods_id ,'spec_image_id'=>$key,'src'=>$val,'store_id'=>$store_id));
            }
        }
    
        //已经可以单独修改，就不需要再保存一次了.
        //呆逼，如果是新增的规格或者新增的商品，那不就尴尬了。
        if($item_color)
        {
            M('SpecColor')->where("goods_id = $goods_id")->delete(); // 把原来是删除再重新插入
            foreach ($item_color as $key => $val)
            {
                M('SpecColor')->insert(array('goods_id'=>$goods_id ,'spec_color_id'=>$key,'spec_color'=>$val,'store_id'=>$store_id));
            }
        }
        refresh_stock($goods_id); // 刷新商品库存


        return true;

    }
    
    
    /**
     * 后置操作方法
     * 自定义的一个函数 用于数据保存后做的相应处理操作, 使用时手动调用
     * @param int $goods_id 商品id
     * @param int $store_id 店铺id
     * @param int $tmpl_goods_id
     */
    public function afterSave_copy($goods_id,$store_id,$tmpl_goods_id)
    {

        // 商品货号
        $goods_sn = "TP".str_pad($goods_id,7,"0",STR_PAD_LEFT);
        Db::name('goods')->where("goods_id = $goods_id and goods_sn = ''")->save(array("goods_sn"=>$goods_sn)); // 根据条件更新记录
        
//        $goods_images = I('goods_images/a');
//        $img_sorts = I('img_sorts/a');
//        $original_img = I('original_img');
//        $item_img = I('item_img/a');
//
//        array_pop($goods_images); // 弹出最后一个
        
        
        $goodsImagesArr = M('GoodsImages')->where("goods_id = $tmpl_goods_id")->getField('img_id,image_url,img_sort'); // 查出所有模板商品的图片

        // 添加图片,主图肯定也添加进去了。。。
        foreach($goodsImagesArr as $key => $val)
        {
            \think\Log::info('复制图片地址'.$val);
            $sort = $goodsImagesArr[$key]['img_sort'];
            if($val == null)  continue;
            //无脑添加就行啦
            $data = array( 'goods_id' => $goods_id,'image_url' => $goodsImagesArr[$key]['image_url'] , 'img_sort'=>$sort);
            M("GoodsImages")->insert($data); // 实例化User对象
        }


        clearCache();


        

        //先确保模板商品所在的经销商有的规格，需要复制的经销商也有。

        $tmplGoods = Db::name('goods')->where('goods_id',$tmpl_goods_id)->find();

        $tmpl_goods_seller_id = Db::name('store')->where('store_id',$tmplGoods['store_id'])->value('seller_id');
        $type_id = M('goods_category')->where("id", $tmplGoods['cat_id3'])->getField('type_id'); // 找到这个分类对应的type_id
        if(empty($type_id)){
            return [ 'status' => 0, 'msg' => '复制商品-目标商品类别不能为空', 'result' => null];
        }
        $spec_id_arr = M('spec_type')->where("type_id", $type_id)->getField('spec_id', true); // 找出这个类型的 所有 规格id
        if(empty($spec_id_arr)){
            return [ 'status' => 0, 'msg' => '复制商品-目标商品类别绑定的规格为空', 'result' => null];
        }

        $linkSpecArr = [];  //33=>343  用旧的做键名，新的做键值。来记录下来
        $specList = D('Spec')->where("id", "in", implode(',', $spec_id_arr))->order('`order` desc')->select(); // 找出这个类型的所有规格
        if ($specList) {
            $tempArr = [];
            foreach ($specList as $k => $v) {



                //去拿所有的目标商品关联的specItem
                $rows = Db::name('SpecItem')->where(['spec_id' => $v['id'],'seller_id'=>$tmplGoods['seller_id']])->order('id asc')->select(); // 获取规格项

                foreach ($rows as &$row){
                    $tempOldId = $row['id'];
                    unset($row['id']);
                    $filter2 = array();
                    $filter2['item'] = $row['item'];
                    $filter2['seller_id'] = SELLER_ID;
                    $filter2['spec_id'] = $row['spec_id'];

//                    trace('filter string is'.json_encode($filter2));
                    //查看自己有没有
                    $isHas = Db::name('SpecItem')->where($filter2)->find();
                    if(!$isHas){

                        $newRowId = Db::name('SpecItem')->insertGetId($row);//写进去

                    }else{
                        $newRowId = $isHas['id'];
                    }
                    $linkSpecArr[$tempOldId] = $newRowId;

                }

            }
        }
    
    
        //提前取出来所有的图片和颜色组合
        $hasSpecImage = Db::name('spec_image')->where(['goods_id' => $tmpl_goods_id])->select();
        $hasSpecColor = Db::name('spec_color')->where(['goods_id' => $tmpl_goods_id])->select();
        
        // 商品规格价钱处理
        $hasSpecGoodsPrice = Db::name('spec_goods_price')->where(['goods_id' => $tmpl_goods_id])->order('item_id asc')->select();
        foreach ($hasSpecGoodsPrice as $tmplSpec){



            unset($tmplSpec['item_id']);
            $tmplSpec['goods_id'] = $goods_id;
            $tmplSpec['store_id'] = $store_id;


            $keyArr = explode('_',$tmplSpec['key']);//key组成的数组 biubiu 88_99_110_120

            //用对照表就完成了，不用那么费事
            foreach ($keyArr as $j=>$item){
                foreach ($linkSpecArr as $k=>$link){
                    //一旦有新旧对应的，这里只能是颜色之类的。就替换掉,而所有商品的属性肯定都在这里了
                    if((int)$item==(int)$k){

                        $hasSpecImage = Db::name('spec_image')->where(['goods_id' => $tmpl_goods_id,'spec_image_id'=>$item])->find();
                        if($hasSpecImage){
                            $hasSpecImage['goods_id'] = $goods_id;
                            $hasSpecImage['store_id'] = $store_id;
                            $hasSpecImage['spec_image_id'] = $link;

                            $has = Db::name('spec_image')->where(['goods_id'=>$goods_id,'store_id'=>$store_id,'spec_image_id'=>$link])->count();
                            if(!$has){
                                Db::name('spec_image')->insert($hasSpecImage);
                            }


                        }

                        $hasSpecColor = Db::name('spec_color')->where(['goods_id' => $tmpl_goods_id,'spec_color_id'=>$item])->find();
                        if($hasSpecColor){
                            $hasSpecColor['goods_id'] = $goods_id;
                            $hasSpecColor['store_id'] = $store_id;
                            $hasSpecColor['spec_color_id'] = $link;
                            $has = Db::name('spec_color')->where(['goods_id'=>$goods_id,'store_id'=>$store_id,'spec_color_id'=>$link])->count();
                            if(!$has){
                                Db::name('spec_color')->insert($hasSpecColor);
                            }


                        }
                        $keyArr[$j] = $link;//用新的替换掉老的
                    }
                }

            }

            sort($keyArr);

            $tmplSpec['key'] = join('_',$keyArr);

            Db::name('spec_goods_price')->insert($tmplSpec);

            $muid = Db::name('seller')->where('seller_id',SELLER_ID)->value('user_id');

            //库存变动放在这里才合适
            $goods = array('goods_id' => $goods_id, 'goods_name' => $tmplGoods['goods_name'], 'store_id' => $store_id,'key_name'=>$tmplSpec['key_name']);
            update_stock_log($muid,$tmplSpec['store_count'], $goods,null,'复制商品');
    
            
        }
        
       
//
//        // 商品规格颜色-图片处理
        //为了把spec_image_id和spec_color_id里面的保存为新商品的记录，不能粗暴的弄
//        $specImageArr = [];
//        foreach ($hasSpecImage as $specImage){
//            $specImage['goods_id'] = $goods_id;
//            $specImage['store_id'] = $store_id;
//
//            $specImageArr[] = $specImage;
//        }
//
//        Db::name('spec_image')->insertAll($specImageArr);
////
////        // 商品规格颜色-color值处理
////
//        $specColorArr = [];
//        foreach ($hasSpecColor as $specColor){
//            $specColor['goods_id'] = $goods_id;
//            $specColor['store_id'] = $store_id;
//            $specColorArr[] = $specColor;
//        }
//
//        Db::name('spec_color')->insertAll($specColorArr);
        
       
        refresh_stock($goods_id); // 刷新商品库存
    
        return [ 'status' => 1, 'msg' => 'success', 'result' => null];
        
    }
    
    
    /**
     *  给指定商品添加属性 或修改属性 更新到 tp_goods_attr
     * @param int $goods_id 商品id
     * @param int $goods_type 商品类型id
     */
    public function saveGoodsAttr_copy($goods_id, $goods_type,$tmpl_goods_id)
    {
        $GoodsAttr = M('GoodsAttr');
        //$Goods = M("Goods");
        
       
        
        $hasGoodsAttrList = $GoodsAttr->where('goods_id', $tmpl_goods_id)->select();
        
        foreach ($hasGoodsAttrList as $goodsAttr){
            unset($goodsAttr['goods_attr_id']);
            $goodsAttr['goods_id'] = $goods_id;
            $GoodsAttr->add($goodsAttr);
        }
        
        
    }
}