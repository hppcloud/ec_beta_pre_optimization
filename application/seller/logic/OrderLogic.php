<?php
namespace app\seller\logic;

use think\Db;
use app\common\logic\WechatLogic;
use think\Exception;

class OrderLogic
{
	/**
	 * 获取店铺今天的销售状况
	 * @param $store_id
	 * @return mixed
	 */
	public function getTodayAmount($store_id){
		$now = strtotime(date('Y-m-d'));
		$today_order = Db::name('order')->where(['add_time'=>['gt',$now],'store_id'=>$store_id])->select();
		$today['today_order']=$today['cancel_order'] =0;
		$goods_price=$total_amount=$order_prom_amount=0;
		foreach($today_order as $key=>$order){
			$today['today_order'] +=1;  //今日总订单
			if($order['order_status']==3 ){
				$today['cancel_order'] +=1;  //今日取消订单
			}
			if(($order['order_status']==1 || $order['order_status'] == 2 || $order['order_status']==4) && ($order['pay_status']== 1 || $order['pay_code'] =='cod')){
				$goods_price +=$order['goods_price']; //今日订单商品总价
				$total_amount +=$order['total_amount']; //今日已收货订单总价
				$order_prom_amount +=$order['order_prom_amount']; //今日订单优惠
			}
		}
		$today['today_amount'] = $goods_price-$order_prom_amount; //今日销售总额（有效下单）
		return $today;
	}

	/**
     * 获取订单商品详情
     * @param $order_id  订单ID
     * @param string $is_send  状态
     * @return mixed
     */
    public function getOrderGoods($order_id,$is_send =''){
        if($is_send){
            $where=" and o.is_send < $is_send";
        }

        $goods_id_arr_cancel = Db::name('return_goods')->where("order_id", $order_id)->where('status','in',[0,1,2,3,4,5,6])->column('rec_id');



        if(count($goods_id_arr_cancel)){

            $where .=" and  o.rec_id not in (".implode(',',$goods_id_arr_cancel).')';
        }


        $select_year = getTabByOrderId($order_id);
        $sql = "SELECT g.*,o.*,(o.goods_num * o.member_goods_price) AS goods_total FROM __PREFIX__order_goods{$select_year} o ".
            "LEFT JOIN __PREFIX__goods g ON o.goods_id = g.goods_id WHERE o.order_id = :order_id ".$where;
        $res = Db::query($sql,['order_id'=>$order_id]);
        return $res;
    }

    /**
     * 获取订单信息
     * @return  bool|array
     */
    public function getOrderInfo($order_id)
    {
        //  订单总金额查询语句		
        $select_year = getTabByOrderId($order_id);
        $order = M('order'.$select_year)->where(array('order_id'=>$order_id,'seller_id'=>SELLER_ID))->find();
        if(!$order) return false;
        $order['address2'] = $this->getAddressName($order['province'],$order['city'],$order['district']);
        $order['address2'] = $order['address2'].$order['address'];
        //已经自动转化了
//        if($order['shipping_id']==3){
//            $order['shipping_info']= json_decode($order['shipping_info'],true);
//            $order['ziti_info']= json_decode($order['ziti_info'],true);
//        }
        return $order;
    }

    /*
     * 根据商品型号获取商品
     */
    public function get_spec_goods($goods_id_arr){
    	if(!is_array($goods_id_arr)) return false;
    		foreach($goods_id_arr as $key => $val)
    		{
    			$arr = array();
    			$goods = M('goods')->where("goods_id", $key)->find();
    			$arr['goods_id'] = $key; // 商品id
    			$arr['goods_name'] = $goods['goods_name'];
    			$arr['goods_sn'] = $goods['goods_sn'];
    			$arr['market_price'] = $goods['market_price'];
    			$arr['goods_price'] = $goods['shop_price'];
    			$arr['cost_price'] = $goods['cost_price'];
    			$arr['member_goods_price'] = $goods['shop_price'];
    			foreach($val as $k => $v)
    			{
    				$arr['goods_num'] = $v['goods_num']; // 购买数量
    				// 如果这商品有规格
    				if($k != 'key')
    				{
    					$arr['spec_key'] = $k;
    					$spec_goods = M('spec_goods_price')->where(['goods_id'=>$key,'key'=>$k])->find();
    					$arr['spec_key_name'] = $spec_goods['key_name'];
    					$arr['member_goods_price'] = $arr['goods_price'] = $spec_goods['price'];
    					$arr['sku'] = $spec_goods['sku'];
    				}
    				$order_goods[] = $arr;
    			}
    		}
    		return $order_goods;	
    }

    /**
     * 订单操作记录
     * @param $order
     * @param $action
     * @param string $note
     * @param int $action_user
     * @param int $user_type
     * @return mixed
     */
    public function orderActionLog($order,$action,$note='',$action_user = 0,$user_type = 0){
        $data['order_id'] = $order['order_id'];
        $data['action_user'] = $action_user;
        $data['seller_id'] = SELLER_ID;
        $data['user_type'] = $user_type; // 0管理员 1商家 2前台用户
        $data['action_note'] = $note;
        $data['order_status'] = $order['order_status'];
        $data['pay_status'] = $order['pay_status'];
        $data['shipping_status'] = $order['shipping_status'];
        $data['log_time'] = time();
        $data['status_desc'] = $action;
        trace('订单记录:'.json_encode($data));
        return M('order_action')->add($data);//订单操作记录
    }

    /**
     * 得到发货单流水号
     */
    public function get_delivery_sn()
    {
		mt_srand((double) microtime() * 1000000);
        return date('YmdHi') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
    }

    /*
     * 获取当前可操作的按钮
     */
    public function getOrderButton($order){
        
        // 三个月以前订单无任何操作按钮
        if((time()-strtotime($order['create_time'])) > (86400 * 90)){
            return array();
        }
        
        /*
         *  操作按钮汇总 ：付款、设为未付款、确认、取消确认、无效、去发货、确认收货、申请退货
         * 
         */
    	$os = $order['order_status'];//订单状态
    	$ss = $order['shipping_status'];//发货状态
    	$ps = $order['pay_status'];//支付状态
		$pt = $order['prom_type'];//订单类型：0默认1抢购2团购3优惠4预售5虚拟6拼团
        $btn = array();
        
        
        //统计购买记录和退款操作记录
        $orderRequireReturnGoodsCount = Db::name('ReturnGoods')->where('order_id',$order['order_id'])->count();
        $orderGoodsCount = Db::name('OrderGoods')->where('order_id',$order['order_id'])->count();
        $orderReturnGoods = Db::name('ReturnGoods')->where('order_id',$order['order_id'])->find();


        if($order['pay_code'] == 'cod') {
        	if($os == 0 && $ss == 0){
				if($pt != 6){
					$btn['confirm'] = '确认';
				}
        	}elseif($os == 1 && $ss == 0 ){
        		$btn['delivery'] = '去发货';
				if($pt != 6){
//					$btn['cancel'] = '取消确认';
				}
        	}elseif($ss == 1 && $os == 1 && $ps == 0){
//        		$btn['pay'] = '付款';
        	}elseif($ps == 1 && $ss == 1 && $os == 1){
//				if($pt != 6){
//					$btn['pay_cancel'] = '设为未付款';
//				}
        	}elseif($os == 1 && $ss == 2){
				$btn['delivery'] = '去发货';
			}
        }else{

        	if($ps == 0 && $os == 0 || $ps == 2){
//                $btn['pay'] = '付款';
        	}elseif($os == 0 && $ps == 1){
				if($pt != 6){
					$btn['pay_cancel'] = '设为未付款';
					$btn['confirm'] = '确认';
				}
        	}elseif($os == 1 && $ps == 1 && $ss==0){
				if($pt != 6){
//					$btn['cancel'] = '取消确认';
				}
        		$btn['delivery'] = '去发货';
        	}elseif(($os == 1 && $ps == 1 && $ss==2)){
//				$btn['delivery'] = '去发货';
			}

			if($os==1&&$ps==1&&$ss===0){
                $btn['seller_order_cancel'] = '取消订单';//支付后的订单，在没有发货前可以取消的
            }
        } 
               
        if(($ss == 1||$ss == 0) && $os == 1 && $ps == 1){
//        	$btn['delivery_confirm'] = '确认收货';
//        	$btn['refund'] = '申请退货';//这里无法出现，因为退货是按照商品来发起的
        }elseif($os == 2 || $os == 4){
//        	$btn['refund'] = '申请退货';//这里无法出现，因为退货是按照商品来发起的
        }elseif($os == 3 || $os == 5){
//        	$btn['remove'] = '移除';
        }
//        if($os != 5 && $ps != 1){
//        	$btn['invalid'] = '无效';
//        }

        //如果订单的未已收货，而且支付状态为1,就可以后台显示商家主动换货的按钮。
        if($os==4||$os==2){
            $btn['return'] = '申请换货';
            $btn['refund'] = '申请退货';
        }
        
        if($order['order_status'] < 2)
        {
             $btn['edit'] = '修改订单'; // 修改订单   
             $select_year = getTabByOrderId($order['order_id']);
             $c = M('order_goods'.$select_year)->where('order_id',$order['order_id'])->sum('goods_num');
             if($c >= 2 && $order['pay_status'] == 1)
                 $btn['split'] = '拆分订单'; // 拆分订单 
             
        }

        //兼容部分退款的，但是还有商品需要发货的情况
        if($os==5&&$orderGoodsCount!=$orderRequireReturnGoodsCount&&$ss==0){
            $btn['delivery'] = '去发货';
        }


        //退款订单 用户取消或者经销商未同意
        if($os==5&&$ss==0&&$orderReturnGoods&&($orderReturnGoods['status']==-1||$orderReturnGoods['status']==-2)){
            $btn['delivery'] = '去发货';
        }


        
        return $btn;
    }

    
    public function orderProcessHandle($order_id,$act,$store_id = 0){
    	$updata = array();
    	switch ($act){
    		case 'pay': //付款
                $order_sn = M('order')->where("order_id", $order_id)->getField("order_sn");
                update_pay_status($order_sn); // 调用确认收货按钮
    			return true;    			
    		case 'pay_cancel': //取消付款
    			$updata['pay_status'] = 0;
    			break;
    		case 'confirm': //确认订单
    			$updata['order_status'] = 1;
    			break;
    		case 'cancel': //取消确认
    			$updata['order_status'] = 0;
    			break;
//    		case 'invalid': //作废订单
//    			$updata['order_status'] = 5;
//    			break;
    		case 'remove': //移除订单
    			$this->delOrder($order_id,$store_id);
    			break;
    		case 'delivery_confirm'://确认收货
    			confirm_order($order_id); // 调用确认收货按钮
    			return true;
    		default:
    			return true;
    	}                
    	return M('order')->where(['order_id'=>$order_id,'seller_id'=>SELLER_ID])->save($updata);//改变订单状态
    }

    /**
     *	处理发货单
     * @param array $data
     * @param $store_id
     * @return mixed
     */
    public function deliveryHandle($data,$seller_id){
        Db::startTrans();
        
        try{
            $order = $this->getOrderInfo($data['order_id']);
            if($order['prom_type'] == 5) return false;
            $orderGoods = $this->getOrderGoods($data['order_id']);
            $selectgoods = $data['goods'];
    
            $data['order_sn'] = $order['order_sn'];
            $data['delivery_sn'] = $this->get_delivery_sn();
            $data['zipcode'] = $order['zipcode'];
            $data['user_id'] = $order['user_id'];
            $data['admin_id'] = session('seller_id');
            $data['consignee'] = $order['consignee'];
            $data['mobile'] = $order['mobile'];
            $data['country'] = $order['country'];
            $data['province'] = $order['province'];
            $data['city'] = $order['city'];
            $data['district'] = $order['district'];
            $data['address'] = $order['address'];
            $data['shipping_price'] = $order['shipping_price'];
            $data['create_time'] = time();
            $data['seller_id'] = $seller_id;
            if($data['shipping'] == 1){
                return $this->updateOrderShipping($data,$order);
            }else{
                if($data['seller_address_id']){
                    $seller_address = Db::name('seller_address')->where(['seller_address_id'=>$data['seller_address_id'],'seller_id'=>SELLER_ID,'type' => 0])->find();
                }else{
                    $seller_address = Db::name('seller_address')->where(['seller_id' => SELLER_ID, 'type' => 0])->order('is_default desc')->find();
                }
        
                if($seller_address){
                    $data['seller_address_consignee'] = $seller_address['consignee'];
                    $data['seller_address_mobile'] = $seller_address['mobile'];
                    $data['seller_address_province_id'] = $seller_address['province_id'];
                    $data['seller_address_city_id'] = $seller_address['city_id'];
                    $data['seller_address_district_id'] = $seller_address['district_id'];
                    $data['seller_address'] = $seller_address['address'];
                }
                $did = Db::name('delivery_doc')->add($data);
                $is_delivery = 0;
                foreach ($orderGoods as $k=>$v){
                    if($v['is_send'] == 1){
                        $is_delivery++;
                    }
                    if($v['is_send'] == 0 && in_array($v['rec_id'],$selectgoods)){
                
                        $res['is_send'] = 1;
                        $res['delivery_id'] = $did;
                        Db::name('order_goods')->where(['rec_id'=>$v['rec_id']])->save($res);//改变订单商品发货状态
                        $is_delivery++;
                    }
            
                }
                $updata['shipping_time'] = time();
                $updata['shipping_code'] = $data['shipping_code'];
                $updata['shipping_name'] = $data['shipping_name'];
                if($is_delivery == count($orderGoods)){
                    $updata['shipping_status'] = 1;//全部发货
                }else{
                    $updata['shipping_status'] = 1;//部分发货(不要改，我特意改的)
                }
            }
    
            $result = Db::name('order')->where(['order_id'=>$data['order_id']])->save($updata);//改变订单状态
            
            
    
            // 发送微信模板消息通知
//		$wechat = new WechatLogic;
//        $wechat->sendTemplateMsgOnDeliver($data);
    
    
    
            //商家发货，发送邮件
            
            $user_id = $order['user_id'];
            $users = Db::name('users')->where('user_id', $user_id)->field('user_id , nickname , mobile,email')->find();
    
            
            //array('商家发货','尊敬的${user_name}用户，您的订单${order_sn}已发货，收货人${consignee}，请您及时查收'),
            if($users&&$result!==false){
        
                
                
                //如果有短信就发短信
                if(!isEmptyObject($users['mobile'])){
                    //商家发货, 发送短信给客户
                    $res = checkEnableSendSms("5");
                    if($res){
                     
                        $sender = $users['mobile'];
                        $params = array('user_name'=>'商城' ,'order_sn'=>$order['order_sn'],'consignee'=>$order['consignee']);
                        $sendRT = sendSms("5", $sender, $params);
                        trace('发货发送短信给用户'.json_encode($sendRT));
                    }
            
                }
        
                //如果有邮箱就发邮件
                if(!isEmptyObject($users['email'])){
    
                    $nickname = '';
                    //按照优先级来
                    if(!isEmptyObject($users['nickname'])){
                        $nickname = $users['nickname'];
                    }elseif ($order['shipping_code']==='ziti'){
                        $ziti_info = $order['ziti_info'];//前面已经转化过
                        if(isset($ziti_info['user_name'])&&!isEmptyObject($ziti_info['user_name'])){
                            $nickname = $ziti_info['user_name'];
                        }
                    }elseif($order['shipping_code']==='shunfeng'&&!isEmptyObject($order['consignee'])){
                        $nickname = $data['consignee'];
                    }
                    
                    
                    $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
　

<head>
    　　
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    　　
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style>
        * {
            font-family: "SF Pro SC", "SF Pro Text", "SF Pro Icons", "PingFang SC", "Helvetica Neue", "Helvetica", "Arial", sans-serif;
        }
        .top-txt {
            position: absolute;
            width: 100%;
            top: 66px;
            left: 64px;
            color: white;
            font-size: 24px;
        }
        .top-small {
            position: absolute;
            bottom: 52px;
            color: white;
            left: 64px;
            width: 100%;
            font-size: 15px;
        }
       
        .cell {
            display: block;
            margin: 24px auto;
            padding: 18px;
            box-sizing: border-box;
            font-size: 14px;
            color: #585858;
            background: white;
            border-radius: 4px;
            border-collapse: separate;
            border-spacing: 10px 5px;
        }
        .tit {
            font-size: 16px;
            text-align: center;
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 10px;
            margin-top: 0;
        }
        .bb {
            border-bottom: 1px solid #eaeaea;
            padding-bottom: 16px;
            color: #a7a4a4;
        }
        a {
            color: #2196F3;
            text-decoration: none;
        }
    </style>
    　
</head>

<body style="margin: 0 auto 24px; padding: 0;">
    <table border="0" cellpadding="0" cellspacing="0" width="600px" style="margin: 0 auto;position: relative;">
        <tr style="position:relative;display:block;">
            <td>
                <img border="0" width="600" src="./assets/fahuo.jpg" style="display:block;outline:none;text-decoration:none;border:none;">
            </td>
        </tr>
        <tr style="background: #f3f4f5;">
            <td class="cell" width="544">
                <p style="font-size: 18px;margin-bottom: 0px;">尊敬的 '.$nickname.' 您好！</p>
                <p class="bb"> 感谢您在Apple Employee Choice购物！</p>
                <p style="line-height: 2;">
                    您于 <a href="#">'.date('Y-m-d',$order['create_time']).'</a> 日提交的订单 <a href="#">'.$order['order_sn'].'</a> 已出库发货，正在配送中，请耐心等待。物流单号：<a href="http://www.sf-express.com/cn/sc/">'.$data['invoice_no'].'</a> （顺丰快递）。
                    <br/>
                    平台不会以订单无效等为由主动要求您提供银行卡信息操作退款，谨防诈骗！感谢您对我们的支持。
                </p>
            </td>
        </tr>
        <tr style="background: #e2e2e2;">
            <td width="540" style="text-align:center;display: block;margin: 16px auto;line-height: 4;font-size: 14px;">
                温馨提示：如果您有任何售后问题，请您联系经销商，电话：010-54353566
            </td>
        </tr>
    </table>
</body>

</html>';
    
    
                    sendEmail($users['email'], '发货通知',$content);
                    //"尊敬的{$nickname}用户，您的订单{$order['order_sn']}已发货，收货人{$order['consignee']}，请您及时查收");
                    
                    
                    
                    
                }
        
        
        
            }
            
            
    
            $order['shipping_status']=$updata['shipping_status'];
            
            $seller_user_id = Db::name('seller')->where('seller_id',SELLER_ID)->value('user_id');
    
            $rt = $this->orderActionLog($order,'经销商操作订单发货',$data['note'],$seller_user_id,1);//操作日志
            Db::commit();
            return ['code'=>$rt,'msg'=>''];
        }catch (Exception $e){
            
            Db::rollback();
            return ['code'=>0,'msg'=>$e->getMessage()];
        }
    }

    public function getSellerAddress($seller_address_id){
        $sellerAddress = Db::name('sellerAddress')->where('seller_address_id',$seller_address_id)->find();
        return $this->getRegionFull($sellerAddress['province_id'],$sellerAddress['city_id'],$sellerAddress['district_id'],$sellerAddress['town_id'],$sellerAddress['address']);
    }

    public function getRegionFull($provienceId,$cityId,$districtId,$townId,$detail=null){

        $provience = Db::name('region')->where('id',$provienceId)->field('name')->find();
        $city = Db::name('region')->where('id',$cityId)->field('name')->find();
        $district = Db::name('region')->where('id',$districtId)->field('name')->find();
        $town = Db::name('region')->where('id',$townId)->field('name')->find();

        $address = '';
        if($provience)$address.=$provience['name'];
        if($city)$address .=$city['name'];
        if($district)$address.=$district['name'];
        if($town)$address.=$town['name'];
        if($detail)$address.=$detail;

        return $address;
    }
    /**
     * 修改订单发货信息
     * @param array $data
     * @param array $order
     * @param string $store_id
     * @return bool|mixed
     */
    public function updateOrderShipping($data=[],$order=[],$store_id=''){
        $updata['shipping_code'] = $data['shipping_code'];
        $updata['shipping_name'] = $data['shipping_name'];
        M('order')->where(['order_id'=>$data['order_id']])->save($updata); //改变物流信息
        $updata['invoice_no'] = $data['invoice_no'];
        $delivery_res = M('delivery_doc')->where(['order_id'=>$data['order_id']])->save($updata);  //改变售后的信息
        if ($delivery_res){
//            $seller_id = session('seller_id');
            return $this->orderActionLog($order,'订单修改发货信息',$data['note'],SELLER_ID1);//操作日志
        }else{
            return false;
        }

    }

    /**
     * 获取地区名字
     * @param int $p
     * @param int $c
     * @param int $d
     * @return string
     */
    public function getAddressName($p=0,$c=0,$d=0){
        $p = M('region')->where(array('id'=>$p))->field('name')->find();
        $c = M('region')->where(array('id'=>$c))->field('name')->find();
        $d = M('region')->where(array('id'=>$d))->field('name')->find();
        return $p['name'].','.$c['name'].','.$d['name'].',';
    }

    /**
     * 删除订单
     *
     */
    function delOrder($order_id,$store_id){
        $select_year = getTabByOrderId($order_id);
    	$a = M('order'.$select_year)->where(array('order_id'=>$order_id,'store_id'=>$store_id))->update(['deleted'=>1]);
    	//$b = M('order_goods')->where(array('order_id'=>$order_id,'store_id'=>$store_id))->delete();
    	return $a;
    }

    /**
     * 获取店铺指定时间内已支付订单
     * @param $store_id
     * @param $statustime
     * @param $endtime
     * @return mixed
     */
    public function getOrderPaidAmount($store_id,$statustime='',$endtime=''){
        $where = ['store_id'=>$store_id,'order_status'=>['in','0,1,2,4'],'deleted'=>0];
        if ($statustime && $endtime){
            $where['add_time'] =['between',[$statustime,$endtime]];
        }
        $data['paid_order_sum'] = Db::name('order')->where($where)->count();
        $paid_order_money = Db::name('order')->where($where)->sum('total_amount-shipping_price-coupon_price-order_prom_amount');  //订单总价减去物流，减去各种优惠
        $data['paid_order_money'] = !empty($paid_order_money) ? $paid_order_money : 0;
        return $data;
    }
}