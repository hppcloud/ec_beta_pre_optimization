<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/5/26
 * Time: 22:01
 */
return[
    '__domain__'=>[
        'api_test'     => 'api',// 测试
        'api_release'  => 'api',// 预发布
        'api'          => 'api',// 正式（生产）
        // 泛域名规则建议在最后定义
//        '*.user'    =>  'user',
//        '*'         => 'book',
    ],
    '__pattern__' => [
        'name' => '\w+',
    ],
    // api版本v1路由
    'v1/:controller/:function'=>'api/v1.:controller/:function',// 有方法名时
    'v1/:controller'=>'api/v1.:controller/index',// 省略方法名时

    // api版本v2路由
    'v2/:controller/:function'=>'api/v2.:controller/:function',// 有方法名时
    'v2/:controller'=>'api/v2.:controller/index',// 省略方法名时
];
