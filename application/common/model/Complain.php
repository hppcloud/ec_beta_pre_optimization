<?php

namespace app\common\model;

use think\Db;
use think\Model;

class Complain extends Model
{
    public function goods()
    {
        return $this->hasOne('goods','goods_id','order_goods_id');
    }

    public function getComplainStateAttr($value,$data){
        $complain_state = array(1=>'待处理',2=>'对话中',3=>'待仲裁',4=>'已完成');
        return $complain_state[$data['complain_state']];
    }

    public function getComplainPicAttr($value,$data){
       return  unserialize($data['complain_pic']);
    }
}
