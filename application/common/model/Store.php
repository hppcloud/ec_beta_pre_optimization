<?php

namespace app\common\model;
use think\Db;
use think\Model;
class Store extends Model {

    public function shippingAreas(){
        return $this->hasMany('ShippingArea','store_id','store_id');
    }
    public function carts()
    {
        return $this->hasMany('cart','store_id','store_id');
    }
    public function getAddressRegionAttr($value, $data){
        $regions = Db::name('region')->where('id', 'IN', [$data['province_id'], $data['city_id'], $data['district']])->order('level desc')->select();
        $address = '';
        if($regions){
            foreach($regions as $regionKey=>$regionVal){
                $address = $regionVal['name'] . $address;
            }
        }
        return $address;
    }

    /**
     * 获取店铺分类的评分
     * @param $value
     * @param $data
     * @return array|false|\PDOStatement|string|Model
     */
    public function getStoreClassStatisticsAttr($value, $data)
    {
        $comparison_where = array('sc_id' => $data['sc_id'], 'deleted' => 0);
        $field = "avg(store_desccredit) as store_desccredit_avg,avg(store_servicecredit) as store_servicecredit_avg,avg(store_deliverycredit) as store_deliverycredit_avg";
        $statistics = Db::name('store')->field($field)->where($comparison_where)->cache('true')->find();
        if($statistics && $statistics['store_desccredit_avg']>0 && $statistics['store_servicecredit_avg']>0 && $statistics['store_deliverycredit_avg']>0){
            $statistics['store_desccredit_match'] = ($data['store_desccredit'] - $statistics['store_desccredit_avg']) / $statistics['store_desccredit_avg'] * 100;
            $statistics['store_servicecredit_match'] = ($data['store_servicecredit'] - $statistics['store_servicecredit_avg']) / $statistics['store_servicecredit_avg'] * 100;
            $statistics['store_deliverycredit_match'] = ($data['store_deliverycredit'] - $statistics['store_deliverycredit_avg']) / $statistics['store_deliverycredit_avg'] * 100;
        }else{
            $statistics['store_desccredit_match'] = 100;
            $statistics['store_servicecredit_match'] = 100;
            $statistics['store_deliverycredit_match'] = 100;
        }
        return $statistics;
    }

    public function storeGoodsClass()
    {
        return $this->hasMany('StoreGoodsClass', 'store_id', 'store_id')->cache(true);
    }

    public function storeGoodsClassTopParent()
    {
        return $this->hasMany('StoreGoodsClass', 'store_id', 'store_id')->where(['parent_id' => 0])->cache(true);
    }

    public function province()
    {
        return $this->hasOne('region', 'id', 'province_id')->cache(true);
    }
    public function city()
    {
        return $this->hasOne('region', 'id', 'city_id')->cache(true);
    }
    public function district()
    {
        return $this->hasOne('region', 'id', 'district')->cache(true);
    }

    public function goods()
    {
        return $this->hasMany('goods','store_id','store_id');
    }

    /**
     * 获取店铺商品总数
     * @param $value
     * @param $data
     * @return int|string
     */
    public function getGoodsCountAttr($value, $data){
        return Db::name('goods')->where(['store_id'=>$data['store_id']])->count();
    }

    public function getTotalCreditAttr($value, $data)
    {
        return ($data['store_desccredit'] + $data['store_servicecredit'] + $data['store_servicecredit']) / 3;
    }

    /**
     * 根据邮箱后缀来获取门店
     */
    static public function getByEmail($email,$isAll=false,$isCheck=false){

        $emailPostfix = explode('@',$email)[1];
        $where = "find_in_set('$emailPostfix',store_email)";
        if($isCheck){
            $where .= " and validate = 1 and store_enabled = 1";
        }

        $storeArr =  self::where($where)->select();

        if($isAll){
            return $storeArr;
        }

        if(count($storeArr)){
            return $storeArr[0];
        }
        return null;
    }

    /**
     * 绑定的门店
     * @param $account 查询的账号
     * @return array|false|\PDOStatement|string|Model
     */
    static function getBindStore($account){
        $field = is_numeric($account)?'u.mobile':'u.email';
        return self::field('s.*')
            ->alias('s')
            ->join('UserStore us','us.store_id = s.store_id')
            ->join('Users u','u.user_id = us.user_id')
            ->where([$field => $account])
            ->find();
    }

    /**
     * 检查门店
     */
    static function check($store){

        if($store['validate']!=1){
            api(1021,'你绑定的店铺未审核或审核未通过!');
        }

        if ($store['store_enabled']!=1) {
            api(1022, '你绑定的店铺已经关闭!');
        }
    }
}
