<?php
namespace app\common\model;

use think\Model;
use think\Db;

/**
 * @package Home\Model
 */
class StoreCollect extends Model
{

    public function store()
    {
        return $this->hasOne('store','store_id','store_id');
    }
}