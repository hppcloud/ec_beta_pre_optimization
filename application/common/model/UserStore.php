<?php
/**
 * Created by PhpStorm.
 * User: chen
 * Date: 2018/5/26
 * Time: 19:06
 */

namespace app\common\model;


use think\Model;

class UserStore extends Model
{
    static function getStore($user_id){
        return self::field('s.*')
            ->alias('u')
            ->join('Store s','u.store_id=s.store_id')
            ->where(['u.user_id'=>$user_id])
            ->find();
    }
}