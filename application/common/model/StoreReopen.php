<?php
namespace app\common\model;

use think\Model;

class StoreReopen extends Model
{
    public function getReopenStateAttr($value, $data){
        switch ($data['re_state']){
            case -1 : return '审核失败'; break;
            case 0  : return '未上传凭证'; break;
            case 1  : return '审核中'; break;
            case 2  : return '审核通过'; break;
        }
    }
}
