<?php

namespace app\common\model;

use app\common\logic\GoodsLogic;
use think\Db;
use think\Model;

class SpecGoodsPrice extends Model
{

    public function promGoods()
    {
        return $this->hasOne('PromGoods', 'id', 'prom_id')->cache(true,10);
    }

    public function goods()
    {
        return $this->hasOne('Goods', 'goods_id', 'goods_id')->cache(true,10);
    }


    public function getPromDescAttr($value, $data)
    {
        //1抢购2团购3优惠促销4预售5虚拟(5其实没用)6拼团
        switch($data['prom_type']) {
            case 1:
                $desc = '普通';
                break;
            case 2:
                $desc = '团购';
                break;
            case 3:
                $desc = '促销';
                break;
            case 4:
                $desc = '预售';
                break;
            case 6:
                $desc = '拼团';
                break;
            default:
                $desc = '普通';
        }
        return $desc;
    }

    /**
     * 价格组合
     */
    static function priceArr($goodsId,$rate=1,$basePrice=0){

        $logic = new GoodsLogic();

         # 201903 添加 是否共享库存

        $isShare = M('goods')->where('goods_id',$goodsId)->getField('is_share_stock');

        $priceList = self::field('key,price,store_count,is_share')
            ->where('goods_id',$goodsId)
            ->select();
        $priceArr = [];
       
        foreach ($priceList as $v){
            $priceArr[$v['key']]=[
                //增加四舍五入取整,属性未折扣价格
                'discount'=>$rate*10,
                'attr_price'=>$v['price'],
                'orig_price'=>$basePrice+$v['price'],
                'sell_price'=>round(($basePrice+$v['price'])*$rate),
                # 201903 修改
                'store_count'=>$isShare==1?$logic->getAlikeGoodsStock($goodsId,$v['key']):$v['store_count']
            ];
        }
        return $priceArr;
    }
}
