<?php

namespace app\common\model;

use app\common\logic\CartLogic;
use think\Db;
use think\Model;
use app\common\logic\FlashSaleLogic;
use app\common\logic\GroupBuyLogic;
use think\Request;

class Users extends Model
{
    //自定义初始化
    protected static function init()
    {
        //TODO:自定义的初始化
    }

    /**
     * 用户下线分销金额
     * @param $value
     * @param $data
     * @return float|int
     */
    public function getRebateMoneyAttr($value, $data){
      $sum_money = DB::name('rebate_log')->where(['status' => 3,'buy_user_id'=>$data['user_id']])->sum('money');
	   $rebate_money = empty($sum_money) ? (float)0 : $sum_money;
	   return  $rebate_money;
    }

    /**
     * 通过id获取用户
     */
    static function getUserById($userId){

        // 用户
        $user = self::get(['user_id'=>$userId]);
        if(!$user){
            return null;
        }

        // 店铺
        $store = Store::getBindStore($user['email']?$user['email']:$user['mobile']);
        if(!$store){
            return ['user'=>$user,'store'=>$store,'seller'=>null];
        }

        // 经销商
        $seller = Seller::get($store['seller_id']);
        return ['user'=>$user,'store'=>$store,'seller'=>$seller];

    }

    /**
     *  通过邮箱获取用户
     */
    static function getUserByEmail($email){

        // 店铺
        $store = Store::getByEmail($email);
        if(!$store){
            api(1020,'您的企业还未加入Employee Choice，点击获取加入方式');
        }

        if(!$store['store_enabled']){
            api(1021,'店铺已经关闭!');
        }

        // 经销商
        $seller = Seller::get($store['seller_id']);
        if(!$seller){
            api(1022,'该店铺未分配经销商！');
        }

        // 用户
        $user = self::get(['email'=>$email]);
        if(!$user){
            $user = self::create([
                'email'=>$email,
                'password'=>md5(C("AUTH_CODE").get_rand_str(8,0,1)),
                'reg_time'=>time(),
                'last_login'=>time(),
                'last_ip'=>$_SERVER["REMOTE_ADDR"]
            ]);
        }

        $userStore = UserStore::get(['user_id'=>$user['user_id']]);
        if(!$userStore){
            // 创建用户和店铺的关系
             UserStore::create([
                'user_id'=>$user['user_id'],
                'store_id'=>$store['store_id'],
                'store_name'=>$store['store_name'],
                'true_name'=>'',
                'qq'=>'',
                'mobile'=>'',
                'store_img'=>$store['store_logo'],
                'store_time'=>$store['store_time']?$store['store_time']:0,
            ]);
            // 注册就赠送优惠券
            CouponList::presentByRegister($store['store_id'],$seller['seller_id'],$user['user_id']);
        }

        // 检查是否有优惠券，没有就赠送一个
        $isHasRegCoupon = Db::name('CouponList')->where(['uid'=>$user['user_id'],'type'=>4,'store_id'=>$store['store_id']])->find();

        //没有记录才去检查是不是要送
        if(!$isHasRegCoupon) {
            $filter = [
                'type' => 4,
                //'use_type'=>3,//指定店铺通用 or 分类使用
                'seller_id' => $seller['seller_id'],
                'send_start_time' => ['>=', 1536118166],//先去掉时间限制  大于2018-09-05 0:0:0，这样就不需要删除之前的数据了
                'send_end_time' => ['>=', time()],//先去掉时间限制
            ];
            $isCoupon = Db::name('coupon')->where($filter)->order('id desc')->find();

            //如果不存在就自动生成一个给他
            if ($isCoupon) {
                //看优惠券和店铺绑定的表是否有记录
                $storeBindCoupon = Db::name('CouponStore')->where(['cid' => $isCoupon['id'], 'store_id' => $store['store_id']])->find();
                if ($storeBindCoupon && $storeBindCoupon['cid']) {
                    CouponList::presentByRegister($store['store_id'], $seller['seller_id'], $user['user_id']);
                }
            }
        }

       return [
            'user'=>$user,
            'store'=>$store?$store:null,
            'seller'=>$seller?$seller:null
        ];
    }

    /**
     * 登录
     */
    static function login(Users $user){

        $user->token = md5(time().mt_rand(1, 999999999));
        $user->last_login = time();
        $rt= $user->save();

        $user = $user->toArray();
        unset($user['password']);

        $cartLogic = new CartLogic();
        $cartLogic->setUserId($user->user_id);
        $cartLogic->setUniqueId(I("unique_id"));
        $cartLogic->doUserLoginHandle();  // 用户登录后 需要对购物车 一些操作

        if($rt){
            return $user;
        }else{
            return false;
        }
    }
}
