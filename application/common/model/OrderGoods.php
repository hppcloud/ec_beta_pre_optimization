<?php

namespace app\common\model;
use think\Model;
class OrderGoods extends Model {

    protected $autoWriteTimestamp = true;
    protected $table='';

    //自定义初始化
    protected function initialize()
    {
        parent::initialize();
        $select_year = select_year(); // 查询 三个月,今年内,2016年等....订单
        $prefix = C('database.prefix');  //获取表前缀
        $this->table = $prefix.'order_goods'.$select_year;

        self::beforeInsert(function ($model) {
            $model->status_tip = 0;
            $model->status_tip = '无退货退款';
        });
    }

    public function goods()
    {
        return $this->hasOne('goods','goods_id','goods_id');
    }

    public function returnGoods()
    {
        return $this->hasOne('ReturnGoods','rec_id','rec_id');
    }

    public function getFinalPriceAttr($value, $data){
       if($data['prom_type'] == 4){
           return $data['goods_price'];
       }else{
           return $value;
       }
    }

    /**
     * 获取预定订单商品的价格
     */
    static function getReservePrice($reserveOrderSn){
        $list = self::field('og.final_price,og.goods_num')
            ->alias('og')
            ->join('Order o','o.order_id=og.order_id')
            ->where(['o.order_sn'=>$reserveOrderSn])
            ->select();
        if(!$list){
            api(-1,'该预定订单不存在！');
        }

        $money = 0;
        foreach ($list as $v){
            $money = $money+($v['final_price']*$v['goods_num']);
        }
        return $money;
    }

    /**
     * 预处理，商品退货退款状态
     */
    public function getStatusAttr($value,$data){

        if($data['status_tip']!=''){
            return $value;
        }

        // status_tip为空说明是老数据，为了兼容再写一遍数据
        $returnGoods = ReturnGoods::get(['rec_id'=>$data['rec_id']]);
        if(!$returnGoods){
            $this->status = 0;
            $this->status_tip = '无退货退款';
            $this->save();
            return $this->status;
        }

        if(in_array($returnGoods->status,[0,1,2,3,4])){
            $this->status = $returnGoods->type?2:1;
            $this->status_tip = $returnGoods->type?'退货退款中':'退款中';
            $this->save();
            return $this->status;
        }

        if(in_array($returnGoods->status,[6])){
            $this->status = $returnGoods->type?4:3;
            $this->status_tip = $returnGoods->type?'已退货退款':'已退款';
            $this->save();
            return $this->status;
        }

        if(in_array($returnGoods->status,[-1,5,7])){
            $this->status = $returnGoods->type?6:5;
            $this->status_tip = $returnGoods->type?'退货退款失败':'退款失败';
            $this->save();
            return $this->status;
        }

        return 0;
    }

//    /**
//     * 获取商品退货退款状态
//     */
//    public function getStatus(){
//
//        if($this->status_tip!=''){
//            return $this->status;
//        }
//
//        // status_tip为空说明是老数据，为了兼容再写一遍数据
//        $returnGoods = ReturnGoods::get(['rec_id'=>$this->rec_id]);
//        if(!$returnGoods){
//            $this->status = 0;
//            $this->status_tip = '无退货退款';
//            $this->save();
//            return $this->status;
//        }
//
//        if(in_array($returnGoods->status,[0,1,2,3,4])){
//            $this->status = $returnGoods->type?2:1;
//            $this->status_tip = $returnGoods->type?'退货退款中':'退款中';
//            $this->save();
//            return $this->status;
//        }
//
//        if(in_array($returnGoods->status,[6])){
//            $this->status = $returnGoods->type?4:3;
//            $this->status_tip = $returnGoods->type?'已退货退款':'已退款';
//            $this->save();
//            return $this->status;
//        }
//
//        if(in_array($returnGoods->status,[-1,5,7])){
//            $this->status = $returnGoods->type?6:5;
//            $this->status_tip = $returnGoods->type?'退货退款失败':'退款失败';
//            $this->save();
//            return $this->status;
//        }
//
//        return 0;
//    }

    /**
     * 是否可以退款
     */
    function isAbleRefund($order){
        if($order['status'] == 1 && in_array($this->status,[0,5])){
            return true;
        }
        return false;
    }

    /**
     * 是否可以退货退款
     */
    function isAbleReturnGoods($order){
        if(in_array($order['status'],[2,3])&& in_array($this->status,[0,6])){
            return true;
        }
        return false;
    }

    /**
     * 申请退货退款
     */
    function doReturn($type){
        $this->status = $type?2:1;
        $this->status_tip = $type?'退货退款中':'退款中';
        return $this->save();
    }

    /**
     * 用户取消退货
     */
    function cancelReturn(){
        $this->status = 0;
        $this->status_tip = '无退货退款';
        return $this->save();
    }

    /**
     * 完成退货或退款
     * @type 0仅退款，1退货退款，（这个值从return_goods表中取）
     * @return false|int
     */
    function finishReturn($type){
        $this->status = $type?4:3;
        $this->status_tip = $type?'已退货退款':'已退款';
        return $this->save();
    }

    /**
     * 拒绝退货或退款
     * @type 0仅退款，1退货退款，（这个值从return_goods表中取）
     * @return false|int
     */
    function refuseReturn($type){
        $this->status = $type?5:6;
        $this->status_tip = $type?'退货退款失败':'退款失败';
        return $this->save();
    }
}
