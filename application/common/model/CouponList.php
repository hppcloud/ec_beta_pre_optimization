<?php
namespace app\common\model;

use app\common\logic\Store as StoreLogic;
use think\Model;

class CouponList extends Model
{
    protected $error;

    public function coupon()
    {
        return $this->hasOne('coupon','id','cid')->bind('name');
    }

    /**
     * 通过注册赠送优惠券
     */
    static function presentByRegister($storeId,$sellerId,$userId){

        $storeRegCouponId = StoreLogic::getRegSendCoupon($storeId,$sellerId);//先获取，如果没有的话就临时创建一个

        trace('赠送优惠券'.$storeRegCouponId);
        if(!$storeRegCouponId){
            return false;
        }
        
        $coupon = Coupon::get($storeRegCouponId);

        if(!$coupon){
            return false;
        }
        
        $userCoupon =  self::create([
            'uid'=>$userId,
            'cid'=>$coupon['id'],
            'store_id'=>$storeId,
            'money'=>$coupon['money'],
            'type'=>$coupon['type'],
            'send_time'=>time(),
        ]);

        if($userCoupon){
            $coupon['send_num'] = $coupon['send_num'] +1;
            $coupon->save();
        }

        return $userCoupon;
    }

    /**
     * 回收赠送优惠券
     */
    static function presentByRecycle($userId,$storeId,$sellerId,$orderId,$money){

        $couponId = StoreLogic::getRecoverSendCoupon($storeId, $sellerId);//获取门店设置的回收优惠券规则
        $coupon = Coupon::get($couponId);
        if(!$coupon){
            api(-1,'平台没有配置回收优惠券规则！');
        }
        if(self::get(['get_order_id'=>$orderId])){
            api(-1,'该订单已经赠送过了！');
        };

        $userCoupon =  self::create([
            'uid'=>$userId,
            'cid'=>$coupon['id'],
            'store_id'=>$storeId,
            'type'=>$coupon['type'],
            'get_order_id'=>$orderId,
            'money'=>round($money+$money*$coupon['percent']/100),// 四舍五入取整
            'send_time'=>time(),
        ]);

        if($userCoupon){
            $coupon['send_num'] = $coupon['send_num'] +1;
            $coupon->save();
        }

        return $userCoupon;
    }

    /**
     * 优惠券抵扣
     */
    static function deduction($userId,$storeId,$userCouponId,$extend){
        $where = [
            'cl.id'=>$userCouponId,
            'cl.uid'=>$userId,
            'cl.store_id'=>$storeId,
            'cl.status'=>0,
            'c.status'=>1,
            'use_start_time'=>['<=',time()],
            'use_end_time'=>['>=',time()],
            'deleted'=>0
        ];
        $coupon = self::field('cl.money,c.condition')
            ->alias('cl')
            ->join('Coupon c','cl.cid=c.id')
            ->where($where)
            ->find();

        if(!$coupon){
            api(-1,'你没有该优惠券或不可用！');
        }
        if($coupon['condition']>$extend['order_amount']){
            api(-1,'该优惠券未达到满额条件！');
        }
        if($coupon['money']<=0){
            api(-1,'该优惠券金额异常！');
        }

        if($coupon['money']>$extend['order_amount']){
            api(-1,'优惠券金额不能大于订单金额，请不要灌水！');
        }

        return $coupon['money'];
    }

    /**
     * 确定使用优惠券
     */
    static function confirmUse($userId,$userCouponId,$order){

        $couponList = self::get(['id'=>$userCouponId,'uid'=>$userId]);
        if(!$couponList){
            return false;
        }
        $couponList['order_id']= $order['order_id'];
        $couponList['use_time']= time();
        $couponList['status']= 1;
        if($couponList->save()){
            trace(json_encode(['order_id'=>$order['order_id'],'use_time'=>time(),'status'=>1]));
            $coupon = Coupon::get(['id'=>$couponList['cid']]);
            if(!$coupon){
                return false;
            }
            $coupon['use_num'] = $coupon['use_num']+1;
            if($coupon->save()){
               return false;
            }
            return true;
        }
        return false;
    }

    /**
     * 发放 - 来自平台的优惠券
     */
    static function sendFromPlatform($storeId,$userId){

        // 获取平台发放中的优惠券
        $time = time();
        $where = [
            'store_id'=>0,
            'type'=>4,
            'status'=>1,
            'seller_id'=>0,
            'send_start_time'=>['<=',$time],
            'send_end_time'=>['>=',$time],
        ];
        $couponArr = Coupon::all($where);
        foreach ($couponArr as $coupon){
            $cl = self::get(['uid'=>$userId,'cid'=>$coupon['id']]);
            if($cl){
                continue;
            }
            // 没有则发送
            self::create([
                'cid'=>$coupon['id'],
                'type'=>$coupon['type'],
                'uid'=>$userId,
                'money'=>$coupon['money'],
                'send_time'=>$time,
                'store_id'=>$storeId,
            ]);
            $coupon['send_num'] = $coupon['send_num']+1;
            $coupon->save();
        }
    }

    /**
     * 退回已使用的优惠券
     */
    static function drawBack($orderId){
        $couponList = self::get(['order_id'=>$orderId,'status'=>1]);
        $coupon = Coupon::get($couponList['cid']);
        //为1就意味着只能用一次，不退还了
        if($coupon['is_refund']!==1){
            $rt = self::update(['status'=>0,'use_time'=>0],['order_id'=>$orderId,'status'=>1]);
            if($rt!==false){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
        
    }
}
