<?php

namespace app\common\model;
use think\Model;
class GroupBuy extends Model {
    public function goods(){
        return $this->hasOne('goods','goods_id','goods_id');
    }
    public function specGoodsPrice(){
        return $this->hasOne('specGoodsPrice','item_id','item_id');
    }
    //剩余团购库存
    public function getStoreCountAttr($value, $data)
    {
        return $data['goods_num'] - $data['buy_num'];
    }
    //状态描述
    public function getStatusDescAttr($value, $data)
    {
        $status = array('审核中', '正常', '审核失败', '管理员关闭');
        if ($data['status'] != 1) {
            return $status[$data['status']];
        } else {
            if (time() < $data['start_time']) {
                return '未开始';
            } else if (time() > $data['start_time'] && time() < $data['end_time']) {
                return '进行中';
            } else {
                return '已结束';
            }
        }
    }
}
