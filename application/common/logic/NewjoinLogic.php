<?php

namespace app\common\logic;

class NewjoinLogic
{
    /**
     * 上传营业证书
     * @param bool $force 是否强制上传
     * @return array
     */
    public function uploadBusinessCertificate($force = false)
    {
        $img = '';
        if ($_FILES['business_licence_cert']['tmp_name']) {
            $file = request()->file('business_licence_cert');
            $image_upload_limit_size = config('image_upload_limit_size');
            $validate = ['size' => $image_upload_limit_size, 'ext' => 'jpg,png,gif,jpeg'];
            $dir = UPLOAD_PATH.'store/cert/'.date('Y-m-d').'/';
            if (!($_exists = file_exists($dir))) {
                mkdir($dir);
            }
            $info = $file->rule(function ($file) {
                return  md5(mt_rand()); // 使用自定义的文件保存规则
            })->validate($validate)->move($dir, true);
            if (!$info) {
                return ['status' => -1, 'msg' => $file->getError()];
            }
            $img = '/' . $dir. $info->getFilename();
        }

        if ($force && !$img) {
            return ['status' => -1, 'msg' => '没有上传营业执照'];
        }

        return ['status' => 1, 'msg' => '上传成功', 'result' => $img];
    }
}