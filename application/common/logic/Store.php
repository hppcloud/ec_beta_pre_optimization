<?php
namespace app\common\logic;
use think\Db;
use think\Exception;

/**
 *
 * Class orderLogic
 * @package common\Logic
 */
class Store
{
    private $store;
    protected $seller_id=null;

    public function __construct($store_id=null)
    {
        $this->store = \app\common\model\Store::get($store_id);
    }
    /**
     * 更新店铺评分
     */
    public function updateStoreScore()
    {
        $order_comment = Db::name('order_comment')->field("AVG(describe_score) AS desc_credit,AVG(seller_score) AS service_credit,AVG(logistics_score) AS delivery_credit")
            ->where(['store_id'=>$this->store['store_id']])->find();
        if($order_comment){
            $this->store['store_desccredit'] = $order_comment['desc_credit'];
            $this->store['store_servicecredit'] = $order_comment['service_credit'];
            $this->store['store_deliverycredit'] = $order_comment['delivery_credit'];
            $this->store->save();
        }
    }

    /**
     * 获取店铺设置的注册赠送优惠券，如果不存在则自动创建一个。
     */

    public static function getRegSendCoupon($storeid,$sellerid){
        
        
        $filter = [
            'c.type'=>4,
            //'use_type'=>3,//指定店铺通用 or 分类使用
            'c.seller_id'=>$sellerid,
            'c.send_start_time'=>['<=',time()],//先去掉时间限制  大于2018-09-05 0:0:0，这样就不需要删除之前的数据了
            'c.send_end_time'=>['>=',time()],//先去掉时间限制
            's.store_id'=>$storeid
        ];


        $coupon = Db::name('coupon')
            ->alias('c')
            ->join('tp_coupon_store s','c.id = s.cid')
            ->where($filter)
            ->order('c.id desc')
            ->field('c.*,s.store_id,s.seller_id,s.cid')
            ->find();




        //如果不存在就自动生成一个给他
        if(!$coupon){
           
            return false;
            
        }else{
            //如果不在发放日期，则不需要发放
            if ($coupon['send_start_time'] > time() || $coupon['send_end_time'] < time()) {
                return false;
            }
            if($coupon['send_num']<$coupon['createnum']||$coupon['createnum']==0){
                return $coupon['id'];
            }else{
                return false;
            }

//            //看优惠券和店铺绑定的表是否有记录
//            $storeBindCoupon = Db::name('CouponStore')->where(['cid'=>$isHas['id'],'store_id'=>$storeid])->find();
//            if($storeBindCoupon&&$storeBindCoupon['cid']){
//
//                $coupon = Db::name('Coupon')->where('id',$storeBindCoupon['cid'])->find();
//                //只有数量够、或者无限才送
//
//
//            }else{
//                return false;
//            }
        }
       
    }

    public static function getRecoverSendCoupon($storeid,$sellerid){

        $filter = [
            's.store_id'=>$storeid,
            'type'=>5,
            //'use_type'=>3,//指定店铺通用,不应该加这个，因为后台现在支持分类控制
            'c.seller_id'=>$sellerid,
            'send_start_time' => ['<=', time()],//先去掉时间限制  大于2018-08-12 0:0:0，这样就不需要删除之前的数据了
            'send_end_time'=>['>=',time()],//先去掉时间限制
        ];

        $coupon = Db::name('coupon')
            ->alias('c')
            ->join('tp_coupon_store s', 'c.id = s.cid')
            ->where($filter)
            ->order('c.id desc')
            ->field('c.*,s.store_id,s.seller_id,s.cid')
            ->find();

        // 如果经销商店铺没有设置回收优惠券，则默认系统规则发放（返回0）
        if (!$coupon) {
            return 1;// 优惠券模板id=1，对应优惠券表id为1的数据
        }
        //如果不在发放日期，则不需要发放
        if ($coupon['send_start_time'] > time() || $coupon['send_end_time'] < time()) {
            return 1;// 优惠券模板id=1，对应优惠券表id为1的数据
        }
        // 按经销商店铺设置的回收优惠券规则赠送
        if ($coupon['send_num'] < $coupon['createnum'] || $coupon['createnum'] == 0) {
            return $coupon['id'];
        } else {
            //api(-1,'回收优惠券后台配置异常！');
            //如果经销商设置的规则达到上限，则走平台回收券规则
            return 1;// 优惠券模板id=1，对应优惠券表id为1的数据
        }

    }


    /**
     * 更新指定店铺的注册赠送相关配置
     * @param $storeid
     * @param int $reg_send_coupon 是否开启 1为开启0 为关闭。默认关闭
     * @param int $reg_send_coupon_val 赠送的面额
     * @return array
     */
    public static function upRegSendCoupon($storeid,$sellerid,$reg_send_coupon=0,$reg_send_coupon_val=0,$reg_send_coupon_limit=0){

        
        if($reg_send_coupon_val<0){
            return ['code'=>0,'msg'=>'reg_send_coupon_val参数错误','data'=>['cid'=>null,'action'=>null]];
        }

//        Db::startTrans();

        try{


            $r = Db::name('store')->where('store_id',$storeid)->update(['reg_send_coupon'=>$reg_send_coupon]);//先修改开关

            if($r===false){
                return ['code'=>0,'msg'=>'修改赠送优惠券开关配置失败','data'=>['cid'=>null,'action'=>null]];
            }

            $filter = [
                'store_id'=>$storeid,
                'type'=>4,
                'use_type'=>3,//指定店铺通用
                'seller_id'=>$sellerid
            ];
            $isHas = Db::name('coupon')->where($filter)->find();
            
            if(!$isHas){
                
                try{
                    $filter['createnum'] = 0;
                    $filter['money'] = $reg_send_coupon_val;
                    $filter['condition'] = $reg_send_coupon_limit;//限制金额
                    $filter['name'] = '新会员注册赠送优惠券';
                    $filter['coupon_info'] = '店铺:'.$storeid.'的注册赠送优惠券';
                    $filter['add_time'] = time();
                    $filter['send_start_time'] = time();
                    $filter['send_end_time'] = time()+3600*24*365*10;//十年后停止赠送
                    $filter['use_start_time'] = time();
                    $filter['use_end_time'] = time()+3600*24*365*10;//十年后停止使用
    
                   
                    $nid = Db::name('coupon')->insertGetId($filter);
    
                    if($nid){
                        Db::name('store')->where('store_id',$storeid)->update(['reg_coupon_id'=>$nid]);//关联起来
                        return ['code'=>1,'msg'=>'店铺新增注册赠送优惠券成功','data'=>['cid'=>$nid,'action'=>'add']];
                    }else{
                        return ['code'=>0,'msg'=>'店铺新增注册赠送优惠券失败','data'=>['cid'=>null,'action'=>'add']];
                    }
                }catch (Exception $e){
                    return ['code'=>0,'msg'=>'店铺新增注册赠送优惠券失败'.$e->getMessage(),'data'=>['cid'=>$isHas['id'],'action'=>'update']];
                }
               
            }else{
                $rt = $rt1 = false;
                $rt = Db::name('coupon')->where($filter)->update(['money'=>$reg_send_coupon_val,'condition'=>$reg_send_coupon_limit]);//修改价格
                if($isHas['id']){
                    $rt1 = Db::name('store')->where('store_id',$storeid)->update(['reg_coupon_id'=>$isHas['id']]);//关联起来
                }
                
                if($rt!==false && $rt1!==false){
                    return ['code'=>1,'msg'=>'更新成功','data'=>['cid'=>$isHas['id'],'action'=>'update']];
                }else{
                    return ['code'=>0,'msg'=>'更新失败','data'=>['cid'=>$isHas['id'],'action'=>'update']];
                }

            }
//            Db::commit();
        }catch (Exception $e){
            trace('更新店铺的注册赠送优惠券信息失败:'.$e->getMessage());
            return ['code'=>0,'msg'=>'操作失败：'.$e->getMessage(),'data'=>['cid'=>null,'action'=>null]];
//            Db::rollback();
        }



    }


    public static function upRecoverSendCoupon($storeid,$sellerid,$recover_send_coupon=0,$recover_coupon_percent=0,$recover_coupon_limit=0){

        if($recover_coupon_percent<0){
            return ['code'=>0,'msg'=>'recover_coupon_percent参数错误','data'=>['cid'=>null,'action'=>null]];
        }

//        Db::startTrans();

        try{


            $r = Db::name('store')->where('store_id',$storeid)->update(['recover_send_coupon'=>$recover_send_coupon]);//先修改开关

            if($r===false){
                return ['code'=>0,'msg'=>'修改回收赠送优惠券开关配置失败','data'=>['cid'=>null,'action'=>null]];
            }

            $filter = [
                'store_id'=>$storeid,
                'type'=>5,
                'use_type'=>3,//指定店铺通用
                'seller_id'=>$sellerid
            ];
            $isHas = Db::name('coupon')->where($filter)->find();



            if(!$isHas){
                $filter['createnum'] = 0;
                $filter['percent'] = $recover_coupon_percent;
                $filter['condition'] = $recover_coupon_limit;
                $filter['name'] = '回收赠送优惠券';
                $filter['coupon_info'] = '店铺:'.$storeid.'的回收赠送优惠券';
                $filter['add_time'] = time();
                $filter['send_start_time'] = time();
                $filter['send_end_time'] = time()+3600*24*365*10;//十年后停止赠送
                $filter['use_start_time'] = time();
                $filter['use_end_time'] = time()+3600*24*365*10;//十年后停止使用
                $nid = Db::name('coupon')->insertGetId($filter);

                if($nid){
                    Db::name('store')->where('store_id',$storeid)->update(['recover_coupon_id'=>$nid]);//关联起来
                    return ['code'=>1,'msg'=>'更新成功','data'=>['cid'=>$nid,'action'=>'add']];
                }else{
                    return ['code'=>0,'msg'=>'更新成功','data'=>['cid'=>null,'action'=>'add']];
                }
            }else{
                $rt = Db::name('coupon')->where($filter)->update(['percent'=>$recover_coupon_percent,'condition'=>$recover_coupon_limit]);//修改价格
    
                Db::name('store')->where('store_id',$storeid)->update(['recover_coupon_id'=>$isHas['id']]);//关联起来
                
                if($rt!==false){
                    return ['code'=>1,'msg'=>'更新成功','data'=>['cid'=>$isHas['id'],'action'=>'update']];
                }else{
                    return ['code'=>0,'msg'=>'更新失败','data'=>['cid'=>$isHas['id'],'action'=>'update']];
                }

            }
//            Db::commit();
        }catch (Exception $e){
            trace('更新店铺的回收赠送优惠券信息失败:'.$e->getMessage());
            return ['code'=>0,'msg'=>'操作失败：'.$e->getMessage(),'data'=>['cid'=>null,'action'=>null]];
//            Db::rollback();
        }



    }

}