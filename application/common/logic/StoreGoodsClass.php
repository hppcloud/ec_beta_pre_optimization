<?php
namespace app\common\logic;

use think\Model;

/**
 * 活动逻辑类
 */
class StoreGoodsClass extends Model
{
    /**
     * 获取店铺商品分类
     * @param $store_id
     * @param int $parent_id
     * @param array $result
     * @return array
     */
    public function getStoreGoodsClass($store_id, $parent_id = 0, &$result = array())
    {
        $store_goods_class_where = array(
            'store_id' => $store_id,
            'parent_id' => $parent_id,
        );
        $arr = M('store_goods_class')->where($store_goods_class_where)->order('cat_sort desc')->select();
        if (empty($arr)) {
            return array();
        }
        foreach ($arr as $cm) {
            $thisArr =& $result[];
            $cm['children'] = $this->getStoreGoodsClass($store_id, $cm['cat_id'], $thisArr);
            $thisArr = $cm;
        }
        return $result;
    }
}