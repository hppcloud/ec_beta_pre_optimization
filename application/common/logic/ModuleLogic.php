<?php

namespace app\common\logic;


class ModuleLogic
{
    /**
     * 所有模块
     * @var array
     */
    public $modules = [];

    /**
     * 可见模块
     * @var array
     */
    public $showModules = [];

    public function getModules($onlyShow = true)
    {
        if ($this->modules) {
            return $onlyShow ? $this->showModules : $this->modules;
        }

        $isShow = Saas::instance()->isBaseUser() ? 1 : 0;
        $modules = [
            [
                'name'  => 'admin', 'title' => '平台后台', 'show' => 1,
                'privilege' => [
                    'system'=>'系统设置','content'=>'内容管理','goods'=>'商品中心','member'=>'会员中心',
                    'order'=>'订单中心','marketing'=>'营销推广','tools'=>'插件工具','count'=>'统计报表',
                    'weixin'=>'微信管理','store'=>'店铺管理','distribut'=>'分销管理','maintenance'=>'运营','manage'=>'经销商管理'
                ],
            ],
            [
                'name'  => 'seller', 'title' => '商家后台', 'show' => 1,
                'privilege' => [
                    'goods'=>'商品管理','order'=>'订单管理','promtion'=>'促销管理','store'=>'店铺管理',
                    'service'=>'售后服务','charts'=>'数据报告','mesaage'=>'客服消息','seller'=>'账号管理',
                    'finance'=>'财务管理','distribut'=>'分销管理','maintenance'=>'运营'
                ]
            ],
            [
                'name'  => 'home', 'title' => 'PC端', 'show' => $isShow,
                'privilege' => ['basic' => '首页', 'goods' => '商品'],
            ],
            [
                'name'  => 'mobile', 'title' => '手机端','show' => $isShow,
                'privilege' => ['basic' => '基础功能'],
            ],
            [
                'name'  => 'api', 'title' => 'api接口', 'show' => $isShow,
                'privilege' => ['basic' => '基础功能'],
            ],
            [
                'name'  => 'shop', 'title' => '门店', 'show' => $isShow,
                'privilege' => ['basic' => '基础功能'],
            ],
        ];

        $this->modules = $modules;
        foreach ($modules as $key => $module) {
            if (!$module['show']) {
                unset($modules[$key]);
            }
        }
        $this->showModules = $modules;

        return $onlyShow ? $this->showModules : $this->modules;
    }

    public function getModule($moduleIdx, $onlyShow = true)
    {
        if (!self::isModuleExist($moduleIdx, $onlyShow)) {
            return [];
        }

        $modules = $this->getModules($onlyShow);
        return $modules[$moduleIdx];
    }

    public function isModuleExist($moduleIdx, $onlyShow = true)
    {
        return key_exists($moduleIdx, $this->getModules($onlyShow));
    }

    public function getPrivilege($moduleIdx, $onlyShow = true)
    {
        $modules = $this->getModules($onlyShow);
        return $modules[$moduleIdx]['privilege'];
    }
}