<?php
namespace app\common\logic;

use app\common\model\CouponList;
use think\Model;

/**
 * 活动逻辑类
 */
class ActivityLogic extends Model
{
 

    /**
     * 获取优惠券查询对象
     * @param int $queryType 0:count 1:select
     * @param type $user_id
     * @param int $type 查询类型 0:未使用，1:已使用，2:已过期
     * @param null $orderBy 排序类型，use_end_time、send_time,默认send_time
     * @param int  $belone 0:具体商家，1:自营, 2:所有商家
     * @param int $store_id
     * @param int $order_money
     * @return mixed
     */
    private function getCouponQuery($queryType, $user_id, $type = 0, $orderBy = null, $belone = 0,  $order_money = 0)
    {
        $where['l.uid'] = $user_id;
        $where['l.deleted'] = 0;
        $where['c.status'] = 1;

        //查询条件
        if (empty($type)) {
            // 可使用
            $where['c.use_end_time'] = array('gt', time());
            $where['c.status'] = 1;
            $where['l.status'] = 0;// 0未使用1已使用2已过期
        } elseif ($type == 1) {
            //已使用
            $where['l.order_id'] = array('gt', 0);
            $where['l.use_time'] = array('gt', 0);
            $where['l.status'] = 1;
        } elseif ($type == 2) {
            //已过期
            $where['c.use_end_time'] = array('lt', time());
            $where['l.status'] = array('neq', 1);
        }
        if ($orderBy == 'use_end_time') {
            //即将过期
            $order['c.use_end_time'] = 'asc';
        } elseif ($orderBy == 'send_time') {
            //最近到账
            $where['l.send_time'] = array('lt',time());
            $order['l.send_time'] = 'desc';
        } elseif (empty($orderBy)) {
            $order = array('l.send_time' => 'DESC', 'l.use_time');
        }
        $query = CouponList::field('l.id')
            ->alias('l')
            ->join('coupon c','l.cid = c.id')
            ->where($where);


        if ($queryType != 0) {
            $query = CouponList::field('l.*,c.name,c.use_type,c.money amount,c.use_start_time,c.use_end_time,c.condition,c.coupon_info')
                ->alias('l')
                ->join('coupon c','l.cid = c.id')
                ->where($where)
                ->order($order)
                ->paginate(100);

           // 如果用户优惠券金额为0，则修复数据
            foreach ($query as $v){
                if($v['money']==0&&$v['amount']!=0){
                    $v['money'] = $v['amount'];
                    CouponList::update(['money'=>$v['amount']],['id'=>$v['id']]);
                }
            }
        }

        return $query;


//        return CouponList::field('l.*,c.name,c.use_type,c.money,c.use_start_time,c.use_end_time,c.condition')
//            ->alias('l')
//            ->join('coupon c','l.cid = c.id')
//            ->where($where)
//            ->order($order)
//            ->paginate();
    }
    
    /**
     * 获取优惠券数目
     */
    public function getUserCouponNum($user_id, $type = 0, $orderBy = null, $belone = 0,  $order_money = 0)
    {
        $query = $this->getCouponQuery(0, $user_id, $type, $orderBy, $belone, $order_money);
        return $query->count();
    }
    
    /**
     * 获取用户优惠券列表
     */
    public function getUserCouponList($user_id, $type = 0, $orderBy = null, $belone = 0,  $order_money = 0)
    {
        $query = $this->getCouponQuery(1, $user_id, $type, $orderBy, $belone, $order_money);
        return $query;
    }

}