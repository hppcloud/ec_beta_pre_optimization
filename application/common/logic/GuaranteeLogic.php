<?php
namespace app\common\logic;

use think\Model;
use think\Db;
/**
 * 保障服务类logic
 * Class CatsLogic
 * @package common\Logic
 */
class GuaranteeLogic extends Model
{
	public function guaranteeApply(){
		
	}
	
	public function guaranteeCheck(){
		
	}
	
	public function guaranteePay(){
		
	}
	
	public function guaranteeHandleLog($data){
		return db('guarantee_log')->insert($data);
	}
	
	public function costLog($data){
		return db('guarantee_costlog')->insert($data);
	}
}