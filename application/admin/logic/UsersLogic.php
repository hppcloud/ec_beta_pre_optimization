<?php
namespace app\admin\logic;

use think\Model;
use think\Db;

class UsersLogic extends Model
{

    /**
     * 获取指定用户信息
     * @param $uid int 用户UID
     * @param bool $relation 是否关联查询
     *
     * @return mixed 找到返回数组
     */
    public function detail($uid, $relation = true)
    {
        $user = M('users')->where(array('user_id' => $uid))->relation($relation)->find();
        return $user;
    }

    /**
     * 改变用户信息
     * @param int $uid
     * @param array $data
     * @return array
     */
    public function updateUser($uid = 0, $data = array())
    {
        $db_res = M('users')->where(array("user_id" => $uid))->data($data)->save();
        if ($db_res) {
            return array(1, "用户信息修改成功");
        } else {
            return array(0, "用户信息修改失败");
        }
    }


    /**
     * 添加用户
     * @param $user
     * @return array
     */
    public function addUser($user)
    {
        $user_count = Db::name('users')
            ->where(function ($query) use ($user) {
                if ($user['email']) {
                    $query->where('email', $user['email']);
                }
                if ($user['mobile']) {
                    $query->whereOr('mobile', $user['mobile']);
                }
            })
            ->count();
        if ($user_count > 0) {
            return array('status' => -1, 'msg' => '账号已存在');
        }

        $user['password'] = encrypt($user['password']);
        $user['reg_time'] = time();
        $user_id = M('users')->add($user);
        if (!$user_id) {
            return array('status' => -1, 'msg' => '添加失败');
        } else {
            $pay_points = tpCache('basic.reg_integral'); // 会员注册赠送积分
            if ($pay_points > 0)
                accountLog($user_id, 0, $pay_points, '会员注册赠送积分'); // 记录日志流水
            // 会员注册送优惠券
//    		$coupon = M('coupon')->where("send_end_time > ".time()." and ((createnum - send_num) > 0 or createnum = 0) and type = 2")->select();
//    		if(!empty($coupon)){
//    			foreach ($coupon as $key => $val)
//    			{
//    				M('coupon_list')->add(array('cid'=>$val['id'],'type'=>$val['type'],'uid'=>$user_id,'send_time'=>time()));
//    				M('Coupon')->where("id = {$val['id']}")->setInc('send_num'); // 优惠券领取数量加一
//    			}
//    		}
            return array('status' => 1, 'msg' => '添加成功', 'user_id' => $user_id);
        }
    }
    
    
    /**
     * @desc 第三方账号统一登录接口
     * @param $channel
     * @param $code
     * @param $state
     */
    public function snsLogin($channel,$code,$state){
        
        switch ($channel){
            case 'weibo_wap':
                
                $memberInfo = OAuthLoginClass::authLogin('weibo_wap',$code,$state);
                $member = Member::get(['weibo_id'=>$memberInfo['id']]);
                
                //新用户注册
                if(!isset($member)){
                    
                    $member = new Member();
                    $member->headimgurl = $memberInfo['profile_image_url'];
                    $member->weibo_id=$memberInfo['id'];
                    $member->weibo_name = emojiDecode($memberInfo['name']);
                    $member->nickname = emojiDecode($memberInfo['name']);
                    $member->login_num=0;
                    $member->save();//保存一下
                    
                }
                
                break;
            //到这里终于一样的了
            case 'wechat_pub':
                
                $memberInfo = OAuthLoginClass::authLogin('wechat_pub',$code,$state);
//                if(!isset($memberInfo['unionid'])){
//                    error('获取用户信息失败');
//                }
//                $member = Member::get(['wx_union_id'=>$memberInfo['unionid']]);
                $member = Member::get(['wx_pub_openid'=>$memberInfo['openid']]);
                //新用户注册
                if(!isset($member)){
                    
                    $member = new Member();
                    $member->nickname = emojiDecode($memberInfo['nickname']);
                    $member->province = $memberInfo['province'];
                    $member->city = $memberInfo['city'];
                    $member->headimgurl = $memberInfo['headimgurl'];
                    $member->wx_pub_openid = $memberInfo['openid'];
//                    $member->wx_union_id=$memberInfo['unionid'];
//                    $member->nickname = $memberInfo['nickname'];
                    $member->sex = $memberInfo['sex'];
                    $member->login_num=0;
                    $member->save();//保存一下
                    
                    //新用户注册给积分
                    //$member->changePoint(20,7,1,'注册奖励积分');
                    
                    
                }
                break;
            default:
                return callerror('第三方登录接口channel参数错误');
                break;
        }
        
        $this->doLogin($member->id);
        
        $memberinfo = Member::get($member->id);
        $result=[
            'access-token'=>$memberinfo->token,
            'token_expire'=>$memberinfo->token_expire,
            'wx_pub_openid' => $memberinfo->wx_pub_openid,
            'is_bind_phone'=>isset($memberinfo->phone)&&$memberinfo->phone!=''?1:0
        ];
        
        return callsuccess($result);
//        return ;
    
    }





}