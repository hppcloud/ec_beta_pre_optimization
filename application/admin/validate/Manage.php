<?php
namespace app\admin\validate;

use think\Validate;

/**
 * Description of Article
 *
 * @author Administrator
 */
class Manage extends Validate
{
    //验证规则
  
    protected $rule = [
        'seller_name|经销商名称'                 => 'require',
        'enabled|状态'                 => 'require',
        'seller_account|经销商账号'                 => 'require',
        'seller_contact|经销商客服电话'                 => 'require',
        'seller_logo|经销商logo'                => 'require',
        'seller_level|经销商等级'               => 'require',
    ];
    
   
    
}
