<?php
/**
 * 专题管理
 * Date: 2016-06-09
 *  拼团控制器
 */

namespace app\admin\controller;
use app\common\model\Order;
use app\common\model\PreSell as PreSellModel;
use think\Loader;
use think\Db;
use think\Page;

class PreSell extends Base
{
	public function _initialize() {
		parent::_initialize();
		$this->assign('time_begin',date('Y-m-d', strtotime("-3 month")+86400));
		$this->assign('time_end',date('Y-m-d', strtotime('+1 days')));
	}
	public function index()
	{	
	    header("Content-type: text/html; charset=utf-8");

	}


	/**
	 * 审核
	 */
	public function examine(){
		header("Content-type: text/html; charset=utf-8");

	}

	public function order_list(){
		header("Content-type: text/html; charset=utf-8");

	}

}