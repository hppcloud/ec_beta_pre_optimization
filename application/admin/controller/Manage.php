<?php
/*
 * 经销商管理类
 */
namespace app\admin\controller;
use think\Db;
use think\Exception;
use think\Loader;
use think\Page;

class Manage extends Base{
    
    public $table = 'seller';
    /*
     * 经销商列表
     */
    public function managelist(){
    
        $map = ['is_deleted'=>0,'is_admin'=>1];
        
        if(!isEmptyObject(input('enabled'))){
            $map['enabled'] = input('enabled');
        }
    
        if(!isEmptyObject(input('seller_level'))){
            $map['seller_level'] = input('seller_level');
        }
    
        if(!isEmptyObject(input('seller_name'))){
            $map['seller_name'] = array('like','%'.input('seller_name').'%');
        }
    
        

        // B. 开始查询
        $count = Db::name('seller')->where($map)->count();
        // B.2 开始分页
        $Page = new Page($count, 10);
        $show = $Page->show();
        $lists = Db::name('seller')->where($map)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        //$lists = Db::name('seller')->where('is_deleted',0)->select();
        $this->assign('page', $show);
        $this->assign('pager', $Page);
        $this->assign('lists',$lists);
       return $this->fetch();
    }
    
    public function checkPhone(){
        $phone = I('phone');
       
        if(!$phone){
            $this->ajaxReturn(['status' => 0, 'msg' => '手机号码必填']);
        }
        
        if(!check_mobile($phone)){
            $this->ajaxReturn(['status' => 0, 'msg' => '手机号码格式不符']);
        }
    
        $isHas = Db::name('users')->where('mobile',$phone)->count();
        if($isHas>0){
            $this->ajaxReturn(['status' => 0, 'msg' => '该手机号码已存在']);
        }

        $isHasSeller = Db::name('seller')->where('seller_account',$phone)->count();
        if($isHasSeller>0){
            $this->ajaxReturn(['status' => 0, 'msg' => '该手机号码已存在']);
        }
    
        $this->ajaxReturn(['status' => 1, 'msg' => '手机号码可用']);
        
    }
    
    /*
     * 添加经销商
     */
    public function manage(){
        
        
        //修改
        if(IS_POST){
            
            $params = $this->request->post();
            $validate = Loader::validate('Manage');
            if (!$validate->batch()->check($params)) {
                $this->error($validate->getError());
            }
            
            //修改
            $seller_id = I('seller_id',0);
            if($seller_id){

                
                if(isset($params['password'])){
                    if($params['password']!=''){
                        $pwd = $params['password'];
                    
                        $params['password'] = md5(C("AUTH_CODE").$params['password']);
                        \think\log::info('修改密码，密码是'.$params['password']);
                    }else{
                        unset($params['password']);
                    }
                   
                    
                }
                $seller = Db::name('seller')->where('seller_id',$seller_id)->find();
                
                if(isset($params['seller_account'])&&$params['seller_account']!=''){
                    $params['mobile'] = $params['seller_account'];
                    $isHas = Db::name('users')->where('user_id','neq',$seller['user_id'])->where('mobile',$params['seller_account'])->count();
//                    echo Db::name('users')->getLastSql();
//                    print_r($isHas);exit;
                    if($isHas>0){
                        $this->error('该手机号码已经被使用');
                    }

                }
                
                $params['reg_time'] = time();


                //更新用户和seller表
                Db::name('users')->where('user_id',$seller['user_id'])->update($params);
                Db::name('seller')->where('seller_id',$seller_id)->update($params);




//                if(isset($params['password'])&&$params['password']!=''){
//
//                    $sender = $seller['seller_account'];
//                    $params = array('code'=>$pwd);
//                    sendSms("8", $sender, $params);
//                }


                $this->success('经销商信息修改成功','admin/manage/managelist');
                
            }else{
                $isHas = Db::name('users')->where('mobile',$params['seller_account'])->count();
                if($isHas>0){
                    $this->error('该手机号码已经被使用');
                }
                $randstr = strtolower(get_rand_str(8,0,1));
                $pwd = md5(C("AUTH_CODE").$randstr);
                \think\log::info('新建经销商密码是'.$randstr);
                
                $user = ['password'=>$pwd,'is_admin'=>1,'mobile'=>$params['seller_account'],'reg_time'=>time()];
                $userId = Db::name('users')->insertGetId($user);
                $params['user_id'] = $userId;
                $params['add_time'] = time();
                Db::name('seller')->insertGetId($params);
    
               
                $sender = $params['seller_account'];
                $params = array('code'=>$randstr);
                sendSms("8", $sender, $params);
                
                $this->success('经销商信息新增成功','admin/manage/managelist');
            }
            
        }
        
        //新增或者编辑
        if(IS_GET){
            $seller_id = I('seller_id',0);
            //编辑了
            if($seller_id){
                $seller = Db::name('Seller')->where('seller_id',$seller_id)->find();
//                $seller['user'] = Db::name('users')->where('id',$seller['user_id'])->find();
                $this->assign('seller',$seller);
                $this->assign('title','编辑经销商信息');
            }
            $action = I('action');
            if($action&&$action=='detail'){
                $this->assign('action',$action);
                $this->assign('title','经销商详情');
            }
            $seller_levels = [1=>'一级分销商',2=>'二级分销商',3=>'三级分销商'];
            $this->assign('seller_levels',$seller_levels);
            return $this->fetch();
        }
        
    }
    
    
    /**
     * 后台可以给每个经销商设置一个账号密用来登录
     */
    public function addEditSellerVisitor(){
        $seller_id = input('seller_id',null);
        if(!$seller_id){
            $this->error('seller_id缺失');
        }


        
        if($this->request->isGet()){
            
            $visitor = Db::name('seller')
                ->alias('s')
                ->join('tp_users u','s.user_id=u.user_id')
                ->where('s.seller_pid',$seller_id)
                ->where('s.is_admin',-1)
                ->field('s.*,u.mobile')
                ->find();


            if (!$visitor) {
                $visitor['seller_id'] = $seller_id;
            }
           
            
           
            $this->assign('visitor',$visitor);
            return $this->fetch('visitor');
        }
    
    
        $params = $this->request->post();
        if(isset($params['user_id'])&&!isEmptyObject($params['user_id'])){
        
            
            
            if(isset($params['password'])){
                if($params['password']!=''){
//                    $pwd = $params['password'];
                
                    $params['password'] = md5(C("AUTH_CODE").$params['password']);
                    \think\log::info('后台修改临时经销商账号修改密码，密码是'.$params['password']);
                }else{
                    unset($params['password']);
                }
            
            
            }
        
            if(isset($params['seller_account'])&&$params['seller_account']!=''){
                $params['mobile'] = $params['seller_account'];
            }
        
            $params['reg_time'] = time();
        
            $seller = Db::name('seller')->where('user_id',$params['user_id'])->find();
            
            unset($params['seller_id']);

            $params['is_admin'] = -1;
        
            //更新用户和seller表
            Db::name('users')->where('user_id',$seller['user_id'])->update($params);
            Db::name('seller')->where('seller_id',$seller['seller_id'])->update($params);

            $this->success('临时经销商账号信息修改成功','admin/manage/managelist');
        
        }else{
    
            $isHas = Db::name('seller')->where('seller_pid',$params['seller_id'])->where('is_admin',-1)->count();
    
            if($isHas){
                $this->error('该经销商已经有临时账号');
                die;
            }
            
            Db::startTrans();
            try{
                $isHas = Db::name('users')->where('mobile',$params['seller_account'])->count();
                if($isHas>0){
                    $this->error('该手机号码已经被使用');
                }
//                $randstr = strtolower(get_rand_str(8,0,1));
                $pwd = md5(C("AUTH_CODE") . $params['password']);
                \think\log::info('新建临时经销商账号密码是' . $params['password']);
    
                $user = ['password'=>$pwd,'is_admin'=>-1,'mobile'=>$params['seller_account'],'reg_time'=>time()];
                $rt1 = $userId = Db::name('users')->insertGetId($user);
                if(!$rt1){
                    Db::rollback();
                    $this->error('写入用户信息失败');
                }
                $params['user_id'] = $userId;
                $params['add_time'] = time();
                $params['is_admin'] = -1;
                $params['seller_pid'] = $seller_id;
                
                unset($params['seller_id']);
                
                
                $params['seller_contact']=$params['seller_logo'] = '';
                $rt2 = Db::name('seller')->insertGetId($params);
                if(!$rt2){
                    Db::rollback();
                    $this->error('写入临时账号信息失败');
                }
    
                
                Db::commit();
                $this->success('临时经销商账号新增成功','admin/manage/managelist');
            }catch (Exception $e){
                Db::rollback();
                echo '添加临时账号失败'.$e->getMessage();
//                $this->error('添加临时账号失败'.$e->getMessage());
            }
            
        }
        
        
    }
    
   
    
    
    /**
     * 删除菜单
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function del()
    {
    
        $params = $this->request->post();
        try{
        
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'经销商删除成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'删除失败','result'=>null]);
        }catch (Exception $e){
        
            return $this->ajaxReturn(['status'=>0,'msg'=>'删除失败','result'=>$e->getMessage()]);
        
        }
    }
    
    /**
     * 菜单禁用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function forbid()
    {
    
        $params = $this->request->post();
        try{
        
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'经销商禁用成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'禁用失败','result'=>null]);
        }catch (Exception $e){
    
            return $this->ajaxReturn(['status'=>0,'msg'=>'禁用失败','result'=>$e->getMessage()]);
            
        }
    }
    
    /**
     * 启用
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public function resume()
    {
        $params = $this->request->post();
        try{
            
            if ($this->changeColumn($this->table)) {
                return $this->ajaxReturn(['status'=>1,'msg'=>'经销商启用成功','result'=>null]);
            }
            return $this->ajaxReturn(['status'=>0,'msg'=>'启用失败','result'=>null]);
        }catch (Exception $e){
    
            return $this->ajaxReturn(['status'=>0,'msg'=>'禁用失败','result'=>$e->getMessage()]);
        }
        
       
    }
    
    public function action(){
    
    }
    
    /*
     * 关闭经销商
     */
    public function delmanage(){
       return $this->fetch(); 
    }
    /*
     * 查看经销商详情
     */
    public function detailmanage(){
       return $this->fetch();
    }
    
    
    /*
     * 查看经销商店铺列表
     */
    public function showmanageshoplist(){
       $sellerId = I('seller_id');
       if(!$sellerId){
           $this->error('经销商id不存在');
       }
        $map = ['seller_id'=>$sellerId];
       
       $store_name = I('store_name');
       if(isset($store_name)&&$store_name!=''){
           $map['store_name'] = array('like','%'.$store_name.'%');
       }
    
       $store_enabled = I('store_enabled');
        if(isset($store_enabled)&&$store_enabled!=''){
            $map['store_enabled'] = $store_enabled;
        }
        
        
    
        
        // B. 开始查询
        $count = Db::name('store')->where($map)->count();
        // B.2 开始分页
        $Page = new Page($count, 10);
        $show = $Page->show();
        $lists = Db::name('store')->where($map)->limit($Page->firstRow . ',' . $Page->listRows)->order('store_time desc')->select();
    
        //按数字大小排序，所以排名.(居然是整个系统的排名）
//        $query = 'select s.store_id,s.store_name,s.seller_name,count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id    group by s.store_id order by total desc';
//
//        $storeCustomerList = Db::query($query);
        
        
        
        //绑定人数
//        foreach ($lists as &$store){
//            $store['customer'] = 0;
//            foreach ($storeCustomerList as $key=>$storeCustomer){
//                if($store['store_id']==$storeCustomer['store_id']){
//                    $store['customer'] = $storeCustomer['total'];
//                    $store['customerIndex'] = $key+1;
//                    continue;
//                }
//            }
//        }
    
//        $countQuery = 'select count(*) as total from tp_user_store as u JOIN tp_store as s on u.store_id=s.store_id WHERE s.seller_id='.$sellerId;
//        $count = Db::query($countQuery);
        
        
        
        $this->assign('page', $show);
        $this->assign('pager', $Page);
        $this->assign('lists',$lists);
        
       return $this->fetch();
    }
    
    /**
     * 切换店铺的开/关状态
     */
    public function taggleEnableFun(){
        $storeId = I('store_id');
        $store_enabled = I('store_enabled');
        
        if(!isset($storeId)||!isset($store_enabled)){
            return $this->ajaxReturn(['status'=>0,'msg'=>'禁用失败','result'=>'store_id或者状态为空']);
        }
        
        $rt = Db::name('store')->where('store_id',$storeId)->update(['store_enabled'=>$store_enabled]);
    
        if ($rt) {
            return $this->ajaxReturn(['status'=>1,'msg'=>'操作成功','result'=>null]);
        }
        return $this->ajaxReturn(['status'=>0,'msg'=>'操作失败','result'=>null]);
        
        
    }
}