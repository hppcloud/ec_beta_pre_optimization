<?php
/**
 *  拼团控制器
 */

namespace app\admin\controller;

use app\common\model\Order;
use app\common\model\team\TeamActivity;
use app\common\model\team\TeamFound;
use think\Loader;
use think\Db;
use think\Page;

class Team extends Base
{
	public function _initialize() {
		parent::_initialize();
		$this->assign('time_begin',date('Y-m-d', strtotime("-3 month")+86400));
		$this->assign('time_end',date('Y-m-d', strtotime('+1 days')));
	}
	public function index()
	{	
		header("Content-type: text/html; charset=utf-8");

	}

	public function info()
	{
		header("Content-type: text/html; charset=utf-8");

	}

	/**
	 * 审核
	 */
	public function examine(){
		header("Content-type: text/html; charset=utf-8");

	}

	public function found_order_list(){
		header("Content-type: text/html; charset=utf-8");

	}

	public function order_list(){
		header("Content-type: text/html; charset=utf-8");

	}

	/**
	 * 团长佣金
	 */
	public function bonus(){
		header("Content-type: text/html; charset=utf-8");

	}

	public function doBonus(){
		header("Content-type: text/html; charset=utf-8");

	}
}