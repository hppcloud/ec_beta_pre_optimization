<?php
namespace app\admin\controller;

use app\admin\logic\StoreLogic;
use app\common\logic\ModuleLogic;
use app\seller\logic\StoreLogic as SellerStoreLogic;
use think\Db;
use think\Loader;
use think\Page;
use app\common\model\SellerAddress;

class Store extends Base{

    /**
     * 迁移店铺
     */
    public function translate()
    {
        if ($this->request->isGet()) {

            $seller_list = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
            $store_list = Db::name('store')->field('store_id,store_name')->select();

//            var_dump($store_list);
//            die;
            $this->assign('seller_list', $seller_list);
            $this->assign('store_list', $store_list);
            return $this->fetch();
        }

        if ($this->request->isPost()) {

            set_time_limit(0);
            $rule = [
                'store_id|店铺id' => 'require',
                'seller_id|经销商id' => 'require'
            ];
            $check = $this->validate($this->request->param(), $rule);
            if ($check !== true) {
                $this->error($check);
            }

            $store_id = input('store_id');
            $store = Db::name('store')->where('store_id', $store_id)->find();
            $seller_id = input('seller_id');


            $sellerStoreLogic = new SellerStoreLogic();


            $rt = $sellerStoreLogic->transferStore($store_id, $seller_id, $store['seller_id']);


            if ($rt['code'] === 1) {
                $this->success('迁移成功');
            } else {
                $this->error('迁移失败:' . $rt['msg']);
            }




        }
    }

    public function researchStore()
    {
        $title = input('title');
        $width = input('width', 300);
        $list = Db::name('store')->where('store_name', 'like', '%' . $title . '%')->select();
        $this->assign('list', $list);
        $this->assign('width', $width);
        $this->view->engine->layout(false);
        return $this->fetch('research_store');

    }

    public function researchSeller()
    {
        $title = input('title');
        $width = input('width', 300);
        $list = Db::name('seller')->where('seller_name', 'like', '%' . $title . '%')->select();
        $this->assign('list', $list);
        $this->assign('width', $width);
        $this->view->engine->layout(false);
        return $this->fetch('research_seller');

    }

    /**
     * 订单信息导出
     * 表头 订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function export_order()
    {
        $select_year = $this->select_year;
        $begin =  $this->begin;
        $end   =  $this->end;

        // 搜索条件 STORE_ID
        $condition = array('seller_id' => SELLER_ID,'deleted'=>0); // 商家id
        I('store_id') ? $condition['store_id'] = I('store_id') : false;
        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1

        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
        I('order_status') != '' ?  : false;

        if(I('order_status') != ''){
            if(I('order_status')=='refund'){
                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
            }else{
                $condition['order_status'] = I('order_status/d');
            }

        }



        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单

        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
//        $condition['prom_type'] = array('lt',5);


//        $count = Db::name('order'.$select_year)->where($condition)->count();


//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }


        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
        $order_id_arr = I('order_id_arr',null);
        if($order_id_arr&&$order_id_arr!=''){
            $condition = [];
            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
        }




        $region = Db::name('region')->cache(true)->getField('id,name');



//        var_dump($where);die;

        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order'.$select_year)->where($condition)->order('order_id')->select();


        $user_id_arr = [];
        foreach ($orderList as $order){
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id','in',$user_id_arr)->field('user_id,email')->select();
        foreach ($orderList as &$order){
            foreach ($userList as $user){
                if($order['user_id']==$user['user_id']){
                    $order['email'] = $user['email'];
                }
            }
        }



        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');



        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">日期</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">物流单号</td>';
        $strTable .= '</tr>';


        foreach ($orderList as $k => $val) {

            $tempStore = [];

            foreach ($stores as $store){

                if($store['store_id']===$val['store_id']){

                    $tempStore = $store;

                }
            }


            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' .  ' '. '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] . '['.$tempStore['store_id'].']' . ' </td>';//店铺名称


            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:m:s',$val['create_time']) . ' </td>';//日期
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态


            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价
            $orderGoods = Db::name('OrderGoods')
                ->alias('g')
                ->join('SpecGoodsPrice p','g.goods_id=p.goods_id and g.spec_key = p.key')
                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $strGoods = "";
            foreach ($orderGoods as $goods) {

                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'];
                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price']. " SKU：" . $goods['sku'];
                $strGoods .= "<br />";
            }
            unset($orderGoods);
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strGoods . ' </td>';

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式

            /**
             * 发票信息
             *
             */
            $invoice = Db::name('invoice')->where('order_id',$val['order_id'])->find();
            $strInvoice = '';


            $strInvoice .= "发票类型：" . $INVOICE_TYPE[$val['invoice_type']] . " 索要类型：" . $INVOICE_BLAG_WAY[$invoice['blag_way']];
            $strInvoice .= "<br />";
            $strInvoice .= "抬头：" . $val['invoice_title'] ;
            $strInvoice .= "<br />";
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strInvoice . '</td>';//发票信息
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['email'] . '</td>';//用户账号,邮箱

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言


            /**
             * 物流信息
             * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
             */

            $deliver_doc = Db::name('deliveryDoc')->where('order_id',$val['order_id'])->find();

            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['address']}" . ' </td>';//收货地址
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的




//
//
//
//
//
//
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['consignee']}" . ' </td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['address'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['goods_price'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . '</td>';
//            if($val['pay_name'] == ''){
//                $strTable .= '<td style="text-align:left;font-size:12px;">在线支付</td>';
//            }else{
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';
//            }
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->pay_status[$val['pay_status']] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->shipping_status[$val['shipping_status']] . '</td>';

            $strTable .= '</tr>';
        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, 'order');
        exit();
    }


    public function export_store(){

        $map = '';
        $map .= 'is_own_shop = 0 and deleted= 0' ;
        $map.= ' ' ;


        $query = 'select s.*,count(*) as total from tp_user_store as u RIGHT JOIN tp_store as s on u.store_id=s.store_id WHERE '.$map."  group by s.store_id order by total desc,s.add_time desc";

        $storeList = Db::query($query);



        $sellermap = ['is_deleted'=>0,'is_admin'=>1];

        $sellers = Db::name('seller')->where($sellermap)->select();

//        店铺ID
//店铺名称
//经销商名称
//进驻日期
//状态
//用户人数
//邮箱后缀

        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">进驻日期</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户人数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">邮箱后缀</td>';
        $strTable .= '</tr>';


        foreach ($storeList as $k => $val) {

            $tempStore = [];

            foreach ($sellers as $seller){

                if($seller['seller_id']===$val['seller_id']){

                    $tempSeller = $seller;

                }
            }


            $strTable .= '<tr>';

            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' .$val['store_id']. '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['store_name'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] . '['.$tempSeller['seller_id'].']' . ' </td>';//店铺名称



            $status = $val['store_enabled']==1?'开启':'关闭';
            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:m:s',$val['add_time']) . ' </td>';//日期
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $status . ' </td>';//订单状态
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total'] . ' </td>';//订单状态
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_email'] . ' </td>';//订单状态




            $strTable .= '</tr>';
        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, 'order');
        exit();





    }

	//店铺等级
	public function store_grade(){
		$model =  M('store_grade');
		$count = $model->count();
		$Page = new Page($count,10);
		$list = $model->order('sg_id')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		$show = $Page->show();
		$this->assign('pager',$Page);
		$this->assign('page',$show);
		return $this->fetch();
	}

	public function grade_info()
	{
		$sg_id = input('sg_id/d');
		if ($sg_id) {
			$info = Db::name('store_grade')->where('sg_id',$sg_id)->find();
			if(empty($info)){
				$this->error('没有找到相关的记录');
			}
			$info['sg_act_limits'] = explode(',', $info['sg_act_limits']);
			$this->assign('info', $info);
		}
		$right = Db::name('system_menu')->where(array('type' => 1))->order('id')->select();
        if(is_array($right)) {
            foreach ($right as $k => $val) {
                if (!empty($info)) {
                    $val['enable'] = in_array($val['id'], $info['sg_act_limits']);
                }
                $modules[$val['group']][] = $val;
            }
        }else{
            $this->error('数据格式错误！！');
        }
		//权限组
        $moduleLogic = new ModuleLogic;
        $group = $moduleLogic->getPrivilege(1);
		$this->assign('group', $group);
		$this->assign('modules', $modules);
		return $this->fetch();
	}

	public function grade_info_save(){
		$data = I('post.');
		$data['sg_act_limits'] = is_array($data['right']) ? implode(',', $data['right']) : '';
        if($data['act'] == 'del'){  //删除
            if(M('store')->where(array('grade_id'=>$data['del_id']))->count()>0){
                $this->ajaxReturn(['status'=>0,'msg'=>'该等级下有开通店铺，不得删除']);
            }else{
                $r = M('store_grade')->where("sg_id=".$data['del_id'])->delete();
                $this->ajaxReturn(['status'=>1,'msg'=>'删除成功']);
            }
        }else{
            //添加，编辑
            $res = M('store_grade')->where(['sg_name'=>$data['sg_name'],'sg_id'=>['neq',$data['sg_id']]])->count();
            $res>0 && $this->error('店铺等级名称已存在！！');
            if($data['sg_id'] > 0){
                $r = M('store_grade')->where("sg_id=".$data['sg_id'])->save($data);
            }else{
                $r = M('store_grade')->add($data);
            }

        }
		if($r){
			$this->success('编辑成功',U('Store/store_grade'));
		}else{
			$this->error('提交失败');
		}
	}
	
	public function store_class(){
		$count = Db::name('store_class')->count();
		$Page = new Page($count,10);
		$list = Db::name('store_class')->order('sc_sort desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		$show = $Page->show();
		$this->assign('pager',$Page);
		$this->assign('page',$show);
		return $this->fetch();
	}
	
	//店铺分类
	public function class_info(){
		$sc_id = I('sc_id');
		if($sc_id){
			$info = M('store_class')->where("sc_id=$sc_id")->find();
			$this->assign('info',$info);
		}
		return $this->fetch();
	}
	
	public function class_info_save(){
		$data = I('post.');
		$storeClassValidate = Loader::validate('StoreClass');
		if($data['sc_id'] > 0){
			if($data['act']== 'del'){
				$storeClassCount = Db::name('store')->where('sc_id',$data['sc_id'])->count();
				if($storeClassCount > 0){
					$this->ajaxReturn(['status' => 0, 'msg' => '该分类下有开通店铺，不得删除', 'result' => '']);
				}else{
					$r = Db::name('store_class')->where("sc_id", $data['sc_id'])->delete();
					if($r !== false){
						$this->ajaxReturn(['status' => 1, 'msg' => '删除成功', 'result' => '']);
					}else{
						$this->ajaxReturn(['status' => 0, 'msg' => '删除失败', 'result' => '']);
					}
				}
			}else{
				if (!$storeClassValidate->batch()->check($data)) {
					$this->ajaxReturn(['status' => 0, 'msg' => '编辑失败', 'result' => $storeClassValidate->getError()]);
				}else{
					$r = Db::name('store_class')->where("sc_id", $data['sc_id'])->save($data);
					if($r !== false){
						$this->ajaxReturn(['status' => 1, 'msg' => '编辑成功', 'result' => '']);
					}else{
						$this->ajaxReturn(['status' => 0, 'msg' => '编辑失败', 'result' => '']);
					}
				}
			}
		}else{
			if (!$storeClassValidate->batch()->check($data)) {
				$this->ajaxReturn(['status' => 0, 'msg' => '添加失败', 'result' => $storeClassValidate->getError()]);
			}else{
				$r = Db::name('store_class')->add($data);
				if($r !== false){
					$this->ajaxReturn(['status' => 1, 'msg' => '添加成功', 'result' => '']);
				}else{
					$this->ajaxReturn(['status' => 0, 'msg' => '添加失败', 'result' => '']);
				}
			}
		}
	}
	
	//普通店铺列表
	public function store_list(){



        $map =['is_own_shop'=>0,'deleted'=>0];



        if(!isEmptyObject(I("store_enabled"))){
            $map['store_enabled']=  I("store_enabled");
        }


//		$seller_name = I('seller_name');
//		if($seller_name) $map['seller_name'] = array('like',"%$seller_name%");
        $seller_id = I('seller_id/d',null);
        if($seller_id){
            $map['seller_id']=$seller_id;
        }

        $store_email = I('store_email',null);
        if($store_email&&!isEmptyObject($store_email)){
            $map['store_email']= array('like','%'.$store_email.'%');
        }



        $store_name = I('store_name');
        if($store_name&&!isEmptyObject($store_name)){
            $map['store_name']= array('like','%'.$store_name.'%');
        }

        $validate = I('validate/d');
        if(!isEmptyObject($validate)||$validate===0){
            $map['validate'] = $validate;
        }
//        array(3) { ["is_own_shop"]=> int(0) ["deleted"]=> int(0) ["validate"]=> string(0) "" }
//        var_dump($map);die;
        
        


		$count = Db::name('store')->where($map)->count();
		$Page = new Page($count,10);

        $list = Db::name('store')->where($map)->order('store_id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();


//        $map = '';
//        $map .= 'is_own_shop = 0 and deleted= 0' ;
//        $map.= ' ' ;
//
//        if(I("store_enabled",null)&&I("store_enabled")!=''){
//            $map.=  (' and store_enabled = '.store_stateI("store_enabled"));
//        }
//
//        $seller_id = I('seller_id/d');
//        if($seller_id){
//            $map.=( ' and seller_id= '.$seller_id);
//        }
//
//        $store_email = I('store_email',null);
//        if($store_email){
//            $map.= (" and store_email like '%".$store_email."%'");
//        }
//
//
//
//        $store_name = I('store_name');
//        if($store_name){
//            $map.= (" and store_name like '%".$store_name."%'");
//        }
//        $count = Db::name('store')->where($map)->count();

//        $filter = '';
//        $filter .= 's.is_own_shop = 0 and s.deleted= 0' ;
//        $filter.= ' ' ;
//
//        if(I("store_enabled",null)&&I("store_enabled")!=''){
//            $filter.=  ' and s.store_enabled = '.I("store_enabled");
//        }
//
//        $seller_id = I('seller_id/d');
//        if($seller_id){
//            $filter.=( ' and s.seller_id= '.$seller_id);
//        }
//
//        $store_email = I('store_email',null);
//        if($store_email){
//            $filter.= (" and s.store_email like '%".$store_email."%'");
//        }
//
//        $store_name = I('store_name');
//        if($store_name){
//            $filter.= (" and s.store_name like '%".$store_name."%'");
//        }





//        var_dump($filter);die;


//        $query = 'select s.*,count(*) as total from tp_user_store as u RIGHT JOIN tp_store as s on u.store_id=s.store_id WHERE '.$filter."  group by s.store_id order by total desc,s.add_time desc limit {$Page->firstRow},{$Page->listRows}";
//
//        $list = Db::query($query);



//		$store_ids = get_arr_column($list, 'store_id');
//		$store_ids = implode(",",$store_ids);
        
        
        
        //按数字大小排序，所以排名
        
        
        
        
//        //绑定人数
//        foreach ($list as &$store){
//            $store['customer'] = 0;
//            foreach ($storeCustomerList as $key=>$storeCustomer){
//                if($store['store_id']==$storeCustomer['store_id']){
//                    $store['customer'] = $storeCustomer['total'];
//                    $store['customerIndex'] = $key+1;
//                    continue;
//                }
//            }
//        }


        $sellermap = ['is_deleted'=>0,'is_admin'=>1];


        $sellers = Db::name('seller')->where($sellermap)->select();

        $this->assign('sellers',$sellers);

		$this->assign('list',$list);

		$show = $Page->show();
		$this->assign('page',$show);
		$this->assign('pager',$Page);
		$store_grade = M('store_grade')->getField('sg_id,sg_name');
		$this->assign('store_grade',$store_grade);
		$this->assign('store_class',M('store_class')->getField('sc_id,sc_name',true));
		return $this->fetch();
	}
	
	/*添加店铺*/
	public function store_add(){
        $is_own_shop = I('is_own_shop',1);
		if(IS_POST){
			$store_name = trim(I('store_name'));
			$user_name = trim(I('user_name'));
			$seller_name = trim(I('seller_name'));
            $password = trim(I('password'));
            if(strlen($password) < 6){
                $this->ajaxReturn(['status'=>0,'msg'=>'登录密码不能为空且不能少于6个字符']);
            }
			if(M('store')->where("store_name='$store_name'")->count()>0){
				$this->ajaxReturn(['status'=>0,'msg'=>'店铺名称已存在']);
			}
			if(M('seller')->where("seller_name='$seller_name'")->count()>0){
                $this->ajaxReturn(['status'=>0,'msg'=>'此会员已被占用']);
			}
			$user_id = M('users')->where("email='$user_name' or mobile='$user_name'")->getField('user_id');
			if($user_id){
				if(M('store')->where(array('user_id'=>$user_id))->count()>0){
                    $this->ajaxReturn(['status'=>0,'msg'=>'该会员已经申请开通过店铺']);
				}
			}else{   //没有这个用户就创建一个
                if(check_email($user_name)){
                    $user_data['email'] = $user_name;
                }else{
                    $user_data['mobile'] = $user_name;
                };
                $user_data['password'] = $password;
                $user_obj = new \app\admin\logic\UsersLogic();
                $add_user_res = $user_obj->addUser($user_data);
                if($add_user_res['status'] !=1) $this->ajaxReturn($add_user_res);
                $user_id = $add_user_res['user_id'];
            }
			$store = array('store_name'=>$store_name,
				'user_id'       =>  $user_id,	
                'user_name'     =>  $user_name,
                'store_state'   =>  1,
				'seller_name'   =>  $seller_name,
				'seller_id'		=>	0,
                'password'      =>  I('password'),
				'store_time'    =>  time(),
                'is_own_shop'   =>  $is_own_shop
			);
			$storeLogic = new StoreLogic();
			if($storeLogic->addStore($store)){
				if($is_own_shop == 1){
                    $this->ajaxReturn(['status'=>1,'msg'=>'店铺添加成功','url'=>U('Store/store_own_list')]);
				}else{
                    $this->ajaxReturn(['status'=>1,'msg'=>'店铺添加成功','url'=>U('Store/store_list')]);
				}
				exit;
			}else{
                $this->ajaxReturn(['status'=>0,'msg'=>'店铺添加失败']);
			}
		}
		$this->assign('is_own_shop',$is_own_shop);
		return $this->fetch();
	}
	
	/*验证店铺名称，店铺登录账号*/
	public function store_check(){
		$store_name = I('store_name');
		$seller_name = I('seller_name');
		$user_name = I('user_name');
		$res = array('status'=>1);
		if($store_name && M('store')->where("store_name='$store_name'")->count()>0){
			$res = array('status'=>-1,'msg'=>'店铺名称已存在');
		}
		
		if(!empty($user_name)){
			if(!check_email($user_name) && !check_mobile($user_name)){
				$res = array('status'=>-1,'msg'=>'店主账号格式有误');
			}
			if(M('users')->where("email='$user_name' or mobile='$user_name'")->count()>0){
				$res = array('status'=>-1,'msg'=>'会员名称已被占用');
			}
		}

		if($seller_name && M('seller')->where("seller_name='$seller_name'")->count()>0){
			$res = array('status'=>-1,'msg'=>'此账号名称已被占用');
		}
		respose($res);
	}
	
	/*编辑自营店铺*/
	public function store_edit(){
		if(IS_POST){
			$data = I('post.');
			if(M('store')->where("store_id=".$data['store_id'])->save($data)){
				$this->success('编辑店铺成功',U('Store/store_own_list'));
				exit;
			}else{
				$this->error('编辑失败');
			}
		}
		$store_id = I('store_id',0);
		$store = M('store')->where("store_id=$store_id")->find();
		$this->assign('store',$store);
		return $this->fetch();
	}

    public function store_validate(){

        $store_id = I('store_id');

        if(!$store_id){
            $this->error('store_id参数错误');
        }

        if(IS_POST){

            $validate = I('validate/d');
            if(!in_array($validate,[-1,1])){
                $this->error('审核结果错误');
            }



            $rt = Db::name('store')->where('store_id',$store_id)->update(['validate'=>$validate]);

            if($rt!==false){
                storeValidateLog($store_id,$validate,I('remark'));
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }

        }



        if($store_id>0){
            $store = M('store')->where("store_id=$store_id")->find();
            $this->assign('store',$store);
            $apply = M('store_apply')->where('store_id='.$store['store_id'])->order('id desc')->find();

            $apply['protocal_photo_arr'] = explode('|',$apply['protocal_photo']);

            $SellerAddress = new SellerAddress();
            $apply['seller_address'] = $SellerAddress->where('seller_address_id',$apply['seller_address_id'])->find();
            $this->assign('apply',$apply);

            $validate_log = Db::name('StoreValidateLog')->where('store_id',$store_id)->select();
            $this->assign('log',$validate_log);

        }

        return $this->fetch();
    }

	//编辑外驻店铺
	public function store_info_edit(){
		if(IS_POST){
			$map = I('post.');
			$store = $map['store'];
            $store['deposit'] = $map['store']['deposit'];
			unset($map['store']);
			$store['province_id'] = $map['company_province']; //省
			$store['city_id'] = $map['company_city']; //市
			$store['district'] = $map['company_district'];  //区
			$store['company_name'] = $map['company_name'];
			$store['store_phone'] = $map['store_person_mobile'];
			$store['store_address'] = $map['company_address'];
			$store['store_address'] = $map['company_address'];
			$store['store_end_time'] = strtotime($map['store_end_time']);
			$store['store_qq'] = $map['store_person_qq'];
			$map['business_permanent'] == 'Y' && $map['business_date_end'] = '长期';
            if($store['city_id']<1 || $store['district']<1){
                $this->error('请填写完整的公司所在地');
            }
			$save_store = M('store')->where(array('store_id'=>$map['store_id']))->save($store);
            $select_apply = M('store_apply')->where(array('user_id'=>$store['user_id']))->find();
            if($select_apply){
                //fix: 管理员后台编辑店铺分类, 商家店铺分类没同时更新 @{
                $map['sc_id'] = $store['sc_id'];
                $store_class = Db::name('store_class')->where("sc_id", $map['sc_id'])->find();
                $map['sc_name'] = $store_class['sc_name']; 
                // @}
                $save_apply = M('store_apply')->where(array('user_id'=>$store['user_id']))->save($map);
            }else{
                $map['user_id']=$store['user_id'];
                $save_apply = M('store_apply')->add($map);
            }
			if($save_store===false && $save_apply===false){
                $this->error('编辑失败');
			}else{
                if($store['store_state'] == 0){
                    M('goods')->where(array('store_id'=>$map['store_id']))->save(array('is_on_sale'=>0));//关闭店铺，同时下架店铺所有商品
                }
                $backuUrl = I('get.is_own_shop') == 1 ? U('Store/store_own_list') : U('Store/store_list');  //自营店铺页
                    $this->success('编辑店铺成功',$backuUrl);
                exit;
			}
		}
		$store_id = I('store_id');
		if($store_id>0){
			$store = M('store')->where("store_id=$store_id")->find();
			$this->assign('store',$store);
			$apply = M('store_apply')->where('user_id='.$store['user_id'])->find();
			$bank_province_name = M('region')->where(array('id'=>$apply['bank_province']))->getField('name');
			$bank_city_name = M('region')->where(array('id'=>$apply['bank_city']))->getField('name');
			$this->assign('bank_province_name',$bank_province_name);
			$this->assign('bank_city_name',$bank_city_name);
			$this->assign('apply',$apply);
            $city = M('region')->where(array('parent_id'=>$apply['company_province'],'level'=>2))->select();
            $area = M('region')->where(array('parent_id'=>$apply['company_city'],'level'=>3))->select();
            $this->assign('city',$city);
            $this->assign('area',$area);
		}
		$company_type = config('company_type');
		$rate_list = config('rate_list');
		$this->assign('rate_list', $rate_list);
		$this->assign('company_type', $company_type);
		$this->assign('store_grade',M('store_grade')->getField('sg_id,sg_name'));
		$this->assign('store_class',M('store_class')->getField('sc_id,sc_name'));
		$province = M('region')->where(array('parent_id'=>0,'level'=>1))->select();
		$this->assign('province',$province);
		return $this->fetch();
	}
	
	/*删除店铺*/
	public function store_del(){
		$store_id = I('del_id');
		if($store_id > 1){
			$store = Db::name('store')->where("store_id", $store_id)->find();
			$storeGoodsCount = Db::name('goods')->where("store_id", $store_id)->count();
			if ($storeGoodsCount > 0) {
				$this->ajaxReturn(['status' => 0, 'msg' => '该店铺有发布商品，不得删除', 'result' => '']);
			}else{
				Db::name('store')->where("store_id", $store_id)->save(['deleted'=>1,'store_state'=>0]);
				Db::name('seller')->where("store_id", $store_id)->save(['enabled'=>1]);
//				Db::name('users')->where("user_id", $store['user_id'])->delete();
				Db::name('store_apply')->where("user_id", $store['user_id'])->save(['apply_state'=>2]);
				adminLog("删除店铺".$store['store_name']);
				$this->ajaxReturn(['status' => 1, 'msg' => '删除店铺'.$store['store_name'].'成功', 'result' => '']);
			}
		}else{
			$this->ajaxReturn(['status' => 0, 'msg' => '基础自营店，不得删除', 'result' => '']);
		}
	}
	
	//店铺信息
	public function store_info(){
		$store_id = I('store_id');
		$store = M('store')->where("store_id=".$store_id)->find();
		$this->assign('store',$store);
		$store_grade = M('store_grade')->where(array('sg_id'=>$store['grade_id']))->find();
		$this->assign('store_grade',$store_grade);
		$apply = M('store_apply')->where("user_id=".$store['user_id'])->find();
		$province_name = M('region')->where(array('id'=>$apply['company_province']))->getField('name');
		$city_name = M('region')->where(array('id'=>$apply['company_city']))->getField('name');
		$district_name = M('region')->where(array('id'=>$apply['company_district']))->getField('name');
		$bank_province_name = M('region')->where(array('id'=>$apply['bank_province']))->getField('name');
		$bank_city_name = M('region')->where(array('id'=>$apply['bank_city']))->getField('name');
		$this->assign('bank_province_name',$bank_province_name);
		$this->assign('bank_city_name',$bank_city_name);
		$this->assign('province_name',$province_name);
		$this->assign('city_name',$city_name);
		$this->assign('district_name',$district_name);
		$this->assign('apply',$apply);
		$bind_class_list = M('store_bind_class')->where("store_id=".$store_id)->select();
		$goods_class = M('goods_category')->getField('id,name');
		for($i = 0, $j = count($bind_class_list); $i < $j; $i++) {
			$bind_class_list[$i]['class_1_name'] = $goods_class[$bind_class_list[$i]['class_1']];
			$bind_class_list[$i]['class_2_name'] = $goods_class[$bind_class_list[$i]['class_2']];
			$bind_class_list[$i]['class_3_name'] = $goods_class[$bind_class_list[$i]['class_3']];
		}
		$this->assign('bind_class_list',$bind_class_list);
		return $this->fetch();
	}
	
	//自营店铺列表
	public function store_own_list(){

		$model =  M('store');
		$map['is_own_shop'] = 1 ;
		$grade_id = I("grade_id");
		if($grade_id>0) $map['grade_id'] = $grade_id;
		$sc_id =I('sc_id');
		if($sc_id>0) $map['sc_id'] = $sc_id;
		$store_state = I("store_state");
        switch ($store_state){
            case '': '';break;
            case 3:
                $map['store_end_time'] = ['between',[time(),strtotime('+1 month')]];
                break;  //即将过期（1个月）
            case 4:
                $map['store_end_time'] = ['between',1,time()];
                break;  //已过期
            default : $map['store_state'] = $store_state;break;
        }
		$seller_name = I('seller_name');
		if($seller_name) $map['seller_name'] = array('like',"%$seller_name%");
		$store_name = I('store_name');
		if($store_name) $map['store_name'] = array('like',"%$store_name%");
		$count = $model->where($map)->count();
		$Page = new Page($count,10);
		$list = $model->where($map)->order('store_id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);	
		$show = $Page->show();
		$this->assign('page',$show);
		$this->assign('pager',$Page);
		$store_grade = M('store_grade')->getField('sg_id,sg_name');
		$this->assign('store_grade',$store_grade);
		$this->assign('store_class',M('store_class')->getField('sc_id,sc_name'));
		return $this->fetch();
	}
	
	//店铺申请列表
	public function apply_list(){
		$model =  M('store_apply');
		$map['apply_state'] = array('neq',1);
		$map['add_time'] = array('gt',0); //填写完成的才显示
		$sg_id = I("sg_id");
		if($sg_id>0) $map['sg_id'] = $sg_id;
		$sc_id =I('sc_id');
		if($sc_id>0) $map['sc_id'] = $sc_id;
		$seller_name = I('seller_name');
		$seller_name && $map['seller_name'] = array('like',"%$seller_name%");
		$store_name = I('store_name');
		$store_name && $map['store_name'] = array('like',"%$store_name%");
		$count = $model->where($map)->count();
		$Page = new Page($count,10);
		$list = $model->where($map)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		$show = $Page->show();
		$this->assign('pager',$Page);
		$this->assign('page',$show);
		$this->assign('store_grade',M('store_grade')->getField('sg_id,sg_name'));
		$this->assign('store_class',M('store_class')->getField('sc_id,sc_name'));
		return $this->fetch();
	}
	
	public function apply_del(){
		$id = I('del_id');
		if($id && M('store_apply')->where(array('id'=>$id))->delete()){
//			$this->success('操作成功',U('Store/apply_list'));
            $this->ajaxReturn(['status'=>1,'操作成功']);
		}else{
//			$this->error('操作失败');
            $this->ajaxReturn(['status'=>0,'操作失败']);
		}
	}
	//经营类目申请列表
	public function apply_class_list(){
		$state = I('state'); //审核状态
        $store_name = I('store_name');  ///店铺名称
        if($store_name) {
            $store_where['store_name'] = $store_name;  ///店铺名称
            $bind_class_where['store_id'] = Db::name('store')->where($store_where)->getField('store_id');
        }
        $state>=0 && $bind_class_where['state'] = $state;
        $count = Db::name('store_bind_class')->where($bind_class_where)->count();
        $Page = new Page($count,20);
        $bind_class = Db::name('store_bind_class')
            ->where($bind_class_where)
            ->order('bid desc')
            ->limit($Page->firstRow,$Page->listRows)
            ->select();
		$goods_class = Db::name('goods_category')->cache(true)->getField('id,name');
		for($i = 0, $j = count($bind_class); $i < $j; $i++) {
			$bind_class[$i]['class_1_name'] = $goods_class[$bind_class[$i]['class_1']];
			$bind_class[$i]['class_2_name'] = $goods_class[$bind_class[$i]['class_2']];
			$bind_class[$i]['class_3_name'] = $goods_class[$bind_class[$i]['class_3']];
			$store = Db::name('store')->where("store_id=".$bind_class[$i]['store_id'])->find();
			$bind_class[$i]['store_name'] = $store['store_name'];
			$bind_class[$i]['seller_name'] = $store['seller_name'];
		}
		$this->assign('bind_class',$bind_class);
		$this->assign('page',$Page);
		return $this->fetch();
	}
	
	//查看-添加店铺经营类目
	public function store_class_info(){
		$store_id = I('store_id');
		$store = M('store')->where(array('store_id'=>$store_id))->find();
		$this->assign('store',$store);
		if(IS_POST){
			$data = I('post.');
			$data['state'] = 1;
			$where = 'class_3 ='.$data['class_3'].' and store_id='.$data['store_id'];
			if(M('store_bind_class')->where($where)->count()>0){
				$this->ajaxReturn(['status'=>-1,'msg'=>'该店铺已申请过此类目']);
			}
            if($data['class_1']==0 || $data['class_3']==0 || $data['class_3']==0){
                $this->ajaxReturn(['status'=>-1,'msg'=>'必须选择第三级类目']);
            }
			$data['commis_rate'] = M('goods_category')->where(array('id'=>$data['class_3']))->getField('commission');
			if(M('store_bind_class')->add($data)){
				adminLog('添加店铺经营类目，类目编号:'.$data['class_3'].',店铺编号:'.$data['store_id']);
				$this->ajaxReturn(['status'=>1,'msg'=>'添加经营类目成功']);exit;
			}else{
				$this->ajaxReturn(['status'=>-1,'msg'=>'操作失败']);
			}
		}
		$bind_class_list = M('store_bind_class')->where('store_id='.$store_id)->select();
		$goods_class = M('goods_category')->getField('id,name');
		for($i = 0, $j = count($bind_class_list); $i < $j; $i++) {
			$bind_class_list[$i]['class_1_name'] = $goods_class[$bind_class_list[$i]['class_1']];
			$bind_class_list[$i]['class_2_name'] = $goods_class[$bind_class_list[$i]['class_2']];
			$bind_class_list[$i]['class_3_name'] = $goods_class[$bind_class_list[$i]['class_3']];
		}
		$this->assign('bind_class_list',$bind_class_list);
		$cat_list = M('goods_category')->where("parent_id = 0")->select();
		$this->assign('cat_list',$cat_list);
		return $this->fetch();
	}
	
	
	public function apply_class_save(){
		$data = I('post.');
		if($data['act']== 'del'){
			$r = M('store_bind_class')->where("bid=".$data['del_id'])->delete();
			respose(1);
		}else{
			$data = I('get.');
			$r = M('store_bind_class')->where("bid=".$data['bid'])->save(array('state'=>$data['state']));
		}
		if($r){
			$this->success('操作成功',U('Store/apply_class_list'));
		}else{
			$this->error('提交失败');
		}
	}
	
	//店铺申请信息详情
	public function apply_info(){
		$id = I('id');
		$apply = M('store_apply')->where("id=$id")->find();
		$province_name = M('region')->where(array('id'=>$apply['company_province']))->getField('name');
		$city_name = M('region')->where(array('id'=>$apply['company_city']))->getField('name');
		$district_name = M('region')->where(array('id'=>$apply['company_district']))->getField('name');
		$this->assign('province_name',$province_name);
		$this->assign('city_name',$city_name);
		$this->assign('district_name',$district_name);
		$bank_province_name = M('region')->where(array('id'=>$apply['bank_province']))->getField('name');
		$bank_city_name = M('region')->where(array('id'=>$apply['bank_city']))->getField('name');
		$this->assign('bank_province_name',$bank_province_name);
		$this->assign('bank_city_name',$bank_city_name);
		$goods_cates = M('goods_category')->getField('id,name,commission');
		if(!empty($apply['store_class_ids'])){
			$store_class_ids = unserialize($apply['store_class_ids']);
            if(!is_array($store_class_ids)){ $this->error('经营类目数据格式错误！！');};
			foreach ($store_class_ids as $val){
				$cat = explode(',', $val);
				$bind_class_list[] = array(
						'class_1'=>$goods_cates[$cat[0]]['name'],
						'class_2'=>$goods_cates[$cat[1]]['name'],
						'class_3'=>$goods_cates[$cat[2]]['name'],
						'commis_rate'=>$goods_cates[$cat[2]]['commission'],
						'value'=>$val,
				);
			}
			$this->assign('bind_class_list',$bind_class_list);
		}
		$company_type = config('company_type');
		$this->assign('company_type', $company_type);
		$this->assign('apply',$apply);

		$apply_log = M('admin_log')->where(array('log_type'=>1))->select();
		$this->assign('apply_log',$apply_log);
		$this->assign('store_grade',M('store_grade')->select());
		$this->assign('store_class',M('store_class')->select());
		return $this->fetch();
	}
	
	//审核店铺开通申请
	public function review(){
		$data = I('post.');
		//test
		/*
		$url = 'http://www.welive.com/index/api/apiStoreAdd';
		$params = [
				'token' => 'http://bb2t.com',
				'store' => [
						'storeid' => 44,
						'store_name' => '测试1',
						'avatar' => '',
						'web_id' => '',
						'phone' => "",
						'email' => "",
						'city' => "",
				],
				'boss' => [
						'username' => 'pp',
						'password' => '123456',
				]
		];
		$params = http_build_query($params);
		$res = $this->sendToImStore($url,$params);
		var_dump($res);
		die;
		*/
		if($data['id']){
			$apply = M('store_apply')->where(array('id'=>$data['id']))->find();
			if(empty($apply['store_name'])){
				$this->error('店铺名称不能为空.');
			}
			if($apply && M('store_apply')->where("id=".$data['id'])->save($data)){				
				if($data['apply_state'] == 1){ //同意开店 审核通过
					$users = M('users')->where(array('user_id'=>$apply['user_id']))->find();
					if(empty($users)) $this->error('申请会员不存在或已被删除，请检查');
					$time = time();$store_end_time = $time+24*3600*365*$data['store_end_time'];//开店时长
					$store = array('user_id'=>$apply['user_id'],'seller_name'=>$apply['seller_name'],
							'user_name'=>empty($users['email']) ? $users['mobile'] : $users['email'],
							'grade_id'=>empty($data['sg_id']) ? 1 : $data['sg_id'],'store_name'=>$apply['store_name'],'sc_id'=>$apply['sc_id'],
							'company_name'=>$apply['company_name'],'store_phone'=>$apply['store_person_mobile'],
							'store_address'=>empty($apply['store_address']) ? '待完善' : $apply['store_address'] ,
							'store_time'=>$time,'store_state'=>1,'store_qq'=>$apply['store_person_qq'],
							'store_end_time'=>$store_end_time,'province_id'=>$apply['company_province'],
							'city_id'=>$apply['company_city'],'district'=>$apply['company_district']							
					);
					$store_id = M('store')->add($store);//通过审核开通店铺
					if($store_id){
						$seller = array('seller_name'=>$apply['seller_name'],'user_id'=>$apply['user_id'],
							'group_id'=>0,'store_id'=>$store_id,'is_admin'=>1,'add_time'=>time()
						);
						M('seller')->add($seller);//点击店铺管理员
						//绑定商家申请类目
						if(!empty($apply['store_class_ids'])){
							$goods_cates = M('goods_category')->where(array('level'=>3))->getField('id,name,commission');
							$store_class_ids = unserialize($apply['store_class_ids']);
                            if(!is_array($store_class_ids)){ $this->error('经营类目数据格式错误！！');};
							foreach ($store_class_ids as $val){
								$cat = explode(',', $val);
								$bind_class = array('store_id'=>$store_id,'commis_rate'=>$goods_cates[$cat[2]]['commission'],
										'class_1'=>$cat[0],'class_2'=>$cat[1],'class_3'=>$cat[2],'state'=>1);
								M('store_bind_class')->add($bind_class);
							}
						}
					}
					adminLog($apply['store_name'].'开店申请审核通过',1);

					//审核通过  调用iM接口 添加店铺与企业管理人
					$url = 'http://xxxxxxx/index/api/apiStoreAdd';
					$params = [
							'token' => 'http://bb2t.com',
							'store' => [
									'storeid' => $store_id,
									'store_name' => $apply['store_name'],
									'avatar' => '',
									'web_id' => $apply['company_website'],
									'phone' => $apply['store_person_mobile'],
									'email' => $apply['store_person_email'],
									'city' => $apply['store_address'],
							],
							'boss' => [
									'username' => $apply['seller_name'],
									'password' => '123456',
							]
					];
					$params = http_build_query($params);
					$res = $this->sendToImStore($url,$params);
					//var_dump($res);die;

				}else if($data['apply_state'] == 2){  //审核不通过
					adminLog($apply['store_name'].'开店申请审核未通过，备注信息：'.$data['review_msg'],1);
				}
				$this->success('操作成功',U('Store/store_list'));
			}else{
				$this->error('提交失败',U('Store/apply_list'));
			}
		}
	}

	//向im 发送数据
	public function sendToImStore($url,$data)
	{
		//初始化一个curl会话
		$ch = curl_init();
		//设置curl传输选项。

		curl_setopt($ch, CURLOPT_URL, $url); 	//定义表单提交地址
		curl_setopt($ch, CURLOPT_POST, true);   	//定义提交类型 1：POST ；0：GET
		//curl_setopt($ch, CURLOPT_HEADER, 0); 	//定义是否显示状态头 1：显示 ； 0：不显示
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	//定义是否直接输出返回流
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 	//定义提交的数据，这里是XML文件

		//执行一个curl会话
		$res = curl_exec($ch);

		//关闭一个curl会话
		curl_close($ch);
		return $res;
	}


    /**
     * 签约申请
     * @return mixed
     */
	public function reopen_list()
	{
        $reopen_where=[];
        if (IS_POST){
            $seller_name = input('seller_name'); //店主名称
            $store_name = input('store_name');  //店铺名称
            $sc_id = input('sc_id');       //店铺类别
            $grade_id = input('grade_id');       //店铺等级
            $seller_name ? $where['seller_name'] = ['like',"%$seller_name%"]: false; //店主名称
            $sc_id ? $where['sc_id'] = $sc_id : false;       //店铺类别
            $grade_id ? $reopen_where['re_grade_id'] = $grade_id : false;       //店铺等级
            $store_id_arr = Db::name('store')->where($where)->getField('store_id',true);  //查找店铺ID
            $store_ids = implode(',',$store_id_arr);
            $reopen_where['re_store_id'] = ['in',$store_ids];   //店铺id
            $reopen_where['re_store_name'] = ['like',"%$store_name%"];  //店铺名称
        }
        $StoreReopenModel =new  \app\common\model\StoreReopen();
		$count = $StoreReopenModel->where($reopen_where)->count();
        $page = new Page($count,20);
        $StoreReopenObj = $StoreReopenModel->where($reopen_where)->order('re_id desc')->limit($page->listRows,$page->firstRow)->select();
        if($StoreReopenObj){
            $list = collection($StoreReopenObj)->append(['reopen_state'])->toArray();
        }
        $store_grade = Db::name('store_grade')->getField('sg_id,sg_name');
        $store_class = Db::name('store_class')->getField('sc_id,sc_name');
		$this->assign('store_grade', $store_grade);
		$this->assign('store_class', $store_class);
		$this->assign('list', $list);
		$this->assign('page', $page);
		return $this->fetch();
	}

    /**
     * @return mixed
     */
    public function reopen_info(){
        $StoreReopenModel =new  \app\common\model\StoreReopen();
        $save_where = ['re_id'=>input('re_id/d')];
        $reopen = $StoreReopenModel::get($save_where);
        $data = $reopen->append(['reopen_state'])->toArray();
        $this->assign('reopen',$data);
        return $this->fetch();
    }

    /**
     * 审核签约申请
     */
    public function reopen_save(){
        $StoreReopenModel =new  \app\common\model\StoreReopen();
        $save_where = ['re_id'=>input('re_id/d')];
        $re_state = input('re_state/d');
        $admin_note = input('admin_note');
        $reopen = $StoreReopenModel::get($save_where);
        if ($reopen){
            if ($re_state == 2){
                Db::name('store')->where(['store_id'=>$reopen['re_store_id']])->update(['store_end_time'=>$reopen['re_end_time'],'grade_id'=>$reopen['re_grade_id']]);
            }
            $res = $StoreReopenModel->where($save_where)->save(['re_state'=>$re_state,'admin_note'=>$admin_note]);
            if ($res !== false){
                $this->ajaxReturn(['status'=>1,'msg'=>'操作成功！','url'=>U('Store/reopen_list')]);
            }
            $this->ajaxReturn(['status'=>-1,'msg'=>'操作失败！']);
        }else{
            $this->ajaxReturn(['status'=>0,'msg'=>'非法操作！']);
        }
    }

    /**
     * 删除签约申请
     */
    public  function  reopen_del(){
        $StoreReopenModel =new  \app\common\model\StoreReopen();
        $del_where = ['re_id'=>input('re_id/d',0)];
        $res = $StoreReopenModel->where($del_where)->delete();
        if ($res === false)$this->ajaxReturn(['status'=>-1,'msg'=>'删除失败']);
        $this->ajaxReturn(['status'=>1,'msg'=>'删除成功']);
    }

	public function domain_list(){
		$model =  M('store');
		$map['store_state'] = 1;
		$store_domian = I("store_domain");
		if($store_domian) {
			$map['store_domian'] = array('like',"%$store_domian%");
		}else{
			$map['store_domain'] = array('neq',"");
		}
		$seller_name = I('seller_name');
		if($seller_name) $map['seller_name'] = array('like',"%$seller_name%");
		$store_name = I('store_name');
		if($store_name) $map['store_name'] = array('like',"%$store_name%");
		$count = $model->where($map)->count();
		$Page = new Page($count,20);
		$list = $model->where($map)->order('store_id DESC')->limit($Page->firstRow.','.$Page->listRows)->select();
		$this->assign('list',$list);
		
		$show = $Page->show();
		$this->assign('page',$show);
		$this->assign('pager',$Page);
		return $this->fetch();
	}
	
        /**
        *  店家满意度 
        *  2017-9-22 14：45
        */
	public function satisfaction(){
            //A.查询条件
            ($user_name  = I('user_name'))  && $map['u.nickname']    = ['like',"%$user_name%"];            
            ($store_name = I('store_name')) && $map['s.store_name']  = ['like',"%$store_name%"];         
            $prefix      = C('prefix');
            $field       = [
                'oc.*',
                'u.user_id',
                'u.nickname',
                'o.order_id',
                'o.order_sn',
                's.store_id',
                's.store_name',
            ];
            // B. 开始查询
        $count = Db::name('order_comment oc')
                    ->field($field)
                    ->join($prefix . 'users u  ','u.user_id = oc.user_id','left')
                    ->join($prefix . 'store s  ','s.store_id = oc.store_id','left')
                    ->join($prefix . 'order o  ','o.order_id = oc.order_id','left')
                    ->where($map)
                    ->order('order_commemt_id DESC')
                    ->count();
            // B.2 开始分页
            $Page = new Page($count,20);
            $show = $Page->show();
            $list = Db::name('order_comment oc')
                ->field($field)
                ->join($prefix . 'users u  ','u.user_id = oc.user_id','left')
                ->join($prefix . 'store s  ','s.store_id = oc.store_id','left')
                ->join($prefix . 'order o  ','o.order_id = oc.order_id','left')
                ->where($map)
                ->order('order_commemt_id DESC')
                ->limit($Page->firstRow.','.$Page->listRows)->select();
            $this->assign('page',$show);
            $this->assign('pager',$Page);
            $this->assign('list',$list);        
            return $this->fetch();
	}
	
        /**
        *  店铺评分
        *  2017-9-22 15：05
        */
	public function score(){     
            $model =  M('order_comment oc');
            $store_name = I('store_name');
            if($store_name) $map['store_name'] = array('like',"%$store_name%");
             $field=[
                'store_id',
                'COUNT(*) AS number',
                'AVG(describe_score) AS describe_score',
                'AVG(seller_score) AS seller_score',
                'AVG(logistics_score) AS logistics_score', 
            ];
            $count = $model->field($field)->where($map)->group("store_id")->count();
            $Page = new Page($count,20);           
            $list = $model->field($field)
                        ->where($map)
                        ->order('order_commemt_id DESC')
                        ->limit($Page->firstRow.','.$Page->listRows)
                        ->group("store_id")
                        ->select();
            if($list && is_array($list)){
                foreach($list as $k => $v){                                  
                    $v['store_name'] = M('store')->cache(true)
                            ->where('store_id = '.$v['store_id'])
                            ->getfield('store_name');               
                    $order_comment_list[] = $v;
                }
            }             
            $show = $Page->show();
            $this->assign('page',$show);
            $this->assign('pager',$Page);
            $this->assign('list',$order_comment_list);
            return $this->fetch();
	}
}