<?php
namespace app\admin\controller;
use app\admin\logic\GoodsLogic;
use app\common\model\Order;
use think\Db;
use think\Page;

use app\common\model\SellerAddress;

class Report extends Base{
    
    private $tempList = [];
    public function _initialize(){
        parent::_initialize();
    }

    /**
     * 导出预订订单
     */
    public function export_reserve_order(){

        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time.' 00:00:00');
            $end = strtotime($end_time.' 23:59:59');
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }

        // 搜索条件 STORE_ID
        $condition = array('deleted'=>0); // 商家id
//        I('store_id') ? $condition['store_id'] = I('store_id') : false;
//        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

//        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1
        $condition['prom_type'] = 4;

        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

//        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;
//        I('order_status') != '' ?  : false;
//
//        if(I('order_status') != ''){
//            if(I('order_status')=='refund'){
//                $condition['order_status'] = array('in',[5,6,7,8]);//都算退款
//            }else{
//                $condition['order_status'] = I('order_status/d');
//            }
//
//        }


//
//        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
//        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
//        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
//        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
//
//        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');



//        $region = Db::name('region')->cache(true)->getField('id,name');


        $orderList = Db::name('order')->where($condition)->order('order_id')->select();


        $user_id_arr = [];
        foreach ($orderList as $order){
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id','in',$user_id_arr)->field('user_id,email,mobile')->select();
        foreach ($orderList as &$order){
            foreach ($userList as $user){
                if($order['user_id']==$user['user_id']){
                    $order['user_email'] = $user['email'];
                    $order['user_mobile'] = $user['mobile'];
                }
            }
        }



        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->field('store_id,store_name,seller_id')->select();
        $sellers = Db::name('seller')->field('seller_id,seller_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');





        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">日期</td>';


        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">邮箱</td>';


//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '</tr>';


        foreach ($orderList as $k => $val) {

            $tempStore = [];
            $tempSeller = [];

            foreach ($stores as $store){

                if($store['store_id']===$val['store_id']){

                    $tempStore = $store;

                }
            }




            foreach ($sellers as $seller){


                if($seller['seller_id']==$tempStore['seller_id']){

                    $tempSeller = $seller;

                }
            }




            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_id']. '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] . '['.$tempSeller['seller_id'].']' . ' </td>';//经销商名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] . '['.$tempStore['store_id'].']' . ' </td>';//店铺名称

            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s',$val['create_time']) . ' </td>';//日期
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态


            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价
            $orderGoods = Db::name('OrderGoods')->where('order_id', $val['order_id'])->select();
//                ->alias('g')
//                ->join('SpecGoodsPrice p','g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $strGoods = "";
            foreach ($orderGoods as $goods) {

                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'];
                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price']. " SKU：" . $goods['sku'];
                $strGoods .= "<br />";
            }
            unset($orderGoods);
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strGoods . ' </td>';

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式

            /**
             * 发票信息
             *
             */
            $invoice = Db::name('invoice')->where('order_id',$val['order_id'])->find();
            $strInvoice = '';


//            $strInvoice .= "发票类型：" . $INVOICE_TYPE[$val['invoice_type']] . " 索要类型：" . $INVOICE_BLAG_WAY[$invoice['blag_way']];
//            $strInvoice .= "<br />";
//            $strInvoice .= "抬头：" . $val['invoice_title'] ;
//            $strInvoice .= "<br />";
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strInvoice . '</td>';//发票信息
            $user_count = !empty($val['user_email']) ? $val['user_email'] : $val['user_mobile'];
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $user_count . '</td>';//用户账号,邮箱

//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言


            /**
             * 物流信息
             * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
             */

            //$deliver_doc = Db::name('deliveryDoc')->where('order_id',$val['order_id'])->find();

//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['address']}" . ' </td>';//收货地址
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的




//
//
//
//
//
//
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['consignee']}" . ' </td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['address'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['goods_price'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . '</td>';
//            if($val['pay_name'] == ''){
//                $strTable .= '<td style="text-align:left;font-size:12px;">在线支付</td>';
//            }else{
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';
//            }
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->pay_status[$val['pay_status']] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->shipping_status[$val['shipping_status']] . '</td>';

            $strTable .= '</tr>';
        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '超管:导出预约订单');
        exit();

    }
    
    /**
     * 其他的零星数据统计
     */
    public function other(){


        set_time_limit(0);


        $begin = $this->begin;
        $end = $this->end;


        //,'enabled'=>1
        $sellerList = Db::name('seller')->where(['is_deleted'=>0,'is_admin'=>1])->select();
//        echo '<pre>';
//        var_dump($sellerList);die;
        foreach ($sellerList as &$seller){

            //筛选店铺条件
            $stores = Db::name('store')
                ->where('seller_id',$seller['seller_id'])
                ->where('deleted',0)
                ->whereBetween('add_time', [$begin, $end])
                ->field('store_id,store_name')->select();
            
            $store_id_arr = [];
            
            $seller['store_count'] = count($stores);//开设店铺总数
           
            foreach ($stores as $store){
                $store_id_arr[] = $store['store_id'];
            }
    
    
            $reg_user = Db::name('UserStore')
                ->alias('us')
                ->join('tp_users u','us.user_id=u.user_id')
                ->where('us.store_id','in',$store_id_arr)
                ->whereBetween('u.reg_time', [$begin, $end])
                ->field('us.user_id,us.store_id')
                ->select();
            $seller['reg_user_count'] = count($reg_user);//注册数
    
            $active_store_arr = [];//已激活店

            foreach ($stores as $store){
                foreach ($reg_user as $user){
                    if($store['store_id']==$user['store_id']&&!in_array($store['store_id'],$active_store_arr)){
                        $active_store_arr[] = $store['store_id'];
                    }
                }
            }

            $seller['store_active_count'] = count($active_store_arr);//已激活店铺数

            $seller['order_num_count'] = Db::name('order')->where('pay_status', 1)->where('order_status', 'in', [1, 2, 4])->whereBetween('create_time', [$begin, $end])->where('seller_id', $seller['seller_id'])->count();//交易订单总数

            $seller['order_amount_count'] = Db::name('order')->where('seller_id', $seller['seller_id'])->where('order_status', 'in', [1, 2, 4])->whereBetween('create_time', [$begin, $end])->where('pay_status', 1)->sum('total_amount');//交易额

            $order_id_arr = Db::name('order')->where('seller_id', $seller['seller_id'])->where('order_status', 'in', [1, 2, 4])->whereBetween('create_time', [$begin, $end])->where('pay_status', 1)->column('order_id');
            $apple_num_count = Db::name('orderGoods')
                ->alias('o')
                ->join('goods g','o.goods_id=g.goods_id')
                ->where('o.order_id','in',$order_id_arr)->where('g.cat_id3','in',[5,10,12])->sum('goods_num');
            $seller['apple_num_count'] = $apple_num_count;

            $all_num_count = Db::name('orderGoods')
                ->alias('o')
                ->join('goods g','o.goods_id=g.goods_id')
                ->where('o.order_id','in',$order_id_arr)->sum('goods_num');
            $seller['all_num_count'] = $all_num_count;
            if($all_num_count>0){
                $seller['apple_percent'] = round(($apple_num_count/$all_num_count*100),2).'%';//Apple主产品销售量占比
            }else{
                $seller['apple_percent'] = '0%';//Apple主产品销售量占比
            }
            
        }
        
        $this->assign('list',$sellerList);
        return $this->fetch();
    }

    /**
     * 销售概况(订单是已付款，且状态是1,2,4的才统计)
     * @return mixed
     */
    public function index(){
        $now = strtotime(date('Y-m-d'));
        $today['today_amount'] = M('order')->where("add_time>$now AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4)")->sum('total_amount');//今日销售总额
        $today['today_order'] = M('order')->where("add_time>$now and (pay_status=1 or pay_code='cod')")->count();//今日订单数
        $today['cancel_order'] = M('order')->where("add_time>$now AND order_status=3")->count();//今日取消订单
        if ($today['today_order'] == 0) {
            $today['sign'] = round(0, 2);
        } else {
            $today['sign'] = round($today['today_amount'] / $today['today_order'], 2);
        }
        $this->assign('today',$today);
        $select_year = $this->select_year;
        $begin = $this->begin;
        $end = $this->end;
        $res = Db::name("order".$select_year)
            ->field(" COUNT(*) as tnum,sum(total_amount-shipping_price) as amount, FROM_UNIXTIME(add_time,'%Y-%m-%d') as gap ")
            ->where(" add_time >$begin and add_time < $end AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4) ")
            ->group('gap')
            ->select();
        foreach ($res as $val){
            $arr[$val['gap']] = $val['tnum'];
            $brr[$val['gap']] = $val['amount'];
            $tnum += $val['tnum'];
            $tamount += $val['amount'];
        }
        for($i=$this->begin;$i<=$this->end;$i=$i+24*3600){
            $tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
            $tmp_amount = empty($brr[date('Y-m-d',$i)]) ? 0 : $brr[date('Y-m-d',$i)];
            $tmp_sign = empty($tmp_num) ? 0 : round($tmp_amount/$tmp_num,2);
            $order_arr[] = $tmp_num;
            $amount_arr[] = $tmp_amount;
            $sign_arr[] = $tmp_sign;
            $date = date('Y-m-d',$i);
            $list[] = array('day'=>$date,'order_num'=>$tmp_num,'amount'=>$tmp_amount,'sign'=>$tmp_sign,'end'=>date('Y-m-d',$i+24*60*60));
            $day[] = $date;
        }
        rsort($list);
        $this->assign('list',$list);
        $result = array('order'=>$order_arr,'amount'=>$amount_arr,'sign'=>$sign_arr,'time'=>$day);
        $this->assign('result',json_encode($result));
        return $this->fetch();
    }

    /**
     * 销量排行列表
     * @return mixed
     */
    public function saleTop(){
//        ->where("is_send = 1")
//
            $begin = $this->begin;
        $end_time = $this->end;



        $rec_ids = Db::name('order_goods'.$this->select_year)
            ->alias('og')

            ->join('tp_return_goods rg','rg.goods_id = og.goods_id and rg.order_id=og.order_id and rg.rec_id=og.rec_id','LEFT')
            ->where('rg.status','in',[0,1,2,3,4,6])//排除退款成功或者在退款中的
//            ->where("is_send = 1")

            ->whereTime('og.create_time', 'between', [$begin, $end_time])
//            ->cache(true,3600)
            ->column('og.rec_id');



        $count = Db::name('order_goods'.$this->select_year)
            ->alias('og')
            ->join('tp_order o','og.order_id=o.order_id')
            ->where('o.order_status','neq',3)
//            ->where('o.order_status','in',[1,2,4,5])
            ->join('tp_return_goods rg','rg.goods_id = og.goods_id and rg.order_id=og.order_id and rg.rec_id=og.rec_id','LEFT')
            ->where('og.rec_id','not in',$rec_ids)
            //->where('rg.status','not in',[1,2,3,4,6])//排除退款成功或者在退款中的
            ->whereTime('og.create_time', 'between', [$begin, $end_time])
            ->group('og.goods_id')->count();
        $Page = new Page($count,$this->page_size);

        $res = Db::name('order_goods'.$this->select_year)
            ->alias('og')
            ->join('tp_order o','og.order_id=o.order_id')
            ->where('o.order_status','neq',3)//新版本调整好再跟着换
//            ->where('o.order_status','in',[1,2,4,5])
            ->join('tp_return_goods rg','rg.goods_id = og.goods_id and rg.order_id=og.order_id and rg.rec_id=og.rec_id','LEFT')
            ->where('og.rec_id','not in',$rec_ids)
//            ->where('rg.status','not in',[1,2,3,4,6])//排除退款成功或者在退款中的
            ->field('og.goods_id,og.goods_name,og.goods_sn,sum(og.goods_num) as sale_num,sum(og.goods_num*og.goods_price) as sale_amount')
//            ->where("is_send = 1")
            ->group(' og.goods_id,og.goods_sn,og.goods_name')->order('sale_amount DESC')->limit($Page->firstRow,$Page->listRows)
            ->whereTime('og.create_time', 'between', [$begin, $end_time])
//            ->cache(true,3600)
            ->select();

//        $count = Db::name('order_goods'.$this->select_year)
//            ->whereTime('create_time', 'between', [$begin, $end_time])
//            ->group('goods_id')->count();
//        $Page = new Page($count,$this->page_size);
//        $res = Db::name('order_goods'.$this->select_year)
//            ->field('goods_id,goods_name,goods_sn,sum(goods_num) as sale_num,sum(goods_num*goods_price) as sale_amount')
////            ->where("is_send = 1")
//            ->group(' goods_id,goods_sn,goods_name')->order('sale_amount DESC')->limit($Page->firstRow,$Page->listRows)
//            ->whereTime('create_time', 'between', [$begin, $end_time])
////            ->cache(true,3600)
//            ->select();


        $this->assign('list',$res);
        $this->assign('p',I('p/d',1));
        $this->assign('page_size',$this->page_size);
        $this->assign('page',$Page);
        return $this->fetch();
    }

    /**
     * 销售概况订单列表
     * @return mixed
     */
    public function saleOrder(){
        $end_time = $this->begin+24*60*60;
        $order_where = "o.add_time>$this->begin and o.add_time<$end_time";  //交易成功的有效订单
        $order_count = Db::name('order')->alias('o')->where($order_where)->whereIn('order_status','1,2,4')->count();
        $Page = new Page($order_count,20);
        $order_list = Db::name('order')->alias('o')
            ->field('o.order_id,o.order_sn,o.master_order_sn,o.goods_price,o.shipping_price,o.total_amount,o.add_time,u.user_id,u.nickname,s.store_id,s.store_name')
            ->join('users u','u.user_id = o.user_id','left')->join('store s','s.store_id = o.store_id','left')
            ->where($order_where)->whereIn('order_status','1,2,4')
            ->limit($Page->firstRow,$Page->listRows)->select();
        $this->assign('order_list',$order_list);
        $this->assign('page',$Page);
        return $this->fetch();
    }

    /**
     * 会员排行榜
     * @return mixed
     */
    public function userTop(){
        
        $mobile = I('mobile');
        $email = I('email');
        $order_where = [
            'o.add_time'=>['Between',"$this->begin,$this->end"],
            'o.pay_status'=>1
        ];
        if($mobile){
            $user_where['mobile'] =$mobile;
        }
        if($email){
            $user_where['email'] = $email;
        }
        if($user_where){   //有查询单个用户的条件就去找出user_id
            $user_id = Db::name('users')->where($user_where)->getField('user_id');
            $order_where['o.user_id']=$user_id;
        }
        $count = Db::name('order')->alias('o')->where($order_where)->group('o.user_id')->count();  //统计数量
        $Page = new Page($count,$this->page_size);
        $list = Db::name('order')->alias('o')
            ->field('count(o.order_id) as order_num,sum(o.total_amount) as amount,o.user_id,u.mobile,u.email,u.nickname')
            ->join('users u','o.user_id=u.user_id','LEFT')
            ->where($order_where)
            ->group('o.user_id')
            ->order('amount DESC')
            ->limit($Page->firstRow,$Page->listRows)
//            ->cache(true)
            ->select();   //以用户ID分组查询
        $this->assign('pager',$Page);
        $this->assign('p',I('p/d',1));
        $this->assign('page_size',$this->page_size);
        $this->assign('list',$list);
        return $this->fetch();
    }

    /**
     * 用户订单
     * @return mixed
     */
    public function userOrder(){
        $orderModel = new Order();
        $user_id = trim(I('user_id'));
        // 搜索条件
        $condition=[
            'add_time'=>['Between',"$this->begin,$this->end"],
            'pay_status'=>1,
            'user_id' => $user_id,
        ];

        $keyType = I("keytype");
        $keywords = I('keywords','','trim');

        $store_name = ($keyType && $keyType == 'store_name') ? $keywords :  I('store_name','','trim');
        if($store_name)
        {
            $store_id_arr = M('store')->where("store_name like '%$store_name%'")->getField('store_id',true);
            if($store_id_arr)
            {
                $condition['store_id'] = array('in',$store_id_arr);
            }
        }
        $pay_code = input('pay_code');
        $order_sn = ($keyType && $keyType == 'order_sn') ? $keywords : I('order_sn') ;
        $order_sn ? $condition['order_sn'] = trim($order_sn) : false;
        $pay_code != '' ? $condition['pay_code'] = $pay_code : false;   //支付方式


        $count = $orderModel->where($condition)->count();
        $Page  = new Page($count,$this->page_size);
        $orderList = $orderModel->where($condition)
            ->limit("{$Page->firstRow},{$Page->listRows}")->order('add_time desc')->select();
        $store_list = DB::name('store')->getField('store_id,store_name');


        $this->assign('store_list',$store_list);
        $this->assign('orderList',$orderList);
        $this->assign('user_id',$user_id);
        $this->assign('keywords',$keywords);
        $this->assign('page',$Page);// 赋值分页输出
        return $this->fetch();
    }

    public function saleList(){
        $cat_id = I('cat_id',0);
        $brand_id = I('brand_id',0);
        $where = "o.add_time>$this->begin and o.add_time<$this->end and order_status in(1,2,4) ";  //交易成功的有效订单
        if($cat_id>0){
            $where .= " and (g.cat_id1=$cat_id or g.cat_id2=$cat_id or g.cat_id3=$cat_id)";
            $this->assign('cat_id',$cat_id);
        }

        if($brand_id>0){
            $where .= " and g.brand_id=$brand_id";
            $this->assign('brand_id',$brand_id);
        }

        $seller_id = I('seller_id',0);

        if($seller_id){
            $store_ids = Db::name('store')->where('seller_id',$seller_id)->column('store_id');
            if(count($store_ids)>0){
                $store_ids_str = implode(',',$store_ids);
                $where .= " and g.store_id in ($store_ids_str)";
            }

        }

        $count = Db::name('order_goods')->alias('og')
            ->join('order o','og.order_id=o.order_id ','left')
            ->join('goods g','og.goods_id = g.goods_id','left')
            ->where($where)->count();  //统计数量
        $Page = new Page($count,20);
        $show = $Page->show();

        $res = Db::name('order_goods')->alias('og')->field('og.*,o.order_sn,o.shipping_name,o.pay_name,o.add_time,u.email')
            ->join('order o', 'og.order_id=o.order_id ', 'left')
            ->join('goods g', 'og.goods_id = g.goods_id', 'left')
            ->join('users u', 'o.user_id = u.user_id', 'left')
            ->where($where)->order('FROM_UNIXTIME(o.add_time,"%Y-%m-%d") desc,og.goods_num desc')
            ->limit($Page->firstRow,$Page->listRows)
            ->select();
        $this->assign('list',$res);
        $this->assign('pager',$Page);
        $this->assign('page',$show);

        $GoodsLogic = new GoodsLogic();
        $brandList = $GoodsLogic->getSortBrands();  //获取排好序的品牌列表
        $categoryList = $GoodsLogic->getSortCategory(); //获取排好序的分类列表
        $this->assign('categoryList',$categoryList);
        $this->assign('brandList',$brandList);


        $sellerList = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
        $this->assign('sellerList', $sellerList);
        return $this->fetch();
    }

    public function user(){
        $today = strtotime(date('Y-m-d'));
        $month = strtotime(date('Y-m-01'));
        $user['today'] = D('users')->where("reg_time>$today")->count();//今日新增会员
        $user['month'] = D('users')->where("reg_time>$month")->count();//本月新增会员
        $user['total'] = D('users')->count();//会员总数
        $user['user_money'] = D('users')->sum('user_money');//会员余额总额
        $res = M('order')->cache(true)->distinct(true)->field('user_id')->select();
        $user['hasorder'] = count($res);
        $this->assign('user',$user);
        $sql = "SELECT COUNT(*) as num,FROM_UNIXTIME(reg_time,'%Y-%m-%d') as gap from __PREFIX__users where reg_time>$this->begin and reg_time<$this->end group by gap";
        $new = DB::query($sql);//新增会员趋势
        foreach ($new as $val){
            $arr[$val['gap']] = $val['num'];
        }

        for($i=$this->begin;$i<=$this->end;$i=$i+24*3600){
            $brr[] = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
            $day[] = date('Y-m-d',$i);
        }
        $result = array('data'=>$brr,'time'=>$day);
        $this->assign('result',json_encode($result));
        return $this->fetch();
    }

    /**
     * 运营概况详情
     * @return mixed
     */
    public function financeDetail(){
        $begin = $this->begin;
        $end_time = $this->begin+24*60*60;
        $order_where = [
            'o.pay_status'=>1,
            'o.shipping_status'=>['in','0,1'],
//            'og.is_send'=>['in','1,2']
        ];  //交易成功的有效订单
        $order_count = Db::name('order')->alias('o')
            ->join('order_goods og','o.order_id = og.order_id','left')->join('users u','u.user_id = o.user_id','left')
            ->whereTime('o.add_time', 'between', [$begin, $end_time])->where($order_where)
            ->group('o.order_id')->count();
        $Page = new Page($order_count,50);

        $order_list = Db::name('order')->alias('o')
            ->field('o.*,u.user_id,u.nickname,SUM(og.cost_price) as coupon_amount')
            ->join('order_goods og','o.order_id = og.order_id','left')->join('users u','u.user_id = o.user_id','left')
            ->where($order_where)->whereTime('o.add_time', 'between', [$begin, $end_time])
            ->group('o.order_id')->limit($Page->firstRow,$Page->listRows)->select();
        $this->assign('order_list',$order_list);
        $this->assign('page',$Page);
        return $this->fetch();
    }

    /**
     * 财务统计
     * @return mixed
     */
    public function finance(){
        $begin = $this->begin;
        $end_time = $this->end;
//        ,'o.shipping_status'=>1
        $order = Db::name('order')->alias('o')
            ->where(['o.pay_status'=>1])->whereTime('o.add_time', 'between', [$begin, $end_time])
            ->order('o.add_time asc')->field('o.*')->select();  //以时间升序
        $order_id_arr = get_arr_column($order,'order_id');
        $order_ids = implode(',',$order_id_arr);            //订单ID组
    
//        'is_send'=>['in','1,2'],
        $order_goods = Db::name('order_goods')->where(['order_id'=>['in',$order_ids]])->group('order_id')
            ->order('order_id asc')->getField('order_id,sum(goods_num*cost_price) as cost_price,sum(goods_num*member_goods_price) as goods_amount');  //订单商品退货的不算
        $frist_key = key($order);  //第一个key
        $sratus_date = $begin;//strtotime(date('Y-m-d',$order["$frist_key"]['add_time']));  //有数据那天为循环初始时间，大范围查询可以避免前面输出一堆没用的数据
        $key = array_keys($order);
        $lastkey = end($key);//最后一个key
        $end_date = $end_time;//strtotime(date('Y-m-d',$order["$lastkey"]['add_time']))+24*3600;  //数据最后时间为循环结束点，大范围查询可以避免前面输出一堆没用的数据
        for($i=$sratus_date;$i<=$end_date;$i=$i+24*3600){   //循环时间
            $date = $day[] = date('Y-m-d',$i);
            $everyday_end_time = $i+24*3600;
            $goods_amount=$cost_price =$shipping_amount=$coupon_amount=$order_prom_amount=$total_amount=0.00; //初始化变量
            foreach ($order as $okey => $oval){   //循环订单
                $for_order_id = $oval['order_id'];
                if (!isset($order_goods["$for_order_id"])){
                    unset($order[$for_order_id]);           //去掉整个订单都了退货后的
                }
                if($oval['add_time'] >= $i && $oval['add_time']<$everyday_end_time){      //统计同一天内的数据
                    $goods_amount      += $oval['goods_price'];
                    $total_amount      += $oval['total_amount'];
                    $cost_price        += $order_goods["$for_order_id"]['cost_price']; //订单成本价
                    $shipping_amount   += $oval['shipping_price'];
                    $coupon_amount     += $oval['coupon_price'];
                    $order_prom_amount += $oval['order_prom_amount'];
                    unset($order[$okey]);  //省的来回循环
                }
            }
            //拼装输出到图表的数据
            $goods_arr[]    = $goods_amount;
            $total_arr[]    = $total_amount;
            $cost_arr[]     = $cost_price ;
            $shipping_arr[] = $shipping_amount;
            $coupon_arr[]   = $coupon_amount;

            $list[] = [
                'day'=>$date,
                'goods_amount'      => $goods_amount,
                'total_amount'      => $total_amount,
                'cost_amount'       => $cost_price,
                'shipping_amount'   => $shipping_amount,
                'coupon_amount'     => $coupon_amount,
                'order_prom_amount' => $order_prom_amount,
                'end'=>$everyday_end_time,
            ];  //拼装列表
        }
        rsort($list);
        $this->assign('list',$list);
        $result = ['goods_arr'=>$goods_arr,'cost_arr'=>$cost_arr,'shipping_arr'=>$shipping_arr,'coupon_arr'=>$coupon_arr,'time'=>$day];
//        echo '<pre>';
//        var_dump($result);die;
        $this->assign('result',json_encode($result));
        return $this->fetch();
    }


    /**
     * @desc 图表分析
     */
    public function charts(){

        $now = strtotime(date('Y-m-d'));
        $today['today_amount'] = M('order')->where("add_time>$now AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4)")->sum('total_amount');//今日销售总额
        $today['today_order'] = M('order')->where("add_time>$now and (pay_status=1 or pay_code='cod')")->count();//今日订单数
        $today['cancel_order'] = M('order')->where("add_time>$now AND order_status=3")->count();//今日取消订单
        if ($today['today_order'] == 0) {
            $today['sign'] = round(0, 2);
        } else {
            $today['sign'] = round($today['today_amount'] / $today['today_order'], 2);
        }
        $this->assign('today',$today);
        $select_year = $this->select_year;
        $begin = $this->begin;
        $end = $this->end;


        $res = Db::name("order".$select_year)
            ->field(" COUNT(*) as tnum,sum(total_amount-shipping_price) as amount, FROM_UNIXTIME(add_time,'%Y-%m-%d') as gap ")
            ->where(" add_time >$begin and add_time < $end AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4) ")
            ->group('gap')
            ->select();
        foreach ($res as $val){
            $arr[$val['gap']] = $val['tnum'];
            $brr[$val['gap']] = $val['amount'];
            $tnum += $val['tnum'];
            $tamount += $val['amount'];
        }

        //获取每天注册的用户人数
        $users = Db::name("users")
            ->field(" COUNT(*) as tusernum,FROM_UNIXTIME(reg_time,'%Y-%m-%d') as gap ")
            ->where(" reg_time >$begin and reg_time < $end ")
            ->group('gap')
            ->select();

        foreach ($users as $val){
            $crr[$val['gap']] = $val['tusernum'];
        }


        for($i=$this->begin;$i<=$this->end;$i=$i+24*3600){
            $tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
            $temp_user_num = empty($crr[date('Y-m-d',$i)]) ? 0 : $crr[date('Y-m-d',$i)];
            $tmp_amount = empty($brr[date('Y-m-d',$i)]) ? 0 : $brr[date('Y-m-d',$i)];
            $tmp_sign = empty($tmp_num) ? 0 : round($tmp_amount/$tmp_num,2);
            $user_arr[] = $temp_user_num;
            $order_arr[] = $tmp_num;
            $amount_arr[] = $tmp_amount;
            $sign_arr[] = $tmp_sign;
            $date = date('Y-m-d',$i);
            $list[] = array('day'=>$date,'order_num'=>$tmp_num,'amount'=>$tmp_amount,'sign'=>$tmp_sign,'user'=>$temp_user_num,'end'=>date('Y-m-d',$i+24*60*60));
            $day[] = $date;
        }
        rsort($list);
        $this->assign('list',$list);


        //获取所有成功的订单
        $where_order = "o.create_time>$this->begin and o.create_time<$this->end and order_status in(1,2,4) ";  //交易成功的有效订单
//        $count = Db::name('order_goods')->alias('og')
//            ->join('order o','og.order_id=o.order_id ','left')
//            ->join('goods g','og.goods_id = g.goods_id','left')
//            ->where($where_order)->count();  //统计数量


        $orderGoodsList = Db::name('order_goods')
            ->alias('og')
            ->field('COUNT(*) as tgoodsnum,og.goods_name,og.goods_id,sum(og.goods_num) as total')
            ->join('order o','og.order_id=o.order_id ','left')
            ->where($where_order)
            ->order('total desc')
            ->group('og.goods_id,og.goods_name')
            ->select();

        foreach ($orderGoodsList as $order_goods){
            $goodsArr[] = [
              'name'=>$order_goods['goods_name'],
                'value'=>$order_goods['total']
            ];
        }



//
//        {value:335, name:'直接访问'},
//        {value:310, name:'邮件营销'},
//        {value:234, name:'联盟广告'},
//        {value:135, name:'视频广告'},
//        {value:1548, name:'搜索引擎'}



        $result = array('order'=>$order_arr,'amount'=>$amount_arr,'sign'=>$sign_arr,'time'=>$day,'user'=>$user_arr,'goods'=>$goodsArr);
        $this->assign('result',json_encode($result));


        $base['totalSeller'] = Db::name('seller')->where('enabled','eq',1)->count();
        $base['totalOrder'] = Db::name('order')->where("(pay_status=1 or pay_code='cod') and order_status in(1,2,4) ")->count();

        $base['totalSale'] = 0;
        $allOrder = Db::name('order')->where("(pay_status=1 or pay_code='cod') and order_status in(1,2,4) ")->field('total_amount,order_id')->select();
        foreach ($allOrder as $val){
            $base['totalSale'] += $val['total_amount'];
        }

        $base['totalUser'] = Db::name('users')->count();
        $this->assign('base',$base);


        return $this->fetch();
    }


    /**
     * @desc 经销商排行分析
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function sellerTop(){

        $now = strtotime(date('Y-m-d'));
        $tnum = 0;//总订单数量
        $tamount = 0;//总成交额
        $select_year = $this->select_year;
        $begin = $this->begin;
        $end = $this->end;
        $res = Db::name("order".$select_year)
            ->field(" COUNT(*) as tnum,sum(total_amount) as amount,seller_id ")
//            ->field(" COUNT(*) as tnum,sum(total_amount-shipping_price) as amount,seller_id ")//数据不对核查一下20180925
            ->where(" create_time >$begin and create_time < $end AND pay_status=1  and order_status in(1,2,4) and deleted=0 ")
//            ->where(" create_time >$begin and create_time < $end AND (pay_status=1 or pay_code='cod') and order_status in(1,2,4) ")//数据不对核查一下20180925
//            ->group('gap')
            ->group('seller_id')
            ->order('amount desc')
            ->select();

        $sellerList = Db::name('seller')->field('seller_id,seller_name')->select();

        foreach ($res as &$top){
            foreach ($sellerList as $seller){
                if($top['seller_id']==$seller['seller_id']){
                    $top['seller_name'] = $seller['seller_name'];
                }
            }


        }

//        var_dump($res);die;
//        foreach ($res as $val){
//            $arr[$val['gap']] = $val['tnum'];//订单额
//            $brr[$val['gap']] = $val['amount'];//成交额
//            $tnum += $val['tnum'];
//            $tamount += $val['amount'];
//        }
//
//        for($i=$this->begin;$i<=$this->end;$i=$i+24*3600){
//            $tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
//            $tmp_amount = empty($brr[date('Y-m-d',$i)]) ? 0 : $brr[date('Y-m-d',$i)];
//            $tmp_sign = empty($tmp_num) ? 0 : round($tmp_amount/$tmp_num,2);
//            $order_arr[] = $tmp_num;
//            $amount_arr[] = $tmp_amount;
//            $sign_arr[] = $tmp_sign;
//            $date = date('Y-m-d',$i);
//            $list[] = array('day'=>$date,'order_num'=>$tmp_num,'amount'=>$tmp_amount,'sign'=>$tmp_sign,'end'=>date('Y-m-d',$i+24*60*60));
//            $day[] = $date;
//        }
//        rsort($list);
        $this->assign('list',$res);
//        $result = array('order'=>$order_arr,'amount'=>$amount_arr,'sign'=>$sign_arr,'time'=>$day);
//        $this->assign('result',json_encode($result));
        return $this->fetch();

    }


    /**
     * @desc 指定经销商店铺排行
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function storeTop(){

        $seller_id = I('seller_id');


        $now = strtotime(date('Y-m-d'));
        $tnum = 0;//总订单数量
        $tamount = 0;//总成交额
        $select_year = $this->select_year;
        $begin = $this->begin;
        $end = $this->end;


        $res = Db::name("order".$select_year)
            ->field(" COUNT(*) as tnum,sum(total_amount-shipping_price) as amount,seller_id,store_id ")
            ->where(" create_time >$begin and create_time < $end AND (pay_status=1) and order_status in(1,2,4) and seller_id=$seller_id ")
//            ->group('gap')
            ->group('store_id')
            ->order('amount desc')
            ->select();

        $storeList = Db::name('store')->field('store_id,store_name')->select();

        foreach ($res as &$top){
            foreach ($storeList as $store){
                if($top['store_id']==$store['store_id']){
                    $top['store_name'] = $store['store_name'];
                }
            }


        }

//        var_dump($res);die;
//        foreach ($res as $val){
//            $arr[$val['gap']] = $val['tnum'];//订单额
//            $brr[$val['gap']] = $val['amount'];//成交额
//            $tnum += $val['tnum'];
//            $tamount += $val['amount'];
//        }
//
//        for($i=$this->begin;$i<=$this->end;$i=$i+24*3600){
//            $tmp_num = empty($arr[date('Y-m-d',$i)]) ? 0 : $arr[date('Y-m-d',$i)];
//            $tmp_amount = empty($brr[date('Y-m-d',$i)]) ? 0 : $brr[date('Y-m-d',$i)];
//            $tmp_sign = empty($tmp_num) ? 0 : round($tmp_amount/$tmp_num,2);
//            $order_arr[] = $tmp_num;
//            $amount_arr[] = $tmp_amount;
//            $sign_arr[] = $tmp_sign;
//            $date = date('Y-m-d',$i);
//            $list[] = array('day'=>$date,'order_num'=>$tmp_num,'amount'=>$tmp_amount,'sign'=>$tmp_sign,'end'=>date('Y-m-d',$i+24*60*60));
//            $day[] = $date;
//        }
//        rsort($list);
        $this->assign('list',$res);
//        $result = array('order'=>$order_arr,'amount'=>$amount_arr,'sign'=>$sign_arr,'time'=>$day);
//        $this->assign('result',json_encode($result));
        return $this->fetch();

    }


    public function export()
    {
        $action = I('action', null);


        switch ($action) {
            case 'pay_order':
                $this->exportOrder();
                break;
            case 'pay_order_refund':
                $this->exportOrder_refund();
                break;
            case 'reserve_order':
                $this->exportReserveOrder();
                break;
            case 'no_pay_order':
                $this->export_reserve_order();
                break;
            case 'store':
                $this->export_store();
                break;
            default:
                return $this->fetch();
                break;
        }


    }


    /**
     * 导出店铺相关数据
     * 2018-09-27
     */
    public function export_store()
    {
        set_time_limit(0);

//        select * from tp_order WHERE master_order_sn is not null AND  master_order_sn <> '' and pay_status=1 AND pay_time>1537459199;
        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time.' 00:00:00');
            $end = strtotime($end_time.' 23:59:59');
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }
        

        
        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')
            ->alias('s')
            ->join('tp_store_apply sa','s.store_id=sa.store_id','LEFT')
            //->where('s.validate',1)
            ->where('s.deleted',0)
            ->whereBetween('s.add_time', [$begin, $end])
            ->field('s.*,sa.company_staff,sa.seller_address_id,sa.protocol_createtime,sa.protocol_starttime,sa.protocol_endtime')
            ->select();
        
        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
    


        $SellerAddress = new SellerAddress();
        $address_list = $SellerAddress->where('type', 2)->order('seller_address_id desc')->select();
        

//        $condition['order_status'] = array('in', [1, 2, 4]);
        $condition['pay_status'] = array('gt', 0);
        
        
//        $orderList = Db::name('order')->where($condition)->field('order_id,sum(total_amount) as amount_count,count(*) as pay_count_num,store_id,seller_id')->group('store_id')->select();

        $orderList = Db::name('order')->field('order_id,total_amount,store_id,seller_id,pay_status,order_status')->select();


        $storeUser = Db::name('UserStore')
            ->alias('su')
            ->join('Users u','su.user_id=u.user_id')
            ->join('store s','su.store_id=s.store_id')
            ->where('s.deleted',0)
            ->whereBetween('u.reg_time', [$begin, $end])//如果显示的是指定时间内的，确实需要有这一行
            ->field('count(*) as user_total,su.store_id')->group('store_id')->select();
        //->whereBetween('u.reg_time', [$begin, $end])

        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">经销商id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺id</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">验证机制</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">公司名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">签约时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="300">协议有效时间</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">覆盖员工人数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">对应ESC门店</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺URL</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">邮箱后缀</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">创建日期</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">注册人数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">订单总数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">成交订单数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">成交金额总数</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">审核状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺状态</td>';

        $strTable .= '</tr>';


        foreach ($stores as $k => $val) {

            $tempSeller = [];

//            $OrderTotalAmount = $val['amount_count'];
//            $payOrderCount = $val['pay_count_num'];


            $userCount = $OrderCount = $OrderTotalAmount = $payOrderCount = 0;

            foreach ($orderList as $order) {

                if ($order['store_id'] == $val['store_id']) {
                    $OrderCount++;
                    //包含退款的，和其他统计口径一致
                    if (in_array($order['order_status'], [1, 2, 4]) && $order['pay_status'] > 0) {
                        $OrderTotalAmount += $order['total_amount'];
                        $payOrderCount++;
                    }

                }
            }

            foreach ($sellers as $seller) {
                //print_r($seller);
                if ($seller['seller_id'] === $val['seller_id']) {

                    $tempSeller = $seller;

                }
            }

            foreach ($storeUser as $user) {
                if ($user['store_id'] == $val['store_id']) {
                    $userCount = $user['user_total'];
                }
            }
            
            
            foreach ($address_list as $address){
                if($address['seller_address_id']==$val['seller_address_id']){
                    $val['seller_full_address'] = $address['fulladdress'];
                    $val['seller_address_title'] = $address['title'];
                }
            }
    
            if(isset($val['protocol_createtime'])){
                $val['protocol_createtime'] = date('Y-m-d',strtotime($val['protocol_createtime']));
            }
            
            if(isset($val['protocol_starttime'])){
                $val['protocol_starttime'] = date('Y-m-d',strtotime($val['protocol_starttime']));
            }
            
            
            if(isset($val['protocol_endtime'])){
                $val['protocol_endtime'] = date('Y-m-d',strtotime($val['protocol_endtime']));
            }


            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $tempSeller['seller_id'] . '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $tempSeller['seller_name'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_id'] . ' </td>';//店铺名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_name'] . ' </td>';//店铺名称
    
    
//            $strTable .= '<td style="text-align:center;font-size:12px;" width="120">验证机制</td>';
//            $strTable .= '<td style="text-align:center;font-size:12px;" width="120">公司名称</td>';
//            $strTable .= '<td style="text-align:center;font-size:12px;" width="120">签约时间</td>';
//            $strTable .= '<td style="text-align:center;font-size:12px;" width="300">协议有效时间</td>';
    
            
            $validate_channel = '邀请码';
            if($val['validate_channel'] =='email'){
                $validate_channel = '邮箱';
            }
    
         
    
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $validate_channel . ' </td>';//验证机制
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['company_name'] . ' </td>';//公司名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['protocol_createtime'] . ' </td>';//签约时间
    
    
            $protocol_str ='';
            if($val['protocol_starttime']&&$val['protocol_endtime']){
                $protocol_str = $val['protocol_starttime'].'-'.$val['protocol_endtime'];
            }
            
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $protocol_str  . ' </td>';//协议有效时间
            
            
    
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['company_staff'] . ' </td>';//覆盖员工数量
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['seller_address_title'] . ' </td>';//ESC门店名称
            

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_domain'] . ' </td>';//URL
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['store_email'] . ' </td>';//邮箱后缀
            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['add_time']) . ' </td>';//创建日期

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $userCount . ' </td>';//注册人数


            $strTable .= '<td style="text-align:left;font-size:12px;">' . $OrderCount . ' </td>';//订单总数
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $payOrderCount . ' </td>';//成交订单总数
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $OrderTotalAmount . ' </td>';//成交订单总额
    
    
            $arr = [1=>'通过',0=>'待审核',-1=>'未通过'];
            $validate_text = '待审核';
            
            if(isset($arr[$val['validate']])){
                $validate_text = $arr[$val['validate']];
            }
            
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $validate_text . ' </td>';//店铺审核状态
            
            
            $status = $val['store_enabled'] == 1 ? '开启' : '关闭';
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $status . ' </td>';//店铺状态


            $strTable .= '</tr>';
        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '超管：店铺数据汇总');
        exit();
    }


    /**
     * 预定订单转化数据
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function exportReserveOrder()
    {


//        select * from tp_order WHERE master_order_sn is not null AND  master_order_sn <> '' and pay_status=1 AND pay_time>1537459199;
        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time.' 00:00:00');
            $end = strtotime($end_time.' 23:59:59');
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }
        // 搜索条件 STORE_ID
        $condition = array('deleted' => 0);
        
        //预定订单
        $condition['master_order_sn'] = array('exp', ' is not null AND master_order_sn !=""');
//        $condition['master_order_sn'] = array('exp', ' !=""');
//        $condition['master_order_sn'] = array('neq', '');
        
        $condition['pay_status'] = array('in', [1, 3]);
        
        
        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

//        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;


//        $region = Db::name('region')->cache(true)->getField('id,name');
        
        
        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order')->where($condition)->order('order_id')->select();

//        var_dump($condition);
//        die;
        
        $user_id_arr = [];
        foreach ($orderList as $order) {
            $user_id_arr[] = $order['user_id'];
        }
        
        $userList = Db::name('users')->where('user_id', 'in', $user_id_arr)->field('user_id,email,mobile')->select();
        foreach ($orderList as &$order) {
            foreach ($userList as $user) {
                if ($order['user_id'] == $user['user_id']) {
                    $order['user_email'] = $user['email'];
                    $order['user_mobile'] = $user['mobile'];
                }
            }
        }
        
        
        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->field('store_id,store_name')->select();
        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');
        
        
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商ID</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">下单时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">支付时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品名称 </td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品属性</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品数量</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">SKU名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">单价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">优惠金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
    
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">索要类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票抬头</td>';
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
        
        
        $strTable .= '</tr>';
    
    
        $cat3_arr = Db::name('GoodsCategory')->column('id,name');
    
    
    
        foreach ($orderList as $k => $val) {
        
            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {
            
                if ($store['store_id'] === $val['store_id']) {
                
                    $tempStore = $store;
                
                }
            }

            foreach ($sellers as $seller) {

                if ($seller['seller_id'] === $val['seller_id']) {

                    $tempSeller = $seller;

                }
            }
        
            $orderGoods = Db::name('OrderGoods')
                ->alias('og')
                ->join('tp_goods g', 'g.goods_id=og.goods_id')
                ->where('order_id', $val['order_id'])
                ->field('og.*,g.cat_id3')
                ->select();
        
            $row = count($orderGoods);
        
        
        
        
        
            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价

//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
        
            foreach ($orderGoods as $key => $goods) {
            
                $strGoods = "";
            
                $strTable .= '<tr>';
            
                $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
                $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] .' </td>';//经销商名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_id'] . ' </td>';//经销商id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] .  ' </td>';//店铺名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_id']. ' </td>';//店铺id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . ' </td>';//日期
                $pay_time = '';
                if($val['pay_time']){
                    $pay_time = date('Y-m-d H:i:s', $val['pay_time']);
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' .  $pay_time . ' </td>';//支付时间
    
                if(!isEmptyObject($val['master_order_sn'])){
                    $order_type ='预约转化';
                }else{
                    $order_type = '直接购买';
                }
            
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $order_type . ' </td>';//订单类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态
            
            
            
            
                if (isset($cat3_arr[$goods['cat_id3']])) {
                    $cat3_name = $cat3_arr[$goods['cat_id3']];
                } else {
                    $cat3_name = '';
                }

//                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'] . ' 商品类别:' . $cat3_name . ' ';
//                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
//                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];
            
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $cat3_name . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['spec_key_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_num'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['sku'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['final_price'] . ' </td>';
            
            
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式
            
                $user_count = !empty($val['user_email']) ? $val['user_email'] : $val['user_mobile'];
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $user_count . '</td>';//用户账号,邮箱
            
            
            
            
                /**
                 * 物流信息
                 * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
                 */
            
                
               
                $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话

                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['province'], $deliver_doc['city'], $deliver_doc['district']])->column('name');
                $sendFullAddress = implode('', $region) . $val['address'];
                //修复没有发货记录的时候地址不显示的问题
//                if(isEmptyObject($sendFullAddress)){
//                    $sendFullAddress = $val['address'];
//                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sendFullAddress . ' </td>';//收货地址
                
                

//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
            
              
            
                /**
                 * 发票信息
                 *
                 */
                $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();
            
            
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_TYPE[$val['invoice_type']] . '</td>';//发票类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_BLAG_WAY[$invoice['blag_way']] . '</td>';//索要类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['invoice_title'] . '</td>';//发票抬头
             
            
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言
            
            
                $strTable .= '</tr>';
            
            }
//            unset($orderGoods);
        
        
        
        }
        $strTable .= '</table>';
        
        unset($orderList);
        downloadExcel($strTable, '超管:导出预订单转化数据');
        exit();
        
        
    }
    
    
    
    
    
    protected function exportReserveOrder_old()
    {


//        select * from tp_order WHERE master_order_sn is not null AND  master_order_sn <> '' and pay_status=1 AND pay_time>1537459199;
        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time);
            $end = strtotime($end_time);
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }
        // 搜索条件 STORE_ID
        $condition = array('deleted' => 0);

        //
        $condition['master_order_sn'] = array('exp', ' is not null AND master_order_sn !=""');
//        $condition['master_order_sn'] = array('exp', ' !=""');
//        $condition['master_order_sn'] = array('neq', '');

        $condition['pay_status'] = 1;


        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

//        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;


//        $region = Db::name('region')->cache(true)->getField('id,name');


        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order')->where($condition)->order('order_id')->select();

//        var_dump($condition);
//        die;

        $user_id_arr = [];
        foreach ($orderList as $order) {
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id', 'in', $user_id_arr)->field('user_id,email,mobile')->select();
        foreach ($orderList as &$order) {
            foreach ($userList as $user) {
                if ($order['user_id'] == $user['user_id']) {
                    $order['email'] = $user['email'];
                    $order['mobile'] = $user['mobile'];
                }
            }
        }


        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->field('store_id,store_name')->select();
        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');


        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">日期</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票信息</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';

//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货状态</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">物流单号</td>';
        $strTable .= '</tr>';


        foreach ($orderList as $k => $val) {
            //print_r($val);
            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {
                //print_r($store);
                if ($store['store_id'] === $val['store_id']) {

                    $tempStore = $store;

                }
            }
            //print_r($tempStore);
            foreach ($sellers as $seller) {
                //print_r($seller);
                if ($seller['seller_id'] === $val['seller_id']) {

                    $tempSeller = $seller;

                }
            }
            //print_r($tempSeller);
            //exit;

            $strTable .= '<tr>';
            //$val['order_id']
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
            $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] . '[' . $tempSeller['seller_id'] . ']' . ' </td>';//店铺名称
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] . '[' . $tempStore['store_id'] . ']' . ' </td>';//店铺名称


            $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . ' </td>';//日期
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态


            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价
            $orderGoods = Db::name('OrderGoods')->where('order_id', $val['order_id'])->select();
//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $strGoods = "";
            foreach ($orderGoods as $goods) {

                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'];
                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];
                $strGoods .= "<br />";
            }
            unset($orderGoods);
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strGoods . ' </td>';

            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . ' </td>';//应付金额
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式

            /**
             * 发票信息
             *
             */
            $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();
            $strInvoice = '';


            $strInvoice .= "发票类型：" . $INVOICE_TYPE[$val['invoice_type']] . " 索要类型：" . $INVOICE_BLAG_WAY[$invoice['blag_way']];
            $strInvoice .= "<br />";
            $strInvoice .= "抬头：" . $val['invoice_title'];
            $strInvoice .= "<br />";
            
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $strInvoice . '</td>';//发票信息
            $user_count = !empty($val['email']) ? $val['email'] : $val['mobile'];
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $user_count . '</td>';//用户账号,邮箱

//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言


            /**
             * 物流信息
             * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
             */

//            $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
//
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['address']}" . ' </td>';//收货地址
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的


//
//
//
//
//
//
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . "{$region[$val['province']]},{$region[$val['city']]},{$region[$val['district']]},{$region[$val['twon']]}{$val['consignee']}" . ' </td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['address'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['goods_price'] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['order_amount'] . '</td>';
//            if($val['pay_name'] == ''){
//                $strTable .= '<td style="text-align:left;font-size:12px;">在线支付</td>';
//            }else{
//                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';
//            }
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->pay_status[$val['pay_status']] . '</td>';
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $this->shipping_status[$val['shipping_status']] . '</td>';

            $strTable .= '</tr>';
        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '超管:导出预订单转化数据');
        exit();


    }
    
    
    /**
     * 判断是不是需要跨行
     */
    protected function getIsHasMultiOrder($arr,$order_id){
        //是否在里面
        if(in_array($order_id,$this->tempList)){
            return 0;
        }
        
        $i = 0;
        
        //返回数量
        foreach ($arr as $item){
            if($item['order_id']===$order_id){
                $i++;
            }
        }
    
        //表示已经做了colspan了
        $this->tempList[] = $order_id;
        
        if($i===0){
            $i=1;
        }
        
        return $i;
        
    }


    /**
     * 经销商支付交易订单导出内容
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function exportOrder()
    {
        set_time_limit(0);
        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time.' 00:00:00');
            $end = strtotime($end_time.' 23:59:59');
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }
        
        
        // 搜索条件 STORE_ID
        $condition = array('deleted' => 0); // 商家id

        $condition['order_status'] = array('in', [1, 2, 4]);



//        'seller_id' => SELLER_ID,
//        I('store_id') ? $condition['store_id'] = I('store_id') : false;
//        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

//        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1

        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }

        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;


//
//
//        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
//        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
//        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
//        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
//
//        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
//        $condition['prom_type'] = array('lt',5);


//        $count = Db::name('order'.$select_year)->where($condition)->count();


//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }


        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
//        $order_id_arr = I('order_id_arr',null);
//        if($order_id_arr&&$order_id_arr!=''){
//            $condition = [];
//            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
//        }


        $region = Db::name('region')->cache(true)->getField('id,name');


//        var_dump($where);die;

        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order')->where($condition)->order('order_id')->select();


        $user_id_arr = [];
        foreach ($orderList as $order) {
            $user_id_arr[] = $order['user_id'];
        }

        $userList = Db::name('users')->where('user_id', 'in', $user_id_arr)->field('user_id,email,mobile')->select();
        foreach ($orderList as &$order) {
            foreach ($userList as $user) {
                if ($order['user_id'] == $user['user_id']) {
                    $order['user_email'] = $user['email'];
                    $order['user_mobile'] = $user['mobile'];
                }
            }
        }


        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->field('store_id,store_name')->select();
        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');


        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">下单时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">支付时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品名称 </td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品属性</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品数量</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">SKU名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">单价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">成交价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">优惠金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';

        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">预约时间</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">快递公司</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;" >物流单号</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货联系方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货地址</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货时间</td>';
    
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">索要类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票抬头</td>';
    
    
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
       
        
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货状态</td>';
        
        
        $strTable .= '</tr>';


        $cat3_arr = Db::name('GoodsCategory')->column('id,name');



        foreach ($orderList as $k => $val) {

            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {

                if ($store['store_id'] === $val['store_id']) {

                    $tempStore = $store;

                }
            }

            foreach ($sellers as $seller) {

                if ($seller['seller_id'] === $val['seller_id']) {

                    $tempSeller = $seller;

                }
            }

            $orderGoods = Db::name('OrderGoods')
                ->alias('og')
                ->join('tp_goods g', 'g.goods_id=og.goods_id')
                ->where('order_id', $val['order_id'])
                ->field('og.*,g.cat_id3')
                ->select();

            $row = count($orderGoods);





            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价

//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $i = 0;
            foreach ($orderGoods as $key => $goods) {

                $strGoods = "";

                $strTable .= '<tr>';

                $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
                $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] .' </td>';//经销商名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_id'] . ' </td>';//经销商id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] .  ' </td>';//店铺名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_id']. ' </td>';//店铺id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . ' </td>';//日期
    
                $pay_time = '';
                if($val['pay_time']){
                    $pay_time = date('Y-m-d H:i:s', $val['pay_time']);
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' .  $pay_time . ' </td>';//支付时间
                
                if(!isEmptyObject($val['master_order_sn'])){
                    $order_type ='预约转化';
                }else{
                    $order_type = '直接购买';
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $order_type . ' </td>';//订单类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_STATUS[$val['order_status']] . ' </td>';//订单状态




                if (isset($cat3_arr[$goods['cat_id3']])) {
                    $cat3_name = $cat3_arr[$goods['cat_id3']];
                } else {
                    $cat3_name = '';
                }

//                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'] . ' 商品类别:' . $cat3_name . ' ';
//                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
//                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];

                $strTable .= '<td style="text-align:left;font-size:12px;">' . $cat3_name . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['spec_key_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_num'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['sku'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_price'] . ' </td>';
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['final_price'] . ' </td>';//成交价
    
                //多个订单
                $pay_total_money = '';
                $pay_total_money = $val['order_amount']+$val['coupon_price'];
                if($row>1){
                    
                    if($i===0){
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $pay_total_money . ' </td>';//应付金额
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                    }else{
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//折叠
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//折叠
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//优惠金额
                    }
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $pay_total_money . ' </td>';//应付金额
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式
    
                $user_count = !empty($val['user_email']) ? $val['user_email'] : $val['user_mobile'];
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $user_count . '</td>';//用户账号,邮箱

                /**
                 * 物流信息
                 * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
                 */
                $deliver_doc = '';
                
                $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
    
                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['province'], $deliver_doc['city'], $deliver_doc['district']])->column('name');
                $sendFullAddress = implode('', $region) . $val['address'];
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sendFullAddress . ' </td>';//收货地址
    
                
                if($val['shipping_id']==3){
                    $shipping_info = json_decode($val['shipping_info'],true);
                    $ziti_info= json_decode($val['ziti_info'],true);

                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_name'] . '</td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_mobile'] . '</td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['plan_time'] . '</td>';//取件时间
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件时间
                }
                
                
                
                
//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['shipping_name'] . '</td>';//快递公司
               
                $strTable .= '<td style="text-align:left;font-size:12px;">&nbsp;' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的
    
    
                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['seller_address_province_id'], $deliver_doc['seller_address_city_id'], $deliver_doc['seller_address_district_id']])->column('name');
                $sellerFullAddress = implode('', $region) . $deliver_doc['seller_address'];
                
                
    
                if(!empty($shipping_info)){
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['consignee'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['mobile'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['province_name'].$shipping_info['district_name'].$shipping_info['address'] . '</td>';//发货仓库
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_consignee'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_mobile'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $sellerFullAddress . '</td>';//发货仓库
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s',$deliver_doc['create_time']) . '</td>';//发货时间

    
                /**
                 * 发票信息
                 *
                 */
                $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();
               
    
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_TYPE[$val['invoice_type']] . '</td>';//发票类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_BLAG_WAY[$invoice['blag_way']] . '</td>';//索要类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['invoice_title'] . '</td>';//发票抬头


                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言


                $strTable .= '</tr>';
                $i++;

            }
//            unset($orderGoods);



        }
        $strTable .= '</table>';

        unset($orderList);
        downloadExcel($strTable, '超管:经销商支付交易订单');
        exit();


    }
    
    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 所有支付订单导出（包含退款的)
     */
    protected function exportOrder_refund()
    {
        set_time_limit(0);
        $start_time = I('start_time', null);
        $end_time = I('end_time', null);
        if(!empty($start_time) && !empty($end_time)){
            $begin = strtotime($start_time.' 00:00:00');
            $end = strtotime($end_time.' 23:59:59');
        }else{
            $begin = $this->begin;
            $end = $this->end;
        }
        
        
        // 搜索条件 STORE_ID
        $condition = array('deleted' => 0); // 商家id
        
        //$condition['order_status'] = array('in', [1, 2, 4]);
        $condition['pay_status'] = array('in', [1, 3]);


//        'seller_id' => SELLER_ID,
//        I('store_id') ? $condition['store_id'] = I('store_id') : false;
//        I('consignee') ? $condition['consignee'] = trim(I('consignee')) : false;

//        I('prom_type') ? $condition['prom_type'] = I('prom_type') : array('lt',5);//预订订单是4也显示出来了1
        
        if ($begin && $end) {
            $condition['create_time'] = array('between', "$begin,$end");
        }
        
        I('order_sn') ? $condition['order_sn'] = trim(I('order_sn')) : false;


//
//
//        I('pay_status') != '' ? $condition['pay_status'] = I('pay_status/d') : false;
//        I('pay_code') != '' ? $condition['pay_code'] = I('pay_code') : false;
//        I('shipping_status') != '' ? $condition['shipping_status'] = I('shipping_status/d') : false;
//        I('order_statis_id/d') != '' ? $condition['order_statis_id'] = I('order_statis_id/d') : false; // 结算统计的订单
//
//        $sort_order = I('order_by', 'DESC') . ' ' . I('sort');
//        $condition['prom_type'] = array('lt',5);


//        $count = Db::name('order'.$select_year)->where($condition)->count();


//        //搜索条件
//        $where['seller_id'] = SELLER_ID;
//
//        $store_id = I('store_id',null);
//        if($store_id){
//            $where['store_id'] = $store_id;
//        }
//
//        $consignee = I('consignee');//收货人
//        if ($consignee) {
//            $where['consignee'] = ['like', '%'.$consignee.'%'];
//        }
//
//        $order_sn = I('order_sn');
//        if ($order_sn) {
//            $where['order_sn'] = $order_sn;
//        }
//
//        $order_status = I('order_status');
//        if ($order_status) {
//            $where['order_status'] = $order_status;
//        }
//
//        $timegap = I('timegap');
//        if ($timegap) {
//            $gap = explode('-', $timegap);
//            $begin = strtotime($gap[0]);
//            $end = strtotime($gap[1]);
//            $where['add_time'] = ['between',[$begin,$end]];
//        }
        
        
        /**
         * 用户手动勾选的优先级最高，覆盖之前的所有
         */
//        $order_id_arr = I('order_id_arr',null);
//        if($order_id_arr&&$order_id_arr!=''){
//            $condition = [];
//            $condition['order_id'] = array('in',$order_id_arr);//explode(',',$order_id_arr));
//        }
        
        
        $region = Db::name('region')->cache(true)->getField('id,name');


//        var_dump($where);die;
        
        //->field("*,FROM_UNIXTIME(add_time,'%Y-%m-%d') as create_time")
        $orderList = Db::name('order')->where($condition)->order('order_id')->select();
        
        
        $user_id_arr = [];
        foreach ($orderList as $order) {
            $user_id_arr[] = $order['user_id'];
        }
        
        $userList = Db::name('users')->where('user_id', 'in', $user_id_arr)->field('user_id,email,mobile')->select();
        foreach ($orderList as &$order) {
            foreach ($userList as $user) {
                if ($order['user_id'] == $user['user_id']) {
                    $order['user_email'] = $user['email'];
                    $order['user_mobile'] = $user['mobile'];
                }
            }
        }
        
        
        //只读一次比每次都读一次消耗小很多
        $stores = Db::name('store')->field('store_id,store_name')->select();
        $sellers = Db::name('seller')->where('is_admin', 1)->field('seller_id,seller_name')->select();
        $ORDER_STATUS = config('ORDER_STATUS');
        $SHIPPING_STATUS = config('SHIPPING_STATUS');
        $PAY_STATUS = config('PAY_STATUS');
        $INVOICE_TYPE = config('INVOICE_TYPE');
        $INVOICE_BLAG_WAY = config('INVOICE_BLAG_WAY');
        
        
        $strTable = '<table width="500" border="1">';
        $strTable .= '<tr>';

//        订单编号	店铺名称	下单时间	订单状态	发货状态	商品名称	数量	规格属性	商品型号	单价	合计金额	收货地址	收货人	收货人电话	支付方式	发票信息	买家留言	发货状态	配送方式	物流单号	序列号
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">序列号</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;">订单编号</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">经销商ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="120">店铺ID</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">下单时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="100">支付时间</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">订单状态</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品类别</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品名称 </td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品属性</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">商品数量</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">SKU名称</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">单价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">成交价</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">应付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">实付金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">优惠金额(元)</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">支付方式</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">用户账号</td>';
//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">收货地址</td>';
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">取件人电话</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">预约时间</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">配送方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">快递公司</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;width:120px;" >物流单号</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货人</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货联系方式</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货地址</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货时间</td>';
        
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">索要类型</td>';
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发票抬头</td>';
        
        
        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">买家留言</td>';


//        $strTable .= '<td style="text-align:center;font-size:12px;" width="*">发货状态</td>';
        
        
        $strTable .= '</tr>';
        
        
        $cat3_arr = Db::name('GoodsCategory')->column('id,name');
        
        
        
        foreach ($orderList as $k => $val) {
            
            $tempStore = [];
            $tempSeller = [];
            foreach ($stores as $store) {
                
                if ($store['store_id'] === $val['store_id']) {
                    
                    $tempStore = $store;
                    
                }
            }
            
            foreach ($sellers as $seller) {
                
                if ($seller['seller_id'] === $val['seller_id']) {
                    
                    $tempSeller = $seller;
                    
                }
            }
            
            
            //订单加入退款状态，而目前状态又都下放到return-goods表和order_goods表关联来确认是否有退款，退款到什么状态了。
            $orderGoods = Db::name('OrderGoods')
                ->alias('og')
                ->join('tp_goods g', 'g.goods_id=og.goods_id')
                ->join('tp_return_goods rg','og.order_id=rg.order_id and og.goods_id=rg.goods_id','LEFT')
                ->where('og.order_id', $val['order_id'])
                ->field('og.*,g.cat_id3,rg.status as return_goods_status')
                ->select();
            
            $row = count($orderGoods);
            
            
            
            
            
            /**
             * 商品信息
             */
            //商品名称	数量	规格属性	商品型号	单价

//                ->alias('g')
//                ->join('SpecGoodsPrice p', 'g.goods_id=p.goods_id and g.spec_key = p.key')
//                ->where('g.order_id', $val['order_id'])->field('g.*,p.sku')->select();
            $i = 0;
            foreach ($orderGoods as $key => $goods) {
                
                $strGoods = "";
                
                $strTable .= '<tr>';
                
                $strTable .= '<td style="text-align:center;font-size:12px;" >&nbsp;' . $val['order_id'] . '</td>';//订单id,故意留空
                $strTable .= '<td style="text-align:center;font-size:12px;">&nbsp;' . $val['order_sn'] . '</td>';//订单编号
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_name'] .' </td>';//经销商名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempSeller['seller_id'] . ' </td>';//经销商id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_name'] .  ' </td>';//店铺名称
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $tempStore['store_id']. ' </td>';//店铺id
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s', $val['create_time']) . ' </td>';//日期
                
                $pay_time = '';
                if($val['pay_time']){
                    $pay_time = date('Y-m-d H:i:s', $val['pay_time']);
                }
                $strTable .= '<td style="text-align:left;font-size:12px;">' .  $pay_time . ' </td>';//支付时间
                
                if(!isEmptyObject($val['master_order_sn'])){
                    $order_type ='预约转化';
                }else{
                    $order_type = '直接购买';
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $order_type . ' </td>';//订单类型
                
                
                $ORDER_GOODS_STATUS = $ORDER_STATUS[$val['order_status']];
                
//                $ORDER_GOODS_STATUS_ARR = [0=>'待审核',1=>'']
//                -2用户取消 -1请求驳回 0待审核 1通过 2已发货 3已收货待检验 4检测货品完成 5检测货品不成功 6退款成功 7退款失败
                
                switch ($goods['return_goods_status']){
                    case 0:
                    if(!isEmptyObject($goods['return_goods_status'])){
                        $ORDER_GOODS_STATUS = '退款中-待审核';
                    }
                    
                    break;
                    case 1:
                    $ORDER_GOODS_STATUS = '退款中-审核通过';
                    break;
                    case 2:
                    $ORDER_GOODS_STATUS = '退款中-已发货';
                    break;
                    case 3:
                    $ORDER_GOODS_STATUS = '退款中-已收货待检验';
                    break;
                    case 4:
                    $ORDER_GOODS_STATUS = '退款中-检测货品完成';
                    break;
                    case 6:
                    $ORDER_GOODS_STATUS = '退款成功';
                        break;
                        
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $ORDER_GOODS_STATUS . ' </td>';//订单状态
                
                
                
                
                if (isset($cat3_arr[$goods['cat_id3']])) {
                    $cat3_name = $cat3_arr[$goods['cat_id3']];
                } else {
                    $cat3_name = '';
                }

//                $strGoods .= "商品编号：" . $goods['goods_sn'] . " 商品名称：" . $goods['goods_name'] . ' 商品类别:' . $cat3_name . ' ';
//                if ($goods['spec_key_name'] != '') $strGoods .= " 规格：" . $goods['spec_key_name'];
//                $strGoods .= "数量：" . $goods['goods_num'] . " 单价：" . $goods['final_price'] . " SKU：" . $goods['sku'];
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $cat3_name . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['spec_key_name'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_num'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['sku'] . ' </td>';
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['goods_price'] . ' </td>';
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $goods['final_price'] . ' </td>';//成交价
    
                //多个订单
                $pay_total_money = '';
                $pay_total_money = $val['order_amount']+$val['coupon_price'];
                if($row>1){
        
                    if($i===0){
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $pay_total_money . ' </td>';//应付金额
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                        $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                    }else{
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//折叠
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//折叠
                        $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//优惠金额
                    }
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $pay_total_money . ' </td>';//应付金额
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['total_amount'] . ' </td>';//实付金额，可能有优惠什么的
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['coupon_price'] . ' </td>';//优惠金额
                }
              
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['pay_name'] . '</td>';//支付方式
                
                $user_count = !empty($val['user_email']) ? $val['user_email'] : $val['user_mobile'];
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $user_count . '</td>';//用户账号,邮箱
                
                
                
                
                /**
                 * 物流信息
                 * 这里没法做多个，因为现在订单里面有记录收货地址和自提的信息。如果发货可以分开发或者拆单的操作，name就需要对数据库进行改版。成本很高，后面有需求再改
                 */
                $deliver_doc = '';
                
                $deliver_doc = Db::name('deliveryDoc')->where('order_id', $val['order_id'])->find();
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['consignee'] . ' </td>';//收货人
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['mobile'] . '</td>';//收货人电话
                
                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['province'], $deliver_doc['city'], $deliver_doc['district']])->column('name');
                $sendFullAddress = implode('', $region) . $val['address'];
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $sendFullAddress . ' </td>';//收货地址
                
                
                if($val['shipping_id']==3){
                    $shipping_info = json_decode($val['shipping_info'],true);
                    $ziti_info= json_decode($val['ziti_info'],true);
                    
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_name'] . '</td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['user_mobile'] . '</td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $ziti_info['plan_time'] . '</td>';//取件时间
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件人联系方式
                    $strTable .= '<td style="text-align:left;font-size:12px;"></td>';//取件时间
                }




//            $strTable .= '<td style="text-align:left;font-size:12px;">' . $SHIPPING_STATUS[$val['shipping_status']] . '</td>';//发货状态
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['shipping_name'] . '</td>';//配送方式
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['shipping_name'] . '</td>';//快递公司
                
                $strTable .= '<td style="text-align:left;font-size:12px;">&nbsp;' . $deliver_doc['invoice_no'] . '</td>';//物流单号,这名字怎么怪怪的
                
                
                $region = Db::name('region')->where('id', 'IN', [$deliver_doc['seller_address_province_id'], $deliver_doc['seller_address_city_id'], $deliver_doc['seller_address_district_id']])->column('name');
                $sellerFullAddress = implode('', $region) . $deliver_doc['seller_address'];
                
                
                
                if(!empty($shipping_info)){
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['consignee'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['mobile'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $shipping_info['province_name'].$shipping_info['district_name'].$shipping_info['address'] . '</td>';//发货仓库
                }else{
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_consignee'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $deliver_doc['seller_address_mobile'] . '</td>';//发货仓库
                    $strTable .= '<td style="text-align:left;font-size:12px;">' . $sellerFullAddress . '</td>';//发货仓库
                }
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . date('Y-m-d H:i:s',$deliver_doc['create_time']) . '</td>';//发货时间
                
                
                /**
                 * 发票信息
                 *
                 */
                $invoice = Db::name('invoice')->where('order_id', $val['order_id'])->find();
                
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_TYPE[$val['invoice_type']] . '</td>';//发票类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $INVOICE_BLAG_WAY[$invoice['blag_way']] . '</td>';//索要类型
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['invoice_title'] . '</td>';//发票抬头
                
                
                $strTable .= '<td style="text-align:left;font-size:12px;">' . $val['user_note'] . ' </td>';//买家留言
                
                
                $strTable .= '</tr>';
                $i++;
                
            }
//            unset($orderGoods);
        
        
        
        }
        $strTable .= '</table>';
        
        unset($orderList);
        downloadExcel($strTable, '超管:经销商支付交易订单(包含退款)');
        exit();
        
        
    }


}