<?php
namespace app\admin\controller;
use think\AjaxPage;
use think\Page;
use think\Db;
use think\Loader;
use app\seller\logic\GoodsCategoryLogic;

class Coupon extends Base {
    /**----------------------------------------------*/
     /*                优惠券控制器                  */
    /**----------------------------------------------*/
    
    
    /**
     * 获取店铺商品分类
     */
    public function goods_category()
    {
        $parent_id = input('parent_id/d', 0); // 商品分类 父id
        $GoodsCategoryLogic = new GoodsCategoryLogic();
        $GoodsCategoryLogic->setStore($this->storeInfo);
        $goods_category_list = $GoodsCategoryLogic->getStoreGoodsCategory($parent_id);
        $this->ajaxReturn($goods_category_list);
    }
    
    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 根据cid和type获取可以勾选和已经勾选中的店铺
     */
    public function getStoreList()
    {
        header("Content-type: text/html; charset=utf-8");
        
        $cid = I('id',null);
        $type = I('type',null);
        
        empty($type) && exit('');
        $return_arr = array(
            'status' => 1,
            'msg' => '操作成功',
            'data' => array('url' => U('Goods/goodsAttributeList')),
        );
        $query = Db::name('coupon')
            ->alias('c')
            ->join('tp_coupon_store s','c.id=s.cid')
            ->where("c.type", $type)->where('s.seller_id',SELLER_ID);
        
        
        $list = [];
        
        //获取经销商下并且有效的店铺
        $allStores = Db::name('store')->where('seller_id',SELLER_ID)->where('store_enabled',1)->select();
        
        
        //已有商品的需要勾选check
        if($cid){
            $isHasStores = $query->where('c.id',$cid)->field('c.*,s.store_id,s.seller_id')->select();
            // 找到这个分类对应的type_id
            $isHasStoreIds = get_arr_column($isHasStores,'store_id');
            //把已有的放进来
            foreach ($allStores as $store){
                if(in_array($store['store_id'],$isHasStoreIds)){
                    $store['check'] = 1;
                    $list[] = $store;
                }
            }
            
        }else{
            $isHasStores = $query->field('c.*,s.store_id,s.seller_id')->select();
            // 找到这个分类对应的type_id
            $isHasStoreIds = get_arr_column($isHasStores,'store_id');
        }
        
        //把还可以选择的放进来
        foreach ($allStores as $store){
            if(!in_array($store['store_id'],$isHasStoreIds)){
                $list[] = $store;
            }
        }
        
        
        $this->assign('stores',$list);
        return $this->fetch();
    }
    
    /**
     *  商品分类列表
     */
    public function storeList(){
        
        
        //只拿有效的
        $where = ['seller_id'=>SELLER_ID];
        
        //优先按照store_id的下来框来，而且store_name也是独一无二的，所以可以用来检索。
        $store_id = I('store_id',null);
        $store_name = I("store_name",null);
        if($store_id){
            $where['store_id'] = array('eq',$store_id);
            $this->assign('store_id',$store_id);
        }else{
            if($store_name){
                $store = Store::get(['store_name'=>$store_name]);
                if($store){
                    $where['store_id'] = array('eq',$store['store_id']);
                }
            }
            $this->assign('store_name',$store_name);
        }
        
        $stores = Db::name('store')->where('seller_id',SELLER_ID)->field('store_id,store_name')->select();
        $this->assign('stores',$stores);
        
        $cats =  Db::name('goods_category')->where('level',3)->order('sort_order desc')->select();
        
        
        $discountList = Db::name('sellerDiscount')->where($where)->whereNotNull('store_id')->order('store_id')->select();
        
        foreach ($discountList as &$discount){
            
            
            if(isset($discount['discount_start'])){
                $discount['discount_start'] = date('Y-m-d',$discount['discount_start']);
                
            }
            
            if(isset($discount['discount_end'])){
                
                $discount['discount_end'] = date('Y-m-d',$discount['discount_end']);
            }
            
            //加上名字好区分
            foreach ($stores as $store){
                if($discount['store_id']==$store['store_id']){
                    $discount['store_name'] = $store['store_name'];
                }
            }
            
            foreach ($cats as $cat){
                if($discount['catid']==$cat['id']){
                    $discount['catname'] = $cat['name'];
                }
            }
        }
        
        
        $this->assign('lists',$discountList);
        
        return $this->fetch('storeList');
    }
    
    /**
     * 后台新增优惠券
     * @return mixed
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function single(){
    
        $seller_id = 0;
    
        $this->assign('isadd',1);
    
    
        if (IS_POST) {
        
            $data = I('post.');
            if ($data['type']>0){
                $data['send_start_time'] = strtotime($data['send_start_time']);
                $data['send_end_time'] = strtotime($data['send_end_time']);
            }else{
                $data['send_start_time'] = '';
                $data['send_end_time'] = '';
            }
//            $data['store_name'] = Db::name('store')->where('id',$data['store_id'])->column('store_name');
            $data['use_end_time'] = strtotime($data['use_end_time']);
            $data['use_start_time'] = strtotime($data['use_start_time']);
        
        
        
            if (empty($data['id'])) {
            
            
                //只有新增才做校验
                $couponValidate = Loader::validate('Coupon');
                
                if (!$couponValidate->batch()->check($data)) {
                    $this->ajaxReturn([
                        'status' => 0, 'msg' => '操作失败:表单校验不合格',
                        'result' => $couponValidate->getError(),
                        'token'    => \think\Request::instance()->token()
                    ]);
                }
            
            
                $type = input('type', null);
                $money = input('money', null);
                $percent = input('percent/d', null);


//                var_dump($data);
//                die;
            
                if ($type == 4 && !$money) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '注册赠送优惠券必须设置金额', 'result' => '']);
                }
            
            
                if ($type == 5 && (!$percent || $percent < 1)) {
                    $this->ajaxReturn(['status' => 0, 'msg' => '回收赠送优惠券必须设置百分比', 'result' => '']);
                }
            
                //I('store/a')
                //改成全局的store_id
                $store_id_arr = Db::name('store')->column('store_id');
                if(count($store_id_arr)<1){
                    $this->ajaxReturn(['status' => 0, 'msg' => '提交失败:至少选择一个店铺', 'result' => '']);
                }
            
                //店铺通用
                if($data['use_type'] == 3){

//                    $stores = I('store/a');
//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选', 'result' => '']);
//                    }
                
                    $data['add_time'] = time();
                    $data['seller_id'] = $seller_id;
                    $newCid = Db::name('coupon')->insertGetId($data);

//                    $newCid = Db::name('goods_coupon')->insertGetId(['coupon_id'=>$row,'seller_id'=>SELLER_ID]);
                    $temp = [];
                    foreach ($store_id_arr as $store_id){
                        $temp[]=['store_id'=>$store_id,'cid'=>$newCid,'seller_id'=>$seller_id];
                    }
                    if(count($temp)>0){
                        Db::name('CouponStore')->insertAll($temp);
                    }
                
                }

//                if($data['use_type'] == 1){
//
//                    $data['add_time'] = time();
//                    $data['seller_id'] = SELLER_ID;
//                    $row = Db::name('coupon')->insertGetId($data);
//
//                    //指定商品
//                    foreach ($data['goods_id'] as $v) {
//                        Db::name('goods_coupon')->add(['coupon_id'=>$row,'goods_id'=>$v,'seller_id'=>SELLER_ID]);
//                    }
//
//                }
            
                //指定类别，则店铺是可选的了
                if($data['use_type'] == 2){

//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '设置指定分类优惠券时店铺至少选择一个', 'result' => '']);
//                    }
                
                    $data['add_time'] = time();
                    $data['seller_id'] = $seller_id;
                    $newCid = Db::name('coupon')->insertGetId($data);
                    
                    $goodsCoupon = ['coupon_id'=>$newCid,'goods_category_id'=>$data['cat_id3']];
                    Db::name('GoodsCoupon')->insertGetId($goodsCoupon);
                    
                
                    $temp = [];
                    foreach ($store_id_arr as $store_id){
                        $temp[]=['store_id'=>$store_id,'cid'=>$newCid,'seller_id'=>$seller_id];
                    }
                    if(count($temp)>0){
                        Db::name('CouponStore')->insertAll($temp);
                    }

//                    Db::name('goods_coupon')->add(['coupon_id'=>$row,'goods_category_id'=>$data['cat_id3'],'store_id'=>$store_id,'seller_id'=>SELLER_ID]);
                
                }
            
            
            
            } else {
            
            
//                $store_id_arr = I('store/a');
//                if(count($store_id_arr)<1){
//                    $this->ajaxReturn(['status' => 0, 'msg' => '提交失败:至少选择一个店铺', 'result' => '']);
//                }
            
            
            
                $row = M('coupon')->where(array('id' => $data['id'], 'seller_id' => $seller_id))->save($data);
            
            
                //删除之前的老的
//                Db::name('CouponStore')->where('cid',$data['id'])->delete();//先删除后添加

            
            
                //全局的不需要绑定店铺
//                $temp = [];
//                foreach ($store_id_arr as $store_id) {
//                    $temp[] = ['store_id' => $store_id, 'cid' => $data['id'], 'seller_id' => $seller_id];
//                }
//
//
//                if (count($temp) > 0) {
//
//                    Db::name('CouponStore')->insertAll($temp);
//                }
                //清空之前的记录
                Db::name('GoodsCoupon')->where('coupon_id',$data['id'])->delete();
            
                if($data['use_type'] == 3){

//                    $temp = [];
//                    foreach ($store_id_arr as $store_id){
//                        $temp[]=['store_id'=>$store_id,'cid'=>$data['id'],'seller_id'=>SELLER_ID];
//                    }
//                    if(count($temp)>0){
//                        Db::name('CouponStore')->insertAll($temp);
//                    }

//                    if(count($stores)<1){
//                        $this->ajaxReturn(['status' => 0, 'msg' => '店铺必选', 'result' => '']);
//                    }
//                    Db::name('goods_coupon')->add(['coupon_id'=>$data['id'],'store_id'=>$store_id,'seller_id'=>SELLER_ID,]);
                
                
                }
            
            
                //指定商品
//                if($data['use_type'] == 1){
//                    foreach ($data['goods_id'] as $v) {
//                        Db::name('goods_coupon')->add(['coupon_id'=>$data['id'],'goods_id'=>$v,'seller_id'=>SELLER_ID,'add_time'=>time()]);
//                    }
//                }
            
            
                //指定商品分类id
                if($data['use_type'] == 2){
    
    
                   
                    $goodsCoupon = ['coupon_id'=>$data['id'],'goods_category_id'=>$data['cat_id3']];
                    Db::name('GoodsCoupon')->insertGetId($goodsCoupon);
                    
                }
            
            }
            if ($row !== false) {
                $this->ajaxReturn(['status' => 1, 'msg' => '提交成功', 'result' => '']);
            } else {
                $this->ajaxReturn(['status' => 0, 'msg' => '提交失败', 'result' => '']);
            }
        }
    
        
        $coupon_price_list = Db::name('coupon_price')->where('')->select();
        if(empty($coupon_price_list)){
            $this->error('总平台没有设置优惠券面额，商家不能添加优惠券');
        }
        $cid = I('get.id/d');
        if ($cid) {
        
        
            $coupon = M('coupon')->where(array('id' => $cid, 'seller_id' => $seller_id))->find();
            if (empty($coupon)) {
                $this->error('代金券不存在');
            }else{
            
                $this->assign('isadd',0);
            
            
                if($coupon['use_type'] == 2){
                    
                    $goods_coupon = Db::name('goods_coupon')->where('coupon_id',$cid)->find();
                    $cat_info = M('goods_category')->where(array('id'=>$goods_coupon['goods_category_id']))->find();
                    $cat_path = explode('_', $cat_info['parent_id_path']);
                    
                    
                    $coupon['cat_id1'] = $cat_path[1];
                    $coupon['cat_id2'] = $cat_path[2];
                    $coupon['cat_id3'] = $goods_coupon['goods_category_id'];
                    
                    //$isStores = Db::name('goods_coupon')->where(['coupon_id'=>$cid,'seller_id'=>SELLER_ID])->column('store_id');
                
                }
            
            
                if($coupon['use_type'] == 1){
                    $coupon_goods_ids = Db::name('goods_coupon')->where('coupon_id',$cid)->getField('goods_id',true);
                    $enable_goods = M('goods')->where("goods_id", "in", $coupon_goods_ids)->select();
                    $this->assign('enable_goods',$enable_goods);
                
                    //$isStores = Db::name('goods_coupon')->where(['coupon_id'=>$cid,'seller_id'=>SELLER_ID])->column('store_id');
                
                }
            
            
                if($coupon['use_type'] == 3){
                
                    //$isStores = Db::name('goods_coupon')->where('coupon_id',$cid)->column('store_id');
                
                }
            
            
                //三种渠道都有选择店铺列表
//                foreach ($stores as &$store) {
//                    $store['select'] = 0;
//                    foreach ($isStores as $selectStore) {
//                        if ($store['store_id'] == $selectStore) {
//                            $store['select'] = 1;
//                            break;
//                        }
//                    }
//                }
            
            
            }
            $this->assign('coupon', $coupon);
        
        
        } else {
            $def['send_start_time'] = strtotime("+1 day");
            $def['send_end_time'] = strtotime("+1 month");
            $def['use_start_time'] = strtotime("+1 day");
            $def['use_end_time'] = strtotime("+2 month");
            $this->assign('coupon', $def);
        }
        $cat_list = M('goods_category')->where(['parent_id' => 0])->select();//自营店已绑定所有分类
//        $bind_all_gc = M('store')->where(array('store_id'=>STORE_ID))->getField('bind_all_gc');
//        if ($bind_all_gc == 1) {
//            $cat_list = M('goods_category')->where(['parent_id' => 0])->select();//自营店已绑定所有分类
//        } else {
//            //自营店已绑定所有分类
//            $cat_list = Db::name('goods_category')->where(['parent_id' => 0])->where('id', 'IN', function ($query) {
//                $query->name('store_bind_class')->where('store_id', STORE_ID)->where('state', 1)->field('class_1');
//            })->select();
//        }
    

        $this->assign('cat_list',$cat_list);
        $this->assign('coupon_price_list',$coupon_price_list);
        return $this->fetch();
        
        
        
    }
    
    
    /*
     * 优惠券类型列表
     */
    public function index(){
        //获取优惠券列表
        $map = array();
        $name = I('name');
        if(!empty($name)) $map['name'] = array('like',"%$name%");
        $begin = strtotime(I('add_time_begin'));
        $end = strtotime(I('add_time_end'));
        if($begin && $end){
            $map['add_time'] = array('between',"$begin,$end");
        }
        $count =  M('coupon')->where($map)->count();
        $Page = new Page($count,10);
        $show = $Page->show();
        $lists = M('coupon')->where($map)->order('add_time desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        
        $seller_id = get_arr_column($lists,'seller_id');
        if(!empty($seller_id)){
            $seller = M('seller')->where("seller_id in (".implode(',', $seller_id).")")->getField('seller_id,seller_name');
        }
        $this->assign('seller',$seller);
        $this->assign('lists',$lists);
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('pager',$Page);// 赋值分页输出
        $this->assign('coupons',C('COUPON_TYPE'));
        return $this->fetch();
    }

    /*
     * 添加编辑一个优惠券类型
     */
    public function coupon_info(){
        $cid = I('get.id');
        if($cid){
            $coupon = M('coupon')->where(array('id'=>$cid))->find();
            $coupon['store_name'] = M('store')->where(array('store_id'=>$coupon['store_id']))->getField('store_name');
            $this->assign('coupon',$coupon);
        }else{
            $def['send_start_time'] = strtotime("+1 day");
            $def['send_end_time'] = strtotime("+1 month");
            $def['use_start_time'] = strtotime("+1 day");
            $def['use_end_time'] = strtotime("+2 month");
            $this->assign('coupon',$def);
        }
        $this->assign('coupons',C('COUPON_TYPE'));
        return $this->fetch();
    }

    /*
    * 优惠券发放
    */
    public function make_coupon(){
        //获取优惠券ID
        $cid = I('get.id');
        $type = I('get.type');
        //查询是否存在优惠券
        $data = M('coupon')->where(array('id'=>$cid))->find();
        $remain = $data['createnum'] - $data['send_num'];//剩余派发量
    	if($remain<=0) $this->error($data['name'].'已经发放完了');
        if(!$data) $this->error("优惠券类型不存在");
        if($type != 3) $this->error("该优惠券类型不支持发放");
        if(IS_POST){
            $num  = I('post.num/d');
            if($num>$remain) $this->error($data['name'].'发放量不够了');
            if(!$num > 0) $this->error("发放数量不能小于0");
            $add['cid'] = $cid;
            $add['type'] = $type;
            $add['send_time'] = time();
            for($i=0;$i<$num; $i++){
                do{
                    $code = get_rand_str(8,0,1);//获取随机8位字符串
                    $check_exist = M('coupon_list')->where(array('code'=>$code))->find();
                }while($check_exist);
                $add['code'] = $code;
                M('coupon_list')->add($add);
            }
            M('coupon')->where("id=$cid")->setInc('send_num',$num);
            adminLog("发放".$num.'张'.$data['name']);
            $this->success("发放成功",U('Admin/Coupon/index'));
            exit;
        }
        $this->assign('coupon',$data);
        return $this->fetch();
    }
    
    public function ajax_get_user(){
    	//搜索条件
    	$condition = array();
    	I('mobile') ? $condition['mobile'] = I('mobile') : false;
    	I('email') ? $condition['email'] = I('email') : false;
    	$nickname = I('nickname');
    	if(!empty($nickname)){
    		$condition['nickname'] = array('like',"%$nickname%");
    	}
    	$model = M('users');
    	$count = $model->where($condition)->count();
    	$Page  = new AjaxPage($count,10);
    	foreach($condition as $key=>$val) {
    		$Page->parameter[$key] = urlencode($val);
    	}
    	$show = $Page->show();
    	$userList = $model->where($condition)->order("user_id desc")->limit($Page->firstRow.','.$Page->listRows)->select();
        
        $user_level = M('user_level')->getField('level_id,level_name',true);       
        $this->assign('user_level',$user_level);
    	$this->assign('userList',$userList);
    	$this->assign('page',$show);
    	return $this->fetch();
    }
    
    public function send_coupon(){
    	$cid = I('cid');    	
    	if(IS_POST){
    		$level_id = I('level_id');
    		$user_id = I('user_id');
    		$insert = '';
    		$coupon = M('coupon')->where("id=$cid")->find();
    		if($coupon['createnum']>0){
    			$remain = $coupon['createnum'] - $coupon['send_num'];//剩余派发量
    			if($remain<=0) $this->error($coupon['name'].'已经发放完了');
    		}
    		
    		if(empty($user_id) && $level_id>=0){
    			if($level_id==0){
    				$user = M('users')->where("is_lock=0")->select();
    			}else{
    				$user = M('users')->where("is_lock=0 and level_id=$level_id")->select();
    			}
    			if($user){
    				$able = count($user);//本次发送量
    				if($coupon['createnum']>0 && $remain<$able){
    					$this->error($coupon['name'].'派发量只剩'.$remain.'张');
    				}
    				foreach ($user as $k=>$val){
    					$user_id = $val['user_id'];
    					$time = time();
    					$gap = ($k+1) == $able ? '' : ',';
    					$insert .= "($cid,1,$user_id,$time)$gap";
    				}
    			}
    		}else{
    			$able = count($user_id);//本次发送量
    			if($coupon['createnum']>0 && $remain<$able){
    				$this->error($coupon['name'].'派发量只剩'.$remain.'张');
    			}
    			foreach ($user_id as $k=>$v){
    				$time = time();
    				$gap = ($k+1) == $able ? '' : ',';
    				$insert .= "($cid,1,$v,$time)$gap";
    			}
    		}
			$sql = "insert into __PREFIX__coupon_list (`cid`,`type`,`uid`,`send_time`) VALUES $insert";
            Db::execute($sql);
			M('coupon')->where("id=$cid")->setInc('send_num',$able);
			adminLog("发放".$able.'张'.$coupon['name']);
			$this->success("发放成功");
			exit;
    	}
    	$level = M('user_level')->select();
    	$this->assign('level',$level);
    	$this->assign('cid',$cid);
    	return $this->fetch();
    }

    /*
     * 删除优惠券类型
     */
    public function del_coupon(){
        //获取优惠券ID
        $cid = I('get.id');
        //查询是否存在优惠券
        $row = M('coupon')->where(array('id'=>$cid))->delete();
        if (!$row) {
            $this->ajaxReturn(['status' => 0, 'msg' => '优惠券不存在，删除失败']);
        }
        
        //删除此类型下的优惠券
        M('coupon_list')->where(array('cid'=>$cid))->delete();
        $this->ajaxReturn(['status' => 1, 'msg' => '删除成功']);
    }


    /*
     * 优惠券详细查看
     */
    public function coupon_list(){
        //获取优惠券ID
        $cid = I('get.id');
        //查询是否存在优惠券
        $check_coupon = M('coupon')->field('id,type')->where(array('id'=>$cid))->find();
        if(!$check_coupon['id'] > 0)
            $this->error('不存在该类型优惠券');
       
        //查询该优惠券的列表的数量
        $sql = "SELECT count(1) as c FROM __PREFIX__coupon_list  l ".
                "LEFT JOIN __PREFIX__coupon c ON c.id = l.cid ". //联合优惠券表查询名称
                "LEFT JOIN __PREFIX__order o ON o.order_id = l.order_id ".     //联合订单表查询订单编号
                "LEFT JOIN __PREFIX__users u ON u.user_id = l.uid WHERE l.cid = ".$cid;    //联合用户表去查询用户名        
        
        $count = Db::query($sql);
        $count = $count[0]['c'];
    	$Page = new Page($count,10);
    	$show = $Page->show();
        
        //查询该优惠券的列表
        $sql = "SELECT l.*,c.name,o.order_sn,u.nickname FROM __PREFIX__coupon_list  l ".
                "LEFT JOIN __PREFIX__coupon c ON c.id = l.cid ". //联合优惠券表查询名称
                "LEFT JOIN __PREFIX__order o ON o.order_id = l.order_id ".     //联合订单表查询订单编号
                "LEFT JOIN __PREFIX__users u ON u.user_id = l.uid WHERE l.cid = ".$cid.    //联合用户表去查询用户名
                " limit {$Page->firstRow} , {$Page->listRows}";
        $coupon_list = Db::query($sql);
        $this->assign('coupon_type',C('COUPON_TYPE'));
        $this->assign('type',$check_coupon['type']);       
        $this->assign('lists',$coupon_list);            	
    	$this->assign('page',$show);        
    	$this->assign('pager',$Page);
        return $this->fetch();
    }
    
    /*
     * 删除一张优惠券
     */
    public function coupon_list_del(){
        //获取优惠券ID
        $cid = I('get.id');
        if(!$cid)
            $this->error("缺少参数值");
        //查询是否存在优惠券
         $row = M('coupon_list')->where(array('id'=>$cid))->delete();
        if(!$row)
            $this->error('删除失败');
        $this->success('删除成功');
    }

    /**
     * 优惠券面额列表
     * @return mixed
     */
    public function coupon_price_list()
    {
        $couponPriceCount =  Db::name('coupon_price')->where('')->count();
        $Page = new Page($couponPriceCount, 10);
        $show = $Page->show();
        $couponPriceList = Db::name('coupon_price')->where('')->limit($Page->firstRow.','.$Page->listRows)->select();
        $this->assign('couponPriceList',$couponPriceList);
        $this->assign('page',$show);
        $this->assign('pager',$Page);
        return $this->fetch();
    }

    public function coupon_price_info()
    {
        $coupon_price_id = input('coupon_price_id');
        if (IS_POST) {
            $coupon_price_value = input('coupon_price_value');
            if ($coupon_price_id) {
                //更新
                $same_count = Db::name('coupon_price')->where('coupon_price_id','<>',$coupon_price_id)->where('coupon_price_value',$coupon_price_value)->count();
                if($same_count > 0){
                    $this->error('已存在相同面额');
                }
                $res = Db::name('coupon_price')->where(['coupon_price_id' => $coupon_price_id])->update(['coupon_price_value' => $coupon_price_value]);
            } else {
                //插入
                $same_count = Db::name('coupon_price')->where('coupon_price_value',$coupon_price_value)->count();
                if($same_count > 0){
                    $this->error('已存在相同面额');
                }
                $res = Db::name('coupon_price')->insert(['coupon_price_value' => $coupon_price_value]);
            }
            if ($res !== false) {
                $this->success('操作成功', U('Coupon/coupon_price_list'));
            }else{
                $this->error('操作失败');
            }
        }
        if ($coupon_price_id) {
            $coupon_price_info = Db::name('coupon_price')->where('coupon_price_id', $coupon_price_id)->find();
            $this->assign('coupon_price_info', $coupon_price_info);
        }
        return $this->fetch();
    }

    public function coupon_price_del()
    {
        $coupon_price_id= I('post.del_id');
        if(empty($coupon_price_id)){
            $this->ajaxReturn('参数错误');
        }
        $del = Db::name('coupon_price')->where('coupon_price_id',$coupon_price_id)->delete();
        if($del !== false){
            $this->ajaxReturn(['code'=>1,'msg'=>'删除成功','result'=>'']);
        }else{
            $this->ajaxReturn(['code'=>0,'msg'=>'删除失败','result'=>'']);
        }
    }
}